<!-- 04-02-ktm_objects -->

Transaction:

- [https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-createtransaction](https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-createtransaction)
- [https://www.vergiliusproject.com/kernels/x64/Windows%207%20%7C%202008R2/SP1/_KTRANSACTION](https://www.vergiliusproject.com/kernels/x64/Windows%207%20%7C%202008R2/SP1/_KTRANSACTION)

Transaction Manager:

- [https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-createtransactionmanager](https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-createtransactionmanager)
- [https://www.vergiliusproject.com/kernels/x64/Windows%207%20%7C%202008R2/SP1/_KTM](https://www.vergiliusproject.com/kernels/x64/Windows%207%20%7C%202008R2/SP1/_KTM)
- [https://docs.microsoft.com/en-us/windows/win32/ktm/transaction-managers](https://docs.microsoft.com/en-us/windows/win32/ktm/transaction-managers)

Resource Manager:

- [https://www.vergiliusproject.com/kernels/x64/Windows%207%20%7C%202008R2/SP1/_KRESOURCEMANAGER](https://www.vergiliusproject.com/kernels/x64/Windows%207%20%7C%202008R2/SP1/_KRESOURCEMANAGER)

Enlistment:

- [https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-createenlistment](https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-createenlistment)
- [https://www.vergiliusproject.com/kernels/x64/Windows%207%20%7C%202008R2/SP1/_KENLISTMENT](https://www.vergiliusproject.com/kernels/x64/Windows%207%20%7C%202008R2/SP1/_KENLISTMENT)
- [https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-commitcomplete](https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-commitcomplete)

Other:

- [https://github.com/zodiacon/PoolMonX/blob/master/res/pooltag.txt](https://github.com/zodiacon/PoolMonX/blob/master/res/pooltag.txt)
