<!-- 07-4-data_control_with_pool_manipulation -->

- [https://github.com/reactos/reactos/blob/master/drivers/filesystems/npfs/npfs.h](https://github.com/reactos/reactos/blob/master/drivers/filesystems/npfs/npfs.h)
- [http://www.uninformed.org/?v=10&a=2&t=pdf](http://www.uninformed.org/?v=10&a=2&t=pdf)
- [https://media.blackhat.com/bh-dc-11/Mandt/BlackHat_DC_2011_Mandt_kernelpool-Slides.pdf](https://media.blackhat.com/bh-dc-11/Mandt/BlackHat_DC_2011_Mandt_kernelpool-Slides.pdf)
- [https://www.gatewatcher.com/en/news/blog/windows-kernel-pool-spraying](https://www.gatewatcher.com/en/news/blog/windows-kernel-pool-spraying)
- [https://census-labs.com/media/windows_10_rs2_rs3_exploitation_primitives.pdf](https://census-labs.com/media/windows_10_rs2_rs3_exploitation_primitives.pdf)
