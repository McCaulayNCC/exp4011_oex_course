<!-- 06-3-lab_winning_the_race_with_windbg -->

Re-generate Visual Studio projects:

```
build.bat hello
```

Enable verifier:

```
verifier.exe /driver tm.sys /flags 0x00000809
```

Check verifier:

```
verifier /querysettings
```

Verifier usage:

- [https://msdn.microsoft.com/en-gb/library/windows/hardware/ff556083%28v=vs.85%29.aspx](https://msdn.microsoft.com/en-gb/library/windows/hardware/ff556083%28v=vs.85%29.aspx)
- [https://www.osronline.com/article.cfm%5Earticle=589.htm](https://www.osronline.com/article.cfm%5Earticle=589.htm)

Debugging:

```
// vulnerable function entry point
ba e 1 tm!TmRecoverResourceManagerExt

// skip a breakpoint many times
g;g;g;g;g;g;g;g;g;g;g;

// block recovery thread before sending notification
ba e 1 tm!TmRecoverResourceManagerExt+0x23a
!patch

// unblock recovery thread and hit a breakpoint
!unpatch
ba e 1 tm!TmRecoverResourceManagerExt+0x28d
```
