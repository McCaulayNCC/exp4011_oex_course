<!-- 15-1-_after_class_spread_the_word-->

If you liked the class, then please consider sharing something about it on Twitter (#OST2) or LinkedIn (#OpenSecurityTraining2).

And as always if you want to help out OST2 in other ways, we've got a list of ways you can help [here](https://ost2.fyi/How%20to%20Help.html).