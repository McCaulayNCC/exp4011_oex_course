<!-- 03-1-binary_diffing_updates -->

- [https://portal.msrc.microsoft.com/en-us/security-guidance](https://portal.msrc.microsoft.com/en-us/security-guidance)
- [https://www.zynamics.com/bindiff.html](https://www.zynamics.com/bindiff.html)
- [https://www.zynamics.com/software.html](https://www.zynamics.com/software.html)
- [https://www.zynamics.com/bindiff/manual/index.html](https://www.zynamics.com/bindiff/manual/index.html)
- [https://github.com/joxeankoret/diaphora](https://github.com/joxeankoret/diaphora)
