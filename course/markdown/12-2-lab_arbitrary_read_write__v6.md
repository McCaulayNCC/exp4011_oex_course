<!-- 12-2-lab_arbitrary_read_write -->

Debugging:

```
// post race win - superior enlistment to aid debugging
ba e 1 tm!TmRecoverResourceManagerExt+0x18c

// where bit check done on previous ThreadLock
ba e 1 nt!KiTryUnwaitThread+0x67

// where write 0 primitive happens
ba e 1 nt!KiTryUnwaitThread+0x2c0
```

Optional for debugging:

```
// vulnerable function entry point
ba e 1 tm!TmRecoverResourceManagerExt

// beginning of main loop - debug every iteration
!process 0 0 ArbitraryReadWrite_lab.exe
.process <KPROCESS ADDRESS>
tm!TmRecoverResourceManagerExt+0x102
```
