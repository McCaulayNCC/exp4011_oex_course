<!-- 04-03-ktm_states -->

- [https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-getnotificationresourcemanager](https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-getnotificationresourcemanager)
- [https://docs.microsoft.com/en-us/windows/win32/ktm/notification-mask](https://docs.microsoft.com/en-us/windows/win32/ktm/notification-mask)
- [https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-prepreparecomplete](https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-prepreparecomplete)
- [https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-preprepareenlistment](https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-preprepareenlistment)
- [https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-preparecomplete](https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-preparecomplete)
- [https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-prepareenlistment](https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-prepareenlistment)
- [https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-commitcomplete](https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-commitcomplete)
- [https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-commitenlistment](https://docs.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-commitenlistment)
- [https://docs.microsoft.com/en-us/windows/win32/ktm/recovery-processing](https://docs.microsoft.com/en-us/windows/win32/ktm/recovery-processing)
