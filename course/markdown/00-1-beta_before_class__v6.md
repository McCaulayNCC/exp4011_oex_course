<!-- 00-1-beta_before_class -->

1. Make sure you have the ost2.fyi email domain allowlisted in your mail system, so that you don't miss course announcements. (It's relatively new in the grand scheme of things and sometimes still gets sent to spam.)

2. Ask questions on the forums, not via email, twitter, etc. This is so that if something's wrong, other students can see and deal with it accordingly until a fix arrives (at which point the forum post may be deleted if it's no longer relevant.) Students are encouraged to try to answer each others' questions.

3. Cedric Halbronn is the instructor. Xeno Kovah is the platform support. If you need to email either or both of them about the class, email exp4011@ost2.fyi

4. Use the "Mark as complete" as you go through materials, to see your progress reflected on the "Progress" faux-tab at the top of the page. There are also checkmarks next to sections that go green when *the system* thinks you've completed content... but those are unrelated, and usually disabled, but Xeno hasn't yet found how to re-disable them after an Open edX upgrade. "Mark as complete" are the true completion trackers from the class.

5. Keep track of how long it takes you to go through the material. Write it down in a document off to the side, because the available Open edX mechanisms for soliciting timing feedback are suboptimal and we may need to request you to re-submit your timing later.


6. If you use subtitles in videos and see anything wrong, please submit a correction as described in the next section. Crowdsourcing the fixes helps instructors focus on content creation.

---

7. This "Exp4011: Windows Kernel Exploitation: Race Condition + UAF in KTM" class assumes you already took [Dbg3011: Advanced WinDbg](https://beta.ost2.fyi/courses/course-v1:OpenSecurityTraining2+Dbg3011_Advanced_WinDbg+2022_V1/about) and [Arch2821: Windows OS Internals 2](https://beta.ost2.fyi/courses/course-v1:OpenSecurityTraining2+Arch2021_Windows_OS_Internals_2+2022_V1/about). If you didn't, you need to take these classes first.