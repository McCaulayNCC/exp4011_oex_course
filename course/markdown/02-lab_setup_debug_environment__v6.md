<!-- 02-lab_setup_debug_environment -->

Reversing and debugging tools:

* [https://www.vmware.com/products/workstation-pro.html](https://www.vmware.com/products/workstation-pro.html)
* [https://www.hex-rays.com/products/ida/](https://www.hex-rays.com/products/ida/)
* [https://www.hex-rays.com/products/decompiler/](https://www.hex-rays.com/products/decompiler/)
* [https://www.ghidra-sre.org/](https://www.ghidra-sre.org/)
* [https://github.com/joxeankoret/diaphora](https://github.com/joxeankoret/diaphora)
* [https://github.com/IDArlingTeam/IDArling](https://github.com/IDArlingTeam/IDArling)
* [https://www.hex-rays.com/ida-teams/](https://www.hex-rays.com/ida-teams/)
* [https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/debugger-download-tools](https://docs.microsoft.com/en-us/windows-hardware/drivers/debugger/debugger-download-tools)
* [http://sysprogs.com/legacy/virtualkd/](http://sysprogs.com/legacy/virtualkd/)
* [https://x64dbg.com/](https://x64dbg.com/)
* [https://github.com/bootleg/ret-sync](https://github.com/bootleg/ret-sync)
* [https://github.com/igogo-x86/HexRaysPyTools](https://github.com/igogo-x86/HexRaysPyTools)

VM credentials: `IEUser`/`Passw0rd!`

Fixing file permissions:

* [https://www.drivereasy.com/knowledge/you-require-permission-from-trustedinstaller-error-fixing-guide/](https://www.drivereasy.com/knowledge/you-require-permission-from-trustedinstaller-error-fixing-guide/)

Checking if `tm.sys` symbols are working:

```
u tm!TmRecoverResourceManagerExt
```