<!-- 04-10-improving_reversed_code -->

- [Shifted pointers in IDA](https://www.hex-rays.com/products/ida/support/idadoc/1695.shtml)
- [Shifted pointers in Ghidra](https://github.com/NationalSecurityAgency/ghidra/issues/573)
