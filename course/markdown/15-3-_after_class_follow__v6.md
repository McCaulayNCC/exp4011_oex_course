<!-- 15-3-_after_class_follow -->

# Follow us to hear about new classes in the future
[![OST2](https://img.shields.io/badge/Mastodon_Cedric_Halbronn-follow-blue)](https://infosec.exchange/@saidelike)  
[![OST2](https://img.shields.io/badge/Twitter_Cedric_Halbronn-follow-blue)](https://Twitter.com/saidelike)  
[![OST2](https://img.shields.io/badge/Mastodon_OpenSecTraining-follow-blue)](https://infosec.exchange/@OpenSecurityTraining2)  
[![OST2](https://img.shields.io/badge/Twitter_OpenSecTraining-follow-blue)](https://Twitter.com/OpenSecTraining)  
[![OST2](https://img.shields.io/badge/LinkedIn_OST2-follow-blue)](https://www.linkedin.com/company/ost2)  
[![OST2](https://img.shields.io/badge/Reddit_/r/OST2-subscribe-red)](https://reddit.com/r/OST2)  
[![OST2](https://img.shields.io/badge/Slack_OST2_Special_Interest_Groups-join-red)](https://join.slack.com/t/ost2-sigs/shared_invite/zt-1q0u8uwgk-xl8K02g0g7GLZKXHnilCoQ)  


# Join the Alumni Slack Channel

If your Progress page shows a grade of >= 97% then you're now eligible to join the "Exp4011 Alumni" Slack channel! The channel is meant to be a low-noise channel for students to be able to keep in touch with the instructor, seek/advertise jobs that use the skills you learned in the class, make suggestions for future class content, etc. To join, email exp4011@ost2.fyi with a subject line of "Exp4011 alumni join" and you will receive an invite.