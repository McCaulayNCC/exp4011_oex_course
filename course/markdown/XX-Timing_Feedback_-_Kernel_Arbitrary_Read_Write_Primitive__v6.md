<!-- XX-Timing_Feedback_-_Kernel_Arbitrary_Read_Write_Primitive -->

## NOTE: Your submission of this timing feedback will not show up in your overall course Progress view at the top of the page. This is because of some limitations of Open edX's grading system, but it is OK and expected.