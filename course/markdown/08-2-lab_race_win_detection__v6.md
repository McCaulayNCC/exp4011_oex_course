<!-- 08-2-lab_race_win_detection -->

Debugging:

```
// vulnerable function entry point
ba e 1 tm!TmRecoverResourceManagerExt

// skip a breakpoint many times
g;g;g;g;g;g;g;g;g;g;g;

// block recovery thread before sending notification
ba e 1 tm!TmRecoverResourceManagerExt+0x23a
!patch

// unblock recovery thread and hit a breakpoint
!unpatch
ba e 1 tm!TmRecoverResourceManagerExt+0x28d

// post race win - superior enlistment to aid debugging
ba e 1 tm!TmRecoverResourceManagerExt+0x18c

// beginning of main loop - debug every iteration
!process 0 0 racewindetection_lab.exe
.process <KPROCESS ADDRESS>
tm!TmRecoverResourceManagerExt+0x102
```
