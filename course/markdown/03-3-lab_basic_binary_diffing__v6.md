<!-- 03-3-lab_basic_binary_diffing -->

* [https://www.vergiliusproject.com/kernels/x64/Windows%2010%20|%202016/1809%20Redstone%205%20(October%20Update%29/_KRESOURCEMANAGER](https://www.vergiliusproject.com/kernels/x64/Windows%2010%20|%202016/1809%20Redstone%205%20(October%20Update%29/_KRESOURCEMANAGER)
* [https://learn.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-tmrecoverresourcemanager](https://learn.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-tmrecoverresourcemanager)
* [https://learn.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-kewaitforsingleobject](https://learn.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-kewaitforsingleobject)
* [https://learn.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-kereleasemutex](https://learn.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-kereleasemutex)
* [https://learn.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-obfreferenceobject](https://learn.microsoft.com/en-us/windows-hardware/drivers/ddi/wdm/nf-wdm-obfreferenceobject)
