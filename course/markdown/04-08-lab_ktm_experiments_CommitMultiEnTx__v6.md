<!-- 04-08-lab_ktm_experiments_CommitMultiEnTx -->

- [https://learn.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-preparecomplete](https://learn.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-preparecomplete)
- [https://learn.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-commitcomplete](https://learn.microsoft.com/en-us/windows/win32/api/ktmw32/nf-ktmw32-commitcomplete)
- [https://www.vergiliusproject.com/kernels/x64/Windows%207%20%7C%202008R2/SP1/_KTRANSACTION](https://www.vergiliusproject.com/kernels/x64/Windows%207%20%7C%202008R2/SP1/_KTRANSACTION)
- [https://www.vergiliusproject.com/kernels/x64/Windows%207%20%7C%202008R2/SP1/_KENLISTMENT](https://www.vergiliusproject.com/kernels/x64/Windows%207%20%7C%202008R2/SP1/_KENLISTMENT)
