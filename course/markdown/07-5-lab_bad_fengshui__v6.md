<!-- 07-5-lab_bad_fengshui -->

Disable Verifier:

```
verifier.exe /driver tm.sys /flags 0x0
```

Improve !poolfind speed:

```
.cache 0x1fffff
```

Track Enlistment allocation:

```
sxd sse
ba e 1 tm!TmCreateEnlistmentExt+0xf3 ".printf\"Allocated Enlistment: %p\", @rcx; .echo; g;"
!pool <pool address>
dt nt!_KENLISTMENT <pool address>
```

or

```
!poolfind TmEn
```

Track notifications:

```
!poolfind TmFN
```

Print structure field `Transaction` inside `KENLISTMENT` structure:

```
dt nt!_KENLISTMENT Transaction <kenlistment pool address>
```
