<!-- 12-1-priv_esc_strategies -->

* [https://web.archive.org/web/20221206142105/http://www.nosuchcon.org/talks/2013/D3_02_Nikita_Exploiting_Hardcore_Pool_Corruptions_in_Microsoft_Windows_Kernel.pdf](https://web.archive.org/web/20221206142105/http://www.nosuchcon.org/talks/2013/D3_02_Nikita_Exploiting_Hardcore_Pool_Corruptions_in_Microsoft_Windows_Kernel.pdf)
* [https://medium.com/@ashabdalhalim/a-light-on-windows-10s-object-header-typeindex-value-e8f907e7073a](https://medium.com/@ashabdalhalim/a-light-on-windows-10s-object-header-typeindex-value-e8f907e7073a)
* [https://j00ru.vexillium.org/2011/06/smep-what-is-it-and-how-to-beat-it-on-windows/](https://j00ru.vexillium.org/2011/06/smep-what-is-it-and-how-to-beat-it-on-windows/)
* [http://powerofcommunity.net/poc2018/nikita.pdf](http://powerofcommunity.net/poc2018/nikita.pdf)
* [https://www.vergiliusproject.com/kernels/x64/Windows%2010%20%7C%202016/1809%20Redstone%205%20(October%20Update%29/_KWAIT_BLOCK](https://www.vergiliusproject.com/kernels/x64/Windows%2010%20%7C%202016/1809%20Redstone%205%20(October%20Update%29/_KWAIT_BLOCK)
* [https://www.slideshare.net/scovetta/kernelpool](https://www.slideshare.net/scovetta/kernelpool)
