<!-- 01-intro -->

Download all the slides and materials from [https://gitlab.com/opensecuritytraining/exp4011_windows_kernel_exploitation/-/tree/main/](https://gitlab.com/opensecuritytraining/exp4011_windows_kernel_exploitation/-/tree/main/)

* `slides/`
    * `XX-whatever.pdf`: slides deck for a given video
    * `exp4011-windows-kernel-race-uaf-ktm.pdf`: full slides deck (merging all `XX-whatever.pdf`)
* `tools/`
    * `ghidra_project/` and `IEUser/`: ret-sync configuration files
    * `ssh-config/`: private/public SSH keys to use between the Debugger VM and Target VM
    * `cve-2018-8611.gar`: Ghidra project archive
    * `labs/*.zip`: Labs/exercises to solve
    * `*.bat`, `*.cmd` and `*.js`: WinDbg starting and helper scripts
    * `cheatsheet_*.md`: List of commands for Ghidra and WinDbg
    * `reverse/`
        * `*.dll`, `*.sys` and `*.exe`: Windows binaries to configure the Target VM
        * `*.h`: C header files containing structures/enums to import in Ghidra/IDA
    * `patch_diffing/*.c`: vulnerable function's decompiled code before and after the patch
    * `references/`: Windows exploitation papers and Intel Instruction Set reference

