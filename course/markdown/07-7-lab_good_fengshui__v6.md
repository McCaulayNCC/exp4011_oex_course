<!-- 07-7-lab_good_fengshui -->

Improve !poolfind speed:

```
.cache 0x1fffff
```

Track Named Pipe allocation:

```
.reload /f npfs.sys
!process 0 0 namedpipespray_lab.exe
ba e 1 /p <KPROCESS ADDRESS> npfs!NpAddDataQueueEntry+0x1da ".printf\"NpFr: %p\", @rax; .echo; g"
!pool <pool address>
```

or

```
!poolfind NpFr
```

Track Enlistment allocation:

```
sxd sse
ba e 1 tm!TmCreateEnlistmentExt+0xf3 ".printf\"Allocated Enlistment: %p\", @rcx; .echo; g;"
!pool <pool address>
dt nt!_KENLISTMENT <pool address>
```

or

```
!poolfind TmEn
```

Track notifications:

```
ba e 1 tm!TmpInitializeEnlistment+0xd1 ".printf\"Allocated Notification: %p\", @rax; .echo; g;"
!pool <pool address>
```

or

```
!poolfind TmFN
```
