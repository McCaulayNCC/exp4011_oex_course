<!-- 04-01-ktm_basics -->

KTM MSDN documentation:

- [KTM Portal](https://docs.microsoft.com/en-us/windows/win32/ktm/kernel-transaction-manager-portal)
- [Introduction to KTM](https://docs.microsoft.com/en-us/windows-hardware/drivers/kernel/introduction-to-ktm)

KTM internals videos:

- [Transactional Vista: Kernel Transaction Manager and friends (TxF, TxR)](https://web.archive.org/web/20201205231231/https://channel9.msdn.com/Shows/Going+Deep/Transactional-Vista-Kernel-Transaction-Manager-and-friends-TxF-TxR)
- [ARCast - Transactional File System and Registry](https://web.archive.org/web/20211107230058/https://channel9.msdn.com/Shows/ARCast+with+Ron+Jacobs/ARCast-Transactional-File-System-and-Registry)
- [Surendra Verma: Vista Transactional File System](https://web.archive.org/web/20201128184620/https://channel9.msdn.com/Shows/Going+Deep/Surendra-Verma-Vista-Transactional-File-System)

KTM research:

* [CVE-2010-1889](https://packetstormsecurity.com/files/92845/Microsoft-Windows-KTM-Invalid-Free-With-Reused-Transaction-GUID.html)
* [MS15-038](https://bugs.chromium.org/p/project-zero/issues/detail?id=245&can=1&q=transaction)
* [CVE-2017-8481](https://bugs.chromium.org/p/project-zero/issues/detail?id=1207)
* [CVE-2018-8611](https://securelist.com/zero-day-in-windows-kernel-transaction-manager-cve-2018-8611/89253/)
* [Proton Bot malware uses KTM](https://fumik0.com/2019/05/24/overview-of-proton-bot-another-loader-in-the-wild/)
