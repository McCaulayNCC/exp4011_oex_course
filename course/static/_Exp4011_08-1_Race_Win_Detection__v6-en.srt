1
00:00:04,580 --> 00:00:10,019
so we know now that we can have some

2
00:00:07,200 --> 00:00:12,300
pointer in the kernel pointing into

3
00:00:10,019 --> 00:00:14,639
userland but right now we're using the

4
00:00:12,300 --> 00:00:16,920
debugger to win the race and that's how

5
00:00:14,639 --> 00:00:19,140
we know when it's happened because we

6
00:00:16,920 --> 00:00:21,359
are analyzing the registers states or

7
00:00:19,140 --> 00:00:23,460
whatever but the goal is to start to

8
00:00:21,359 --> 00:00:25,800
eliminate the need for the debugger to

9
00:00:23,460 --> 00:00:28,500
help us winning the race so one of the

10
00:00:25,800 --> 00:00:30,960
main things is that how do we detect

11
00:00:28,500 --> 00:00:33,360
whether or not we actually won the race

12
00:00:30,960 --> 00:00:35,820
but from userland so that we can

13
00:00:33,360 --> 00:00:38,399
potentially exploit it in some sort of

14
00:00:35,820 --> 00:00:40,739
way that is useful how do we avoid the

15
00:00:38,399 --> 00:00:43,379
code crashing so obviously we don't want

16
00:00:40,739 --> 00:00:46,559
to use a bunch of A's when we replace

17
00:00:43,379 --> 00:00:48,480
the KENLISTMENT chunk and while we are

18
00:00:46,559 --> 00:00:51,300
still using the debugger it would be

19
00:00:48,480 --> 00:00:53,820
nice if we could avoid breakpoints that

20
00:00:51,300 --> 00:00:56,699
slow down debugging and so it would be

21
00:00:53,820 --> 00:00:59,460
nice if we could figure out a way to

22
00:00:56,699 --> 00:01:01,379
just kick in the debugger as soon as we

23
00:00:59,460 --> 00:01:04,199
have successfully won the race condition

24
00:01:01,379 --> 00:01:07,380
so that we don't have to use the

25
00:01:04,199 --> 00:01:10,619
!patch command or do stepping or whatever

26
00:01:07,380 --> 00:01:13,200
and only debug the states after the race

27
00:01:10,619 --> 00:01:15,780
is won that would be really useful so

28
00:01:13,200 --> 00:01:18,540
this are the type of things that as we get

29
00:01:15,780 --> 00:01:21,479
ideas about the code and how we want to

30
00:01:18,540 --> 00:01:23,640
win the race and post race win stuff

31
00:01:21,479 --> 00:01:26,520
we think about these and make little

32
00:01:23,640 --> 00:01:28,080
recipe list of what we want to solve in

33
00:01:26,520 --> 00:01:30,060
this video we're going to look at things

34
00:01:28,080 --> 00:01:32,400
we can do after we win the race

35
00:01:30,060 --> 00:01:35,040
condition we are going to see how we can

36
00:01:32,400 --> 00:01:38,640
detect that we win the race

37
00:01:35,040 --> 00:01:40,320
but from userland since the kernel is

38
00:01:38,640 --> 00:01:42,840
going to touch our userland

39
00:01:40,320 --> 00:01:46,259
enlistments we are going to see what we

40
00:01:42,840 --> 00:01:49,380
need to set in our fake enlistments in

41
00:01:46,259 --> 00:01:52,439
order to avoid a crash in the kernel

42
00:01:49,380 --> 00:01:56,340
and we are going to see how we can make

43
00:01:52,439 --> 00:01:58,619
sure that the debugger kicks in right

44
00:01:56,340 --> 00:01:59,939
after we win the race okay let's get

45
00:01:58,619 --> 00:02:03,240
started

46
00:01:59,939 --> 00:02:06,000
so we know there is no SMAP on Windows

47
00:02:03,240 --> 00:02:09,000
10 1809 at the time of recording this

48
00:02:06,000 --> 00:02:11,459
there is no SMAP at all on any Windows

49
00:02:09,000 --> 00:02:13,739
version so we know that we can give a

50
00:02:11,459 --> 00:02:16,080
userland pointer to the kernel and that

51
00:02:13,739 --> 00:02:18,300
the kernel will happily use the userland

52
00:02:16,080 --> 00:02:20,760
pointer without causing any issue and so

53
00:02:18,300 --> 00:02:22,920
basically what we can do is we can just

54
00:02:20,760 --> 00:02:25,500
create a fake KENLISTMENT in

55
00:02:22,920 --> 00:02:29,040
userland and provide it to the kernel so

56
00:02:25,500 --> 00:02:31,500
the strategy is we replace the freed

57
00:02:29,040 --> 00:02:34,200
KENLISTMENT in the kernel with named pipe

58
00:02:31,500 --> 00:02:37,920
data and instead of providing a bunch of

59
00:02:34,200 --> 00:02:40,620
A's we just provide a NextSameRm flink

60
00:02:37,920 --> 00:02:43,080
pointer to a userland address that is

61
00:02:40,620 --> 00:02:45,840
holding KENLISTMENT structure that we

62
00:02:43,080 --> 00:02:48,239
can populate with any contents since it

63
00:02:45,840 --> 00:02:49,860
is in  userland and so the main loop

64
00:02:48,239 --> 00:02:52,379
of the vulnerable function has

65
00:02:49,860 --> 00:02:54,900
functionality that can leak us

66
00:02:52,379 --> 00:02:56,879
information about whether or not we want

67
00:02:54,900 --> 00:02:59,400
the race and it's related to the fact

68
00:02:56,879 --> 00:03:01,739
that the loop tries to be efficient in

69
00:02:59,400 --> 00:03:04,140
the case where it has to parse from the

70
00:03:01,739 --> 00:03:06,780
enlistment head a second time and the

71
00:03:04,140 --> 00:03:09,840
efficiency is that when it detects a

72
00:03:06,780 --> 00:03:12,480
notifiable KENLISTMENT it will unset

73
00:03:09,840 --> 00:03:15,120
the notifiable flag so that it never

74
00:03:12,480 --> 00:03:17,700
tries to notify that again so if we

75
00:03:15,120 --> 00:03:19,739
provide a pointer to a userland

76
00:03:17,700 --> 00:03:22,620
KENLISTMENT we control everything in the

77
00:03:19,739 --> 00:03:25,140
KENLISTMENTs so initially we can set the

78
00:03:22,620 --> 00:03:27,060
KENLISTMENTs notifiable flag and when the

79
00:03:25,140 --> 00:03:28,800
kernel starts to process our fake

80
00:03:27,060 --> 00:03:31,620
KENLISTMENTs after we win the race

81
00:03:28,800 --> 00:03:33,540
condition it will unset that flag and we

82
00:03:31,620 --> 00:03:36,180
will never unset that flag ourselves

83
00:03:33,540 --> 00:03:38,400
from userland and no other process is

84
00:03:36,180 --> 00:03:40,920
accessing our memory so we can basically

85
00:03:38,400 --> 00:03:42,599
just try to trigger the bug and then

86
00:03:40,920 --> 00:03:45,540
test this notifiable flag from

87
00:03:42,599 --> 00:03:47,400
userland and if that flag is unset it

88
00:03:45,540 --> 00:03:49,260
means that we managed to win the race

89
00:03:47,400 --> 00:03:51,659
condition and the kernel is now

90
00:03:49,260 --> 00:03:53,760
interacting with our userland memory and

91
00:03:51,659 --> 00:03:56,700
so I have a small animation to explain

92
00:03:53,760 --> 00:03:58,860
this and so this is the main loop of the

93
00:03:56,700 --> 00:04:01,319
vulnerable function and so we assume the

94
00:03:58,860 --> 00:04:02,640
recovery thread is blocked here and we

95
00:04:01,319 --> 00:04:04,739
won the race condition from another

96
00:04:02,640 --> 00:04:06,959
thread and so because we won the race

97
00:04:04,739 --> 00:04:09,599
condition we are able to control the

98
00:04:06,959 --> 00:04:12,120
NextSameRm flink pointer and so it

99
00:04:09,599 --> 00:04:14,459
fetches our fake KENLISTMENTs address from

100
00:04:12,120 --> 00:04:16,500
userland and save it into pEnlistment

101
00:04:14,459 --> 00:04:20,340
shifted and then it restarts

102
00:04:16,500 --> 00:04:22,680
looping and so obviously because

103
00:04:20,340 --> 00:04:24,300
pEnlistment shifted points to userland it

104
00:04:22,680 --> 00:04:27,060
means that it doesn't point to the

105
00:04:24,300 --> 00:04:29,400
KENLISTMENT head so it won't exit the

106
00:04:27,060 --> 00:04:31,259
loop and it will do another iteration a

107
00:04:29,400 --> 00:04:33,960
funny thing is we could actually make it

108
00:04:31,259 --> 00:04:36,600
spin forever here but it's not that

109
00:04:33,960 --> 00:04:38,759
interesting because it won't give us any

110
00:04:36,600 --> 00:04:42,120
control and so we can make sure the

111
00:04:38,759 --> 00:04:44,880
KENLISTMENT finalized
flag is not set so it

112
00:04:42,120 --> 00:04:47,040
enters the else condition instead and we

113
00:04:44,880 --> 00:04:49,320
can see that if we actually set the

114
00:04:47,040 --> 00:04:52,020
notifiable flag it will go inside

115
00:04:49,320 --> 00:04:54,600
another if condition and at the end of

116
00:04:52,020 --> 00:04:57,419
this if condition it will actually unset

117
00:04:54,600 --> 00:04:59,460
this flag and so because it is our fake

118
00:04:57,419 --> 00:05:01,979
KENLISTMENT from userland we are able to

119
00:04:59,460 --> 00:05:04,259
detect it since we can check the memory

120
00:05:01,979 --> 00:05:05,880
from userlandand so basically this is

121
00:05:04,259 --> 00:05:07,860
what it looks like if you're trying to

122
00:05:05,880 --> 00:05:10,259
visualize the memory you've got the

123
00:05:07,860 --> 00:05:12,720
chain of enlistment is pointing to some

124
00:05:10,259 --> 00:05:15,000
chunk that is use-after-freed and this

125
00:05:12,720 --> 00:05:17,040
use-after-free chunk points to a fake

126
00:05:15,000 --> 00:05:19,979
userland enlistment with the

127
00:05:17,040 --> 00:05:22,199
notifiable flag set and this flag gets

128
00:05:19,979 --> 00:05:24,600
unset by the kernel after we win the

129
00:05:22,199 --> 00:05:27,300
race condition and so this is all well

130
00:05:24,600 --> 00:05:29,340
and good but we're still stuck in this

131
00:05:27,300 --> 00:05:31,680
loop that's going to process our fake

132
00:05:29,340 --> 00:05:34,139
userland KENLISTMENTs and is going to

133
00:05:31,680 --> 00:05:36,539
try to do stuff on it so when it's going

134
00:05:34,139 --> 00:05:38,639
to try to fetch the flink pointer from

135
00:05:36,539 --> 00:05:40,380
that userland KENLISTMENTs and keep

136
00:05:38,639 --> 00:05:43,440
going through the loop because if we

137
00:05:40,380 --> 00:05:45,780
remember it's bounded by pointing at the

138
00:05:43,440 --> 00:05:47,820
enlistments head which is never going to do

139
00:05:45,780 --> 00:05:51,120
now because the loop is going to be

140
00:05:47,820 --> 00:05:53,699
stuck inside of userland due to us

141
00:05:51,120 --> 00:05:56,580
providing userland KENLISTMENTs so we need

142
00:05:53,699 --> 00:05:59,039
a way to basically control the loop a

143
00:05:56,580 --> 00:06:01,440
little bit more and detect that we won

144
00:05:59,039 --> 00:06:04,740
the race as start doing stuff and so

145
00:06:01,440 --> 00:06:06,960
basically the trick is just introduce a

146
00:06:04,740 --> 00:06:09,360
flink pointer inside of the userland

147
00:06:06,960 --> 00:06:11,280
enlistment to point to itself and that

148
00:06:09,360 --> 00:06:13,620
will basically just cause the thread in

149
00:06:11,280 --> 00:06:16,680
the kernel to infinite loop but it will

150
00:06:13,620 --> 00:06:19,560
infinite loop in a way that we now

151
00:06:16,680 --> 00:06:21,720
control everything and we can break it

152
00:06:19,560 --> 00:06:24,120
out of the infinite loop later from

153
00:06:21,720 --> 00:06:26,220
userland by just changing the NextSameRm

154
00:06:24,120 --> 00:06:27,840
flink pointer to point somewhere else

155
00:06:26,220 --> 00:06:29,819
in userland again and so this

156
00:06:27,840 --> 00:06:32,819
technique we came up with we've just

157
00:06:29,819 --> 00:06:35,280
referred to as a trap enlistment because

158
00:06:32,819 --> 00:06:37,979
we are basically trapping the kernel in

159
00:06:35,280 --> 00:06:40,560
userland and so yeah I mentioned that

160
00:06:37,979 --> 00:06:42,660
once this state happens not only can we

161
00:06:40,560 --> 00:06:45,180
detect it from userland but we also

162
00:06:42,660 --> 00:06:47,819
want to be able to ideally trigger the

163
00:06:45,180 --> 00:06:50,100
debugger to kick in as soon as the race

164
00:06:47,819 --> 00:06:52,500
is won so we can analyze how it's

165
00:06:50,100 --> 00:06:54,780
processing the userland
KENLISTMENTs and we

166
00:06:52,500 --> 00:06:56,819
don't want to just have a breakpoint at

167
00:06:54,780 --> 00:06:59,100
a commonly executive part of the main

168
00:06:56,819 --> 00:07:02,819
loop because as I explained we are

169
00:06:59,100 --> 00:07:04,680
introducing like hex 5000 enlistments

170
00:07:02,819 --> 00:07:06,660
that the kernel is going to be parsing

171
00:07:04,680 --> 00:07:08,639
while this is happening and so you don't

172
00:07:06,660 --> 00:07:11,039
want to have a breakpoint triggering for

173
00:07:08,639 --> 00:07:13,560
every iteration and so we came up with a

174
00:07:11,039 --> 00:07:15,419
little trick which is that we don't use

175
00:07:13,560 --> 00:07:17,759
superior enlistments in a normal

176
00:07:15,419 --> 00:07:20,400
scenario to trigger the bug and so there

177
00:07:17,759 --> 00:07:22,979
is this code in the loop that only gets

178
00:07:20,400 --> 00:07:25,440
called if an enlistments happened to be

179
00:07:22,979 --> 00:07:28,020
superior as you can see due to the

180
00:07:25,440 --> 00:07:29,699
is_superior variable being set it will go

181
00:07:28,020 --> 00:07:31,919
into that specific

182
00:07:29,699 --> 00:07:33,900
if condition and so when you're normally

183
00:07:31,919 --> 00:07:36,180
looping through and winning the race

184
00:07:33,900 --> 00:07:38,520
you're never going to hit this code but

185
00:07:36,180 --> 00:07:41,099
because we control all of the fields in

186
00:07:38,520 --> 00:07:43,319
our fake userland enlistment we can also

187
00:07:41,099 --> 00:07:45,479
just set the flags to say that it's

188
00:07:43,319 --> 00:07:47,639
superior and then it will enter this

189
00:07:45,479 --> 00:07:49,919
code that is highlighted in yellow and

190
00:07:47,639 --> 00:07:52,380
so before we trigger the bug at all we

191
00:07:49,919 --> 00:07:54,720
can go in WinDbg and set a breakpoint

192
00:07:52,380 --> 00:07:57,240
on this code and then as soon as the

193
00:07:54,720 --> 00:07:59,759
race condition hits it processes the

194
00:07:57,240 --> 00:08:01,740
userland enlistment and tests the

195
00:07:59,759 --> 00:08:04,259
superior flag as though the debugger

196
00:08:01,740 --> 00:08:06,479
kicks in and then from that point we can

197
00:08:04,259 --> 00:08:08,699
start debugging more interesting details

198
00:08:06,479 --> 00:08:11,220
of what it's doing with the userland

199
00:08:08,699 --> 00:08:13,139
enlistment so yeah basically the trick

200
00:08:11,220 --> 00:08:15,780
is to set the KENLISTMENT superior flag

201
00:08:13,139 --> 00:08:17,639
and then also part of that as you can

202
00:08:15,780 --> 00:08:20,639
see a little bit above the yellow part

203
00:08:17,639 --> 00:08:22,800
it is basically pulling out a pointer to

204
00:08:20,639 --> 00:08:25,440
a KTRANSACTION and that KTRANSACTION

205
00:08:22,800 --> 00:08:28,080
is from userland and is testing the

206
00:08:25,440 --> 00:08:29,639
state field of that KTRANSACTION so it

207
00:08:28,080 --> 00:08:32,820
means that we have to create a

208
00:08:29,639 --> 00:08:35,399
transaction structure in userland set a

209
00:08:32,820 --> 00:08:38,159
state variable for it and then point our

210
00:08:35,399 --> 00:08:40,500
fake KENLISTMENT transaction pointer to

211
00:08:38,159 --> 00:08:42,659
that fake KTRANSACTION and that's

212
00:08:40,500 --> 00:08:44,940
basically what it looks like so this

213
00:08:42,659 --> 00:08:47,880
enlistment that allows us to kick in and

214
00:08:44,940 --> 00:08:51,540
debugger we call it a debug enlistment

215
00:08:47,880 --> 00:08:53,700
technically the debug enlistment can also be

216
00:08:51,540 --> 00:08:56,220
a trap enlistment at the same time but

217
00:08:53,700 --> 00:08:58,620
you could point its NextSameRm flink

218
00:08:56,220 --> 00:09:01,200
pointer to a trap enlistment and just

219
00:08:58,620 --> 00:09:03,360
have the kernel parse the debug

220
00:09:01,200 --> 00:09:05,700
enlistment and then jump at the next

221
00:09:03,360 --> 00:09:08,640
phase of the loop to the trap enlistment

222
00:09:05,700 --> 00:09:11,040
this slide is just to give you ideas of

223
00:09:08,640 --> 00:09:13,260
stuff to think about and so something to

224
00:09:11,040 --> 00:09:15,540
just think about in general is that

225
00:09:13,260 --> 00:09:18,600
because the kernel is just looping over

226
00:09:15,540 --> 00:09:20,880
this NextSameRm flink pointer in the

227
00:09:18,600 --> 00:09:23,580
trap enlistment and it's gonna do that

228
00:09:20,880 --> 00:09:26,339
indefinitely it gives us the opportunity

229
00:09:23,580 --> 00:09:28,380
to do things and so basically anytime we

230
00:09:26,339 --> 00:09:30,660
want to force the kernel to do something

231
00:09:28,380 --> 00:09:33,180
useful for us in the loop we can just

232
00:09:30,660 --> 00:09:35,880
modify that flink pointer to something

233
00:09:33,180 --> 00:09:40,260
else and so this is demonstrated in this

234
00:09:35,880 --> 00:09:42,899
diagram at the 3 to N phases where there

235
00:09:40,260 --> 00:09:45,420
is a primitive the idea is you trap the

236
00:09:42,899 --> 00:09:48,000
kernel using this trap enlistment you

237
00:09:45,420 --> 00:09:51,000
detect you won the race and then you

238
00:09:48,000 --> 00:09:53,339
inject new KENLISTMENTs over time that

239
00:09:51,000 --> 00:09:55,080
force the loop to do certain things and

240
00:09:53,339 --> 00:09:57,060
every time you need to measure what

241
00:09:55,080 --> 00:09:59,220
happened or know whether or not the

242
00:09:57,060 --> 00:10:02,100
kernel has completed something that you

243
00:09:59,220 --> 00:10:04,260
want the kernel to be doing with this

244
00:10:02,100 --> 00:10:06,480
newly injected KENLISTMENT you just

245
00:10:04,260 --> 00:10:09,060
point this new set of inserted

246
00:10:06,480 --> 00:10:11,580
KENLISTMENT to a new trap enlistments

247
00:10:09,060 --> 00:10:13,980
and wait for that trap enlistment notifiable

248
00:10:11,580 --> 00:10:15,720
flag to be unset so you

249
00:10:13,980 --> 00:10:19,399
basically just have this little new

250
00:10:15,720 --> 00:10:19,399
state machine that you can play with

