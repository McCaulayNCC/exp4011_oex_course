1
00:00:01,500 --> 00:00:05,819
hi everyone so in this part we're gonna

2
00:00:03,899 --> 00:00:07,680
go over two different labs to experiment

3
00:00:05,819 --> 00:00:09,179
different heap feng shui techniques the

4
00:00:07,680 --> 00:00:12,360
first one will be about experimenting

5
00:00:09,179 --> 00:00:14,400
the feng shui using KENLISTMENTs and the

6
00:00:12,360 --> 00:00:16,740
second one will be about using named pipes

7
00:00:14,400 --> 00:00:19,440
okay let's get started so if you have

8
00:00:16,740 --> 00:00:21,660
enabled verifier previously you'll have

9
00:00:19,440 --> 00:00:24,060
to make sure to disable it from now on

10
00:00:21,660 --> 00:00:26,160
because verifier can change the memory

11
00:00:24,060 --> 00:00:28,680
layout and because we are going to focus

12
00:00:26,160 --> 00:00:31,380
on exploitation we want the memory to be

13
00:00:28,680 --> 00:00:34,559
unmodified compared to real world

14
00:00:31,380 --> 00:00:37,920
scenario and so in the target VM you can

15
00:00:34,559 --> 00:00:41,100
use the verifier command to disable it

16
00:00:37,920 --> 00:00:43,739
on the tm.sys driver and then you have

17
00:00:41,100 --> 00:00:46,500
to reboot the VM and alternatively you

18
00:00:43,739 --> 00:00:48,899
should be able to restore a snapshot if

19
00:00:46,500 --> 00:00:50,820
you created one earlier in the course as

20
00:00:48,899 --> 00:00:54,120
mentioned previously we initially

21
00:00:50,820 --> 00:00:56,399
investigated using KENLISTMENTs the feng

22
00:00:54,120 --> 00:00:59,100
shui and so we said it's not efficient

23
00:00:56,399 --> 00:01:01,440
in practice but on purpose we are going

24
00:00:59,100 --> 00:01:03,840
to write the code to do it and then we

25
00:01:01,440 --> 00:01:05,519
to see why it doesn't work we think it

26
00:01:03,840 --> 00:01:07,619
is useful to show the whole process

27
00:01:05,519 --> 00:01:09,420
because this is usually what you have to

28
00:01:07,619 --> 00:01:12,720
do you generally don't know in advance

29
00:01:09,420 --> 00:01:14,820
what is going to work versus not but the

30
00:01:12,720 --> 00:01:17,159
whole point about testing the easy stuff

31
00:01:14,820 --> 00:01:19,680
first is that you don't have to reverse

32
00:01:17,159 --> 00:01:22,619
engineer much and it is always good to

33
00:01:19,680 --> 00:01:25,500
confirm that other things are saying

34
00:01:22,619 --> 00:01:27,720
also we know we won't be able to use the

35
00:01:25,500 --> 00:01:29,820
KENLISTMENT for the replacement chunk

36
00:01:27,720 --> 00:01:32,640
because we wouldn't control the data

37
00:01:29,820 --> 00:01:35,700
especially the Flink pointer but it is

38
00:01:32,640 --> 00:01:38,579
good to test as a first attempt one

39
00:01:35,700 --> 00:01:40,320
thing to note is that in this lab we're

40
00:01:38,579 --> 00:01:42,479
not going to take into account the

41
00:01:40,320 --> 00:01:44,520
delayed free mitigation and the main

42
00:01:42,479 --> 00:01:47,700
reason for that is that we are going to

43
00:01:44,520 --> 00:01:49,979
be manipulating many KENLISTMENTs and so

44
00:01:47,700 --> 00:01:53,159
we are going to free many of them and so

45
00:01:49,979 --> 00:01:55,200
in practice the delayed free list is

46
00:01:53,159 --> 00:01:57,299
going to be flushed anyway and so we

47
00:01:55,200 --> 00:01:59,640
won't provide code to deal with the

48
00:01:57,299 --> 00:02:02,820
delayed free mechanism yet and we will

49
00:01:59,640 --> 00:02:06,360
deal with it later in the a course so

50
00:02:02,820 --> 00:02:10,739
the goal of this lab is to create holes

51
00:02:06,360 --> 00:02:13,560
of size KENLISTMENTs using KENLISTMENTs

52
00:02:10,739 --> 00:02:16,920
themselves and then we're going to fill

53
00:02:13,560 --> 00:02:19,200
these holes with new KENLISTMENTs that

54
00:02:16,920 --> 00:02:21,300
we would suppose would be our race

55
00:02:19,200 --> 00:02:23,580
condition enlistments and so once we've

56
00:02:21,300 --> 00:02:25,980
done that we will be able to inspect the

57
00:02:23,580 --> 00:02:28,860
memory to understand why it is not an

58
00:02:25,980 --> 00:02:31,140
ideal way for creating holes in order to

59
00:02:28,860 --> 00:02:34,080
locate the enlistments in memory we

60
00:02:31,140 --> 00:02:36,540
can use the !poolfind command which

61
00:02:34,080 --> 00:02:41,280
allows you to specify a tag to look for

62
00:02:36,540 --> 00:02:43,860
so if we use !poolfind TmEn it will

63
00:02:41,280 --> 00:02:46,440
find all the memory chunks that contain

64
00:02:43,860 --> 00:02:48,959
KENLISTMENTs structures however this

65
00:02:46,440 --> 00:02:51,599
command is quite slow so another way is

66
00:02:48,959 --> 00:02:54,420
to use breakpoints to locate when

67
00:02:51,599 --> 00:02:56,760
certain KENLISTMENTs are allocated in

68
00:02:54,420 --> 00:03:00,360
order to locate certain areas in memory

69
00:02:56,760 --> 00:03:03,060
and then just browse adjacent areas to

70
00:03:00,360 --> 00:03:06,900
find more of them so this case we can

71
00:03:03,060 --> 00:03:09,540
use the !pool command and specify an

72
00:03:06,900 --> 00:03:12,180
address to look for and so at the bottom

73
00:03:09,540 --> 00:03:14,819
we provide the breakpoints you can use

74
00:03:12,180 --> 00:03:18,260
to log all the KENLISTMENTs allocations

75
00:03:14,819 --> 00:03:22,560
and we generally advise you to use the

76
00:03:18,260 --> 00:03:26,780
sxd sse command beforehand and so this

77
00:03:22,560 --> 00:03:29,580
sxd sse command allows you to disable

78
00:03:26,780 --> 00:03:31,920
single step exceptions because for some

79
00:03:29,580 --> 00:03:34,440
reason when you enable a breakpoint like

80
00:03:31,920 --> 00:03:36,780
this and print out the data it will

81
00:03:34,440 --> 00:03:39,659
print a warning about the single step

82
00:03:36,780 --> 00:03:41,940
exception which then interweaves with

83
00:03:39,659 --> 00:03:44,400
the actual data you want to be printed

84
00:03:41,940 --> 00:03:46,620
out and then it's really hard to read so

85
00:03:44,400 --> 00:03:48,840
you can basically tell WinDbg to disable

86
00:03:46,620 --> 00:03:51,180
handling this type of exception and then

87
00:03:48,840 --> 00:03:53,340
it won't print it this is the kind of

88
00:03:51,180 --> 00:03:55,500
output we can get from WinDbg and you

89
00:03:53,340 --> 00:03:57,959
can see that initially the layout looks

90
00:03:55,500 --> 00:04:00,900
good all of our KENLISTMENTs are

91
00:03:57,959 --> 00:04:03,200
adjacent as you can see the TmEn tag

92
00:04:00,900 --> 00:04:06,420
and there all of size

93
00:04:03,200 --> 00:04:08,760
240 hex bytes and then we create the

94
00:04:06,420 --> 00:04:11,099
holes and we analyze the memory and you

95
00:04:08,760 --> 00:04:13,379
can also try to reallocate the holes but

96
00:04:11,099 --> 00:04:15,120
without reading notification from the

97
00:04:13,379 --> 00:04:18,060
resource manager and it should be fine

98
00:04:15,120 --> 00:04:20,880
as you can see allocated free allocated

99
00:04:18,060 --> 00:04:24,720
free allocated free and all the

100
00:04:20,880 --> 00:04:27,300
allocated one are TmEn tag so denoting

101
00:04:24,720 --> 00:04:29,639
a KENLISTMENTs and finally we can try

102
00:04:27,300 --> 00:04:31,979
the same but this time we also read

103
00:04:29,639 --> 00:04:33,780
notifications from the resource manager

104
00:04:31,979 --> 00:04:35,940
and we should see that the layout is

105
00:04:33,780 --> 00:04:39,300
quite random here we can see that we

106
00:04:35,940 --> 00:04:42,000
have a TM enlistment a TmEn again

107
00:04:39,300 --> 00:04:45,900
and in between we have the actual

108
00:04:42,000 --> 00:04:47,460
notification chunks the TmFN and so yeah

109
00:04:45,900 --> 00:04:49,979
it's not what we want and the other

110
00:04:47,460 --> 00:04:54,720
thing we can see is that not only we

111
00:04:49,979 --> 00:04:58,139
have 240 hex byte chunks for the normal

112
00:04:54,720 --> 00:05:01,259
KENLISTMENTs but we also have 290 hex

113
00:04:58,139 --> 00:05:03,680
byte chunks as you can see here and here

114
00:05:01,259 --> 00:05:08,100
and actually if you think about it

115
00:05:03,680 --> 00:05:11,220
290 is actually 240 plus 50 so it's

116
00:05:08,100 --> 00:05:13,380
basically the coalition between an

117
00:05:11,220 --> 00:05:16,020
enlistments chunk and a notification

118
00:05:13,380 --> 00:05:17,160
chunk so yeah the layout is not what we

119
00:05:16,020 --> 00:05:19,199
want

120
00:05:17,160 --> 00:05:21,240
so the first thing we want to check is

121
00:05:19,199 --> 00:05:24,960
that our target VM doesn't have verifier

122
00:05:21,240 --> 00:05:28,080
enabled so with CTRL+M we can

123
00:05:24,960 --> 00:05:29,940
look at the snapshots so we can see I

124
00:05:28,080 --> 00:05:32,520
had enabled verifier in the latest one

125
00:05:29,940 --> 00:05:34,620
this one doesn't have it I just want to

126
00:05:32,520 --> 00:05:38,660
show you how that you can also check

127
00:05:34,620 --> 00:05:38,660
that from the command line as well

128
00:05:39,780 --> 00:05:45,300
so we can see

129
00:05:42,060 --> 00:05:46,259
that have you executed verifier query

130
00:05:45,300 --> 00:05:48,900
settings

131
00:05:46,259 --> 00:05:53,160
and that nothing is ticked

132
00:05:48,900 --> 00:05:55,080
and that the verifier drivers is none so

133
00:05:53,160 --> 00:05:57,660
there is nothing now let's have a look

134
00:05:55,080 --> 00:06:00,419
at the actual code for the lab we're

135
00:05:57,660 --> 00:06:03,120
interested in the enlistment spray in

136
00:06:00,419 --> 00:06:05,580
the feng shui section

137
00:06:03,120 --> 00:06:08,639
so we'll look at the EnlistmentSpray.c

138
00:06:05,580 --> 00:06:11,000
so we can see a lot of things that we've

139
00:06:08,639 --> 00:06:15,120
seen already

140
00:06:11,000 --> 00:06:18,180
there is a wrapper around create thread

141
00:06:15,120 --> 00:06:19,800
that takes the function to basically

142
00:06:18,180 --> 00:06:21,840
create the new thread as well as the

143
00:06:19,800 --> 00:06:23,580
argument to pass that function and again

144
00:06:21,840 --> 00:06:27,300
it's called X

145
00:06:23,580 --> 00:06:28,800
create thread the X prefix then we have

146
00:06:27,300 --> 00:06:30,979
the helper function that we've seen

147
00:06:28,800 --> 00:06:30,979
already

148
00:06:31,919 --> 00:06:35,699
so we have the

149
00:06:33,960 --> 00:06:37,680
GetNotificationResourceManager function that is called

150
00:06:35,699 --> 00:06:39,960
to retrieve a notification

151
00:06:37,680 --> 00:06:42,500
from the resource manager and the one

152
00:06:39,960 --> 00:06:46,340
that loops all of them to read

153
00:06:42,500 --> 00:06:46,340
up to max attempts

154
00:06:47,639 --> 00:06:52,319
there's a new function that also calls

155
00:06:49,979 --> 00:06:55,620
GetNotificationResourceManager

156
00:06:52,319 --> 00:06:57,539
but this time it's actually to drain all

157
00:06:55,620 --> 00:06:58,380
the notifications without printing

158
00:06:57,539 --> 00:07:02,039
anything

159
00:06:58,380 --> 00:07:05,220
the idea is we're gonna drain either up

160
00:07:02,039 --> 00:07:07,800
to the count number or if we pass -1

161
00:07:05,220 --> 00:07:10,380
it's going to basically drain all the

162
00:07:07,800 --> 00:07:13,139
notifications and it's gonna stop when

163
00:07:10,380 --> 00:07:16,160
the timeout expires without any

164
00:07:13,139 --> 00:07:16,160
notification being available

165
00:07:19,380 --> 00:07:25,560
and then we have the main function so we

166
00:07:22,020 --> 00:07:27,900
can see handles for different objects we

167
00:07:25,560 --> 00:07:30,180
have three transactions the first one

168
00:07:27,900 --> 00:07:32,280
will be alternated with the second one

169
00:07:30,180 --> 00:07:34,139
and once we've created the holes we'll

170
00:07:32,280 --> 00:07:36,479
have a third transaction that we can use

171
00:07:34,139 --> 00:07:38,460
to replace the holes and we'll have a

172
00:07:36,479 --> 00:07:42,060
set of enlistments for each transaction

173
00:07:38,460 --> 00:07:44,699
each having the same counts which is one

174
00:07:42,060 --> 00:07:48,060
thousand hex so 4K

175
00:07:44,699 --> 00:07:51,360
total enlistments per transaction then

176
00:07:48,060 --> 00:07:53,759
we have the instructions of the lab so

177
00:07:51,360 --> 00:07:56,400
the goal is to spray a KENLISTMENT

178
00:07:53,759 --> 00:07:58,139
structures and have alternating KENLISTMENT

179
00:07:56,400 --> 00:07:59,940
structures being spread from two

180
00:07:58,139 --> 00:08:02,039
transactions free one set of

181
00:07:59,940 --> 00:08:04,319
KENLISTMENTs from one
of the transaction and

182
00:08:02,039 --> 00:08:06,060
leave the other intact and then observe

183
00:08:04,319 --> 00:08:08,220
the resulting holes even though it's not

184
00:08:06,060 --> 00:08:10,440
really ideal as we'll see so then we

185
00:08:08,220 --> 00:08:13,080
have all the objects that we need to

186
00:08:10,440 --> 00:08:14,759
have initialized transaction manager the

187
00:08:13,080 --> 00:08:17,099
resource manager so we can actually do

188
00:08:14,759 --> 00:08:19,379
stuff so the goal is going to be to

189
00:08:17,099 --> 00:08:22,080
create two transactions by coding create

190
00:08:19,379 --> 00:08:24,060
transaction and then spray an

191
00:08:22,080 --> 00:08:27,120
enlistments by calling create

192
00:08:24,060 --> 00:08:29,940
enlistments on both sets in order to

193
00:08:27,120 --> 00:08:31,860
have an alternating spray once we've

194
00:08:29,940 --> 00:08:33,300
done that we see there is a call to

195
00:08:31,860 --> 00:08:35,399
create transaction in the third

196
00:08:33,300 --> 00:08:38,159
transaction at this stage we should be

197
00:08:35,399 --> 00:08:41,820
able to identify all the KENLISTMENTs

198
00:08:38,159 --> 00:08:43,200
being spread in an adjacency layout and

199
00:08:41,820 --> 00:08:45,779
so you should be able to use either a

200
00:08:43,200 --> 00:08:47,580
!pool or !poolfind command to

201
00:08:45,779 --> 00:08:49,980
find them and then the goal is going to

202
00:08:47,580 --> 00:08:52,680
be to actually free the enlistments

203
00:08:49,980 --> 00:08:55,740
from transaction number two so we

204
00:08:52,680 --> 00:08:58,320
provide code to actually change the state

205
00:08:55,740 --> 00:09:00,300
of the enlistments from the set B so

206
00:08:58,320 --> 00:09:02,940
associated with transaction two and the

207
00:09:00,300 --> 00:09:05,279
goal is going to be to prepare complete

208
00:09:02,940 --> 00:09:07,560
and to commit complete them as well and

209
00:09:05,279 --> 00:09:11,220
so at that stage you should be able to

210
00:09:07,560 --> 00:09:13,680
see that the enlistments of transaction

211
00:09:11,220 --> 00:09:16,140
number two are free and so you should be

212
00:09:13,680 --> 00:09:17,640
able to analyze the layout and see some

213
00:09:16,140 --> 00:09:20,040
holes so hopefully you get some

214
00:09:17,640 --> 00:09:22,620
allocated KENLISTMENTs adjacent to

215
00:09:20,040 --> 00:09:24,660
some KENLISTMENTs
size holes and so then we

216
00:09:22,620 --> 00:09:26,640
provide code to actually refill the

217
00:09:24,660 --> 00:09:28,500
holes with new KENLISTMENTs associated

218
00:09:26,640 --> 00:09:30,000
with transaction manager and then you

219
00:09:28,500 --> 00:09:31,980
should be able to analyze in the layout

220
00:09:30,000 --> 00:09:34,740
so feel free to play with the actual

221
00:09:31,980 --> 00:09:36,420
drain notification function because as

222
00:09:34,740 --> 00:09:38,279
we said depending on if you actually

223
00:09:36,420 --> 00:09:40,320
read the notifications you should see

224
00:09:38,279 --> 00:09:43,580
differences in the actual layout okay

225
00:09:40,320 --> 00:09:43,580
now it's your turn

