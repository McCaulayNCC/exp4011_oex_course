1
00:00:01,500 --> 00:00:07,860
so we're going to build the

2
00:00:04,319 --> 00:00:07,860
CommitSingleEnlistmentTx.exe

3
00:00:13,080 --> 00:00:16,519
and we're going to run it

4
00:00:16,800 --> 00:00:20,220
and see it gives out the instruction

5
00:00:18,240 --> 00:00:21,779
then it's asking first to set

6
00:00:20,220 --> 00:00:23,880
breakpoints

7
00:00:21,779 --> 00:00:25,680
then it tells that CommitTransactionAsync is called

8
00:00:23,880 --> 00:00:27,779
was called and CommitComplete

9
00:00:25,680 --> 00:00:29,580
was called actually nothing

10
00:00:27,779 --> 00:00:31,439
was done because we haven't modified the

11
00:00:29,580 --> 00:00:32,579
code

12
00:00:31,439 --> 00:00:35,600
so what we're going to do is we're going

13
00:00:32,579 --> 00:00:35,600
to set the breakpoints

14
00:00:36,059 --> 00:00:38,960
in WinDbg

15
00:00:43,379 --> 00:00:46,640
for these three functions

16
00:00:55,260 --> 00:00:58,940
and we're gonna rerun the binary

17
00:01:00,780 --> 00:01:04,680
so here we can see TmCreateEnlistment is

18
00:01:03,660 --> 00:01:08,720
called

19
00:01:04,680 --> 00:01:08,720
it's because the code is already here

20
00:01:09,000 --> 00:01:14,000
but it's passing and handle that is

21
00:01:11,520 --> 00:01:14,000
invalid

22
00:01:15,960 --> 00:01:19,520
and that's all it hits

23
00:01:20,159 --> 00:01:25,159
so what we're going to add now is a call

24
00:01:22,320 --> 00:01:25,159
to create transaction

25
00:01:26,520 --> 00:01:31,439
so CreateTransactionExt takes several

26
00:01:29,100 --> 00:01:33,840
arguments and see the lp transaction

27
00:01:31,439 --> 00:01:35,939
attribute is optional and null can be

28
00:01:33,840 --> 00:01:38,880
passed the second argument is reserved

29
00:01:35,939 --> 00:01:41,100
and should be zero create option is

30
00:01:38,880 --> 00:01:42,900
optional as well we don't really need to

31
00:01:41,100 --> 00:01:46,380
promote anything here

32
00:01:42,900 --> 00:01:50,340
isolation level is zero isolation

33
00:01:46,380 --> 00:01:52,500
flag is zero timeout is optional so we

34
00:01:50,340 --> 00:01:54,899
can specify zero to provide an infinite

35
00:01:52,500 --> 00:01:58,140
timeout and description is optional so

36
00:01:54,899 --> 00:01:59,159
basically we can pass zero null to most

37
00:01:58,140 --> 00:02:01,740
of the

38
00:01:59,159 --> 00:02:05,840
arguments so the handle to the

39
00:02:01,740 --> 00:02:05,840
transaction is called hTx

40
00:02:08,280 --> 00:02:15,900
so we're going to pass null

41
00:02:11,520 --> 00:02:18,620
zero zero zero zero

42
00:02:15,900 --> 00:02:18,620
and null

43
00:02:19,080 --> 00:02:24,300
we're gonna try rebuilding that

44
00:02:21,420 --> 00:02:27,900
and see if we hit create transaction

45
00:02:24,300 --> 00:02:31,040
so we need to exit the binary to be able

46
00:02:27,900 --> 00:02:31,040
to push the binary

47
00:02:34,319 --> 00:02:42,300
and now let's run it

48
00:02:37,800 --> 00:02:44,940
so we haven't hit any breakpoint yet

49
00:02:42,300 --> 00:02:47,840
when I hit enter you can see it hits

50
00:02:44,940 --> 00:02:47,840
create transaction

51
00:02:48,120 --> 00:02:52,440
then it hits create enlistment and

52
00:02:50,400 --> 00:02:54,360
that's all

53
00:02:52,440 --> 00:02:58,099
so now what we want to do is we want to

54
00:02:54,360 --> 00:02:58,099
add a call to commit complete

55
00:03:01,620 --> 00:03:06,780
so commit complete takes an argument to

56
00:03:04,440 --> 00:03:09,180
the enlistment handle and your virtual

57
00:03:06,780 --> 00:03:13,400
clock the virtual clock if you specify

58
00:03:09,180 --> 00:03:13,400
null the virtual value is not changed

59
00:03:16,500 --> 00:03:21,180
so we're just going to call commit

60
00:03:18,480 --> 00:03:23,040
complete on the

61
00:03:21,180 --> 00:03:25,879
enlistment handle

62
00:03:23,040 --> 00:03:25,879
and null

63
00:03:31,200 --> 00:03:38,420
we exit the binary and rebuild it

64
00:03:35,760 --> 00:03:38,420
now I'm gonna run the binary

65
00:03:42,239 --> 00:03:47,280
see it hasn't hit the breakpoint yet

66
00:03:45,659 --> 00:03:51,319
we hit enter

67
00:03:47,280 --> 00:03:51,319
we see it create the transaction

68
00:03:52,260 --> 00:03:56,940
to create the an enlistment

69
00:03:54,599 --> 00:03:59,120
and then it commit completely

70
00:03:56,940 --> 00:03:59,120
the enlistment

71
00:04:02,400 --> 00:04:07,260
okay so now we have a working binary can

72
00:04:05,459 --> 00:04:09,540
debug it

73
00:04:07,260 --> 00:04:11,340
so I have actually reverse engineered

74
00:04:09,540 --> 00:04:13,860
the functions related to the three

75
00:04:11,340 --> 00:04:15,959
breakpoints that we have defined so just

76
00:04:13,860 --> 00:04:17,340
to give you a quick overview what you

77
00:04:15,959 --> 00:04:20,820
need to do

78
00:04:17,340 --> 00:04:22,139
is basically Define the arguments of the

79
00:04:20,820 --> 00:04:22,979
function

80
00:04:22,139 --> 00:04:24,540
um

81
00:04:22,979 --> 00:04:27,000
see that

82
00:04:24,540 --> 00:04:28,800
some

83
00:04:27,000 --> 00:04:32,699
some function is being called to

84
00:04:28,800 --> 00:04:35,460
actually retrieve an object related to a

85
00:04:32,699 --> 00:04:37,259
handle then create the object all these

86
00:04:35,460 --> 00:04:40,080
functions are well known and can be

87
00:04:37,259 --> 00:04:43,940
defined so every Everything Is defined

88
00:04:40,080 --> 00:04:43,940
in in Ghidra the second one is

89
00:04:44,220 --> 00:04:50,699
create enlistment same thing arguments

90
00:04:47,479 --> 00:04:52,740
and or reference object by handle so we

91
00:04:50,699 --> 00:04:54,780
can see the fact the same functions are

92
00:04:52,740 --> 00:04:56,699
called in all the functions so once

93
00:04:54,780 --> 00:04:58,080
you've defined it once it will propagate

94
00:04:56,699 --> 00:05:01,020
to the other functions which is very

95
00:04:58,080 --> 00:05:04,320
handy and finally the commit complete

96
00:05:01,020 --> 00:05:07,820
enlistment with the arguments okay

97
00:05:04,320 --> 00:05:07,820
so now we're ready

98
00:05:08,880 --> 00:05:13,800
and we can actually

99
00:05:10,860 --> 00:05:16,199
debug it

100
00:05:13,800 --> 00:05:17,759
so we are now ready to debug the

101
00:05:16,199 --> 00:05:20,880
creation of the transaction

102
00:05:17,759 --> 00:05:22,860
the creation of the enlistment and the

103
00:05:20,880 --> 00:05:24,300
change of state for the enlistment we're

104
00:05:22,860 --> 00:05:26,759
going to analyze the KTRANSACTION

105
00:05:24,300 --> 00:05:28,620
structure as well as the KENLISTMENT

106
00:05:26,759 --> 00:05:30,960
structure we can see in the debugger

107
00:05:28,620 --> 00:05:32,340
that nothing hits yet we've pushed the

108
00:05:30,960 --> 00:05:35,300
binary already and we're going to

109
00:05:32,340 --> 00:05:35,300
execute it now

110
00:05:35,580 --> 00:05:40,620
so nothing hit in the debugger

111
00:05:39,000 --> 00:05:42,539
we're going to start

112
00:05:40,620 --> 00:05:45,360
creating the transaction by hitting

113
00:05:42,539 --> 00:05:48,500
enter now we hit the

114
00:05:45,360 --> 00:05:48,500
NtCreateTransactionExt function

115
00:05:49,080 --> 00:05:53,280
if we look in Ghidra

116
00:05:51,060 --> 00:05:56,340
we're going to skip all the code that

117
00:05:53,280 --> 00:05:58,919
actually does all the checks for the

118
00:05:56,340 --> 00:06:02,280
arguments we see that there is the

119
00:05:58,919 --> 00:06:04,259
creation of the object for the

120
00:06:02,280 --> 00:06:06,660
transaction here

121
00:06:04,259 --> 00:06:08,820
and we see that after that it's actually

122
00:06:06,660 --> 00:06:10,440
initializing the transaction so we're

123
00:06:08,820 --> 00:06:12,060
going to set the breakpoint on this

124
00:06:10,440 --> 00:06:15,300
TmInitializeTransaction function because

125
00:06:12,060 --> 00:06:17,699
it's passing the TX object I want to see

126
00:06:15,300 --> 00:06:20,419
how it's populating the object so we use

127
00:06:17,699 --> 00:06:20,419
Ctrl F2

128
00:06:24,060 --> 00:06:28,639
we see that it's actually setting

129
00:06:25,680 --> 00:06:28,639
breakpoint in WinDbg

130
00:06:30,720 --> 00:06:35,940
so we hit that break point

131
00:06:34,020 --> 00:06:39,080
so in the first argument we should have

132
00:06:35,940 --> 00:06:39,080
a transaction object

133
00:06:59,280 --> 00:07:04,080
so for now we can see

134
00:07:01,620 --> 00:07:06,000
it's actually

135
00:07:04,080 --> 00:07:07,680
not initialized the cookie is not valid

136
00:07:06,000 --> 00:07:10,699
this entry is not valid so we're going

137
00:07:07,680 --> 00:07:10,699
to step over that call

138
00:07:15,600 --> 00:07:21,120
so now we can see the cookie is valid it

139
00:07:18,960 --> 00:07:24,120
actually has a GUID

140
00:07:21,120 --> 00:07:26,580
the enlistment head is empty at the moment

141
00:07:24,120 --> 00:07:30,020
the state of the transaction is active

142
00:07:26,580 --> 00:07:30,020
it doesn't have any Enlistment

143
00:07:34,440 --> 00:07:38,520
it doesn't point to any transaction

144
00:07:36,479 --> 00:07:41,539
manager the outcome of the transaction

145
00:07:38,520 --> 00:07:41,539
is undetermined

146
00:07:47,900 --> 00:07:54,539
so the next thing we can check is if it

147
00:07:51,060 --> 00:07:56,220
actually evolves over time

148
00:07:54,539 --> 00:08:00,300
so we're going to set the breakpoint

149
00:07:56,220 --> 00:08:03,620
after the insert object with Ctrl F2

150
00:08:00,300 --> 00:08:03,620
and then continue execution

151
00:08:04,139 --> 00:08:08,280
okay so here is telling us that he

152
00:08:06,060 --> 00:08:11,220
wasn't able to set the hardware

153
00:08:08,280 --> 00:08:14,220
breakpoint due to too many breakpoints

154
00:08:11,220 --> 00:08:17,120
so we can actually delete

155
00:08:14,220 --> 00:08:17,120
the number three

156
00:08:17,400 --> 00:08:20,599
and continue execution

157
00:08:22,680 --> 00:08:27,360
so now

158
00:08:25,020 --> 00:08:30,500
and check

159
00:08:27,360 --> 00:08:30,500
that it's still

160
00:08:30,780 --> 00:08:33,560
the same

161
00:08:35,159 --> 00:08:40,880
I'm mostly interested to know if it's

162
00:08:37,080 --> 00:08:40,880
going to initialize the TM inside

163
00:08:41,039 --> 00:08:45,260
but I guess it doesn't look like it's

164
00:08:42,899 --> 00:08:45,260
the case

165
00:08:47,580 --> 00:08:50,480
just going to step over

166
00:08:50,519 --> 00:08:56,060
that call

167
00:08:52,740 --> 00:08:56,060
then check again

168
00:08:56,279 --> 00:09:01,860
right you know it's not updating Tm yet

169
00:08:59,279 --> 00:09:04,440
at that state so yeah

170
00:09:01,860 --> 00:09:08,839
so we've analyzed the transaction

171
00:09:04,440 --> 00:09:08,839
when NtCreateTransaction is called

172
00:09:09,180 --> 00:09:15,000
so now we continue execution and we hit

173
00:09:12,120 --> 00:09:17,040
the NtCreateEnlistment function so

174
00:09:15,000 --> 00:09:19,920
the NtCreateEnlistment function is

175
00:09:17,040 --> 00:09:21,600
going to actually retrieve the

176
00:09:19,920 --> 00:09:23,100
transaction and then create the

177
00:09:21,600 --> 00:09:25,019
enlistment so we're going to set a

178
00:09:23,100 --> 00:09:28,680
breakpoint on the create enlistment

179
00:09:25,019 --> 00:09:32,700
function with Ctrl F2 and then

180
00:09:28,680 --> 00:09:34,980
you need to delete the previous

181
00:09:32,700 --> 00:09:37,440
break points

182
00:09:34,980 --> 00:09:41,100
so we're going to continue execution

183
00:09:37,440 --> 00:09:42,480
so here we have one two three four five

184
00:09:41,100 --> 00:09:46,040
six

185
00:09:42,480 --> 00:09:46,040
so on the stack

186
00:09:46,800 --> 00:09:53,480
so on the stack we see this is the

187
00:09:50,519 --> 00:09:53,480
RM object

188
00:10:05,040 --> 00:10:10,140
you can see the resource measure is

189
00:10:07,860 --> 00:10:12,720
online the cookie

190
00:10:10,140 --> 00:10:14,640
as well as the fact that the Tm points

191
00:10:12,720 --> 00:10:17,100
to a valid Tm so here you can actually

192
00:10:14,640 --> 00:10:19,740
click here to see it at the get

193
00:10:17,100 --> 00:10:22,080
transaction manager KTM so you can see

194
00:10:19,740 --> 00:10:24,420
it's a cookie the KTM is online the

195
00:10:22,080 --> 00:10:28,920
transaction manager is online

196
00:10:24,420 --> 00:10:33,140
and then we know the second or the sixth

197
00:10:28,920 --> 00:10:33,140
argument is going to be a transaction

198
00:10:41,640 --> 00:10:48,920
so this is the actual

199
00:10:44,760 --> 00:10:48,920
transaction we had before right

200
00:10:49,200 --> 00:10:53,880
so you can confirm that the transaction

201
00:10:51,839 --> 00:10:55,800
we had when we actually called

202
00:10:53,880 --> 00:10:58,260
NtCreateTransaction is the one that is now

203
00:10:55,800 --> 00:11:01,760
passed to the create

204
00:10:58,260 --> 00:11:01,760
NtCreateEnlistment function so

205
00:11:02,519 --> 00:11:06,899
now that we are NtCreateEnlistmentExt

206
00:11:04,440 --> 00:11:09,120
which is going to step over the

207
00:11:06,899 --> 00:11:11,940
TmCreateEnlistment call

208
00:11:09,120 --> 00:11:13,440
so another question is where is our

209
00:11:11,940 --> 00:11:16,260
Enlistment

210
00:11:13,440 --> 00:11:19,260
so if we print again the KTRANSACTION

211
00:11:16,260 --> 00:11:22,200
we can see that now enlistment head

212
00:11:19,260 --> 00:11:25,260
contains one entry

213
00:11:22,200 --> 00:11:27,120
and before that before the call he was

214
00:11:25,260 --> 00:11:29,640
actually empty so now there is one

215
00:11:27,120 --> 00:11:31,320
Enlistment

216
00:11:29,640 --> 00:11:34,260
so

217
00:11:31,320 --> 00:11:37,680
so how do we print the enlistment head so

218
00:11:34,260 --> 00:11:40,620
we know this point to the single entry

219
00:11:37,680 --> 00:11:44,420
if we actually try to print that at the

220
00:11:42,365 --> 00:11:46,165
KENLISTMENT let's see what happens

221
00:11:52,084 --> 00:11:57,065
it doesn't look like its valid right because

222
00:11:54,185 --> 00:11:59,045
the Cookie doesn't match so let's have a

223
00:11:57,065 --> 00:12:03,024
look at the actual structures

224
00:11:59,045 --> 00:12:03,024
we are interested in the KTRANSACTION

225
00:12:05,885 --> 00:12:12,005
the KTRANSACTION has a list of

226
00:12:09,185 --> 00:12:15,445
enlistments so we are also interested in

227
00:12:12,005 --> 00:12:15,445
the KENLISTMENT

228
00:12:17,465 --> 00:12:23,165
if you look at the KENLISTMENT

229
00:12:19,105 --> 00:12:25,024
the KENLISTMENT actually has two lists one

230
00:12:23,165 --> 00:12:27,665
of the KENLISTMENT being part of the same

231
00:12:25,024 --> 00:12:29,764
transaction and another one of KENLISTMENT

232
00:12:27,665 --> 00:12:33,065
being part of the same resource manager

233
00:12:29,764 --> 00:12:35,345
so what could the enlistment head points

234
00:12:33,065 --> 00:12:38,045
to well actually it's going to point to

235
00:12:35,345 --> 00:12:39,545
the enlistment but in the middle of the

236
00:12:38,045 --> 00:12:43,385
enlistment so it's going to point to

237
00:12:39,545 --> 00:12:46,024
offset 78 so here instead of printing

238
00:12:43,385 --> 00:12:49,505
the KENLISTMENT at this address we need to

239
00:12:46,024 --> 00:12:49,505
subtract 78

240
00:12:49,805 --> 00:12:55,084
so let's do that here we can see it

241
00:12:52,865 --> 00:12:58,805
aligns correctly with the cookie and the

242
00:12:55,084 --> 00:13:02,704
enlistment is valid and active the

243
00:12:58,805 --> 00:13:05,584
actual lists are also valid pointers the

244
00:13:02,704 --> 00:13:08,345
notification mask is 39FFF

245
00:13:05,584 --> 00:13:11,785
something and it matches the

246
00:13:08,345 --> 00:13:11,785
notification mask that we used

247
00:13:18,965 --> 00:13:23,764
and the flags are zero so we have now

248
00:13:21,545 --> 00:13:27,125
confirmed the creation of the

249
00:13:23,764 --> 00:13:29,584
enlistments and that the enlistment is

250
00:13:27,125 --> 00:13:32,345
linking the transaction and the resource

251
00:13:29,584 --> 00:13:34,084
manager so this confirms what NtCreateEnlistment

252
00:13:32,345 --> 00:13:37,105
is doing so now we're

253
00:13:34,084 --> 00:13:37,105
gonna continue execution

254
00:13:38,584 --> 00:13:43,384
so we hit the NtCommitCompleteExt

255
00:13:41,644 --> 00:13:45,605
function

256
00:13:43,384 --> 00:13:49,384
so this function we're going to skip all

257
00:13:45,605 --> 00:13:52,925
the checks of the arguments we see here

258
00:13:49,384 --> 00:13:55,144
it's retrieved the enlistment object based

259
00:13:52,925 --> 00:13:57,185
on the handle and finally it's called

260
00:13:55,144 --> 00:13:58,805
the TmCommitComplete function so we're

261
00:13:57,185 --> 00:14:02,245
going to set a breakpoint on

262
00:13:58,805 --> 00:14:02,245
TmCommitComplete with Ctrl F2

263
00:14:02,945 --> 00:14:06,605
and we're gonna delete the Third

264
00:14:05,345 --> 00:14:08,105
breakpoint

265
00:14:06,605 --> 00:14:10,945
that we don't need anymore we're going

266
00:14:08,105 --> 00:14:10,945
to continue execution

267
00:14:12,005 --> 00:14:17,524
so we can check that rcx which is the

268
00:14:15,005 --> 00:14:19,565
enlistment object

269
00:14:17,524 --> 00:14:22,384
is actually matching the KENLISTMENT

270
00:14:19,565 --> 00:14:24,805
that we showed earlier so to do that we

271
00:14:22,384 --> 00:14:24,805
can do

272
00:14:27,305 --> 00:14:30,785
so here the problem is that it's

273
00:14:28,925 --> 00:14:33,005
actually interpreting the numbers

274
00:14:30,785 --> 00:14:37,805
incorrectly

275
00:14:33,005 --> 00:14:40,204
right so yeah here I had to specify 0x

276
00:14:37,805 --> 00:14:43,685
in prompt even though normally

277
00:14:40,204 --> 00:14:46,324
in most commands it interprets the

278
00:14:43,685 --> 00:14:48,965
numbers as hexadecimal in the question

279
00:14:46,324 --> 00:14:51,545
mark command it doesn't so I had to

280
00:14:48,965 --> 00:14:53,465
actually specify that in a it's an

281
00:14:51,545 --> 00:14:56,225
hexadecimal number so it computes the

282
00:14:53,465 --> 00:14:58,264
right value and we can confirm that this

283
00:14:56,225 --> 00:15:00,605
KENLISTMENT is the one we're looking at

284
00:14:58,264 --> 00:15:03,264
earlier

285
00:15:00,605 --> 00:15:03,264
so

286
00:15:04,084 --> 00:15:08,665
this is equivalent to using this address

287
00:15:11,225 --> 00:15:16,884
so at that stage before calling

288
00:15:14,225 --> 00:15:20,285
TmCommitComplete we can see the

289
00:15:16,884 --> 00:15:23,704
enlistment is the enlistment delegated

290
00:15:20,285 --> 00:15:26,185
state also if we look back at the

291
00:15:23,704 --> 00:15:26,185
KTRANSACTION

292
00:15:27,605 --> 00:15:32,105
we can see the transaction is in the

293
00:15:30,545 --> 00:15:34,944
state

294
00:15:32,105 --> 00:15:34,944
delegated

295
00:15:39,905 --> 00:15:43,444
so what we're going to do is we're going

296
00:15:41,345 --> 00:15:45,685
to step over that call and see what

297
00:15:43,444 --> 00:15:45,685
happens

298
00:15:47,884 --> 00:15:52,264
so if we look again at the

299
00:15:49,204 --> 00:15:55,204
KTRANSACTION hey cool it worked

300
00:15:52,264 --> 00:15:58,285
the outcome is committed we managed to

301
00:15:55,204 --> 00:15:58,285
commit the transaction

302
00:15:58,384 --> 00:16:05,345
but before that the outcome was under

303
00:16:01,324 --> 00:16:07,444
termined so that works

304
00:16:05,345 --> 00:16:09,365
so we we managed to commit the

305
00:16:07,444 --> 00:16:11,285
transaction just by commiting the

306
00:16:09,365 --> 00:16:13,704
single enlistment that is part of the

307
00:16:11,285 --> 00:16:13,704
transaction

308
00:16:13,745 --> 00:16:19,685
so let's analyze the KENLISTMENT as well

309
00:16:16,565 --> 00:16:21,365
yeah that works as well the KENLISTMENT

310
00:16:19,685 --> 00:16:22,985
committed which I guess is expected

311
00:16:21,365 --> 00:16:25,565
because we know the transaction was

312
00:16:22,985 --> 00:16:28,024
committed anyway okay so to summarize we

313
00:16:25,565 --> 00:16:30,005
managed to create a transaction create

314
00:16:28,024 --> 00:16:31,324
an enlistment associated with the

315
00:16:30,005 --> 00:16:34,264
resource measure and that transaction

316
00:16:31,324 --> 00:16:37,024
and then wait for the transaction to be

317
00:16:34,264 --> 00:16:39,485
committed and then we committed the

318
00:16:37,024 --> 00:16:41,764
enlistment and that actually committed

319
00:16:39,485 --> 00:16:45,305
the transaction because the transaction

320
00:16:41,764 --> 00:16:47,725
only holds one enlistment okay thank you

321
00:16:45,305 --> 00:16:47,725
for watching

