1
00:00:00,179 --> 00:00:05,460
in this video we're going to try to

2
00:00:02,820 --> 00:00:08,700
answer this question how do you actually

3
00:00:05,460 --> 00:00:11,280
free the enlistment on demand and so

4
00:00:08,700 --> 00:00:13,860
basically you figure that out through

5
00:00:11,280 --> 00:00:17,279
reversing there is a function which is

6
00:00:13,860 --> 00:00:20,760
called TmpFinalizeEnlistment which

7
00:00:17,279 --> 00:00:23,820
ends up setting that finalized flag and

8
00:00:20,760 --> 00:00:26,460
it drops the ref count and so through

9
00:00:23,820 --> 00:00:29,039
reversing you can figure out that this

10
00:00:26,460 --> 00:00:32,160
TmpFinalizeEnlistment function is

11
00:00:29,039 --> 00:00:35,219
called kernell when calling the commit

12
00:00:32,160 --> 00:00:37,440
complete API from userland and in the

13
00:00:35,219 --> 00:00:39,719
recovery code inside TmRecoverResourceManager

14
00:00:37,440 --> 00:00:41,040
we know that it drops

15
00:00:39,719 --> 00:00:43,559
the ref count

16
00:00:41,040 --> 00:00:46,739
that it was holding right before it

17
00:00:43,559 --> 00:00:49,140
relocks the resource manager mutex and

18
00:00:46,739 --> 00:00:51,780
then anything else that might be holding

19
00:00:49,140 --> 00:00:54,120
a reference to that enlistment also

20
00:00:51,780 --> 00:00:57,180
needs to be taken into account in order

21
00:00:54,120 --> 00:01:00,539
to drop the reference count to zero and

22
00:00:57,180 --> 00:01:02,760
then only the kernel will finally free

23
00:01:00,539 --> 00:01:04,619
the enlistment but in order to actually

24
00:01:02,760 --> 00:01:07,680
deal with the enlistment from userland

25
00:01:04,619 --> 00:01:10,020
we actually created it with create an

26
00:01:07,680 --> 00:01:13,080
enlistment and got a handle in

27
00:01:10,020 --> 00:01:15,240
userland so we technically still have a

28
00:01:13,080 --> 00:01:18,000
reference in userland to that

29
00:01:15,240 --> 00:01:21,180
enlistment so it won't be freed until

30
00:01:18,000 --> 00:01:23,220
we get rid of that reference so

31
00:01:21,180 --> 00:01:24,360
basically just closing the handle from

32
00:01:23,220 --> 00:01:26,939
userland

33
00:01:24,360 --> 00:01:29,400
should be enough to drop the reference

34
00:01:26,939 --> 00:01:31,799
count to zero so the enlistment is

35
00:01:29,400 --> 00:01:35,280
effectively freed and so you can confirm

36
00:01:31,799 --> 00:01:37,979
from reversing and debugging that indeed

37
00:01:35,280 --> 00:01:40,320
that is the case so the naive approach

38
00:01:37,979 --> 00:01:42,180
to doing this because you don't know

39
00:01:40,320 --> 00:01:45,600
what KENLISTMENT to free

40
00:01:42,180 --> 00:01:47,579
necessarily is to just free them all and

41
00:01:45,600 --> 00:01:50,460
that's very much enough to at least

42
00:01:47,579 --> 00:01:52,799
confirm that you can trigger a use after

43
00:01:50,460 --> 00:01:55,079
free by forcing the race window opened

44
00:01:52,799 --> 00:01:57,659
with the debugger but the bad part of

45
00:01:55,079 --> 00:01:59,820
this is that if you are imagining real

46
00:01:57,659 --> 00:02:02,159
world exploitation of this and you want

47
00:01:59,820 --> 00:02:04,380
to win the race and you don't know which

48
00:02:02,159 --> 00:02:07,200
enlistment to free every time you

49
00:02:04,380 --> 00:02:09,720
attempt to figure the race which you

50
00:02:07,200 --> 00:02:11,099
might have to do thousands of times you

51
00:02:09,720 --> 00:02:13,500
would be creating a whole bunch of

52
00:02:11,099 --> 00:02:15,900
KENLISTMENT and then freeing them all

53
00:02:13,500 --> 00:02:18,959
and you would have to be reconfiguring

54
00:02:15,900 --> 00:02:21,300
that over and over again for each

55
00:02:18,959 --> 00:02:23,400
attempt and that would create a lot of

56
00:02:21,300 --> 00:02:26,280
noise on the kernel pool which is not

57
00:02:23,400 --> 00:02:28,379
ideal from our reliability perspective

58
00:02:26,280 --> 00:02:31,200
and so there is a actually a better

59
00:02:28,379 --> 00:02:34,080
approach to doing that but it is good to

60
00:02:31,200 --> 00:02:36,540
do the naive way first because it's

61
00:02:34,080 --> 00:02:38,760
faster to implement and it helps

62
00:02:36,540 --> 00:02:42,000
confirming your mental model so the

63
00:02:38,760 --> 00:02:44,519
better approach is to try to figure out

64
00:02:42,000 --> 00:02:47,580
which enlistment to free specifically

65
00:02:44,519 --> 00:02:50,940
in kernel but figuring that out from

66
00:02:47,580 --> 00:02:53,640
userland obviously so here I advise you

67
00:02:50,940 --> 00:02:56,459
pause the video and try to think about

68
00:02:53,640 --> 00:02:58,920
any ideas you might have to figure out

69
00:02:56,459 --> 00:03:01,440
which enlistment is currently being

70
00:02:58,920 --> 00:03:04,500
handled in the TmRecoverResourceManager

71
00:03:01,440 --> 00:03:07,860
loop that we would want to free

72
00:03:04,500 --> 00:03:10,019
by closing its handle and then you can

73
00:03:07,860 --> 00:03:12,959
move to the next slide so basically the

74
00:03:10,019 --> 00:03:15,420
whole point of the TmRecoverResourceManager

75
00:03:12,959 --> 00:03:18,420
function is that for every

76
00:03:15,420 --> 00:03:20,780
enlistment in the list it is queuing a

77
00:03:18,420 --> 00:03:22,860
notification related to that particular

78
00:03:20,780 --> 00:03:24,900
enlistments and you can query the

79
00:03:22,860 --> 00:03:27,540
notification from userland so so the

80
00:03:24,900 --> 00:03:30,840
point of the vulnerable code is to tell

81
00:03:27,540 --> 00:03:34,680
what enlistment has just been touched

82
00:03:30,840 --> 00:03:37,560
and so it is basically leaking you its

83
00:03:34,680 --> 00:03:42,060
internal state about which enlistment

84
00:03:37,560 --> 00:03:45,000
was the most recent one it touched and

85
00:03:42,060 --> 00:03:48,540
so if it is stuck somewhere in an

86
00:03:45,000 --> 00:03:51,120
infinite loop or stuck trying to lock

87
00:03:48,540 --> 00:03:53,700
the resource manager mutex and you query

88
00:03:51,120 --> 00:03:56,519
all of the notifications that have been

89
00:03:53,700 --> 00:03:59,340
queued into the notification queue the

90
00:03:56,519 --> 00:04:02,459
very last enlistment that is referenced

91
00:03:59,340 --> 00:04:04,500
in that notification queue is the

92
00:04:02,459 --> 00:04:07,140
enlistment that will have been

93
00:04:04,500 --> 00:04:09,780
potentially use-after-freed and so that

94
00:04:07,140 --> 00:04:12,120
that is the only one that you need to

95
00:04:09,780 --> 00:04:14,640
free effectively so it may not be that

96
00:04:12,120 --> 00:04:16,260
obvious at first but after spending

97
00:04:14,640 --> 00:04:18,660
quite a bit of time reversing and

98
00:04:16,260 --> 00:04:20,820
testing it should become obvious that

99
00:04:18,660 --> 00:04:22,800
that is the best approach and so inside

100
00:04:20,820 --> 00:04:25,979
the TmRecoverResourceManager

101
00:04:22,800 --> 00:04:28,320
function there is a call to to

102
00:04:25,979 --> 00:04:30,840
TmpSetNotificationResourceManager and that

103
00:04:28,320 --> 00:04:33,300
TmpSetNotificationResourceManager

104
00:04:30,840 --> 00:04:35,340
function basically puts something in the

105
00:04:33,300 --> 00:04:37,680
notification queue of the resource

106
00:04:35,340 --> 00:04:40,380
manager about that particular enlistment

107
00:04:37,680 --> 00:04:42,900
and then from userland you can call

108
00:04:40,380 --> 00:04:45,600
the get notification resource manager

109
00:04:42,900 --> 00:04:48,000
API as we have been doing from the labs

110
00:04:45,600 --> 00:04:50,040
and you can either correlate the

111
00:04:48,000 --> 00:04:53,460
enlistment that got the most recent

112
00:04:50,040 --> 00:04:55,740
notification based on its GUID which is

113
00:04:53,460 --> 00:04:58,199
a unique identifier associated with

114
00:04:55,740 --> 00:05:00,660
every enlistment or alternatively because

115
00:04:58,199 --> 00:05:03,360
really everything the kernel will be

116
00:05:00,660 --> 00:05:05,639
doing is deterministic based on the

117
00:05:03,360 --> 00:05:07,919
userland program you wrote and you know

118
00:05:05,639 --> 00:05:10,259
how many enlistments you are creating

119
00:05:07,919 --> 00:05:12,900
that are going to be introduced into

120
00:05:10,259 --> 00:05:15,300
that linked list in the kernel and then

121
00:05:12,900 --> 00:05:17,580
force TmRecoverResourceManager to

122
00:05:15,300 --> 00:05:19,860
process so you can basically just count

123
00:05:17,580 --> 00:05:22,020
the number of notifications that were

124
00:05:19,860 --> 00:05:24,660
queued and correlated to which

125
00:05:22,020 --> 00:05:26,940
enlistment it is in your userland

126
00:05:24,660 --> 00:05:29,460
locally tracked array of enlistments

127
00:05:26,940 --> 00:05:31,740
which is probably the fastest and most

128
00:05:29,460 --> 00:05:34,199
effective approach because basically

129
00:05:31,740 --> 00:05:36,360
when you are trying to win a race you

130
00:05:34,199 --> 00:05:39,360
want to figure out information as fast

131
00:05:36,360 --> 00:05:43,500
as possible and so doing a GUID compare

132
00:05:39,360 --> 00:05:46,860
is a lot slower than just doing an array

133
00:05:43,500 --> 00:05:48,780
index and so basically the strategy is

134
00:05:46,860 --> 00:05:50,460
that the function is going to unlock the

135
00:05:48,780 --> 00:05:52,680
resource manager and send the

136
00:05:50,460 --> 00:05:55,620
notification we can correlate the

137
00:05:52,680 --> 00:05:58,500
notification to the enlistments and free

138
00:05:55,620 --> 00:06:00,960
it by closing the handle and then we

139
00:05:58,500 --> 00:06:03,240
remove the infinite loop in the debugger

140
00:06:00,960 --> 00:06:05,759
and then the user-after-free hopefully

141
00:06:03,240 --> 00:06:07,680
trigger and there are basically two ways

142
00:06:05,759 --> 00:06:10,199
that you can detect it you can

143
00:06:07,680 --> 00:06:13,080
technically just debug and just step in

144
00:06:10,199 --> 00:06:15,419
the debugger after you have unpatched

145
00:06:13,080 --> 00:06:17,880
the infinite loop and then confirm

146
00:06:15,419 --> 00:06:21,300
manually using the !pool command

147
00:06:17,880 --> 00:06:23,580
that the enlistment chunk is freed or has

148
00:06:21,300 --> 00:06:26,340
been replaced by something else the

149
00:06:23,580 --> 00:06:28,620
other way is there is a debugging

150
00:06:26,340 --> 00:06:30,960
feature by Microsoft called driver

151
00:06:28,620 --> 00:06:34,620
verifier and basically what the driver

152
00:06:30,960 --> 00:06:37,860
verifier does is that it sets up memory

153
00:06:34,620 --> 00:06:41,160
pools in a special way that allows it to

154
00:06:37,860 --> 00:06:43,979
detect overruns and use-after-frees

155
00:06:41,160 --> 00:06:47,759
automatically it is really slow to run

156
00:06:43,979 --> 00:06:50,280
on a system but you can specify that you

157
00:06:47,759 --> 00:06:52,680
only want allocations from a specific

158
00:06:50,280 --> 00:06:55,380
driver to have this verifier

159
00:06:52,680 --> 00:06:58,500
functionality applied on and basically

160
00:06:55,380 --> 00:07:02,520
what you can do is to just apply the

161
00:06:58,500 --> 00:07:05,280
driver verifier on the tm.sys driver and

162
00:07:02,520 --> 00:07:07,860
then if a use-after-free happens it

163
00:07:05,280 --> 00:07:10,620
will just BSOD the machine right away

164
00:07:07,860 --> 00:07:13,139
and because you have WinDbg attached

165
00:07:10,620 --> 00:07:14,000
it will give you a crash analysis where

166
00:07:13,139 --> 00:07:16,740
you can

167
00:07:14,000 --> 00:07:19,139
confirm if it was the chunk associated

168
00:07:16,740 --> 00:07:22,080
with the enlistment that was just

169
00:07:19,139 --> 00:07:24,840
use-after-freed so this is the the function

170
00:07:22,080 --> 00:07:27,599
we provide in the lab for counting

171
00:07:24,840 --> 00:07:29,699
notifications the way it works is it

172
00:07:27,599 --> 00:07:32,280
calls the get notification resource

173
00:07:29,699 --> 00:07:34,440
manager API a bunch of times and

174
00:07:32,280 --> 00:07:36,360
increment a counter each time it to

175
00:07:34,440 --> 00:07:39,000
count the number of notification we

176
00:07:36,360 --> 00:07:41,759
encountered so far and you call that API

177
00:07:39,000 --> 00:07:43,919
until you get a timeout error indicating

178
00:07:41,759 --> 00:07:46,440
there is no new notification available

179
00:07:43,919 --> 00:07:49,580
and when that happens you just return

180
00:07:46,440 --> 00:07:52,440
the total count of notifications

181
00:07:49,580 --> 00:07:55,860
received and so after this function

182
00:07:52,440 --> 00:07:59,340
returns you can use the counter as an

183
00:07:55,860 --> 00:08:00,720
index in your array of enlistments that

184
00:07:59,340 --> 00:08:02,900
you are tracking in

185
00:08:00,720 --> 00:08:02,900
userland

