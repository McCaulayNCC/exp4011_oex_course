1
00:00:02,700 --> 00:00:07,859
hi everyone

2
00:00:04,319 --> 00:00:09,360
so I know you are very keen to exploits

3
00:00:07,859 --> 00:00:12,120
this vulnerablity

4
00:00:09,360 --> 00:00:14,280
and start leaking kernel
enlistments to userland

5
00:00:12,120 --> 00:00:15,660
then get an????????

6
00:00:14,280 --> 00:00:17,400
primitive

7
00:00:15,660 --> 00:00:20,640
and finally popping a shell

8
00:00:17,400 --> 00:00:22,920
so I'm sure you're gonna ask me why are

9
00:00:20,640 --> 00:00:25,140
we going to focus first on actually

10
00:00:22,920 --> 00:00:27,720
returning from the lower function

11
00:00:25,140 --> 00:00:30,300
cleanly don't worry I'm going to explain

12
00:00:27,720 --> 00:00:33,420
that and so in this video

13
00:00:30,300 --> 00:00:36,840
we're going to look into how to return

14
00:00:33,420 --> 00:00:38,700
?????? to userland without crashing

15
00:00:36,840 --> 00:00:41,879
so it can actually

16
00:00:38,700 --> 00:00:43,200
explore vulnerablity variously in the next

17
00:00:41,879 --> 00:00:45,780
few videos

18
00:00:43,200 --> 00:00:48,780
okay let's get started

19
00:00:45,780 --> 00:00:50,820
okay so we have solved many problems

20
00:00:48,780 --> 00:00:53,760
already so we have these trap

21
00:00:50,820 --> 00:00:55,800
enlistments that allow us to avoid a

22
00:00:53,760 --> 00:00:58,140
kernel crash so that's cool we can

23
00:00:55,800 --> 00:01:00,780
detect from userland that we won the

24
00:00:58,140 --> 00:01:03,000
race by checking for a flag change in

25
00:01:00,780 --> 00:01:05,460
our fake userland kenlistment which

26
00:01:03,000 --> 00:01:07,979
is cool too you can in theory inject

27
00:01:05,460 --> 00:01:10,619
some enlistments into this linked list

28
00:01:07,979 --> 00:01:12,540
at any time we want to do things to

29
00:01:10,619 --> 00:01:14,400
abuse the bug but now we have another

30
00:01:12,540 --> 00:01:16,619
problem to solve which is that

31
00:01:14,400 --> 00:01:19,080
eventually we're going to want to break

32
00:01:16,619 --> 00:01:21,420
out of this loop and ideally return from

33
00:01:19,080 --> 00:01:24,060
the kernel so that everything is

34
00:01:21,420 --> 00:01:26,640
relatively stable and this is useful for

35
00:01:24,060 --> 00:01:28,439
multiple reasons one of which is when

36
00:01:26,640 --> 00:01:30,600
you're doing explore development in

37
00:01:28,439 --> 00:01:33,000
general and you're not sure how the

38
00:01:30,600 --> 00:01:34,920
exploit primitives are going to work and

39
00:01:33,000 --> 00:01:37,020
you don't know whether or not you can

40
00:01:34,920 --> 00:01:38,880
patch memory and you want to just be

41
00:01:37,020 --> 00:01:40,979
testing triggering the vulnerability

42
00:01:38,880 --> 00:01:43,560
faster and faster by winning the race

43
00:01:40,979 --> 00:01:45,720
and testing new ideas ideally you want

44
00:01:43,560 --> 00:01:48,299
to win the race test your exploitation

45
00:01:45,720 --> 00:01:50,700
ideas and then not have to reboot your

46
00:01:48,299 --> 00:01:52,439
VM or restore from a snapshot because

47
00:01:50,700 --> 00:01:54,720
then you have to reattach the debugger

48
00:01:52,439 --> 00:01:56,280
and so on reset your breakpoints so even

49
00:01:54,720 --> 00:01:59,520
though you don't have code execution

50
00:01:56,280 --> 00:02:02,220
having clean recovery is ideal if

51
00:01:59,520 --> 00:02:04,860
possible and just in general keeping

52
00:02:02,220 --> 00:02:07,200
this type of technique in mind of how to

53
00:02:04,860 --> 00:02:09,840
cleanly escape the kernel is useful

54
00:02:07,200 --> 00:02:12,239
anytime you're doing a kernel exploit for

55
00:02:09,840 --> 00:02:14,879
stability or whatever so it's good to

56
00:02:12,239 --> 00:02:17,340
have in mind from the get-go and so

57
00:02:14,879 --> 00:02:19,800
basically in the vulnerable function we

58
00:02:17,340 --> 00:02:22,140
can see three ways for Escaping the loop

59
00:02:19,800 --> 00:02:24,180
the first one is
effectively the pEnlistment

60
00:02:22,140 --> 00:02:26,640
it's been shifted which is now pointing

61
00:02:24,180 --> 00:02:28,800
into userland would need to point to the

62
00:02:26,640 --> 00:02:31,860
K resource managers enlistment head

63
00:02:28,800 --> 00:02:33,599
address since this is what is tested at

64
00:02:31,860 --> 00:02:36,180
the beginning of this loop the problem

65
00:02:33,599 --> 00:02:38,760
is we don't know this enlistment head

66
00:02:36,180 --> 00:02:41,280
because it's a kernel address the second

67
00:02:38,760 --> 00:02:43,500
method for Escaping the loop is the

68
00:02:41,280 --> 00:02:45,660
resource manager could go offline at

69
00:02:43,500 --> 00:02:47,220
which point the loop would be escaped by

70
00:02:45,660 --> 00:02:49,500
but in order for the kenlistment

71
00:02:47,220 --> 00:02:51,239
manager to go offline I want to say it's

72
00:02:49,500 --> 00:02:53,459
not possible to switch that state

73
00:02:51,239 --> 00:02:55,500
because of the enlistments that are

74
00:02:53,459 --> 00:02:57,120
still associated with it but I'm not

75
00:02:55,500 --> 00:02:59,700
actually entirely sure and I actually

76
00:02:57,120 --> 00:03:01,980
don't remember if we tried that but

77
00:02:59,700 --> 00:03:04,800
because we had another way we didn't

78
00:03:01,980 --> 00:03:06,599
bother the third method for Escaping the

79
00:03:04,800 --> 00:03:09,060
loop would be to get certification

80
00:03:06,599 --> 00:03:11,340
somehow and just execute a shell code

81
00:03:09,060 --> 00:03:13,440
and have that shell could jump at the

82
00:03:11,340 --> 00:03:16,140
end of the rumble function but this

83
00:03:13,440 --> 00:03:19,980
requires bypassing mitigations such as

84
00:03:16,140 --> 00:03:22,440
kernel CFG or finding a way to control

85
00:03:19,980 --> 00:03:25,739
the insertion pointer which to be honest

86
00:03:22,440 --> 00:03:29,180
is not the easiest one thing to note is

87
00:03:25,739 --> 00:03:31,920
that we can't wait for all of the other

88
00:03:29,180 --> 00:03:34,019
enlistments that were originally part of

89
00:03:31,920 --> 00:03:36,300
the linked list to get parsed even

90
00:03:34,019 --> 00:03:39,120
though we know that the end of that list

91
00:03:36,300 --> 00:03:40,980
will necessarily point to the K resource

92
00:03:39,120 --> 00:03:43,560
manager enlistments head in the kernel

93
00:03:40,980 --> 00:03:47,280
and the reason is because we are not

94
00:03:43,560 --> 00:03:49,739
longer in that kernel list we are in just

95
00:03:47,280 --> 00:03:52,739
in userland and the kernel is parsing

96
00:03:49,739 --> 00:03:55,379
our userland data and so I mentioned

97
00:03:52,739 --> 00:03:57,239
there are potentially three methods to

98
00:03:55,379 --> 00:04:00,120
escape the loop and among the three

99
00:03:57,239 --> 00:04:02,459
methods I want to set the easiest one is

100
00:04:00,120 --> 00:04:05,220
definitely finding an information leak

101
00:04:02,459 --> 00:04:07,980
that allows us to leak the k resource

102
00:04:05,220 --> 00:04:10,739
manager address because then we can

103
00:04:07,980 --> 00:04:12,599
deduce the enlistment head address since

104
00:04:10,739 --> 00:04:14,879
it's just enough set from the beginning

105
00:04:12,599 --> 00:04:17,519
of the k Resource manager and if we

106
00:04:14,879 --> 00:04:21,000
are able to leave that address we can

107
00:04:17,519 --> 00:04:23,699
just set the next samerm flink pointer

108
00:04:21,000 --> 00:04:25,979
of our userand k enlistments to that

109
00:04:23,699 --> 00:04:28,500
specific address it will just exit the

110
00:04:25,979 --> 00:04:30,900
loop another reason for choosing the

111
00:04:28,500 --> 00:04:33,000
infolete method to escape the loop is

112
00:04:30,900 --> 00:04:35,520
that generally to exploit a kernel

113
00:04:33,000 --> 00:04:38,040
vulnerabilty like this you want to build an

114
00:04:35,520 --> 00:04:40,800
arbitrary read write primitive and to do

115
00:04:38,040 --> 00:04:43,020
so you are going to need an info leak in

116
00:04:40,800 --> 00:04:45,540
the first place anyway so it's basically

117
00:04:43,020 --> 00:04:48,060
killing two birds with one stone and

118
00:04:45,540 --> 00:04:50,400
another a rhythm for using the infinite

119
00:04:48,060 --> 00:04:53,580
method to escape the loop is that we

120
00:04:50,400 --> 00:04:55,919
know the kernel already touches our fake

121
00:04:53,580 --> 00:04:59,280
userland kenlistments and so potentially

122
00:04:55,919 --> 00:05:01,320
we can leak other stuff if we find good

123
00:04:59,280 --> 00:05:04,979
code path for the matter this diagram

124
00:05:01,320 --> 00:05:07,440
shows all the fake userland enlistments

125
00:05:04,979 --> 00:05:09,600
that we can build so far and so the new

126
00:05:07,440 --> 00:05:12,479
enlistments we introduce in the final

127
00:05:09,600 --> 00:05:14,639
step is the escape enlistment basically

128
00:05:12,479 --> 00:05:16,919
the idea would be that in step one we

129
00:05:14,639 --> 00:05:19,620
have this ??? enlistment that is used

130
00:05:16,919 --> 00:05:22,320
to replace the free kenlistment chunk and

131
00:05:19,620 --> 00:05:24,600
this enlistment is also named use after

132
00:05:22,320 --> 00:05:27,419
free enlistment in this diagram in

133
00:05:24,600 --> 00:05:29,820
step 2 there is a trap enlistment that

134
00:05:27,419 --> 00:05:31,860
initially points to itself so we can

135
00:05:29,820 --> 00:05:35,280
detect the race condition from

136
00:05:31,860 --> 00:05:37,620
userland in step 3 we introduce primitive

137
00:05:35,280 --> 00:05:40,560
enlistments to elevate our privileges

138
00:05:37,620 --> 00:05:43,800
and they are yet to be found and in the

139
00:05:40,560 --> 00:05:46,380
last step we can inject some new user

140
00:05:43,800 --> 00:05:49,680
land fake enlistments that we call all

141
00:05:46,380 --> 00:05:52,560
escape enlistments which has its next

142
00:05:49,680 --> 00:05:54,600
samerm flame pointer set to the K

143
00:05:52,560 --> 00:05:56,460
resource manager enlistment head in

144
00:05:54,600 --> 00:05:59,160
order to escape the loop and return to

145
00:05:56,460 --> 00:06:02,039
userland and so basically you only do

146
00:05:59,160 --> 00:06:04,979
that after you've completely escalated

147
00:06:02,039 --> 00:06:07,380
privileges or got kernel code execution

148
00:06:04,979 --> 00:06:09,960
or whatever and this is because all the

149
00:06:07,380 --> 00:06:11,940
primitives we can trigger are possible

150
00:06:09,960 --> 00:06:15,060
because we have some control from

151
00:06:11,940 --> 00:06:17,220
crafting fake ser land enlistments and

152
00:06:15,060 --> 00:06:20,880
there is this kernel thread which

153
00:06:17,220 --> 00:06:23,160
executes in this loop and is passing a

154
00:06:20,880 --> 00:06:25,380
fake userland enlistments and so as

155
00:06:23,160 --> 00:06:27,600
soon as we make this kernel thread exit

156
00:06:25,380 --> 00:06:30,000
this loop by using an escape enlistment

157
00:06:27,600 --> 00:06:32,819
it will go back to userland and we

158
00:06:30,000 --> 00:06:34,979
basically lose all control and so we

159
00:06:32,819 --> 00:06:37,560
need to escalate our privileges before

160
00:06:34,979 --> 00:06:41,240
we exit the loop and so to do so we

161
00:06:37,560 --> 00:06:41,240
basically need an information leak

