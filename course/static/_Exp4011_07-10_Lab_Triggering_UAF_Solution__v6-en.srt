1
00:00:00,179 --> 00:00:04,440
so I haven't modified the source code

2
00:00:02,760 --> 00:00:07,080
yet but I'm just going to run the binary

3
00:00:04,440 --> 00:00:11,540
to get the flavor or what it's doing so

4
00:00:07,080 --> 00:00:11,540
I've attached WinDbg to the target VM

5
00:00:11,940 --> 00:00:18,480
and the first thing it's doing is it's

6
00:00:14,700 --> 00:00:20,640
draining some named pipes

7
00:00:18,480 --> 00:00:23,880
okay so now it's telling me to set

8
00:00:20,640 --> 00:00:26,400
breakpoints uh for the recovery thread

9
00:00:23,880 --> 00:00:29,340
basically to see when it enters the

10
00:00:26,400 --> 00:00:32,099
vulnerable function and then when it actually

11
00:00:29,340 --> 00:00:36,239
loops for each enlistment when it's

12
00:00:32,099 --> 00:00:38,660
trying to send a notification so like in

13
00:00:36,239 --> 00:00:38,660
WinDbg

14
00:00:40,680 --> 00:00:48,200
I have these two breakpoints so just to

15
00:00:44,100 --> 00:00:48,200
show you what this address is about

16
00:00:50,700 --> 00:00:54,960
you can see it's when it's calling

17
00:00:53,039 --> 00:00:58,579
TmpSetNotificationResourceManager so I'm

18
00:00:54,960 --> 00:00:58,579
gonna enable both breakpoints

19
00:00:59,039 --> 00:01:02,120
and continue execution

20
00:01:02,460 --> 00:01:08,580
so I'm hitting enter now

21
00:01:05,400 --> 00:01:11,520
so we can see it hangs

22
00:01:08,580 --> 00:01:13,200
so now the recovery thread is entering

23
00:01:11,520 --> 00:01:14,760
the vulnerable function and I'm

24
00:01:13,200 --> 00:01:18,000
continuing execution

25
00:01:14,760 --> 00:01:21,479
and now it's going to actually set a

26
00:01:18,000 --> 00:01:24,000
notification for the first enlistment

27
00:01:21,479 --> 00:01:25,619
so if I look at the actual code we can

28
00:01:24,000 --> 00:01:27,540
see it's when it's calling

29
00:01:25,619 --> 00:01:29,400
TmpSetNotificationResourceManager and the

30
00:01:27,540 --> 00:01:31,619
arguments are the resource managers

31
00:01:29,400 --> 00:01:33,119
first argument and the enlistment in the

32
00:01:31,619 --> 00:01:35,520
second argument

33
00:01:33,119 --> 00:01:40,220
and if I look at the signature the

34
00:01:35,520 --> 00:01:40,220
arguments are in RCX and R8

35
00:01:54,119 --> 00:01:58,820
so this is a resource manager

36
00:02:02,399 --> 00:02:05,720
and this will be an enlistment

37
00:02:15,840 --> 00:02:22,440
so if we look at both of them

38
00:02:18,420 --> 00:02:24,599
the resource manager has a cookie valid

39
00:02:22,440 --> 00:02:27,319
if we look at the

40
00:02:24,599 --> 00:02:30,959
valid transaction manager as well

41
00:02:27,319 --> 00:02:33,480
and the enlistment count is 5000 hex

42
00:02:30,959 --> 00:02:35,459
which corresponds to the value we have

43
00:02:33,480 --> 00:02:38,040
in the source code and we can see the

44
00:02:35,459 --> 00:02:40,680
enlistment head is pointing to the linked

45
00:02:38,040 --> 00:02:43,200
list and then for the enlistment

46
00:02:40,680 --> 00:02:46,760
you see it's a valid enlistment and it's

47
00:02:43,200 --> 00:02:46,760
in the committed notified state

48
00:02:47,640 --> 00:02:52,620
okay

49
00:02:48,959 --> 00:02:56,640
so now if you want to skip breakpoints

50
00:02:52,620 --> 00:03:00,120
what we can do is we can use G

51
00:02:56,640 --> 00:03:04,280
to go several times so I'm just gonna

52
00:03:00,120 --> 00:03:04,280
copy that a bunch of times

53
00:03:09,780 --> 00:03:13,940
and we need that to be more than 32.

54
00:03:31,560 --> 00:03:35,300
okay so I'm gonna

55
00:03:36,140 --> 00:03:41,640
look at the actual

56
00:03:38,420 --> 00:03:43,080
debugging session so nothing is printed

57
00:03:41,640 --> 00:03:44,940
yet

58
00:03:43,080 --> 00:03:47,120
I'm just going to disable the breakpoint

59
00:03:44,940 --> 00:03:47,120
now

60
00:03:48,299 --> 00:03:51,019
continue execution

61
00:03:51,959 --> 00:03:55,920
so

62
00:03:53,640 --> 00:03:57,900
well obviously it's telling us to block

63
00:03:55,920 --> 00:03:59,519
the recovery thread which we're not

64
00:03:57,900 --> 00:04:01,080
going to do right now because we we're

65
00:03:59,519 --> 00:04:02,940
not doing anything we haven't coded

66
00:04:01,080 --> 00:04:05,879
anything but if I continue execution

67
00:04:02,940 --> 00:04:08,040
it's basically telling us that it

68
00:04:05,879 --> 00:04:10,500
couldn't actually count the notification

69
00:04:08,040 --> 00:04:12,360
it's because the code is not there yet

70
00:04:10,500 --> 00:04:14,700
we need to add the code to code the

71
00:04:12,360 --> 00:04:16,979
notification and then it's telling us to

72
00:04:14,700 --> 00:04:20,220
unpatch the recovery thread

73
00:04:16,979 --> 00:04:22,680
um so in order to get the user-after-free

74
00:04:20,220 --> 00:04:24,479
okay so now we're ready to modify the

75
00:04:22,680 --> 00:04:26,520
source code

76
00:04:24,479 --> 00:04:28,320
so there are two functions to actually

77
00:04:26,520 --> 00:04:31,500
count the resource managers

78
00:04:28,320 --> 00:04:33,060
notifications if we look at them we can

79
00:04:31,500 --> 00:04:35,699
see

80
00:04:33,060 --> 00:04:38,820
the count_notifications return the number

81
00:04:35,699 --> 00:04:40,380
of queued notification encountered

82
00:04:38,820 --> 00:04:43,320
and what it's going to do is going to

83
00:04:40,380 --> 00:04:45,660
set timeout and then it's going to loop

84
00:04:43,320 --> 00:04:48,120
over until it actually times out without

85
00:04:45,660 --> 00:04:51,300
any notification whereas the first one

86
00:04:48,120 --> 00:04:53,400
which is the blocking one

87
00:04:51,300 --> 00:04:55,860
is doing a bit of additional things

88
00:04:53,400 --> 00:04:59,340
uh the first thing it does is it

89
00:04:55,860 --> 00:05:02,460
actually get a notification first with

90
00:04:59,340 --> 00:05:05,520
an infinite timeout and so the idea is

91
00:05:02,460 --> 00:05:07,919
that we want to make sure we at least

92
00:05:05,520 --> 00:05:12,540
get one notification

93
00:05:07,919 --> 00:05:14,160
before we consider it's a timeout and I

94
00:05:12,540 --> 00:05:15,900
think it's going to help with debugging

95
00:05:14,160 --> 00:05:18,720
because when we're going to be debugging

96
00:05:15,900 --> 00:05:20,940
potentially the thread may just get no

97
00:05:18,720 --> 00:05:24,120
notification because we're debugging

98
00:05:20,940 --> 00:05:26,400
and so I think we should choose this one

99
00:05:24,120 --> 00:05:29,660
it's taking as an argument the

100
00:05:26,400 --> 00:05:29,660
resource manager handle

101
00:05:35,160 --> 00:05:42,680
so we want to read at least 32

102
00:05:39,180 --> 00:05:42,680
enlistments notification

103
00:05:51,840 --> 00:05:56,120
so the notified counts

104
00:06:05,820 --> 00:06:11,300
so while the notified counts is less than

105
00:06:08,940 --> 00:06:11,300
32

106
00:06:20,639 --> 00:06:26,960
so we updated it counts

107
00:06:23,780 --> 00:06:26,960
to be

108
00:06:27,419 --> 00:06:33,919
equal to

109
00:06:29,580 --> 00:06:33,919
um reading some notifications

110
00:06:39,600 --> 00:06:44,340
from the resource manager handle so the

111
00:06:41,880 --> 00:06:47,660
resource manager handle should be in the

112
00:06:44,340 --> 00:06:47,660
pxvars

113
00:07:04,979 --> 00:07:13,220
I'm just gonna add a printf

114
00:07:08,759 --> 00:07:13,220
when we actually get some notification

115
00:07:26,580 --> 00:07:28,819


116
00:07:43,680 --> 00:07:49,440
so what we're doing is we loop until we

117
00:07:46,979 --> 00:07:51,539
actually reached reading at least 32

118
00:07:49,440 --> 00:07:53,099
notifications and so we're going to

119
00:07:51,539 --> 00:07:55,199
block each time and we're going to

120
00:07:53,099 --> 00:07:57,780
increase the notified count and each I'm

121
00:07:55,199 --> 00:07:59,520
going to show how much we read then it

122
00:07:57,780 --> 00:08:01,979
should actually show how many

123
00:07:59,520 --> 00:08:04,319
notifications have been received because

124
00:08:01,979 --> 00:08:08,060
initially we saw that it was actually

125
00:08:04,319 --> 00:08:08,060
showing only zero

126
00:08:17,880 --> 00:08:23,280
so the next thing we need to do is we

127
00:08:20,039 --> 00:08:25,699
need to actually free the latest touch

128
00:08:23,280 --> 00:08:28,080
enlistment and then free all the others

129
00:08:25,699 --> 00:08:29,520
so the way I'm going to do it is I'm

130
00:08:28,080 --> 00:08:33,419
going to actually free them all

131
00:08:29,520 --> 00:08:38,060
backwards starting from the latest touch

132
00:08:33,419 --> 00:08:38,060
so I'm gonna need an index

133
00:08:53,459 --> 00:08:57,959
so the latest touch and enlistment has

134
00:08:56,399 --> 00:08:59,580
this index

135
00:08:57,959 --> 00:09:03,240
-1 because that's the number of

136
00:08:59,580 --> 00:09:06,260
enlistments because it starts at zero

137
00:09:03,240 --> 00:09:06,260
so it's going to be

138
00:09:07,740 --> 00:09:10,519
en index

139
00:09:10,920 --> 00:09:12,920
-1

140
00:09:15,360 --> 00:09:21,420
we want to loop until we reach 0 in the

141
00:09:19,140 --> 00:09:23,880
first iteration but then in the following

142
00:09:21,420 --> 00:09:27,660
iteration it needs to be taken to

143
00:09:23,880 --> 00:09:27,660
account the actual Delta

144
00:09:27,959 --> 00:09:32,720
seems to be

145
00:09:30,000 --> 00:09:32,720
I

146
00:09:46,980 --> 00:09:49,160
so on the first iteration

147
00:09:53,959 --> 00:10:01,860
notified count will be for instance 10

148
00:09:57,360 --> 00:10:06,180
on 32 and then an index will be 32 so

149
00:10:01,860 --> 00:10:09,500
it will start from 31 index 31 until 0.

150
00:10:06,180 --> 00:10:09,500
so that works

151
00:10:11,279 --> 00:10:14,899
and then we just need to decrease

152
00:10:21,779 --> 00:10:29,220
okay so what we need is we need to get

153
00:10:25,560 --> 00:10:32,720
the enlistment from the actual array so

154
00:10:29,220 --> 00:10:32,720
this will be in pxvars

155
00:10:39,480 --> 00:10:41,540
um

156
00:10:50,100 --> 00:10:53,959
so it's your en list

157
00:10:56,160 --> 00:11:01,980
and then it's going to be pentries

158
00:10:58,740 --> 00:11:05,160
which is an array and we need the index

159
00:11:01,980 --> 00:11:07,980
I and then we need to take

160
00:11:05,160 --> 00:11:09,660
the actual handle

161
00:11:07,980 --> 00:11:12,560
so here we're going to store that into

162
00:11:09,660 --> 00:11:12,560
a variable

163
00:11:14,399 --> 00:11:18,500
so latest touched enlistment

164
00:11:30,180 --> 00:11:34,920
okay so now that we've got the latest

165
00:11:33,240 --> 00:11:37,380
touched enlistment now we can actually

166
00:11:34,920 --> 00:11:40,820
commit complete this enlistment and then

167
00:11:37,380 --> 00:11:40,820
close the handle

168
00:11:46,800 --> 00:11:49,800
complete

169
00:11:51,959 --> 00:11:57,260
so

170
00:11:54,000 --> 00:11:57,260
this handle

171
00:12:00,240 --> 00:12:03,200
we can pass it on

172
00:12:07,500 --> 00:12:11,779
and finally we need to close the handle

173
00:12:21,720 --> 00:12:26,579
so I'm just going to add a little bit of

174
00:12:24,540 --> 00:12:29,459
output just to make it clearer what's

175
00:12:26,579 --> 00:12:31,940
going to happen in the debugger so I'm

176
00:12:29,459 --> 00:12:31,940
gonna do

177
00:12:33,720 --> 00:12:36,500
if

178
00:12:38,459 --> 00:12:41,360
I

179
00:12:43,260 --> 00:12:48,420
equal the first iteration

180
00:12:52,019 --> 00:12:55,100
and do a printf

181
00:13:16,560 --> 00:13:21,920
and then otherwise I'm just gonna print

182
00:13:23,579 --> 00:13:26,240
a list

183
00:13:44,160 --> 00:13:48,120
so the first iteration is going to call

184
00:13:45,899 --> 00:13:51,480
printf and it will actually print

185
00:13:48,120 --> 00:13:54,360
free elistment at index

186
00:13:51,480 --> 00:13:58,100
and then

187
00:13:54,360 --> 00:13:58,100
I actually add

188
00:14:01,260 --> 00:14:06,300
just to get some output that we're

189
00:14:03,720 --> 00:14:07,800
freeing the right stuff okay let's try

190
00:14:06,300 --> 00:14:09,540
that code

191
00:14:07,800 --> 00:14:11,899
okay so I'm just going to rerun the

192
00:14:09,540 --> 00:14:11,899
binary

193
00:14:16,620 --> 00:14:21,600
it's draining some named pipes

194
00:14:19,920 --> 00:14:26,300
okay

195
00:14:21,600 --> 00:14:26,300
so now you can enable the breakpoints

196
00:14:39,000 --> 00:14:44,820
continue the execution now we're gonna

197
00:14:42,060 --> 00:14:47,100
continue execution

198
00:14:44,820 --> 00:14:49,440
you can see it hangs as

199
00:14:47,100 --> 00:14:51,839
you can see we enter the vulnerable

200
00:14:49,440 --> 00:14:55,139
function

201
00:14:51,839 --> 00:14:57,540
and now we're hitting the code that's

202
00:14:55,139 --> 00:15:00,360
going to send notification so now I'm

203
00:14:57,540 --> 00:15:03,680
gonna use the the break the the command

204
00:15:00,360 --> 00:15:03,680
to actually skip it many times

205
00:15:04,800 --> 00:15:07,040
a few moments later

206
00:15:09,720 --> 00:15:15,959
okay so now we have touched many

207
00:15:13,740 --> 00:15:18,300
enlistment so

208
00:15:15,959 --> 00:15:20,480
what we want to do is we want to

209
00:15:18,300 --> 00:15:23,639
actually

210
00:15:20,480 --> 00:15:26,820
patch the actual code so it doesn't

211
00:15:23,639 --> 00:15:31,040
continue and it's just stuck after it's

212
00:15:26,820 --> 00:15:31,040
handled that specific enlistment

213
00:15:31,380 --> 00:15:37,940
and so when I continue execution now

214
00:15:35,040 --> 00:15:40,980
we see that it's actually

215
00:15:37,940 --> 00:15:43,019
reaching these code and then it's

216
00:15:40,980 --> 00:15:45,899
patching the mutex so now it's stuck

217
00:15:43,019 --> 00:15:49,800
like the actual recovery thread is stuck

218
00:15:45,899 --> 00:15:52,380
in just after sending the notification

219
00:15:49,800 --> 00:15:54,720
so it's setting up to make sure we

220
00:15:52,380 --> 00:15:56,760
actually block the recovery thread which

221
00:15:54,720 --> 00:15:59,300
we've just done so now we can continue

222
00:15:56,760 --> 00:15:59,300
execution

223
00:16:01,920 --> 00:16:09,540
okay so it's telling us it counted 56

224
00:16:06,600 --> 00:16:11,820
enlistments thanks to our go command that

225
00:16:09,540 --> 00:16:14,160
we touch many enlistments is we started

226
00:16:11,820 --> 00:16:16,680
index 55

227
00:16:14,160 --> 00:16:19,800
okay so that's good

228
00:16:16,680 --> 00:16:21,660
and now our latest enlistment should

229
00:16:19,800 --> 00:16:24,240
be free because we free not only this

230
00:16:21,660 --> 00:16:26,579
one but also many others more than 32 so

231
00:16:24,240 --> 00:16:28,500
it should be free not in the delayed

232
00:16:26,579 --> 00:16:31,380
free list anymore so now we can unblock

233
00:16:28,500 --> 00:16:33,660
the recovery thread and we can analyze a

234
00:16:31,380 --> 00:16:35,899
few instructions after for instance this

235
00:16:33,660 --> 00:16:35,899
address

236
00:16:41,100 --> 00:16:44,540
I'm gonna set the breakpoints

237
00:16:48,839 --> 00:16:56,820
and if we look at the okay

238
00:16:52,740 --> 00:16:59,759
address we had before it was 23A

239
00:16:56,820 --> 00:17:02,540
okay so what I'm going to do is I'm

240
00:16:59,759 --> 00:17:02,540
going to unpatch

241
00:17:04,559 --> 00:17:08,900
and then let's run and see what happens

242
00:17:09,839 --> 00:17:14,939
okay

243
00:17:11,400 --> 00:17:17,579
so now if we go back here we see that

244
00:17:14,939 --> 00:17:20,959
it's telling us okay did it work so

245
00:17:17,579 --> 00:17:20,959
let's go back to the debugger

246
00:17:22,260 --> 00:17:27,260
see where we are

247
00:17:25,140 --> 00:17:27,260
thank you

248
00:17:27,959 --> 00:17:34,880
so this call has returned and

249
00:17:31,980 --> 00:17:34,880
we are now

250
00:17:37,080 --> 00:17:44,340
here so on this CMP

251
00:17:39,960 --> 00:17:46,820
so it's testing the resource manager

252
00:17:44,340 --> 00:17:46,820
States

253
00:17:47,640 --> 00:17:52,520
okay so we're going to step

254
00:17:49,260 --> 00:17:52,520
slowly and see what happens

255
00:17:54,660 --> 00:18:00,140
this is our

256
00:17:57,000 --> 00:18:00,140
 manager

257
00:18:00,720 --> 00:18:03,679
as expected

258
00:18:21,960 --> 00:18:27,799
so now it's testing the B enlistment

259
00:18:24,960 --> 00:18:27,799
is finalized

260
00:18:29,400 --> 00:18:33,179
so obviously it's zero because we want

261
00:18:31,740 --> 00:18:35,340
the rates we trigger that we want the

262
00:18:33,179 --> 00:18:37,580
race so the flag is not set so that's

263
00:18:35,340 --> 00:18:37,580
good

264
00:18:37,740 --> 00:18:44,539
so it's going to continue and then here

265
00:18:40,500 --> 00:18:44,539
it's actually handling our

266
00:18:46,080 --> 00:18:50,100
pEnlistment shifted NextSameRM

267
00:18:49,140 --> 00:18:53,600
Flink

268
00:18:50,100 --> 00:18:53,600
so if you look at RDI

269
00:19:06,539 --> 00:19:11,700
ah nice ah great it worked

270
00:19:09,780 --> 00:19:14,220
so basically

271
00:19:11,700 --> 00:19:16,620
it's it's actually handing enlistment

272
00:19:14,220 --> 00:19:19,140
but you can see it's been replaced

273
00:19:16,620 --> 00:19:21,559
by a named pipe chunk so if you look at

274
00:19:19,140 --> 00:19:21,559
that chunk

275
00:19:22,620 --> 00:19:28,520
it's all controlled data and if we look

276
00:19:25,320 --> 00:19:28,520
from RDI

277
00:19:29,340 --> 00:19:34,559
it's pointing

278
00:19:30,960 --> 00:19:36,240
to that data we control so basically

279
00:19:34,559 --> 00:19:38,539
where it is right now it's gonna

280
00:19:36,240 --> 00:19:38,539
dereference

281
00:19:38,780 --> 00:19:47,120
RDI it's going to get 41 41 41 in RDI

282
00:19:43,260 --> 00:19:47,120
and then it's gonna Save that

283
00:19:49,380 --> 00:19:53,539
okay

284
00:19:50,760 --> 00:19:53,539
so now RDI

285
00:19:56,580 --> 00:20:02,160
is our controlled pointer

286
00:19:59,220 --> 00:20:03,960
and it's going to save that into

287
00:20:02,160 --> 00:20:07,220
pEnlistment shifted

288
00:20:03,960 --> 00:20:07,220
which is a local variable

289
00:20:08,880 --> 00:20:12,080
and now it's going to loop again

290
00:20:13,740 --> 00:20:19,280
from the beginning of this loop and now

291
00:20:15,720 --> 00:20:19,280
it's testing if RDI

292
00:20:22,080 --> 00:20:28,100
is equal to the head of the list

293
00:20:24,840 --> 00:20:28,100
which obviously it starts

294
00:20:29,100 --> 00:20:34,919
and look we are back to the beginning of

295
00:20:31,260 --> 00:20:38,280
the loop here it's actually retrieving

296
00:20:34,919 --> 00:20:40,200
RDI it's using RDI and it's actually

297
00:20:38,280 --> 00:20:43,980
getting the beginning of the enlistment

298
00:20:40,200 --> 00:20:46,080
from RDI by subtracting 88

299
00:20:43,980 --> 00:20:49,740
so now

300
00:20:46,080 --> 00:20:51,840
R14 is supposed to be the beginning of

301
00:20:49,740 --> 00:20:53,280
the enlistment but it's all wrong

302
00:20:51,840 --> 00:20:55,919
and now

303
00:20:53,280 --> 00:20:59,520
it's going to actually try to retrieve

304
00:20:55,919 --> 00:21:01,740
the flag of the enlistments

305
00:20:59,520 --> 00:21:04,080
or maybe it's gonna pass the enlistment

306
00:21:01,740 --> 00:21:06,260
to ObfReferenceObject

307
00:21:04,080 --> 00:21:06,260
and now it's going to crash

308
00:21:06,500 --> 00:21:12,679
because R14 plus AC is an invalid pointer

309
00:21:12,780 --> 00:21:17,480
boom

310
00:21:14,760 --> 00:21:17,480
we did it

