1
00:00:00,480 --> 00:00:06,299
so we want to add the code to actually

2
00:00:03,360 --> 00:00:08,580
do the feng shui with the named pipes so

3
00:00:06,299 --> 00:00:11,700
here we are filling the holes with

4
00:00:08,580 --> 00:00:14,460
actual buffers from the named pipe one

5
00:00:11,700 --> 00:00:18,000
and then this buffer will also be used

6
00:00:14,460 --> 00:00:20,820
when doing the alternate writes into the

7
00:00:18,000 --> 00:00:22,500
named pipes so the buffers are buffer

8
00:00:20,820 --> 00:00:25,160
one and buffer two so we need to

9
00:00:22,500 --> 00:00:25,160
initialize them

10
00:00:50,780 --> 00:00:57,480
so we allocate the buffer of size spray_len

11
00:00:54,539 --> 00:00:59,960
and then we're going to fill it with A's

12
00:00:57,480 --> 00:00:59,960
for instance

13
00:01:28,380 --> 00:01:33,020
uh we need to do the same for the second

14
00:01:30,420 --> 00:01:33,020
buffer

15
00:01:33,119 --> 00:01:39,259
and in this one we're going to just

16
00:01:35,220 --> 00:01:39,259
spray them with B's

17
00:01:46,500 --> 00:01:51,000
okay

18
00:01:47,939 --> 00:01:54,360
so here we're filling holes and now

19
00:01:51,000 --> 00:01:57,060
we're going to need to also

20
00:01:54,360 --> 00:02:00,540
alternate writes so we can use something

21
00:01:57,060 --> 00:02:02,880
similar to this code

22
00:02:00,540 --> 00:02:04,860
but instead of using the extra count

23
00:02:02,880 --> 00:02:06,899
we're going to use the actual count

24
00:02:04,860 --> 00:02:08,459
that is passed as an argument see the

25
00:02:06,899 --> 00:02:13,220
actual count here because that's the

26
00:02:08,459 --> 00:02:13,220
one we want to create holes for

27
00:02:16,140 --> 00:02:20,700
and we want to alternate the write so we

28
00:02:18,540 --> 00:02:24,319
want to do it for both then the first

29
00:02:20,700 --> 00:02:24,319
named pipe the second named pipe

30
00:02:35,099 --> 00:02:41,060
and so then we have code to actually

31
00:02:37,860 --> 00:02:41,060
create the holes

32
00:02:45,239 --> 00:02:49,200
and finally once the hole has been

33
00:02:47,099 --> 00:02:51,480
created we want to be able to reallocate

34
00:02:49,200 --> 00:02:53,099
the enlistments so if we go back to

35
00:02:51,480 --> 00:02:55,920
enlistments spray

36
00:02:53,099 --> 00:02:58,860
we had some code to actually

37
00:02:55,920 --> 00:03:02,060
allocate a bunch of enlistments

38
00:02:58,860 --> 00:03:02,060
that we can reuse

39
00:03:16,680 --> 00:03:24,140
okay so this will be allocated into

40
00:03:19,739 --> 00:03:24,140
enlistment set A

41
00:03:36,780 --> 00:03:42,060
from transaction number one

42
00:03:40,140 --> 00:03:44,040
because we only have one transaction now

43
00:03:42,060 --> 00:03:47,280
that we are using named pipes for the

44
00:03:44,040 --> 00:03:51,080
spray okay so now let's build the binary

45
00:03:47,280 --> 00:03:51,080
and push it onto the target VM

46
00:03:53,400 --> 00:03:57,480
so we have attached WinDbg to the

47
00:03:56,040 --> 00:04:00,540
target VM and now we're running the

48
00:03:57,480 --> 00:04:04,459
binary so we can set the breakpoint for

49
00:04:00,540 --> 00:04:04,459
the named pipe allocation

50
00:04:09,720 --> 00:04:15,959
so we make sure to reload

51
00:04:12,360 --> 00:04:19,820
the module to first putting the symbols

52
00:04:15,959 --> 00:04:19,820
we're getting the KPROCESS address

53
00:04:22,320 --> 00:04:25,139
and we're going to set the breakpoint on

54
00:04:24,300 --> 00:04:27,780
the

55
00:04:25,139 --> 00:04:29,820
specific address but just for that

56
00:04:27,780 --> 00:04:33,680
particular process

57
00:04:29,820 --> 00:04:33,680
should replay the KPROCESS address

58
00:04:37,020 --> 00:04:43,440
then we continue execution

59
00:04:40,320 --> 00:04:46,800
so we can see we have allocations for

60
00:04:43,440 --> 00:04:48,360
the named pipe writes

61
00:04:46,800 --> 00:04:50,780
so because it's quite verbose I'm just

62
00:04:48,360 --> 00:04:50,780
going to break

63
00:04:52,560 --> 00:04:56,180
and disable the breakpoint

64
00:05:03,120 --> 00:05:07,199
so now it's telling us that it just

65
00:05:04,979 --> 00:05:08,340
sprayed a bunch of named pipe to fill the

66
00:05:07,199 --> 00:05:12,860
holes

67
00:05:08,340 --> 00:05:12,860
so we can re-enable the breakpoint

68
00:05:23,460 --> 00:05:27,360
and then continue execution so here

69
00:05:25,440 --> 00:05:30,180
we're gonna hopefully have alternate

70
00:05:27,360 --> 00:05:33,139
types and actually we see that it failed

71
00:05:30,180 --> 00:05:33,139
interestingly

72
00:05:33,600 --> 00:05:39,600
so let's look at the actual error on the

73
00:05:36,600 --> 00:05:39,600
MSDN 998

74
00:06:02,039 --> 00:06:08,300
so it says invalid access to memory

75
00:06:04,199 --> 00:06:08,300
location so when we did the writes

76
00:06:09,780 --> 00:06:13,520
so if we go back to the code

77
00:06:21,419 --> 00:06:27,300
right so as you can see I forgot to

78
00:06:25,199 --> 00:06:30,240
replace buff1 with buff2 so basically

79
00:06:27,300 --> 00:06:33,479
what happened is when I filled

80
00:06:30,240 --> 00:06:35,100
the buffers I only filled buff1 and I

81
00:06:33,479 --> 00:06:37,620
only allocated buff1 in the first place

82
00:06:35,100 --> 00:06:39,600
so buff2 is a NULL pointer so here the

83
00:06:37,620 --> 00:06:41,220
write file is actually taking a NULL

84
00:06:39,600 --> 00:06:44,479
pointer as an argument so it's failing

85
00:06:41,220 --> 00:06:44,479
so I need to fix that

86
00:06:45,419 --> 00:06:50,720
to actually

87
00:06:47,819 --> 00:06:50,720
a buff2

88
00:06:54,419 --> 00:06:57,680
I'm just gonna rerun it

89
00:06:57,780 --> 00:07:02,759
so that that's a good example of having

90
00:07:00,259 --> 00:07:05,720
detections when it fails so you can

91
00:07:02,759 --> 00:07:05,720
actually debug the problem

92
00:07:15,419 --> 00:07:19,280
so I'm just going to rebuild the binary

93
00:07:25,699 --> 00:07:32,819
okay so I'm executing the binary now and

94
00:07:28,740 --> 00:07:36,680
it's telling me to set the breakpoint so

95
00:07:32,819 --> 00:07:36,680
I'm going to re-enable the breakpoint

96
00:07:37,620 --> 00:07:40,680
but actually it's enabled already so

97
00:07:39,660 --> 00:07:43,340
that's fine

98
00:07:40,680 --> 00:07:43,340
I'm gonna continue execution

99
00:07:44,039 --> 00:07:46,400


100
00:08:04,800 --> 00:08:08,880
so we have my debugging session is a bit

101
00:08:07,080 --> 00:08:12,440
slow

102
00:08:08,880 --> 00:08:12,440
and it's not printing anything

103
00:08:13,380 --> 00:08:16,639
looks like it's hanging

104
00:08:21,180 --> 00:08:24,900
I think it's possible it's because the

105
00:08:22,919 --> 00:08:26,940
breakpoint I have is set on the wrong

106
00:08:24,900 --> 00:08:29,340
KPROCESS now

107
00:08:26,940 --> 00:08:31,199
so basically I need to disable it and I

108
00:08:29,340 --> 00:08:35,459
need to redo

109
00:08:31,199 --> 00:08:35,459
the process command

110
00:08:37,800 --> 00:08:42,419
because obviously the

111
00:08:39,719 --> 00:08:45,020
KPROCESS is different because I reran

112
00:08:42,419 --> 00:08:45,020
the binary

113
00:08:45,959 --> 00:08:52,040
so now we can see it's printing the

114
00:08:48,720 --> 00:08:52,040
chunks addresses

115
00:08:55,740 --> 00:08:59,600
so I'm disabled breakpoint

116
00:09:08,220 --> 00:09:14,300
so now to fill the holes so I'm just

117
00:09:10,980 --> 00:09:14,300
gonna continue execution again

118
00:09:18,600 --> 00:09:23,100
okay so here one of the problem I have

119
00:09:20,580 --> 00:09:25,620
is that because I hit enter several

120
00:09:23,100 --> 00:09:28,380
times before left out he wasn't actually

121
00:09:25,620 --> 00:09:30,779
taking into account my keys he skipped

122
00:09:28,380 --> 00:09:34,019
the steps so I wasn't able to analyze

123
00:09:30,779 --> 00:09:35,760
the actual alternative named pipe so

124
00:09:34,019 --> 00:09:37,880
I'm just going to CTRL+C and do it

125
00:09:35,760 --> 00:09:37,880
again

126
00:09:52,140 --> 00:09:57,360
so I'm running the binary so I'm just

127
00:09:54,779 --> 00:09:59,220
going to skip analyzing the allocations

128
00:09:57,360 --> 00:10:02,040
to fill the holes but now I'm going to

129
00:09:59,220 --> 00:10:04,860
enable the breakpoints

130
00:10:02,040 --> 00:10:08,660
okay

131
00:10:04,860 --> 00:10:08,660
so I'm getting the KPROCESS address

132
00:10:13,620 --> 00:10:17,660
and I'm setting a breakpoint for the

133
00:10:15,060 --> 00:10:17,660
first two address

134
00:10:18,800 --> 00:10:22,279
continuing the execution

135
00:10:23,459 --> 00:10:28,459
and now I'm seeing the allocations for

136
00:10:25,740 --> 00:10:28,459
the named pipes

137
00:10:30,300 --> 00:10:33,680
so I'm just going to set

138
00:10:35,100 --> 00:10:37,640
timer

139
00:10:51,720 --> 00:10:57,200
so we can see that initially the named

140
00:10:53,279 --> 00:10:57,200
pip writes are not actually adjacent

141
00:11:07,700 --> 00:11:11,779
a few moments later

142
00:11:17,519 --> 00:11:23,160
so as you can see the addresses are not

143
00:11:20,459 --> 00:11:25,440
adjacent it's quite slow so I'm just

144
00:11:23,160 --> 00:11:29,120
going to disable the breakpoints and

145
00:11:25,440 --> 00:11:29,120
I'll analyze it after

146
00:11:36,839 --> 00:11:42,839
so it's really slow to

147
00:11:39,600 --> 00:11:45,260
answer to my break

148
00:11:42,839 --> 00:11:45,260
okay

149
00:11:54,779 --> 00:11:58,920
so now I'm back to

150
00:11:57,120 --> 00:12:00,300
the spray so here it's telling me

151
00:11:58,920 --> 00:12:03,660
that you just sprayed all the

152
00:12:00,300 --> 00:12:05,399
alternative named pipe writes so now I can

153
00:12:03,660 --> 00:12:08,240
break in the debugger and use the pool

154
00:12:05,399 --> 00:12:08,240
find command

155
00:12:10,380 --> 00:12:14,339
on the NpFr

156
00:12:13,200 --> 00:12:17,100
tag

157
00:12:14,339 --> 00:12:20,779
so before doing that actually make sure

158
00:12:17,100 --> 00:12:20,779
that your cache has a big value

159
00:12:23,519 --> 00:12:27,420
so here you can see the 1fffff so that's

160
00:12:26,100 --> 00:12:30,260
good

161
00:12:27,420 --> 00:12:30,260
so it's faster

162
00:12:38,279 --> 00:12:42,480
so we can see that we actually have

163
00:12:40,320 --> 00:12:44,040
adjacency for some of them

164
00:12:42,480 --> 00:12:45,540
which is good

165
00:12:44,040 --> 00:12:48,380
so I'm just going to wait so that it

166
00:12:45,540 --> 00:12:48,380
prints some of them

167
00:13:01,200 --> 00:13:07,139
a few moments later

168
00:13:04,139 --> 00:13:07,139
okay so i'm going to stop the poolfind

169
00:13:20,779 --> 00:13:23,959
command here

170
00:13:24,899 --> 00:13:28,820
after approximately 10 minutes

171
00:13:32,120 --> 00:13:37,820
so let's analyze

172
00:13:35,040 --> 00:13:37,820
the layout

173
00:13:41,519 --> 00:13:46,139
so we can see lots of named pipe writes

174
00:13:44,940 --> 00:13:48,920
chunks

175
00:13:46,139 --> 00:13:48,920
which is great

176
00:13:55,139 --> 00:13:57,980
again

177
00:14:03,660 --> 00:14:07,980
okay so that's good

178
00:14:06,000 --> 00:14:10,220
so if for instance if I take the first

179
00:14:07,980 --> 00:14:10,220
one

180
00:14:10,800 --> 00:14:16,339
if we analyze the actual data into that

181
00:14:13,440 --> 00:14:16,339
these chunks

182
00:14:17,399 --> 00:14:20,839
we can see B's

183
00:14:23,160 --> 00:14:28,860
so if we look at the actual size of

184
00:14:25,620 --> 00:14:33,779
these chunks we see that there are 250

185
00:14:28,860 --> 00:14:37,700
hex plus 250 gives 4a0 and so on

186
00:14:33,779 --> 00:14:37,700
so if we add 250

187
00:14:41,040 --> 00:14:47,220
so we have B's

188
00:14:44,820 --> 00:14:49,860
A's

189
00:14:47,220 --> 00:14:52,399
B's

190
00:14:49,860 --> 00:14:52,399
A's

191
00:14:53,279 --> 00:14:58,920
so we have a pretty good layout

192
00:14:56,519 --> 00:15:01,279
as you can see if we look at another

193
00:14:58,920 --> 00:15:01,279
area

194
00:15:02,519 --> 00:15:09,079
which was we looked at bd00

195
00:15:06,060 --> 00:15:09,079
if you look at

196
00:15:10,079 --> 00:15:13,339
the second area

197
00:15:23,399 --> 00:15:26,899
B's

198
00:15:25,079 --> 00:15:29,339
A's

199
00:15:26,899 --> 00:15:32,220
B's A's

200
00:15:29,339 --> 00:15:35,339
B's

201
00:15:32,220 --> 00:15:38,040
and so on so that's good so we have

202
00:15:35,339 --> 00:15:40,639
alternating named pipes so that's exactly

203
00:15:38,040 --> 00:15:40,639
what we want

204
00:15:41,639 --> 00:15:44,600
so then

205
00:15:44,639 --> 00:15:47,519
um

206
00:15:46,139 --> 00:15:49,380
what we're going to do is we're going to

207
00:15:47,519 --> 00:15:52,880
actually free them

208
00:15:49,380 --> 00:15:52,880
so we're going to continue execution

209
00:16:03,600 --> 00:16:06,779
okay so we're creating the holes so it's

210
00:16:05,459 --> 00:16:10,279
going to take a few seconds because

211
00:16:06,779 --> 00:16:10,279
we're creating many holes

212
00:16:10,680 --> 00:16:14,820
that's okay debugger

213
00:16:12,540 --> 00:16:16,440
and now we're done so we can see it's

214
00:16:14,820 --> 00:16:18,000
getting out that it should have created

215
00:16:16,440 --> 00:16:20,480
the holes so we're going to break in the

216
00:16:18,000 --> 00:16:20,480
debugger

217
00:16:20,940 --> 00:16:26,600
and we're going to reanalyze the

218
00:16:22,800 --> 00:16:26,600
sections that we analyzed earlier

219
00:16:27,899 --> 00:16:33,180
okay so as you can see we have an

220
00:16:30,600 --> 00:16:35,339
allocated chunk for an a named pipe then a

221
00:16:33,180 --> 00:16:36,959
free chunk, an allocated chunk, free check

222
00:16:35,339 --> 00:16:41,000
and so on so that's good that's what we

223
00:16:36,959 --> 00:16:41,000
wanted and if we look at another region

224
00:16:41,300 --> 00:16:44,779
like this one

225
00:16:45,120 --> 00:16:49,639
same thing we have allocated free

226
00:16:46,860 --> 00:16:49,639
allocated free

227
00:16:51,120 --> 00:16:54,540
okay

228
00:16:52,320 --> 00:16:57,300
so now what we're going to do is we're

229
00:16:54,540 --> 00:17:00,000
going to actually continue execution

230
00:16:57,300 --> 00:17:02,519
so we have our holes now so what we want

231
00:17:00,000 --> 00:17:04,439
is we want to see the reallocation of

232
00:17:02,519 --> 00:17:06,240
enlistments so we're going to use two

233
00:17:04,439 --> 00:17:08,280
breakpoints for that the first

234
00:17:06,240 --> 00:17:09,959
breakpoint is going to be to see the

235
00:17:08,280 --> 00:17:11,939
allocated enlistments and the second

236
00:17:09,959 --> 00:17:15,919
one is to see the allocated notification

237
00:17:11,939 --> 00:17:15,919
which is the noise we want to avoid

238
00:17:23,220 --> 00:17:26,819
okay

239
00:17:24,959 --> 00:17:30,980
so we'll continue execution and now

240
00:17:26,819 --> 00:17:30,980
we're going to reallocate the enlistments

241
00:17:31,620 --> 00:17:36,960
so we can see for each enlistment

242
00:17:33,780 --> 00:17:39,360
allocated there is a notification as well

243
00:17:36,960 --> 00:17:41,780
so we're just gonna start

244
00:17:39,360 --> 00:17:41,780
a timer

245
00:17:51,120 --> 00:17:53,240
yeah

246
00:17:54,000 --> 00:17:58,320
so actually I'm going to put the timer

247
00:17:55,679 --> 00:18:00,179
and just break the debugger

248
00:17:58,320 --> 00:18:04,039
and then what I want to see is I want to

249
00:18:00,179 --> 00:18:04,039
see where the enlistment is allocated

250
00:18:08,400 --> 00:18:13,020
so that's good because we can see

251
00:18:10,140 --> 00:18:15,500
the enlistment was allocated just after a

252
00:18:13,020 --> 00:18:19,220
named pipe so that's pretty good

253
00:18:15,500 --> 00:18:19,220
and the notification

254
00:18:21,840 --> 00:18:26,640
is in a totally different region where

255
00:18:24,299 --> 00:18:29,220
there is no named pipe and no

256
00:18:26,640 --> 00:18:32,039
KENLISTMENTs

257
00:18:29,220 --> 00:18:35,240
so that's good sign as well

258
00:18:32,039 --> 00:18:35,240
I'll take another one

259
00:18:40,880 --> 00:18:45,840
again the enlistment is close to an named

260
00:18:43,500 --> 00:18:50,640
pipe okay so I'm just going to disable

261
00:18:45,840 --> 00:18:50,640
the breakpoint where they are quite slow

262
00:18:52,740 --> 00:18:56,900
um

263
00:18:53,640 --> 00:18:56,900
we're gonna continue execution

264
00:18:57,960 --> 00:19:02,220
so now it's telling me that it has

265
00:18:59,520 --> 00:19:04,740
reallocated all the KENLISTMENTs so I'm

266
00:19:02,220 --> 00:19:07,400
gonna break in the debugger and I'm

267
00:19:04,740 --> 00:19:07,400
gonna use the

268
00:19:08,100 --> 00:19:11,299
poolfind command

269
00:19:11,640 --> 00:19:16,340
but this time we have TmEn

270
00:19:48,240 --> 00:19:52,039
a few moments later

271
00:20:00,919 --> 00:20:08,660
okay so I'm gonna break here

272
00:20:04,679 --> 00:20:08,660
to see what we have

273
00:20:09,240 --> 00:20:12,919
after almost four minutes

274
00:20:12,960 --> 00:20:15,740
so

275
00:20:17,700 --> 00:20:25,080
we saw earlier that we actually had Tm

276
00:20:21,539 --> 00:20:28,520
enlistments close to NpFr chunks

277
00:20:25,080 --> 00:20:28,520
let's reanalyze them

278
00:20:30,539 --> 00:20:35,640
so more specifically where we had lots

279
00:20:33,179 --> 00:20:39,080
of calls

280
00:20:35,640 --> 00:20:39,080
so it's like the addresses

281
00:20:41,160 --> 00:20:45,980
4b0

282
00:20:43,140 --> 00:20:45,980
and so on

283
00:20:52,280 --> 00:20:58,320
so we can see enlistment close

284
00:20:55,320 --> 00:21:00,539
to an NpFr chunk

285
00:20:58,320 --> 00:21:02,520
so we can see also that not all the free

286
00:21:00,539 --> 00:21:04,679
chunks have been reallocated with enlistment

287
00:21:02,520 --> 00:21:08,299
but at least the Tm enlistment is close

288
00:21:04,679 --> 00:21:08,299
to a named pipe chunk

289
00:21:09,720 --> 00:21:14,480
so I'm gonna

290
00:21:11,880 --> 00:21:14,480
analyze

291
00:21:20,400 --> 00:21:22,700
the other area

292
00:21:28,080 --> 00:21:40,525
so again we see an enlistment

293
00:21:40,760 --> 00:21:45,780
so here it's close to a free chunk

294
00:21:42,900 --> 00:21:48,000
actually which is not great because it

295
00:21:45,780 --> 00:21:50,280
means these two changes will coalesce

296
00:21:48,000 --> 00:21:52,520
but at least the chunch is after so if we

297
00:21:50,280 --> 00:21:52,520
actually

298
00:21:53,760 --> 00:21:57,080
look just before that

299
00:22:04,159 --> 00:22:09,120
we'll see another

300
00:22:06,480 --> 00:22:10,740
layout which is not tests so let's

301
00:22:09,120 --> 00:22:15,020
analyze now

302
00:22:10,740 --> 00:22:15,020
some allocated KENLISTMENTs

303
00:22:18,659 --> 00:22:22,980
so we actually see it's between free

304
00:22:20,520 --> 00:22:25,940
chunks which is not ideal

305
00:22:22,980 --> 00:22:25,940
we look at other

306
00:22:30,360 --> 00:22:34,080
so here we have allocated enlistment

307
00:22:32,520 --> 00:22:37,880
close to each other which is not ideal

308
00:22:34,080 --> 00:22:37,880
except if we only free one of them

309
00:22:46,200 --> 00:22:51,900
and here we have and enlistment close to

310
00:22:48,539 --> 00:22:54,179
named pipe so it's it's okay-ish it's not

311
00:22:51,900 --> 00:22:56,580
the perfect feng shui layout but it's

312
00:22:54,179 --> 00:23:00,140
it's starting to make sense

313
00:22:56,580 --> 00:23:00,140
so let's continue execution now

314
00:23:01,980 --> 00:23:09,059
so one thing we can do maybe to improve

315
00:23:05,700 --> 00:23:13,380
the layout is to spray more

316
00:23:09,059 --> 00:23:15,360
extra chunks before we actually

317
00:23:13,380 --> 00:23:16,820
allocate the feng shui so here I'm gonna

318
00:23:15,360 --> 00:23:19,919
add

319
00:23:16,820 --> 00:23:21,840
1000 just to fill all the holes

320
00:23:19,919 --> 00:23:23,960
beforehand and I'm going to rebuild the

321
00:23:21,840 --> 00:23:23,960
binary

322
00:23:38,940 --> 00:23:43,799
so now I'm going to restart the binary

323
00:23:41,580 --> 00:23:44,880
so be because we know most of it works

324
00:23:43,799 --> 00:23:47,520
and that I'm not going to set

325
00:23:44,880 --> 00:23:50,220
breakpoints at the moment I'm just gonna

326
00:23:47,520 --> 00:23:52,919
do this allocations

327
00:23:50,220 --> 00:23:57,659
freeing

328
00:23:52,919 --> 00:23:59,640
the chunks and only when reallocating the

329
00:23:57,659 --> 00:24:01,820
KENLISTMENT because I'm just gonna look at

330
00:23:59,640 --> 00:24:01,820
them

331
00:24:04,100 --> 00:24:10,679
okay so now we should have holes

332
00:24:07,260 --> 00:24:14,100
so if I go back in the debugger

333
00:24:10,679 --> 00:24:17,460
and look at my break points

334
00:24:14,100 --> 00:24:20,360
we see I want to re-enable the create

335
00:24:17,460 --> 00:24:20,360
enlistment

336
00:24:22,860 --> 00:24:25,880
and notification

337
00:24:30,059 --> 00:24:34,400
so now I should see where they are

338
00:24:32,220 --> 00:24:34,400
located

339
00:24:36,600 --> 00:24:41,419
so I'm just going to break the debugger

340
00:24:42,240 --> 00:24:46,740
it may take a bit of time to actually

341
00:24:44,520 --> 00:24:50,840
handle the break

342
00:24:46,740 --> 00:24:50,840
so now if I look at one of the address

343
00:24:56,520 --> 00:25:03,140
oh nice so we have enlistment, named pipe, enlistment,

344
00:25:00,120 --> 00:25:03,140
named pipe

345
00:25:10,620 --> 00:25:14,480
again an enlistment, named pipe,

346
00:25:12,600 --> 00:25:17,460
enlistment, named pipe so basically just by

347
00:25:14,480 --> 00:25:20,880
increasing filling the holes I was able

348
00:25:17,460 --> 00:25:24,000
to get a way better layout which is

349
00:25:20,880 --> 00:25:26,039
pretty interesting and if we look back

350
00:25:24,000 --> 00:25:29,659
at the actual

351
00:25:26,039 --> 00:25:29,659
notification where they are located

352
00:25:37,080 --> 00:25:40,860
they are in totally different region

353
00:25:39,120 --> 00:25:42,840
without any

354
00:25:40,860 --> 00:25:44,940
KENLISTMENTs

355
00:25:42,840 --> 00:25:47,039
okay great

356
00:25:44,940 --> 00:25:50,220
so this is this kind of

357
00:25:47,039 --> 00:25:52,380
results were just seeing this is pretty

358
00:25:50,220 --> 00:25:54,360
neat and it's going to allow us to

359
00:25:52,380 --> 00:25:57,380
continue working on the exploit okay

360
00:25:54,360 --> 00:25:57,380
thank you for watching

