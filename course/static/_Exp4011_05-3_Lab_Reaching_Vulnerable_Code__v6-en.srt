1
00:00:02,240 --> 00:00:09,120
hi everyone the goal of this lab is

2
00:00:05,759 --> 00:00:11,940
going to reach the vulnerable code in

3
00:00:09,120 --> 00:00:13,980
the TmRecoverResourceExt

4
00:00:11,940 --> 00:00:16,619
function so the first thing we're going

5
00:00:13,980 --> 00:00:19,380
to need to do is we're going to need to

6
00:00:16,619 --> 00:00:21,840
see how we can enter this function which

7
00:00:19,380 --> 00:00:24,300
is basically how to recover without even

8
00:00:21,840 --> 00:00:28,019
trying to win any race to trigger any

9
00:00:24,300 --> 00:00:30,300
crash just reaching this recovery state

10
00:00:28,019 --> 00:00:33,660
once we have reached this function the

11
00:00:30,300 --> 00:00:35,820
second goal is going to be to reach the

12
00:00:33,660 --> 00:00:38,700
vulnerable path in the function so we're

13
00:00:35,820 --> 00:00:40,920
going to need to have specific states

14
00:00:38,700 --> 00:00:42,840
for both the enlistments and the

15
00:00:40,920 --> 00:00:46,739
transaction and the last thing we want

16
00:00:42,840 --> 00:00:49,800
to have is to be able to track what

17
00:00:46,739 --> 00:00:51,899
happens from userland okay let's get

18
00:00:49,800 --> 00:00:54,360
started so the userland function we're

19
00:00:51,899 --> 00:00:56,640
interested in is recover resource

20
00:00:54,360 --> 00:00:59,280
manager and it allows for reaching the

21
00:00:56,640 --> 00:01:01,620
TmRecoverResourceManagerExt function

22
00:00:59,280 --> 00:01:04,680
that is the vulnerable function through the

23
00:01:01,620 --> 00:01:06,240
NtRecoverResourceManager syscall

24
00:01:04,680 --> 00:01:09,420
once we have reached the variable

25
00:01:06,240 --> 00:01:11,939
function we want to get the

26
00:01:09,420 --> 00:01:14,220
bSendNotification flag set so one of the

27
00:01:11,939 --> 00:01:16,320
option that we didn't show previously to

28
00:01:14,220 --> 00:01:18,360
simplify is to have a superior

29
00:01:16,320 --> 00:01:20,640
enlistments and a transaction in a

30
00:01:18,360 --> 00:01:22,680
specific state and originally we tried

31
00:01:20,640 --> 00:01:25,740
to do it with the superior enlistment

32
00:01:22,680 --> 00:01:27,960
for no particular reasons and it ended

33
00:01:25,740 --> 00:01:30,840
up causing lots of problems for instance

34
00:01:27,960 --> 00:01:33,659
it would require logs to be written on

35
00:01:30,840 --> 00:01:36,659
disk and then logs would be full quite

36
00:01:33,659 --> 00:01:38,579
quickly and we were not able to do it

37
00:01:36,659 --> 00:01:40,979
this way so we went this other route

38
00:01:38,579 --> 00:01:43,320
which is basically to have a regular

39
00:01:40,979 --> 00:01:45,900
enlistment which is not Superior and get

40
00:01:43,320 --> 00:01:48,420
the transaction into a prepared state

41
00:01:45,900 --> 00:01:51,180
and we chose the prepare state because

42
00:01:48,420 --> 00:01:54,060
we didn't know what an in doubt state was

43
00:01:51,180 --> 00:01:56,280
and it sounded more complicated and also

44
00:01:54,060 --> 00:01:58,920
because we encountered the prepared

45
00:01:56,280 --> 00:02:01,079
state already and the committed state is

46
00:01:58,920 --> 00:02:04,140
just later on in the process and so the

47
00:02:01,079 --> 00:02:06,180
prepared state seemed easier to reach so

48
00:02:04,140 --> 00:02:08,940
basically then the question is how do we

49
00:02:06,180 --> 00:02:11,760
get a transaction in the KTransactionPrepared

50
00:02:08,940 --> 00:02:14,099
state we have kind of touched

51
00:02:11,760 --> 00:02:16,800
this a little bit in the KTM experiments

52
00:02:14,099 --> 00:02:19,319
lab previously but we'll look at it more

53
00:02:16,800 --> 00:02:22,560
deeply now basically there there is this

54
00:02:19,319 --> 00:02:24,900
function called TmpProcessNotificationResponse

55
00:02:22,560 --> 00:02:27,300
that you encounter many

56
00:02:24,900 --> 00:02:30,120
times if you reverse engineer the KTM

57
00:02:27,300 --> 00:02:32,580
driver and so anytime state transition

58
00:02:30,120 --> 00:02:34,739
stuff happens this function would get

59
00:02:32,580 --> 00:02:37,980
called the main thing is that there is a

60
00:02:34,739 --> 00:02:40,500
callback passed as well as two arrays

61
00:02:37,980 --> 00:02:42,480
that dictates the states that the

62
00:02:40,500 --> 00:02:45,000
enlistments or transaction may be in

63
00:02:42,480 --> 00:02:48,540
and then what happens possibly is that

64
00:02:45,000 --> 00:02:52,140
the callback is called and so we know

65
00:02:48,540 --> 00:02:55,319
that calling pre-prepare complete or

66
00:02:52,140 --> 00:02:57,360
prepare complete or commit complete from

67
00:02:55,319 --> 00:02:59,760
userland will change the state of

68
00:02:57,360 --> 00:03:02,459
enlistments so it can be assumed that

69
00:02:59,760 --> 00:03:04,980
that will also transition the state of

70
00:03:02,459 --> 00:03:07,920
the underneath transaction as well and

71
00:03:04,980 --> 00:03:09,900
we confirm that in the WinDbg debugger

72
00:03:07,920 --> 00:03:12,000
during a previous lab and so if you look

73
00:03:09,900 --> 00:03:14,879
at the implementation in kernel of such

74
00:03:12,000 --> 00:03:16,860
call like TmPrepareComplete that gets

75
00:03:14,879 --> 00:03:19,860
called on the kernel side when calling

76
00:03:16,860 --> 00:03:23,280
prepare complete in userland what you

77
00:03:19,860 --> 00:03:26,340
see is some of the arguments get set up

78
00:03:23,280 --> 00:03:28,680
in a specific way to call this

79
00:03:26,340 --> 00:03:30,120
TmpProcessNotificationResponse which is

80
00:03:28,680 --> 00:03:32,580
basically the function we've just

81
00:03:30,120 --> 00:03:36,420
mentioned and in this case the callback

82
00:03:32,580 --> 00:03:39,120
is TmpTxActionDoPrepareComplete

83
00:03:36,420 --> 00:03:41,400
where TX stands for transaction and it

84
00:03:39,120 --> 00:03:44,280
is basically saying if all of the

85
00:03:41,400 --> 00:03:46,860
enlistments states are in the preparing

86
00:03:44,280 --> 00:03:49,500
state it will flip them all to the

87
00:03:46,860 --> 00:03:52,140
prepared state afterwards so this is the

88
00:03:49,500 --> 00:03:55,440
states beforehand and this is the state

89
00:03:52,140 --> 00:03:58,080
after and what ends up happening is this

90
00:03:55,440 --> 00:04:00,780
callback is called which at first when

91
00:03:58,080 --> 00:04:03,120
you read this code it might not be

92
00:04:00,780 --> 00:04:05,220
obvious but after for a bit of reversing

93
00:04:03,120 --> 00:04:07,140
and debugging you understand this is

94
00:04:05,220 --> 00:04:09,420
what is happening and this is just to

95
00:04:07,140 --> 00:04:12,540
show the list of functions being called

96
00:04:09,420 --> 00:04:15,239
from the prepare complete API in

97
00:04:12,540 --> 00:04:17,160
userland which calls into the NtPrepareComplete

98
00:04:15,239 --> 00:04:19,500
syscall that then ended up

99
00:04:17,160 --> 00:04:21,840
calling the TmPrepareComplete function

100
00:04:19,500 --> 00:04:24,180
we just talked about this slide is to

101
00:04:21,840 --> 00:04:25,860
show the actual callback function that

102
00:04:24,180 --> 00:04:28,139
ends up being called when all the

103
00:04:25,860 --> 00:04:30,600
enlistments are in the preparing state

104
00:04:28,139 --> 00:04:32,580
meaning that they can transition to the

105
00:04:30,600 --> 00:04:35,100
prepared state and we see in the cut

106
00:04:32,580 --> 00:04:37,860
snippet that the transaction also

107
00:04:35,100 --> 00:04:39,660
transitions to the prepared state we

108
00:04:37,860 --> 00:04:43,199
know what the flags are from the public

109
00:04:39,660 --> 00:04:46,020
enum and so since we reversed a bunch of

110
00:04:43,199 --> 00:04:48,479
KTM functions and we have updated all of

111
00:04:46,020 --> 00:04:50,820
the structures we can find references to

112
00:04:48,479 --> 00:04:53,100
flags and stuff like this and when we

113
00:04:50,820 --> 00:04:56,160
realize that we need to find some code

114
00:04:53,100 --> 00:04:58,380
that sets certain flags it's easy to

115
00:04:56,160 --> 00:05:01,259
find so this is just an example of the

116
00:04:58,380 --> 00:05:03,180
payoff of reversing a whole ton of code

117
00:05:01,259 --> 00:05:05,520
that you might not realized from the

118
00:05:03,180 --> 00:05:07,259
get-go is going to be useful but because

119
00:05:05,520 --> 00:05:09,720
you have no other options just

120
00:05:07,259 --> 00:05:11,820
annotating stuff is always good for the

121
00:05:09,720 --> 00:05:13,979
future and so basically from userland in

122
00:05:11,820 --> 00:05:16,199
general we are trying to trigger this

123
00:05:13,979 --> 00:05:18,000
straight state transition and so as

124
00:05:16,199 --> 00:05:20,460
mentioned previously a bunch of the

125
00:05:18,000 --> 00:05:23,039
public states associated with both the

126
00:05:20,460 --> 00:05:24,539
enlistments and transactions exist and

127
00:05:23,039 --> 00:05:27,300
there is a match between the

128
00:05:24,539 --> 00:05:29,759
KENLISTMENT_STATE and the

129
00:05:27,300 --> 00:05:32,160
KTRANSACTION_STATE since all of the enlistments

130
00:05:29,759 --> 00:05:34,500
associated with the transaction range a

131
00:05:32,160 --> 00:05:37,020
certain state and they all have that

132
00:05:34,500 --> 00:05:39,660
similar state it allows the transaction

133
00:05:37,020 --> 00:05:41,400
to move to the next state as well so we

134
00:05:39,660 --> 00:05:43,500
know that we want to trigger a recovery

135
00:05:41,400 --> 00:05:46,139
of a resource manager with a transaction

136
00:05:43,500 --> 00:05:48,720
in the prepared state and so in order to

137
00:05:46,139 --> 00:05:50,520
do that we have a vague idea of how to

138
00:05:48,720 --> 00:05:52,979
proceed we start by creating a

139
00:05:50,520 --> 00:05:55,380
transaction we create two enlistments

140
00:05:52,979 --> 00:05:58,320
associated with that transaction just so

141
00:05:55,380 --> 00:06:00,360
that if we do hit the recovery code we

142
00:05:58,320 --> 00:06:01,800
can watch it step through the linked

143
00:06:00,360 --> 00:06:04,740
list of more than just one Enlistment

144
00:06:01,800 --> 00:06:06,840
moments then we call the function as

145
00:06:04,740 --> 00:06:08,940
usual about committing to do the

146
00:06:06,840 --> 00:06:12,120
transaction we transition both of the

147
00:06:08,940 --> 00:06:14,820
enlistments to being pre-prepared so the

148
00:06:12,120 --> 00:06:17,220
transaction is also changed to being

149
00:06:14,820 --> 00:06:19,740
pre-prepared then we transition both of

150
00:06:17,220 --> 00:06:22,139
the enlistment to being prepared so the

151
00:06:19,740 --> 00:06:24,060
transaction is also changed to being

152
00:06:22,139 --> 00:06:26,520
prepared and that should be enough to

153
00:06:24,060 --> 00:06:28,620
trigger the path we are interested in in

154
00:06:26,520 --> 00:06:30,720
the TmRecoverResourceManager in

155
00:06:28,620 --> 00:06:33,000
userland we saw code in the Visual Studio

156
00:06:30,720 --> 00:06:35,100
lab that will output the state

157
00:06:33,000 --> 00:06:37,259
transition we can basically read the

158
00:06:35,100 --> 00:06:38,940
notification from userland that

159
00:06:37,259 --> 00:06:40,560
correlate to the code in the kernel in

160
00:06:38,940 --> 00:06:43,560
the recovery loop that we looked at

161
00:06:40,560 --> 00:06:46,139
earlier related to the bSendNotification

162
00:06:43,560 --> 00:06:48,660
flag so when that loop processes one

163
00:06:46,139 --> 00:06:50,819
enlistment and sends a notification you

164
00:06:48,660 --> 00:06:52,440
can read out the notification afterwards

165
00:06:50,819 --> 00:06:54,900
in userland and see that the

166
00:06:52,440 --> 00:06:56,819
vulnerable function hits the path we are

167
00:06:54,900 --> 00:06:59,759
interested in and this will look like

168
00:06:56,819 --> 00:07:02,160
this output that we can see from

169
00:06:59,759 --> 00:07:04,560
userland and we previously saw that in

170
00:07:02,160 --> 00:07:06,840
the labs so in this lab the goal is to

171
00:07:04,560 --> 00:07:08,819
confirm we can trigger the vulnerable

172
00:07:06,840 --> 00:07:11,460
code that sends a notification for the

173
00:07:08,819 --> 00:07:13,560
prepared state and so here we don't want

174
00:07:11,460 --> 00:07:15,660
to commit the enlistments as we want

175
00:07:13,560 --> 00:07:18,120
them to still be in the prepared state

176
00:07:15,660 --> 00:07:20,819
and then we want to add a call to

177
00:07:18,120 --> 00:07:23,400
recover resource manager in userland and

178
00:07:20,819 --> 00:07:25,080
so when the kernel code hits the

179
00:07:23,400 --> 00:07:26,759
TmRecoverResourceManagerExt

180
00:07:25,080 --> 00:07:28,560
vulnerable function the state of the

181
00:07:26,759 --> 00:07:30,840
enlistments being in the prepared

182
00:07:28,560 --> 00:07:32,580
state will make it set the

183
00:07:30,840 --> 00:07:34,860
bSendNotification flag and so the

184
00:07:32,580 --> 00:07:36,960
notifications should be sent to userland

185
00:07:34,860 --> 00:07:39,180
and so to monitor that in the debugger

186
00:07:36,960 --> 00:07:41,639
we'll have to set the breakpoint on the

187
00:07:39,180 --> 00:07:44,819
vulnerable function TmRecoverResourceManagerExt

188
00:07:41,639 --> 00:07:47,699
as well as the function named

189
00:07:44,819 --> 00:07:50,400
TmpSetNotificationResourceManager

190
00:07:47,699 --> 00:07:52,860
that is called by the first one when a

191
00:07:50,400 --> 00:07:55,620
notification is sent and so even though

192
00:07:52,860 --> 00:07:57,900
the main goal is to reach the vulnerable

193
00:07:55,620 --> 00:08:00,180
path in this function it's good to keep

194
00:07:57,900 --> 00:08:01,680
in mind future goals because while

195
00:08:00,180 --> 00:08:04,440
you're stepping and understanding the

196
00:08:01,680 --> 00:08:06,599
code better it's easier to solve future

197
00:08:04,440 --> 00:08:08,460
problems so another thing we're going to

198
00:08:06,599 --> 00:08:11,099
need is to be able to finalize an

199
00:08:08,460 --> 00:08:12,660
enlistment while it's in the loop and

200
00:08:11,099 --> 00:08:14,340
trying to recovering the resource

201
00:08:12,660 --> 00:08:16,860
manager just because at some point we're

202
00:08:14,340 --> 00:08:19,139
going to have to free one enlistment so

203
00:08:16,860 --> 00:08:20,940
a stale pointer is used and we trigger

204
00:08:19,139 --> 00:08:23,099
use after free and the second thing

205
00:08:20,940 --> 00:08:25,500
we're going to need to solve is how do

206
00:08:23,099 --> 00:08:28,500
we replace the memory that was

207
00:08:25,500 --> 00:08:31,139
previously a KENLISTMENT to be replaced

208
00:08:28,500 --> 00:08:33,360
with controlled data so we can abuse the

209
00:08:31,139 --> 00:08:36,240
use after free so in Visual Studio you

210
00:08:33,360 --> 00:08:37,800
can open the TriggerVulnCode.c and

211
00:08:36,240 --> 00:08:39,479
you'll see quickly that it's very

212
00:08:37,800 --> 00:08:42,300
similar to the previous lab

213
00:08:39,479 --> 00:08:45,899
CommitMultiEnTx so you have the similar

214
00:08:42,300 --> 00:08:47,940
helper functions and the instructions of

215
00:08:45,899 --> 00:08:50,160
the lab is to add a call to prepare

216
00:08:47,940 --> 00:08:52,500
complete for both enlistments and to add

217
00:08:50,160 --> 00:08:56,120
a call to recover resource manager and

218
00:08:52,500 --> 00:08:56,120
to break on these two functions

219
00:08:59,240 --> 00:09:03,740
we're going to work from now on with the

220
00:09:02,580 --> 00:09:06,660
latest

221
00:09:03,740 --> 00:09:09,540
tm_vuln.sys just because we are going to use

222
00:09:06,660 --> 00:09:12,779
all the annotations that we provide so

223
00:09:09,540 --> 00:09:15,560
you can focus on the actual exploitation

224
00:09:12,779 --> 00:09:15,560
of vulnerability

225
00:09:15,600 --> 00:09:21,180
you can also load the

226
00:09:17,880 --> 00:09:23,160
ntoskrnl.exe corresponding to the

227
00:09:21,180 --> 00:09:26,399
latest version

228
00:09:23,160 --> 00:09:28,380
and you'll notice lots of bookmarks in

229
00:09:26,399 --> 00:09:30,779
the actual file that you can easily

230
00:09:28,380 --> 00:09:32,700
access the functions for the future labs

231
00:09:30,779 --> 00:09:34,920
for instance we are interested at the

232
00:09:32,700 --> 00:09:36,959
moment is the TmRecoverResourceManagerExt

233
00:09:34,920 --> 00:09:39,300
function and you'll see that it's all

234
00:09:36,959 --> 00:09:43,080
annotated and you'll see the

235
00:09:39,300 --> 00:09:45,600
bSendNotification flag so we see the that if

236
00:09:43,080 --> 00:09:49,339
if the enlistment is not finalized it's

237
00:09:45,600 --> 00:09:49,339
going to go into that if condition

238
00:09:50,040 --> 00:09:56,100
and if certain flags are set for the

239
00:09:53,820 --> 00:10:00,800
transaction like if it's in a prepared

240
00:09:56,100 --> 00:10:00,800
state it can actually set the flag

241
00:10:01,680 --> 00:10:06,120
and then if this flag is set it's going

242
00:10:04,200 --> 00:10:07,860
to go into this if loop and it's going

243
00:10:06,120 --> 00:10:09,540
to call the TmpSetNotificationResourceManager

244
00:10:07,860 --> 00:10:12,000
function we want to

245
00:10:09,540 --> 00:10:13,920
reach so really the goal is to modify

246
00:10:12,000 --> 00:10:16,800
the code to actually first hit this

247
00:10:13,920 --> 00:10:20,459
function but also make sure you enter in

248
00:10:16,800 --> 00:10:23,459
the if condition and that the flag

249
00:10:20,459 --> 00:10:25,080
bSendNotification is actually set to true and

250
00:10:23,459 --> 00:10:29,660
then that you hit this function call

251
00:10:25,080 --> 00:10:29,660
okay now it's your turn

