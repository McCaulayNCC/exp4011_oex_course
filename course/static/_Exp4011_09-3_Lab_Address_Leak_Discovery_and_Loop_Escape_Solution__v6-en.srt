1
00:00:00,780 --> 00:00:06,359
okay so we have pushed the escaping loop

2
00:00:03,540 --> 00:00:08,040
binary onto the tag NDM and for now we

3
00:00:06,359 --> 00:00:11,160
haven't modified it because we know it

4
00:00:08,040 --> 00:00:13,080
has the code to actually trigger the

5
00:00:11,160 --> 00:00:14,639
spray enlistment the leak enlistment and

6
00:00:13,080 --> 00:00:16,320
then the Trap enlistment and we know the

7
00:00:14,639 --> 00:00:18,960
leak enlistment will allow us to

8
00:00:16,320 --> 00:00:20,880
leak data so we want to be analyzing the

9
00:00:18,960 --> 00:00:23,699
leak enlistment being touched by the

10
00:00:20,880 --> 00:00:25,260
kernel so now we have reached the state

11
00:00:23,699 --> 00:00:27,980
where it's actually telling us to set

12
00:00:25,260 --> 00:00:27,980
breakpoints

13
00:00:31,500 --> 00:00:37,020
so we're going to add the two break

14
00:00:33,899 --> 00:00:39,920
points I'm going to continue execution

15
00:00:37,020 --> 00:00:39,920
I'm going to hit enter

16
00:00:41,399 --> 00:00:46,440
so now we reach the vulnerable function

17
00:00:44,579 --> 00:00:49,800
we're going to continue execution

18
00:00:46,440 --> 00:00:51,899
and I will hit the function being called

19
00:00:49,800 --> 00:00:56,960
tmpsetnotification so we are going

20
00:00:51,899 --> 00:00:56,960
to skip this function at least 32 times

21
00:00:58,440 --> 00:01:02,239
a few moments later

22
00:01:03,059 --> 00:01:10,580
okay so now I'm gonna patch the code so

23
00:01:06,360 --> 00:01:10,580
so the recovery thread is stuck

24
00:01:15,780 --> 00:01:19,100
and continue execution

25
00:01:20,340 --> 00:01:25,080
okay so now the recovery thread is stuck

26
00:01:22,500 --> 00:01:27,680
in the kernel so we can continue

27
00:01:25,080 --> 00:01:27,680
execution

28
00:01:29,820 --> 00:01:35,460
okay so now it's telling us that

29
00:01:33,240 --> 00:01:37,680
we should unblock the recovery thread

30
00:01:35,460 --> 00:01:41,060
and then analyze what is happening to

31
00:01:37,680 --> 00:01:41,060
the leak enlistment

32
00:01:49,020 --> 00:01:52,939
so now I'm going to unpatch the memory

33
00:01:56,700 --> 00:02:01,259
and now I'm going to disable these two

34
00:01:58,979 --> 00:02:03,899
breakpoints and I'm gonna actually

35
00:02:01,259 --> 00:02:07,439
enable the breakpoint just after the

36
00:02:03,899 --> 00:02:09,239
recovery thread has been unblocked

37
00:02:07,439 --> 00:02:11,940
and I'm not going to use the actual

38
00:02:09,239 --> 00:02:14,340
Superior breakpoints because I want to

39
00:02:11,940 --> 00:02:16,739
be analyzing the enlistments before

40
00:02:14,340 --> 00:02:20,360
it's touched by anything and then see

41
00:02:16,739 --> 00:02:20,360
what actually modifies it

42
00:02:26,099 --> 00:02:31,700
okay continue execution

43
00:02:29,040 --> 00:02:31,700
okay

44
00:02:33,480 --> 00:02:37,459
so now we are here

45
00:02:40,920 --> 00:02:46,140
so we are just after

46
00:02:43,620 --> 00:02:49,099
and we're going to use the F10 key in

47
00:02:46,140 --> 00:02:49,099
order to stack

48
00:03:03,300 --> 00:03:08,580
so sometimes the decompiler output is

49
00:03:05,819 --> 00:03:12,440
not actually synchronized correctly

50
00:03:08,580 --> 00:03:12,440
so we are here

51
00:03:27,360 --> 00:03:32,879
okay so now we've passed the instruction

52
00:03:30,239 --> 00:03:36,900
that actually updates rdi

53
00:03:32,879 --> 00:03:40,019
so now if we look at rdi

54
00:03:36,900 --> 00:03:41,220
okay so it's rdi is pointing to our userland

55
00:03:40,019 --> 00:03:43,620
enlistment

56
00:03:41,220 --> 00:03:47,819
and if we go back here and look at where

57
00:03:43,620 --> 00:03:47,819
our userland enlistment is 1c3e72a0030

58
00:03:49,220 --> 00:03:54,420
1c3 e72a

59
00:03:53,040 --> 00:03:57,720
B8

60
00:03:54,420 --> 00:04:00,180
that's the V offset is just because it's

61
00:03:57,720 --> 00:04:02,420
taking into account the next samerm flink

62
00:04:05,400 --> 00:04:11,700
so we can see it gives us the

63
00:04:08,280 --> 00:04:13,980
e7a0030

64
00:04:11,700 --> 00:04:15,720
so this is the enlistment so if we print

65
00:04:13,980 --> 00:04:19,220
that address

66
00:04:15,720 --> 00:04:19,220
as a kenlistment

67
00:04:24,660 --> 00:04:29,400
let's see what we have

68
00:04:27,120 --> 00:04:30,960
so obviously most of the fields are

69
00:04:29,400 --> 00:04:33,000
zeroed because

70
00:04:30,960 --> 00:04:34,560
it's a fake userland enlistment and we

71
00:04:33,000 --> 00:04:36,780
haven't initialized all the fields but

72
00:04:34,560 --> 00:04:38,940
we can see that the next samerm

73
00:04:36,780 --> 00:04:40,020
is set like the flink pointer because

74
00:04:38,940 --> 00:04:42,479
it's going to point to the Trap

75
00:04:40,020 --> 00:04:45,240
enlistment also it has a transaction

76
00:04:42,479 --> 00:04:49,620
and the transaction

77
00:04:45,240 --> 00:04:53,120
has some field defined like the actual

78
00:04:49,620 --> 00:04:53,120
state is defined

79
00:04:54,120 --> 00:04:59,040
and so if we go back to kenlistmen

80
00:04:56,340 --> 00:05:02,220
there is the namespace link which is a

81
00:04:59,040 --> 00:05:05,300
substructure which has a links which is

82
00:05:02,220 --> 00:05:05,300
again all zeros

83
00:05:07,520 --> 00:05:13,800
it also has a mutex

84
00:05:10,860 --> 00:05:17,340
which is embedded as well which is a k

85
00:05:13,800 --> 00:05:20,520
mutant structure and it has everything

86
00:05:17,340 --> 00:05:24,860
set to zero it has a header

87
00:05:20,520 --> 00:05:24,860
which is all set to zero as well

88
00:05:30,360 --> 00:05:34,560
the only thing that is said is the

89
00:05:32,340 --> 00:05:37,680
mutant type which is set to two

90
00:05:34,560 --> 00:05:40,740
and it has a wait list head

91
00:05:37,680 --> 00:05:42,780
which is also set to userland and addresses

92
00:05:40,740 --> 00:05:45,740
due to the fact that we actually set

93
00:05:42,780 --> 00:05:45,740
them in the code

94
00:05:48,660 --> 00:05:52,139
so let's go back to the userland

95
00:05:50,280 --> 00:05:54,539
enlistments

96
00:05:52,139 --> 00:05:56,639
so we had the mutex we've analyzed the

97
00:05:54,539 --> 00:05:57,600
header we can look at the muted missed

98
00:05:56,639 --> 00:06:01,340
entry

99
00:05:57,600 --> 00:06:01,340
we can see they are all zeros

100
00:06:04,020 --> 00:06:08,419
like if we go back to the kenlistment

101
00:06:18,960 --> 00:06:23,580
so most of the other

102
00:06:20,940 --> 00:06:25,199
field are actually pointers so they are

103
00:06:23,580 --> 00:06:28,199
not actually embedded structures there

104
00:06:25,199 --> 00:06:31,860
is the kenlistment history

105
00:06:28,199 --> 00:06:33,960
which is an array and we can see

106
00:06:31,860 --> 00:06:38,220
it contains two field that are also

107
00:06:33,960 --> 00:06:40,860
zeros okay so now we know what was in

108
00:06:38,220 --> 00:06:43,199
the Kenlistment our userland

109
00:06:40,860 --> 00:06:45,259
enlistment before anything happens so

110
00:06:43,199 --> 00:06:48,860
let's step

111
00:06:45,259 --> 00:06:48,860
we'll use F10

112
00:06:50,520 --> 00:06:57,620
so here is testing the actual enlistment

113
00:06:53,460 --> 00:06:57,620
head if we look at where we are

114
00:07:03,419 --> 00:07:08,759
we can see rdi is our fake userland

115
00:07:05,880 --> 00:07:10,740
enlistment our leak enlistment and

116
00:07:08,759 --> 00:07:13,139
this is the actual enlistment head

117
00:07:10,740 --> 00:07:15,180
address so if we look at the actual K

118
00:07:13,139 --> 00:07:17,100
resource manager lest's look at that

119
00:07:15,180 --> 00:07:20,100
so the k roussources
manager enlistment head

120
00:07:17,100 --> 00:07:20,100
is at offset

121
00:07:28,440 --> 00:07:31,440
110

122
00:07:45,240 --> 00:07:51,419
so here we print the K resource manager

123
00:07:48,180 --> 00:07:54,240
and we can see we started with like 5000

124
00:07:51,419 --> 00:07:56,880
enlistments and now it's a little

125
00:07:54,240 --> 00:07:59,120
bit below that amount okay which is

126
00:07:56,880 --> 00:07:59,120
normal

127
00:08:01,680 --> 00:08:06,300
okay

128
00:08:03,120 --> 00:08:09,259
so we know this is the enlistment head

129
00:08:06,300 --> 00:08:09,259
so let's continue

130
00:08:35,099 --> 00:08:40,620
okay so now we have a call to up

131
00:08:37,979 --> 00:08:43,039
reference objects so let's step over

132
00:08:40,620 --> 00:08:43,039
this

133
00:08:43,560 --> 00:08:47,779
if we reanalyze our kenlistment

134
00:08:53,160 --> 00:08:59,459
thing is

135
00:08:55,320 --> 00:09:01,860
we know that this call a breath of

136
00:08:59,459 --> 00:09:05,160
reference object actually modified the

137
00:09:01,860 --> 00:09:08,100
object header which is before kenlistment

138
00:09:05,160 --> 00:09:10,019
in memory so our actual enlistment

139
00:09:08,100 --> 00:09:13,700
hasn't been modified as far as I can

140
00:09:10,019 --> 00:09:13,700
tell so let's continue execution

141
00:09:24,300 --> 00:09:31,080
so now we have our ke wait for single

142
00:09:27,899 --> 00:09:33,120
objects on the actual enlistment mutex

143
00:09:31,080 --> 00:09:34,140
and it's passing the pointer to the

144
00:09:33,120 --> 00:09:36,060
mutex

145
00:09:34,140 --> 00:09:37,620
so we know the mutex is embedded into

146
00:09:36,060 --> 00:09:40,640
kenlistment

147
00:09:37,620 --> 00:09:40,640
so let's step over

148
00:09:41,760 --> 00:09:45,380
so this is now stepping over

149
00:09:46,800 --> 00:09:51,140
and let's analyze our kenlistment

150
00:09:52,560 --> 00:09:57,000
so we are more interested in the mutex

151
00:09:54,720 --> 00:09:59,339
because we know that's what has been

152
00:09:57,000 --> 00:10:00,600
passed to the KE wait for single object

153
00:09:59,339 --> 00:10:03,720
function

154
00:10:00,600 --> 00:10:06,440
oh nice look at the honors thread this

155
00:10:03,720 --> 00:10:06,440
wasn't set before

156
00:10:16,980 --> 00:10:21,899
look that was before it was set to zero

157
00:10:19,800 --> 00:10:23,820
so the owner's thread has been set by

158
00:10:21,899 --> 00:10:26,760
the KE wait for a single object function

159
00:10:23,820 --> 00:10:29,779
very nice and it's a k thread anything

160
00:10:26,760 --> 00:10:29,779
else has been modified

161
00:10:31,080 --> 00:10:34,880
so we still have our mutant type

162
00:10:35,880 --> 00:10:39,860
the userland pointers are still there

163
00:10:43,019 --> 00:10:46,640
what about the mutant fees entry

164
00:10:47,100 --> 00:10:51,680
oh nice two other kernel pointers

165
00:10:55,380 --> 00:11:00,839
so if we look again at this one

166
00:10:57,839 --> 00:11:00,839
so we know in the mutantlistentry

167
00:11:09,500 --> 00:11:15,180
we have two pointers

168
00:11:13,260 --> 00:11:16,800
and here we have another pointer so we

169
00:11:15,180 --> 00:11:18,480
have thread pointers so let's

170
00:11:16,800 --> 00:11:21,980
let's look at what they are we know we

171
00:11:18,480 --> 00:11:21,980
can use the ??? pool command

172
00:11:34,260 --> 00:11:39,240
okay so it's telling us that the flink

173
00:11:36,660 --> 00:11:42,079
pointer is actually a thread what about

174
00:11:39,240 --> 00:11:42,079
the blink pointer

175
00:11:47,700 --> 00:11:53,720
very nice it's telling us that it's a

176
00:11:50,339 --> 00:11:53,720
k resource manager object

177
00:11:55,079 --> 00:11:59,579
and we kind of know this one is also a

178
00:11:57,360 --> 00:12:02,779
thread but we're gonna check with the

179
00:11:59,579 --> 00:12:02,779
???? pool command

180
00:12:07,260 --> 00:12:12,060
okay we see the address of this thread

181
00:12:09,420 --> 00:12:14,519
is actually the same address of the

182
00:12:12,060 --> 00:12:17,279
thread that was set in the previous

183
00:12:14,519 --> 00:12:18,899
pointer which shown very nice so if we

184
00:12:17,279 --> 00:12:21,740
go back to the k resource manager

185
00:12:18,899 --> 00:12:21,740
that we showed

186
00:12:37,980 --> 00:12:40,220
so the address we showed of
the k ressource manager 8510

187
00:12:47,459 --> 00:12:53,459
it's actually in that chunk so actually

188
00:12:50,279 --> 00:12:54,779
we have the K resource manager that was

189
00:12:53,459 --> 00:12:56,760
passed as an argument to the number

190
00:12:54,779 --> 00:12:59,639
function that is actually written into

191
00:12:56,760 --> 00:13:02,240
our userland objects very nice and another

192
00:12:59,639 --> 00:13:02,240
thread

193
00:13:03,779 --> 00:13:09,360
but because we have the K resource

194
00:13:06,240 --> 00:13:12,560
manager address we can actually read it

195
00:13:09,360 --> 00:13:12,560
from userland right

196
00:13:17,459 --> 00:13:23,940
and the offset of the K resource manager

197
00:13:19,800 --> 00:13:28,079
that we leak so it's 89c8

198
00:13:23,940 --> 00:13:30,480
so where does it point so let's

199
00:13:28,079 --> 00:13:34,040
check the address that we link

200
00:13:30,480 --> 00:13:34,040
not the k resource manager

201
00:13:36,120 --> 00:13:40,260
so we want the address

202
00:13:38,100 --> 00:13:41,579
that really and we want the address of

203
00:13:40,260 --> 00:13:43,880
the beginning of the k Resource

204
00:13:41,579 --> 00:13:43,880
manager

205
00:13:48,360 --> 00:13:55,380
so it's at offset 40.

206
00:13:52,320 --> 00:13:58,920
so if you look at the K resource manager

207
00:13:55,380 --> 00:14:02,220
offsets 40 is inside the mutex which

208
00:13:58,920 --> 00:14:04,260
makes sense so we have 28 for the mutant

209
00:14:02,220 --> 00:14:07,519
and then we need another

210
00:14:04,260 --> 00:14:07,519
18 hex

211
00:14:09,480 --> 00:14:14,959
so 18 hex is indeed into the mutant his

212
00:14:12,540 --> 00:14:14,959
entry

213
00:14:15,600 --> 00:14:19,980
so because it's offset 18 it means it's

214
00:14:18,540 --> 00:14:23,579
going to be the

215
00:14:19,980 --> 00:14:26,100
address of the Flink so

216
00:14:23,579 --> 00:14:29,820
when we're going to delete the address

217
00:14:26,100 --> 00:14:31,620
we know we're leaking inside the mutex

218
00:14:29,820 --> 00:14:33,720
and then inside the mutex we know we

219
00:14:31,620 --> 00:14:35,639
look inside the mutant list enty

220
00:14:33,720 --> 00:14:36,660
and then we know we leave the rest of

221
00:14:35,639 --> 00:14:38,880
the flink

222
00:14:36,660 --> 00:14:41,160
and so what we need to do basically is

223
00:14:38,880 --> 00:14:44,339
we need to get the address we leak and

224
00:14:41,160 --> 00:14:48,120
then subtract the offset of the mutant

225
00:14:44,339 --> 00:14:50,760
list entry which is 18 hex

226
00:14:48,120 --> 00:14:53,940
and then we land to the address of the

227
00:14:50,760 --> 00:14:57,240
mutant which is at the beginning of new

228
00:14:53,940 --> 00:14:58,920
text and then we need to subtract 28 to

229
00:14:57,240 --> 00:15:01,920
be at the beginning of the k ressource

230
00:14:58,920 --> 00:15:04,639
manager and once we are there we can add

231
00:15:01,920 --> 00:15:08,220
the offset of the enlistment head which is

232
00:15:04,639 --> 00:15:12,260
110. so let's continue debugging just to

233
00:15:08,220 --> 00:15:12,260
see what happens I'm using F10

234
00:15:15,899 --> 00:15:20,779
so we're testing if the end screen is

235
00:15:18,300 --> 00:15:20,779
notifiable

236
00:15:22,500 --> 00:15:28,459
then we're testing if the enlistment is

237
00:15:25,500 --> 00:15:28,459
Superior

238
00:15:28,560 --> 00:15:33,320
then we're testing if the transaction is

239
00:15:30,779 --> 00:15:33,320
in doubt

240
00:15:36,360 --> 00:15:42,440
okay now we're reaching the superior

241
00:15:38,779 --> 00:15:42,440
breakpoints very nice

242
00:15:44,940 --> 00:15:49,100
so we're setting the notification flag

243
00:15:51,620 --> 00:15:55,139
so now

244
00:15:53,639 --> 00:16:00,139
the

245
00:15:55,139 --> 00:16:00,139
enlistment is notified on flag is unset

246
00:16:09,860 --> 00:16:13,519
so where are we

247
00:16:15,899 --> 00:16:21,980
so it's calling the

248
00:16:18,120 --> 00:16:21,980
Ka release mutex function

249
00:16:25,199 --> 00:16:29,060
so I'm just going to continue execution

250
00:16:34,079 --> 00:16:38,579
okay nice so what is happening now is

251
00:16:37,019 --> 00:16:41,160
that

252
00:16:38,579 --> 00:16:43,019
we are again posts

253
00:16:41,160 --> 00:16:45,920
sending the notification so I'm just

254
00:16:43,019 --> 00:16:45,920
going to continue execution

255
00:16:47,699 --> 00:16:52,079
and now

256
00:16:49,100 --> 00:16:55,160
again post execution so I'm just going

257
00:16:52,079 --> 00:16:55,160
to disable the breakpoint

258
00:17:01,220 --> 00:17:05,360
okay nothing hits

259
00:17:06,480 --> 00:17:12,540
and if we go back to the target VM

260
00:17:10,260 --> 00:17:14,160
so you see the detected that we won the

261
00:17:12,540 --> 00:17:16,079
race

262
00:17:14,160 --> 00:17:19,140
but now if we actually continue

263
00:17:16,079 --> 00:17:20,640
execution we don't detect that we leak

264
00:17:19,140 --> 00:17:22,559
the K resource manager address because

265
00:17:20,640 --> 00:17:24,419
the code is not there and so we're not

266
00:17:22,559 --> 00:17:26,959
able to actually inject an escape and

267
00:17:25,767 --> 00:17:28,307
enlistment

268
00:17:29,187 --> 00:17:33,328
and it crashes

269
00:17:31,407 --> 00:17:35,968
because when we hit enter what what

270
00:17:33,328 --> 00:17:39,387
happened is that I injected a wrong

271
00:17:35,968 --> 00:17:42,508
enlistment that has a neural pointer

272
00:17:39,387 --> 00:17:45,147
okay so I have added code into the

273
00:17:42,508 --> 00:17:47,907
escape loop function in order to

274
00:17:45,147 --> 00:17:51,027
actually retrieve the K resource manager

275
00:17:47,907 --> 00:17:53,368
base address and then compute where the

276
00:17:51,027 --> 00:17:56,907
enlistment head is in order to actually

277
00:17:53,368 --> 00:18:00,088
inject this escape enlistment into the

278
00:17:56,907 --> 00:18:01,348
linked list of fake enlistment and so we

279
00:18:00,088 --> 00:18:03,328
know the address of the leak and this

280
00:18:01,348 --> 00:18:05,548
one we have allocated in userland so

281
00:18:03,328 --> 00:18:08,068
we can retrieve a pointer to the actual

282
00:18:05,548 --> 00:18:10,767
new text and then from the mutex we

283
00:18:08,068 --> 00:18:13,828
actually retrieve the blink that we

284
00:18:10,767 --> 00:18:16,228
figured out is actually set to an offset

285
00:18:13,828 --> 00:18:17,848
into the care resource manager and so we

286
00:18:16,228 --> 00:18:20,128
noticed in the debugger that it's

287
00:18:17,848 --> 00:18:22,828
actually pointing to the mutant Place

288
00:18:20,128 --> 00:18:25,407
entry field and so we basically subtract

289
00:18:22,828 --> 00:18:28,887
the mutant list entry field from the K

290
00:18:25,407 --> 00:18:30,688
mutant and then once we actually land to

291
00:18:28,887 --> 00:18:32,968
the beginning of the K mutant which is

292
00:18:30,688 --> 00:18:35,428
actually the mutex variable we actually

293
00:18:32,968 --> 00:18:37,407
subtract the offset of the mutex into

294
00:18:35,428 --> 00:18:40,308
the k resource manager so this will

295
00:18:37,407 --> 00:18:43,828
basically retrieve the

296
00:18:40,308 --> 00:18:46,647
offset that we saw earlier

297
00:18:43,828 --> 00:18:48,328
because we know we need to subtract the

298
00:18:46,647 --> 00:18:50,848
address of the

299
00:18:48,328 --> 00:18:54,508
mutant list entry inside the kamutant

300
00:18:50,848 --> 00:18:57,267
which is offset 28 and then the offset

301
00:18:54,508 --> 00:19:01,248
of the mutex inside the k ressource

302
00:18:57,267 --> 00:19:01,248
manager which is offset 28.

303
00:19:01,647 --> 00:19:05,968
so now we should be able to leave the K

304
00:19:03,448 --> 00:19:09,508
resource manager address and so we need

305
00:19:05,968 --> 00:19:11,968
to inject a new enlistment into the list

306
00:19:09,508 --> 00:19:13,767
so we know the inject enlistment will

307
00:19:11,968 --> 00:19:16,048
inject the escape enlistment so we need

308
00:19:13,767 --> 00:19:18,088
to build the escape enlistment and so we

309
00:19:16,048 --> 00:19:20,368
retrieve the

310
00:19:18,088 --> 00:19:22,588
first Enlistment

311
00:19:20,368 --> 00:19:25,887
which is pointed by the enlistment head so

312
00:19:22,588 --> 00:19:27,988
maybe the penlistment

313
00:19:25,887 --> 00:19:30,387
not a great name but it's basically the

314
00:19:27,988 --> 00:19:32,968
enlistment that is at the head of the

315
00:19:30,387 --> 00:19:35,668
list and it points to a k enlistment so

316
00:19:32,968 --> 00:19:37,767
it's a k enlistment pointer

317
00:19:35,668 --> 00:19:41,548
so now what we can do is we can build

318
00:19:37,767 --> 00:19:43,647
our userland enlistment that is the

319
00:19:41,548 --> 00:19:44,848
escape enlistment and we are passing

320
00:19:43,647 --> 00:19:48,628
this address

321
00:19:44,848 --> 00:19:51,568
which is uh the actual fake address

322
00:19:48,628 --> 00:19:53,548
which will be used to actually set the

323
00:19:51,568 --> 00:19:56,008
escape enlistment next samerm flink

324
00:19:53,548 --> 00:19:58,228
pointer to the actual address of the

325
00:19:56,008 --> 00:20:01,348
enlistment head and the reason we subtract

326
00:19:58,228 --> 00:20:04,708
k enlistment samerm is you've got this

327
00:20:01,348 --> 00:20:06,988
function if you remember text and k

328
00:20:04,708 --> 00:20:11,248
enlistment and then it will actually

329
00:20:06,988 --> 00:20:14,488
add next samerm offsets so we subtract it

330
00:20:11,248 --> 00:20:16,348
so it's actually gonna set the enlistment

331
00:20:14,488 --> 00:20:19,048
next samerm flink pointer to the

332
00:20:16,348 --> 00:20:21,448
actual address of the enlistment head okay

333
00:20:19,048 --> 00:20:24,088
so we have pushed the binary I executed

334
00:20:21,448 --> 00:20:26,548
it and I blocked the recovery thread as

335
00:20:24,088 --> 00:20:28,788
you can see we freed the more than 32

336
00:20:26,548 --> 00:20:32,428
enlistments and now I'm gonna debug

337
00:20:28,788 --> 00:20:35,928
after I unblock the recovery thread

338
00:20:32,428 --> 00:20:35,928
so I unpatch the memory

339
00:20:38,548 --> 00:20:44,428
in this one I'm just going to enable the

340
00:20:41,368 --> 00:20:45,928
breakpoints that is only for the super

341
00:20:44,428 --> 00:20:49,407
case

342
00:20:45,928 --> 00:20:51,508
so it's going to be
after the leak enlistment

343
00:20:49,407 --> 00:20:53,728
is touched but that's going to

344
00:20:51,508 --> 00:20:56,668
simulate the same kind of scenario where

345
00:20:53,728 --> 00:20:58,708
we won't actually simulate the win

346
00:20:56,668 --> 00:21:01,647
winning the race condition anymore later

347
00:20:58,708 --> 00:21:04,488
and we just rely on the superior

348
00:21:01,647 --> 00:21:04,488
breakpoints

349
00:21:05,308 --> 00:21:09,088
nice so we see that we reached our

350
00:21:07,407 --> 00:21:11,308
Superior breakpoints and what happened

351
00:21:09,088 --> 00:21:12,387
from userland so nothing happened for

352
00:21:11,308 --> 00:21:15,688
now so I'm just going to continue

353
00:21:12,387 --> 00:21:18,628
execution so it's hitting the same break

354
00:21:15,688 --> 00:21:20,008
points and actually it's probably for

355
00:21:18,628 --> 00:21:23,088
the

356
00:21:20,008 --> 00:21:23,088
escape enlistment

357
00:21:25,887 --> 00:21:31,728
so now if you continue application okay

358
00:21:28,588 --> 00:21:35,788
so the first thing is it didn't crash

359
00:21:31,728 --> 00:21:38,068
now let's see what happens from userland

360
00:21:35,788 --> 00:21:41,027
so we're able to leak the k ressource

361
00:21:38,068 --> 00:21:41,027
manager address

362
00:21:41,548 --> 00:21:45,387
we're able to get the address of the

363
00:21:42,928 --> 00:21:46,887
first enlistment and close handle files

364
00:21:45,387 --> 00:21:50,748
but I think what matters is that we're

365
00:21:46,887 --> 00:21:50,748
able to exit from kernel

366
00:21:50,848 --> 00:21:54,808
the last thing I want to do is I just

367
00:21:52,527 --> 00:21:58,228
want to make sure no thread is actually

368
00:21:54,808 --> 00:22:02,007
stuck into the recovery function and so

369
00:21:58,228 --> 00:22:03,387
I'm just going to set the breakpoints

370
00:22:02,007 --> 00:22:07,588
on this

371
00:22:03,387 --> 00:22:10,668
and this offset is actually in the loop

372
00:22:07,588 --> 00:22:10,668
so it's actually

373
00:22:11,968 --> 00:22:17,728
there at offset

374
00:22:14,728 --> 00:22:17,728
100 and 02

375
00:22:18,748 --> 00:22:23,608
and if we continue execution we can see

376
00:22:20,907 --> 00:22:26,788
that it never reached that loop so we

377
00:22:23,608 --> 00:22:29,668
exited the actual while loop and no

378
00:22:26,788 --> 00:22:31,828
thread is stuck into the TM recovery

379
00:22:29,668 --> 00:22:33,808
resource manager X function so we did it

380
00:22:31,828 --> 00:22:36,348
I hope you enjoyed this video thank you

381
00:22:33,808 --> 00:22:36,348
for watching

