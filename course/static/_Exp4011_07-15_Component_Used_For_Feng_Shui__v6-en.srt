1
00:00:00,120 --> 00:00:04,920
so for the feng shui we use the named pipes

2
00:00:03,000 --> 00:00:07,259
and we can use it because the

3
00:00:04,920 --> 00:00:10,080
allocations are on the same type of pool

4
00:00:07,259 --> 00:00:12,480
as the KENLISTMENT
basically the named pipe

5
00:00:10,080 --> 00:00:15,599
allocations are on the non-paged pool

6
00:00:12,480 --> 00:00:18,359
same as the KENLISTMENTs and it it is a

7
00:00:15,599 --> 00:00:21,480
requirement that the the object we use

8
00:00:18,359 --> 00:00:24,180
are on the same pool as the

9
00:00:21,480 --> 00:00:25,740
objects we are trying to replace as well

10
00:00:24,180 --> 00:00:28,199
as the object that we are trying to do

11
00:00:25,740 --> 00:00:31,199
the feng shui for
so we can also use the

12
00:00:28,199 --> 00:00:33,899
named pipes to control the data when

13
00:00:31,199 --> 00:00:36,780
replacing the chunk before it triggers

14
00:00:33,899 --> 00:00:39,480
the use-after-free it
gives us much more control

15
00:00:36,780 --> 00:00:42,300
over the KENLISTMENTs
fields our goal is to

16
00:00:39,480 --> 00:00:45,300
replace the KENLISTMENTs
that has been freed

17
00:00:42,300 --> 00:00:47,579
and replaced this NextSameRm flink

18
00:00:45,300 --> 00:00:49,920
pointer with a pointer that points

19
00:00:47,579 --> 00:00:52,860
somewhere we control or whatever in this

20
00:00:49,920 --> 00:00:55,379
case all we are interested in is this

21
00:00:52,860 --> 00:00:58,020
flink pointer as it allows us to have

22
00:00:55,379 --> 00:01:00,420
control to a new fake enlistment with

23
00:00:58,020 --> 00:01:02,219
full control of all its fields but in

24
00:01:00,420 --> 00:01:04,680
general you might have been interested

25
00:01:02,219 --> 00:01:07,560
in controlling other fields too because

26
00:01:04,680 --> 00:01:09,180
it will allow to go in another code path

27
00:01:07,560 --> 00:01:12,659
and so with the named pipe technique

28
00:01:09,180 --> 00:01:15,240
because it corresponds to the data that

29
00:01:12,659 --> 00:01:18,000
you write onto the named pipes we

30
00:01:15,240 --> 00:01:20,119
actually control both the data and the

31
00:01:18,000 --> 00:01:20,119
size

