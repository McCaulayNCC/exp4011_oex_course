1
00:00:00,240 --> 00:00:04,680
hi everyone in this video we're going to

2
00:00:03,480 --> 00:00:07,500
look

3
00:00:04,680 --> 00:00:10,620
how we can actually abuse the increments

4
00:00:07,500 --> 00:00:13,799
primitive in a feasible way because

5
00:00:10,620 --> 00:00:16,619
incrementing a 64-bit value takes a lot

6
00:00:13,799 --> 00:00:18,720
of time but actually if we can do it one

7
00:00:16,619 --> 00:00:22,080
byte at a time that's going to make it

8
00:00:18,720 --> 00:00:23,820
visible but doing so one but at a time

9
00:00:22,080 --> 00:00:25,859
we're going to see there are some side

10
00:00:23,820 --> 00:00:27,660
effects we have to deal with and so

11
00:00:25,859 --> 00:00:31,220
we're gonna have to fix those so let's

12
00:00:27,660 --> 00:00:33,960
see how it works let's get started

13
00:00:31,220 --> 00:00:37,559
the first thing to remember is that the

14
00:00:33,960 --> 00:00:41,280
increment works on a full 32-bit value

15
00:00:37,559 --> 00:00:45,140
so it covers all values from zero zero

16
00:00:41,280 --> 00:00:48,420
zero zero to f f f f f f

17
00:00:45,140 --> 00:00:50,399
so covering four bytes the main

18
00:00:48,420 --> 00:00:53,820
challenge that comes with an increment

19
00:00:50,399 --> 00:00:57,780
primitive is that it could take a long

20
00:00:53,820 --> 00:01:00,600
time if we had to actually increment a

21
00:00:57,780 --> 00:01:03,600
small original value up to a really

22
00:01:00,600 --> 00:01:06,600
large value like if we had to deal with

23
00:01:03,600 --> 00:01:10,260
32 bits it would mean we would need to

24
00:01:06,600 --> 00:01:13,460
potentially increment that to two to the

25
00:01:10,260 --> 00:01:17,280
power of 32 times to change a pointer

26
00:01:13,460 --> 00:01:20,460
which is four billions and so it would

27
00:01:17,280 --> 00:01:23,159
be very slow because remember that for

28
00:01:20,460 --> 00:01:27,000
each increment we actually need the

29
00:01:23,159 --> 00:01:29,520
kernel to fully parse our fake user land

30
00:01:27,000 --> 00:01:32,520
can easements and also because we are

31
00:01:29,520 --> 00:01:36,299
dealing with 64 bits it means we would

32
00:01:32,520 --> 00:01:39,240
need to work on the lower four bytes as

33
00:01:36,299 --> 00:01:42,659
a 32-bit value and then work on the

34
00:01:39,240 --> 00:01:45,540
higher four bytes as another 32-bit

35
00:01:42,659 --> 00:01:48,420
value because the two 32-bit values

36
00:01:45,540 --> 00:01:51,180
would be the one part of the 64-bit

37
00:01:48,420 --> 00:01:54,780
values and so it would actually take

38
00:01:51,180 --> 00:01:57,899
four billions times two so potentially

39
00:01:54,780 --> 00:02:01,079
it would take 8 billions fake user and

40
00:01:57,899 --> 00:02:04,079
Candace builds which would take a very

41
00:02:01,079 --> 00:02:07,020
long time and this would be true if we

42
00:02:04,079 --> 00:02:10,319
wanted to get a really big value but

43
00:02:07,020 --> 00:02:13,680
also after one arbitrary read or write

44
00:02:10,319 --> 00:02:16,620
we built with lots of increments we are

45
00:02:13,680 --> 00:02:20,040
going to need to build another arbitrary

46
00:02:16,620 --> 00:02:22,140
read or arbitrary write and so if we

47
00:02:20,040 --> 00:02:25,739
need to wrap around to get a smaller

48
00:02:22,140 --> 00:02:28,680
value then again we're going to have to

49
00:02:25,739 --> 00:02:31,560
go over 2 to the power of 32 increments

50
00:02:28,680 --> 00:02:34,260
and do that twice so it's not really

51
00:02:31,560 --> 00:02:37,860
doable and so fortunately there is a

52
00:02:34,260 --> 00:02:40,860
trick that will avoid us having to do

53
00:02:37,860 --> 00:02:43,620
these two to the power of 32 increments

54
00:02:40,860 --> 00:02:47,099
twice and so the idea is to increment

55
00:02:43,620 --> 00:02:50,160
each byte independently one at a time

56
00:02:47,099 --> 00:02:54,000
but because we sometimes need to change

57
00:02:50,160 --> 00:02:57,540
a full 64-bit value the idea is that we

58
00:02:54,000 --> 00:03:00,599
just use the 32-bit increment after

59
00:02:57,540 --> 00:03:03,480
Shifting the address one byte at a time

60
00:03:00,599 --> 00:03:06,300
and so effectively having to potentially

61
00:03:03,480 --> 00:03:09,239
deal with eight different addresses all

62
00:03:06,300 --> 00:03:12,860
shifted by one byte and so each byte

63
00:03:09,239 --> 00:03:16,500
will have to be incremented up to

64
00:03:12,860 --> 00:03:19,620
256 times maximum because from the value

65
00:03:16,500 --> 00:03:22,440
0 0 to D value FF potentially and

66
00:03:19,620 --> 00:03:25,819
because we're dealing with eight bytes

67
00:03:22,440 --> 00:03:30,180
it means maximum

68
00:03:25,819 --> 00:03:33,060
256 times 8 increments to deal with

69
00:03:30,180 --> 00:03:35,760
these eight bytes and we'll see why but

70
00:03:33,060 --> 00:03:38,099
we also need to multiply this by 2 to

71
00:03:35,760 --> 00:03:40,500
reach the maximum of times we need to

72
00:03:38,099 --> 00:03:44,519
use the increments so it's effectively a

73
00:03:40,500 --> 00:03:47,940
maximum of 256 times 8 times 2

74
00:03:44,519 --> 00:03:53,340
increments we need so let's assume we

75
00:03:47,940 --> 00:03:55,860
have a 64-bit value at address 1000 hex

76
00:03:53,340 --> 00:03:58,680
so it's effectively these eight bytes

77
00:03:55,860 --> 00:04:02,580
and let's assume that this value is

78
00:03:58,680 --> 00:04:05,280
originally said to be all zeros we said

79
00:04:02,580 --> 00:04:08,879
that if we were working with the full

80
00:04:05,280 --> 00:04:12,840
32-bit values that are part of the

81
00:04:08,879 --> 00:04:15,060
64-bit value so this part and this part

82
00:04:12,840 --> 00:04:18,900
which are recipe to be two different

83
00:04:15,060 --> 00:04:21,660
32-bit values it will mean potentially 2

84
00:04:18,900 --> 00:04:24,300
to the power of 32 increments for this

85
00:04:21,660 --> 00:04:27,540
part and again 2 to the power of 32

86
00:04:24,300 --> 00:04:30,120
increments for this other part which we

87
00:04:27,540 --> 00:04:31,919
don't really want to deal with right and

88
00:04:30,120 --> 00:04:34,620
so now let's assume that we want to

89
00:04:31,919 --> 00:04:38,759
write the value one two three four in

90
00:04:34,620 --> 00:04:41,400
HEX but we don't want to actually do one

91
00:04:38,759 --> 00:04:45,479
two three four increments and so the

92
00:04:41,400 --> 00:04:48,840
first increment we do is at address 1000

93
00:04:45,479 --> 00:04:52,139
hex and we're working on the full 32-bit

94
00:04:48,840 --> 00:04:56,280
value starting from the first byte so

95
00:04:52,139 --> 00:04:59,400
effectively all the lower four bytes up

96
00:04:56,280 --> 00:05:01,979
to here are part of this 32-bit value

97
00:04:59,400 --> 00:05:06,960
but it doesn't really matter because

98
00:05:01,979 --> 00:05:10,080
we're only incrementing 34 times in HEX

99
00:05:06,960 --> 00:05:13,500
so we get this these second increments

100
00:05:10,080 --> 00:05:15,720
we do is that shifted address 1001 so

101
00:05:13,500 --> 00:05:17,400
starting from this byte and again we're

102
00:05:15,720 --> 00:05:19,740
working on the full 32-bit value

103
00:05:17,400 --> 00:05:21,720
starting at this second byte so it's

104
00:05:19,740 --> 00:05:23,580
effectively working on these four bytes

105
00:05:21,720 --> 00:05:26,220
up to here but again it doesn't matter

106
00:05:23,580 --> 00:05:28,500
because we are only incrementing 12 hex

107
00:05:26,220 --> 00:05:32,220
times and so this is what we get after

108
00:05:28,500 --> 00:05:35,340
this uh 12 hex increments and so the

109
00:05:32,220 --> 00:05:38,460
question is assuming we have the 40 hex

110
00:05:35,340 --> 00:05:41,000
bytes on the 8 bytes shown in this

111
00:05:38,460 --> 00:05:44,160
diagram so it's at address

112
00:05:41,000 --> 00:05:47,940
1007 hex it's basically the most

113
00:05:44,160 --> 00:05:51,060
significant byte of this 64-bit value

114
00:05:47,940 --> 00:05:52,800
starting at address 1000 hex and so

115
00:05:51,060 --> 00:05:56,699
let's say we want to actually obtain a

116
00:05:52,800 --> 00:05:59,100
lower value such as 3F so with the

117
00:05:56,699 --> 00:06:01,259
dotted line we are actually showing the

118
00:05:59,100 --> 00:06:03,720
following byte in memory which in this

119
00:06:01,259 --> 00:06:06,600
case is zero but it could be any value

120
00:06:03,720 --> 00:06:11,340
right and so to go from these single

121
00:06:06,600 --> 00:06:16,919
bytes 40 hex to 0 0 hex we will need to

122
00:06:11,340 --> 00:06:19,080
increment c0 hex times at address 1007

123
00:06:16,919 --> 00:06:22,100
and again we're working on the full

124
00:06:19,080 --> 00:06:26,220
32-bit value starting at this address

125
00:06:22,100 --> 00:06:29,039
1007 so the actual 32-bit value is this

126
00:06:26,220 --> 00:06:33,020
byte this byte and two other bytes and

127
00:06:29,039 --> 00:06:35,880
so we can increment from 40 hex to FFX

128
00:06:33,020 --> 00:06:37,680
without any problem but there is a

129
00:06:35,880 --> 00:06:39,660
problem when we're going to try to

130
00:06:37,680 --> 00:06:41,280
increment one more time and it's

131
00:06:39,660 --> 00:06:43,199
basically because our increment is

132
00:06:41,280 --> 00:06:47,280
working on the full 32-bit value

133
00:06:43,199 --> 00:06:50,460
starting at this address at address 1007

134
00:06:47,280 --> 00:06:56,639
hex and so when we're going to cross

135
00:06:50,460 --> 00:06:59,819
from FF hex to 0 0 hex the adjacent byte

136
00:06:56,639 --> 00:07:01,979
will be incremented by one due to the

137
00:06:59,819 --> 00:07:04,919
carry and because we're working on the

138
00:07:01,979 --> 00:07:06,960
full 30-bit value starting from here so

139
00:07:04,919 --> 00:07:10,919
effectively working on these four bytes

140
00:07:06,960 --> 00:07:13,139
and so it's basically 100 hex but

141
00:07:10,919 --> 00:07:16,199
written in userton million and so it

142
00:07:13,139 --> 00:07:18,240
gives zero zero zero one and so we

143
00:07:16,199 --> 00:07:21,419
basically corrupted the following byte

144
00:07:18,240 --> 00:07:24,419
with the zero one hex value which is not

145
00:07:21,419 --> 00:07:27,479
really what we wanted and so if there is

146
00:07:24,419 --> 00:07:30,180
important data after this 64-bit value

147
00:07:27,479 --> 00:07:33,960
that we're trying to change we basically

148
00:07:30,180 --> 00:07:36,660
just lost this data and we corrupted it

149
00:07:33,960 --> 00:07:38,819
and this is far from ideal because if

150
00:07:36,660 --> 00:07:41,280
you correct something in the kernel it's

151
00:07:38,819 --> 00:07:43,440
going to basically crash as soon as this

152
00:07:41,280 --> 00:07:45,479
is used by the kernel if what you

153
00:07:43,440 --> 00:07:48,120
corrupted makes the kernel go into a

154
00:07:45,479 --> 00:07:50,699
some kind of weird state and so we can

155
00:07:48,120 --> 00:07:56,759
actually continue the increments using

156
00:07:50,699 --> 00:07:59,639
the same 1007 hex address and after FF

157
00:07:56,759 --> 00:08:04,199
increments in total this is what we get

158
00:07:59,639 --> 00:08:07,020
so this is one three F hex but written

159
00:08:04,199 --> 00:08:09,720
in little Indian and so it gives 3 at 0

160
00:08:07,020 --> 00:08:12,599
1 in memory and so we get the Wanted

161
00:08:09,720 --> 00:08:15,660
value for that particular bytes which is

162
00:08:12,599 --> 00:08:18,180
3F but the adjacent byte is corrupted

163
00:08:15,660 --> 00:08:20,940
already and we can't do anything about

164
00:08:18,180 --> 00:08:23,819
it and so I guess the question is how to

165
00:08:20,940 --> 00:08:24,840
avoid that the corruption happens in the

166
00:08:23,819 --> 00:08:27,180
first place

167
00:08:24,840 --> 00:08:30,500
and so for instance if we actually want

168
00:08:27,180 --> 00:08:34,380
to change the current value in this

169
00:08:30,500 --> 00:08:36,899
64-bit value so the current value is one

170
00:08:34,380 --> 00:08:39,539
two three four five six seven eight nine

171
00:08:36,899 --> 00:08:43,260
a in HEX and so let's say we want to

172
00:08:39,539 --> 00:08:46,500
change this value to a lower value so

173
00:08:43,260 --> 00:08:49,860
for instance just 1000 hex so it would

174
00:08:46,500 --> 00:08:52,080
be all zero one zero zero zero and so

175
00:08:49,860 --> 00:08:54,779
before we actually do that we're going

176
00:08:52,080 --> 00:08:58,500
to actually reset the full 64-bit value

177
00:08:54,779 --> 00:09:01,500
to 0 using the increments and only after

178
00:08:58,500 --> 00:09:04,980
doing that we're going to reuse the

179
00:09:01,500 --> 00:09:07,980
increment to reach the lower value which

180
00:09:04,980 --> 00:09:10,560
is 1000 hex that we want and so how do

181
00:09:07,980 --> 00:09:12,180
we actually reset all the bytes to zero

182
00:09:10,560 --> 00:09:15,660
because we can't really use the

183
00:09:12,180 --> 00:09:20,220
increments up to 2 to the power of 32

184
00:09:15,660 --> 00:09:24,180
times at address 1000 hex align and

185
00:09:20,220 --> 00:09:27,420
again at address 1004 aligned because we

186
00:09:24,180 --> 00:09:31,200
saw it would not be possible time wise

187
00:09:27,420 --> 00:09:34,560
and so instead what we do is we work on

188
00:09:31,200 --> 00:09:36,839
each byte separately again and so the

189
00:09:34,560 --> 00:09:40,800
idea is we are going to set every byte

190
00:09:36,839 --> 00:09:42,959
to FF independently using misalign

191
00:09:40,800 --> 00:09:47,459
increments and so we're going to first

192
00:09:42,959 --> 00:09:52,560
increment the first byte from line a to

193
00:09:47,459 --> 00:09:56,820
FF hex and this is doable by using six

194
00:09:52,560 --> 00:09:58,860
five hex increments at address 1000 hex

195
00:09:56,820 --> 00:10:01,920
and then similarly we're going to work

196
00:09:58,860 --> 00:10:06,360
on this second bytes and we're going to

197
00:10:01,920 --> 00:10:09,480
increment this byte from 7 8 hex to FF

198
00:10:06,360 --> 00:10:13,279
hex as well and this is doable by using

199
00:10:09,480 --> 00:10:16,260
eight seven hex increments at address

200
00:10:13,279 --> 00:10:20,540
1001 and similarly for all the other

201
00:10:16,260 --> 00:10:24,899
bytes so we use N9 increments at address

202
00:10:20,540 --> 00:10:29,399
1002 and so on at address 1003 at

203
00:10:24,899 --> 00:10:33,540
address 1004 and so on and we do that up

204
00:10:29,399 --> 00:10:37,140
until the 1007 hex address where we do

205
00:10:33,540 --> 00:10:39,420
asset increments to go from 0 0 to FF

206
00:10:37,140 --> 00:10:44,480
and the whole goal of doing all of that

207
00:10:39,420 --> 00:10:47,459
is that we reach the ffff eight times

208
00:10:44,480 --> 00:10:51,720
64-bit value and so the cool thing about

209
00:10:47,459 --> 00:10:55,079
this is that we can actually do two

210
00:10:51,720 --> 00:10:59,279
32-bits align increments in order to

211
00:10:55,079 --> 00:11:02,339
reach the full all zero 64-bit value and

212
00:10:59,279 --> 00:11:05,820
so we do one increment at the Align

213
00:11:02,339 --> 00:11:09,060
address 1000 hex and it's actually going

214
00:11:05,820 --> 00:11:14,579
to wrap around the 32-bit value and go

215
00:11:09,060 --> 00:11:17,100
from all F 32-bit value to all 0 32-bit

216
00:11:14,579 --> 00:11:19,800
value and so there is no side effects on

217
00:11:17,100 --> 00:11:24,000
any adjacent bytes because we only work

218
00:11:19,800 --> 00:11:28,459
on these 32 bits value and finally we do

219
00:11:24,000 --> 00:11:32,339
one more increments at the Align address

220
00:11:28,459 --> 00:11:35,220
1004 hex and again it's going to wrap

221
00:11:32,339 --> 00:11:38,820
around the 32-bit value but without side

222
00:11:35,220 --> 00:11:42,779
effects on adjacent bytes and so from

223
00:11:38,820 --> 00:11:45,240
that stage we can reach any new 64-bit

224
00:11:42,779 --> 00:11:47,820
value we want to target without having

225
00:11:45,240 --> 00:11:50,640
to worry about any corruption happening

226
00:11:47,820 --> 00:11:52,860
and so if we can be maximum number of

227
00:11:50,640 --> 00:11:55,680
times we actually need to use the

228
00:11:52,860 --> 00:11:58,620
increment primitive for a given 64-bit

229
00:11:55,680 --> 00:12:01,500
value so we can reach any target value

230
00:11:58,620 --> 00:12:03,019
basically we set that for each byte we

231
00:12:01,500 --> 00:12:07,560
potentially need

232
00:12:03,019 --> 00:12:09,899
256 increments to reset each byte to FF

233
00:12:07,560 --> 00:12:12,839
and we do that using the misalign

234
00:12:09,899 --> 00:12:16,680
increments then we do two more

235
00:12:12,839 --> 00:12:19,860
increments at the two 32-bit align

236
00:12:16,680 --> 00:12:21,959
addresses that are part of that 64-bit

237
00:12:19,860 --> 00:12:25,860
address and we do that in order to

238
00:12:21,959 --> 00:12:29,519
change the 64-bit value from all F's to

239
00:12:25,860 --> 00:12:32,060
all zeros and then again for each byte

240
00:12:29,519 --> 00:12:35,940
we potentially need another

241
00:12:32,060 --> 00:12:40,380
256 increments to set each byte to

242
00:12:35,940 --> 00:12:43,339
anything we want in the 0 0 FF range so

243
00:12:40,380 --> 00:12:47,399
this is why at the end we need 2 times

244
00:12:43,339 --> 00:12:50,399
256 times 8 incrementing total maximum

245
00:12:47,399 --> 00:12:52,740
to set a given value to anything we want

246
00:12:50,399 --> 00:12:55,040
using the increment primitive and this

247
00:12:52,740 --> 00:12:59,220
is basically just about

248
00:12:55,040 --> 00:13:02,639
4096 increments maximum per arbitrary

249
00:12:59,220 --> 00:13:07,459
right we need instead of the 8 billions

250
00:13:02,639 --> 00:13:07,459
if we didn't use these tricks

