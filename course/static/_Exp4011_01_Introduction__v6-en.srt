1
00:00:00,000 --> 00:00:05,779
hi everyone I'm very excited to welcome

2
00:00:03,419 --> 00:00:09,420
you on the exploitation

3
00:00:05,779 --> 00:00:12,480
4011 this training is going to be about

4
00:00:09,420 --> 00:00:16,080
Windows kernel exploitation exploiting

5
00:00:12,480 --> 00:00:18,539
a race condition which becomes a use

6
00:00:16,080 --> 00:00:21,240
after free in the kernel transaction

7
00:00:18,539 --> 00:00:23,160
manager let's get started basically what

8
00:00:21,240 --> 00:00:25,019
we're going to try to go over in this

9
00:00:23,160 --> 00:00:27,119
course is just explaining what is

10
00:00:25,019 --> 00:00:29,279
involved in exploiting a Windows kernel

11
00:00:27,119 --> 00:00:32,220
vulnerbality in general you just need to

12
00:00:29,279 --> 00:00:34,739
be able to diff a bug understand what

13
00:00:32,220 --> 00:00:37,500
components it is in and be comfortable

14
00:00:34,739 --> 00:00:39,719
with reversing and just learn a bunch of

15
00:00:37,500 --> 00:00:41,700
stuff along the way that you'll need for

16
00:00:39,719 --> 00:00:43,379
exploiting the vulnerability a lot of

17
00:00:41,700 --> 00:00:45,719
people feel like Windows
kernel exploitation

18
00:00:43,379 --> 00:00:47,399
is not approachable I suppose because

19
00:00:45,719 --> 00:00:50,399
they don't have enough prerequisite

20
00:00:47,399 --> 00:00:53,219
knowledge yet which maybe you'll see is

21
00:00:50,399 --> 00:00:56,039
not necessarily true in that vein a lot

22
00:00:53,219 --> 00:00:58,860
a lot of what we are going to cover is

23
00:00:56,039 --> 00:01:01,260
binary diffing and reversing a specific

24
00:00:58,860 --> 00:01:03,480
patch for the bug that will cover and

25
00:01:01,260 --> 00:01:06,540
sort of our approach to cleaning up

26
00:01:03,480 --> 00:01:08,700
reversing databases so it is much more

27
00:01:06,540 --> 00:01:10,740
similar to reading C code and then

28
00:01:08,700 --> 00:01:12,720
you'll have a good foundation to figure

29
00:01:10,740 --> 00:01:14,640
out what you need to learn to actually

30
00:01:12,720 --> 00:01:18,240
exploit the vulnerability everything is

31
00:01:14,640 --> 00:01:20,340
going to be focused on Windows 10 1809

32
00:01:18,240 --> 00:01:22,860
which was the latest version of Windows

33
00:01:20,340 --> 00:01:25,140
10 vulnerable to that bug that we will

34
00:01:22,860 --> 00:01:27,299
be focusing on part of that will be

35
00:01:25,140 --> 00:01:29,759
about understanding the tools a lot of

36
00:01:27,299 --> 00:01:31,799
the course examples is centered around

37
00:01:29,759 --> 00:01:34,200
just encouraging everyone to spend time

38
00:01:31,799 --> 00:01:36,540
using the tools basically the more time

39
00:01:34,200 --> 00:01:38,520
you spend in Tools in general the better

40
00:01:36,540 --> 00:01:40,500
one of the thing that is going to be

41
00:01:38,520 --> 00:01:42,900
interesting to some degree is that this

42
00:01:40,500 --> 00:01:46,079
course is not going to be like a crazy

43
00:01:42,900 --> 00:01:48,240
brain dump of kernel internals because the

44
00:01:46,079 --> 00:01:50,399
thing we want to get across is that you

45
00:01:48,240 --> 00:01:52,380
don't really need to read all of a book

46
00:01:50,399 --> 00:01:54,119
like Windows internals or know

47
00:01:52,380 --> 00:01:56,040
everything about the Windows kernel to

48
00:01:54,119 --> 00:01:58,380
really exploit Windows kernel memory

49
00:01:56,040 --> 00:02:01,079
corruption vulnerabilities that being said

50
00:01:58,380 --> 00:02:04,020
feel free to refer to the architecture

51
00:02:01,079 --> 00:02:06,780
2011 course for some general Windows

52
00:02:04,020 --> 00:02:09,000
internals that we will assume you are

53
00:02:06,780 --> 00:02:11,879
familiar with the most important aspect

54
00:02:09,000 --> 00:02:13,800
of this course is confidence building I

55
00:02:11,879 --> 00:02:16,080
suppose if you're following this course

56
00:02:13,800 --> 00:02:17,640
I assume you have already exploited at

57
00:02:16,080 --> 00:02:20,220
least some other memory corruption

58
00:02:17,640 --> 00:02:21,540
vulnerabilities in other pieces of software so

59
00:02:20,220 --> 00:02:23,280
I'm pretty sure you are capable of

60
00:02:21,540 --> 00:02:25,319
following this course that being said

61
00:02:23,280 --> 00:02:27,599
this course will show you the general

62
00:02:25,319 --> 00:02:30,060
approach so you can more easily exploit

63
00:02:27,599 --> 00:02:32,160
vulnerabilities and see where it is worth

64
00:02:30,060 --> 00:02:34,200
spending time to solve the problems we

65
00:02:32,160 --> 00:02:37,200
generally encounter when trying to

66
00:02:34,200 --> 00:02:39,660
exploit memory corruption vulnerabilities

67
00:02:37,200 --> 00:02:42,180
we'll start by checking the debug

68
00:02:39,660 --> 00:02:44,819
environment that is required to do the

69
00:02:42,180 --> 00:02:49,560
whole training by building it on top of

70
00:02:44,819 --> 00:02:52,739
the dbg 3011 training and adding all the

71
00:02:49,560 --> 00:02:55,620
KTM specifics we will do some binary

72
00:02:52,739 --> 00:02:56,640
diffing to locate where the bug is in

73
00:02:55,620 --> 00:02:58,920
the patch

74
00:02:56,640 --> 00:03:02,040
we will explain some kernel transaction

75
00:02:58,920 --> 00:03:04,260
userland APIs that we can call from

76
00:03:02,040 --> 00:03:07,319
userland to trigger some specifics in

77
00:03:04,260 --> 00:03:09,180
kernel and we'll go over some KTM

78
00:03:07,319 --> 00:03:11,640
like kernel transaction manager

79
00:03:09,180 --> 00:03:13,860
structures into the kernel that are

80
00:03:11,640 --> 00:03:17,099
touched by the driver which is called

81
00:03:13,860 --> 00:03:20,280
tm.sys for transaction manager we will

82
00:03:17,099 --> 00:03:23,220
go over the methods to understand what

83
00:03:20,280 --> 00:03:26,220
the vulnerability is by combining both

84
00:03:23,220 --> 00:03:28,379
our binary diffing approach and the

85
00:03:26,220 --> 00:03:30,239
actual kernel transaction manager

86
00:03:28,379 --> 00:03:34,260
structures that are touching to the

87
00:03:30,239 --> 00:03:37,500
kernel then we will analyze how to patch

88
00:03:34,260 --> 00:03:39,420
things in the debugger in WinDbg to

89
00:03:37,500 --> 00:03:42,120
confirm there is a race condition

90
00:03:39,420 --> 00:03:43,739
vulnerability but without having to deal with

91
00:03:42,120 --> 00:03:45,840
actually triggering the race condition

92
00:03:43,739 --> 00:03:47,700
once we have confirmed that with the

93
00:03:45,840 --> 00:03:50,459
debugger the idea is we are able to

94
00:03:47,700 --> 00:03:53,280
splits exploit tasks between different

95
00:03:50,459 --> 00:03:56,640
people so one person can actually focus

96
00:03:53,280 --> 00:03:58,799
on solving exploitation problems to find

97
00:03:56,640 --> 00:04:00,840
 techniques to get a vulnerability read

98
00:03:58,799 --> 00:04:03,659
write primitive and the other person can

99
00:04:00,840 --> 00:04:06,000
actually focus on actually winning the

100
00:04:03,659 --> 00:04:08,159
race condition without the debugger then

101
00:04:06,000 --> 00:04:11,700
we'll basically work on actual

102
00:04:08,159 --> 00:04:13,200
exploitation techniques and problems for

103
00:04:11,700 --> 00:04:16,260
the rest of the training

104
00:04:13,200 --> 00:04:20,160
so we'll focus on kernel heap manipulation

105
00:04:16,260 --> 00:04:23,100
and ways to do heap feng shui on the

106
00:04:20,160 --> 00:04:25,800
kernel in order to abuse our use

107
00:04:23,100 --> 00:04:28,440
after free vulnerability successfully

108
00:04:25,800 --> 00:04:31,500
so we'll show a very common approach in

109
00:04:28,440 --> 00:04:34,620
kernel exploitation which is to find a

110
00:04:31,500 --> 00:04:37,259
way to detect we won the race condition

111
00:04:34,620 --> 00:04:40,139
and then exit the actual vulnerable

112
00:04:37,259 --> 00:04:42,720
function cleanly and return to

113
00:04:40,139 --> 00:04:46,740
userland the goal of that if we do that

114
00:04:42,720 --> 00:04:49,560
initially is then we are able to trigger

115
00:04:46,740 --> 00:04:51,900
different scenarios without crashing and

116
00:04:49,560 --> 00:04:54,419
so it's easier to debug the exploits

117
00:04:51,900 --> 00:04:57,139
without having to restore a snapshot

118
00:04:54,419 --> 00:05:00,780
many times we will then look into

119
00:04:57,139 --> 00:05:04,440
finding a kernel address Revelation so

120
00:05:00,780 --> 00:05:06,660
we can bypass kernel ASLR we will then

121
00:05:04,440 --> 00:05:08,280
show how to effectively win the race

122
00:05:06,660 --> 00:05:10,860
condition without the help of the

123
00:05:08,280 --> 00:05:13,500
debugger finally we will show how to

124
00:05:10,860 --> 00:05:15,720
build an arbitrary read write primitive to

125
00:05:13,500 --> 00:05:18,780
export the vulnerability at the end of

126
00:05:15,720 --> 00:05:21,360
this course the goal is to have a

127
00:05:18,780 --> 00:05:24,479
workable exploit working on Windows 10

128
00:05:21,360 --> 00:05:27,300
1809 that allows you to elevate

129
00:05:24,479 --> 00:05:30,440
privileges for your own process to system

130
00:05:27,300 --> 00:05:30,440
let's get started

