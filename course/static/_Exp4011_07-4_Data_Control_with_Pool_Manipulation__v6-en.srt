1
00:00:00,000 --> 00:00:06,240
in this video we're going to look into

2
00:00:02,100 --> 00:00:09,120
how we can replace the freed KENLISTMENT

3
00:00:06,240 --> 00:00:11,160
with data we control before we trigger

4
00:00:09,120 --> 00:00:13,559
the use after free and we can abuse it

5
00:00:11,160 --> 00:00:16,260
so we mentioned that using the named pipe

6
00:00:13,559 --> 00:00:18,240
it gives us data control and it's

7
00:00:16,260 --> 00:00:21,480
technically true and you almost control

8
00:00:18,240 --> 00:00:24,539
all of the data so the question is is

9
00:00:21,480 --> 00:00:27,359
one named pipe allocation all we need to

10
00:00:24,539 --> 00:00:30,480
reallocate one chunk and replace our

11
00:00:27,359 --> 00:00:33,059
KENLISTMENT and it turns out

12
00:00:30,480 --> 00:00:35,880
there is technically another problem

13
00:00:33,059 --> 00:00:38,160
which is that when we free the

14
00:00:35,880 --> 00:00:40,739
KENLISTMENT the kernel is still doing other

15
00:00:38,160 --> 00:00:43,680
stuff and so it may be freeing other

16
00:00:40,739 --> 00:00:46,559
chunks and so there may be other free

17
00:00:43,680 --> 00:00:48,780
chunks that will be used instead when we

18
00:00:46,559 --> 00:00:51,000
provide an allocation in hopes of

19
00:00:48,780 --> 00:00:53,640
filling the chain that will be used as a

20
00:00:51,000 --> 00:00:55,500
free and you don't know for sure if that

21
00:00:53,640 --> 00:00:57,840
happened so when you're triggering a bug

22
00:00:55,500 --> 00:01:01,260
like this actually you don't just

23
00:00:57,840 --> 00:01:03,840
allocate one fake KENLISTMENT and hope it

24
00:01:01,260 --> 00:01:07,320
fills the hole you kind of spray a bunch

25
00:01:03,840 --> 00:01:09,659
and how many is just based on kind of

26
00:01:07,320 --> 00:01:11,700
trial and error and you're trying to

27
00:01:09,659 --> 00:01:14,280
deal with the case that maybe the kernel

28
00:01:11,700 --> 00:01:17,040
just freed some other memory and your

29
00:01:14,280 --> 00:01:20,159
fake KENLISTMENT will go into those extra

30
00:01:17,040 --> 00:01:23,400
holes rather than the hole you want so

31
00:01:20,159 --> 00:01:25,979
the solution is to spray like 10 fake

32
00:01:23,400 --> 00:01:28,979
 KENLISTMENTs every time we think we won

33
00:01:25,979 --> 00:01:31,799
the race and in practice it's enough to

34
00:01:28,979 --> 00:01:34,020
have a small animation to show what

35
00:01:31,799 --> 00:01:36,720
happens now so if we go back to the

36
00:01:34,020 --> 00:01:40,320
layout we want we just spray

37
00:01:36,720 --> 00:01:43,439
KENLISTMENTs that become adjacent so

38
00:01:40,320 --> 00:01:45,479
let's say this KENLISTMENT is the one

39
00:01:43,439 --> 00:01:49,619
that is going to be use-after-freed

40
00:01:45,479 --> 00:01:52,500
so it is first freed and now we allocate

41
00:01:49,619 --> 00:01:55,619
a new chunk as we try to reallocate into

42
00:01:52,500 --> 00:01:57,540
that slot but oh it is still free we

43
00:01:55,619 --> 00:01:59,939
don't know where our allocation occurred

44
00:01:57,540 --> 00:02:03,420
but it definitely didn't occur where

45
00:01:59,939 --> 00:02:05,700
wanted and so if we do it 10 times then

46
00:02:03,420 --> 00:02:08,280
the idea is that the hole will

47
00:02:05,700 --> 00:02:10,619
eventually be taken by one of our 10

48
00:02:08,280 --> 00:02:12,239
allocations and we finally get what we

49
00:02:10,619 --> 00:02:15,140
wanted which is that the allocation

50
00:02:12,239 --> 00:02:17,340
happens in our previously freed

51
00:02:15,140 --> 00:02:20,459
KENLISTMENT so now it's probably a good

52
00:02:17,340 --> 00:02:23,040
time to revisit the end goal which is

53
00:02:20,459 --> 00:02:26,160
that using the named pipe write we want

54
00:02:23,040 --> 00:02:29,459
to introduce a fake KENLISTMENT that

55
00:02:26,160 --> 00:02:31,620
will have a controlled flink pointer so

56
00:02:29,459 --> 00:02:34,800
we need to understand what data we

57
00:02:31,620 --> 00:02:37,440
control or do not control in the

58
00:02:34,800 --> 00:02:39,360
replacement chunk basically you do

59
00:02:37,440 --> 00:02:42,000
control most of the data in the name

60
00:02:39,360 --> 00:02:44,160
pipe except a small header in front of

61
00:02:42,000 --> 00:02:47,220
it but you also need to take into

62
00:02:44,160 --> 00:02:50,459
account the layout of the original

63
00:02:47,220 --> 00:02:53,040
KENLISTMENT since it has its own header

64
00:02:50,459 --> 00:02:54,780
structures too due to being tracked by

65
00:02:53,040 --> 00:02:57,840
the object manager one thing about

66
00:02:54,780 --> 00:03:00,720
writing data into a named pipe is that it

67
00:02:57,840 --> 00:03:02,760
isn't tracked by the object manager so

68
00:03:00,720 --> 00:03:05,099
there is a structure in front of the

69
00:03:02,760 --> 00:03:07,680
data that you write to the named pipe

70
00:03:05,099 --> 00:03:10,319
called data entry and then immediately

71
00:03:07,680 --> 00:03:13,140
adjacent to it is the data you control

72
00:03:10,319 --> 00:03:15,959
whereas in the case of the KENLISTMENT

73
00:03:13,140 --> 00:03:18,780
because it's an actual object like a

74
00:03:15,959 --> 00:03:21,180
kernel object it's managed by the object

75
00:03:18,780 --> 00:03:24,120
manager and so when you allocate it on

76
00:03:21,180 --> 00:03:26,400
the heap it has a header which is used

77
00:03:24,120 --> 00:03:29,940
for tracking process quota information

78
00:03:26,400 --> 00:03:32,700
then it has the actual object header and

79
00:03:29,940 --> 00:03:35,040
finally it has the actual KENLISTMENT

80
00:03:32,700 --> 00:03:38,220
so you have to figure out this memory

81
00:03:35,040 --> 00:03:40,739
layout for both the KENLISTMENT and

82
00:03:38,220 --> 00:03:43,019
for the pipes data so you are able to

83
00:03:40,739 --> 00:03:45,420
work out the actual offsets in the name

84
00:03:43,019 --> 00:03:47,519
pipe data that you supply so it

85
00:03:45,420 --> 00:03:50,220
correlates to the actual flink pointer

86
00:03:47,519 --> 00:03:52,860
in the KENLISTMENT chunk and the same for

87
00:03:50,220 --> 00:03:55,200
basically other fields in the KENLISTMENT

88
00:03:52,860 --> 00:03:58,019
structure so just a small animation to

89
00:03:55,200 --> 00:04:00,780
show the actual KENLISTMENT chunk in

90
00:03:58,019 --> 00:04:02,940
memory and so what we did is we dump the

91
00:04:00,780 --> 00:04:05,220
address that is returned from the

92
00:04:02,940 --> 00:04:07,560
pool allocation and we've done the different

93
00:04:05,220 --> 00:04:10,439
structures that are part of that chunk

94
00:04:07,560 --> 00:04:12,599
and so we first done the pool header and

95
00:04:10,439 --> 00:04:15,420
you can see that the contents are

96
00:04:12,599 --> 00:04:18,600
relatively the same by checking the sizes

97
00:04:15,420 --> 00:04:20,760
and the pool tag and whatnot and then we

98
00:04:18,600 --> 00:04:24,000
can see that there is an object header

99
00:04:20,760 --> 00:04:26,220
quota info which just tracks the process

100
00:04:24,000 --> 00:04:28,740
associated with the allocation and then

101
00:04:26,220 --> 00:04:31,500
we see the actual object header which

102
00:04:28,740 --> 00:04:33,600
the object manager uses to track

103
00:04:31,500 --> 00:04:36,000
reference counting and stuff and then

104
00:04:33,600 --> 00:04:38,580
the actual KENLISTMENT with valid

105
00:04:36,000 --> 00:04:42,120
fields like the cookie or the various

106
00:04:38,580 --> 00:04:44,520
pointers and so this just confirms that

107
00:04:42,120 --> 00:04:46,440
our understanding of what is being

108
00:04:44,520 --> 00:04:49,199
placed in memory is correct and

109
00:04:46,440 --> 00:04:51,479
basically it is either other people have

110
00:04:49,199 --> 00:04:54,060
documented it or you know that the

111
00:04:51,479 --> 00:04:56,759
object manager will always create an

112
00:04:54,060 --> 00:04:59,580
object header and you look at the flags

113
00:04:56,759 --> 00:05:01,919
that are passed to those APIs to

114
00:04:59,580 --> 00:05:04,320
understand and whether or not an

115
00:05:01,919 --> 00:05:06,300
additional header like the object header

116
00:05:04,320 --> 00:05:08,520
quota info structure will be present

117
00:05:06,300 --> 00:05:11,820
or you figure it out manually in the

118
00:05:08,520 --> 00:05:14,160
debugger and yes that often just takes a

119
00:05:11,820 --> 00:05:16,860
lot of trial error in WinDbg until

120
00:05:14,160 --> 00:05:19,259
things actually match similarly here

121
00:05:16,860 --> 00:05:21,120
with the data entry structure that is

122
00:05:19,259 --> 00:05:23,280
part of the named pipe right allocation

123
00:05:21,120 --> 00:05:26,100
so the data entry structure itself is

124
00:05:23,280 --> 00:05:29,340
not documented by Microsoft but ReactOS

125
00:05:26,100 --> 00:05:31,380
has their own implementation which seems

126
00:05:29,340 --> 00:05:35,100
to be correct they just don't call it

127
00:05:31,380 --> 00:05:39,120
data entry and instead they call it NP

128
00:05:35,100 --> 00:05:41,699
data queue entry so finding it just

129
00:05:39,120 --> 00:05:44,340
meant like finding the named pipe code in

130
00:05:41,699 --> 00:05:47,160
ReactOS then at the bottom there is

131
00:05:44,340 --> 00:05:50,100
this WinDbg output and we can see there

132
00:05:47,160 --> 00:05:52,380
is a pool header there is the data entry

133
00:05:50,100 --> 00:05:54,600
structure and then at the rightmost

134
00:05:52,380 --> 00:05:57,180
column of the fourth line we start to

135
00:05:54,600 --> 00:05:59,820
have control data 00 01

136
00:05:57,180 --> 00:06:02,340
02 03 04 05

137
00:05:59,820 --> 00:06:04,320
06 07 and so on and so

138
00:06:02,340 --> 00:06:06,539
when you use incrementing values for

139
00:06:04,320 --> 00:06:09,900
Control data like this you can work out

140
00:06:06,539 --> 00:06:12,000
which value is actually being queried in

141
00:06:09,900 --> 00:06:14,699
a use-after-free case and you can figure

142
00:06:12,000 --> 00:06:16,860
out the exact offset inside of the data

143
00:06:14,699 --> 00:06:19,440
you control to replace with the data you

144
00:06:16,860 --> 00:06:22,319
want or whatever so for a lot of the

145
00:06:19,440 --> 00:06:24,720
pool functionality the algorithms and

146
00:06:22,319 --> 00:06:27,060
techniques for spraying in general and

147
00:06:24,720 --> 00:06:29,580
things like the delayed free are very

148
00:06:27,060 --> 00:06:31,860
well documented the main thing is that a

149
00:06:29,580 --> 00:06:35,340
lot of these papers go really really

150
00:06:31,860 --> 00:06:36,900
deep on lots of different internals and

151
00:06:35,340 --> 00:06:39,840
the main thing with papers like this

152
00:06:36,900 --> 00:06:41,819
sometimes to realize is like aside from

153
00:06:39,840 --> 00:06:44,160
the delayed free logic you don't really

154
00:06:41,819 --> 00:06:46,199
need to know that much about how the

155
00:06:44,160 --> 00:06:48,600
algorithm of the non-page pool really

156
00:06:46,199 --> 00:06:50,819
works there is the delayed free thing

157
00:06:48,600 --> 00:06:53,400
sure there is the fact that the pool

158
00:06:50,819 --> 00:06:55,919
tags exist at all that lets you identify

159
00:06:53,400 --> 00:06:57,720
things when analyzing memory with

160
00:06:55,919 --> 00:07:00,840
!pool command

161
00:06:57,720 --> 00:07:03,600
and then there's the idea of chunk

162
00:07:00,840 --> 00:07:06,180
coalescing which almost all heaps use

163
00:07:03,600 --> 00:07:09,539
anyways so if you're familiar with most

164
00:07:06,180 --> 00:07:11,520
heaps it's okay to like assume that that

165
00:07:09,539 --> 00:07:13,800
type of coalition will probably occur

166
00:07:11,520 --> 00:07:16,440
but the thing is if you read some of

167
00:07:13,800 --> 00:07:19,440
these papers like that Tarjei Mandt one

168
00:07:16,440 --> 00:07:22,319
from BlackHat 2011 it goes into way

169
00:07:19,440 --> 00:07:24,419
more details than you actually need and

170
00:07:22,319 --> 00:07:26,699
it's just a word of caution I guess

171
00:07:24,419 --> 00:07:29,220
which is that if you read a paper like

172
00:07:26,699 --> 00:07:31,500
this you might think that doing stuff on

173
00:07:29,220 --> 00:07:33,180
a kernel pool is really complicated and

174
00:07:31,500 --> 00:07:36,120
you need to know all of this information

175
00:07:33,180 --> 00:07:37,800
but actually really the approach is

176
00:07:36,120 --> 00:07:40,620
usually just bare bones information

177
00:07:37,800 --> 00:07:43,080
enough to get by and move on to the next

178
00:07:40,620 --> 00:07:46,520
step and only really start bringing new

179
00:07:43,080 --> 00:07:46,520
things if you have to

