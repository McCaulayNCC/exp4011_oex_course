1
00:00:00,539 --> 00:00:06,359
so the goal of the next lab is to

2
00:00:02,940 --> 00:00:09,660
actually commit a transaction with a

3
00:00:06,359 --> 00:00:11,940
single enlistment so we're gonna have to

4
00:00:09,660 --> 00:00:13,620
actually create a transaction and then

5
00:00:11,940 --> 00:00:16,139
commit the enlistment we're gonna have

6
00:00:13,620 --> 00:00:18,180
to set breakpoints on functions like

7
00:00:16,139 --> 00:00:20,100
when we create the transaction when we

8
00:00:18,180 --> 00:00:21,660
create the enlistment and we when we

9
00:00:20,100 --> 00:00:24,539
commit the enlistment and we're gonna

10
00:00:21,660 --> 00:00:26,519
then analyze the KTRANSACTION object as

11
00:00:24,539 --> 00:00:29,760
well as the KENLISTMENT object okay

12
00:00:26,519 --> 00:00:32,640
let's get started so let's look at the

13
00:00:29,760 --> 00:00:34,920
CommitSingleEnTx.c file we

14
00:00:32,640 --> 00:00:38,340
can see similarities to the previous lab

15
00:00:34,920 --> 00:00:41,219
which is to we have headers related to KTM

16
00:00:38,340 --> 00:00:43,920
the library being linked to we have a

17
00:00:41,219 --> 00:00:47,160
special mask that we can use for being

18
00:00:43,920 --> 00:00:49,559
notified for the transaction we

19
00:00:47,160 --> 00:00:51,660
have handles being defined a default

20
00:00:49,559 --> 00:00:53,640
GUID for the resource manager then the

21
00:00:51,660 --> 00:00:55,440
instruction of the labs so then we

22
00:00:53,640 --> 00:00:57,239
similar to before we create a

23
00:00:55,440 --> 00:00:59,399
transaction manager which is volatile

24
00:00:57,239 --> 00:01:01,860
then we recover the transaction manager

25
00:00:59,399 --> 00:01:03,780
we create the resource manager volatile

26
00:01:01,860 --> 00:01:05,760
as well passing the handle to the

27
00:01:03,780 --> 00:01:07,260
transaction manager then we recover the

28
00:01:05,760 --> 00:01:09,420
resource manager then it's going to

29
00:01:07,260 --> 00:01:11,939
basically wait that you set breakpoints

30
00:01:09,420 --> 00:01:13,380
then there is some instructions to

31
00:01:11,939 --> 00:01:14,460
actually create a transaction so we're

32
00:01:13,380 --> 00:01:17,360
going to need to call the create

33
00:01:14,460 --> 00:01:17,360
transaction function

34
00:01:20,040 --> 00:01:24,600
so we're gonna have to look at the

35
00:01:22,200 --> 00:01:26,880
arguments for that function then it's

36
00:01:24,600 --> 00:01:29,759
already creating the create enlistment

37
00:01:26,880 --> 00:01:31,799
passing the special mask and then it's

38
00:01:29,759 --> 00:01:35,400
calling a function called commit

39
00:01:31,799 --> 00:01:35,400
transaction async

40
00:01:38,280 --> 00:01:43,020
so this function is actually not defined

41
00:01:40,860 --> 00:01:45,420
but what this function is doing is that

42
00:01:43,020 --> 00:01:48,780
it's waiting on the actual transaction

43
00:01:45,420 --> 00:01:50,640
that you commit all the enlistments and

44
00:01:48,780 --> 00:01:54,380
to commit the enlistment you need to

45
00:01:50,640 --> 00:01:54,380
call commit complete

46
00:01:56,759 --> 00:02:01,020
so if we look for commit complete what

47
00:01:59,159 --> 00:02:02,939
it says is indicate that the resource

48
00:02:01,020 --> 00:02:04,619
manager has finished committing a

49
00:02:02,939 --> 00:02:06,960
transaction that was requested by the

50
00:02:04,619 --> 00:02:09,840
transaction manager and you give it a

51
00:02:06,960 --> 00:02:13,020
enlistment handle so yeah you need to add

52
00:02:09,840 --> 00:02:15,660
these two calls and then debug it and

53
00:02:13,020 --> 00:02:17,840
analyze what is happening now it's your

54
00:02:15,660 --> 00:02:17,840
turn

