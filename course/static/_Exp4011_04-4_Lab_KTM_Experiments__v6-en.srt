1
00:00:00,060 --> 00:00:05,640
hi everyone in this part you are going

2
00:00:02,760 --> 00:00:07,200
to work so the first goal of this lab is

3
00:00:05,640 --> 00:00:10,500
to introduce you to the Visual Studio

4
00:00:07,200 --> 00:00:12,660
project so you're able to understand how

5
00:00:10,500 --> 00:00:14,400
to do all the labs in the future but

6
00:00:12,660 --> 00:00:16,199
most importantly you are going to learn

7
00:00:14,400 --> 00:00:18,720
about the kernel transaction manager

8
00:00:16,199 --> 00:00:21,480
APIs how to call them from userland

9
00:00:18,720 --> 00:00:23,820
and how to debug them in kernel there

10
00:00:21,480 --> 00:00:26,220
will be three Labs the first one is

11
00:00:23,820 --> 00:00:28,500
going to be about creating a resource

12
00:00:26,220 --> 00:00:30,660
manager very simple one just to get

13
00:00:28,500 --> 00:00:32,940
started the second lab is about creating

14
00:00:30,660 --> 00:00:34,980
a transaction and that has one

15
00:00:32,940 --> 00:00:37,079
enlistment and making sure you can

16
00:00:34,980 --> 00:00:39,719
commit that transaction and the third

17
00:00:37,079 --> 00:00:42,420
lab is about creating a transaction with

18
00:00:39,719 --> 00:00:45,239
multiple enlistments and checking the

19
00:00:42,420 --> 00:00:47,100
states for these enlistments until they

20
00:00:45,239 --> 00:00:49,559
are committed so the full transaction is

21
00:00:47,100 --> 00:00:51,780
committed okay let's get started you

22
00:00:49,559 --> 00:00:54,420
need to do it in the right folder and

23
00:00:51,780 --> 00:00:56,039
then run the build.bat command in order

24
00:00:54,420 --> 00:00:58,079
to generate the new Visual Studio

25
00:00:56,039 --> 00:01:00,059
project files so we are going to use

26
00:00:58,079 --> 00:01:02,039
Visual Studio to load our projects you

27
00:01:00,059 --> 00:01:04,199
you extracted the files already they

28
00:01:02,039 --> 00:01:07,560
should be in a folder in something like

29
00:01:04,199 --> 00:01:08,820
tools Labs Visual Studio Labs

30
00:01:07,560 --> 00:01:12,240
corresponding to the Visual Studio

31
00:01:08,820 --> 00:01:14,400
labs.zip being extracted and inside you

32
00:01:12,240 --> 00:01:17,880
should have a build folder if you

33
00:01:14,400 --> 00:01:20,100
already run the build.bat script

34
00:01:17,880 --> 00:01:22,380
inside that folder you'll see a solution

35
00:01:20,100 --> 00:01:23,220
file so you can start Visual Studio from

36
00:01:22,380 --> 00:01:25,860
that

37
00:01:23,220 --> 00:01:29,159
so you will notice under the visual

38
00:01:25,860 --> 00:01:31,680
studio project you have a part0 key

39
00:01:29,159 --> 00:01:34,799
with the HelloWorld_lab but you don't

40
00:01:31,680 --> 00:01:36,479
see any part one at the moment so

41
00:01:34,799 --> 00:01:38,040
basically you're gonna have to go into

42
00:01:36,479 --> 00:01:40,140
the tools

43
00:01:38,040 --> 00:01:42,119
folder where you actually extracted the

44
00:01:40,140 --> 00:01:44,340
labs and you should see a Visual Studio

45
00:01:42,119 --> 00:01:46,079
Labs folder along the Visual Studio

46
00:01:44,340 --> 00:01:48,600
labs.zip file that you have already

47
00:01:46,079 --> 00:01:50,700
extracted and so the goal is basically

48
00:01:48,600 --> 00:01:54,240
to copy the

49
00:01:50,700 --> 00:01:57,299
part1.zip file and go into

50
00:01:54,240 --> 00:02:00,119
Visual Studio labs and the labs folder

51
00:01:57,299 --> 00:02:02,460
and extract it into that folder you see

52
00:02:00,119 --> 00:02:04,920
that for part0 there is a hello word

53
00:02:02,460 --> 00:02:06,780
there is some content into part zero

54
00:02:04,920 --> 00:02:07,979
however there is no content into part

55
00:02:06,780 --> 00:02:11,520
one yet

56
00:02:07,979 --> 00:02:12,900
so we're gonna paste part one here and

57
00:02:11,520 --> 00:02:15,379
just extract it

58
00:02:12,900 --> 00:02:15,379
here

59
00:02:15,420 --> 00:02:18,480
it's going to ask you if you want to

60
00:02:16,860 --> 00:02:20,900
replace the different files you say yes

61
00:02:18,480 --> 00:02:20,900
to all

62
00:02:21,540 --> 00:02:26,640
and now we can basically

63
00:02:24,420 --> 00:02:28,379
delete this ZIP file and we're going to

64
00:02:26,640 --> 00:02:31,520
see into part one that there is some

65
00:02:28,379 --> 00:02:31,520
content extracted

66
00:02:34,980 --> 00:02:39,420
so now we want to update the visual

67
00:02:37,140 --> 00:02:41,400
studio project to take into account the

68
00:02:39,420 --> 00:02:43,680
new files we have extracted so remember

69
00:02:41,400 --> 00:02:45,599
only part0 is available at the moment

70
00:02:43,680 --> 00:02:47,879
so what we're going to do is we're going

71
00:02:45,599 --> 00:02:51,319
to open a command prompt and go into

72
00:02:47,879 --> 00:02:51,319
Labs Visual Studio labs

73
00:02:53,340 --> 00:02:58,920
and from here we're going to rebuild the

74
00:02:56,160 --> 00:03:01,160
solution file and just execute build.bat

75
00:02:58,920 --> 00:03:01,160
hello

76
00:03:03,599 --> 00:03:08,580
so sure it's going to copy the

77
00:03:05,519 --> 00:03:11,159
HelloWorld_lab.exe into the

78
00:03:08,580 --> 00:03:14,220
target VM but most importantly it's

79
00:03:11,159 --> 00:03:16,860
going to update the solution file

80
00:03:14,220 --> 00:03:18,959
if we go back to Visual Studio we see

81
00:03:16,860 --> 00:03:20,819
that actually the solution file has been

82
00:03:18,959 --> 00:03:23,760
modified which is exactly what we wanted

83
00:03:20,819 --> 00:03:26,220
so we can reload it and now we see that

84
00:03:23,760 --> 00:03:28,980
part one has been added and we have

85
00:03:26,220 --> 00:03:31,200
access to the three labs in the KTM

86
00:03:28,980 --> 00:03:33,739
Basics create resource manager commit

87
00:03:31,200 --> 00:03:36,180
single enlistment transaction and commit

88
00:03:33,739 --> 00:03:38,159
multi-enlisments transaction so now let's

89
00:03:36,180 --> 00:03:39,840
move to the goals of the different

90
00:03:38,159 --> 00:03:42,360
Visual Studio lab that we're going to do

91
00:03:39,840 --> 00:03:45,000
right now and I'm gonna explain that

92
00:03:42,360 --> 00:03:47,400
with some demo so the goal here is going

93
00:03:45,000 --> 00:03:50,879
to be to familiarize yourself with

94
00:03:47,400 --> 00:03:53,459
actually building the projects and Labs

95
00:03:50,879 --> 00:03:55,500
from Visual Studio directly so for

96
00:03:53,459 --> 00:03:57,060
instance here we see that we only have

97
00:03:55,500 --> 00:03:59,819
the header word that we already built

98
00:03:57,060 --> 00:04:03,599
that we can execute from a desktop

99
00:03:59,819 --> 00:04:07,319
but however if we delete it

100
00:04:03,599 --> 00:04:10,140
and we go here and we just rebuild

101
00:04:07,319 --> 00:04:12,239
the hello world project

102
00:04:10,140 --> 00:04:15,920
we can see that it's going to be copied

103
00:04:12,239 --> 00:04:15,920
onto the target VM again

104
00:04:16,019 --> 00:04:21,479
so the goal of these labs are going to

105
00:04:18,959 --> 00:04:23,699
be to familiarize yourself with the KTM

106
00:04:21,479 --> 00:04:28,080
APIs

107
00:04:23,699 --> 00:04:31,740
we see that it Imports the KTM headers

108
00:04:28,080 --> 00:04:35,220
and to practice Ghidra WinDbg ret-sync

109
00:04:31,740 --> 00:04:39,000
to understand KTM each lab has a section

110
00:04:35,220 --> 00:04:42,180
with a lab to do section where you need

111
00:04:39,000 --> 00:04:44,160
to add certain code and feel free to use

112
00:04:42,180 --> 00:04:48,440
the cheat sheet

113
00:04:44,160 --> 00:04:48,440
from both Ghidra and WinDbg

114
00:04:50,880 --> 00:04:55,919
they give you information on some

115
00:04:52,800 --> 00:05:00,560
commands that are handy to type in order

116
00:04:55,919 --> 00:05:00,560
to understand better what KTM is

117
00:05:01,800 --> 00:05:06,300
so now let's move on to the create

118
00:05:04,259 --> 00:05:09,419
resource manager lab so this is the

119
00:05:06,300 --> 00:05:11,280
first lab regarding KTM internals it's

120
00:05:09,419 --> 00:05:13,800
quite simple one but it's gonna get you

121
00:05:11,280 --> 00:05:16,080
started the idea is basically to create

122
00:05:13,800 --> 00:05:18,540
a resource manager the binary will be

123
00:05:16,080 --> 00:05:21,060
pulled directly to the target VM and you

124
00:05:18,540 --> 00:05:23,280
need to compile it add code to create a

125
00:05:21,060 --> 00:05:26,100
volatile resource manager and then break

126
00:05:23,280 --> 00:05:28,800
on a certain syscall which is

127
00:05:26,100 --> 00:05:31,199
NtCreateResourceManagerExt in WinDbg and then

128
00:05:28,800 --> 00:05:33,600
analyze the different kernel objects

129
00:05:31,199 --> 00:05:35,880
like KRESOURCEMANAGER and KTM

130
00:05:33,600 --> 00:05:38,639
respectively related to the resource

131
00:05:35,880 --> 00:05:41,100
manager and the transaction manager so

132
00:05:38,639 --> 00:05:43,680
all the labs have the same skeleton so

133
00:05:41,100 --> 00:05:45,360
it's good to know how we created them so

134
00:05:43,680 --> 00:05:49,100
the first thing is to build the project

135
00:05:45,360 --> 00:05:49,100
and modify to make sure it builds

136
00:05:56,940 --> 00:06:02,060
you can see it's going to copy the file

137
00:05:58,919 --> 00:06:02,060
onto the target VM

138
00:06:05,460 --> 00:06:09,419
so when you run the generated binary

139
00:06:07,440 --> 00:06:11,220
from the target VM it is going to give

140
00:06:09,419 --> 00:06:13,139
you the instructions of the lab you can

141
00:06:11,220 --> 00:06:15,539
see here the goal is to add code to

142
00:06:13,139 --> 00:06:17,699
create a volatile resource manager and

143
00:06:15,539 --> 00:06:20,520
then break in the debugger on the actual

144
00:06:17,699 --> 00:06:22,020
NT create resource manager X syscall and

145
00:06:20,520 --> 00:06:23,880
finally you'll be able to analyze the

146
00:06:22,020 --> 00:06:25,500
structures in the kernel related to the

147
00:06:23,880 --> 00:06:27,479
resource manager and the transaction

148
00:06:25,500 --> 00:06:29,460
manager so you get familiar with what

149
00:06:27,479 --> 00:06:31,979
objects are created into the kernel so

150
00:06:29,460 --> 00:06:34,680
one quick thing about the fact that

151
00:06:31,979 --> 00:06:35,699
actually here it's waiting that we hit a

152
00:06:34,680 --> 00:06:38,639
key

153
00:06:35,699 --> 00:06:40,880
you'll notice that if we try to rebuild

154
00:06:38,639 --> 00:06:40,880
it

155
00:06:41,280 --> 00:06:45,539
can actually fail

156
00:06:43,919 --> 00:06:47,940
and so if you see this kind of thing

157
00:06:45,539 --> 00:06:50,220
where it fails to build it just make

158
00:06:47,940 --> 00:06:52,680
sure it's not related to the actual

159
00:06:50,220 --> 00:06:55,380
binary being executed already and

160
00:06:52,680 --> 00:06:57,479
waiting for a key to be hit here if we

161
00:06:55,380 --> 00:07:02,360
actually unblock it

162
00:06:57,479 --> 00:07:02,360
and we try to rebuild it it's gonna work

163
00:07:04,620 --> 00:07:08,039
okay so let's look at the actual source

164
00:07:06,840 --> 00:07:11,039
code

165
00:07:08,039 --> 00:07:13,800
so we can see it's the create resource

166
00:07:11,039 --> 00:07:16,680
manager lab and we see it actually links

167
00:07:13,800 --> 00:07:20,539
to KTM libraries it also Imports some

168
00:07:16,680 --> 00:07:20,539
KTM header files

169
00:07:20,759 --> 00:07:25,440
we see that we've defined a handle for a

170
00:07:23,880 --> 00:07:28,080
transaction manager and a resource

171
00:07:25,440 --> 00:07:30,539
manager to default values that are

172
00:07:28,080 --> 00:07:32,880
undefined then there is a default GUID

173
00:07:30,539 --> 00:07:34,740
for resource manager that we've defined

174
00:07:32,880 --> 00:07:36,060
and then we see it starts printing the

175
00:07:34,740 --> 00:07:38,639
instruction of the lab that we saw

176
00:07:36,060 --> 00:07:40,500
earlier when executing the binary we see

177
00:07:38,639 --> 00:07:43,500
it creates a transaction manager which

178
00:07:40,500 --> 00:07:45,479
is volatile and saves it into the handle

179
00:07:43,500 --> 00:07:46,800
for the transaction manager it prints

180
00:07:45,479 --> 00:07:48,900
that the transaction manager has been

181
00:07:46,800 --> 00:07:50,639
created then it recovers the transaction

182
00:07:48,900 --> 00:07:53,520
manager and prints that it has been

183
00:07:50,639 --> 00:07:55,740
recovered then it's prints that you need

184
00:07:53,520 --> 00:07:57,360
to hit a key to create the resource

185
00:07:55,740 --> 00:07:59,699
manager and then there is the get

186
00:07:57,360 --> 00:08:02,639
character function being called and this

187
00:07:59,699 --> 00:08:04,800
blocks the actual binary before you you

188
00:08:02,639 --> 00:08:06,840
need to continue and you need to enter a

189
00:08:04,800 --> 00:08:09,300
key to be able to continue here there is

190
00:08:06,840 --> 00:08:11,819
the actual instructions that you need to

191
00:08:09,300 --> 00:08:14,520
add some code to actually solve the

192
00:08:11,819 --> 00:08:16,319
challenge then it's checking a handle to

193
00:08:14,520 --> 00:08:17,880
Resource measure so probably this code

194
00:08:16,319 --> 00:08:19,919
will actually initialize the resource

195
00:08:17,880 --> 00:08:21,419
manager handle and show the fact that

196
00:08:19,919 --> 00:08:23,879
the resource measure has been created

197
00:08:21,419 --> 00:08:26,400
finally it's called the function to

198
00:08:23,879 --> 00:08:29,099
recover the resource manager and it

199
00:08:26,400 --> 00:08:31,379
prints that information and finally it's

200
00:08:29,099 --> 00:08:33,599
gonna block again just before leaving

201
00:08:31,379 --> 00:08:35,760
just so you can because when you exit

202
00:08:33,599 --> 00:08:38,760
the binary everything related to what

203
00:08:35,760 --> 00:08:40,680
you've initialized will be freed so this

204
00:08:38,760 --> 00:08:42,659
is to avoid things to be freed until

205
00:08:40,680 --> 00:08:45,660
you actually want them to be freed so

206
00:08:42,659 --> 00:08:48,320
let's just build it and modify it and

207
00:08:45,660 --> 00:08:48,320
rerun it

208
00:08:59,399 --> 00:09:03,180
so we're going to execute the create

209
00:09:01,080 --> 00:09:05,339
resource manager lab we see it print the

210
00:09:03,180 --> 00:09:06,779
instructions in print the two

211
00:09:05,339 --> 00:09:08,640
information about the transaction

212
00:09:06,779 --> 00:09:10,440
manager being created and recovered and

213
00:09:08,640 --> 00:09:12,060
then it blocks so we've noticed it

214
00:09:10,440 --> 00:09:13,380
actually blocks just before doing

215
00:09:12,060 --> 00:09:15,420
anything with the create resource

216
00:09:13,380 --> 00:09:17,459
manager function that we haven't written

217
00:09:15,420 --> 00:09:20,519
yet and if we hit enter

218
00:09:17,459 --> 00:09:22,019
it's actually going to exit because the

219
00:09:20,519 --> 00:09:24,240
resource manager hasn't been created

220
00:09:22,019 --> 00:09:26,459
correctly so we know we need to add a

221
00:09:24,240 --> 00:09:29,880
function called create resource manager

222
00:09:26,459 --> 00:09:31,860
so if we look that up on the actual

223
00:09:29,880 --> 00:09:34,080
internet

224
00:09:31,860 --> 00:09:36,480
we're going to see that this function is

225
00:09:34,080 --> 00:09:38,399
defined and we need to check the

226
00:09:36,480 --> 00:09:40,680
different arguments how we can actually

227
00:09:38,399 --> 00:09:42,660
create a volatile resource manager we

228
00:09:40,680 --> 00:09:45,899
see that the first argument can be null

229
00:09:42,660 --> 00:09:48,360
the second one needs to be a valid GUID

230
00:09:45,899 --> 00:09:50,339
which is not null then we can use the

231
00:09:48,360 --> 00:09:52,560
third argument to Define that it's a

232
00:09:50,339 --> 00:09:54,180
volatile resource manager then we need

233
00:09:52,560 --> 00:09:56,820
to pass the handle to the transaction

234
00:09:54,180 --> 00:09:58,920
manager and finally a description for

235
00:09:56,820 --> 00:10:01,320
the resource manager which is optional

236
00:09:58,920 --> 00:10:02,519
so probably can be known as well so the

237
00:10:01,320 --> 00:10:07,640
last thing we're going to need for this

238
00:10:02,519 --> 00:10:07,640
lab is to have Ghidra and WinDbg started

239
00:10:07,860 --> 00:10:12,360
so let's start Ghidra

240
00:10:10,800 --> 00:10:15,300
we're going to work with the

241
00:10:12,360 --> 00:10:18,500
tm_vuln.sys in the

242
00:10:15,300 --> 00:10:18,500
basic bin diffing

243
00:10:19,200 --> 00:10:23,060
which is the one we have modified so far

244
00:10:24,360 --> 00:10:30,620
I'm going to also load the

245
00:10:27,420 --> 00:10:30,620
ntos kernel

246
00:10:30,899 --> 00:10:36,600
that has been modified for us

247
00:10:33,180 --> 00:10:38,700
both in the same code browser then we

248
00:10:36,600 --> 00:10:41,100
need to use a WinDbg so we're going to

249
00:10:38,700 --> 00:10:43,440
use the network approach connect to the

250
00:10:41,100 --> 00:10:45,660
target VM if you look at the actual path

251
00:10:43,440 --> 00:10:48,120
script to start WinDbg you can see

252
00:10:45,660 --> 00:10:50,339
that we have commented out the other

253
00:10:48,120 --> 00:10:53,700
methods and the one we use is the one to

254
00:10:50,339 --> 00:10:56,220
execute the dbg prep.cmd file that

255
00:10:53,700 --> 00:10:58,920
WinDbg is started if we look at the actual

256
00:10:56,220 --> 00:11:01,320
file we can see it's starting ret-sync

257
00:10:58,920 --> 00:11:04,920
it's it's it's trying to sync with Ghidra

258
00:11:01,320 --> 00:11:07,800
and then it reloads the tm.sys module we

259
00:11:04,920 --> 00:11:09,240
for now we commented out the patch

260
00:11:07,800 --> 00:11:11,339
command that we're going to use later

261
00:11:09,240 --> 00:11:15,920
and basically that's all we do then we

262
00:11:11,339 --> 00:11:15,920
continue execution so let's start WinDbg

263
00:11:18,360 --> 00:11:23,279
we can see it's actually connected to

264
00:11:21,000 --> 00:11:24,720
the target however for now it hasn't

265
00:11:23,279 --> 00:11:27,180
done anything so if you want to do

266
00:11:24,720 --> 00:11:28,740
something you can break when you break

267
00:11:27,180 --> 00:11:31,920
you're going to see that actually it's

268
00:11:28,740 --> 00:11:34,140
going to execute the dbg prep.cmd here

269
00:11:31,920 --> 00:11:36,000
it loaded ret-sync however it failed to

270
00:11:34,140 --> 00:11:37,079
synchronize with Ghidra if you go in

271
00:11:36,000 --> 00:11:39,180
Ghidra

272
00:11:37,079 --> 00:11:41,160
we can check that the ret-sync plugin

273
00:11:39,180 --> 00:11:44,100
module is Idle so we have haven't

274
00:11:41,160 --> 00:11:46,500
started this module yet so if we go in

275
00:11:44,100 --> 00:11:48,540
the cheat sheet Ghidra but markdown we

276
00:11:46,500 --> 00:11:50,940
can see that to enable ret-sync we need

277
00:11:48,540 --> 00:11:53,820
to use ALT+S so we go back into Ghidra

278
00:11:50,940 --> 00:11:55,740
and use ALT+S to start the ret-sync plugin

279
00:11:53,820 --> 00:11:59,160
now we can see it's actually listening

280
00:11:55,740 --> 00:12:01,680
and if we use the !sync command in

281
00:11:59,160 --> 00:12:03,540
WinDbg we can see that it's synchronized

282
00:12:01,680 --> 00:12:05,220
with Ghidra so if you just want to

283
00:12:03,540 --> 00:12:07,440
confirm that all the debugging

284
00:12:05,220 --> 00:12:09,600
environment works with Ghidra WinDbg and

285
00:12:07,440 --> 00:12:11,880
running our binary we see that we need

286
00:12:09,600 --> 00:12:13,560
to add a call to create resource manager

287
00:12:11,880 --> 00:12:15,480
which we haven't done yet but we can

288
00:12:13,560 --> 00:12:18,420
probably confirm it works with another

289
00:12:15,480 --> 00:12:20,339
call so if we have the create

290
00:12:18,420 --> 00:12:22,920
transaction manager call we can guess

291
00:12:20,339 --> 00:12:25,200
that there should be a

292
00:12:22,920 --> 00:12:27,420
NtCreateResourceManagerExt function probably

293
00:12:25,200 --> 00:12:29,519
so let's break on that function in

294
00:12:27,420 --> 00:12:31,680
WinDbg so we're going to add a

295
00:12:29,519 --> 00:12:34,800
breakpoint

296
00:12:31,680 --> 00:12:36,600
for that we replace

297
00:12:34,800 --> 00:12:39,320
ResourceManager with

298
00:12:36,600 --> 00:12:39,320
TransactionManagerExt

299
00:12:40,320 --> 00:12:45,959
and then we continue execution now if we

300
00:12:43,560 --> 00:12:47,820
have pushed the binary already and we

301
00:12:45,959 --> 00:12:49,860
re-execute it

302
00:12:47,820 --> 00:12:51,420
we see that it hangs we can't do

303
00:12:49,860 --> 00:12:53,940
anything with the VM it's because it hit

304
00:12:51,420 --> 00:12:57,180
the breakpoints so here we see

305
00:12:53,940 --> 00:13:00,360
some back trace and in Ghidra we see

306
00:12:57,180 --> 00:13:02,579
that actually it hit the function so we

307
00:13:00,360 --> 00:13:05,060
have synchronized it and we're able to

308
00:13:02,579 --> 00:13:05,060
debug

309
00:13:05,700 --> 00:13:11,720
the actual function you can see it's

310
00:13:09,180 --> 00:13:11,720
changing

311
00:13:14,579 --> 00:13:19,100
now it's time for you to do the job

