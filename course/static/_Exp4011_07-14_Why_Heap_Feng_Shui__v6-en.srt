1
00:00:00,060 --> 00:00:05,040
the reason we do heap feng shui is so we

2
00:00:02,760 --> 00:00:07,859
know exactly the memory layouts because

3
00:00:05,040 --> 00:00:09,599
we want the flink pointer to point to

4
00:00:07,859 --> 00:00:11,820
deterministic piece of memory that we

5
00:00:09,599 --> 00:00:13,799
control we know where to point the flink

6
00:00:11,820 --> 00:00:16,020
pointer since we can make it point to

7
00:00:13,799 --> 00:00:18,840
userland memory but we need to control

8
00:00:16,020 --> 00:00:21,300
it in the first place so once we have

9
00:00:18,840 --> 00:00:22,920
freed our KENLISTMENT chunk and we

10
00:00:21,300 --> 00:00:25,800
actually replace it with some data we

11
00:00:22,920 --> 00:00:28,320
control our fake objects we want this

12
00:00:25,800 --> 00:00:31,439
fake object to fit exactly in the same

13
00:00:28,320 --> 00:00:33,780
place in memory and the reason is it

14
00:00:31,439 --> 00:00:35,760
basically it is the fact that we can

15
00:00:33,780 --> 00:00:37,800
know exactly where in the chunk

16
00:00:35,760 --> 00:00:40,440
everything will be and so we can replace

17
00:00:37,800 --> 00:00:42,540
the flink at the right offset but also

18
00:00:40,440 --> 00:00:45,420
we could set another flag somewhere in

19
00:00:42,540 --> 00:00:46,920
in the object if needed even though we

20
00:00:45,420 --> 00:00:49,500
don't need it for exploiting this

21
00:00:46,920 --> 00:00:51,960
particular vulnerability and if we don't

22
00:00:49,500 --> 00:00:54,059
use heap feng shui techniques basically it's

23
00:00:51,960 --> 00:00:55,559
a bit more random because if there is

24
00:00:54,059 --> 00:00:58,079
some case where we free our target

25
00:00:55,559 --> 00:01:01,320
object and then the two chunks coalesce

26
00:00:58,079 --> 00:01:03,600
then when we are trying to replay the

27
00:01:01,320 --> 00:01:05,939
object and do an allocation it's going

28
00:01:03,600 --> 00:01:08,820
to be random and the replacement object

29
00:01:05,939 --> 00:01:11,280
might not be part of our free chunk in

30
00:01:08,820 --> 00:01:14,280
general if the feng shui is bad it's

31
00:01:11,280 --> 00:01:17,220
never going to fit this hole because

32
00:01:14,280 --> 00:01:19,020
there is a larger hole so maybe it's

33
00:01:17,220 --> 00:01:21,540
going to use another hole somewhere else

34
00:01:19,020 --> 00:01:23,580
because it fits better or maybe it's

35
00:01:21,540 --> 00:01:25,380
going to use the start of the hole at

36
00:01:23,580 --> 00:01:28,380
the wrong offset and so it's an

37
00:01:25,380 --> 00:01:30,360
alignment problem so generally I would

38
00:01:28,380 --> 00:01:32,460
say that heap feng shui is a general

39
00:01:30,360 --> 00:01:35,040
technique people use to make exploiting

40
00:01:32,460 --> 00:01:37,259
use after free more reliably because

41
00:01:35,040 --> 00:01:39,240
they have an expected layout on the heap

42
00:01:37,259 --> 00:01:41,540
and they can more reliably achieve their

43
00:01:39,240 --> 00:01:44,520
goal it could be that the exploit works

44
00:01:41,540 --> 00:01:46,320
once by luck without doing the feng shui

45
00:01:44,520 --> 00:01:48,960
but it's just that if you want it to

46
00:01:46,320 --> 00:01:51,180
work all the time or as much as possible

47
00:01:48,960 --> 00:01:53,540
feng shui is a good method to achieve

48
00:01:51,180 --> 00:01:53,540
your goal

