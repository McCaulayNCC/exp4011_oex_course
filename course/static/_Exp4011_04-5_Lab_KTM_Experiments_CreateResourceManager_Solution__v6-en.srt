1
00:00:00,240 --> 00:00:05,040
so to solve the create resource manager

2
00:00:03,120 --> 00:00:08,160
lab we know we need to add a call to

3
00:00:05,040 --> 00:00:10,440
create resource manager and make it a

4
00:00:08,160 --> 00:00:13,019
volatile resource manager so this is

5
00:00:10,440 --> 00:00:14,940
where we need to add the call we look up

6
00:00:13,019 --> 00:00:16,440
the create resource manager function

7
00:00:14,940 --> 00:00:19,320
prototype

8
00:00:16,440 --> 00:00:22,320
and see how we can do that

9
00:00:19,320 --> 00:00:25,039
so we're going to initialize the

10
00:00:22,320 --> 00:00:25,039
handle

11
00:00:30,060 --> 00:00:34,200
and here we see we can specify different

12
00:00:32,399 --> 00:00:36,239
arguments

13
00:00:34,200 --> 00:00:38,399
so the first one needs to be a pointer

14
00:00:36,239 --> 00:00:40,200
to a security attribute but it can also

15
00:00:38,399 --> 00:00:42,899
be null for default attribute so we're

16
00:00:40,200 --> 00:00:45,600
just going to specify null

17
00:00:42,899 --> 00:00:49,079
then we need to specify a GUID

18
00:00:45,600 --> 00:00:52,140
for now I'm just going to specify null

19
00:00:49,079 --> 00:00:54,780
then we have create options any option

20
00:00:52,140 --> 00:00:58,140
for the new resource manager and we want

21
00:00:54,780 --> 00:01:01,760
to specify a volatile one

22
00:00:58,140 --> 00:01:01,760
so we're going to use that macro

23
00:01:03,000 --> 00:01:07,320
then we need to specify the handle to

24
00:01:05,700 --> 00:01:08,760
the transaction manager that will manage

25
00:01:07,320 --> 00:01:09,780
the transaction for this resource

26
00:01:08,760 --> 00:01:13,080
manager

27
00:01:09,780 --> 00:01:15,600
so above we see we've specified we got

28
00:01:13,080 --> 00:01:17,159
the transaction manager handle from the

29
00:01:15,600 --> 00:01:19,799
create transaction manager function so

30
00:01:17,159 --> 00:01:22,320
we can do that

31
00:01:19,799 --> 00:01:23,880
and finally a description for this

32
00:01:22,320 --> 00:01:26,720
resource manager we're just going to use

33
00:01:23,880 --> 00:01:26,720
null for now

34
00:01:31,259 --> 00:01:36,780
okay so now that we've done that

35
00:01:34,200 --> 00:01:39,380
we're going to try to build this and

36
00:01:36,780 --> 00:01:39,380
just run it

37
00:01:58,680 --> 00:02:02,880
looks like it's failing to copy the

38
00:02:00,600 --> 00:02:05,479
binary probably because it's blocked on

39
00:02:02,880 --> 00:02:05,479
a breakpoint

40
00:02:08,399 --> 00:02:13,080
no it's not oh it's because the binary

41
00:02:10,619 --> 00:02:14,520
was already executing so I'm just going

42
00:02:13,080 --> 00:02:18,680
to exit it

43
00:02:14,520 --> 00:02:18,680
and then just rebuild it

44
00:02:20,580 --> 00:02:23,180
okay

45
00:02:27,420 --> 00:02:34,140
so now we execute our modified binary

46
00:02:32,099 --> 00:02:36,480
we see we're gonna hit the key to create

47
00:02:34,140 --> 00:02:39,599
the resource manager and then it fails

48
00:02:36,480 --> 00:02:43,459
with the error 998 if we look up the

49
00:02:39,599 --> 00:02:43,459
error code on the msdm

50
00:02:44,340 --> 00:02:48,620
and we are interested in the 998

51
00:02:49,019 --> 00:02:54,060
we see that it's because of invalid

52
00:02:50,940 --> 00:02:57,180
access to memory location so if we go

53
00:02:54,060 --> 00:02:58,680
back to the actual parameters one

54
00:02:57,180 --> 00:03:01,080
mistake we've done is that the second

55
00:02:58,680 --> 00:03:03,540
argument needs to be a pointer to the

56
00:03:01,080 --> 00:03:05,519
resource manager GUID this parameter is

57
00:03:03,540 --> 00:03:07,800
required and must not be known so maybe

58
00:03:05,519 --> 00:03:11,340
that's why because we passed null

59
00:03:07,800 --> 00:03:12,659
we need to pass a valid GUID

60
00:03:11,340 --> 00:03:15,300
so

61
00:03:12,659 --> 00:03:18,200
we're gonna give a pointer to the GUID

62
00:03:15,300 --> 00:03:18,200
so let's try again

63
00:03:26,159 --> 00:03:31,379
we hit a key again to create it

64
00:03:28,620 --> 00:03:32,819
now we see it passed the test so we

65
00:03:31,379 --> 00:03:34,920
actually created the resource manager

66
00:03:32,819 --> 00:03:38,819
and now we can hit another key to exit

67
00:03:34,920 --> 00:03:40,620
okay so now we have a working binary we

68
00:03:38,819 --> 00:03:42,720
can debug it

69
00:03:40,620 --> 00:03:44,459
I specified in the lab instruction we're

70
00:03:42,720 --> 00:03:46,739
going to set a breakpoint on the

71
00:03:44,459 --> 00:03:49,280
NtCreateResourceManagerExt function so

72
00:03:46,739 --> 00:03:49,280
WinDbg

73
00:03:50,879 --> 00:03:54,860
we'll set a breakpoint on this

74
00:03:52,560 --> 00:03:54,860
function

75
00:03:55,019 --> 00:04:01,280
and continue execution then we go on the

76
00:03:57,120 --> 00:04:01,280
target VM and just rerun the binary

77
00:04:01,739 --> 00:04:08,879
you can see that it breaks

78
00:04:05,640 --> 00:04:10,980
so here actually it hasn't reached the

79
00:04:08,879 --> 00:04:12,360
the place we are interested in so it's

80
00:04:10,980 --> 00:04:15,140
not the right breakpoint so we'll just

81
00:04:12,360 --> 00:04:15,140
continue execution

82
00:04:16,199 --> 00:04:20,639
now it tells us to hit enter to create a

83
00:04:18,900 --> 00:04:24,720
resource manager so we're just going to

84
00:04:20,639 --> 00:04:26,699
hit enter now we reach our breakpoints

85
00:04:24,720 --> 00:04:30,680
so if you don't see a beautiful back

86
00:04:26,699 --> 00:04:30,680
trace here you can just do reload

87
00:04:34,800 --> 00:04:38,880
so after it has finished you can

88
00:04:37,020 --> 00:04:40,800
probably see the back trace and this

89
00:04:38,880 --> 00:04:43,520
time we see that it's actually coming

90
00:04:40,800 --> 00:04:43,520
from our

91
00:04:43,860 --> 00:04:48,300
userland binary CreateResourceManager

92
00:04:45,960 --> 00:04:50,940
_lab.exe and that is calling

93
00:04:48,300 --> 00:04:53,940
the NtCreateResourceManager syscall

94
00:04:50,940 --> 00:04:55,620
which corresponds to our

95
00:04:53,940 --> 00:04:58,620
CreateResourceManager function

96
00:04:55,620 --> 00:04:59,759
and then it reaches the actual syscall in

97
00:04:58,620 --> 00:05:01,620
kernel

98
00:04:59,759 --> 00:05:04,740
so we can see that WinDbg is

99
00:05:01,620 --> 00:05:07,620
synchronized with the Ghidra one problem

100
00:05:04,740 --> 00:05:10,080
we're gonna see in Ghidra is that the

101
00:05:07,620 --> 00:05:12,180
arguments are not defined all the code

102
00:05:10,080 --> 00:05:14,820
is actually unclear because the

103
00:05:12,180 --> 00:05:18,240
variables are don't have names the

104
00:05:14,820 --> 00:05:20,479
functions don't have any prototype

105
00:05:18,240 --> 00:05:22,580
so I'm just gonna redefine everything

106
00:05:20,479 --> 00:05:26,520
similarly to

107
00:05:22,580 --> 00:05:27,900
the basic binary diffing method we've

108
00:05:26,520 --> 00:05:30,539
used there

109
00:05:27,900 --> 00:05:33,000
I guess something worth nothing too is

110
00:05:30,539 --> 00:05:35,840
that it's still

111
00:05:33,000 --> 00:05:38,280
TmRecoverResourceManagerExt that we want to eventually

112
00:05:35,840 --> 00:05:40,440
reverse correctly but in the process of

113
00:05:38,280 --> 00:05:42,960
understanding KTM we are going to start

114
00:05:40,440 --> 00:05:44,699
looking at a bunch of KTM functions and

115
00:05:42,960 --> 00:05:46,740
the first thing we need to do is create

116
00:05:44,699 --> 00:05:49,020
a resource manager and it is the same

117
00:05:46,740 --> 00:05:51,840
developers that wrote this function and

118
00:05:49,020 --> 00:05:53,639
TmRecoverResourceManagerExt which we

119
00:05:51,840 --> 00:05:55,440
don't know how to run yet

120
00:05:53,639 --> 00:05:57,479
we may as well start documenting

121
00:05:55,440 --> 00:05:59,520
something like NtCreateResourceManager

122
00:05:57,479 --> 00:06:01,560
in Ghidra because it will get us

123
00:05:59,520 --> 00:06:03,419
in the mode of like understanding the

124
00:06:01,560 --> 00:06:05,220
way they write the code the way we need

125
00:06:03,419 --> 00:06:07,139
to annotate the structures the way

126
00:06:05,220 --> 00:06:09,960
certain functions get called or whatever

127
00:06:07,139 --> 00:06:12,360
and in a lot of cases when you define a

128
00:06:09,960 --> 00:06:14,940
structure that structure might propagate

129
00:06:12,360 --> 00:06:17,759
into lower level KTM function in other

130
00:06:14,940 --> 00:06:20,160
areas so what ends up happening is that

131
00:06:17,759 --> 00:06:22,560
you document underline functions that

132
00:06:20,160 --> 00:06:25,380
may be TmRecoverResourceManagerExt

133
00:06:22,560 --> 00:06:28,139
also calls so there is a lot of value in

134
00:06:25,380 --> 00:06:30,600
just starting annotations in simple

135
00:06:28,139 --> 00:06:32,520
functions like NtCreateResourceManager

136
00:06:30,600 --> 00:06:34,800
even though it is not the

137
00:06:32,520 --> 00:06:37,080
function that is the end goal

138
00:06:34,800 --> 00:06:39,900
okay so now we can see we have defined

139
00:06:37,080 --> 00:06:42,000
the arguments of the syscall based on

140
00:06:39,900 --> 00:06:43,979
the msdn page

141
00:06:42,000 --> 00:06:46,020
related to the NtCreateResourceManager

142
00:06:43,979 --> 00:06:47,940
syscall so the first argument is

143
00:06:46,020 --> 00:06:49,259
actually the return handle for the

144
00:06:47,940 --> 00:06:51,419
resource manager and all the other

145
00:06:49,259 --> 00:06:54,360
arguments are the same as the create

146
00:06:51,419 --> 00:06:57,419
resource manager API userland we can

147
00:06:54,360 --> 00:07:00,120
see we have defined most of the

148
00:06:57,419 --> 00:07:02,100
variables based on the understanding of

149
00:07:00,120 --> 00:07:04,199
the function we can see that initially

150
00:07:02,100 --> 00:07:06,419
based on the PreviousMode depending on

151
00:07:04,199 --> 00:07:08,940
it's called from userland or kernel mode

152
00:07:06,419 --> 00:07:11,759
it's actually going to do certain checks

153
00:07:08,940 --> 00:07:13,080
on the arguments and if it fails to pass

154
00:07:11,759 --> 00:07:15,560
the check it's going to actually raise

155
00:07:13,080 --> 00:07:15,560
exceptions

156
00:07:16,259 --> 00:07:21,180
then we can see it checks other

157
00:07:19,080 --> 00:07:24,060
arguments like the create option that

158
00:07:21,180 --> 00:07:28,400
the value is more than four also checks

159
00:07:24,060 --> 00:07:28,400
that the description is not null

160
00:07:30,060 --> 00:07:34,740
we can see here from here that there are

161
00:07:32,340 --> 00:07:36,960
certain APIs being called

162
00:07:34,740 --> 00:07:39,720
ObReferenceObjectByHandle so this function is

163
00:07:36,960 --> 00:07:41,580
used to get an object based on the

164
00:07:39,720 --> 00:07:43,440
handle so we can see it's passing the

165
00:07:41,580 --> 00:07:45,300
transaction matter handle which we

166
00:07:43,440 --> 00:07:46,919
assume is valid based on the code we

167
00:07:45,300 --> 00:07:48,960
call from userland and then it's

168
00:07:46,919 --> 00:07:51,900
actually going to initialize an object

169
00:07:48,960 --> 00:07:54,840
so if we look for that API

170
00:07:51,900 --> 00:07:56,880
we can see provides access validation on

171
00:07:54,840 --> 00:07:58,500
the object handle and if access can be

172
00:07:56,880 --> 00:08:00,060
granted return the corresponding

173
00:07:58,500 --> 00:08:01,620
pointer to the object so we pass a

174
00:08:00,060 --> 00:08:04,500
handle and it's going to return an

175
00:08:01,620 --> 00:08:07,139
object. We see the handle for the object

176
00:08:04,500 --> 00:08:09,780
and pointer to variable that receives a

177
00:08:07,139 --> 00:08:11,819
folder to the object's body so in our

178
00:08:09,780 --> 00:08:14,880
case we can see we're requesting for

179
00:08:11,819 --> 00:08:17,759
transaction manager object type so this

180
00:08:14,880 --> 00:08:19,979
should be renamed

181
00:08:17,759 --> 00:08:22,759
transaction manager

182
00:08:19,979 --> 00:08:22,759
object

183
00:08:29,300 --> 00:08:36,000
and then here it's creating an object of

184
00:08:33,060 --> 00:08:39,740
the type resource manager object type so

185
00:08:36,000 --> 00:08:39,740
if we look for that function

186
00:08:59,100 --> 00:09:03,839
we see it takes the object type as fork

187
00:09:01,680 --> 00:09:07,100
argument attribute and then the actual

188
00:09:03,839 --> 00:09:07,100
object is returned

189
00:09:09,839 --> 00:09:14,519
so the prototype doesn't match the one

190
00:09:12,540 --> 00:09:16,860
we found but basically here the only

191
00:09:14,519 --> 00:09:18,899
argument that we can see that is going

192
00:09:16,860 --> 00:09:21,360
to be a return value is this one so I

193
00:09:18,899 --> 00:09:24,740
assume this one is the object

194
00:09:21,360 --> 00:09:24,740
or the resource manager

195
00:09:28,940 --> 00:09:33,660
here we can see a call to TM initialize

196
00:09:32,100 --> 00:09:35,339
resource measure which would make sense

197
00:09:33,660 --> 00:09:37,440
that it takes the resource manager

198
00:09:35,339 --> 00:09:40,700
object of the first argument also taking

199
00:09:37,440 --> 00:09:40,700
the TM objects

200
00:09:46,560 --> 00:09:51,660
here it's inserting a resource manager

201
00:09:49,019 --> 00:09:54,180
object into something

202
00:09:51,660 --> 00:09:55,860
okay so I think we have a pretty good

203
00:09:54,180 --> 00:09:57,600
understanding of the function

204
00:09:55,860 --> 00:09:59,399
from a high level perspective it's

205
00:09:57,600 --> 00:10:02,519
checking the arguments

206
00:09:59,399 --> 00:10:04,920
then it's actually accessing the

207
00:10:02,519 --> 00:10:06,779
transaction manager objects and finally

208
00:10:04,920 --> 00:10:10,580
it's creating the resource manager

209
00:10:06,779 --> 00:10:10,580
object and initializing it

210
00:10:10,920 --> 00:10:17,940
so now we're ready to debug it

211
00:10:13,920 --> 00:10:20,100
so I'm going to set a breakpoint on the

212
00:10:17,940 --> 00:10:22,500
actual call where it's actually

213
00:10:20,100 --> 00:10:25,680
retrieving the transaction manager so to

214
00:10:22,500 --> 00:10:27,959
do that I'm going to refer to the Ghidra

215
00:10:25,680 --> 00:10:30,480
cheatsheet it and you can set a breakpoint

216
00:10:27,959 --> 00:10:31,680
from Ghidra into WinDbg using control

217
00:10:30,480 --> 00:10:34,680
F2

218
00:10:31,680 --> 00:10:37,019
so I'm going to go on to the call of

219
00:10:34,680 --> 00:10:39,660
reference object by handle and hit Ctrl

220
00:10:37,019 --> 00:10:42,000
F2 and you can see maybe here that

221
00:10:39,660 --> 00:10:43,560
actually it set a breakpoint

222
00:10:42,000 --> 00:10:46,459
automatically in win back so now I'm

223
00:10:43,560 --> 00:10:46,459
going to continue execution

224
00:10:47,700 --> 00:10:52,579
we can see we've hit our breakpoints on

225
00:10:49,980 --> 00:10:52,579
the actual call

226
00:10:52,800 --> 00:10:57,200
so if we look at the different arguments

227
00:10:58,860 --> 00:11:04,680
we have one two three four five the

228
00:11:02,040 --> 00:11:06,660
fifth argument is actually the pointer

229
00:11:04,680 --> 00:11:08,100
to the transaction manager object that's

230
00:11:06,660 --> 00:11:11,240
going to be returned so it's going to

231
00:11:08,100 --> 00:11:11,240
actually be on the stack

232
00:11:14,100 --> 00:11:18,740
so this is likely a pointer

233
00:11:22,339 --> 00:11:27,740
so if we go over that call by stepping

234
00:11:25,620 --> 00:11:27,740
over

235
00:11:28,980 --> 00:11:31,820
and uh

236
00:11:34,820 --> 00:11:41,060
we print the actual

237
00:11:38,279 --> 00:11:41,060
object again

238
00:11:45,420 --> 00:11:48,620
looks like we are still

239
00:11:57,320 --> 00:12:01,620
so we're still I think we hit the

240
00:12:00,000 --> 00:12:03,860
function twice so we're going to step

241
00:12:01,620 --> 00:12:03,860
over

242
00:12:10,200 --> 00:12:15,720
now we see it has changed

243
00:12:12,839 --> 00:12:18,740
so if we look at this we assume this is

244
00:12:15,720 --> 00:12:18,740
a transaction manager

245
00:12:26,760 --> 00:12:31,560
so here a couple of things we can notice

246
00:12:29,339 --> 00:12:33,180
this is indeed a transaction manager

247
00:12:31,560 --> 00:12:36,019
because we can see the cookie is

248
00:12:33,180 --> 00:12:39,420
initialized to

249
00:12:36,019 --> 00:12:41,700
b00 b00 04 which is known to be a cookie uh

250
00:12:39,420 --> 00:12:44,519
then we can see the transaction manager is

251
00:12:41,700 --> 00:12:45,899
noted as online it has a GUID which for

252
00:12:44,519 --> 00:12:48,600
now we don't know if it's valid but it's

253
00:12:45,899 --> 00:12:50,700
it's initialized there is no log which

254
00:12:48,600 --> 00:12:53,459
is maybe because it's volatile there is

255
00:12:50,700 --> 00:12:55,740
a linked list so this looks about this

256
00:12:53,459 --> 00:12:58,380
looks valid because the same pointer is

257
00:12:55,740 --> 00:13:00,360
used so it means it's actually an empty

258
00:12:58,380 --> 00:13:02,940
list

259
00:13:00,360 --> 00:13:05,700
then it's pointing to a resource manager

260
00:13:02,940 --> 00:13:07,200
for now and another list which is empty

261
00:13:05,700 --> 00:13:09,959
as well

262
00:13:07,200 --> 00:13:12,139
so now let's step until the the next

263
00:13:09,959 --> 00:13:12,139
thing

264
00:13:35,700 --> 00:13:40,260
so we can see it's going to call the up

265
00:13:37,680 --> 00:13:43,820
create object function

266
00:13:40,260 --> 00:13:43,820
so this is here

267
00:13:46,079 --> 00:13:50,700
this is supposed to create a resource

268
00:13:48,600 --> 00:13:53,720
manager object so the resource manager

269
00:13:50,700 --> 00:13:57,120
object is passed from the last argument

270
00:13:53,720 --> 00:14:00,180
and if we look here we can see it's

271
00:13:57,120 --> 00:14:01,500
actually retrieved from RSP plus an

272
00:14:00,180 --> 00:14:05,240
offset

273
00:14:01,500 --> 00:14:05,240
so if we look up

274
00:14:08,959 --> 00:14:16,820
we can see it corresponds to

275
00:14:12,779 --> 00:14:16,820
RSP Plus 68.

276
00:14:24,560 --> 00:14:29,420
so it's not initialized yet so we're

277
00:14:26,880 --> 00:14:29,420
going to step over

278
00:14:32,420 --> 00:14:38,899
so now let's look at this as a

279
00:14:36,060 --> 00:14:38,899
KRESOURCEMANAGER

280
00:14:45,420 --> 00:14:50,100
what comes to mind here is that it's

281
00:14:47,940 --> 00:14:53,220
actually not initialized because there

282
00:14:50,100 --> 00:14:55,260
is no cookie so the list entry doesn't

283
00:14:53,220 --> 00:14:58,500
make any sense it's not the linked list

284
00:14:55,260 --> 00:15:00,540
the GUID is not valid it looks not valid

285
00:14:58,500 --> 00:15:02,579
so either I'm wrong with this address

286
00:15:00,540 --> 00:15:03,959
being the KRESOURCEMANAGER and I got

287
00:15:02,579 --> 00:15:05,579
it wrong oh it's actually not

288
00:15:03,959 --> 00:15:07,980
initialized

289
00:15:05,579 --> 00:15:10,079
if we look again at the code we remember

290
00:15:07,980 --> 00:15:13,019
that actually after we create the object

291
00:15:10,079 --> 00:15:14,699
we actually have a call to initialize

292
00:15:13,019 --> 00:15:17,220
resource manager so I'm just going to

293
00:15:14,699 --> 00:15:18,899
set a breakpoint on this call with Ctrl

294
00:15:17,220 --> 00:15:22,279
F2

295
00:15:18,899 --> 00:15:22,279
and then continue execution

296
00:15:23,100 --> 00:15:29,240
we see we call that function just before

297
00:15:25,440 --> 00:15:29,240
the call if we look at rcx

298
00:15:29,399 --> 00:15:35,100
we can see that corresponds to the

299
00:15:31,560 --> 00:15:36,839
previously shown address that I tried to

300
00:15:35,100 --> 00:15:38,459
interpret as a KRESOURCEMANAGER so

301
00:15:36,839 --> 00:15:42,139
actually it was valid so now I'm just

302
00:15:38,459 --> 00:15:42,139
going to go over that call

303
00:15:43,139 --> 00:15:47,220
and I'm going to print again

304
00:15:45,120 --> 00:15:49,620
the key resource manager so now we can

305
00:15:47,220 --> 00:15:51,779
see it's initialized the cookie is valid

306
00:15:49,620 --> 00:15:53,760
it's actually a resource manager that is

307
00:15:51,779 --> 00:15:55,920
online so the GUID is interesting

308
00:15:53,760 --> 00:15:57,560
because we actually specify the GUID

309
00:15:55,920 --> 00:16:00,600
which is

310
00:15:57,560 --> 00:16:03,779
37 9e 76 c9 and we can see

311
00:16:00,600 --> 00:16:06,000
37 9e 76 c9 so the GUID is the one we

312
00:16:03,779 --> 00:16:08,579
specified on userland that is entry is

313
00:16:06,000 --> 00:16:10,620
valid because it's an empty list there

314
00:16:08,579 --> 00:16:14,519
is no description which is what we did

315
00:16:10,620 --> 00:16:16,860
from userland and the KTM address if we

316
00:16:14,519 --> 00:16:18,779
look back to what we actually specified

317
00:16:16,860 --> 00:16:21,120
what we printed earlier it does

318
00:16:18,779 --> 00:16:23,699
correspond to the transaction manager we

319
00:16:21,120 --> 00:16:25,320
printed earlier so we're good I think we

320
00:16:23,699 --> 00:16:27,720
are done with actually demonstrating

321
00:16:25,320 --> 00:16:29,459
that we can print addresses for the

322
00:16:27,720 --> 00:16:32,399
transaction manager and the resource

323
00:16:29,459 --> 00:16:35,240
manager which was the goal of this lab

324
00:16:32,399 --> 00:16:35,240
thanks for watching

