1
00:00:00,060 --> 00:00:04,740
hi everyone now that we have a better

2
00:00:02,280 --> 00:00:06,960
understanding of KTM structures and

3
00:00:04,740 --> 00:00:09,059
functions in the kernel it's time to

4
00:00:06,960 --> 00:00:11,280
document better the vulnerable function

5
00:00:09,059 --> 00:00:13,799
in Ghidra everything that we'll be

6
00:00:11,280 --> 00:00:15,780
documenting will be useful not only for

7
00:00:13,799 --> 00:00:18,060
understanding the bug but also for

8
00:00:15,780 --> 00:00:20,100
triggering the code of the vulnerable

9
00:00:18,060 --> 00:00:22,859
function and then later

10
00:00:20,100 --> 00:00:24,840
to actually develop better exploit

11
00:00:22,859 --> 00:00:27,660
primitives so how do we get better

12
00:00:24,840 --> 00:00:29,960
binary diffing I can think about two

13
00:00:27,660 --> 00:00:32,759
aspects the first one it is about

14
00:00:29,960 --> 00:00:35,280
dealing with negative offsets the second

15
00:00:32,759 --> 00:00:38,160
one is about importing structures into

16
00:00:35,280 --> 00:00:41,460
Ghidra so all the accesses to the objects

17
00:00:38,160 --> 00:00:43,739
make sense okay let's get started

18
00:00:41,460 --> 00:00:46,559
so there is this concept of shifted

19
00:00:43,739 --> 00:00:48,719
pointer in IDA that solves the negative

20
00:00:46,559 --> 00:00:50,940
offset problem we saw when decompiling

21
00:00:48,719 --> 00:00:52,739
the vulnerable function and it solves it

22
00:00:50,940 --> 00:00:54,539
really well if you remember in the

23
00:00:52,739 --> 00:00:57,600
vulnerable function we saw negative

24
00:00:54,539 --> 00:01:00,059
offsets and these were when an address

25
00:00:57,600 --> 00:01:02,039
to a list entry element inside a

26
00:01:00,059 --> 00:01:03,780
structure was retrieved and then this

27
00:01:02,039 --> 00:01:06,000
pointer was used and so a negative

28
00:01:03,780 --> 00:01:08,640
offset was used from this pointer and we

29
00:01:06,000 --> 00:01:10,619
realized in the debugger that this was

30
00:01:08,640 --> 00:01:12,420
due to the list entry being in the

31
00:01:10,619 --> 00:01:14,520
middle of another structure and so what

32
00:01:12,420 --> 00:01:17,040
the C code was doing was that it was

33
00:01:14,520 --> 00:01:18,900
subtracting some offsets to go back to

34
00:01:17,040 --> 00:01:21,000
the beginning of the structure before it

35
00:01:18,900 --> 00:01:23,100
could actually index the other elements

36
00:01:21,000 --> 00:01:25,280
of the structure from the actual base so

37
00:01:23,100 --> 00:01:28,860
here we have an example of a simple

38
00:01:25,280 --> 00:01:30,960
mystruct structure with four elements

39
00:01:28,860 --> 00:01:33,840
we have a character array

40
00:01:30,960 --> 00:01:36,540
two integers and a double and there is a

41
00:01:33,840 --> 00:01:39,780
special notation in IDA that uses the

42
00:01:36,540 --> 00:01:42,479
shifted special keyword that allows to

43
00:01:39,780 --> 00:01:44,520
define a new type that is known in IDA

44
00:01:42,479 --> 00:01:47,100
as a shifted pointer and so here we

45
00:01:44,520 --> 00:01:50,399
Define the my pointer new type and we

46
00:01:47,100 --> 00:01:53,280
Define it as an INT pointer type and it

47
00:01:50,399 --> 00:01:56,460
is defined as being inside the my struct

48
00:01:53,280 --> 00:01:58,740
structure and being 20 bytes after the

49
00:01:56,460 --> 00:02:01,680
beginning of the structure so here it is

50
00:01:58,740 --> 00:02:04,200
after the character array of 16 bytes

51
00:02:01,680 --> 00:02:06,659
and then the dummy integer which gives a

52
00:02:04,200 --> 00:02:08,880
total of 20 bytes and so it effectively

53
00:02:06,659 --> 00:02:12,300
points to the value variable and this is

54
00:02:08,880 --> 00:02:14,879
why we Define the new type as an INT

55
00:02:12,300 --> 00:02:16,680
star which is a pointer to an INT

56
00:02:14,879 --> 00:02:18,959
because it points the value the third

57
00:02:16,680 --> 00:02:21,599
element and so inside IDA we could

58
00:02:18,959 --> 00:02:24,300
Define a given variable to be of the

59
00:02:21,599 --> 00:02:27,420
type my pointer and IDA would not only

60
00:02:24,300 --> 00:02:30,480
know its type that is a pointer to an

61
00:02:27,420 --> 00:02:33,120
INT but also it would know that it is at

62
00:02:30,480 --> 00:02:35,160
offset 20 into my struct and so

63
00:02:33,120 --> 00:02:38,280
effectively point to the value elements

64
00:02:35,160 --> 00:02:40,800
and so for instance if the rcx register

65
00:02:38,280 --> 00:02:43,379
holds the my pointer shifted pointer

66
00:02:40,800 --> 00:02:46,140
then if there was an access to rcx plus

67
00:02:43,379 --> 00:02:49,019
4 you would know that it is only 4 bytes

68
00:02:46,140 --> 00:02:51,900
after the value so effectively it is

69
00:02:49,019 --> 00:02:54,239
actually accessing the fval and this

70
00:02:51,900 --> 00:02:56,640
is why here it's showing adjusted my

71
00:02:54,239 --> 00:02:59,280
pointer fval and so if there was an

72
00:02:56,640 --> 00:03:01,500
access to rcx minus 4 you would know it

73
00:02:59,280 --> 00:03:04,140
is 4 bytes before the value so

74
00:03:01,500 --> 00:03:06,120
effectively accessing the dummy integer

75
00:03:04,140 --> 00:03:08,700
and so the notation used by the hex

76
00:03:06,120 --> 00:03:11,640
rays  decompiler in IDA is the adjust

77
00:03:08,700 --> 00:03:13,560
macro which stands for adjusted pointer

78
00:03:11,640 --> 00:03:15,959
just to record that it is a shifted

79
00:03:13,560 --> 00:03:17,879
pointer and that it offsets the other

80
00:03:15,959 --> 00:03:19,500
elements from an element of a structure

81
00:03:17,879 --> 00:03:21,360
and this is basically how the negative

82
00:03:19,500 --> 00:03:23,400
offsets are shown correctly in the

83
00:03:21,360 --> 00:03:25,560
decompiler so now let's see how that

84
00:03:23,400 --> 00:03:27,720
would help in our vulnerable function so

85
00:03:25,560 --> 00:03:30,239
this is the output from IDA and we

86
00:03:27,720 --> 00:03:32,099
explain the concept in IDA because at

87
00:03:30,239 --> 00:03:33,720
the time of recording this Ghidra

88
00:03:32,099 --> 00:03:36,000
doesn't support shifted pointer but

89
00:03:33,720 --> 00:03:37,680
we'll see how to work around it in Ghidra

90
00:03:36,000 --> 00:03:39,900
later don't worry but for now let's

91
00:03:37,680 --> 00:03:41,760
focus on IDA so in this

92
00:03:39,900 --> 00:03:43,980
TmRecoverResourceManager function we can see

93
00:03:41,760 --> 00:03:45,840
some negative offsets here and in the

94
00:03:43,980 --> 00:03:48,180
decompiled code we already documented

95
00:03:45,840 --> 00:03:50,400
most types so we know there is a

96
00:03:48,180 --> 00:03:53,099
KRESOURCEMANAGER and it is accessing some

97
00:03:50,400 --> 00:03:55,440
mutex accessing the states the TM

98
00:03:53,099 --> 00:03:58,319
pointer and also a pointer to the

99
00:03:55,440 --> 00:04:01,260
enlistments head
element and finally when it

100
00:03:58,319 --> 00:04:03,900
retrieves the enlistment head f-link here

101
00:04:01,260 --> 00:04:05,760
we hold that into a current enlistment

102
00:04:03,900 --> 00:04:07,620
list entry and when it access the

103
00:04:05,760 --> 00:04:09,360
current enlistment instant entry we can

104
00:04:07,620 --> 00:04:12,000
see the negative offset from that

105
00:04:09,360 --> 00:04:14,519
pointer and it saves it into v9 and then

106
00:04:12,000 --> 00:04:17,519
from that negative offset in v9 there

107
00:04:14,519 --> 00:04:20,880
are multiple accesses from an offset of AC

108
00:04:17,519 --> 00:04:22,979
or an offset of 40 here and here as well

109
00:04:20,880 --> 00:04:25,740
and these two offsets should basically

110
00:04:22,979 --> 00:04:27,660
be computed by first subtracting the

111
00:04:25,740 --> 00:04:30,240
negative offsets to go to the beginning

112
00:04:27,660 --> 00:04:33,780
of a given structure and then adding the

113
00:04:30,240 --> 00:04:35,940
AC or 40 bytes to reach the right field

114
00:04:33,780 --> 00:04:37,680
inside the structure and so in the

115
00:04:35,940 --> 00:04:40,320
previous code we saw that it accesses

116
00:04:37,680 --> 00:04:42,240
the enlistment head inside the KRESOURCEMANAGER

117
00:04:40,320 --> 00:04:44,220
and so what happens with the

118
00:04:42,240 --> 00:04:46,440
enlistment head is that it is going to be

119
00:04:44,220 --> 00:04:49,440
a list entry which is basically a linked

120
00:04:46,440 --> 00:04:51,540
list of KENLISTMENTs but the Flink and

121
00:04:49,440 --> 00:04:54,060
the blink pointers that are part of the

122
00:04:51,540 --> 00:04:55,740
list entry are actually pointers in the

123
00:04:54,060 --> 00:04:58,259
middle of the enlistment structure so

124
00:04:55,740 --> 00:05:00,600
there are Fields before and after and so

125
00:04:58,259 --> 00:05:02,880
when the enlistment head is accessed and we

126
00:05:00,600 --> 00:05:05,340
point inside a given KENLISTMENT the

127
00:05:02,880 --> 00:05:08,280
code will actually subtract the offsets

128
00:05:05,340 --> 00:05:10,620
of NextSameRm to be at the beginning

129
00:05:08,280 --> 00:05:13,259
of the KENLISTMENT before it is able to

130
00:05:10,620 --> 00:05:15,300
index the other fields inside of the

131
00:05:13,259 --> 00:05:17,340
KENLISTMENT structure so using IDA

132
00:05:15,300 --> 00:05:20,460
shifted pointer feature we can Define

133
00:05:17,340 --> 00:05:23,100
the new type which we name KENLISTMENT

134
00:05:20,460 --> 00:05:25,500
next same RM pointer and here we could

135
00:05:23,100 --> 00:05:28,199
use any name really but the advantage of

136
00:05:25,500 --> 00:05:30,740
this naming is that we know exactly that

137
00:05:28,199 --> 00:05:33,960
it is a pointer to the next same RM

138
00:05:30,740 --> 00:05:35,759
field inside the KENLISTMENT structure

139
00:05:33,960 --> 00:05:38,340
and it is actually a pointer to that

140
00:05:35,759 --> 00:05:40,800
field so because this next same RM is a

141
00:05:38,340 --> 00:05:43,080
list entry we Define that new time as a

142
00:05:40,800 --> 00:05:45,660
list entry pointer and also we Define it

143
00:05:43,080 --> 00:05:48,780
as a shifted pointer
inside the KENLISTMENT

144
00:05:45,660 --> 00:05:51,960
structure at offset 88 due to the

145
00:05:48,780 --> 00:05:54,180
NextSameRm at offset 88 from the start

146
00:05:51,960 --> 00:05:56,160
of the KENLISTMENT structure and so here

147
00:05:54,180 --> 00:05:58,979
I'm gonna show you how to make sense of

148
00:05:56,160 --> 00:06:01,199
it and so I have two slides one with the

149
00:05:58,979 --> 00:06:04,020
code before any modification and another

150
00:06:01,199 --> 00:06:05,639
one with the code after modification so

151
00:06:04,020 --> 00:06:07,320
we can see the difference I'm going to

152
00:06:05,639 --> 00:06:09,660
switch between the two so you can see

153
00:06:07,320 --> 00:06:11,820
the actual improvement and so the first

154
00:06:09,660 --> 00:06:13,080
thing we do to make sense of it is we

155
00:06:11,820 --> 00:06:15,300
change the type of the current

156
00:06:13,080 --> 00:06:17,160
enlistment list entry to be our new

157
00:06:15,300 --> 00:06:20,160
shifted pointer type that we have just

158
00:06:17,160 --> 00:06:23,220
defined and so we
Define it as a KENLISTMENT

159
00:06:20,160 --> 00:06:25,319
next same RM pointer since we know it

160
00:06:23,220 --> 00:06:28,199
points to the middle of the KENLISTMENT

161
00:06:25,319 --> 00:06:30,680
structure at offset 88 and so if we just

162
00:06:28,199 --> 00:06:33,060
do that the current line that used to be

163
00:06:30,680 --> 00:06:35,160
accessing a negative offset and the

164
00:06:33,060 --> 00:06:37,800
blink and we wouldn't know what it is

165
00:06:35,160 --> 00:06:40,620
now it's actually showing at an adjusted

166
00:06:37,800 --> 00:06:42,300
pointer from the shifted pointer and so

167
00:06:40,620 --> 00:06:44,100
we know because no field is specified

168
00:06:42,300 --> 00:06:45,720
that it's actually pointing to the

169
00:06:44,100 --> 00:06:47,819
beginning of the KENLISTMENT structure now

170
00:06:45,720 --> 00:06:49,919
and so basically what happens is the

171
00:06:47,819 --> 00:06:52,500
negative offset is replaced with the

172
00:06:49,919 --> 00:06:55,560
adjusted macro since it will indicate

173
00:06:52,500 --> 00:06:56,759
that the 88 byte subtraction makes it

174
00:06:55,560 --> 00:07:00,660
point to the beginning of the canister

175
00:06:56,759 --> 00:07:02,520
and so consequently we can Define the

176
00:07:00,660 --> 00:07:05,280
previous v9 as being a current

177
00:07:02,520 --> 00:07:07,800
enlistment and also we can Define its

178
00:07:05,280 --> 00:07:10,860
type to be the KENLISTMENT so instead

179
00:07:07,800 --> 00:07:13,139
of having v9 plus offset now we have

180
00:07:10,860 --> 00:07:14,880
current enlistment flag because it's

181
00:07:13,139 --> 00:07:17,639
defined at the right time so basically

182
00:07:14,880 --> 00:07:19,319
doing these two simple changes of types

183
00:07:17,639 --> 00:07:22,380
we basically have a very good code

184
00:07:19,319 --> 00:07:24,660
output we can see the offset of AC is

185
00:07:22,380 --> 00:07:28,680
replaced with current enlistment flags and

186
00:07:24,660 --> 00:07:31,259
instead of the access at offset 40 we

187
00:07:28,680 --> 00:07:34,440
have the current enlistment mutex or also

188
00:07:31,259 --> 00:07:38,099
note above that instead of the simple

189
00:07:34,440 --> 00:07:40,800
Flink access inside the list entry we

190
00:07:38,099 --> 00:07:43,199
now have the adjusted pointer and then

191
00:07:40,800 --> 00:07:45,960
next same RM Flink so the difference is

192
00:07:43,199 --> 00:07:48,419
that it actually accesses the next rm

193
00:07:45,960 --> 00:07:50,460
list entry and then the Flink inside the

194
00:07:48,419 --> 00:07:52,560
list entry because it knows it's part of

195
00:07:50,460 --> 00:07:55,199
the KENLISTMENT structure instead of just

196
00:07:52,560 --> 00:07:56,759
the list entry structure so anyway I

197
00:07:55,199 --> 00:07:59,220
don't know what you think but personally

198
00:07:56,759 --> 00:08:01,319
I really like this syntax and I find it

199
00:07:59,220 --> 00:08:03,419
really readable so moving to Ghidra

200
00:08:01,319 --> 00:08:05,460
now unfortunately Ghidra doesn't

201
00:08:03,419 --> 00:08:07,919
support shifted pointer yet and there is

202
00:08:05,460 --> 00:08:09,840
a GitHub issue asking to add support for

203
00:08:07,919 --> 00:08:12,000
it in a future version so it's possible

204
00:08:09,840 --> 00:08:14,759
that they have added support for it

205
00:08:12,000 --> 00:08:16,860
since I have recorded that video so

206
00:08:14,759 --> 00:08:20,580
going back to the KENLISTMENT structure and

207
00:08:16,860 --> 00:08:22,259
the NextSameRm element at offset 88 we

208
00:08:20,580 --> 00:08:24,479
have actually found a workaround to be

209
00:08:22,259 --> 00:08:26,819
able to reference a pointer to that

210
00:08:24,479 --> 00:08:29,160
NextSameRm element even though Ghidra doesn't

211
00:08:26,819 --> 00:08:31,740
support shifted pointers and so

212
00:08:29,160 --> 00:08:33,360
basically the idea is to to remove all

213
00:08:31,740 --> 00:08:35,399
the elements from the start of the

214
00:08:33,360 --> 00:08:37,740
structure and to move them at the end of

215
00:08:35,399 --> 00:08:41,159
the structure so what we do is we used

216
00:08:37,740 --> 00:08:45,000
to have cookie up to NextSameTx before

217
00:08:41,159 --> 00:08:47,880
NextSameRm and now they are all moved at

218
00:08:45,000 --> 00:08:50,760
the end cookie until NextSameTx you can

219
00:08:47,880 --> 00:08:52,920
see the offset that are below 88 and

220
00:08:50,760 --> 00:08:55,080
everything is shifted and what we do is

221
00:08:52,920 --> 00:08:57,959
we Define that as a new structure which

222
00:08:55,080 --> 00:09:00,180
we named KENLISTMENT_shifted_NextSameRm which

223
00:08:57,959 --> 00:09:01,740
is kind of playing the same role as the

224
00:09:00,180 --> 00:09:04,200
shifted pointer structure we used to

225
00:09:01,740 --> 00:09:05,880
define in IDA it is not as handy as the

226
00:09:04,200 --> 00:09:08,279
shifted pointer as we'll see in a second

227
00:09:05,880 --> 00:09:09,959
but it is good enough for most use cases

228
00:09:08,279 --> 00:09:12,540
and so if we look at the Ghidra

229
00:09:09,959 --> 00:09:17,160
decompiler output we had the negative

230
00:09:12,540 --> 00:09:19,740
offset like -5 or -9 and again I'm

231
00:09:17,160 --> 00:09:22,620
showing you the output after the changes

232
00:09:19,740 --> 00:09:24,720
and we see there is no -5 anymore and

233
00:09:22,620 --> 00:09:28,260
it's actually accessing a specific field

234
00:09:24,720 --> 00:09:30,180
so the -9 here is replaced with a flag

235
00:09:28,260 --> 00:09:32,100
and so after using the new shifted

236
00:09:30,180 --> 00:09:34,800
structure for the enlistment

237
00:09:32,100 --> 00:09:37,200
head address here this is before used to

238
00:09:34,800 --> 00:09:39,420
be a list entry and now it's actually a

239
00:09:37,200 --> 00:09:41,640
new structure defined we can see that

240
00:09:39,420 --> 00:09:43,740
all the accesses from the enlistment head

241
00:09:41,640 --> 00:09:45,540
address which is basically the

242
00:09:43,740 --> 00:09:47,220
pEnlistment_shifted here and then all the

243
00:09:45,540 --> 00:09:50,399
accesses for the pEnlistment_shifted

244
00:09:47,220 --> 00:09:52,500
are shown in a similar way to IDA we can

245
00:09:50,399 --> 00:09:55,320
see the access to flags with access to

246
00:09:52,500 --> 00:09:57,540
mutex another one mutex here flags and

247
00:09:55,320 --> 00:09:59,940
the state even the transaction field

248
00:09:57,540 --> 00:10:03,360
they all look better than before where

249
00:09:59,940 --> 00:10:05,760
we would have accesses to unknown Flink

250
00:10:03,360 --> 00:10:08,399
or blink plus offset due to the negative

251
00:10:05,760 --> 00:10:10,980
offset being undefined there is only one

252
00:10:08,399 --> 00:10:13,140
Quirk of this method which is that if it

253
00:10:10,980 --> 00:10:16,019
accesses an element in the structure

254
00:10:13,140 --> 00:10:18,959
before the NextSameRm field it will

255
00:10:16,019 --> 00:10:21,060
actually show in the decompiler as in

256
00:10:18,959 --> 00:10:22,920
-1 index so you can see here it's

257
00:10:21,060 --> 00:10:25,860
accessing the mutex there is a current

258
00:10:22,920 --> 00:10:29,339
enlistment -1 mutex under the current

259
00:10:25,860 --> 00:10:31,920
and enlistment -1 mutex here and it's as

260
00:10:29,339 --> 00:10:34,560
if it's showing the current enlistment

261
00:10:31,920 --> 00:10:36,420
as an array of this shifted KENLISTMENT structure

262
00:10:34,560 --> 00:10:38,700
and this is basically due to the fact

263
00:10:36,420 --> 00:10:41,160
that now it's working with a structure

264
00:10:38,700 --> 00:10:43,200
like this so even though we tricked it

265
00:10:41,160 --> 00:10:45,480
to point to the beginning of the the

266
00:10:43,200 --> 00:10:47,820
field being the NextSameRm the other

267
00:10:45,480 --> 00:10:49,680
fields are after so it's it's actually

268
00:10:47,820 --> 00:10:51,360
thinking about an array of this

269
00:10:49,680 --> 00:10:53,339
structure and it's accessing the

270
00:10:51,360 --> 00:10:55,620
previous one to be able to access the

271
00:10:53,339 --> 00:10:58,620
fields before but in general you can

272
00:10:55,620 --> 00:11:00,660
just ignore the -1 index thing and the code

273
00:10:58,620 --> 00:11:02,940
is still a lot more readable so one more

274
00:11:00,660 --> 00:11:05,100
thing we want is to be able to add

275
00:11:02,940 --> 00:11:07,380
structures in Ghidra in a more automated

276
00:11:05,100 --> 00:11:09,480
way than just defining them one by one

277
00:11:07,380 --> 00:11:11,760
with the GUI and so there is an easy way

278
00:11:09,480 --> 00:11:13,920
to import a C header file so you would

279
00:11:11,760 --> 00:11:17,100
typically Define all your KTM structures

280
00:11:13,920 --> 00:11:20,160
and dependencies into a single C header

281
00:11:17,100 --> 00:11:23,100
file that we name KTM types.h for

282
00:11:20,160 --> 00:11:25,260
instance and you would use Vergilius to

283
00:11:23,100 --> 00:11:27,720
find all the KTM structures and their

284
00:11:25,260 --> 00:11:29,579
dependencies it does take some time to

285
00:11:27,720 --> 00:11:31,800
build such a header file but once you

286
00:11:29,579 --> 00:11:34,560
have done it the advantage is you can

287
00:11:31,800 --> 00:11:36,480
import it into any binary file from any

288
00:11:34,560 --> 00:11:38,459
version and you'll get all the types

289
00:11:36,480 --> 00:11:41,160
imported so for instance you can import

290
00:11:38,459 --> 00:11:43,500
it in both the vulnerable and patch file

291
00:11:41,160 --> 00:11:45,779
in Ghidra and so you can you can go into

292
00:11:43,500 --> 00:11:48,600
the file menu in the code browser and

293
00:11:45,779 --> 00:11:50,459
Ghidra select a given configuration that

294
00:11:48,600 --> 00:11:52,560
you want to extend so typically for

295
00:11:50,459 --> 00:11:54,540
Windows related stuff you would use one

296
00:11:52,560 --> 00:11:56,640
with Visual Studio in the name and the

297
00:11:54,540 --> 00:11:58,920
advantage of using a good base

298
00:11:56,640 --> 00:12:00,899
configuration is that it will already

299
00:11:58,920 --> 00:12:02,880
have some known types that you don't

300
00:12:00,899 --> 00:12:06,360
need to Define and then you click on the

301
00:12:02,880 --> 00:12:08,459
plus sign and select your KTM types file

302
00:12:06,360 --> 00:12:10,560
you want to import and it will import

303
00:12:08,459 --> 00:12:13,019
all the structures from that file when

304
00:12:10,560 --> 00:12:15,660
you click on parse to program button

305
00:12:13,019 --> 00:12:16,920
all of them will be imported into the

306
00:12:15,660 --> 00:12:18,899
context of the file that you are

307
00:12:16,920 --> 00:12:21,480
reversing so it's pretty straightforward

308
00:12:18,899 --> 00:12:23,820
and fast to do once you know how to do

309
00:12:21,480 --> 00:12:26,160
it and one last thing to say

310
00:12:23,820 --> 00:12:28,579
that this approach is useful for IDA

311
00:12:26,160 --> 00:12:28,579
as well

