1
00:00:00,420 --> 00:00:05,279
hi everyone in this video we'll go over

2
00:00:03,300 --> 00:00:07,680
kernel pool manipulations techniques

3
00:00:05,279 --> 00:00:11,340
basically now we have this scenario

4
00:00:07,680 --> 00:00:13,620
where we can have a free chunk used and

5
00:00:11,340 --> 00:00:16,980
we want to figure out a way to replace

6
00:00:13,620 --> 00:00:19,320
it with data that we control before it

7
00:00:16,980 --> 00:00:21,240
is actually reused and we know it's on

8
00:00:19,320 --> 00:00:23,820
the non-page pool so we need to figure

9
00:00:21,240 --> 00:00:26,939
out some tricks to manipulate the

10
00:00:23,820 --> 00:00:30,119
non-page pool in order to get our free

11
00:00:26,939 --> 00:00:32,160
Chunk in a certain area on the heap in

12
00:00:30,119 --> 00:00:33,480
order to be able to reuse it in an

13
00:00:32,160 --> 00:00:35,160
efficient way

14
00:00:33,480 --> 00:00:37,920
so in this part we're going to cover

15
00:00:35,160 --> 00:00:40,140
different things in order to have a

16
00:00:37,920 --> 00:00:42,059
better layout on the pool the first

17
00:00:40,140 --> 00:00:45,000
thing we're going to look at is how we

18
00:00:42,059 --> 00:00:46,800
can manipulate the the poor within two

19
00:00:45,000 --> 00:00:49,920
different techniques the first one is

20
00:00:46,800 --> 00:00:52,320
spraying the heep as much as we can to

21
00:00:49,920 --> 00:00:54,180
get deterministic layout and determine

22
00:00:52,320 --> 00:00:56,219
heap address

23
00:00:54,180 --> 00:00:58,020
and the other one which is heap feng shui

24
00:00:56,219 --> 00:01:00,420
 which relies on

25
00:00:58,020 --> 00:01:03,660
manipulating the heap in a more

26
00:01:00,420 --> 00:01:05,640
efficient and precise way then we're

27
00:01:03,660 --> 00:01:08,280
going to look into the different

28
00:01:05,640 --> 00:01:11,040
mitigations that make it more difficult

29
00:01:08,280 --> 00:01:13,920
to manipulate the heap and one of them

30
00:01:11,040 --> 00:01:16,740
is called delayed free and we'll see how

31
00:01:13,920 --> 00:01:20,100
when we try to free a chunk it doesn't

32
00:01:16,740 --> 00:01:23,580
get freed right away and hard to bypass

33
00:01:20,100 --> 00:01:25,860
that mechanism then we look into how we

34
00:01:23,580 --> 00:01:28,820
can manipulate the heap by creating

35
00:01:25,860 --> 00:01:32,460
holes in order to get our KENLISTMENT

36
00:01:28,820 --> 00:01:35,520
allocated into these specific holes and

37
00:01:32,460 --> 00:01:38,100
what are the advantages of doing so and

38
00:01:35,520 --> 00:01:39,659
also we're going to investigate how to

39
00:01:38,100 --> 00:01:42,600
manipulate the heap without creating

40
00:01:39,659 --> 00:01:46,619
holes and what are the differences finally

41
00:01:42,600 --> 00:01:50,280
we look into what kind of data we want

42
00:01:46,619 --> 00:01:53,220
to use to replace the KENLISTMENT

43
00:01:50,280 --> 00:01:56,280
after we trigger the bug in order to

44
00:01:53,220 --> 00:01:58,860
control the fake enlistment for the next

45
00:01:56,280 --> 00:02:00,000
iteration of the loop okay let's get

46
00:01:58,860 --> 00:02:02,220
started

47
00:02:00,000 --> 00:02:04,500
in general there are two common heap

48
00:02:02,220 --> 00:02:07,500
manipulation techniques that you'll hear

49
00:02:04,500 --> 00:02:09,959
about one is heap spraying and the other

50
00:02:07,500 --> 00:02:12,599
one is heap feng shui so if we focus on

51
00:02:09,959 --> 00:02:15,000
the first one which is the heap spraying

52
00:02:12,599 --> 00:02:18,000
technique the idea is basically you just

53
00:02:15,000 --> 00:02:20,340
allocate a ton of stuff on the heap and

54
00:02:18,000 --> 00:02:23,040
generally the end goal is to have some

55
00:02:20,340 --> 00:02:25,020
predictable address allocated and

56
00:02:23,040 --> 00:02:27,900
generally that technique might help you

57
00:02:25,020 --> 00:02:30,300
bypass ASLR because from now on you know

58
00:02:27,900 --> 00:02:33,060
there is something you control at a

59
00:02:30,300 --> 00:02:35,400
specific address and then the second

60
00:02:33,060 --> 00:02:37,260
technique is a more precise and

61
00:02:35,400 --> 00:02:40,860
interesting technique which people

62
00:02:37,260 --> 00:02:43,800
typically call heap feng shui which is

63
00:02:40,860 --> 00:02:46,200
based off this Chinese term that you can

64
00:02:43,800 --> 00:02:48,660
look up and maybe it will make sense why

65
00:02:46,200 --> 00:02:51,180
they call it heap feng shui so both

66
00:02:48,660 --> 00:02:53,459
techniques are heavily reliant on the

67
00:02:51,180 --> 00:02:56,040
actual software you're targeting so like

68
00:02:53,459 --> 00:02:58,560
brothers typically have some JavaScript

69
00:02:56,040 --> 00:03:01,620
functionality that people like to use

70
00:02:58,560 --> 00:03:04,739
with like certain type of arrays or

71
00:03:01,620 --> 00:03:07,200
back in the days we would basically load

72
00:03:04,739 --> 00:03:09,900
lots of images to get a deterministic

73
00:03:07,200 --> 00:03:13,200
address and bypass ASLR and that would

74
00:03:09,900 --> 00:03:16,280
allow you to spray memory and Kernels

75
00:03:13,200 --> 00:03:18,540
have their own sort of

76
00:03:16,280 --> 00:03:21,120
semi-established techniques but

77
00:03:18,540 --> 00:03:23,220
sometimes it's very situation specific

78
00:03:21,120 --> 00:03:25,379
and so in general for the spray

79
00:03:23,220 --> 00:03:27,599
technique the Baseline idea of what you

80
00:03:25,379 --> 00:03:30,360
want is some code path that you can call

81
00:03:27,599 --> 00:03:33,360
in our case in the kernel or whatever

82
00:03:30,360 --> 00:03:36,780
you're dealing with where you control as

83
00:03:33,360 --> 00:03:39,420
much data as possible and ideally you

84
00:03:36,780 --> 00:03:41,700
can allocate chunks over and over and

85
00:03:39,420 --> 00:03:44,040
over again without a limit for the

86
00:03:41,700 --> 00:03:46,560
spray technique that's basically all

87
00:03:44,040 --> 00:03:48,420
you need you don't care about freeing

88
00:03:46,560 --> 00:03:51,060
you don't care about anything else

89
00:03:48,420 --> 00:03:53,280
finding heap feng shui candidates is a

90
00:03:51,060 --> 00:03:55,319
little bit harder because you generally

91
00:03:53,280 --> 00:03:57,780
want to be able to manipulate things

92
00:03:55,319 --> 00:03:59,700
with a little more granularity so you

93
00:03:57,780 --> 00:04:02,340
want to be able to typically control the

94
00:03:59,700 --> 00:04:04,860
size of the allegation or have a few

95
00:04:02,340 --> 00:04:07,080
options for the size of the allocation

96
00:04:04,860 --> 00:04:09,239
because for something like a use after

97
00:04:07,080 --> 00:04:11,939
free you might be replacing something

98
00:04:09,239 --> 00:04:14,939
like a KENLISTMENT structure which is

99
00:04:11,939 --> 00:04:18,000
of a specific size so you want to find

100
00:04:14,939 --> 00:04:21,000
some sort of heap manipulation code path

101
00:04:18,000 --> 00:04:22,800
that lets you specify a size that

102
00:04:21,000 --> 00:04:25,320
matches the target structure that you

103
00:04:22,800 --> 00:04:27,120
want to replace which in this case would

104
00:04:25,320 --> 00:04:30,840
match the size of the KENLISTMENT

105
00:04:27,120 --> 00:04:33,120
allocation and situationally this can be

106
00:04:30,840 --> 00:04:35,639
really hard and there's lots of

107
00:04:33,120 --> 00:04:38,040
complicated tricks to get around it and

108
00:04:35,639 --> 00:04:40,500
sometimes the techniques are documented

109
00:04:38,040 --> 00:04:43,199
here and there in public exploits or

110
00:04:40,500 --> 00:04:45,479
papers so it really depends sometimes

111
00:04:43,199 --> 00:04:48,180
you need to research it yourself most of

112
00:04:45,479 --> 00:04:49,979
the time you also ideally want a way to

113
00:04:48,180 --> 00:04:52,380
free the chunks because with something

114
00:04:49,979 --> 00:04:54,600
like feng shui you often need to really

115
00:04:52,380 --> 00:04:57,419
manipulate the layout it's not just

116
00:04:54,600 --> 00:04:59,580
spraying a ton of memory sometimes it's

117
00:04:57,419 --> 00:05:02,400
creating holes that you can fill with

118
00:04:59,580 --> 00:05:04,560
other data later and stuff like that and

119
00:05:02,400 --> 00:05:06,840
you still also ideally want a way to

120
00:05:04,560 --> 00:05:08,580
control the contents of the chunks that

121
00:05:06,840 --> 00:05:10,680
you are allocating as much as possible

122
00:05:08,580 --> 00:05:13,139
and so the perfect feng shui candidate

123
00:05:10,680 --> 00:05:15,960
would be a single function or a couple

124
00:05:13,139 --> 00:05:18,900
of functions that let you do all of that

125
00:05:15,960 --> 00:05:21,360
but it's not always that simple and so

126
00:05:18,900 --> 00:05:23,940
originally we didn't know which

127
00:05:21,360 --> 00:05:26,100
KENLISTMENT to free so the idea was to

128
00:05:23,940 --> 00:05:29,100
just free all of the KENLISTMENTs

129
00:05:26,100 --> 00:05:31,800
and replace like a ton of memory with

130
00:05:29,100 --> 00:05:33,900
whatever really big allocations that

131
00:05:31,800 --> 00:05:36,780
didn't necessarily have to match the

132
00:05:33,900 --> 00:05:38,400
size of the KENLISTMENT itself and part

133
00:05:36,780 --> 00:05:40,979
of the problem with something like that

134
00:05:38,400 --> 00:05:43,320
is if you're freeing a whole bunch of

135
00:05:40,979 --> 00:05:46,620
KENLISTMENT at once there is this

136
00:05:43,320 --> 00:05:49,380
concept in heaps where if you have one

137
00:05:46,620 --> 00:05:51,840
in use chunk and another in use chunk

138
00:05:49,380 --> 00:05:54,060
just after or just before in memory and

139
00:05:51,840 --> 00:05:56,340
then you free one and then you free the

140
00:05:54,060 --> 00:05:59,280
other the heap manager or algorithm

141
00:05:56,340 --> 00:06:02,759
tries to be clever and combine those two

142
00:05:59,280 --> 00:06:05,100
free chains into a one bigger free chunk

143
00:06:02,759 --> 00:06:07,620
and so then if you reallocated that

144
00:06:05,100 --> 00:06:09,479
memory the start of the new chunk that

145
00:06:07,620 --> 00:06:11,940
you allocate would be at a different

146
00:06:09,479 --> 00:06:13,979
location that you might expect and so

147
00:06:11,940 --> 00:06:16,979
this type of scenario they call it

148
00:06:13,979 --> 00:06:18,900
coalescing is not ideal especially in

149
00:06:16,979 --> 00:06:21,240
our case where we specifically want to

150
00:06:18,900 --> 00:06:23,880
control something like the flink pointer

151
00:06:21,240 --> 00:06:26,460
of the KENLISTMENT structure so if we

152
00:06:23,880 --> 00:06:28,139
just free them all and we replace a

153
00:06:26,460 --> 00:06:30,360
whole bunch of memory we don't know

154
00:06:28,139 --> 00:06:33,060
exactly where to put something like a

155
00:06:30,360 --> 00:06:35,220
flink pointer in memory necessarily when

156
00:06:33,060 --> 00:06:38,160
we replace the chunk so generally when

157
00:06:35,220 --> 00:06:40,560
doing heap feng shui you want something a

158
00:06:38,160 --> 00:06:44,520
little bit more precise and you want to

159
00:06:40,560 --> 00:06:46,319
avoid that coalescing happens there is a

160
00:06:44,520 --> 00:06:48,419
bunch of mitigation that have been added

161
00:06:46,319 --> 00:06:51,300
to kernel pools over time fortunately

162
00:06:48,419 --> 00:06:53,639
almost none of them relate to our

163
00:06:51,300 --> 00:06:56,880
scenario and so you can refer to the

164
00:06:53,639 --> 00:06:59,580
arch 2021 course for more details on

165
00:06:56,880 --> 00:07:02,160
these mitigations or look up kernel pool

166
00:06:59,580 --> 00:07:04,500
mitigations on Google or whatever

167
00:07:02,160 --> 00:07:07,620
and you will find lots of papers but

168
00:07:04,500 --> 00:07:11,460
basically all the mitigation that try to

169
00:07:07,620 --> 00:07:14,819
encode pointers add cookies or check

170
00:07:11,460 --> 00:07:16,560
bounds when accessing them are trying to

171
00:07:14,819 --> 00:07:18,960
prevent memory corruption

172
00:07:16,560 --> 00:07:21,419
vulnerabilities like buffer overflows

173
00:07:18,960 --> 00:07:23,759
but because in our case it is a use

174
00:07:21,419 --> 00:07:26,759
after free and we said our goal is to replace

175
00:07:23,759 --> 00:07:29,220
the KENLISTMENT with as much control

176
00:07:26,759 --> 00:07:32,639
data as possible what will really matter

177
00:07:29,220 --> 00:07:34,979
is what control we have over the KENLISTMENT

178
00:07:32,639 --> 00:07:37,380
fields and how this KENLISTMENT

179
00:07:34,979 --> 00:07:40,259
is used afterwards and because we are

180
00:07:37,380 --> 00:07:42,360
not corrupting pool metadata like you

181
00:07:40,259 --> 00:07:44,160
would typically do in a buffer overflow

182
00:07:42,360 --> 00:07:47,060
scenario we don't have to worry about

183
00:07:44,160 --> 00:07:50,340
most of them really the only mitigation

184
00:07:47,060 --> 00:07:53,520
that affects us is something called

185
00:07:50,340 --> 00:07:55,880
delayed freed and we'll go over it in

186
00:07:53,520 --> 00:07:55,880
the next video

