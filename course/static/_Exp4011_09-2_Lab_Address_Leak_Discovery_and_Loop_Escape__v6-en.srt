1
00:00:03,179 --> 00:00:07,440
hi everyone

2
00:00:04,860 --> 00:00:09,660
in this video you are going to do

3
00:00:07,440 --> 00:00:11,519
something really fun

4
00:00:09,660 --> 00:00:12,240
you're going to basically work with a

5
00:00:11,519 --> 00:00:14,700
lab

6
00:00:12,240 --> 00:00:17,580
we're going to be able to leak kernel

7
00:00:14,700 --> 00:00:19,260
addresses to userland and then once you

8
00:00:17,580 --> 00:00:22,920
have done that you're going to be able

9
00:00:19,260 --> 00:00:26,760
to exit the kernel whenever you want

10
00:00:22,920 --> 00:00:28,500
and return to userland okay let's get

11
00:00:26,760 --> 00:00:31,800
started

12
00:00:28,500 --> 00:00:35,460
for this lab we still have most of the

13
00:00:31,800 --> 00:00:38,460
helper C files seen previously to craft

14
00:00:35,460 --> 00:00:41,399
the KTM object to get the good Factory

15
00:00:38,460 --> 00:00:42,660
layout using named pipes and our spray

16
00:00:41,399 --> 00:00:45,300
Enlistment

17
00:00:42,660 --> 00:00:47,760
and helpers to trigger the bag with the

18
00:00:45,300 --> 00:00:51,000
recovery thread but now we have a new

19
00:00:47,760 --> 00:00:54,059
helper file called enlistment.c

20
00:00:51,000 --> 00:00:56,520
which has code to build the Trap

21
00:00:54,059 --> 00:00:59,879
enlistment and leak enlistment

22
00:00:56,520 --> 00:01:03,120
respectively to track the kernel and to

23
00:00:59,879 --> 00:01:06,720
leak kernel pointers to userland the

24
00:01:03,120 --> 00:01:08,820
lab's name is escaping loop the idea is

25
00:01:06,720 --> 00:01:10,860
to focus on analyzing what kernel

26
00:01:08,820 --> 00:01:13,979
addresses are written to our fake

27
00:01:10,860 --> 00:01:16,080
userland structures that we control

28
00:01:13,979 --> 00:01:18,000
and consequently what kernel address we

29
00:01:16,080 --> 00:01:21,240
can leak from userland we will use

30
00:01:18,000 --> 00:01:23,220
the !patch command to assist us in

31
00:01:21,240 --> 00:01:26,100
winning the race condition so we can

32
00:01:23,220 --> 00:01:28,560
focus on the information leak part we

33
00:01:26,100 --> 00:01:31,200
provide the code to build a fake leak

34
00:01:28,560 --> 00:01:33,720
enlistments and it will allow us to

35
00:01:31,200 --> 00:01:35,400
focus on stepping the debugger to

36
00:01:33,720 --> 00:01:38,460
understand what part of the code

37
00:01:35,400 --> 00:01:40,680
actually writes calend addresses to our

38
00:01:38,460 --> 00:01:42,720
fake userland enlistments and then we

39
00:01:40,680 --> 00:01:45,600
will be able to modify the code of the

40
00:01:42,720 --> 00:01:48,900
exploits to leak the useful information

41
00:01:45,600 --> 00:01:51,659
from userland and finally we should

42
00:01:48,900 --> 00:01:54,600
be able to inject and escape enlistment

43
00:01:51,659 --> 00:01:55,560
with the right next samerm flink

44
00:01:54,600 --> 00:01:58,380
pointer

45
00:01:55,560 --> 00:02:00,780
set to the k resource manager

46
00:01:58,380 --> 00:02:03,000
enlistment head address in order to exit

47
00:02:00,780 --> 00:02:04,979
the loop even though in this lab we are

48
00:02:03,000 --> 00:02:09,179
patching using winback to win the race

49
00:02:04,979 --> 00:02:12,420
just experiencing using the superior

50
00:02:09,179 --> 00:02:14,879
trick is useful because it's the same

51
00:02:12,420 --> 00:02:16,680
trick we will use once we start patching

52
00:02:14,879 --> 00:02:18,900
in the debugger and we start to

53
00:02:16,680 --> 00:02:21,120
effectively win the right condition as

54
00:02:18,900 --> 00:02:24,540
usual you can follow the instructions

55
00:02:21,120 --> 00:02:27,660
printed when exiting the binary you can

56
00:02:24,540 --> 00:02:28,860
reuse the WinDbg command you use in the

57
00:02:27,660 --> 00:02:31,379
previous lab

58
00:02:28,860 --> 00:02:34,080
as well as the breakpoints related to

59
00:02:31,379 --> 00:02:37,140
the superior enlistment that allows

60
00:02:34,080 --> 00:02:39,300
debugging after we won the race

61
00:02:37,140 --> 00:02:42,120
and then you should be able to step in

62
00:02:39,300 --> 00:02:44,280
the debugger and see where kernel

63
00:02:42,120 --> 00:02:46,379
pointers are written in your fake userland

64
00:02:44,280 --> 00:02:49,319
enlistments this basically means

65
00:02:46,379 --> 00:02:50,760
that your fake userland
enlistment is modified

66
00:02:49,319 --> 00:02:53,519
by the kernel

67
00:02:50,760 --> 00:02:56,120
okay so we're interested in the escaping

68
00:02:53,519 --> 00:02:56,120
loop lab

69
00:02:58,500 --> 00:03:03,360
so if we start from the main function we

70
00:03:00,780 --> 00:03:06,959
see we have the high level goals which

71
00:03:03,360 --> 00:03:09,420
is basically to analyze the kenlistments

72
00:03:06,959 --> 00:03:11,519
means that has been touched by the

73
00:03:09,420 --> 00:03:12,840
kernel and to see that some general

74
00:03:11,519 --> 00:03:17,099
addresses

75
00:03:12,840 --> 00:03:20,220
are actually returned
into these kenlistments

76
00:03:17,099 --> 00:03:24,659
so as usual we have code to initialize

77
00:03:20,220 --> 00:03:27,599
our export vars global

78
00:03:24,659 --> 00:03:29,940
then we have code to count the number of

79
00:03:27,599 --> 00:03:32,700
CPU cores to be able to create different

80
00:03:29,940 --> 00:03:34,340
shredding on different CPU cores then we

81
00:03:32,700 --> 00:03:38,879
have the code to actually

82
00:03:34,340 --> 00:03:41,879
initialize the fake enlistments so these

83
00:03:38,879 --> 00:03:44,220
fake enlistments are all the userland

84
00:03:41,879 --> 00:03:47,340
enlistments that we create in userland

85
00:03:44,220 --> 00:03:49,140
in order to create this chain of

86
00:03:47,340 --> 00:03:51,799
fake userland enlistments and it

87
00:03:49,140 --> 00:03:54,659
start this spray enlistment that we use for

88
00:03:51,799 --> 00:03:56,340
replacing the free chunk before we

89
00:03:54,659 --> 00:04:00,239
trigger the user to free the one we

90
00:03:56,340 --> 00:04:02,819
actually spray using named pipes then we

91
00:04:00,239 --> 00:04:05,220
have the leak enlistment which is

92
00:04:02,819 --> 00:04:08,879
basically used to leak kernel pointers

93
00:04:05,220 --> 00:04:11,220
the one we have added into this lab

94
00:04:08,879 --> 00:04:13,500
compared to the previous slabs and this

95
00:04:11,220 --> 00:04:16,380
leak enlistment will allow us to

96
00:04:13,500 --> 00:04:18,720
leak kernel addresses to userland in

97
00:04:16,380 --> 00:04:21,120
order to escape the loop and then we have

98
00:04:18,720 --> 00:04:25,560
the Trap enlistment which is basically

99
00:04:21,120 --> 00:04:28,199
making sure the kernel is stuck

100
00:04:25,560 --> 00:04:30,600
um and it doesn't crash so we can see

101
00:04:28,199 --> 00:04:32,460
the three different enlistments are

102
00:04:30,600 --> 00:04:34,740
initialized by calling three different

103
00:04:32,460 --> 00:04:38,280
functions all these functions are

104
00:04:34,740 --> 00:04:40,259
defined into the enlistment.c file so if

105
00:04:38,280 --> 00:04:43,560
we look at the init trap enlistment we

106
00:04:40,259 --> 00:04:46,560
see it all it does is it calls initial

107
00:04:43,560 --> 00:04:48,060
and can spends passing null so this

108
00:04:46,560 --> 00:04:52,259
function is actually a generic function

109
00:04:48,060 --> 00:04:56,639
to build a userland enlistment and it sets

110
00:04:52,259 --> 00:04:59,280
specific flags and elements in the

111
00:04:56,639 --> 00:05:02,460
enlistment to make sure it's gonna work

112
00:04:59,280 --> 00:05:06,960
in the kernel so we can see that if we

113
00:05:02,460 --> 00:05:10,380
pass null actually at the Flink

114
00:05:06,960 --> 00:05:12,479
on the arguments which is actually named

115
00:05:10,380 --> 00:05:15,660
Flink_Enlistment

116
00:05:12,479 --> 00:05:18,419
it can actually make it point to itself

117
00:05:15,660 --> 00:05:20,460
so let's see what it does we've seen it

118
00:05:18,419 --> 00:05:23,280
before but basically it set the flags to

119
00:05:20,460 --> 00:05:25,740
Superior and is notifiable

120
00:05:23,280 --> 00:05:28,500
so Superior to be able to reach these

121
00:05:25,740 --> 00:05:32,520
breakpoints that is Handy if you want to

122
00:05:28,500 --> 00:05:34,139
debug post race is notifiable as well so

123
00:05:32,520 --> 00:05:38,940
we can hit that code and also because

124
00:05:34,139 --> 00:05:40,259
then the enlistment will be sent a

125
00:05:38,940 --> 00:05:43,560
notification

126
00:05:40,259 --> 00:05:46,380
then we have the count

127
00:05:43,560 --> 00:05:49,440
for the object header then we have

128
00:05:46,380 --> 00:05:53,300
certain flags and values for the actual

129
00:05:49,440 --> 00:05:53,300
mutex that is part of the Enlistment

130
00:05:55,080 --> 00:06:01,380
so here we see if the Flink enlistment

131
00:05:58,280 --> 00:06:03,600
argument passed is not null we're

132
00:06:01,380 --> 00:06:05,639
going to set we're going to use that to

133
00:06:03,600 --> 00:06:07,680
set the next pointer like the flame

134
00:06:05,639 --> 00:06:09,479
pointer and if it's not set then we're

135
00:06:07,680 --> 00:06:11,460
going to actually make the enlistment

136
00:06:09,479 --> 00:06:13,979
points to itself so it's going to be an

137
00:06:11,460 --> 00:06:17,240
actual trap enlistment and then we see

138
00:06:13,979 --> 00:06:19,560
that we Define the actual transaction

139
00:06:17,240 --> 00:06:22,080
for the enlistment as well as the

140
00:06:19,560 --> 00:06:24,840
states of the transaction

141
00:06:22,080 --> 00:06:27,840
and the transaction had been allocated

142
00:06:24,840 --> 00:06:27,840
beforehand

143
00:06:29,340 --> 00:06:33,800
and so we written that created an

144
00:06:31,680 --> 00:06:33,800
enlistment

145
00:06:34,440 --> 00:06:39,660
so yeah it's a trap enlistment we'll

146
00:06:37,620 --> 00:06:41,699
just create a trap enlistment that points

147
00:06:39,660 --> 00:06:44,220
to itself then we have
the leak enlistmentns

148
00:06:41,699 --> 00:06:46,620
so this is a new function and

149
00:06:44,220 --> 00:06:49,139
we can see we are parsing the actual

150
00:06:46,620 --> 00:06:50,520
next enlistments so because the leak and

151
00:06:49,139 --> 00:06:52,979
enlistment will point to the Trap

152
00:06:50,520 --> 00:06:54,300
enlistment so this function will allow us

153
00:06:52,979 --> 00:06:56,639
to leak

154
00:06:54,300 --> 00:06:58,560
an address relative to the k resource

155
00:06:56,639 --> 00:07:01,400
manager and another address related to

156
00:06:58,560 --> 00:07:01,400
the k thread

157
00:07:01,860 --> 00:07:06,240
so it uses

158
00:07:03,960 --> 00:07:07,740
two variables that are incremented the

159
00:07:06,240 --> 00:07:10,680
first one is single page which is

160
00:07:07,740 --> 00:07:13,319
basically a pointer in userland that

161
00:07:10,680 --> 00:07:16,380
will hold all different structures

162
00:07:13,319 --> 00:07:18,419
and values we need in userland so

163
00:07:16,380 --> 00:07:20,819
it's actually valid and parsed by the

164
00:07:18,419 --> 00:07:23,220
the kernel and then there is a single

165
00:07:20,819 --> 00:07:25,080
page off which stands for offset which

166
00:07:23,220 --> 00:07:26,940
is basically the current offset in the

167
00:07:25,080 --> 00:07:31,340
single page that we are currently

168
00:07:26,940 --> 00:07:31,340
modifying so we know we can append stuff

169
00:07:33,960 --> 00:07:40,919
so we have a KWAIT_BLOCK8 structure

170
00:07:37,380 --> 00:07:44,940
that will detail below one thing we make

171
00:07:40,919 --> 00:07:46,699
sure is we make sure we reserve and

172
00:07:44,940 --> 00:07:51,360
commit these

173
00:07:46,699 --> 00:07:52,319
allocation of 1000 hex bytes which is a

174
00:07:51,360 --> 00:07:54,539
page

175
00:07:52,319 --> 00:07:57,599
so 4K

176
00:07:54,539 --> 00:07:58,979
um bytes and the reason we do that is

177
00:07:57,599 --> 00:08:02,940
because we want to make sure when the

178
00:07:58,979 --> 00:08:07,080
kernel actually parses this page it

179
00:08:02,940 --> 00:08:10,020
won't actually trigger page Folds and so

180
00:08:07,080 --> 00:08:12,479
we want to make sure that all the data

181
00:08:10,020 --> 00:08:16,139
are in the same page because otherwise

182
00:08:12,479 --> 00:08:17,639
the page could possibly not be mapped

183
00:08:16,139 --> 00:08:19,860
and it would trigger a page fold and

184
00:08:17,639 --> 00:08:21,780
because we the write primitive and all

185
00:08:19,860 --> 00:08:24,599
the code that is executed is at dispatch

186
00:08:21,780 --> 00:08:26,940
level it would crash so we have the

187
00:08:24,599 --> 00:08:29,520
offset set to zero

188
00:08:26,940 --> 00:08:33,419
so we start by allocating an object

189
00:08:29,520 --> 00:08:35,940
header as well as the kenlistment on

190
00:08:33,419 --> 00:08:41,159
when I say allocate I mean we reserve

191
00:08:35,940 --> 00:08:44,300
different section in this page so we set

192
00:08:41,159 --> 00:08:47,760
the object header count

193
00:08:44,300 --> 00:08:49,560
we start by getting a pointer to the

194
00:08:47,760 --> 00:08:52,080
actual mutex embedded into the

195
00:08:49,560 --> 00:08:54,959
enlistments and we set it to a mutant

196
00:08:52,080 --> 00:08:56,360
object and a single state of one then we

197
00:08:54,959 --> 00:08:59,220
reserve

198
00:08:56,360 --> 00:09:01,640
some part for the KWAIT_BLOCK8

199
00:08:59,220 --> 00:09:05,040
structure that we'll explain just after

200
00:09:01,640 --> 00:09:07,860
as well as a section for the k  thread

201
00:09:05,040 --> 00:09:10,260
so one thing is that the the actual k

202
00:09:07,860 --> 00:09:13,200
thread and actually is
e thread structure which

203
00:09:10,260 --> 00:09:16,800
embed the k thread is more than 200

204
00:09:13,200 --> 00:09:18,839
hex byte but we don't need the actual

205
00:09:16,800 --> 00:09:21,180
fields that are at the end of this

206
00:09:18,839 --> 00:09:23,220
structure so we actually just Reserve

207
00:09:21,180 --> 00:09:24,899
200 bytes and the reason we do that is

208
00:09:23,220 --> 00:09:27,240
because we want to make sure all the

209
00:09:24,899 --> 00:09:30,540
structures and Fields we need

210
00:09:27,240 --> 00:09:32,880
actually fit into this one page that I

211
00:09:30,540 --> 00:09:36,180
explained earlier

212
00:09:32,880 --> 00:09:40,080
then we actually built the KWAIT_BLOCK8

213
00:09:36,180 --> 00:09:42,180
structure so we Define a valid

214
00:09:40,080 --> 00:09:44,519
thread  pointer that points to your k

215
00:09:42,180 --> 00:09:47,279
thread as well as an object that points

216
00:09:44,519 --> 00:09:50,339
to the actual mutant object and finally

217
00:09:47,279 --> 00:09:52,740
we update the waitlist head

218
00:09:50,339 --> 00:09:55,740
Flink and blink pointer to make them

219
00:09:52,740 --> 00:09:59,160
point to the actualKWAIT_BLOCK8

220
00:09:55,740 --> 00:10:01,680
structure so then we test that

221
00:09:59,160 --> 00:10:03,899
the flame pointer that was passed by as

222
00:10:01,680 --> 00:10:06,180
an argument is actually not null if

223
00:10:03,899 --> 00:10:08,880
it's not null we actually assume it was

224
00:10:06,180 --> 00:10:12,180
passed and it would need to be used for

225
00:10:08,880 --> 00:10:14,220
the Flink pointer but if it was null we

226
00:10:12,180 --> 00:10:16,440
actually set it as a trap enlistment so

227
00:10:14,220 --> 00:10:20,580
then again we set the flags to Superior

228
00:10:16,440 --> 00:10:22,920
and notifiable as usual then we Define a

229
00:10:20,580 --> 00:10:26,700
transaction as well in the in the area

230
00:10:22,920 --> 00:10:28,920
and we make sure this transaction has a

231
00:10:26,700 --> 00:10:31,620
state set to K transaction in doubt and

232
00:10:28,920 --> 00:10:34,680
we set the transaction field for the

233
00:10:31,620 --> 00:10:37,260
enlistments and this test is just uh to

234
00:10:34,680 --> 00:10:42,060
make sure we haven't gone over the the

235
00:10:37,260 --> 00:10:45,060
4K bytes for the page

236
00:10:42,060 --> 00:10:45,060
So basically the leak enlistment

237
00:10:48,740 --> 00:10:55,440
is not created and all the fields are

238
00:10:52,560 --> 00:10:57,540
have already been defined for you to

239
00:10:55,440 --> 00:11:00,079
test so you won't have to actually

240
00:10:57,540 --> 00:11:02,519
modify anything in the leakenlistment

241
00:11:00,079 --> 00:11:06,000
and all you will have to do is actually

242
00:11:02,519 --> 00:11:08,279
debug and analyze what

243
00:11:06,000 --> 00:11:09,899
side effects this leak enlistment

244
00:11:08,279 --> 00:11:11,640
do

245
00:11:09,899 --> 00:11:14,399
um and what

246
00:11:11,640 --> 00:11:19,160
you can get out of it and we'll explain

247
00:11:14,399 --> 00:11:23,220
later how this leak enlistment actually

248
00:11:19,160 --> 00:11:25,560
gets these Canal pointers written to

249
00:11:23,220 --> 00:11:29,279
this spec enlistment in a future video

250
00:11:25,560 --> 00:11:33,120
but obviously if you debug in win bag

251
00:11:29,279 --> 00:11:34,980
you can try to find out yourself why it

252
00:11:33,120 --> 00:11:37,680
actually works the next enlistment

253
00:11:34,980 --> 00:11:39,839
that is initializing spray enlistments

254
00:11:37,680 --> 00:11:41,399
so we pass the following enlistment

255
00:11:39,839 --> 00:11:42,899
which is the leak enlistment because we

256
00:11:41,399 --> 00:11:44,880
want this spray enlistment to point to the

257
00:11:42,899 --> 00:11:48,480
leak enlistment we give the size to

258
00:11:44,880 --> 00:11:49,860
allocate for for it to actually allocate

259
00:11:48,480 --> 00:11:51,740
into

260
00:11:49,860 --> 00:11:55,380
an actual named pipe

261
00:11:51,740 --> 00:11:58,140
that results into a kenlistment size and

262
00:11:55,380 --> 00:12:00,240
this is actually the offsets because if

263
00:11:58,140 --> 00:12:03,060
you remember we passed the kenlistment

264
00:12:00,240 --> 00:12:06,120
offset which is basically the offset in

265
00:12:03,060 --> 00:12:08,940
the named pipe that would allow us to

266
00:12:06,120 --> 00:12:11,640
have good alignment for the all of the

267
00:12:08,940 --> 00:12:14,279
enlistment fields

268
00:12:11,640 --> 00:12:16,800
so if we look at this function

269
00:12:14,279 --> 00:12:19,620
there is a lot of explanation but

270
00:12:16,800 --> 00:12:23,220
basically what you can see is the offset

271
00:12:19,620 --> 00:12:24,959
has to take into account the actual

272
00:12:23,220 --> 00:12:27,360
kenlistment

273
00:12:24,959 --> 00:12:29,339
structures themselves that are before

274
00:12:27,360 --> 00:12:30,720
kenlistment so the object header quota

275
00:12:29,339 --> 00:12:32,640
info and the object header that are

276
00:12:30,720 --> 00:12:35,820
before the kenlistment but also we have

277
00:12:32,640 --> 00:12:38,459
to subtract the data entry which is

278
00:12:35,820 --> 00:12:41,220
related to the name pack itself and here

279
00:12:38,459 --> 00:12:43,920
it's a bit hacky we add 4 because it

280
00:12:41,220 --> 00:12:45,660
looked like from experience that's what

281
00:12:43,920 --> 00:12:47,420
we have to do

282
00:12:45,660 --> 00:12:49,800
um probably due to some kind of

283
00:12:47,420 --> 00:12:52,100
alignment that we have to take into

284
00:12:49,800 --> 00:12:52,100
account

285
00:12:54,240 --> 00:12:58,620
so yeah so the init_fake_

286
00:12:56,459 --> 00:13:00,899
enlistments return this chain of fake

287
00:12:58,620 --> 00:13:03,180
userland fake enlistments from the spray

288
00:13:00,899 --> 00:13:05,100
leak and trap so now let's go back to

289
00:13:03,180 --> 00:13:08,040
the main function

290
00:13:05,100 --> 00:13:09,660
so back to the main function we see

291
00:13:08,040 --> 00:13:10,980
we've initialized the fake enlistment

292
00:13:09,660 --> 00:13:13,440
now we're going to initialize the

293
00:13:10,980 --> 00:13:15,959
threads so the recovery thread as well

294
00:13:13,440 --> 00:13:17,760
as the KTM objects that we need to

295
00:13:15,959 --> 00:13:20,220
actually trigger the bike then we see

296
00:13:17,760 --> 00:13:21,899
that we actually call two functions the

297
00:13:20,220 --> 00:13:24,720
race recovering resource manager

298
00:13:21,899 --> 00:13:27,120
escaping loop and then if it returns

299
00:13:24,720 --> 00:13:29,519
true we can actually set the rate one

300
00:13:27,120 --> 00:13:32,220
variable to true

301
00:13:29,519 --> 00:13:34,440
and then we're going to test this race

302
00:13:32,220 --> 00:13:36,360
one variable and if it's true we're

303
00:13:34,440 --> 00:13:38,880
going to try to escape the loop because

304
00:13:36,360 --> 00:13:40,800
we won the race otherwise it means

305
00:13:38,880 --> 00:13:42,600
something went wrong and finally we have

306
00:13:40,800 --> 00:13:45,079
some cleanup so let's analyze these two

307
00:13:42,600 --> 00:13:45,079
functions

308
00:13:45,120 --> 00:13:50,279
so this function is very similar to the

309
00:13:47,519 --> 00:13:53,160
one we saw in previous slides where we

310
00:13:50,279 --> 00:13:55,860
actually tried to read notification so

311
00:13:53,160 --> 00:13:59,399
we can actually detect we want the race

312
00:13:55,860 --> 00:14:01,980
and so we have instructions we start the

313
00:13:59,399 --> 00:14:03,420
recovery thread by sending an event and

314
00:14:01,980 --> 00:14:05,639
then we're going to actually read all

315
00:14:03,420 --> 00:14:07,500
the notifications and make sure we

316
00:14:05,639 --> 00:14:12,139
actually read at least 32 so we actually

317
00:14:07,500 --> 00:14:12,139
bypass the delay frame mitigation

318
00:14:12,660 --> 00:14:19,320
then we see that we actually free the

319
00:14:16,139 --> 00:14:21,120
latest touch enlistments by uh calling

320
00:14:19,320 --> 00:14:23,639
commit complete and closing the handle

321
00:14:21,120 --> 00:14:25,740
and then frame other enlistment to make

322
00:14:23,639 --> 00:14:27,420
sure we trigger the delay trade

323
00:14:25,740 --> 00:14:30,600
mechanism

324
00:14:27,420 --> 00:14:33,800
then we replace the freed enlistment with

325
00:14:30,600 --> 00:14:33,800
the named pipe spray

326
00:14:37,440 --> 00:14:42,600
and so here we have instructions

327
00:14:40,440 --> 00:14:44,760
so we can see we can unblock the

328
00:14:42,600 --> 00:14:47,519
recovery thread by using the !unpatch

329
00:14:44,760 --> 00:14:49,740
command and we can either drag a

330
00:14:47,519 --> 00:14:51,959
few instructions after it had been

331
00:14:49,740 --> 00:14:53,699
blocked just so we are able to debug

332
00:14:51,959 --> 00:14:57,060
manually what is happening or we can

333
00:14:53,699 --> 00:14:59,779
actually break on the superior case so

334
00:14:57,060 --> 00:15:02,160
we just skip to handling the next

335
00:14:59,779 --> 00:15:04,079
enlistment and I get the advantage of

336
00:15:02,160 --> 00:15:07,320
the thread method is that we may be able

337
00:15:04,079 --> 00:15:09,480
to see how our league enlistment is

338
00:15:07,320 --> 00:15:11,339
passed because if we just go with a

339
00:15:09,480 --> 00:15:12,600
breakpoint on the super case at the

340
00:15:11,339 --> 00:15:14,279
beginning of the loop certain things

341
00:15:12,600 --> 00:15:16,260
certain functions are called and so we

342
00:15:14,279 --> 00:15:17,639
may miss what is happening with the k

343
00:15:16,260 --> 00:15:19,260
enlistment which is the leak and

344
00:15:17,639 --> 00:15:21,480
enlistment the first one that that is

345
00:15:19,260 --> 00:15:25,440
actually touched by the kernel after it

346
00:15:21,480 --> 00:15:27,360
has retrieved the flink pointer from the

347
00:15:25,440 --> 00:15:29,279
spray enlistment so we actually print

348
00:15:27,360 --> 00:15:30,240
the leak enlistment userland  and

349
00:15:29,279 --> 00:15:34,740
address

350
00:15:30,240 --> 00:15:36,240
on on the actual standard in outputs so

351
00:15:34,740 --> 00:15:37,800
if you're watching the debugger you can

352
00:15:36,240 --> 00:15:40,980
also use that address

353
00:15:37,800 --> 00:15:43,980
as a way to find the leak enlistment so

354
00:15:40,980 --> 00:15:47,100
you can analyze its fields

355
00:15:43,980 --> 00:15:49,800
and then we detect that we want the Rays

356
00:15:47,100 --> 00:15:52,380
from userland by checking its notifiable

357
00:15:49,800 --> 00:15:55,560
flag and making sure the flag wasn't set

358
00:15:52,380 --> 00:15:57,360
and if we detect that we won the race

359
00:15:55,560 --> 00:16:00,620
we're going to return true otherwise

360
00:15:57,360 --> 00:16:00,620
we're going to return false

361
00:16:01,320 --> 00:16:06,000
so if we return true we

362
00:16:04,079 --> 00:16:09,199
say that we won the race and then we're

363
00:16:06,000 --> 00:16:09,199
going to call escape loop

364
00:16:09,899 --> 00:16:13,639
so let's look at escape loop now

365
00:16:14,220 --> 00:16:19,019
so this function is the one that you're

366
00:16:16,500 --> 00:16:21,240
going to have to modify the idea is that

367
00:16:19,019 --> 00:16:24,320
you're going to be able to leak certain

368
00:16:21,240 --> 00:16:27,060
kernel pointers and so you'll be able to

369
00:16:24,320 --> 00:16:31,639
modify certain fields from your fake

370
00:16:27,060 --> 00:16:31,639
enlistments in order to escape the loop

371
00:16:32,220 --> 00:16:37,980
so we see in order to actually escape

372
00:16:35,100 --> 00:16:40,800
the loop we we need to actually set the

373
00:16:37,980 --> 00:16:43,860
next samerm flink pointer of one of the

374
00:16:40,800 --> 00:16:46,500
enlistment we inject a userland

375
00:16:43,860 --> 00:16:48,540
we need to set this next samerm flink

376
00:16:46,500 --> 00:16:51,060
pointer to the enlistment head so here we

377
00:16:48,540 --> 00:16:53,040
need to first get the enlistment head but

378
00:16:51,060 --> 00:16:55,560
not to get the enlistment head we we will

379
00:16:53,040 --> 00:16:57,480
actually first deduce the K resource

380
00:16:55,560 --> 00:16:59,100
manager address and then we know the

381
00:16:57,480 --> 00:17:02,579
enlistment head is just an offset from

382
00:16:59,100 --> 00:17:05,400
from this care resource manager so the

383
00:17:02,579 --> 00:17:07,380
leak enlistment we know it's addressed

384
00:17:05,400 --> 00:17:10,079
because it's a usernal address so here

385
00:17:07,380 --> 00:17:12,900
the the idea is going to be to from the

386
00:17:10,079 --> 00:17:15,059
leak enlistment to retrieve leaked

387
00:17:12,900 --> 00:17:17,959
kernel pointers and deduce the K

388
00:17:15,059 --> 00:17:17,959
resource measure address

389
00:17:18,959 --> 00:17:23,520
and then we can print it in userland

390
00:17:21,059 --> 00:17:26,280
then confirm that it it matches the the

391
00:17:23,520 --> 00:17:28,559
one we have in kernel

392
00:17:26,280 --> 00:17:31,320
and then once we have that we can

393
00:17:28,559 --> 00:17:34,740
actually use the init_userland_kenlistment

394
00:17:31,320 --> 00:17:36,960
to create a new enlistment that is our

395
00:17:34,740 --> 00:17:39,000
escape enlistment and once we have

396
00:17:36,960 --> 00:17:40,740
initialized it we can just use this

397
00:17:39,000 --> 00:17:43,440
macro inject enlistment to actually

398
00:17:40,740 --> 00:17:46,440
modify our prop enlistment and inject

399
00:17:43,440 --> 00:17:48,360
into the next samerm flame pointer the

400
00:17:46,440 --> 00:17:51,720
actual new enlistments and so if we

401
00:17:48,360 --> 00:17:53,780
look at the inject enlistment macro we

402
00:17:51,720 --> 00:17:57,120
can see that all it does is it actually

403
00:17:53,780 --> 00:18:00,480
set the flink pointer of the first

404
00:17:57,120 --> 00:18:03,380
argument to be pointing to the second

405
00:18:00,480 --> 00:18:03,380
argument

406
00:18:04,020 --> 00:18:07,620
and so here basically it's going to set

407
00:18:05,940 --> 00:18:10,380
the next samerm and frame pointer of the

408
00:18:07,620 --> 00:18:12,840
trap enlistment to point
the escape enlistment

409
00:18:10,380 --> 00:18:15,320
so if you remember init_userland_enlistment

410
00:18:16,440 --> 00:18:21,419
if the function

411
00:18:18,120 --> 00:18:23,160
that is allowing us to create a generic

412
00:18:21,419 --> 00:18:25,980
enlistment and so it's going to set

413
00:18:23,160 --> 00:18:29,880
the generic Enlistment

414
00:18:25,980 --> 00:18:30,720
that actually if you give it no it will

415
00:18:29,880 --> 00:18:33,600
point

416
00:18:30,720 --> 00:18:36,000
to itself but if you see in our case we

417
00:18:33,600 --> 00:18:39,120
don't want it to point to itself we want

418
00:18:36,000 --> 00:18:41,520
it to point to the actual enlistment

419
00:18:39,120 --> 00:18:43,980
head and so at this point once we have

420
00:18:41,520 --> 00:18:46,080
done that the kernel was looping on this

421
00:18:43,980 --> 00:18:48,480
trap enlistment and so it will quickly

422
00:18:46,080 --> 00:18:51,179
handle our escape enlistment and it

423
00:18:48,480 --> 00:18:53,820
should just exit the loop okay so now I

424
00:18:51,179 --> 00:18:56,700
think you are ready to be back and start

425
00:18:53,820 --> 00:18:58,940
licking kernel powders now it's your

426
00:18:56,700 --> 00:18:58,940
turn

