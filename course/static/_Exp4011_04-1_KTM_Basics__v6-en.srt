1
00:00:03,120 --> 00:00:08,460
hi everyone so it's gonna be time to

2
00:00:06,359 --> 00:00:09,660
talk about the connect transaction

3
00:00:08,460 --> 00:00:12,240
manager

4
00:00:09,660 --> 00:00:15,179
so quickly we're gonna actually use the

5
00:00:12,240 --> 00:00:17,400
abbreviation KTM instead of kernel

6
00:00:15,179 --> 00:00:20,039
transaction manager so yeah we're gonna

7
00:00:17,400 --> 00:00:21,840
start by looking at different things in

8
00:00:20,039 --> 00:00:24,600
the next few videos the first thing

9
00:00:21,840 --> 00:00:27,300
we're gonna look at is just from a high

10
00:00:24,600 --> 00:00:30,119
level perspective detailing what is KTM

11
00:00:27,300 --> 00:00:32,399
what are the different objects then in

12
00:00:30,119 --> 00:00:35,100
the next few videos we'll look into the

13
00:00:32,399 --> 00:00:36,780
actual KTM objects in the kernel what

14
00:00:35,100 --> 00:00:39,180
are the different fields how they

15
00:00:36,780 --> 00:00:41,399
interact with each others and a future

16
00:00:39,180 --> 00:00:44,399
video will be about all the KTM states

17
00:00:41,399 --> 00:00:46,860
which are basically the states

18
00:00:44,399 --> 00:00:49,020
corresponding to different KTM objects

19
00:00:46,860 --> 00:00:50,340
how they evolve over time how they

20
00:00:49,020 --> 00:00:53,399
behave okay

21
00:00:50,340 --> 00:00:55,320
let's get started so here we will

22
00:00:53,399 --> 00:00:57,600
basically assume you don't know anything

23
00:00:55,320 --> 00:00:59,280
about the kernel transaction manager and

24
00:00:57,600 --> 00:01:02,640
that you started reversing during the

25
00:00:59,280 --> 00:01:04,440
patch by using binary different so the

26
00:01:02,640 --> 00:01:06,960
idea is you want to do the best you can

27
00:01:04,440 --> 00:01:09,000
with the little knowledge you have but

28
00:01:06,960 --> 00:01:12,119
when you look at a big function like

29
00:01:09,000 --> 00:01:13,979
this TM recover resource manager axed

30
00:01:12,119 --> 00:01:16,260
you don't really know what a resource

31
00:01:13,979 --> 00:01:19,200
manager is you don't know what

32
00:01:16,260 --> 00:01:21,240
recovering a resource manager means you

33
00:01:19,200 --> 00:01:23,400
don't really know what any of the kernel

34
00:01:21,240 --> 00:01:26,340
structures related to the transaction

35
00:01:23,400 --> 00:01:29,400
manager are so the approach you want to

36
00:01:26,340 --> 00:01:32,759
take is often the same as when reversing

37
00:01:29,400 --> 00:01:35,579
stuff which is that you may as well know

38
00:01:32,759 --> 00:01:37,740
as much as possible about the actual

39
00:01:35,579 --> 00:01:39,420
component and so I said in a previous

40
00:01:37,740 --> 00:01:41,939
video that generally you don't want to

41
00:01:39,420 --> 00:01:44,820
learn all the Windows internals because

42
00:01:41,939 --> 00:01:46,680
it's time consuming but here is an

43
00:01:44,820 --> 00:01:48,780
exception and it's actually a counter

44
00:01:46,680 --> 00:01:50,700
example because we don't know anything

45
00:01:48,780 --> 00:01:53,100
about the current transaction manager at

46
00:01:50,700 --> 00:01:55,500
that time so basically you want to just

47
00:01:53,100 --> 00:01:58,680
read what a bunch of the structures look

48
00:01:55,500 --> 00:02:02,280
like and what the standard APIs to took

49
00:01:58,680 --> 00:02:04,560
to the KTM driver are and the idea is to

50
00:02:02,280 --> 00:02:07,079
get an intuition about what we need to

51
00:02:04,560 --> 00:02:09,479
know to make reversing a bit easier

52
00:02:07,079 --> 00:02:12,000
because if we were blindly documenting

53
00:02:09,479 --> 00:02:14,220
the vulnerable function by improving the

54
00:02:12,000 --> 00:02:17,160
decompiler output and we're able to

55
00:02:14,220 --> 00:02:18,720
understand what the vulnerability is it

56
00:02:17,160 --> 00:02:21,120
is still not going to make a lot of

57
00:02:18,720 --> 00:02:22,920
sense how to exploit it because we don't

58
00:02:21,120 --> 00:02:24,959
know anything about the actual contacts

59
00:02:22,920 --> 00:02:27,360
around that function so there is this

60
00:02:24,959 --> 00:02:30,120
type of exploration into the component

61
00:02:27,360 --> 00:02:32,700
we are targeting in this case the KTM

62
00:02:30,120 --> 00:02:35,280
driver just provides a lot of value from

63
00:02:32,700 --> 00:02:38,700
multiple multiple angles so it's

64
00:02:35,280 --> 00:02:40,500
interesting insofar as like KTM itself

65
00:02:38,700 --> 00:02:42,900
isn't actually that relevant to most

66
00:02:40,500 --> 00:02:44,760
kind of exploitation research but it is

67
00:02:42,900 --> 00:02:47,760
really useful in so far as there isn't

68
00:02:44,760 --> 00:02:49,500
much documentation on it online so it is

69
00:02:47,760 --> 00:02:51,420
really a good opportunity to show the

70
00:02:49,500 --> 00:02:54,420
approach to let's look into something

71
00:02:51,420 --> 00:02:56,760
that nobody else has looked at and how

72
00:02:54,420 --> 00:02:59,940
you end up integrating other bits of

73
00:02:56,760 --> 00:03:03,900
research into it as you need

74
00:02:59,940 --> 00:03:07,620
so if you look up online about KTM

75
00:03:03,900 --> 00:03:10,620
you'll find a bunch of resources so you

76
00:03:07,620 --> 00:03:13,080
can find there is a portal on msdn with

77
00:03:10,620 --> 00:03:14,819
a fair bit of information but if you are

78
00:03:13,080 --> 00:03:16,440
not used to the actual kernel

79
00:03:14,819 --> 00:03:19,379
transaction manager component in general

80
00:03:16,440 --> 00:03:21,959
a lot of it is kind of confusing there

81
00:03:19,379 --> 00:03:24,780
is a lot of new terminology a lot of the

82
00:03:21,959 --> 00:03:27,120
stuff is kind of misleading or hard to

83
00:03:24,780 --> 00:03:29,580
read but it's still nice that there is

84
00:03:27,120 --> 00:03:31,920
any documentation at all and so you can

85
00:03:29,580 --> 00:03:34,739
find some very useful videos from the

86
00:03:31,920 --> 00:03:37,140
Windows Vista era which is when they

87
00:03:34,739 --> 00:03:39,360
introduce KTM into the Windows kernel

88
00:03:37,140 --> 00:03:41,879
and I think they were really excited

89
00:03:39,360 --> 00:03:45,060
that everyone would end up using it so

90
00:03:41,879 --> 00:03:47,280
they wanted to document it and at the

91
00:03:45,060 --> 00:03:49,799
end nobody used it

92
00:03:47,280 --> 00:03:52,200
and so normally this is just a phase

93
00:03:49,799 --> 00:03:54,540
which is kind of boring depending on how

94
00:03:52,200 --> 00:03:57,060
interesting the component is but you

95
00:03:54,540 --> 00:04:00,180
just end up having to read a lot of

96
00:03:57,060 --> 00:04:03,000
stuff and so in our case we find that

97
00:04:00,180 --> 00:04:06,239
despite there is this msdn portal there

98
00:04:03,000 --> 00:04:09,360
is no sample code on on the msdn and

99
00:04:06,239 --> 00:04:12,540
also there is we can't find any sample

100
00:04:09,360 --> 00:04:15,239
code online either of other people using

101
00:04:12,540 --> 00:04:17,180
the KTM component because it seems

102
00:04:15,239 --> 00:04:19,919
basically nobody uses this feature

103
00:04:17,180 --> 00:04:22,979
reactors doesn't have any code related

104
00:04:19,919 --> 00:04:25,560
to it because it was based on Windows NT

105
00:04:22,979 --> 00:04:28,259
there were some leaked Windows source

106
00:04:25,560 --> 00:04:30,720
code but it was also too old to have KTM

107
00:04:28,259 --> 00:04:34,860
related code so this is the case where

108
00:04:30,720 --> 00:04:37,380
basically you have to do reversing and

109
00:04:34,860 --> 00:04:40,259
we have to implement all our own little

110
00:04:37,380 --> 00:04:43,139
code samples from scratch to experiment

111
00:04:40,259 --> 00:04:46,139
with the APIs and the idea is that when

112
00:04:43,139 --> 00:04:49,380
we exploit the bug later we are probably

113
00:04:46,139 --> 00:04:51,540
going to want to know how the APIs work

114
00:04:49,380 --> 00:04:54,300
anyway because we are going to have to

115
00:04:51,540 --> 00:04:57,540
potentially build up all these states

116
00:04:54,300 --> 00:04:59,880
into the KTM components so building up

117
00:04:57,540 --> 00:05:02,699
just a whole bunch of code samples that

118
00:04:59,880 --> 00:05:05,820
we can play with for fun and just see

119
00:05:02,699 --> 00:05:08,040
how things work is just a general good

120
00:05:05,820 --> 00:05:10,740
way for us to improve our knowledge on

121
00:05:08,040 --> 00:05:13,440
the components but still it will allow

122
00:05:10,740 --> 00:05:15,620
us to have it handy later during

123
00:05:13,440 --> 00:05:18,840
exploitation and so this is very

124
00:05:15,620 --> 00:05:21,660
iterative in that you need to read a

125
00:05:18,840 --> 00:05:24,060
bunch of stuff you get a rough idea of

126
00:05:21,660 --> 00:05:27,360
basic functionality you implement some

127
00:05:24,060 --> 00:05:29,940
APIs you spend a bunch of time debugging

128
00:05:27,360 --> 00:05:32,039
because you screwed it up and it doesn't

129
00:05:29,940 --> 00:05:34,259
work maybe you reverse some of the

130
00:05:32,039 --> 00:05:37,860
things related to the APIs because the

131
00:05:34,259 --> 00:05:40,560
documentation is incomplete and then you

132
00:05:37,860 --> 00:05:43,560
get it working and you just rinse and

133
00:05:40,560 --> 00:05:46,320
repeat this process until you build up a

134
00:05:43,560 --> 00:05:48,300
good decent mental model of what things

135
00:05:46,320 --> 00:05:50,520
are so what we're detailing that

136
00:05:48,300 --> 00:05:53,160
following slides is a bit cheating

137
00:05:50,520 --> 00:05:57,060
because we are going to present KTM

138
00:05:53,160 --> 00:05:59,460
internals as our final understanding so

139
00:05:57,060 --> 00:06:01,979
it it is easier and faster for you to

140
00:05:59,460 --> 00:06:05,460
approach things and so if you have some

141
00:06:01,979 --> 00:06:08,220
time now to spend I would recommend you

142
00:06:05,460 --> 00:06:11,060
try reading and watching all these

143
00:06:08,220 --> 00:06:13,680
resources beforehand so you actually

144
00:06:11,060 --> 00:06:15,660
realize what it is to be in that

145
00:06:13,680 --> 00:06:18,300
situation where you don't know anything

146
00:06:15,660 --> 00:06:20,400
and you have to learn everything from

147
00:06:18,300 --> 00:06:22,319
the available resources and then

148
00:06:20,400 --> 00:06:23,819
obviously you can continue watching this

149
00:06:22,319 --> 00:06:27,479
this slide deck

150
00:06:23,819 --> 00:06:29,880
so what is KTM so basically KTM is just

151
00:06:27,479 --> 00:06:32,280
some kernel service that was added in

152
00:06:29,880 --> 00:06:34,919
Windows Vista and it was originally part

153
00:06:32,280 --> 00:06:38,340
of ntos kernel but then later it was

154
00:06:34,919 --> 00:06:40,919
moved to its own driver called tm.cis

155
00:06:38,340 --> 00:06:42,660
and this is mostly relevant for when you

156
00:06:40,919 --> 00:06:46,080
are reversing things or diffing

157
00:06:42,660 --> 00:06:48,539
sometimes it is actually good to reverse

158
00:06:46,080 --> 00:06:50,160
engineer several versions for instance

159
00:06:48,539 --> 00:06:53,220
you can leave it on Windows Vista

160
00:06:50,160 --> 00:06:56,340
Windows 7 or Windows 10 so knowing where

161
00:06:53,220 --> 00:06:58,380
the code is is actually useful since you

162
00:06:56,340 --> 00:07:01,020
might need to switch between different

163
00:06:58,380 --> 00:07:02,759
Windows versions in some case doing

164
00:07:01,020 --> 00:07:04,979
things like diffing it is actually

165
00:07:02,759 --> 00:07:07,919
easier to use something like diaphrag or

166
00:07:04,979 --> 00:07:11,160
bin leaf on a smaller file like tm.sys

167
00:07:07,919 --> 00:07:13,440
than n2skernel.exe because if they fix

168
00:07:11,160 --> 00:07:16,380
some other vulnerabilities or added new

169
00:07:13,440 --> 00:07:19,680
features say as part of the same monthly

170
00:07:16,380 --> 00:07:23,160
released update it could be that ntos

171
00:07:19,680 --> 00:07:26,460
kernel had like numerous other changes

172
00:07:23,160 --> 00:07:29,759
so that FR bindiff would match them all

173
00:07:26,460 --> 00:07:33,419
whereas if it is tm.cis and there is

174
00:07:29,759 --> 00:07:35,400
only one kta vulnerability fixed then

175
00:07:33,419 --> 00:07:37,680
you pretty much know that the changes

176
00:07:35,400 --> 00:07:39,240
will be interesting to look at and

177
00:07:37,680 --> 00:07:41,699
basically the idea of this transaction

178
00:07:39,240 --> 00:07:44,699
manager is to provide what they call

179
00:07:41,699 --> 00:07:47,840
ACID functionality so it is basically

180
00:07:44,699 --> 00:07:51,539
operations which they call Atomic

181
00:07:47,840 --> 00:07:53,819
consistent isolated and durable and so

182
00:07:51,539 --> 00:07:56,819
the idea is you have some work that

183
00:07:53,819 --> 00:07:59,400
needs to be done and that work will be

184
00:07:56,819 --> 00:08:02,099
fairly complicated and so every piece of

185
00:07:59,400 --> 00:08:04,800
that work will sort of all be done in a

186
00:08:02,099 --> 00:08:07,740
way that either it all actually happens

187
00:08:04,800 --> 00:08:11,340
and it is committed which means every

188
00:08:07,740 --> 00:08:13,860
part of it is sort of finished or if any

189
00:08:11,340 --> 00:08:16,500
part of the work fails the whole thing

190
00:08:13,860 --> 00:08:19,860
is reverted as it as if it never

191
00:08:16,500 --> 00:08:22,080
happened and so the APIs are quite

192
00:08:19,860 --> 00:08:24,960
confusing and this could be one of the

193
00:08:22,080 --> 00:08:28,259
reason why nobody ended up using it and

194
00:08:24,960 --> 00:08:31,259
so Microsoft themselves use it for the

195
00:08:28,259 --> 00:08:35,099
registry and NTFS so you could actually

196
00:08:31,259 --> 00:08:37,860
sort of reverse ntx drivers and see

197
00:08:35,099 --> 00:08:39,779
references to the KTM kernel functions

198
00:08:37,860 --> 00:08:42,599
called directly from other part of the

199
00:08:39,779 --> 00:08:44,760
kernel but from our perspective this is

200
00:08:42,599 --> 00:08:46,920
not the best approach because we want to

201
00:08:44,760 --> 00:08:49,920
be able to talk to the KTM service from

202
00:08:46,920 --> 00:08:52,140
user land by calling syscalls since our

203
00:08:49,920 --> 00:08:54,660
goal is to exploit a bug for local

204
00:08:52,140 --> 00:08:57,360
privilege escalation so it is it is a

205
00:08:54,660 --> 00:09:01,260
lot simpler to look at the userline

206
00:08:57,360 --> 00:09:03,060
exposed APIs related to KTM and analyze

207
00:09:01,260 --> 00:09:06,120
the code of the corresponding syscall

208
00:09:03,060 --> 00:09:09,120
implemented in kernel and really it just

209
00:09:06,120 --> 00:09:12,000
boils down to a few dozen APIs and

210
00:09:09,120 --> 00:09:13,560
system calls exposed to user land that

211
00:09:12,000 --> 00:09:16,920
you need to spend time playing around

212
00:09:13,560 --> 00:09:19,980
with and it ends up being a lot of trial

213
00:09:16,920 --> 00:09:22,980
and error through development in general

214
00:09:19,980 --> 00:09:25,980
but this is actually expected if you're

215
00:09:22,980 --> 00:09:29,459
actually doing research on an unknown

216
00:09:25,980 --> 00:09:30,420
component you will spend time doing this

217
00:09:29,459 --> 00:09:33,240
kind of thing

218
00:09:30,420 --> 00:09:36,180
the whole idea of a transaction is that

219
00:09:33,240 --> 00:09:39,899
it is an atomic operation that is made

220
00:09:36,180 --> 00:09:41,760
up of a ton of workers and and parts an

221
00:09:39,899 --> 00:09:45,360
example that is easy enough if you're

222
00:09:41,760 --> 00:09:47,700
familiar with is an SQL database where

223
00:09:45,360 --> 00:09:50,519
the concept of transaction is generally

224
00:09:47,700 --> 00:09:53,760
used the example I think that Microsoft

225
00:09:50,519 --> 00:09:56,760
uses is something like an an ATM an

226
00:09:53,760 --> 00:09:58,620
automated teller machine where you're

227
00:09:56,760 --> 00:10:01,140
transferring money from One bank account

228
00:09:58,620 --> 00:10:03,300
to another bank account and so for

229
00:10:01,140 --> 00:10:05,459
obvious reasons when you have money

230
00:10:03,300 --> 00:10:08,040
removed from one person's bank account

231
00:10:05,459 --> 00:10:10,440
you want to make sure the receiving bank

232
00:10:08,040 --> 00:10:13,260
account has actually received the money

233
00:10:10,440 --> 00:10:15,360
and vice versa if the receiving bank

234
00:10:13,260 --> 00:10:17,700
account has received the money you want

235
00:10:15,360 --> 00:10:20,339
to make sure the source bank account was

236
00:10:17,700 --> 00:10:23,519
debated accordingly so the whole process

237
00:10:20,339 --> 00:10:26,040
would be a transaction and if one part

238
00:10:23,519 --> 00:10:28,320
fails the whole bank transfer will be

239
00:10:26,040 --> 00:10:30,360
reverted another example from our

240
00:10:28,320 --> 00:10:32,519
perspective in the Windows environment

241
00:10:30,360 --> 00:10:34,200
is something like an installer the

242
00:10:32,519 --> 00:10:37,019
installation process of a new software

243
00:10:34,200 --> 00:10:39,560
is going to involve writing maybe a few

244
00:10:37,019 --> 00:10:42,959
hundred files making registry

245
00:10:39,560 --> 00:10:45,300
modification adding Services

246
00:10:42,959 --> 00:10:46,920
Etc and all of the stuff will be part of

247
00:10:45,300 --> 00:10:49,560
a single transaction and that

248
00:10:46,920 --> 00:10:52,560
transaction is the installation and so

249
00:10:49,560 --> 00:10:54,839
if any part of it fails the entire

250
00:10:52,560 --> 00:10:57,180
installation is just reverted and so I

251
00:10:54,839 --> 00:10:59,700
think that that is why KTM was developed

252
00:10:57,180 --> 00:11:01,920
originally because Microsoft thought it

253
00:10:59,700 --> 00:11:04,800
would be very useful for many other

254
00:11:01,920 --> 00:11:07,560
parts of Windows there are three sort of

255
00:11:04,800 --> 00:11:09,839
high level states that a transaction can

256
00:11:07,560 --> 00:11:12,000
go through the first one is the

257
00:11:09,839 --> 00:11:15,120
committing of the transaction itself

258
00:11:12,000 --> 00:11:18,300
which is sort of saying okay this is

259
00:11:15,120 --> 00:11:21,180
this is work we want to do I am now

260
00:11:18,300 --> 00:11:23,760
committing to it and in the process of

261
00:11:21,180 --> 00:11:27,240
committing to it you go through a series

262
00:11:23,760 --> 00:11:29,700
of underlying state changes with the end

263
00:11:27,240 --> 00:11:32,700
result being that the transaction is

264
00:11:29,700 --> 00:11:35,579
completed like an actual installation of

265
00:11:32,700 --> 00:11:38,279
a software happened if any of them fails

266
00:11:35,579 --> 00:11:40,920
the big operation that happens is called

267
00:11:38,279 --> 00:11:43,380
the rollback which basically reverts

268
00:11:40,920 --> 00:11:45,959
everything that happened and you go back

269
00:11:43,380 --> 00:11:48,899
to the very beginning States and finally

270
00:11:45,959 --> 00:11:50,820
the recovery is something where some

271
00:11:48,899 --> 00:11:53,880
error might have occurred during the

272
00:11:50,820 --> 00:11:56,220
operation like say you needed to write a

273
00:11:53,880 --> 00:11:59,579
bunch of files to disk and device system

274
00:11:56,220 --> 00:12:01,800
temporarily went down but then it came

275
00:11:59,579 --> 00:12:03,720
back up that's the type of scenario

276
00:12:01,800 --> 00:12:06,180
where you might be able to keep the

277
00:12:03,720 --> 00:12:09,120
transaction going and so this type of

278
00:12:06,180 --> 00:12:11,880
states are recoverable because you can

279
00:12:09,120 --> 00:12:14,459
basically recover transaction so that

280
00:12:11,880 --> 00:12:17,279
although there was a bump in the road

281
00:12:14,459 --> 00:12:19,760
you can keep installing and maybe what

282
00:12:17,279 --> 00:12:23,160
you committed to do can actually happen

283
00:12:19,760 --> 00:12:25,740
and be completed what is funny is that

284
00:12:23,160 --> 00:12:28,560
even though KTM is interesting and

285
00:12:25,740 --> 00:12:30,240
complicated not a lot of people have

286
00:12:28,560 --> 00:12:33,180
really looked at this service in general

287
00:12:30,240 --> 00:12:35,459
all most people that looked at it and

288
00:12:33,180 --> 00:12:37,800
published vulnerabilities publicly are

289
00:12:35,459 --> 00:12:39,720
actually people working at Project Zero

290
00:12:37,800 --> 00:12:43,019
at the time of this recording and in

291
00:12:39,720 --> 00:12:45,000
2018 there was this blog by Kaspersky

292
00:12:43,019 --> 00:12:47,459
where they documented a zero day

293
00:12:45,000 --> 00:12:50,040
exploited in the wild where they don't

294
00:12:47,459 --> 00:12:51,839
get much technical details and this is

295
00:12:50,040 --> 00:12:54,540
the bag we are going to explore in this

296
00:12:51,839 --> 00:12:58,260
training and then in 2019 some malware

297
00:12:54,540 --> 00:13:00,899
was using the KTM APIs as an obfuscation

298
00:12:58,260 --> 00:13:03,180
mechanism to basically do file system

299
00:13:00,899 --> 00:13:05,820
transactions without calling the file

300
00:13:03,180 --> 00:13:08,160
system APIs directly but instead doing

301
00:13:05,820 --> 00:13:11,540
it indirectly through transaction

302
00:13:08,160 --> 00:13:14,700
related APIs since we made this research

303
00:13:11,540 --> 00:13:17,399
and prepared that course there has been

304
00:13:14,700 --> 00:13:19,079
several presentations around KTM and

305
00:13:17,399 --> 00:13:21,240
more specifically the variety we're

306
00:13:19,079 --> 00:13:23,700
going to target in this course both by

307
00:13:21,240 --> 00:13:26,339
Kaspersky who discovered the exploit in

308
00:13:23,700 --> 00:13:29,100
the wild an NCC group who I am currently

309
00:13:26,339 --> 00:13:31,200
working for so originally kerspersky

310
00:13:29,100 --> 00:13:33,360
almost didn't give any technical detail

311
00:13:31,200 --> 00:13:35,459
or information as all they did was

312
00:13:33,360 --> 00:13:38,040
releasing a Blog with two paragraphs

313
00:13:35,459 --> 00:13:40,260
about high-level view of how the export

314
00:13:38,040 --> 00:13:42,540
was working which will read a bit later

315
00:13:40,260 --> 00:13:44,880
in the course and you'll see it's not

316
00:13:42,540 --> 00:13:47,880
super helpful to actually explore the

317
00:13:44,880 --> 00:13:51,180
gravity in parallel my great colleague

318
00:13:47,880 --> 00:13:53,399
Aaron and I worked on writing an export

319
00:13:51,180 --> 00:13:55,560
for the summer ability and interestingly

320
00:13:53,399 --> 00:13:57,839
we came up with different techniques to

321
00:13:55,560 --> 00:13:59,760
explore the back than what Kaspersky

322
00:13:57,839 --> 00:14:03,480
described seeing in the wild we actually

323
00:13:59,760 --> 00:14:07,019
found way later that a talk was given in

324
00:14:03,480 --> 00:14:10,139
2019 as blue hat but we hadn't found it

325
00:14:07,019 --> 00:14:13,560
due to hard to found links and Google

326
00:14:10,139 --> 00:14:16,980
not referencing it well enough and so at

327
00:14:13,560 --> 00:14:18,959
the end both Kaspersky and US ended up

328
00:14:16,980 --> 00:14:21,660
giving talks at security conferences

329
00:14:18,959 --> 00:14:24,180
with more details gershwinsky gave

330
00:14:21,660 --> 00:14:26,399
actually a bit more details which can be

331
00:14:24,180 --> 00:14:27,959
useful to read but they will be quite

332
00:14:26,399 --> 00:14:30,120
hard to understand until you actually

333
00:14:27,959 --> 00:14:32,220
work on the exploit yourself also the

334
00:14:30,120 --> 00:14:35,040
they never gave all the technical

335
00:14:32,220 --> 00:14:37,260
informations about the techniques being

336
00:14:35,040 --> 00:14:39,060
used by the in the white exploit so

337
00:14:37,260 --> 00:14:41,940
there are still some unknowns also

338
00:14:39,060 --> 00:14:43,980
Kaspersky never shared a hash for the

339
00:14:41,940 --> 00:14:46,019
exploit the codes so we can't actually

340
00:14:43,980 --> 00:14:48,120
reverse engineer that in the white

341
00:14:46,019 --> 00:14:50,940
exploit to confirm any of the

342
00:14:48,120 --> 00:14:53,639
information or to understand the gaps

343
00:14:50,940 --> 00:14:55,139
and to be honest it is hard to

344
00:14:53,639 --> 00:14:57,600
understand all the details they

345
00:14:55,139 --> 00:14:59,760
published even for us who have been

346
00:14:57,600 --> 00:15:02,459
developing a working export for December

347
00:14:59,760 --> 00:15:04,260
ability NCC group did publish a lot of

348
00:15:02,459 --> 00:15:06,959
information on the exploitation

349
00:15:04,260 --> 00:15:08,339
techniques we used and again they are

350
00:15:06,959 --> 00:15:10,800
different from the indual exploit

351
00:15:08,339 --> 00:15:12,779
apparently that being said I would not

352
00:15:10,800 --> 00:15:15,240
recommend going over all that material

353
00:15:12,779 --> 00:15:17,220
if you haven't done it yet just for a

354
00:15:15,240 --> 00:15:19,139
couple of reasons the first reason is

355
00:15:17,220 --> 00:15:20,880
that the whole point of this course is

356
00:15:19,139 --> 00:15:23,519
to actually go over the process of

357
00:15:20,880 --> 00:15:26,100
exploiting availability as if there was

358
00:15:23,519 --> 00:15:29,220
no content available the second reason

359
00:15:26,100 --> 00:15:32,220
is that sure the contents would

360
00:15:29,220 --> 00:15:35,940
potentially help you but it might also

361
00:15:32,220 --> 00:15:38,459
be misleading since what it present is

362
00:15:35,940 --> 00:15:40,560
the actual results once you have

363
00:15:38,459 --> 00:15:43,139
finished your exploits not the actual

364
00:15:40,560 --> 00:15:45,060
methodology to reach that goal which is

365
00:15:43,139 --> 00:15:48,240
kind of what this course is about right

366
00:15:45,060 --> 00:15:50,519
anyway if you have read it already don't

367
00:15:48,240 --> 00:15:53,040
worry as there is a lot to learn in this

368
00:15:50,519 --> 00:15:56,100
course and going over the same concepts

369
00:15:53,040 --> 00:15:58,440
several times is always beneficial for

370
00:15:56,100 --> 00:16:00,899
your brain and your mental model from a

371
00:15:58,440 --> 00:16:03,959
reversing perspective there are sort of

372
00:16:00,899 --> 00:16:05,940
four main color structures that KTM is

373
00:16:03,959 --> 00:16:09,540
always dealing with they are all

374
00:16:05,940 --> 00:16:11,880
referenced counted objects that are

375
00:16:09,540 --> 00:16:13,560
tracked by the object manager and I'll

376
00:16:11,880 --> 00:16:15,720
go over each of them in more details

377
00:16:13,560 --> 00:16:18,180
later but basically from a high level

378
00:16:15,720 --> 00:16:21,000
view there is the transaction manager

379
00:16:18,180 --> 00:16:23,579
which just tracks all the transactions

380
00:16:21,000 --> 00:16:25,260
in general then typically the work that

381
00:16:23,579 --> 00:16:28,380
is going to be done as part of the

382
00:16:25,260 --> 00:16:31,320
transaction is associated with one or

383
00:16:28,380 --> 00:16:34,440
more different resources managers and

384
00:16:31,320 --> 00:16:37,740
the resource you can think of it maybe

385
00:16:34,440 --> 00:16:40,980
like the Windows registry or the NTFS

386
00:16:37,740 --> 00:16:45,360
file system or whatever and when we say

387
00:16:40,980 --> 00:16:47,940
that basically when we say that okay I

388
00:16:45,360 --> 00:16:51,120
am committing to that transaction if the

389
00:16:47,940 --> 00:16:53,820
transaction is about writing 10 files to

390
00:16:51,120 --> 00:16:57,420
the file system and writing 10 registry

391
00:16:53,820 --> 00:16:59,160
keys the work related to the 10 files

392
00:16:57,420 --> 00:17:01,560
will be associated with the file system

393
00:16:59,160 --> 00:17:04,199
resource and so there will be a resource

394
00:17:01,560 --> 00:17:06,600
manager that tracks that work and the

395
00:17:04,199 --> 00:17:08,699
work related to the 10 registry keys

396
00:17:06,600 --> 00:17:11,880
will be associated with another resource

397
00:17:08,699 --> 00:17:14,579
manager dealing with registry keys then

398
00:17:11,880 --> 00:17:17,520
you have the transaction object and

399
00:17:14,579 --> 00:17:20,040
finally you have the enlistment so the

400
00:17:17,520 --> 00:17:23,040
only spell is basically a resource

401
00:17:20,040 --> 00:17:25,439
manager representing sort of a single

402
00:17:23,040 --> 00:17:28,740
piece of work associated with the

403
00:17:25,439 --> 00:17:32,160
transaction and this sort of analyst in

404
00:17:28,740 --> 00:17:34,559
the transaction and say I am going to do

405
00:17:32,160 --> 00:17:36,660
this work so I am committing to

406
00:17:34,559 --> 00:17:39,320
collaborating with the other enlistments

407
00:17:36,660 --> 00:17:42,299
doing work for that same transaction

408
00:17:39,320 --> 00:17:44,700
basically as can be seen in the Windows

409
00:17:42,299 --> 00:17:47,520
scanner in general this figure just

410
00:17:44,700 --> 00:17:49,380
shows the general relationships between

411
00:17:47,520 --> 00:17:51,240
the different KTM structures in the

412
00:17:49,380 --> 00:17:54,299
kernel so there is a slightly

413
00:17:51,240 --> 00:17:56,940
complicated object graph that is created

414
00:17:54,299 --> 00:18:01,140
inside the kernel with some interesting

415
00:17:56,940 --> 00:18:04,140
relationships so we see this KTM like

416
00:18:01,140 --> 00:18:07,260
underscore KTM this could at first be

417
00:18:04,140 --> 00:18:11,039
confusing since it has the same name as

418
00:18:07,260 --> 00:18:13,440
the KTM component but this KTM structure

419
00:18:11,039 --> 00:18:16,320
is the actual transaction manager object

420
00:18:13,440 --> 00:18:19,200
we just talked about it is not tracking

421
00:18:16,320 --> 00:18:21,780
the entire KTM component stuff there is

422
00:18:19,200 --> 00:18:24,059
one or more resource managers associated

423
00:18:21,780 --> 00:18:26,340
with the transaction manager and they

424
00:18:24,059 --> 00:18:28,980
are tracked by the K resource manager

425
00:18:26,340 --> 00:18:32,400
structure and a resource manager object

426
00:18:28,980 --> 00:18:35,220
has a linked list of enlistments and

427
00:18:32,400 --> 00:18:38,220
those those enlistments may be involved

428
00:18:35,220 --> 00:18:40,200
in one transaction or more than one

429
00:18:38,220 --> 00:18:42,900
transaction because a resource manager

430
00:18:40,200 --> 00:18:45,179
tracks all the announcements associated

431
00:18:42,900 --> 00:18:48,000
with the same resource and there might

432
00:18:45,179 --> 00:18:50,340
be multiple transactions happening at

433
00:18:48,000 --> 00:18:53,700
the same time so this is kind of a high

434
00:18:50,340 --> 00:18:57,419
level view of how you can think about

435
00:18:53,700 --> 00:18:59,520
what KTM looks like at a given time in

436
00:18:57,419 --> 00:19:01,500
the kernel but obviously we'll go into

437
00:18:59,520 --> 00:19:05,480
more detail about what the KTM

438
00:19:01,500 --> 00:19:05,480
structures are in the next videos

