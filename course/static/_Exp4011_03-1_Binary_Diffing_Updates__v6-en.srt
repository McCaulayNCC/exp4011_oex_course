1
00:00:01,920 --> 00:00:06,299
the general approach when you want to

2
00:00:04,319 --> 00:00:07,620
explore the vulnerability in a patched

3
00:00:06,299 --> 00:00:10,620
software

4
00:00:07,620 --> 00:00:12,059
is to do what we call binary diffing or

5
00:00:10,620 --> 00:00:15,000
patch diffing

6
00:00:12,059 --> 00:00:16,980
the idea is you compare the version of

7
00:00:15,000 --> 00:00:18,359
the binary before the patch and after

8
00:00:16,980 --> 00:00:21,660
the patch

9
00:00:18,359 --> 00:00:24,900
we are going to go over the MSU format

10
00:00:21,660 --> 00:00:27,119
which is the format used by Microsoft to

11
00:00:24,900 --> 00:00:28,560
embed the patches

12
00:00:27,119 --> 00:00:32,640
then we will talk about the different

13
00:00:28,560 --> 00:00:35,520
methods used for doing patch diffing and

14
00:00:32,640 --> 00:00:37,260
see the drawbacks and advantages OK

15
00:00:35,520 --> 00:00:40,620
let's get started

16
00:00:37,260 --> 00:00:43,379
Microsoft updates are in the MSU format

17
00:00:40,620 --> 00:00:46,020
which stands for Microsoft update

18
00:00:43,379 --> 00:00:49,800
Standalone package and so we often refer

19
00:00:46,020 --> 00:00:51,239
to the updates as patch Tuesday because

20
00:00:49,800 --> 00:00:54,239
they release the new version of

21
00:00:51,239 --> 00:00:56,760
Microsoft products every second Tuesday

22
00:00:54,239 --> 00:00:59,940
of the month Microsoft also uses the

23
00:00:56,760 --> 00:01:02,399
term KB which stands for knowledge base

24
00:00:59,940 --> 00:01:05,479
followed by a number that basically

25
00:01:02,399 --> 00:01:08,220
uniquely identifies a particular update

26
00:01:05,479 --> 00:01:10,860
Microsoft does release exceptional

27
00:01:08,220 --> 00:01:13,680
updates for instance if a zero day was

28
00:01:10,860 --> 00:01:15,600
exploited in the wild usually it is only

29
00:01:13,680 --> 00:01:18,720
for critical reliabilities that can be

30
00:01:15,600 --> 00:01:21,240
exploited remotely and often they did it

31
00:01:18,720 --> 00:01:23,640
for vulnerabilities that are wormable so

32
00:01:21,240 --> 00:01:26,280
that it's that means they can propagate

33
00:01:23,640 --> 00:01:29,340
again and again after exploiting its

34
00:01:26,280 --> 00:01:31,500
patient zero target similarly to a worm

35
00:01:29,340 --> 00:01:33,600
Microsoft has updated their portal

36
00:01:31,500 --> 00:01:35,820
over time which makes it hard if you are

37
00:01:33,600 --> 00:01:38,400
maintaining a tool to parse their portal

38
00:01:35,820 --> 00:01:40,320
but the latest version of their portal

39
00:01:38,400 --> 00:01:42,479
is quite nice and you can easily search

40
00:01:40,320 --> 00:01:45,000
for things like featuring per product

41
00:01:42,479 --> 00:01:47,939
for instance Microsoft Exchange or

42
00:01:45,000 --> 00:01:51,180
Windows 10 or Office Products you

43
00:01:47,939 --> 00:01:54,420
can also filter period or date and they

44
00:01:51,180 --> 00:01:57,299
add to the end of the pages what changes

45
00:01:54,420 --> 00:02:00,119
in case they update an advisory page

46
00:01:57,299 --> 00:02:02,460
with additional information patch

47
00:02:00,119 --> 00:02:04,259
diffing consists in comparing a version of

48
00:02:02,460 --> 00:02:06,659
a file before the patch and after a

49
00:02:04,259 --> 00:02:09,300
patch obviously one requirement is that

50
00:02:06,659 --> 00:02:12,239
it requires having both versions of the

51
00:02:09,300 --> 00:02:14,940
file before the patch and after the

52
00:02:12,239 --> 00:02:16,800
patch and due to the nature of Microsoft

53
00:02:14,940 --> 00:02:20,099
updates you'll see it's not always

54
00:02:16,800 --> 00:02:22,980
obvious how to get these two versions so

55
00:02:20,099 --> 00:02:25,680
the few reasons why that is the case the

56
00:02:22,980 --> 00:02:28,739
first one is that in the past an update

57
00:02:25,680 --> 00:02:31,020
would only contain a file if it has

58
00:02:28,739 --> 00:02:33,420
changed so you would easily find the

59
00:02:31,020 --> 00:02:36,360
patched version in the latest patch

60
00:02:33,420 --> 00:02:38,640
Tuesday however in order to find the

61
00:02:36,360 --> 00:02:41,160
version before the patch you would need

62
00:02:38,640 --> 00:02:43,739
to locate what was the last update that

63
00:02:41,160 --> 00:02:46,080
updated that file and it would be quite

64
00:02:43,739 --> 00:02:50,280
time consuming sometimes second reason

65
00:02:46,080 --> 00:02:54,239
is that nowadays Microsoft mostly uses

66
00:02:50,280 --> 00:02:56,700
cumulative updates which basically embed

67
00:02:54,239 --> 00:02:59,940
all the latest versions of the files

68
00:02:56,700 --> 00:03:02,220
since the release of the iso image so

69
00:02:59,940 --> 00:03:04,860
you will always have all the files that

70
00:03:02,220 --> 00:03:06,959
have been changed since that ISO image

71
00:03:04,860 --> 00:03:09,239
even if it was changed a couple of

72
00:03:06,959 --> 00:03:12,120
months ago you would just be duplicated

73
00:03:09,239 --> 00:03:14,519
in each new community deep update without

74
00:03:12,120 --> 00:03:16,800
changing it except if there is a new

75
00:03:14,519 --> 00:03:18,480
patch version then that new patch

76
00:03:16,800 --> 00:03:20,879
version will be duplicated each month

77
00:03:18,480 --> 00:03:22,560
until the next one and so on there is

78
00:03:20,879 --> 00:03:24,840
one last thing that started to be used

79
00:03:22,560 --> 00:03:27,000
in recent Windows 10 versions which is

80
00:03:24,840 --> 00:03:30,720
that instead of actually having all the

81
00:03:27,000 --> 00:03:32,640
executable files like dlls exe in the

82
00:03:30,720 --> 00:03:35,459
actual PE format

83
00:03:32,640 --> 00:03:39,000
in the patch Tuesday updates what

84
00:03:35,459 --> 00:03:41,940
Microsoft does is it uses a special

85
00:03:39,000 --> 00:03:43,739
compressed format which just includes

86
00:03:41,940 --> 00:03:46,440
the differences from the first version

87
00:03:43,739 --> 00:03:48,659
in the iso so it reduces the size of the

88
00:03:46,440 --> 00:03:50,280
patch Tuesday updates overall but it

89
00:03:48,659 --> 00:03:52,319
makes it a bit more complicated to

90
00:03:50,280 --> 00:03:54,659
reconstruct the versions of the files

91
00:03:52,319 --> 00:03:56,879
pre-patch and post patch because we need

92
00:03:54,659 --> 00:03:59,700
them to do binary diffing you can

93
00:03:56,879 --> 00:04:02,580
extract the MSU file by using the expand

94
00:03:59,700 --> 00:04:05,519
tool and you will end up getting a few

95
00:04:02,580 --> 00:04:08,700
files the main one we are interested in

96
00:04:05,519 --> 00:04:11,159
is a cab file which is quite big almost

97
00:04:08,700 --> 00:04:13,680
the size of the original MSU and then

98
00:04:11,159 --> 00:04:16,199
you can extract the cab file again with

99
00:04:13,680 --> 00:04:18,720
the expand tool again and you end up

100
00:04:16,199 --> 00:04:21,560
with a list of all the binaries that are

101
00:04:18,720 --> 00:04:25,199
part of that update such as the

102
00:04:21,560 --> 00:04:27,240
dll.exe or .sys files the expand tool

103
00:04:25,199 --> 00:04:30,000
is a tool that is on Windows by default

104
00:04:27,240 --> 00:04:32,580
there are two types of MSU in the first

105
00:04:30,000 --> 00:04:34,880
one the old format you will basically on

106
00:04:32,580 --> 00:04:38,220
about all the

107
00:04:34,880 --> 00:04:41,220
.sys .dll .exe files from the actual PE

108
00:04:38,220 --> 00:04:44,479
format and there is the new format since

109
00:04:41,220 --> 00:04:48,180
Windows 10 1809 which is basically

110
00:04:44,479 --> 00:04:50,699
embedding specifically compressed format

111
00:04:48,180 --> 00:04:54,060
to reduce the size of the actual update

112
00:04:50,699 --> 00:04:56,400
the old MSU format only contains a file

113
00:04:54,060 --> 00:04:58,560
if there is a patch in this monthly

114
00:04:56,400 --> 00:05:00,360
update for that particular file so

115
00:04:58,560 --> 00:05:03,120
basically you need to find what month

116
00:05:00,360 --> 00:05:05,400
actually patch that same file to locate

117
00:05:03,120 --> 00:05:07,259
a previous version of that same file if

118
00:05:05,400 --> 00:05:09,840
you want to do binary diffing and so

119
00:05:07,259 --> 00:05:12,720
Microsoft does indicate the supersedence

120
00:05:09,840 --> 00:05:15,720
of a given KB so it would typically say

121
00:05:12,720 --> 00:05:18,300
that a given KB supersedence another KB so

122
00:05:15,720 --> 00:05:20,340
you could look at the previous KB and

123
00:05:18,300 --> 00:05:22,919
extract it to try to find a previous

124
00:05:20,340 --> 00:05:25,259
version of the file before the patch but

125
00:05:22,919 --> 00:05:27,419
sometimes you might not find it so then

126
00:05:25,259 --> 00:05:31,020
you would have to do that supersedence

127
00:05:27,419 --> 00:05:34,560
game for a few hops before you find the

128
00:05:31,020 --> 00:05:36,960
right file since Windows 10 1809 the

129
00:05:34,560 --> 00:05:38,880
format of the MSU has changed and now

130
00:05:36,960 --> 00:05:41,280
instead of embedding the files in their

131
00:05:38,880 --> 00:05:44,220
PE format they use a compressed format

132
00:05:41,280 --> 00:05:46,320
called Ms Delta patch one confusing

133
00:05:44,220 --> 00:05:49,139
thing at first is that they named the

134
00:05:46,320 --> 00:05:50,120
files with their normal extensions so

135
00:05:49,139 --> 00:05:53,280


136
00:05:50,120 --> 00:05:56,400
.exe    .dll.   .sys but
they are not actual PE

137
00:05:53,280 --> 00:05:58,860
files so you need to reconstruct them

138
00:05:56,400 --> 00:06:01,080
yourselves also in one given patch

139
00:05:58,860 --> 00:06:04,080
Tuesday update you will typically have

140
00:06:01,080 --> 00:06:06,380
two version of the same file so for

141
00:06:04,080 --> 00:06:10,259
instance you would have two versions of

142
00:06:06,380 --> 00:06:13,500
ntdll.dll one in a zip folder named R

143
00:06:10,259 --> 00:06:16,800
for reverse and another one with a zip

144
00:06:13,500 --> 00:06:19,199
folder named f for forward and so

145
00:06:16,800 --> 00:06:21,840
typically you would need the origin ISO

146
00:06:19,199 --> 00:06:24,960
image and you would use the file from

147
00:06:21,840 --> 00:06:26,759
the F forward folder to reconstruct the

148
00:06:24,960 --> 00:06:28,919
version of the file for that particular

149
00:06:26,759 --> 00:06:31,020
update Microsoft does not provide

150
00:06:28,919 --> 00:06:33,300
tooling to the conversion by the

151
00:06:31,020 --> 00:06:36,300
implement the functions in

152
00:06:33,300 --> 00:06:39,240
msdelta.dll so you can basically write

153
00:06:36,300 --> 00:06:41,639
your own tool for example in Python that

154
00:06:39,240 --> 00:06:43,740
calls into msdelta.dll in order to

155
00:06:41,639 --> 00:06:46,740
convert the files and get the version

156
00:06:43,740 --> 00:06:49,620
you want what is binary diffing it is

157
00:06:46,740 --> 00:06:51,720
not just the diffing raw bytes if we were

158
00:06:49,620 --> 00:06:53,880
just diffing raw bytes you would not be

159
00:06:51,720 --> 00:06:55,979
super efficient because basically if you

160
00:06:53,880 --> 00:06:58,680
compile source codes at two different

161
00:06:55,979 --> 00:07:01,080
times the resulting compiled code might

162
00:06:58,680 --> 00:07:03,060
be very different and a lot of bytes

163
00:07:01,080 --> 00:07:04,740
might have changed in between but it

164
00:07:03,060 --> 00:07:07,860
would not necessarily reflect source

165
00:07:04,740 --> 00:07:11,160
code changes you could also be because

166
00:07:07,860 --> 00:07:13,620
they added a new function somewhere and

167
00:07:11,160 --> 00:07:16,880
it shifted all the other functions after

168
00:07:13,620 --> 00:07:20,160
that function but there were no actual

169
00:07:16,880 --> 00:07:22,080
changes and the other functions might

170
00:07:20,160 --> 00:07:24,780
not have been modified at all another

171
00:07:22,080 --> 00:07:27,440
reason could be that the compiler was

172
00:07:24,780 --> 00:07:30,120
changed and so the resulting assembly

173
00:07:27,440 --> 00:07:32,520
instructions are different due to the

174
00:07:30,120 --> 00:07:34,680
compiler itself or it could also be due

175
00:07:32,520 --> 00:07:37,919
to really small differences into the

176
00:07:34,680 --> 00:07:40,139
source code that were compiled entirely

177
00:07:37,919 --> 00:07:42,180
differently by the compiler which is

178
00:07:40,139 --> 00:07:44,280
just trying to optimize stuff in general

179
00:07:42,180 --> 00:07:46,259
so yeah it would be really hard to know

180
00:07:44,280 --> 00:07:47,520
what changes were made just from the

181
00:07:46,259 --> 00:07:49,500
byte changes

182
00:07:47,520 --> 00:07:53,039
so generally binary diffing is about

183
00:07:49,500 --> 00:07:55,199
ignoring the bytes changes and ignoring

184
00:07:53,039 --> 00:07:57,479
even the assembly level differences

185
00:07:55,199 --> 00:08:00,419
instead binary diffing works on the

186
00:07:57,479 --> 00:08:03,000
actual functions flow graphs that means

187
00:08:00,419 --> 00:08:06,180
it is going to look at the basic blocks

188
00:08:03,000 --> 00:08:09,060
of a function so basically you would

189
00:08:06,180 --> 00:08:12,180
typically see basic blocks in a

190
00:08:09,060 --> 00:08:14,699
disassembler application like IDA or

191
00:08:12,180 --> 00:08:17,160
Ghidra because it's a series of

192
00:08:14,699 --> 00:08:18,840
instructions that if you execute the

193
00:08:17,160 --> 00:08:21,599
first instruction of that basic block

194
00:08:18,840 --> 00:08:23,639
you will necessarily execute all the

195
00:08:21,599 --> 00:08:25,860
instructions of that block until the

196
00:08:23,639 --> 00:08:28,759
last one of that block and so typically

197
00:08:25,860 --> 00:08:32,039
in instructions at the end of a block

198
00:08:28,759 --> 00:08:35,159
would jump to the first instruction of

199
00:08:32,039 --> 00:08:38,760
another basic block sometimes also an

200
00:08:35,159 --> 00:08:41,279
instructions can jump from the end of a

201
00:08:38,760 --> 00:08:42,719
basic block onto the middle of of

202
00:08:41,279 --> 00:08:45,120
another basic block

203
00:08:42,719 --> 00:08:47,100
and so you would only be looking at the

204
00:08:45,120 --> 00:08:49,860
number of basic blocks in order to do

205
00:08:47,100 --> 00:08:51,540
binary diffing not even the actual

206
00:08:49,860 --> 00:08:53,880
instructions that are part of that block

207
00:08:51,540 --> 00:08:55,800
assuming we are more interested to know

208
00:08:53,880 --> 00:08:58,560
if a new basic block was added or

209
00:08:55,800 --> 00:09:00,899
removed indicating a significant change

210
00:08:58,560 --> 00:09:02,640
in the functional logic we would also be

211
00:09:00,899 --> 00:09:04,920
looking at relations between the

212
00:09:02,640 --> 00:09:08,040
different functions since once we have

213
00:09:04,920 --> 00:09:10,800
figured out a given function call for

214
00:09:08,040 --> 00:09:13,380
example like a function calling three

215
00:09:10,800 --> 00:09:16,500
other functions inside that function we

216
00:09:13,380 --> 00:09:18,899
can assume it is the case for both the

217
00:09:16,500 --> 00:09:22,560
file before the patch and after the

218
00:09:18,899 --> 00:09:24,899
patch and so if that's the case it means

219
00:09:22,560 --> 00:09:27,120
the the three functions can be matched

220
00:09:24,899 --> 00:09:30,180
between the file before the patch and

221
00:09:27,120 --> 00:09:32,459
after the patch and this is true even if

222
00:09:30,180 --> 00:09:35,459
a new function was added in the patch

223
00:09:32,459 --> 00:09:37,740
file before these three functions and it

224
00:09:35,459 --> 00:09:40,260
shifted all these three functions in the

225
00:09:37,740 --> 00:09:42,240
final binary it does not matter because

226
00:09:40,260 --> 00:09:45,000
we are working on the function flow

227
00:09:42,240 --> 00:09:46,860
graphs and the cool thing is we can

228
00:09:45,000 --> 00:09:49,200
basically apply the same algorithms

229
00:09:46,860 --> 00:09:51,180
recursively to the new functions we have

230
00:09:49,200 --> 00:09:54,060
matched from the previous function we

231
00:09:51,180 --> 00:09:55,860
were diffing and do that over and over

232
00:09:54,060 --> 00:09:58,260
what is really interesting with this

233
00:09:55,860 --> 00:10:00,360
method is that it works pretty well and

234
00:09:58,260 --> 00:10:02,760
it gives very good results since it

235
00:10:00,360 --> 00:10:04,860
significantly reduces the number of

236
00:10:02,760 --> 00:10:08,339
functions that you have to manually look

237
00:10:04,860 --> 00:10:11,160
at when doing binary diffing bindiff was

238
00:10:08,339 --> 00:10:12,540
initially developed by xenomics and it

239
00:10:11,160 --> 00:10:14,760
was the first tool to actually implement

240
00:10:12,540 --> 00:10:16,459
this kind of algorithm and now there is

241
00:10:14,760 --> 00:10:20,100
Diaphora as well

242
00:10:16,459 --> 00:10:22,500
Diaphora is only for either IDA Pro or home

243
00:10:20,100 --> 00:10:25,399
at the moment and it doesn't support

244
00:10:22,500 --> 00:10:28,019
Ghidra at the time of recording

245
00:10:25,399 --> 00:10:30,300
Diaphra is basically a Python plugin

246
00:10:28,019 --> 00:10:32,399
that is open source so you can customize

247
00:10:30,300 --> 00:10:34,980
it for like specific algorithms or

248
00:10:32,399 --> 00:10:37,440
patterns you would want to use but

249
00:10:34,980 --> 00:10:40,320
because it relies on Python it's only

250
00:10:37,440 --> 00:10:42,600
available in IDA Pro or home it is

251
00:10:40,320 --> 00:10:44,700
also integrated to IDA what I mean by

252
00:10:42,600 --> 00:10:47,339
that is that you start IDA on both of

253
00:10:44,700 --> 00:10:49,680
your binaries that you want to diff you

254
00:10:47,339 --> 00:10:52,560
run the plugin on each side to generate

255
00:10:49,680 --> 00:10:55,320
a database associated with each file and

256
00:10:52,560 --> 00:10:58,320
then you diff one of the files directly

257
00:10:55,320 --> 00:11:01,560
into the other database directly from

258
00:10:58,320 --> 00:11:03,420
IDA and it will open new tabs in IDA

259
00:11:01,560 --> 00:11:06,300
with the function being matched between

260
00:11:03,420 --> 00:11:08,160
the two versions and the differences and

261
00:11:06,300 --> 00:11:09,779
then you can basically click on some

262
00:11:08,160 --> 00:11:11,820
results and it will show you the

263
00:11:09,779 --> 00:11:13,800
differences either at the assembly level

264
00:11:11,820 --> 00:11:16,019
or at the C Level if you have the

265
00:11:13,800 --> 00:11:18,060
decompiler all of that directly in IDA

266
00:11:16,019 --> 00:11:20,339
it is basically the recommended tool if

267
00:11:18,060 --> 00:11:23,279
you have IDA with Python supports so

268
00:11:20,339 --> 00:11:25,260
IDA Pro or IDA home licenses one

269
00:11:23,279 --> 00:11:27,420
real advantage of Diaphora that it

270
00:11:25,260 --> 00:11:29,640
supports the decompiler what I mean by

271
00:11:27,420 --> 00:11:32,339
that is that Diaphora supports the

272
00:11:29,640 --> 00:11:35,160
decompiler for testing the algorithm

273
00:11:32,339 --> 00:11:37,200
during comparison but also for showing

274
00:11:35,160 --> 00:11:39,420
the actual results after the binary

275
00:11:37,200 --> 00:11:41,940
diffing it is useful because sometimes

276
00:11:39,420 --> 00:11:44,519
applying the algorithm on the decompiled

277
00:11:41,940 --> 00:11:46,260
code gives you better results and it is

278
00:11:44,519 --> 00:11:48,420
also useful to be able to show the

279
00:11:46,260 --> 00:11:50,339
actual diff at the C level because it

280
00:11:48,420 --> 00:11:53,160
is sometimes super readable and you can

281
00:11:50,339 --> 00:11:55,560
spot the differences very quickly versus

282
00:11:53,160 --> 00:11:57,959
if you had to work it out at the basic

283
00:11:55,560 --> 00:11:59,640
block level like if the patch is an

284
00:11:57,959 --> 00:12:01,680
integer overflow or length check

285
00:11:59,640 --> 00:12:03,660
generally looking at the assembly diff

286
00:12:01,680 --> 00:12:05,700
is better because you can figure out

287
00:12:03,660 --> 00:12:07,800
exactly what is the problem and it is

288
00:12:05,700 --> 00:12:09,060
less likely to be error prone for

289
00:12:07,800 --> 00:12:12,899
instance if the decompiler is not

290
00:12:09,060 --> 00:12:15,600
perfect and showing things badly however

291
00:12:12,899 --> 00:12:18,060
if the patch is changing the general

292
00:12:15,600 --> 00:12:20,640
logic of the function by adding a lot of

293
00:12:18,060 --> 00:12:22,860
changes at the time it is often very

294
00:12:20,640 --> 00:12:25,440
useful to have the diff at the C level

295
00:12:22,860 --> 00:12:28,140
because you can spot the actual changes

296
00:12:25,440 --> 00:12:30,720
a lot quicker an interesting thing is

297
00:12:28,140 --> 00:12:34,740
that for instance if you wanted to diff

298
00:12:30,720 --> 00:12:37,079
x86 versus Arm 32 bits because ARM32

299
00:12:34,740 --> 00:12:39,600
instructions have a lot of conditional

300
00:12:37,079 --> 00:12:42,060
instructions the actual function graph

301
00:12:39,600 --> 00:12:44,940
will look significantly different

302
00:12:42,060 --> 00:12:47,100
because a single basic block might have

303
00:12:44,940 --> 00:12:50,639
a lot of instructions that are actually

304
00:12:47,100 --> 00:12:53,279
conditional but x86 would have a more

305
00:12:50,639 --> 00:12:58,260
complicated flow graph than arm32

306
00:12:53,279 --> 00:13:00,600
possibly if you want to diff x86 and x64

307
00:12:58,260 --> 00:13:02,820
they are relatively the same so their

308
00:13:00,600 --> 00:13:04,740
flow graph at the assembly level will be

309
00:13:02,820 --> 00:13:07,200
quite similar what is funny with

310
00:13:04,740 --> 00:13:09,000
Diaphora too is you can actually do

311
00:13:07,200 --> 00:13:11,040
binary diffing between two different

312
00:13:09,000 --> 00:13:13,920
architecture in general because when you

313
00:13:11,040 --> 00:13:16,200
work at the decompiler level the C code

314
00:13:13,920 --> 00:13:19,980
is very likely to be the same and so

315
00:13:16,200 --> 00:13:22,620
I've used the Diaphora between x86 and

316
00:13:19,980 --> 00:13:26,160
64 in the past and it was pretty helpful

317
00:13:22,620 --> 00:13:29,220
the main advantage of bindiff is that it

318
00:13:26,160 --> 00:13:31,500
supports Ghidra on top of IDA and so bin

319
00:13:29,220 --> 00:13:34,440
diff is quite different from Diaphora in

320
00:13:31,500 --> 00:13:36,480
the way you would typically use it you

321
00:13:34,440 --> 00:13:39,720
basically run the plugin from IDA or

322
00:13:36,480 --> 00:13:42,420
Ghidra on each binary to generate a

323
00:13:39,720 --> 00:13:45,540
database associated with each file in

324
00:13:42,420 --> 00:13:49,440
what we call the bin export format and

325
00:13:45,540 --> 00:13:52,200
then this bin exports format is imported

326
00:13:49,440 --> 00:13:54,360
directly into bindiff into like a

327
00:13:52,200 --> 00:13:57,480
standalone bindiff tool that will

328
00:13:54,360 --> 00:13:59,339
display the diff what that means is that

329
00:13:57,480 --> 00:14:01,980
you don't actually use IDA or Ghidra

330
00:13:59,339 --> 00:14:03,959
to see the actual diff so you can't

331
00:14:01,980 --> 00:14:06,420
really navigate in your disassembler or

332
00:14:03,959 --> 00:14:09,480
decompiler to see functions being called

333
00:14:06,420 --> 00:14:12,120
Etc which makes it a lot less nice to

334
00:14:09,480 --> 00:14:14,459
use in my opinion also because it is the

335
00:14:12,120 --> 00:14:17,160
Standalone BIN DIFF tool that display the

336
00:14:14,459 --> 00:14:19,019
diff and this tool only supports the

337
00:14:17,160 --> 00:14:21,600
assembly level it doesn't have a

338
00:14:19,019 --> 00:14:24,240
decompiler you can't see the diff at the

339
00:14:21,600 --> 00:14:25,860
decompiler level either Ghidra does

340
00:14:24,240 --> 00:14:28,560
support something called version

341
00:14:25,860 --> 00:14:31,440
tracking and when Ghidra was released

342
00:14:28,560 --> 00:14:34,399
people tried to use that feature to do

343
00:14:31,440 --> 00:14:38,399
binary diffing but it is not actually

344
00:14:34,399 --> 00:14:41,399
binary diffing as we define it, it is just

345
00:14:38,399 --> 00:14:44,100
byte diffing which is basically useless

346
00:14:41,399 --> 00:14:47,399
from our perspective so basically if you

347
00:14:44,100 --> 00:14:50,699
have IDA and the hex rays decompiler the

348
00:14:47,399 --> 00:14:53,519
best is to use Diaphora since you'll get

349
00:14:50,699 --> 00:14:56,160
the C level diffing on top of the

350
00:14:53,519 --> 00:14:58,920
assembly diffing but if you don't have

351
00:14:56,160 --> 00:15:01,500
the hex rays  decompiler in IDA there

352
00:14:58,920 --> 00:15:03,720
is an alternate three-step way of doing

353
00:15:01,500 --> 00:15:08,160
things so basically I would recommend

354
00:15:03,720 --> 00:15:11,040
you use Diaphora or bin diff in IDA or

355
00:15:08,160 --> 00:15:12,959
even bindiff in Ghidra just to get the

356
00:15:11,040 --> 00:15:15,240
disassembly level diffing so you can

357
00:15:12,959 --> 00:15:18,360
look at the interesting functions that

358
00:15:15,240 --> 00:15:21,240
have been changed and then since Ghidra

359
00:15:18,360 --> 00:15:23,940
is free and has a decompiler you can

360
00:15:21,240 --> 00:15:26,459
document these functions in the Ghidra

361
00:15:23,940 --> 00:15:30,240
decompiled code and finally you can use

362
00:15:26,459 --> 00:15:33,779
the the command line tool called diff or

363
00:15:30,240 --> 00:15:36,660
even like a a tool like win merge to

364
00:15:33,779 --> 00:15:39,660
compare the decompiler output

365
00:15:36,660 --> 00:15:41,880
as if they were actually text files or

366
00:15:39,660 --> 00:15:45,360
source code files so yeah just to

367
00:15:41,880 --> 00:15:47,760
summarize Diaphora works very well if

368
00:15:45,360 --> 00:15:50,699
you have a decompiler in IDA and it

369
00:15:47,760 --> 00:15:53,339
doesn't support Ghidra bindiff does work

370
00:15:50,699 --> 00:15:55,800
both with Ghidra and IDA but doesn't

371
00:15:53,339 --> 00:15:59,839
actually support the decompiler and is

372
00:15:55,800 --> 00:15:59,839
its own Standalone tool

