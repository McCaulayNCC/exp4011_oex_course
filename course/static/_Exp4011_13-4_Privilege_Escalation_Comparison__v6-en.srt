1
00:00:00,240 --> 00:00:05,700
hi everyone

2
00:00:01,620 --> 00:00:08,160
we love the write zero primitive combined

3
00:00:05,700 --> 00:00:10,080
with a writing PreviousMode because it

4
00:00:08,160 --> 00:00:12,900
gives us this good mode that allows us

5
00:00:10,080 --> 00:00:15,960
to do everything we can and want to do

6
00:00:12,900 --> 00:00:17,699
from user land however

7
00:00:15,960 --> 00:00:20,939
we're going to see that it doesn't work

8
00:00:17,699 --> 00:00:22,740
on every architecture and or Windows

9
00:00:20,939 --> 00:00:24,420
operating system so we're going to see

10
00:00:22,740 --> 00:00:27,119
the limitations

11
00:00:24,420 --> 00:00:28,140
and what to use on what environment

12
00:00:27,119 --> 00:00:30,599
really

13
00:00:28,140 --> 00:00:33,180
and we are going to compare it with the

14
00:00:30,599 --> 00:00:34,739
increment primitive okay let's get

15
00:00:33,180 --> 00:00:36,960
started

16
00:00:34,739 --> 00:00:39,300
so let's analyze one more time the

17
00:00:36,960 --> 00:00:42,120
function that gives us the write zero

18
00:00:39,300 --> 00:00:45,059
primitive like the Ki try and Y thread

19
00:00:42,120 --> 00:00:48,180
function so remember we need to pass

20
00:00:45,059 --> 00:00:51,180
this spinlock that is checking that the

21
00:00:48,180 --> 00:00:54,239
lower bits of the shred log is initially

22
00:00:51,180 --> 00:00:56,579
zero and then this lower bit is changed

23
00:00:54,239 --> 00:01:00,780
to one and finally at the end of the

24
00:00:56,579 --> 00:01:04,140
function the full 64-bit thread lock is

25
00:01:00,780 --> 00:01:06,659
set to zero which gives us the right

26
00:01:04,140 --> 00:01:09,540
zero primitive and it allows us to

27
00:01:06,659 --> 00:01:13,680
override PreviousMode and so we noticed

28
00:01:09,540 --> 00:01:16,380
this on 64-bit Windows 10 operating

29
00:01:13,680 --> 00:01:19,080
system but these operations could

30
00:01:16,380 --> 00:01:21,720
actually differ depending on the

31
00:01:19,080 --> 00:01:26,100
operating system version in other words

32
00:01:21,720 --> 00:01:29,159
it all depends what function or macro is

33
00:01:26,100 --> 00:01:31,979
called for the Locking and for the

34
00:01:29,159 --> 00:01:34,740
unlocking operations and interestingly

35
00:01:31,979 --> 00:01:38,759
it turns out that Microsoft has changed

36
00:01:34,740 --> 00:01:40,560
the function being used over time so

37
00:01:38,759 --> 00:01:43,200
let's analyze more deeply what is

38
00:01:40,560 --> 00:01:47,520
happening for the Locking operation on

39
00:01:43,200 --> 00:01:50,640
64 bits so the bit tests and set

40
00:01:47,520 --> 00:01:53,220
instruction like the BTS instruction is

41
00:01:50,640 --> 00:01:56,280
basically going to retrieve the single

42
00:01:53,220 --> 00:01:58,560
bits at the position indicated by the

43
00:01:56,280 --> 00:02:01,799
second operand and it is going to

44
00:01:58,560 --> 00:02:04,560
retrieve that bit from the bit string

45
00:02:01,799 --> 00:02:06,719
indicated by the first of our end and it

46
00:02:04,560 --> 00:02:10,020
is going to set this information like

47
00:02:06,719 --> 00:02:12,060
this single bit in the carry flag which

48
00:02:10,020 --> 00:02:15,000
is possible because it's a single bit

49
00:02:12,060 --> 00:02:18,239
value right and finally it is going to

50
00:02:15,000 --> 00:02:21,420
replace that bit in the original build

51
00:02:18,239 --> 00:02:24,300
string with the value 1 and so in the

52
00:02:21,420 --> 00:02:29,520
example above it is going to check the

53
00:02:24,300 --> 00:02:32,160
bit 0 in the stradlock keyword and save

54
00:02:29,520 --> 00:02:34,500
that into the carry carry flag and

55
00:02:32,160 --> 00:02:37,680
finally it is going to replace that bits

56
00:02:34,500 --> 00:02:42,959
at index 0 in the threadlock keyword

57
00:02:37,680 --> 00:02:46,500
with value 1 and the log prefix is just

58
00:02:42,959 --> 00:02:49,319
here to ensure that the CPU has

59
00:02:46,500 --> 00:02:51,599
exclusive ownership of the appropriate

60
00:02:49,319 --> 00:02:54,239
cache for the duration of the

61
00:02:51,599 --> 00:02:56,760
instruction that follows so in this case

62
00:02:54,239 --> 00:03:00,060
the BTS instruction and so it's kind of

63
00:02:56,760 --> 00:03:03,120
a way to make sure the instruction that

64
00:03:00,060 --> 00:03:05,640
follows is kind of atomic and so now

65
00:03:03,120 --> 00:03:09,060
let's have a look at the unlocking

66
00:03:05,640 --> 00:03:11,760
operation again on 64-bits and so in

67
00:03:09,060 --> 00:03:15,000
this case we see that it's an and

68
00:03:11,760 --> 00:03:17,580
operation so it's going to work on the

69
00:03:15,000 --> 00:03:19,739
full keyword and so the fact that there

70
00:03:17,580 --> 00:03:23,519
is a difference between the Locking

71
00:03:19,739 --> 00:03:26,459
which is done on a single bits like the

72
00:03:23,519 --> 00:03:29,819
lowest bits and the unlocking operation

73
00:03:26,459 --> 00:03:32,220
which is done on the four keyword this

74
00:03:29,819 --> 00:03:34,620
is what makes it possible to override

75
00:03:32,220 --> 00:03:37,500
the PreviousMode as we saw it in

76
00:03:34,620 --> 00:03:40,500
previous section of this course and so

77
00:03:37,500 --> 00:03:44,700
now let's look at the same kind of

78
00:03:40,500 --> 00:03:47,400
locking operation on 32 bits so and and

79
00:03:44,700 --> 00:03:50,159
on older operating system versions like

80
00:03:47,400 --> 00:03:53,040
Windows 7 or Vista and it's interesting

81
00:03:50,159 --> 00:03:56,220
because we actually see that the Locking

82
00:03:53,040 --> 00:04:00,780
actually works on the full 32-bit value

83
00:03:56,220 --> 00:04:03,180
and not on the actual single bits so if

84
00:04:00,780 --> 00:04:07,739
we look at the assembly we see the

85
00:04:03,180 --> 00:04:11,580
original stradlock was in ebx then it

86
00:04:07,739 --> 00:04:15,480
was moved to ECX and then we see the

87
00:04:11,580 --> 00:04:19,199
value was retrieved in eax using the

88
00:04:15,480 --> 00:04:22,199
exchange instruction and so eax holds

89
00:04:19,199 --> 00:04:25,020
the actual shredlock value before any

90
00:04:22,199 --> 00:04:26,880
modification and so finally we see the

91
00:04:25,020 --> 00:04:28,740
check that is done on the on this

92
00:04:26,880 --> 00:04:31,199
original value using the test

93
00:04:28,740 --> 00:04:34,680
instruction and so we see it's done on

94
00:04:31,199 --> 00:04:37,320
the full 32-bit value the ex register

95
00:04:34,680 --> 00:04:40,380
and so it basically means we can't use

96
00:04:37,320 --> 00:04:43,680
the PreviousMode scenario at all on

97
00:04:40,380 --> 00:04:45,540
this operating system versions it's

98
00:04:43,680 --> 00:04:48,419
basically because we need the full

99
00:04:45,540 --> 00:04:51,380
32-bit thread lock to be zero in order

100
00:04:48,419 --> 00:04:54,900
to pass the spinlock which will then

101
00:04:51,380 --> 00:04:58,139
set it to 1 and then it will actually

102
00:04:54,900 --> 00:05:00,840
set again the full 32-bit thread lock

103
00:04:58,139 --> 00:05:03,600
back to zero at the very end of the

104
00:05:00,840 --> 00:05:06,000
function but the problem is in the

105
00:05:03,600 --> 00:05:07,560
context of writing PreviousMode it

106
00:05:06,000 --> 00:05:10,440
means we would need to have previous

107
00:05:07,560 --> 00:05:13,080
mode set to zero initially so we can

108
00:05:10,440 --> 00:05:15,120
override it again with zero so it's

109
00:05:13,080 --> 00:05:18,000
completely useless from an attacker

110
00:05:15,120 --> 00:05:21,300
perspective okay so we have looked at

111
00:05:18,000 --> 00:05:23,340
the Locking and unlocking operations on

112
00:05:21,300 --> 00:05:25,620
various operating systems and

113
00:05:23,340 --> 00:05:28,680
architectures another thing we want to

114
00:05:25,620 --> 00:05:32,039
check on various summer environments is

115
00:05:28,680 --> 00:05:34,500
how PreviousMode is handled across

116
00:05:32,039 --> 00:05:36,900
ciscals and so interestingly if we look

117
00:05:34,500 --> 00:05:40,199
at the Microsoft documentation on the

118
00:05:36,900 --> 00:05:43,199
msdn this is what we read when a user

119
00:05:40,199 --> 00:05:45,680
remote application called the NT or ZW

120
00:05:43,199 --> 00:05:48,960
version of a native system Services

121
00:05:45,680 --> 00:05:51,120
routing the system call mechanism tracks

122
00:05:48,960 --> 00:05:53,580
the calling thread to kernel mode then

123
00:05:51,120 --> 00:05:57,000
to indicate that the parameter values

124
00:05:53,580 --> 00:06:00,060
originated in user mode the Trap handler

125
00:05:57,000 --> 00:06:03,240
for the system core sets the previous

126
00:06:00,060 --> 00:06:05,880
mode field in the thread object so the

127
00:06:03,240 --> 00:06:09,120
case thread for the color to user mode

128
00:06:05,880 --> 00:06:12,539
which means to one and then the native

129
00:06:09,120 --> 00:06:14,759
system Services routing checks the

130
00:06:12,539 --> 00:06:17,340
PreviousMode field of the coding thread

131
00:06:14,759 --> 00:06:20,280
to determine whether the parameters are

132
00:06:17,340 --> 00:06:24,000
from a user mode source and so what this

133
00:06:20,280 --> 00:06:26,880
means is that for every syscall made by a

134
00:06:24,000 --> 00:06:29,419
username thread the kernel should reset

135
00:06:26,880 --> 00:06:32,039
the PreviousMode to one effectively

136
00:06:29,419 --> 00:06:34,560
overwriting any previous value that was

137
00:06:32,039 --> 00:06:37,740
there beforehand but this control

138
00:06:34,560 --> 00:06:40,680
predicts what we saw on Windows 10 1809

139
00:06:37,740 --> 00:06:43,139
right because we confirmed by testing

140
00:06:40,680 --> 00:06:46,740
that once we have overwritten previous

141
00:06:43,139 --> 00:06:49,259
mode with zero then we can just call NT

142
00:06:46,740 --> 00:06:51,720
read virtual memory or NT write virtual

143
00:06:49,259 --> 00:06:54,600
memory and the previous one set to zero

144
00:06:51,720 --> 00:06:57,600
just persists among C scores and so the

145
00:06:54,600 --> 00:07:01,259
question is why and he did the same

146
00:06:57,600 --> 00:07:04,259
across all operating systems and so if

147
00:07:01,259 --> 00:07:06,300
you reverse engineer the Trap handler in

148
00:07:04,259 --> 00:07:09,180
the kernel you will see that the kernel

149
00:07:06,300 --> 00:07:11,819
always assumes that a user land thread

150
00:07:09,180 --> 00:07:14,940
doing a syscall has its PreviousMode set

151
00:07:11,819 --> 00:07:17,940
to 1 in its case thread structure and

152
00:07:14,940 --> 00:07:21,060
will not actually enforce that for every

153
00:07:17,940 --> 00:07:22,680
syscall the only place where the kernel

154
00:07:21,060 --> 00:07:25,560
actually changes the PreviousMode

155
00:07:22,680 --> 00:07:28,620
manually is for optimization reasons

156
00:07:25,560 --> 00:07:30,780
when at some point some kernel code

157
00:07:28,620 --> 00:07:33,300
needs to call other functions in the

158
00:07:30,780 --> 00:07:34,940
kernel it will save the old previous

159
00:07:33,300 --> 00:07:38,220
mode and then

160
00:07:34,940 --> 00:07:40,680
temporarily set PreviousMode to zero in

161
00:07:38,220 --> 00:07:43,500
order to avoid certain checks to be made

162
00:07:40,680 --> 00:07:46,080
and after it will restore the old

163
00:07:43,500 --> 00:07:48,900
PreviousMode the idea behind doing that

164
00:07:46,080 --> 00:07:51,020
is that if a thread that originally came

165
00:07:48,900 --> 00:07:53,220
from user land triggered the syscall

166
00:07:51,020 --> 00:07:55,919
theoretically you would have a previous

167
00:07:53,220 --> 00:07:58,500
mode set to 1 because coming from user

168
00:07:55,919 --> 00:08:01,020
land and if at some point internally

169
00:07:58,500 --> 00:08:04,020
one of the kernel function actually calls

170
00:08:01,020 --> 00:08:06,900
another Canal function the code would

171
00:08:04,020 --> 00:08:09,300
temporarily set PreviousMode to zero in

172
00:08:06,900 --> 00:08:12,180
order to speed up function calls and

173
00:08:09,300 --> 00:08:14,039
avoid checking the arguments because the

174
00:08:12,180 --> 00:08:16,199
arguments have been provided by the

175
00:08:14,039 --> 00:08:18,479
kernel and not from user land and then

176
00:08:16,199 --> 00:08:21,000
the old PreviousMode would then be

177
00:08:18,479 --> 00:08:23,819
restored into the case thread structure

178
00:08:21,000 --> 00:08:26,639
after the function calls so before the

179
00:08:23,819 --> 00:08:29,099
thread returns to username but I guess

180
00:08:26,639 --> 00:08:32,880
the main takeaway from this slide is

181
00:08:29,099 --> 00:08:35,039
that on 64 bits the PreviousMode is

182
00:08:32,880 --> 00:08:38,159
preserved among ciscals

183
00:08:35,039 --> 00:08:40,200
and is not enforced by the kernel when a

184
00:08:38,159 --> 00:08:42,060
syscall is made from username so it's

185
00:08:40,200 --> 00:08:45,540
different from what the Microsoft

186
00:08:42,060 --> 00:08:48,779
documentation stated however if we go

187
00:08:45,540 --> 00:08:51,060
back to 32-bits interestingly in this

188
00:08:48,779 --> 00:08:53,459
case it is done accordingly to the

189
00:08:51,060 --> 00:08:56,339
Microsoft documentation basically the

190
00:08:53,459 --> 00:09:00,240
PreviousMode is enforced by the kernel

191
00:08:56,339 --> 00:09:03,720
for every single syscall and it is done by

192
00:09:00,240 --> 00:09:06,540
looking up a real PreviousMode value in

193
00:09:03,720 --> 00:09:09,300
the Cs register like the segment

194
00:09:06,540 --> 00:09:13,080
register which we can't tamper with from

195
00:09:09,300 --> 00:09:15,300
username and so theoretically there is

196
00:09:13,080 --> 00:09:18,060
still a way to abuse these PreviousMode

197
00:09:15,300 --> 00:09:20,940
on 32 bits which is that we could have

198
00:09:18,060 --> 00:09:23,880
two threads so we would have thread a

199
00:09:20,940 --> 00:09:27,180
that keeps setting the PreviousMode of

200
00:09:23,880 --> 00:09:30,600
straight B to the value 0 and then have

201
00:09:27,180 --> 00:09:32,880
thread B calling NT read virtual

202
00:09:30,600 --> 00:09:35,640
numerator or NT red virtual memory in a

203
00:09:32,880 --> 00:09:37,620
loop until it succeeds indicate thing we

204
00:09:35,640 --> 00:09:40,260
won the race condition from like

205
00:09:37,620 --> 00:09:42,360
changing PreviousMode from thread a and

206
00:09:40,260 --> 00:09:44,459
so in this case the race condition is a

207
00:09:42,360 --> 00:09:47,519
different race condition from the KTM

208
00:09:44,459 --> 00:09:50,100
bug obviously since it's about racing

209
00:09:47,519 --> 00:09:52,140
changes for the PreviousMode for

210
00:09:50,100 --> 00:09:54,540
another thread the other thing we

211
00:09:52,140 --> 00:09:57,060
noticed with the increment primitive is

212
00:09:54,540 --> 00:09:59,459
that it is a lot more complex than the

213
00:09:57,060 --> 00:10:01,920
write zero primitive this is because in

214
00:09:59,459 --> 00:10:04,620
the case of the write zero primitive all

215
00:10:01,920 --> 00:10:06,779
we need is to read the Ki try and white

216
00:10:04,620 --> 00:10:09,180
thread function and we don't have to

217
00:10:06,779 --> 00:10:11,459
worry about crafting any valid K thread

218
00:10:09,180 --> 00:10:14,459
however we saw that for the increment

219
00:10:11,459 --> 00:10:15,720
primitive we need to craft a valid phase

220
00:10:14,459 --> 00:10:18,180
thread like

221
00:10:15,720 --> 00:10:21,180
at least semi-valid case trade structure

222
00:10:18,180 --> 00:10:23,459
and then sets a lot of its field and

223
00:10:21,180 --> 00:10:25,920
other structures to avoid our fake

224
00:10:23,459 --> 00:10:27,720
thread to be scheduled and I guess one

225
00:10:25,920 --> 00:10:30,720
more thing about the increment primitive

226
00:10:27,720 --> 00:10:33,839
is that it only allows us to increment

227
00:10:30,720 --> 00:10:37,080
by one a given value so we need to chain

228
00:10:33,839 --> 00:10:39,420
it several times in order to reach any

229
00:10:37,080 --> 00:10:42,180
value we want which makes it a lot more

230
00:10:39,420 --> 00:10:44,339
complicated than just writing 0 to

231
00:10:42,180 --> 00:10:46,440
PreviousMode because in this case we

232
00:10:44,339 --> 00:10:48,360
obtain a full arbitrary read write

233
00:10:46,440 --> 00:10:51,420
primitive into the kernel from user

234
00:10:48,360 --> 00:10:53,940
land and so this table summarizes the

235
00:10:51,420 --> 00:10:56,640
differences between the write zero

236
00:10:53,940 --> 00:10:58,560
primitive and the increment primitive we

237
00:10:56,640 --> 00:11:01,860
can probably start from the bottom of

238
00:10:58,560 --> 00:11:04,740
the table because Windows 10 1809 is the

239
00:11:01,860 --> 00:11:07,680
version We exploited with the write zero

240
00:11:04,740 --> 00:11:11,220
primitive and so basically for all the

241
00:11:07,680 --> 00:11:14,279
64-bit versions from Windows 7 and above

242
00:11:11,220 --> 00:11:16,980
we know the write zero primitive works

243
00:11:14,279 --> 00:11:19,980
because the lock is done on the lower

244
00:11:16,980 --> 00:11:23,399
single beats and the unlock is done on

245
00:11:19,980 --> 00:11:26,040
the full 64-bit value and we also

246
00:11:23,399 --> 00:11:29,700
confirmed that the PreviousMode is

247
00:11:26,040 --> 00:11:32,519
persistent among ciscalls despite the

248
00:11:29,700 --> 00:11:34,680
what the Microsoft documentation States

249
00:11:32,519 --> 00:11:38,519
we also noticed by reversing

250
00:11:34,680 --> 00:11:40,620
specifically Vista 64 bits that the

251
00:11:38,519 --> 00:11:43,019
write zero primitive doesn't work

252
00:11:40,620 --> 00:11:45,779
because of some code differences that

253
00:11:43,019 --> 00:11:48,600
being said because PreviousMode is

254
00:11:45,779 --> 00:11:51,480
persistent what we could do is just use

255
00:11:48,600 --> 00:11:54,480
the increment primitive to override

256
00:11:51,480 --> 00:11:57,000
PreviousMode to zero and do that once

257
00:11:54,480 --> 00:11:58,980
and then just use the arbitrary read

258
00:11:57,000 --> 00:12:01,500
write primitive from user land Colleen

259
00:11:58,980 --> 00:12:03,779
NT read virtual memory or NT write

260
00:12:01,500 --> 00:12:06,360
virtual memory so it's still potentially

261
00:12:03,779 --> 00:12:09,000
doable to implement like privilege

262
00:12:06,360 --> 00:12:10,740
escalation using the PreviousMode

263
00:12:09,000 --> 00:12:13,860
technique and I guess the funny thing

264
00:12:10,740 --> 00:12:17,040
about this is that because in these days

265
00:12:13,860 --> 00:12:20,820
we're using the increment primitive to

266
00:12:17,040 --> 00:12:24,060
change PreviousMode from 1 to 0 we are

267
00:12:20,820 --> 00:12:28,680
going to basically need to set the full

268
00:12:24,060 --> 00:12:32,220
32-bit value aligned with the one byte

269
00:12:28,680 --> 00:12:35,220
PreviousMode field indicated thread

270
00:12:32,220 --> 00:12:41,060
structure and we're going to need to set

271
00:12:35,220 --> 00:12:45,360
that for 32 bits value to FF FF

272
00:12:41,060 --> 00:12:49,200
and then increment it once to reach 0

273
00:12:45,360 --> 00:12:52,079
for that 32-bit value and since it will

274
00:12:49,200 --> 00:12:54,600
align with PreviousMode it will allow

275
00:12:52,079 --> 00:12:57,600
us to get previous modes set to zero

276
00:12:54,600 --> 00:13:02,040
indicating Cannon Mode and then we have

277
00:12:57,600 --> 00:13:06,360
more complicated cases basically on 32

278
00:13:02,040 --> 00:13:09,060
bits and for Windows 8 and above the red

279
00:13:06,360 --> 00:13:10,980
zero primitive is possible due to the

280
00:13:09,060 --> 00:13:13,079
difference of granularity between the

281
00:13:10,980 --> 00:13:16,200
Locking and unlocking but because

282
00:13:13,079 --> 00:13:19,079
PreviousMode is not persistent among

283
00:13:16,200 --> 00:13:21,779
ciscolls we would have to raise it for

284
00:13:19,079 --> 00:13:25,320
every single syscall which makes it quite

285
00:13:21,779 --> 00:13:29,519
annoying to use and then for even older

286
00:13:25,320 --> 00:13:32,459
versions on 32 bits so for Windows 7 and

287
00:13:29,519 --> 00:13:35,399
Below it's even worse we can't even use

288
00:13:32,459 --> 00:13:38,399
the write zero primitive due to the full

289
00:13:35,399 --> 00:13:40,800
32-bit being used for both the Locking

290
00:13:38,399 --> 00:13:43,740
and unlocking and also PreviousMode is

291
00:13:40,800 --> 00:13:45,480
not persistent so if we ever wanted to

292
00:13:43,740 --> 00:13:47,940
add use PreviousMode in order to

293
00:13:45,480 --> 00:13:50,399
elevate privileges we would have to use

294
00:13:47,940 --> 00:13:53,820
the increment primitive to override

295
00:13:50,399 --> 00:13:56,519
PreviousMode to zero and try to race it

296
00:13:53,820 --> 00:13:58,500
for every single syscall and so this will

297
00:13:56,519 --> 00:14:00,779
be really really hard because the

298
00:13:58,500 --> 00:14:03,540
increment primitive would require a lot

299
00:14:00,779 --> 00:14:06,480
of fake enlistments in order to change

300
00:14:03,540 --> 00:14:08,820
PreviousMode from 1 to 0 and at the

301
00:14:06,480 --> 00:14:11,220
same time we will try to range from

302
00:14:08,820 --> 00:14:13,680
another thread in order to abuse the

303
00:14:11,220 --> 00:14:16,200
overwritten PreviousMode to zero to do

304
00:14:13,680 --> 00:14:18,660
something useful like an actual write or

305
00:14:16,200 --> 00:14:21,060
read in the channel and so it would be

306
00:14:18,660 --> 00:14:23,700
quite unlikely to work properly and so

307
00:14:21,060 --> 00:14:26,100
in this last case it's actually very

308
00:14:23,700 --> 00:14:28,800
useful to have the increment primitive

309
00:14:26,100 --> 00:14:31,380
to work like Standalone without using

310
00:14:28,800 --> 00:14:33,120
PreviousMode at all because it means we

311
00:14:31,380 --> 00:14:36,000
can actually explore the vulnerability

312
00:14:33,120 --> 00:14:39,540
still and so in practice actually for

313
00:14:36,000 --> 00:14:41,519
the three top rows it's actually very

314
00:14:39,540 --> 00:14:44,220
useful to have the increment primitive

315
00:14:41,519 --> 00:14:47,279
because it simplifies exploitation a lot

316
00:14:44,220 --> 00:14:50,120
even if it's a little bit slower to run

317
00:14:47,279 --> 00:14:50,120
the exploits

