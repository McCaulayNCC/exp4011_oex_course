1
00:00:00,060 --> 00:00:04,740
so in this part we're going to look into

2
00:00:02,100 --> 00:00:06,600
the different KTM objects we can

3
00:00:04,740 --> 00:00:08,760
allocate into the kernel we're going to

4
00:00:06,600 --> 00:00:11,099
look into how we can do that from userland

5
00:00:08,760 --> 00:00:13,219
 by calling certain KTM related

6
00:00:11,099 --> 00:00:15,599
APIs we're looking to creating

7
00:00:13,219 --> 00:00:18,420
transactions transaction managers

8
00:00:15,599 --> 00:00:19,440
resource managers and enlistments we're

9
00:00:18,420 --> 00:00:21,600
going to also see that they use

10
00:00:19,440 --> 00:00:24,180
different abbreviations for all of these

11
00:00:21,600 --> 00:00:27,119
different objects TX for transactions TM

12
00:00:24,180 --> 00:00:30,599
for transaction manager RM for resource

13
00:00:27,119 --> 00:00:33,180
manager and EN for enlistment okay let's

14
00:00:30,599 --> 00:00:35,219
get started so a transaction structure

15
00:00:33,180 --> 00:00:37,020
is kind of what you deal with at the

16
00:00:35,219 --> 00:00:38,940
very beginning when you say you are

17
00:00:37,020 --> 00:00:41,100
going to do some work you're going to

18
00:00:38,940 --> 00:00:43,980
need to create a transaction and so

19
00:00:41,100 --> 00:00:46,980
there is a create transaction API which

20
00:00:43,980 --> 00:00:49,140
as it its name implies allows you to

21
00:00:46,980 --> 00:00:51,780
create a transaction it gives you back a

22
00:00:49,140 --> 00:00:53,760
handle to a transaction and on the kernel

23
00:00:51,780 --> 00:00:55,739
side there is actually a KTRANSACTION

24
00:00:53,760 --> 00:00:57,420
structure by reverse engineering and

25
00:00:55,739 --> 00:00:59,399
debugging you can realize that the

26
00:00:57,420 --> 00:01:02,820
transaction is allocated on the non-page

27
00:00:59,399 --> 00:01:04,799
pool and it has a specific pool tag TmTx

28
00:01:02,820 --> 00:01:07,320
which can be useful when poking around

29
00:01:04,799 --> 00:01:10,500
in memory with WinDbg it seems the

30
00:01:07,320 --> 00:01:12,840
convention is to have the TM suffix to

31
00:01:10,500 --> 00:01:16,140
indicate it is in the KTM Channel

32
00:01:12,840 --> 00:01:18,000
component and then they use a suffix to

33
00:01:16,140 --> 00:01:21,180
indicate the type of the object so here

34
00:01:18,000 --> 00:01:24,540
for transaction they use TX and we end

35
00:01:21,180 --> 00:01:27,540
up with the TmTx pool tag but this is

36
00:01:24,540 --> 00:01:30,780
the type of thing although the Kaspersky

37
00:01:27,540 --> 00:01:33,000
blog also says it there's something you

38
00:01:30,780 --> 00:01:36,540
could realize when playing with the KTM

39
00:01:33,000 --> 00:01:39,180
APIs that basically all KTM objects are

40
00:01:36,540 --> 00:01:40,860
allocated on the non-page pool and so

41
00:01:39,180 --> 00:01:42,720
you will probably have to research the

42
00:01:40,860 --> 00:01:45,360
non-page pool if you don't know anything

43
00:01:42,720 --> 00:01:48,060
about it at that time which is perfectly

44
00:01:45,360 --> 00:01:50,700
fine and will do it and so this is an

45
00:01:48,060 --> 00:01:52,740
example of what we find allows us to

46
00:01:50,700 --> 00:01:54,720
create a transaction so the argument for

47
00:01:52,740 --> 00:01:56,880
the create transaction API are

48
00:01:54,720 --> 00:01:59,399
documented on the msdn and they are not

49
00:01:56,880 --> 00:02:01,799
particularly useful or relevant in our

50
00:01:59,399 --> 00:02:04,740
perspective we just want a transaction

51
00:02:01,799 --> 00:02:06,360
to exist so we can interact with KTM and

52
00:02:04,740 --> 00:02:08,399
so we don't really need to pass any

53
00:02:06,360 --> 00:02:10,979
useful argument as you can see and you

54
00:02:08,399 --> 00:02:13,500
can make sure it works by debugging in

55
00:02:10,979 --> 00:02:15,840
the kernel and making sure that

56
00:02:13,500 --> 00:02:17,819
KTRANSACTION structure is created and

57
00:02:15,840 --> 00:02:19,200
that we get a handle in userland so

58
00:02:17,819 --> 00:02:21,660
just looking at the KTRANSACTION

59
00:02:19,200 --> 00:02:23,280
structure on Vergilius because as you can

60
00:02:21,660 --> 00:02:24,900
see most of what we passed to the

61
00:02:23,280 --> 00:02:26,879
function in userland isn't that

62
00:02:24,900 --> 00:02:29,099
interesting basically most of the fields

63
00:02:26,879 --> 00:02:31,800
in the KTRANSACTION structure in kernel

64
00:02:29,099 --> 00:02:33,540
aren't that interesting first either but

65
00:02:31,800 --> 00:02:35,940
if you look at it there are a few things

66
00:02:33,540 --> 00:02:38,580
that stand out there is a cookie which

67
00:02:35,940 --> 00:02:40,800
seems to be associated with all KTM

68
00:02:38,580 --> 00:02:43,019
structures basically they each have

69
00:02:40,800 --> 00:02:45,360
their own little cookie value which is

70
00:02:43,019 --> 00:02:47,459
unique to the type so all KTRANSACTION

71
00:02:45,360 --> 00:02:50,220
structures will have the same cookie

72
00:02:47,459 --> 00:02:52,739
value and then all KRESOURCEMANAGER

73
00:02:50,220 --> 00:02:54,840
structures will have the same cookie

74
00:02:52,739 --> 00:02:57,300
value as well and this is useful when

75
00:02:54,840 --> 00:03:00,540
you are digging around memory in WinDbg

76
00:02:57,300 --> 00:03:02,220
so they all have a mutex associated with

77
00:03:00,540 --> 00:03:04,080
them which gives you an idea about the

78
00:03:02,220 --> 00:03:06,060
type of logic that might be used to

79
00:03:04,080 --> 00:03:09,180
accessing them so you know the fact that

80
00:03:06,060 --> 00:03:11,340
the mutex exists implies there is going

81
00:03:09,180 --> 00:03:13,200
to be potentially multiple threats

82
00:03:11,340 --> 00:03:16,140
trying to manipulate this type of

83
00:03:13,200 --> 00:03:19,080
structures which can hint to the type of

84
00:03:16,140 --> 00:03:21,000
bugs you might be finding too and again

85
00:03:19,080 --> 00:03:23,400
this is a scenario where the structure

86
00:03:21,000 --> 00:03:26,040
is quite big at first it seems

87
00:03:23,400 --> 00:03:28,680
overwhelming but we know that structure

88
00:03:26,040 --> 00:03:31,140
exists and it is documented in the

89
00:03:28,680 --> 00:03:33,120
symbols there are a few things in it

90
00:03:31,140 --> 00:03:36,120
that are interesting but we don't need

91
00:03:33,120 --> 00:03:39,360
to go through it all and look at exactly

92
00:03:36,120 --> 00:03:42,000
what our every field is about we just

93
00:03:39,360 --> 00:03:44,459
know that the transaction exists we know

94
00:03:42,000 --> 00:03:46,140
that we are trying to reverse that big KTM

95
00:03:44,459 --> 00:03:48,299
function that we don't know much about

96
00:03:46,140 --> 00:03:50,099
at the moment and we can assume that

97
00:03:48,299 --> 00:03:52,500
maybe it will end up dealing with a

98
00:03:50,099 --> 00:03:54,840
transaction at some point or some other

99
00:03:52,500 --> 00:03:56,760
structures that it is referencing so we

100
00:03:54,840 --> 00:04:00,000
keep that in mind another interesting

101
00:03:56,760 --> 00:04:02,879
field is the linked list of enlistments

102
00:04:00,000 --> 00:04:04,920
which is named an enlistment head and

103
00:04:02,879 --> 00:04:07,379
basically it's a linked list of

104
00:04:04,920 --> 00:04:09,180
enlistments all associated with that

105
00:04:07,379 --> 00:04:10,799
transaction which is potentially

106
00:04:09,180 --> 00:04:13,920
interesting from the reversing

107
00:04:10,799 --> 00:04:16,079
perspective if we see it Loops over the

108
00:04:13,920 --> 00:04:18,959
elements of that list so just in the

109
00:04:16,079 --> 00:04:21,180
process of reversing and debugging we

110
00:04:18,959 --> 00:04:23,100
found all the cookies value associated

111
00:04:21,180 --> 00:04:25,080
with all the different structures types

112
00:04:23,100 --> 00:04:28,259
and so we document them here this is

113
00:04:25,080 --> 00:04:31,080
mostly useless if you are just trying to

114
00:04:28,259 --> 00:04:34,620
debug but it can be useful when you're

115
00:04:31,080 --> 00:04:37,380
trying to analyze raw memory from the

116
00:04:34,620 --> 00:04:40,199
heap and you quickly want to see where

117
00:04:37,380 --> 00:04:42,120
is the KTRANSACTION in memory or other

118
00:04:40,199 --> 00:04:44,580
structure because you are not exactly

119
00:04:42,120 --> 00:04:46,680
sure at what offset it starts at the

120
00:04:44,580 --> 00:04:48,860
moment you can quickly see the cookie

121
00:04:46,680 --> 00:04:52,979
because they use this

122
00:04:48,860 --> 00:04:55,440
b00b000 something and so from memory you

123
00:04:52,979 --> 00:04:59,580
can then correlate to okay the cookies

124
00:04:55,440 --> 00:05:00,540
at offsets whatever and I see this b00

125
00:04:59,580 --> 00:05:03,780
b00

126
00:05:00,540 --> 00:05:05,520
01 cookie and so I can deduce

127
00:05:03,780 --> 00:05:08,220
where the start of the structure is

128
00:05:05,520 --> 00:05:11,460
another thing is all the KTM structures

129
00:05:08,220 --> 00:05:13,259
have their own pool tag so this is

130
00:05:11,460 --> 00:05:16,919
useful when you're dumping things like

131
00:05:13,259 --> 00:05:19,440
with the !pool command in WinDbg to

132
00:05:16,919 --> 00:05:24,240
see what is on that actual pool for

133
00:05:19,440 --> 00:05:26,520
instance if you see a TmEn pool tag you

134
00:05:24,240 --> 00:05:28,500
know you can print the actual object as

135
00:05:26,520 --> 00:05:30,600
a KENLISTMENT structure to see the

136
00:05:28,500 --> 00:05:32,699
contents of that object so there is the

137
00:05:30,600 --> 00:05:34,320
transaction manager it is usually the

138
00:05:32,699 --> 00:05:36,600
first thing you have to create at all

139
00:05:34,320 --> 00:05:38,460
even before the transaction because

140
00:05:36,600 --> 00:05:42,240
there has to be something to manage the

141
00:05:38,460 --> 00:05:44,280
transaction but while studying KTM you

142
00:05:42,240 --> 00:05:46,560
don't necessarily know that this is the

143
00:05:44,280 --> 00:05:48,300
first thing you have to create so you

144
00:05:46,560 --> 00:05:51,300
would basically have to look at existing

145
00:05:48,300 --> 00:05:54,180
functions and experiment to figure that

146
00:05:51,300 --> 00:05:56,460
out so in the kernel it will create a

147
00:05:54,180 --> 00:05:57,900
specific KTM structure which is

148
00:05:56,460 --> 00:06:00,240
basically the transaction manager

149
00:05:57,900 --> 00:06:02,940
structure and it is allocated on the

150
00:06:00,240 --> 00:06:05,460
non-page pool and we know its tag is

151
00:06:02,940 --> 00:06:07,979
TmTm there are a few interesting flags

152
00:06:05,460 --> 00:06:10,680
the most interesting one is probably the

153
00:06:07,979 --> 00:06:12,960
flag that dictates whether or not the

154
00:06:10,680 --> 00:06:14,580
transaction manager is volatile so there

155
00:06:12,960 --> 00:06:17,100
is something that is not super well

156
00:06:14,580 --> 00:06:19,199
documented on the msdn if I recall

157
00:06:17,100 --> 00:06:21,479
correctly and it could take you quite a

158
00:06:19,199 --> 00:06:23,580
while to realize that the volatile flag

159
00:06:21,479 --> 00:06:26,460
could be useful because you could create

160
00:06:23,580 --> 00:06:28,560
transaction managers you could do all

161
00:06:26,460 --> 00:06:30,240
the stuff you want with them and so you

162
00:06:28,560 --> 00:06:32,100
think the volatile flag doesn't matter

163
00:06:30,240 --> 00:06:35,280
and yeah so basically normally by

164
00:06:32,100 --> 00:06:38,340
default a transaction manager is durable

165
00:06:35,280 --> 00:06:40,620
which means it has a log that lets you

166
00:06:38,340 --> 00:06:42,419
actually see the activity and the

167
00:06:40,620 --> 00:06:45,000
results of what is going on with your

168
00:06:42,419 --> 00:06:47,340
transactions but then later when you are

169
00:06:45,000 --> 00:06:50,340
writing the exploit and triggering a

170
00:06:47,340 --> 00:06:52,199
whole ton of functionality in KTM what

171
00:06:50,340 --> 00:06:55,319
you would realize is that a transaction

172
00:06:52,199 --> 00:06:57,539
manager has a log associated with it and

173
00:06:55,319 --> 00:07:00,840
all of the operations are being written

174
00:06:57,539 --> 00:07:04,380
to the log file and eventually it runs

175
00:07:00,840 --> 00:07:06,240
out of space and if that happens you

176
00:07:04,380 --> 00:07:09,600
won't let you do any operations anymore

177
00:07:06,240 --> 00:07:11,880
because like 10 000 operations later KTM

178
00:07:09,600 --> 00:07:13,500
says that was too many and so the

179
00:07:11,880 --> 00:07:15,960
solution is to make the transaction

180
00:07:13,500 --> 00:07:18,539
manager volatile which means there is no

181
00:07:15,960 --> 00:07:20,400
log created and this is just again an

182
00:07:18,539 --> 00:07:22,680
example of experimentation that is

183
00:07:20,400 --> 00:07:24,780
required it ends up being related to

184
00:07:22,680 --> 00:07:27,240
exploitation but when you are reading

185
00:07:24,780 --> 00:07:29,400
the documentation and experimenting you

186
00:07:27,240 --> 00:07:30,900
don't know it will be useful yet you

187
00:07:29,400 --> 00:07:33,060
would have to play with different flags

188
00:07:30,900 --> 00:07:35,520
and then end up realizing it is actually

189
00:07:33,060 --> 00:07:38,220
useful for exploitation so again we see

190
00:07:35,520 --> 00:07:41,039
the KTM structure has a cookie value and

191
00:07:38,220 --> 00:07:43,020
it has a mutex we also start to realize

192
00:07:41,039 --> 00:07:45,000
that most of these structures have a

193
00:07:43,020 --> 00:07:47,280
state field and the state usually

194
00:07:45,000 --> 00:07:50,599
corresponds to an enum that is publicly

195
00:07:47,280 --> 00:07:54,479
documented also it has a flags field

196
00:07:50,599 --> 00:07:56,400
which basically indicates the type and

197
00:07:54,479 --> 00:07:59,039
state of the structure but that one is

198
00:07:56,400 --> 00:08:00,800
undocumented so you end up needing to

199
00:07:59,039 --> 00:08:03,479
know what certain flags are eventually

200
00:08:00,800 --> 00:08:06,360
which will lead to a lot of reversing

201
00:08:03,479 --> 00:08:09,060
for instance using function names from

202
00:08:06,360 --> 00:08:12,060
Microsoft symbols and seeing where the

203
00:08:09,060 --> 00:08:14,340
undocumented flags are set changed or

204
00:08:12,060 --> 00:08:16,380
checked to do certain things you can

205
00:08:14,340 --> 00:08:20,280
figure out what the undocumented flags

206
00:08:16,380 --> 00:08:22,620
might be you have the TmRm pointer which

207
00:08:20,280 --> 00:08:24,660
is a linked list of associated resource

208
00:08:22,620 --> 00:08:26,879
managers and then you have other fields

209
00:08:24,660 --> 00:08:29,580
that don't matter much for instance

210
00:08:26,879 --> 00:08:31,919
there is a file object that ends up

211
00:08:29,580 --> 00:08:33,839
pointing to the log file but we don't

212
00:08:31,919 --> 00:08:36,180
end up using a log file anyway because

213
00:08:33,839 --> 00:08:38,459
we use a volatile transaction manager so

214
00:08:36,180 --> 00:08:41,039
it is things you recognize because you

215
00:08:38,459 --> 00:08:43,860
know file object is a type of object

216
00:08:41,039 --> 00:08:45,660
managed by the object manager so it

217
00:08:43,860 --> 00:08:48,240
might be interesting but it turns out

218
00:08:45,660 --> 00:08:50,519
not to be the case for us this is just

219
00:08:48,240 --> 00:08:52,980
an example of the undocumented flags

220
00:08:50,519 --> 00:08:55,140
that at first we had no idea what they

221
00:08:52,980 --> 00:08:57,420
were but for instance we found that if

222
00:08:55,140 --> 00:08:59,220
we pass from user land the flag that

223
00:08:57,420 --> 00:09:02,760
says the transaction member should be

224
00:08:59,220 --> 00:09:06,000
volatile the kernel set value 1 in the

225
00:09:02,760 --> 00:09:08,760
flags so we named it KTM flag volatile

226
00:09:06,000 --> 00:09:11,339
to help in reversing more functions and

227
00:09:08,760 --> 00:09:13,500
then also by reversing and figuring out

228
00:09:11,339 --> 00:09:16,320
errors returned and the logic of

229
00:09:13,500 --> 00:09:19,019
functions we can figure out most of

230
00:09:16,320 --> 00:09:21,480
these flags at the end most of all your

231
00:09:19,019 --> 00:09:23,040
other flags aside from the volatile one

232
00:09:21,480 --> 00:09:24,959
are probably not interesting for

233
00:09:23,040 --> 00:09:27,000
exploitation but you don't really know

234
00:09:24,959 --> 00:09:29,160
in advance what you're trying to

235
00:09:27,000 --> 00:09:31,200
understand KTM internals and generally

236
00:09:29,160 --> 00:09:33,180
they still help with reversing to

237
00:09:31,200 --> 00:09:35,100
understand the logic of functions and

238
00:09:33,180 --> 00:09:37,200
know if the functions are you are

239
00:09:35,100 --> 00:09:40,320
actually reversing actually interesting

240
00:09:37,200 --> 00:09:42,440
versus not again the API you can use

241
00:09:40,320 --> 00:09:45,600
from userland and is quite straightforward

242
00:09:42,440 --> 00:09:48,899
and the only arguments that are really

243
00:09:45,600 --> 00:09:51,540
interesting are the flag that says it is

244
00:09:48,899 --> 00:09:53,760
volatile and the log file name being no

245
00:09:51,540 --> 00:09:55,380
since we don't want to create a log file

246
00:09:53,760 --> 00:09:57,779
now let's talk about the resource

247
00:09:55,380 --> 00:10:00,360
manager there are always has to be some

248
00:09:57,779 --> 00:10:02,100
resource like files or registry keys or

249
00:10:00,360 --> 00:10:03,839
something that is is going to be part of

250
00:10:02,100 --> 00:10:05,880
a transaction but the whole point of a

251
00:10:03,839 --> 00:10:07,920
transaction is to do something automatic

252
00:10:05,880 --> 00:10:09,899
atomically so there is at least one

253
00:10:07,920 --> 00:10:12,120
resource manager associated with the

254
00:10:09,899 --> 00:10:14,399
transaction manager for a transaction to

255
00:10:12,120 --> 00:10:16,440
take place so you always create the

256
00:10:14,399 --> 00:10:18,240
transaction manager first and then

257
00:10:16,440 --> 00:10:19,920
immediately after you create the

258
00:10:18,240 --> 00:10:22,260
resource manager because one of the

259
00:10:19,920 --> 00:10:24,540
arguments required to create the

260
00:10:22,260 --> 00:10:26,519
resource manager is actually a handle to

261
00:10:24,540 --> 00:10:28,440
the transaction manager you can notice

262
00:10:26,519 --> 00:10:31,320
there is the optional description

263
00:10:28,440 --> 00:10:33,660
argument which at first you might not be

264
00:10:31,320 --> 00:10:35,459
interested in but it turns out it can be

265
00:10:33,660 --> 00:10:37,019
very useful for exploitation in the end

266
00:10:35,459 --> 00:10:39,480
which is again a reason why playing

267
00:10:37,019 --> 00:10:41,040
around with APIs and testing what's

268
00:10:39,480 --> 00:10:43,380
setting a description field from

269
00:10:41,040 --> 00:10:45,540
userland looks like in ketnel memory is

270
00:10:43,380 --> 00:10:47,640
actually useful to see how you could use

271
00:10:45,540 --> 00:10:49,560
it during the exploitation phase and

272
00:10:47,640 --> 00:10:51,300
sometimes when you're playing around it

273
00:10:49,560 --> 00:10:53,339
seems you are wasting time but little

274
00:10:51,300 --> 00:10:56,160
things like that can actually pay off

275
00:10:53,339 --> 00:10:58,200
when it reminds you that it exists later

276
00:10:56,160 --> 00:11:00,300
when looking for Expedition primitives

277
00:10:58,200 --> 00:11:02,339
so the current structure for resource

278
00:11:00,300 --> 00:11:03,720
manager is called or KRESOURCEMANAGER

279
00:11:02,339 --> 00:11:05,880
with actually a lot of interesting

280
00:11:03,720 --> 00:11:07,560
Fields here they are the standard fields

281
00:11:05,880 --> 00:11:10,560
we mentioned already for other

282
00:11:07,560 --> 00:11:12,480
structures like cookie state flags and

283
00:11:10,560 --> 00:11:15,120
mutex there are two other fields which

284
00:11:12,480 --> 00:11:17,040
are the notification queue and the

285
00:11:15,120 --> 00:11:19,440
notification mutex which are also

286
00:11:17,040 --> 00:11:21,240
interesting you don't know why it is

287
00:11:19,440 --> 00:11:23,820
interesting at first but basically by

288
00:11:21,240 --> 00:11:25,920
reversing KTM you can find out that when

289
00:11:23,820 --> 00:11:28,500
certain activity related to a resource

290
00:11:25,920 --> 00:11:30,899
manager happens that information can be

291
00:11:28,500 --> 00:11:32,339
placed into a notification queue so that

292
00:11:30,899 --> 00:11:35,339
from userland you can query the

293
00:11:32,339 --> 00:11:37,019
resource manager and say hey what's been

294
00:11:35,339 --> 00:11:39,959
going on

295
00:11:37,019 --> 00:11:42,779
and get a list of all the activity and

296
00:11:39,959 --> 00:11:45,600
that can be really useful for inspecting

297
00:11:42,779 --> 00:11:47,040
the state of the kernel from userland

298
00:11:45,600 --> 00:11:48,959
and then there are some other things

299
00:11:47,040 --> 00:11:50,820
that are sort of vaguely interesting

300
00:11:48,959 --> 00:11:53,279
from exploitation perspective like a

301
00:11:50,820 --> 00:11:55,620
function pointer name notification routine

302
00:11:53,279 --> 00:11:58,079
that's actually interesting in general

303
00:11:55,620 --> 00:12:00,000
because if we could leak the address of

304
00:11:58,079 --> 00:12:02,100
a KRESOURCEMANAGER object and we

305
00:12:00,000 --> 00:12:05,100
had a write primitive and it function

306
00:12:02,100 --> 00:12:06,839
pointer was called somehow then we we

307
00:12:05,100 --> 00:12:09,420
know we could potentially get code

308
00:12:06,839 --> 00:12:10,920
execution so lots of things to track and

309
00:12:09,420 --> 00:12:13,440
think about as you poke around

310
00:12:10,920 --> 00:12:15,240
structures another field is the

311
00:12:13,440 --> 00:12:17,339
enlistment head pointer which is

312
00:12:15,240 --> 00:12:20,279
basically a linked list of associated

313
00:12:17,339 --> 00:12:21,779
enlistments and again as soon as you

314
00:12:20,279 --> 00:12:23,640
start playing around with the resource

315
00:12:21,779 --> 00:12:25,560
manager APIs and you look at the

316
00:12:23,640 --> 00:12:27,779
structure if you remember the function

317
00:12:25,560 --> 00:12:30,720
that we want to start reversing relative

318
00:12:27,779 --> 00:12:33,180
to the back which is the

319
00:12:30,720 --> 00:12:34,980
TmRecoverResourceManagerExt so all of a sudden

320
00:12:33,180 --> 00:12:37,500
the KRESOURCEMANAGER structure

321
00:12:34,980 --> 00:12:40,500
is really relevant because we know the

322
00:12:37,500 --> 00:12:42,959
TmRecoverResourceManagerExt function

323
00:12:40,500 --> 00:12:45,240
is referencing the structure based of

324
00:12:42,959 --> 00:12:47,279
the names both dealing with resource

325
00:12:45,240 --> 00:12:49,560
manager and so all of the fields from

326
00:12:47,279 --> 00:12:51,060
this structure are immediately just more

327
00:12:49,560 --> 00:12:52,800
interesting in general because we know

328
00:12:51,060 --> 00:12:55,260
we may have to look at them again in the

329
00:12:52,800 --> 00:12:57,000
future again these are undocumented

330
00:12:55,260 --> 00:12:58,980
flags that we were able to work out for

331
00:12:57,000 --> 00:13:00,839
the resource matter in the end we

332
00:12:58,980 --> 00:13:03,000
reversed and documented almost all of

333
00:13:00,839 --> 00:13:04,860
the flags if I record correctly so we're

334
00:13:03,000 --> 00:13:07,200
able to work out a lot of the logic

335
00:13:04,860 --> 00:13:09,060
related to these flags and so finally

336
00:13:07,200 --> 00:13:11,579
let's talk about enlistments than

337
00:13:09,060 --> 00:13:14,880
themselves again the create enlistment

338
00:13:11,579 --> 00:13:16,500
API is used to create an instrument as

339
00:13:14,880 --> 00:13:18,120
you would imagine there needs to be an

340
00:13:16,500 --> 00:13:20,220
Associated resource manager created

341
00:13:18,120 --> 00:13:22,079
already so you need to pass the resource

342
00:13:20,220 --> 00:13:24,600
manager handle also you have to be

343
00:13:22,079 --> 00:13:26,940
enlisting in some transaction so you

344
00:13:24,600 --> 00:13:28,320
need to pass the transaction handle and

345
00:13:26,940 --> 00:13:30,180
if I record correctly from the

346
00:13:28,320 --> 00:13:31,980
documentation you can see that typically

347
00:13:30,180 --> 00:13:33,899
a transaction will have multiple

348
00:13:31,980 --> 00:13:36,540
enlistments associated with it because

349
00:13:33,899 --> 00:13:38,459
yeah let's say we want to be creating a

350
00:13:36,540 --> 00:13:40,380
transaction with only one enlistment

351
00:13:38,459 --> 00:13:42,300
because maybe there is only one piece of

352
00:13:40,380 --> 00:13:44,639
work to be done but then that piece of

353
00:13:42,300 --> 00:13:46,320
work would be Atomic anyway so there is

354
00:13:44,639 --> 00:13:48,000
no need to have a transaction so

355
00:13:46,320 --> 00:13:49,980
generally the idea is that there is a

356
00:13:48,000 --> 00:13:52,380
lot of work being done each with an

357
00:13:49,980 --> 00:13:54,600
Associated enlistment and they all have

358
00:13:52,380 --> 00:13:56,459
to be synchronized with each other's for

359
00:13:54,600 --> 00:13:58,800
the transaction to actually happen the

360
00:13:56,459 --> 00:14:00,600
kernel structure for enlistment is

361
00:13:58,800 --> 00:14:02,880
called KENLISTMENTs we see the

362
00:14:00,600 --> 00:14:05,399
KENLISTMENTs has a GUID associated with

363
00:14:02,880 --> 00:14:07,680
it which is a randomly generated for

364
00:14:05,399 --> 00:14:09,240
each enlistment being created they

365
00:14:07,680 --> 00:14:11,579
are the standard fields we mentioned

366
00:14:09,240 --> 00:14:14,639
already like the cookie state flags and

367
00:14:11,579 --> 00:14:17,220
mutex there are two linked lists the

368
00:14:14,639 --> 00:14:19,139
first one is all the enlistment that are

369
00:14:17,220 --> 00:14:21,660
associated with a given resource manager

370
00:14:19,139 --> 00:14:24,959
some of those enlistments might be

371
00:14:21,660 --> 00:14:27,120
associated with transaction A and others

372
00:14:24,959 --> 00:14:29,880
might be associated with transaction B

373
00:14:27,120 --> 00:14:32,339
but because we know the transaction also

374
00:14:29,880 --> 00:14:34,500
has its own linked list of all the

375
00:14:32,339 --> 00:14:36,480
enlistments only associated with that

376
00:14:34,500 --> 00:14:38,880
transaction it comes as no surprise that

377
00:14:36,480 --> 00:14:40,560
there is a second link list of all the

378
00:14:38,880 --> 00:14:43,380
enlistments associated with the same

379
00:14:40,560 --> 00:14:45,540
transaction another field is a pointer

380
00:14:43,380 --> 00:14:48,000
to the resource manager structure

381
00:14:45,540 --> 00:14:49,800
associated with the enlistments and also

382
00:14:48,000 --> 00:14:51,240
another field is a pointer to the

383
00:14:49,800 --> 00:14:53,760
transaction structure that the

384
00:14:51,240 --> 00:14:56,279
enlistment is actually doing work for

385
00:14:53,760 --> 00:14:58,620
there is also a notification mask which

386
00:14:56,279 --> 00:15:00,899
basically dictates what notifications

387
00:14:58,620 --> 00:15:03,360
should be queued to the resource manager

388
00:15:00,899 --> 00:15:05,579
related to these enlistments and these

389
00:15:03,360 --> 00:15:08,040
fields will be especially useful when we

390
00:15:05,579 --> 00:15:09,959
get to reversing the

391
00:15:08,040 --> 00:15:12,240
TmRecoverResourceManagerExt function but again

392
00:15:09,959 --> 00:15:14,459
you can't know in advance What fields

393
00:15:12,240 --> 00:15:17,459
will or won't be interesting for sure

394
00:15:14,459 --> 00:15:19,440
again we list undocumented flags we

395
00:15:17,459 --> 00:15:21,240
found related to enlistments a lot of

396
00:15:19,440 --> 00:15:23,100
these are not actually relevant for

397
00:15:21,240 --> 00:15:25,980
exploitation one of them is this

398
00:15:23,100 --> 00:15:27,779
Superior enlistmentsflag and the msdn

399
00:15:25,980 --> 00:15:29,279
documentation for the super

400
00:15:27,779 --> 00:15:31,260
enlistment is really hard to understand

401
00:15:29,279 --> 00:15:33,360
if you get started with KTM in general

402
00:15:31,260 --> 00:15:35,339
and I think I still don't understand how

403
00:15:33,360 --> 00:15:38,040
that works entirely and what they were

404
00:15:35,339 --> 00:15:40,380
going for it with it but when we found

405
00:15:38,040 --> 00:15:42,779
this Superior flag and so you have

406
00:15:40,380 --> 00:15:45,120
enlistment that are of the superior type

407
00:15:42,779 --> 00:15:46,920
which turns out to be not interesting

408
00:15:45,120 --> 00:15:49,320
for exploitation but you can actually

409
00:15:46,920 --> 00:15:51,899
abuse the fact that in general all or

410
00:15:49,320 --> 00:15:54,480
all enlistments are not Superior and so

411
00:15:51,899 --> 00:15:56,279
if somehow you set a superior flag in a

412
00:15:54,480 --> 00:15:58,920
given enlistment it can help with

413
00:15:56,279 --> 00:16:01,440
debugging because the kernel code that

414
00:15:58,920 --> 00:16:03,300
checks this flag in different places and

415
00:16:01,440 --> 00:16:05,820
so you would only hit certain portion of

416
00:16:03,300 --> 00:16:07,440
codes if that flag is set and so it can

417
00:16:05,820 --> 00:16:09,360
help with debugging in general so it's

418
00:16:07,440 --> 00:16:11,760
easier to debug the vulnerability and so

419
00:16:09,360 --> 00:16:14,339
it's useful to know it exists as it

420
00:16:11,760 --> 00:16:17,100
speeds up debugging enhance exploitation

421
00:16:14,339 --> 00:16:19,560
other flags that are interesting are

422
00:16:17,100 --> 00:16:21,180
this finalized flag basically when all

423
00:16:19,560 --> 00:16:22,860
of the workers stated within

424
00:16:21,180 --> 00:16:25,500
KENLISTMENT has actually been completed

425
00:16:22,860 --> 00:16:27,720
then nothing else will happen because it

426
00:16:25,500 --> 00:16:29,880
is kind of done in theory that and

427
00:16:27,720 --> 00:16:32,040
enlistments is sort of ready to be

428
00:16:29,880 --> 00:16:34,019
deleted because there is nothing else

429
00:16:32,040 --> 00:16:36,480
that can happen and so when it is

430
00:16:34,019 --> 00:16:39,000
prepared to be deleted it will set this

431
00:16:36,480 --> 00:16:40,800
flag to indicate it is finalized there

432
00:16:39,000 --> 00:16:44,160
is also another flag that is interesting

433
00:16:40,800 --> 00:16:46,320
which is the is notifiable one we saw

434
00:16:44,160 --> 00:16:48,240
earlier that the resource manager has a

435
00:16:46,320 --> 00:16:50,519
notification queue and by poking around

436
00:16:48,240 --> 00:16:53,759
it turns out that the when the

437
00:16:50,519 --> 00:16:55,740
KENLISTMENT has had some state changes as

438
00:16:53,759 --> 00:16:57,839
it associated with it it may be

439
00:16:55,740 --> 00:17:00,839
notifiable meaning that information

440
00:16:57,839 --> 00:17:02,880
about that KENLISTMENT status changes

441
00:17:00,839 --> 00:17:05,220
will be queued into the notification

442
00:17:02,880 --> 00:17:07,380
queue and if the KENLISTMENT is not

443
00:17:05,220 --> 00:17:10,439
notifiable it won't queue that information

444
00:17:07,380 --> 00:17:12,839
so again at first when you are reversing

445
00:17:10,439 --> 00:17:15,240
these types of flag none of these means

446
00:17:12,839 --> 00:17:17,939
anything because we don't really know

447
00:17:15,240 --> 00:17:20,400
any anything about it but it can seem a

448
00:17:17,939 --> 00:17:22,620
little bit weird but at the end a lot of

449
00:17:20,400 --> 00:17:24,839
it is quite useful so one of the things

450
00:17:22,620 --> 00:17:27,000
that will end up being relevant is how

451
00:17:24,839 --> 00:17:29,460
to actually finalize and free an

452
00:17:27,000 --> 00:17:31,440
enlistment because later once we diff

453
00:17:29,460 --> 00:17:32,820
better the vulnerable function and

454
00:17:31,440 --> 00:17:34,440
document the code to understand the

455
00:17:32,820 --> 00:17:36,660
vulnerability we'll see that it

456
00:17:34,440 --> 00:17:39,000
basically involves an enlistment that

457
00:17:36,660 --> 00:17:41,280
that becomes free and it is a use after

458
00:17:39,000 --> 00:17:43,440
free bug so there is still a reference

459
00:17:41,280 --> 00:17:46,020
to the free enlistments somewhere and

460
00:17:43,440 --> 00:17:48,000
the enlistment is reused resulting in a

461
00:17:46,020 --> 00:17:51,179
user for free and so we found that there

462
00:17:48,000 --> 00:17:53,580
is this function called TmpFinalizeEnlistment

463
00:17:51,179 --> 00:17:55,799
that typically lowers the

464
00:17:53,580 --> 00:17:57,840
reference count of the enlistment object

465
00:17:55,799 --> 00:17:59,820
which dictates whether or not the

466
00:17:57,840 --> 00:18:02,580
enlistment can be freed by the object

467
00:17:59,820 --> 00:18:05,580
manager if it reaches zero but then the

468
00:18:02,580 --> 00:18:07,559
question is when does TmpFinalizeEnlistment

469
00:18:05,580 --> 00:18:09,539
get called and it turns out

470
00:18:07,559 --> 00:18:11,700
enlistments have a series of States

471
00:18:09,539 --> 00:18:13,740
associated with them and all the

472
00:18:11,700 --> 00:18:15,660
enlistments as it is associated with the

473
00:18:13,740 --> 00:18:17,880
same transaction have to have

474
00:18:15,660 --> 00:18:19,679
corresponding States and so to ensure

475
00:18:17,880 --> 00:18:22,260
that the transaction is happening

476
00:18:19,679 --> 00:18:24,960
atomically all of the enlistments kind

477
00:18:22,260 --> 00:18:27,059
of work together and only when they all

478
00:18:24,960 --> 00:18:29,220
are synchronized on a specific state

479
00:18:27,059 --> 00:18:31,679
saying that part of their work is done

480
00:18:29,220 --> 00:18:33,960
they all transition to the next state

481
00:18:31,679 --> 00:18:36,059
and move forward and so the final States

482
00:18:33,960 --> 00:18:38,039
Of Enlistment is when they become

483
00:18:36,059 --> 00:18:40,380
committed with which means the final work

484
00:18:38,039 --> 00:18:42,480
has been done and so when we found that

485
00:18:40,380 --> 00:18:44,760
there is a use an API called commit

486
00:18:42,480 --> 00:18:46,799
complete which can trigger that state's

487
00:18:44,760 --> 00:18:49,260
transition it was a lot easier to

488
00:18:46,799 --> 00:18:51,780
trigger that state changes and so

489
00:18:49,260 --> 00:18:54,720
eventually any other references that are

490
00:18:51,780 --> 00:18:57,000
held into that enlistment once they are

491
00:18:54,720 --> 00:18:59,580
all dropped the enlistment object which

492
00:18:57,000 --> 00:19:01,799
will actually be freed by the object

493
00:18:59,580 --> 00:19:03,360
manager however in general since you

494
00:19:01,799 --> 00:19:05,640
have been using the enlistment from

495
00:19:03,360 --> 00:19:07,260
userland and have created it with the

496
00:19:05,640 --> 00:19:09,600
create enlistment function in the first

497
00:19:07,260 --> 00:19:12,000
place you will still have a handle to it

498
00:19:09,600 --> 00:19:14,880
from userland and so the final reference

499
00:19:12,000 --> 00:19:17,160
is removed by calling close handle and

500
00:19:14,880 --> 00:19:18,900
finally this frees the enlistment so

501
00:19:17,160 --> 00:19:21,240
this is the same diagram we saw earlier

502
00:19:18,900 --> 00:19:23,220
but this time we have a little bit more

503
00:19:21,240 --> 00:19:24,539
understanding of relations between the

504
00:19:23,220 --> 00:19:26,340
different structures and so the

505
00:19:24,539 --> 00:19:28,620
relationship is a little bit more

506
00:19:26,340 --> 00:19:31,080
meaningful the linked lists are a little

507
00:19:28,620 --> 00:19:33,480
bit more meaningful as to what they are

508
00:19:31,080 --> 00:19:35,340
pointing to and why they are associated

509
00:19:33,480 --> 00:19:37,140
with the other structures and so when

510
00:19:35,340 --> 00:19:39,720
you think about KTM
in the kernel in general

511
00:19:37,140 --> 00:19:41,820
our mental model is mostly just thinking

512
00:19:39,720 --> 00:19:45,740
about this type of relationship of

513
00:19:41,820 --> 00:19:45,740
structures more than anything else

