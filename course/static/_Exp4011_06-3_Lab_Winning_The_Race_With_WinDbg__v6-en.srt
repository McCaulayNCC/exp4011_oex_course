1
00:00:02,940 --> 00:00:08,400
hi everyone the purpose of this video

2
00:00:05,520 --> 00:00:11,219
is to show you how we can win the race

3
00:00:08,400 --> 00:00:14,700
condition by using the WinDbg debugger

4
00:00:11,219 --> 00:00:17,279
to help us win that race the first step

5
00:00:14,700 --> 00:00:19,859
to do so is to enable the Microsoft

6
00:00:17,279 --> 00:00:22,619
driver verifier functionality in the

7
00:00:19,859 --> 00:00:24,779
Windows kernel in order to detect when

8
00:00:22,619 --> 00:00:27,539
invalid memory is accessed we're going

9
00:00:24,779 --> 00:00:29,939
to use the WinDbg debugger to patch the

10
00:00:27,539 --> 00:00:32,040
memory in order to block the recovery

11
00:00:29,939 --> 00:00:34,500
thread at a certain time so we can

12
00:00:32,040 --> 00:00:36,360
effectively win the race taking as much

13
00:00:34,500 --> 00:00:38,579
time as we need to confirm the mental

14
00:00:36,360 --> 00:00:40,680
model and finally once the setup is done

15
00:00:38,579 --> 00:00:43,020
we're going to effectively run the

16
00:00:40,680 --> 00:00:45,480
binary and show that we can win the race

17
00:00:43,020 --> 00:00:48,059
in order to confirm our understanding of

18
00:00:45,480 --> 00:00:50,039
the vulnerability okay let's get started

19
00:00:48,059 --> 00:00:51,840
so we'll first go over all the

20
00:00:50,039 --> 00:00:54,899
instructions and then we will show them

21
00:00:51,840 --> 00:00:57,960
uh in the virtual machines the lab we

22
00:00:54,899 --> 00:00:59,940
want to focus on now is in the part 2

23
00:00:57,960 --> 00:01:02,219
archive so we're going to need to

24
00:00:59,940 --> 00:01:05,220
extract it and because the labs are

25
00:01:02,219 --> 00:01:07,560
handled with CMake we need to rerun the

26
00:01:05,220 --> 00:01:09,780
build.bat script in order to generate

27
00:01:07,560 --> 00:01:11,820
the Visual Studio addition files related

28
00:01:09,780 --> 00:01:14,700
to the new Labs you should have done it

29
00:01:11,820 --> 00:01:17,040
already but if not I advise you do a

30
00:01:14,700 --> 00:01:19,020
snapshot of your target VM before any

31
00:01:17,040 --> 00:01:21,540
more modification it is because we are

32
00:01:19,020 --> 00:01:24,000
going to enable verifier in order to

33
00:01:21,540 --> 00:01:26,340
help detecting invalid memory accesses

34
00:01:24,000 --> 00:01:29,400
in this lab that we will want to roll

35
00:01:26,340 --> 00:01:31,380
back to a VM state without verifier for

36
00:01:29,400 --> 00:01:33,840
all the rest of the training and so to

37
00:01:31,380 --> 00:01:36,060
enable verifier you can either do it

38
00:01:33,840 --> 00:01:38,280
from the command line or using the GUI

39
00:01:36,060 --> 00:01:40,979
but basically the idea is that you

40
00:01:38,280 --> 00:01:43,460
modify the verifier configuration and

41
00:01:40,979 --> 00:01:46,200
enable certain flags associated with the

42
00:01:43,460 --> 00:01:48,659
tm.sys kernel driver to indicate that

43
00:01:46,200 --> 00:01:51,240
you want to enable tracking certain pool

44
00:01:48,659 --> 00:01:53,520
allocations since the kernel pool is the

45
00:01:51,240 --> 00:01:56,220
name of the Windows kernel heap it will

46
00:01:53,520 --> 00:01:59,399
allow tracking pool allocations and

47
00:01:56,220 --> 00:02:02,340
frees and it will help detecting when a

48
00:01:59,399 --> 00:02:04,500
freed chunk is accessed whereas it

49
00:02:02,340 --> 00:02:07,320
should not be the case then you need to

50
00:02:04,500 --> 00:02:09,899
reboot the target VM where verifier is

51
00:02:07,320 --> 00:02:12,720
now enabled and after it has rebooted

52
00:02:09,899 --> 00:02:15,780
you can confirm that verifier is enabled

53
00:02:12,720 --> 00:02:18,720
on the tm.sys driver either using

54
00:02:15,780 --> 00:02:21,120
verifier /querysettings or

55
00:02:18,720 --> 00:02:23,400
directly in WinDbg because the output

56
00:02:21,120 --> 00:02:25,980
you'll see a specific line mentioning

57
00:02:23,400 --> 00:02:28,260
the verifier when you'll try to trigger

58
00:02:25,980 --> 00:02:30,660
win the race condition and you want to

59
00:02:28,260 --> 00:02:34,200
see the use after free you can use the

60
00:02:30,660 --> 00:02:37,080
!verifier command and specify the 80

61
00:02:34,200 --> 00:02:39,239
hex flag and the pool address you want

62
00:02:37,080 --> 00:02:41,580
information for and this will basically

63
00:02:39,239 --> 00:02:45,060
dump information about when this address

64
00:02:41,580 --> 00:02:47,220
was allocated and freed by the tm.sys

65
00:02:45,060 --> 00:02:49,200
driver and you'll get a call stack of

66
00:02:47,220 --> 00:02:51,360
the operation which is invaluable

67
00:02:49,200 --> 00:02:53,519
information when trying to track down

68
00:02:51,360 --> 00:02:55,620
use-after-free vulnerabilities it will

69
00:02:53,519 --> 00:02:57,840
tell you that that specific address was

70
00:02:55,620 --> 00:03:00,060
freed by other code and if you see in

71
00:02:57,840 --> 00:03:02,400
the debugger that it tries to reuse that

72
00:03:00,060 --> 00:03:04,739
memory you are basically watching as you

73
00:03:02,400 --> 00:03:07,140
use-after-free and usually here as well

74
00:03:04,739 --> 00:03:09,540
we want to take a snapshot of the VM

75
00:03:07,140 --> 00:03:11,640
booted with verifier enabled this is

76
00:03:09,540 --> 00:03:14,760
because the whole point of enabling

77
00:03:11,640 --> 00:03:16,800
verifier is to detect a crash so the VM

78
00:03:14,760 --> 00:03:19,200
is likely to crash many times while

79
00:03:16,800 --> 00:03:21,420
testing it so once we have made a

80
00:03:19,200 --> 00:03:24,239
snapshot of it we will be able to just

81
00:03:21,420 --> 00:03:26,879
restore that snapshot without having to

82
00:03:24,239 --> 00:03:29,280
reboot the VM and it will avoid having

83
00:03:26,879 --> 00:03:31,860
to reattach the debugger and so on we

84
00:03:29,280 --> 00:03:34,560
provide a helper script called HelpWinTheRace.js

85
00:03:31,860 --> 00:03:36,540
which basically allows to

86
00:03:34,560 --> 00:03:38,879
patch some code it will basically patch

87
00:03:36,540 --> 00:03:41,879
the call KeWaitForSingleObject

88
00:03:38,879 --> 00:03:44,099
instruction that can help us win the

89
00:03:41,879 --> 00:03:46,860
race remember in the vulnerable function

90
00:03:44,099 --> 00:03:48,900
there is a function call to set a

91
00:03:46,860 --> 00:03:51,420
notification then this function

92
00:03:48,900 --> 00:03:53,780
returns and there is a check on the

93
00:03:51,420 --> 00:03:57,180
KENLISTMENT flag to check if it is

94
00:03:53,780 --> 00:03:59,700
finalized and after that it dereferences

95
00:03:57,180 --> 00:04:02,159
the KENLISTMENT reference count and it

96
00:03:59,700 --> 00:04:04,500
calls KeWaitForSingleObject on the

97
00:04:02,159 --> 00:04:06,780
resource manager mutex and that's on

98
00:04:04,500 --> 00:04:09,299
this KeWaitForSingleObject that we

99
00:04:06,780 --> 00:04:11,280
want to patch memory and so the idea is

100
00:04:09,299 --> 00:04:13,500
that we passed that KeWaitForSingleObject

101
00:04:11,280 --> 00:04:16,019
because we want to check

102
00:04:13,500 --> 00:04:19,260
the scenario where the send notification

103
00:04:16,019 --> 00:04:22,620
change the enlistment state but does not

104
00:04:19,260 --> 00:04:25,080
finalize it yet then the call returns

105
00:04:22,620 --> 00:04:27,660
and it checks the enlistment flags so

106
00:04:25,080 --> 00:04:30,960
the enlistment is not finalized yet then

107
00:04:27,660 --> 00:04:33,600
using our patch the recovery thread is

108
00:04:30,960 --> 00:04:35,820
stuck forever and so it gives us time to

109
00:04:33,600 --> 00:04:38,220
do certain things in user level such as

110
00:04:35,820 --> 00:04:40,620
committing the enlistment and closing

111
00:04:38,220 --> 00:04:42,840
the handle to the enlistment and so this

112
00:04:40,620 --> 00:04:45,660
will effectively trigger finalizing the

113
00:04:42,840 --> 00:04:48,300
enlistment however in kernel the check on

114
00:04:45,660 --> 00:04:50,820
the enlistment state was done already and

115
00:04:48,300 --> 00:04:53,639
the enlistment is assumed not finalized

116
00:04:50,820 --> 00:04:55,620
and so what we can do next is removing

117
00:04:53,639 --> 00:04:58,500
the patch and restoring the original

118
00:04:55,620 --> 00:05:00,360
call KeWaitForSingleObject code and

119
00:04:58,500 --> 00:05:02,460
so the recovery thread continues

120
00:05:00,360 --> 00:05:04,919
executing and we should be able to see

121
00:05:02,460 --> 00:05:07,139
the use-after-free at least that is the goal

122
00:05:04,919 --> 00:05:09,300
here and so to help patching and

123
00:05:07,139 --> 00:05:12,360
unpatching the code we use the WinDbg

124
00:05:09,300 --> 00:05:15,540
script written in JavaScript and so we

125
00:05:12,360 --> 00:05:17,699
use the .scriptload command to load

126
00:05:15,540 --> 00:05:19,800
that script in the first place and that

127
00:05:17,699 --> 00:05:23,280
will allow defining two new commands

128
00:05:19,800 --> 00:05:25,680
!patch and !unpatch and when

129
00:05:23,280 --> 00:05:28,320
we use the !patch command it

130
00:05:25,680 --> 00:05:30,539
replaces the call instruction with a

131
00:05:28,320 --> 00:05:32,880
forever jump instruction as we just

132
00:05:30,539 --> 00:05:35,940
detailed and when we use the !unpatch

133
00:05:32,880 --> 00:05:38,280
command it restores the call

134
00:05:35,940 --> 00:05:40,199
instruction bytes in order to solve this

135
00:05:38,280 --> 00:05:44,000
lab we want to make sure we use

136
00:05:40,199 --> 00:05:47,340
automation to load the dbg-prep.cmd

137
00:05:44,000 --> 00:05:50,039
script when we attach with

138
00:05:47,340 --> 00:05:52,919
WinDbg since it will allow loading the

139
00:05:50,039 --> 00:05:55,800
HelpWinTheRace.js script that exposes

140
00:05:52,919 --> 00:05:58,199
the !patch and !unpatch

141
00:05:55,800 --> 00:06:00,960
commands you should already be using

142
00:05:58,199 --> 00:06:04,680
gdb-prep command but you want to make

143
00:06:00,960 --> 00:06:07,139
sure to uncomment the HelpWinTheRace.js

144
00:06:04,680 --> 00:06:09,720
script so once you are

145
00:06:07,139 --> 00:06:11,940
debugging TmRecoverResourceManagerExt

146
00:06:09,720 --> 00:06:14,340
the like the vulnerable function you can

147
00:06:11,940 --> 00:06:16,199
execute the !patch command to

148
00:06:14,340 --> 00:06:18,120
install the patch and you should see

149
00:06:16,199 --> 00:06:20,460
something like that when continuing

150
00:06:18,120 --> 00:06:22,380
execution and it indicates the patch was

151
00:06:20,460 --> 00:06:24,539
successfully installed under the

152
00:06:22,380 --> 00:06:27,360
recovery thread is blocked forever until

153
00:06:24,539 --> 00:06:29,580
further notice at least later when you

154
00:06:27,360 --> 00:06:31,560
want to unblock the recovery thread you

155
00:06:29,580 --> 00:06:33,840
can break in WinDbg and use 

156
00:06:31,560 --> 00:06:35,160
!unpatch command and it will uninstall

157
00:06:33,840 --> 00:06:37,560
the patch and when you continue

158
00:06:35,160 --> 00:06:39,479
execution with the G command the

159
00:06:37,560 --> 00:06:42,060
recovery thread will continue educating

160
00:06:39,479 --> 00:06:44,039
the code after the KE wait for single

161
00:06:42,060 --> 00:06:46,979
code instruction as if nothing happens

162
00:06:44,039 --> 00:06:49,259
this slide is just to summarize what you

163
00:06:46,979 --> 00:06:52,199
need to do now for the setup basically

164
00:06:49,259 --> 00:06:54,900
you just need to first import the part 2

165
00:06:52,199 --> 00:06:58,020
labs in Visual Studio you need to enable

166
00:06:54,900 --> 00:07:00,539
verifier modify the debug scripts to

167
00:06:58,020 --> 00:07:02,580
load HelpWinTheRace.js and the

168
00:07:00,539 --> 00:07:05,400
project to use in Visual Studio is

169
00:07:02,580 --> 00:07:07,680
debugger_race_win this slide summarizes

170
00:07:05,400 --> 00:07:09,539
the steps involved in solving the lab

171
00:07:07,680 --> 00:07:11,520
the source code of that lab is provided

172
00:07:09,539 --> 00:07:13,380
entirely so you don't have to modify

173
00:07:11,520 --> 00:07:15,360
anything obviously we will have a look

174
00:07:13,380 --> 00:07:17,340
at it to understand what it does but you

175
00:07:15,360 --> 00:07:19,680
don't need to add any code and it will

176
00:07:17,340 --> 00:07:22,259
allow you to win the race as long as you

177
00:07:19,680 --> 00:07:24,840
use the debugger and patches correctly

178
00:07:22,259 --> 00:07:26,099
so the code will allow to reach the

179
00:07:24,840 --> 00:07:28,440
TmRecoverResourceManager vulnerable

180
00:07:26,099 --> 00:07:30,419
function in the source code we will see

181
00:07:28,440 --> 00:07:33,120
that the second enlistment is going to

182
00:07:30,419 --> 00:07:35,460
be committed and consequently finalized so

183
00:07:33,120 --> 00:07:37,440
this is the one we want to race so we

184
00:07:35,460 --> 00:07:39,419
will loop up to the second iteration of

185
00:07:37,440 --> 00:07:41,220
the loop then use the !patch command

186
00:07:39,419 --> 00:07:43,139
to block the recovery thread then we

187
00:07:41,220 --> 00:07:45,300
will be able to free the enlistment by

188
00:07:43,139 --> 00:07:47,460
hitting a key in the target VMs console

189
00:07:45,300 --> 00:07:50,400
as this will be handled by another

190
00:07:47,460 --> 00:07:52,860
thread which is just an assisting thread

191
00:07:50,400 --> 00:07:55,020
then we can break in the debugger and

192
00:07:52,860 --> 00:07:57,180
unblock the recovery thread using the

193
00:07:55,020 --> 00:07:59,280
!unpatch command and we should be

194
00:07:57,180 --> 00:08:01,380
able to step in the debugger and observe

195
00:07:59,280 --> 00:08:03,660
the use-after-free and this is just to show

196
00:08:01,380 --> 00:08:06,539
you an example of freed chunk being

197
00:08:03,660 --> 00:08:09,419
reused so for instance the move RDI

198
00:08:06,539 --> 00:08:12,000
[rdi]

199
00:08:09,419 --> 00:08:14,759
code is fetching the

200
00:08:12,000 --> 00:08:17,099
KENLISTMENT NextSameRm Flink pointer

201
00:08:14,759 --> 00:08:19,919
and if we check what it contains with

202
00:08:17,099 --> 00:08:22,259
the !pool command we see it points

203
00:08:19,919 --> 00:08:24,180
to a free chunk so this shows the

204
00:08:22,259 --> 00:08:26,460
use-after-free which indicates we won the race

205
00:08:24,180 --> 00:08:29,340
condition in the first place

206
00:08:26,460 --> 00:08:32,039
so the first thing to do is to extract

207
00:08:29,340 --> 00:08:34,800
the part 2 Labs so as you can see here

208
00:08:32,039 --> 00:08:36,979
we don't have part two in our Visual

209
00:08:34,800 --> 00:08:40,380
Studio project if you're going to Labs

210
00:08:36,979 --> 00:08:42,899
Visual Studio Labs we can see a part2.zip

211
00:08:40,380 --> 00:08:44,700
archive however in Visual Studio labs

212
00:08:42,899 --> 00:08:47,580
labs

213
00:08:44,700 --> 00:08:49,860
part one we have some

214
00:08:47,580 --> 00:08:54,540
labs in it but in part two we don't have

215
00:08:49,860 --> 00:08:56,760
them yet and so all you need to do is to

216
00:08:54,540 --> 00:09:01,380
extract that because as you can see it

217
00:08:56,760 --> 00:09:03,360
has the part 2 in it so we're gonna copy

218
00:09:01,380 --> 00:09:06,360
this into

219
00:09:03,360 --> 00:09:10,100
Labs here

220
00:09:06,360 --> 00:09:10,100
and I'm gonna extract it here

221
00:09:10,380 --> 00:09:15,480
it's going to act as to replace the part2

222
00:09:12,899 --> 00:09:18,080
CMakeLists.txt which obviously

223
00:09:15,480 --> 00:09:18,080
exists already

224
00:09:18,720 --> 00:09:22,320
now we can delete the zip archive and we

225
00:09:20,880 --> 00:09:25,459
can see we have

226
00:09:22,320 --> 00:09:25,459
part two folders

227
00:09:25,620 --> 00:09:30,120
so the last step is to regenerate the

228
00:09:27,779 --> 00:09:33,899
visual Studio Projects to take into

229
00:09:30,120 --> 00:09:38,839
account the new labs

230
00:09:33,899 --> 00:09:38,839
so we go into Visual Studio labs

231
00:09:38,880 --> 00:09:45,200
and we just execute build.bat

232
00:09:42,660 --> 00:09:45,200
hello

233
00:09:50,459 --> 00:09:56,220
now if we go back to our Visual Studio

234
00:09:53,339 --> 00:10:00,540
project we see that it's been modified

235
00:09:56,220 --> 00:10:02,940
outside the environment we can reload it

236
00:10:00,540 --> 00:10:04,560
and uh you can see we have access to the

237
00:10:02,940 --> 00:10:07,080
part two labs

238
00:10:04,560 --> 00:10:09,060
so now we move to the target VM and

239
00:10:07,080 --> 00:10:12,000
we're going to enable verifier

240
00:10:09,060 --> 00:10:14,700
so the first thing is we can check that

241
00:10:12,000 --> 00:10:17,040
verifier is not enabled by executing the

242
00:10:14,700 --> 00:10:20,220
verifier query settings command we can

243
00:10:17,040 --> 00:10:23,220
see none of the flags are enabled and

244
00:10:20,220 --> 00:10:25,700
also no driver is actually configured so

245
00:10:23,220 --> 00:10:31,380
we execute the verifier.exe /driver

246
00:10:25,700 --> 00:10:34,140
tm.sys /flags and then 809 hex

247
00:10:31,380 --> 00:10:38,000
we need to do it into an admin prompt

248
00:10:34,140 --> 00:10:38,000
otherwise you won't have permission

249
00:10:41,160 --> 00:10:46,740
so it's telling us that we enabled

250
00:10:43,800 --> 00:10:48,180
certain checks relative to the pool

251
00:10:46,740 --> 00:10:51,060
tracking

252
00:10:48,180 --> 00:10:54,540
and it is done on the tm.sys driver but

253
00:10:51,060 --> 00:10:55,860
this requires a reboot

254
00:10:54,540 --> 00:10:58,459
so we're just going to reboot the

255
00:10:55,860 --> 00:10:58,459
virtual machine

256
00:11:00,959 --> 00:11:06,240
if it does hangs like this you may have

257
00:11:03,600 --> 00:11:09,320
to reattach with the WinDbg debugger

258
00:11:06,240 --> 00:11:09,320
from the debugger VM

259
00:11:16,500 --> 00:11:22,620
once you're attached

260
00:11:18,360 --> 00:11:25,380
it's likely to unblock the target VM

261
00:11:22,620 --> 00:11:27,480
so when the target virtual machine is

262
00:11:25,380 --> 00:11:30,300
actually booting you can see

263
00:11:27,480 --> 00:11:34,440
from the WinDbg debugger that driver

264
00:11:30,300 --> 00:11:36,779
verifier is enabled for tm.sys

265
00:11:34,440 --> 00:11:41,700
and after it has booted

266
00:11:36,779 --> 00:11:44,420
you can actually use the verifier

267
00:11:41,700 --> 00:11:44,420
command

268
00:11:50,220 --> 00:11:55,680
to confirm it as well from the command

269
00:11:52,680 --> 00:11:57,420
line so from that state what we want to

270
00:11:55,680 --> 00:11:59,880
do is you want to do a snapshot of the

271
00:11:57,420 --> 00:12:03,560
virtual machine so I'm just gonna

272
00:11:59,880 --> 00:12:03,560
enable SSH first

273
00:12:12,540 --> 00:12:16,920
and now I'm going to do a snapshot of

274
00:12:14,519 --> 00:12:19,880
the virtual machine

275
00:12:16,920 --> 00:12:19,880
CTRL+M

276
00:12:19,980 --> 00:12:23,880
verifier enabled

277
00:12:31,079 --> 00:12:35,519
so let's have a quick look to the

278
00:12:33,180 --> 00:12:37,320
HelpWinTheRace.js file that's going to

279
00:12:35,519 --> 00:12:39,079
be loaded

280
00:12:37,320 --> 00:12:42,300
in WinDbg

281
00:12:39,079 --> 00:12:46,680
so at the very end we see the initialize

282
00:12:42,300 --> 00:12:50,100
script function which is generic API to

283
00:12:46,680 --> 00:12:51,959
actually register the extension and some

284
00:12:50,100 --> 00:12:53,940
commands so here it's going to register

285
00:12:51,959 --> 00:12:55,380
two commands the patch command and the

286
00:12:53,940 --> 00:12:57,899
unpatch command and we have the actual

287
00:12:55,380 --> 00:12:58,800
functions in JavaScript that is being

288
00:12:57,899 --> 00:13:02,220
called

289
00:12:58,800 --> 00:13:04,380
so if we look first at the install patch

290
00:13:02,220 --> 00:13:07,019
function we see that the first thing it

291
00:13:04,380 --> 00:13:10,260
does it reloads the symbols so the

292
00:13:07,019 --> 00:13:12,120
actual function names are known and we

293
00:13:10,260 --> 00:13:14,160
can actually set breakpoints or delete

294
00:13:12,120 --> 00:13:16,920
breakpoints

295
00:13:14,160 --> 00:13:19,740
then it's actually setting a breakpoint

296
00:13:16,920 --> 00:13:21,959
on a particular patch location

297
00:13:19,740 --> 00:13:23,940
so the patch location is defined at the

298
00:13:21,959 --> 00:13:26,459
beginning of the script we can see it's

299
00:13:23,940 --> 00:13:28,200
in the tm.sys module it's in the

300
00:13:26,459 --> 00:13:31,380
TmRecoverResourceManagerExt and a

301
00:13:28,200 --> 00:13:33,360
particular offset 27e

302
00:13:31,380 --> 00:13:35,700
so we said earlier it's going to be done

303
00:13:33,360 --> 00:13:38,639
on the call KeWaitForSingleObject

304
00:13:35,700 --> 00:13:40,860
but for implementation reasons there is

305
00:13:38,639 --> 00:13:43,380
a command saying that we don't do it on

306
00:13:40,860 --> 00:13:46,139
the actual koe e-weed for single object

307
00:13:43,380 --> 00:13:48,600
function because of some Dynamic patch

308
00:13:46,139 --> 00:13:50,760
that is done on this instruction but we

309
00:13:48,600 --> 00:13:52,860
do it on the actual move rcx or BX that

310
00:13:50,760 --> 00:13:54,560
you just before so that's why it's done

311
00:13:52,860 --> 00:13:57,600
on the

312
00:13:54,560 --> 00:14:01,079
27e offset

313
00:13:57,600 --> 00:14:03,959
that being said if we go back to the

314
00:14:01,079 --> 00:14:06,740
patch location being used we see that

315
00:14:03,959 --> 00:14:11,760
there is a break point on this location

316
00:14:06,740 --> 00:14:14,100
and when this breakpoint is set we

317
00:14:11,760 --> 00:14:16,079
actually Define a command to execute

318
00:14:14,100 --> 00:14:19,740
more specifically we're going to execute

319
00:14:16,079 --> 00:14:22,260
the dx@$scriptContents.

320
00:14:19,740 --> 00:14:24,600
installPatch_bp_handler

321
00:14:22,260 --> 00:14:26,100
which is a function we can define above

322
00:14:24,600 --> 00:14:27,660
but basically what it's going to do

323
00:14:26,100 --> 00:14:31,320
we're going to see that in a second

324
00:14:27,660 --> 00:14:34,740
before the patch we see the move rcx

325
00:14:31,320 --> 00:14:36,300
original code and after the patch we see

326
00:14:34,740 --> 00:14:38,240
a jump

327
00:14:36,300 --> 00:14:41,100
to the offset

328
00:14:38,240 --> 00:14:43,079
27e which is basically itself so it's

329
00:14:41,100 --> 00:14:46,079
going to loop here forever and it does

330
00:14:43,079 --> 00:14:50,240
that by patching the patch location with

331
00:14:46,079 --> 00:14:50,240
a jump to itself

332
00:14:50,519 --> 00:14:56,899
so when the breakpoint hits we see it

333
00:14:54,180 --> 00:14:56,899
calls this function

334
00:14:58,139 --> 00:15:03,240
so this function what it does is it

335
00:15:00,720 --> 00:15:06,360
executes the K command to show the back

336
00:15:03,240 --> 00:15:08,760
trace and print the output and then it

337
00:15:06,360 --> 00:15:10,500
removes the patch location by calling

338
00:15:08,760 --> 00:15:12,360
the remove breakpoint

339
00:15:10,500 --> 00:15:15,420
and if you look at the remove breakpoint

340
00:15:12,360 --> 00:15:18,240
function it only supports one breakpoint

341
00:15:15,420 --> 00:15:20,160
so no other breakpoints need to exist in

342
00:15:18,240 --> 00:15:22,800
the context and what it is going to do

343
00:15:20,160 --> 00:15:24,240
is going to basically call the BC to

344
00:15:22,800 --> 00:15:25,620
clear the breakpoint to remove the

345
00:15:24,240 --> 00:15:28,820
breakpoints on that particular

346
00:15:25,620 --> 00:15:28,820
breakpoint ID

347
00:15:35,100 --> 00:15:38,279
so now let's have a look at the

348
00:15:36,959 --> 00:15:40,260
unpatch

349
00:15:38,279 --> 00:15:43,019
command

350
00:15:40,260 --> 00:15:44,940
the unpatch command is just again

351
00:15:43,019 --> 00:15:46,620
loading the symbols to make sure the

352
00:15:44,940 --> 00:15:49,199
functions are known and then it's just

353
00:15:46,620 --> 00:15:52,620
deleting the breakpoint if it still

354
00:15:49,199 --> 00:15:55,740
exists and in any case it will actually

355
00:15:52,620 --> 00:15:58,260
restore the old code by executing the EB

356
00:15:55,740 --> 00:16:01,800
to edit the bytes of the patch location

357
00:15:58,260 --> 00:16:03,660
and just to restore the previous bytes

358
00:16:01,800 --> 00:16:06,300
and if we look at the previous bytes

359
00:16:03,660 --> 00:16:08,459
they are defined here 48

360
00:16:06,300 --> 00:16:10,980
8b

361
00:16:08,459 --> 00:16:13,740
but really all you need to understand is

362
00:16:10,980 --> 00:16:15,060
that it exposes the two commands that we

363
00:16:13,740 --> 00:16:17,100
need to use

364
00:16:15,060 --> 00:16:20,180
so now let's have a look at the actual

365
00:16:17,100 --> 00:16:23,100
lab source code we go into the

366
00:16:20,180 --> 00:16:25,920
DebuggerRaceWin.c so there are lots of

367
00:16:23,100 --> 00:16:28,800
code that we've already seen so I'll

368
00:16:25,920 --> 00:16:32,339
just go over the new code we see we

369
00:16:28,800 --> 00:16:36,660
define different core IDs it's because

370
00:16:32,339 --> 00:16:39,360
we're gonna want to pin certain code to

371
00:16:36,660 --> 00:16:42,480
specific CPU calls and remember in the

372
00:16:39,360 --> 00:16:45,120
target VM we have defined the target VM

373
00:16:42,480 --> 00:16:48,720
to have several processors at least two

374
00:16:45,120 --> 00:16:51,360
in this case we need for this lab

375
00:16:48,720 --> 00:16:54,779
so we have the general structure we've

376
00:16:51,360 --> 00:16:57,120
already seen a functions that exits with

377
00:16:54,779 --> 00:17:00,380
a printf this is the function that

378
00:16:57,120 --> 00:17:04,639
actually pins to a CPU the current

379
00:17:00,380 --> 00:17:04,639
code being executed

380
00:17:05,220 --> 00:17:11,220
then this is the handler for a second

381
00:17:08,699 --> 00:17:13,260
thread that will be created which we

382
00:17:11,220 --> 00:17:15,299
named the assisting thread and it's

383
00:17:13,260 --> 00:17:18,540
going to be past the enlistment handle

384
00:17:15,299 --> 00:17:19,679
for the second enlistment and basically

385
00:17:18,540 --> 00:17:21,120
what it's going to do it's going to

386
00:17:19,679 --> 00:17:24,480
print some instructions but then it's

387
00:17:21,120 --> 00:17:27,240
gonna basically wait that we hit a key

388
00:17:24,480 --> 00:17:30,059
before it actually does anything and its

389
00:17:27,240 --> 00:17:33,600
goal is going to be to commit the

390
00:17:30,059 --> 00:17:36,360
enlistment and to close the handle so

391
00:17:33,600 --> 00:17:38,700
the an enlistment is freed in kernel and

392
00:17:36,360 --> 00:17:42,020
that's all it does but it will only do

393
00:17:38,700 --> 00:17:42,020
it when we hit the key

394
00:17:42,179 --> 00:17:46,980
so then we have helper function that

395
00:17:43,980 --> 00:17:50,340
we've already seen to deal with GUID to

396
00:17:46,980 --> 00:17:53,100
print the notification messages

397
00:17:50,340 --> 00:17:55,020
as well as this function to get the

398
00:17:53,100 --> 00:17:58,620
notification from the resource manager

399
00:17:55,020 --> 00:18:02,340
and this function that loops over all

400
00:17:58,620 --> 00:18:05,220
the available notifications

401
00:18:02,340 --> 00:18:07,080
so now we look at the main function we

402
00:18:05,220 --> 00:18:10,200
have the instructions of the lab which

403
00:18:07,080 --> 00:18:12,660
is basically to use the patch command

404
00:18:10,200 --> 00:18:15,840
and then loop for the second iteration

405
00:18:12,660 --> 00:18:17,820
of the loop to trigger use after free on

406
00:18:15,840 --> 00:18:19,740
the second enlistment but basically it's

407
00:18:17,820 --> 00:18:21,660
called all the functions that we've seen

408
00:18:19,740 --> 00:18:23,820
so far it's called the create

409
00:18:21,660 --> 00:18:25,799
transaction manager and then recover

410
00:18:23,820 --> 00:18:28,320
transaction manager same thing for the

411
00:18:25,799 --> 00:18:30,600
resource manager create resource manager

412
00:18:28,320 --> 00:18:33,960
and recover resource manager it creates

413
00:18:30,600 --> 00:18:35,580
a transaction and then it creates two

414
00:18:33,960 --> 00:18:38,059
enlistments for that specific

415
00:18:35,580 --> 00:18:38,059
transaction

416
00:18:38,820 --> 00:18:46,620
and then it reads the notifications

417
00:18:42,120 --> 00:18:49,799
before it's pre-prepared the both of the

418
00:18:46,620 --> 00:18:52,020
enlistment and prepares both of the

419
00:18:49,799 --> 00:18:55,020
enlistments so here we do not want to

420
00:18:52,020 --> 00:18:56,880
commit completes yet because we want the

421
00:18:55,020 --> 00:18:58,679
vulnerable function to send notification

422
00:18:56,880 --> 00:19:01,080
to our enlistment so we need them to

423
00:18:58,679 --> 00:19:02,220
still exist however when we're going to

424
00:19:01,080 --> 00:19:04,020
hit the key

425
00:19:02,220 --> 00:19:05,580
it's going to basically create this

426
00:19:04,020 --> 00:19:08,520
additional thread that we've just seen

427
00:19:05,580 --> 00:19:12,000
that will basically

428
00:19:08,520 --> 00:19:14,780
um close the enlistments and

429
00:19:12,000 --> 00:19:14,780
commit it

430
00:19:22,020 --> 00:19:28,320
so really once you have defined the

431
00:19:26,039 --> 00:19:30,480
breakpoint you can hit a key and this

432
00:19:28,320 --> 00:19:32,400
thread will take care of freeing the

433
00:19:30,480 --> 00:19:34,260
enlistments and you should see the

434
00:19:32,400 --> 00:19:38,840
use-after-free in the debugger

435
00:19:34,260 --> 00:19:38,840
okay now it's your turn

