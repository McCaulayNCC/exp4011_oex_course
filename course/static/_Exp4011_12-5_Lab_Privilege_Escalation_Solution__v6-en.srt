1
00:00:00,060 --> 00:00:05,580
so let's have a look at the code I have

2
00:00:02,760 --> 00:00:07,560
implemented to solve that lab so the

3
00:00:05,580 --> 00:00:11,639
first thing we need to do is we need to

4
00:00:07,560 --> 00:00:14,340
read the e-process address for our

5
00:00:11,639 --> 00:00:16,619
recovery thread so we know we lead the

6
00:00:14,340 --> 00:00:19,859
case thread address so we can just add

7
00:00:16,619 --> 00:00:22,199
the process offset inside the case

8
00:00:19,859 --> 00:00:25,560
thread structure and we will be able to

9
00:00:22,199 --> 00:00:28,380
leak one EPROCESS pointer which in this

10
00:00:25,560 --> 00:00:30,660
case is the process associated with our

11
00:00:28,380 --> 00:00:33,239
exploits and then we can call the fine

12
00:00:30,660 --> 00:00:34,860
e-process god mode Prem function that's

13
00:00:33,239 --> 00:00:38,700
going to basically start from this

14
00:00:34,860 --> 00:00:41,160
e-process and find the process with the

15
00:00:38,700 --> 00:00:44,660
pid4 and so we know it's going to be our

16
00:00:41,160 --> 00:00:48,300
system e-process and then from this

17
00:00:44,660 --> 00:00:51,420
e-process address we can just access the

18
00:00:48,300 --> 00:00:54,660
token pointer by reading the keyword

19
00:00:51,420 --> 00:00:57,360
from the e-process plus the offset to

20
00:00:54,660 --> 00:01:00,420
the actual token and we say that

21
00:00:57,360 --> 00:01:02,940
and here optionally what we do is we use

22
00:01:00,420 --> 00:01:05,580
the system token folder and we update

23
00:01:02,940 --> 00:01:08,100
the ref counts so in the Windows world

24
00:01:05,580 --> 00:01:10,799
it's going to be the pointer count and

25
00:01:08,100 --> 00:01:14,040
the handle count basically the pointer

26
00:01:10,799 --> 00:01:16,799
count is the number of direct Cannon

27
00:01:14,040 --> 00:01:21,119
Mode pointer references and handle count

28
00:01:16,799 --> 00:01:23,759
is the number of indirect references via

29
00:01:21,119 --> 00:01:26,939
handle both from username and from

30
00:01:23,759 --> 00:01:29,159
Canada and basically what we do is we

31
00:01:26,939 --> 00:01:31,759
take the system token pointer released

32
00:01:29,159 --> 00:01:35,820
and we know the lower bytes at least

33
00:01:31,759 --> 00:01:38,400
half of it is actually not part of the

34
00:01:35,820 --> 00:01:41,820
actual pointer so we take the adjusted

35
00:01:38,400 --> 00:01:44,520
boiler and just remove the lower part

36
00:01:41,820 --> 00:01:47,040
and then we have this pointer to the

37
00:01:44,520 --> 00:01:49,680
Token structure but we want to modify

38
00:01:47,040 --> 00:01:52,619
the object header that is before the

39
00:01:49,680 --> 00:01:55,140
token structure in memory and so we

40
00:01:52,619 --> 00:01:57,119
subtract the object header structure

41
00:01:55,140 --> 00:01:59,280
size and we get an object header and so

42
00:01:57,119 --> 00:02:01,880
from from there we can modify the

43
00:01:59,280 --> 00:02:05,700
pointer count so we just read the value

44
00:02:01,880 --> 00:02:08,220
into a username variable and then we add

45
00:02:05,700 --> 00:02:11,459
to for instance potentially you can add

46
00:02:08,220 --> 00:02:15,060
one only that should be enough or you

47
00:02:11,459 --> 00:02:17,640
can add 100 if you want but yeah just be

48
00:02:15,060 --> 00:02:19,680
careful not to add too much because if

49
00:02:17,640 --> 00:02:22,800
you wrap around then it can be

50
00:02:19,680 --> 00:02:25,379
problematic but yeah I guess if you

51
00:02:22,800 --> 00:02:27,599
don't run that many times the X by 2 is

52
00:02:25,379 --> 00:02:30,720
is perfect so we just update Point

53
00:02:27,599 --> 00:02:34,560
account and 100 count the same way and

54
00:02:30,720 --> 00:02:37,440
finally we update the pointer to the

55
00:02:34,560 --> 00:02:41,180
Token in our e-process so we take our

56
00:02:37,440 --> 00:02:43,980
e-process we just read the previous

57
00:02:41,180 --> 00:02:46,140
token before we override it in case we

58
00:02:43,980 --> 00:02:48,000
want to restore it later just a good

59
00:02:46,140 --> 00:02:50,040
habit to do this kind of thing before

60
00:02:48,000 --> 00:02:53,400
your current memory just to save it and

61
00:02:50,040 --> 00:02:55,080
then we override it with the actual

62
00:02:53,400 --> 00:02:57,840
system.inforter

63
00:02:55,080 --> 00:03:01,080
okay so now it's time to actually run

64
00:02:57,840 --> 00:03:03,720
the exploits so I did push the privilege

65
00:03:01,080 --> 00:03:06,959
escalation lab executable on the target

66
00:03:03,720 --> 00:03:09,000
VM and yeah for this lab I won't

67
00:03:06,959 --> 00:03:11,280
actually use the debugger I mean I've

68
00:03:09,000 --> 00:03:14,659
attached it but I I didn't set any

69
00:03:11,280 --> 00:03:21,000
breakpoints and yeah we can see the

70
00:03:14,659 --> 00:03:24,379
cmd.xz has the IE user normal user and

71
00:03:21,000 --> 00:03:24,379
I'm just going to run the export now

72
00:03:32,540 --> 00:03:36,140
thank you

73
00:03:34,739 --> 00:03:39,300
so we won the race condition

74
00:03:36,140 --> 00:03:39,300
[pause]

75
00:03:45,480 --> 00:03:52,140
so we leave the addresses if you want

76
00:03:48,180 --> 00:03:55,140
you can even comment this uh message so

77
00:03:52,140 --> 00:03:57,540
you don't have to hit a key yeah we see

78
00:03:55,140 --> 00:04:00,840
it spawn the shell just by saying

79
00:03:57,540 --> 00:04:04,080
Microsoft Windows is the text for the

80
00:04:00,840 --> 00:04:06,360
CMD and we can see in ProcessExplorer

81
00:04:04,080 --> 00:04:07,500
that our shell is in the authority

82
00:04:06,360 --> 00:04:10,019
system

83
00:04:07,500 --> 00:04:13,500
which I can confirm with who am I and if

84
00:04:10,019 --> 00:04:16,040
I did the exploits it kills it and I can

85
00:04:13,500 --> 00:04:16,040
rerun it

86
00:04:33,660 --> 00:04:36,660
foreign

87
00:04:50,960 --> 00:04:56,960
feels good isn't it okay

88
00:04:54,120 --> 00:04:56,960
last time

89
00:05:14,960 --> 00:05:20,639
interesting so this could happen I'm not

90
00:05:18,419 --> 00:05:23,400
sure exactly why but sometimes it hangs

91
00:05:20,639 --> 00:05:25,440
and it doesn't actually let me exit the

92
00:05:23,400 --> 00:05:28,139
process for some reason so in this case

93
00:05:25,440 --> 00:05:32,520
I can just kill it it should just be

94
00:05:28,139 --> 00:05:35,300
stable still and I can respawn a CMD

95
00:05:32,520 --> 00:05:35,300
and try again

96
00:05:56,280 --> 00:05:59,280
[pause]

97
00:06:23,880 --> 00:06:26,959
I hope you have enjoyed this, and I will see you in the next video

