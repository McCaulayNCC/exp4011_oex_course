1
00:00:03,240 --> 00:00:06,779
hi everyone

2
00:00:04,860 --> 00:00:09,179
in this part you are going to have to

3
00:00:06,779 --> 00:00:11,400
modify the source code of the lab to do

4
00:00:09,179 --> 00:00:15,480
stuff after you win the race condition

5
00:00:11,400 --> 00:00:17,340
yeah finally exploitation stuff in order

6
00:00:15,480 --> 00:00:19,740
to do so you're going to have to craft

7
00:00:17,340 --> 00:00:22,680
fake userland enlistments in

8
00:00:19,740 --> 00:00:23,939
userland and these will be parsed by the

9
00:00:22,680 --> 00:00:26,340
kernel

10
00:00:23,939 --> 00:00:27,960
also the goal is going to be to detect

11
00:00:26,340 --> 00:00:30,000
from userland

12
00:00:27,960 --> 00:00:32,279
that the kernel touch your userland

13
00:00:30,000 --> 00:00:34,200
enlistments and so that you won the

14
00:00:32,279 --> 00:00:36,719
race and finally

15
00:00:34,200 --> 00:00:38,340
you're gonna have to find a good spot to

16
00:00:36,719 --> 00:00:41,100
set the breakpoint so you can actually

17
00:00:38,340 --> 00:00:43,920
debug that you won the race very easily

18
00:00:41,100 --> 00:00:45,120
in all the future labs okay let's get

19
00:00:43,920 --> 00:00:47,520
started

20
00:00:45,120 --> 00:00:49,620
as we said most of the code in the main

21
00:00:47,520 --> 00:00:51,480
loop is executed for each iteration of

22
00:00:49,620 --> 00:00:53,820
the loop and so we don't really want to

23
00:00:51,480 --> 00:00:56,160
set a breakpoint that would be executed

24
00:00:53,820 --> 00:00:57,899
each time and an enlistment is parsed

25
00:00:56,160 --> 00:01:00,180
and so we are going to set a breakpoint

26
00:00:57,899 --> 00:01:02,160
in an area that is never reached in a

27
00:01:00,180 --> 00:01:04,379
normal scenario and so we're choosing to

28
00:01:02,160 --> 00:01:06,299
set a breakpoint in the code handling

29
00:01:04,379 --> 00:01:08,820
Superior enlistments and so this

30
00:01:06,299 --> 00:01:11,220
breakpoint will only hit when the

31
00:01:08,820 --> 00:01:13,740
kernel code is starting to handle our

32
00:01:11,220 --> 00:01:15,720
fake useland KENLISTMENT so the lab

33
00:01:13,740 --> 00:01:18,180
we're going to work on right now is

34
00:01:15,720 --> 00:01:21,060
called race win detection and so the

35
00:01:18,180 --> 00:01:23,759
main goal is to craft a good spray at

36
00:01:21,060 --> 00:01:26,340
KENLISTMENT and a good trap

37
00:01:23,759 --> 00:01:28,200
KENLISTMENT and hit a breakpoint in the

38
00:01:26,340 --> 00:01:30,780
debugger after we win the race condition

39
00:01:28,200 --> 00:01:33,299
and so we are still going to use the

40
00:01:30,780 --> 00:01:35,640
!patch command in the debugger to

41
00:01:33,299 --> 00:01:38,340
help us win the race but instead of

42
00:01:35,640 --> 00:01:40,619
spraying A's in the named pipe to replace

43
00:01:38,340 --> 00:01:43,079
our KENLISTMENTs that we want to use

44
00:01:40,619 --> 00:01:46,079
after free we are going to craft a spray

45
00:01:43,079 --> 00:01:48,600
KENLISTMENT that will have it's

46
00:01:46,079 --> 00:01:51,180
NextSameRm flink pointer at the right

47
00:01:48,600 --> 00:01:53,700
offset inside the named pipe chunk and

48
00:01:51,180 --> 00:01:56,460
this spray KENLISTMENT will be pointing

49
00:01:53,700 --> 00:01:58,979
to a trap KENLISTMENT that has its

50
00:01:56,460 --> 00:02:01,979
NextSameRm flink pointer pointing to itself

51
00:01:58,979 --> 00:02:04,140
so the kernel infinite loop and so by

52
00:02:01,979 --> 00:02:06,840
making the trap KENLISTMENT have the

53
00:02:04,140 --> 00:02:08,940
superior flag it will ease debugging the

54
00:02:06,840 --> 00:02:10,739
debugger should kick in right after we

55
00:02:08,940 --> 00:02:13,680
win the race and we should be able to

56
00:02:10,739 --> 00:02:15,959
debug the change of flag on our trap

57
00:02:13,680 --> 00:02:18,000
KENLISTMENT one thing to say that we

58
00:02:15,959 --> 00:02:20,520
won't be able to debug our spray

59
00:02:18,000 --> 00:02:24,300
enlistments because our breakpoint will

60
00:02:20,520 --> 00:02:26,760
only hit after the
spray KENLISTMENT NextSameRm

61
00:02:24,300 --> 00:02:28,739
flink pointer is retrieved at the end of

62
00:02:26,760 --> 00:02:30,840
the main loop and so the next iteration

63
00:02:28,739 --> 00:02:32,879
of the loop will be executed on our

64
00:02:30,840 --> 00:02:35,340
trap enlistment and then our

65
00:02:32,879 --> 00:02:38,700
debugger will kick in so let's start on

66
00:02:35,340 --> 00:02:40,800
the RaceWinDetection.c file and more

67
00:02:38,700 --> 00:02:43,080
specifically the main function so we can

68
00:02:40,800 --> 00:02:45,379
see we have instructions which is to

69
00:02:43,080 --> 00:02:48,840
craft a spray enlistment

70
00:02:45,379 --> 00:02:51,060
as well as a trap enlistment use WinDbg

71
00:02:48,840 --> 00:02:53,640
back to assist in blocking the recovery

72
00:02:51,060 --> 00:02:57,180
thread and then add code to detect the

73
00:02:53,640 --> 00:03:00,060
race was won from userland we can see

74
00:02:57,180 --> 00:03:03,660
that there is the exploit init function

75
00:03:00,060 --> 00:03:07,819
that just initialized the uh variables

76
00:03:03,660 --> 00:03:07,819
for the exploit vars structure

77
00:03:09,660 --> 00:03:14,099
we see it's checking the number of cores

78
00:03:12,060 --> 00:03:15,420
to make sure we have at least two CPUs

79
00:03:14,099 --> 00:03:17,220
so we can actually create different

80
00:03:15,420 --> 00:03:20,159
threads in different cores so then it's

81
00:03:17,220 --> 00:03:22,940
calling uh the init fake enlistment

82
00:03:20,159 --> 00:03:22,940
function

83
00:03:23,040 --> 00:03:27,360
so we can see this function is defined

84
00:03:25,080 --> 00:03:30,060
in RaceWinDetection.c the same C file

85
00:03:27,360 --> 00:03:32,040
and that we're going to have to add code

86
00:03:30,060 --> 00:03:34,739
to this function but the main thing is

87
00:03:32,040 --> 00:03:36,959
that its goal is to actually create two

88
00:03:34,739 --> 00:03:40,680
fake enlistments
 a spray enlistment

89
00:03:36,959 --> 00:03:42,480
that will replace the freed KENLISTMENT

90
00:03:40,680 --> 00:03:44,760
chunk before we trigger the use after

91
00:03:42,480 --> 00:03:46,500
free and this spray_enlistment in userland

92
00:03:44,760 --> 00:03:48,420
will have to point to another

93
00:03:46,500 --> 00:03:49,980
enlistment called trap enlistment and

94
00:03:48,420 --> 00:03:52,019
this trap enlistment will point to

95
00:03:49,980 --> 00:03:54,180
itself to make sure that we are able to

96
00:03:52,019 --> 00:03:55,680
trap the kernel into an infinite loop

97
00:03:54,180 --> 00:03:57,780
and so this init_fake_enlistment

98
00:03:55,680 --> 00:04:00,000
calls two functions build trap

99
00:03:57,780 --> 00:04:02,099
enlistment as well as init spray

100
00:04:00,000 --> 00:04:04,860
enlistment so if we look at build trap

101
00:04:02,099 --> 00:04:06,900
enlistment we can see that this function

102
00:04:04,860 --> 00:04:08,940
is designed to build a trap enlistment

103
00:04:06,900 --> 00:04:11,580
that points to itself so we can see that

104
00:04:08,940 --> 00:04:14,519
it's allocating an object header that

105
00:04:11,580 --> 00:04:17,160
needs to be before the KENLISTMENT

106
00:04:14,519 --> 00:04:20,100
structure and it's also allocating a

107
00:04:17,160 --> 00:04:22,199
KTRANSACTION because we saw that we

108
00:04:20,100 --> 00:04:24,720
need the KENLISTMENT to have a valid

109
00:04:22,199 --> 00:04:27,419
transaction field so it's initializing a

110
00:04:24,720 --> 00:04:29,280
pointer count for the object header and

111
00:04:27,419 --> 00:04:32,280
then it's filling some mutex information

112
00:04:29,280 --> 00:04:35,580
into the KENLISTMENT and we see we'll

113
00:04:32,280 --> 00:04:39,860
have to actually fill certain fields in

114
00:04:35,580 --> 00:04:39,860
order to make the kernel happy

115
00:04:44,639 --> 00:04:49,620
so once that trap enlistment is saved

116
00:04:46,979 --> 00:04:52,080
into the exploit vars structure we see

117
00:04:49,620 --> 00:04:55,020
that we have to actually find an offset

118
00:04:52,080 --> 00:04:57,180
in the chunk that actually points to the

119
00:04:55,020 --> 00:04:58,860
fake KENLISTMENT and the reason is

120
00:04:57,180 --> 00:05:00,540
because we're going to use named pipe to

121
00:04:58,860 --> 00:05:02,460
spray the data and we're going to have

122
00:05:00,540 --> 00:05:05,280
to take into account the structures from

123
00:05:02,460 --> 00:05:07,740
both the named pipe and
the KENLISTMENT so

124
00:05:05,280 --> 00:05:09,900
the offsets of the NextSameRm flink

125
00:05:07,740 --> 00:05:12,259
pointer when it's retrieved is a valid

126
00:05:09,900 --> 00:05:12,259
pointer

127
00:05:15,960 --> 00:05:21,600
so we can see in its spray enlistment is

128
00:05:18,600 --> 00:05:24,419
called and it's passed the spray

129
00:05:21,600 --> 00:05:26,400
enlistment size as well as the pointer

130
00:05:24,419 --> 00:05:29,539
to the trap enlistment that has just

131
00:05:26,400 --> 00:05:29,539
been initialized

132
00:05:33,240 --> 00:05:41,960
right so this function allocates a chunk

133
00:05:36,300 --> 00:05:41,960
of the actual size that has been spread

134
00:05:42,979 --> 00:05:49,199
and then it actually assume the

135
00:05:46,740 --> 00:05:51,060
KENLISTMENT starts from the beginning

136
00:05:49,199 --> 00:05:53,100
of the chunk plus the offset that we

137
00:05:51,060 --> 00:05:55,199
need to find out and so because we are

138
00:05:53,100 --> 00:05:57,180
building our spray enlistment and

139
00:05:55,199 --> 00:05:59,460
we want that spray enlistment to

140
00:05:57,180 --> 00:06:01,919
point to the trap enlistment since we've

141
00:05:59,460 --> 00:06:04,380
passed our trap enlistment at the Flink

142
00:06:01,919 --> 00:06:06,780
argument the last thing we have to do is

143
00:06:04,380 --> 00:06:09,360
basically to point our spray

144
00:06:06,780 --> 00:06:12,440
enlistment NextSameRm.Flink pointer to

145
00:06:09,360 --> 00:06:12,440
the actual trap enlistment

146
00:06:13,320 --> 00:06:18,360
so basically the init fake

147
00:06:15,720 --> 00:06:21,900
enlistment builds the trap enlistment

148
00:06:18,360 --> 00:06:23,819
and then create the spray enlistment and

149
00:06:21,900 --> 00:06:25,979
make the spray enlistment point to the

150
00:06:23,819 --> 00:06:28,380
trap enlistment so now we go back to the

151
00:06:25,979 --> 00:06:31,440
main function and we see it called the

152
00:06:28,380 --> 00:06:33,120
init_threds_and_ktm_object so we've

153
00:06:31,440 --> 00:06:35,220
seen this function before because it's

154
00:06:33,120 --> 00:06:37,080
defining trigger.c

155
00:06:35,220 --> 00:06:41,039
but basically this function will

156
00:06:37,080 --> 00:06:43,139
initialize the KTM objects so we have

157
00:06:41,039 --> 00:06:45,180
all the environments so we can recover

158
00:06:43,139 --> 00:06:47,940
the resource manager and trigger the bug

159
00:06:45,180 --> 00:06:49,560
but it also defined the threads that

160
00:06:47,940 --> 00:06:51,419
charge the recovery thread which

161
00:06:49,560 --> 00:06:53,520
triggered the back but also the pipe

162
00:06:51,419 --> 00:06:57,060
server thread which actually helped us

163
00:06:53,520 --> 00:06:59,580
spray named pipe chunks so we when we

164
00:06:57,060 --> 00:07:03,180
want to replace the freed KENLISTMENT

165
00:06:59,580 --> 00:07:05,419
before the use after free

166
00:07:03,180 --> 00:07:05,419
okay

167
00:07:09,000 --> 00:07:13,259
and now we're back to the main function

168
00:07:11,039 --> 00:07:15,240
and we see it called the race recovering

169
00:07:13,259 --> 00:07:17,759
resource manager userland detection

170
00:07:15,240 --> 00:07:20,639
function which is specific to this

171
00:07:17,759 --> 00:07:22,199
RaceWinDetection.c and this function is

172
00:07:20,639 --> 00:07:25,860
responsible for

173
00:07:22,199 --> 00:07:27,479
looping over our enlistments while we

174
00:07:25,860 --> 00:07:30,060
trigger the bug so we can see

175
00:07:27,479 --> 00:07:31,740
instructions telling us that we need to

176
00:07:30,060 --> 00:07:35,099
actually at least

177
00:07:31,740 --> 00:07:36,840
detect that 32 KENLISTMENTs have been

178
00:07:35,099 --> 00:07:38,940
touched by the kernel before we can

179
00:07:36,840 --> 00:07:40,860
actually free the latest one that has

180
00:07:38,940 --> 00:07:42,960
been touched so we work around the

181
00:07:40,860 --> 00:07:45,360
delayed free list we see here that we

182
00:07:42,960 --> 00:07:47,940
set the event to actually make the

183
00:07:45,360 --> 00:07:50,520
recovery thread trigger the bug in

184
00:07:47,940 --> 00:07:53,099
another thread

185
00:07:50,520 --> 00:07:55,020
and then from the main thread what we're

186
00:07:53,099 --> 00:07:56,400
going to do is we're going to read the

187
00:07:55,020 --> 00:08:00,900
notification

188
00:07:56,400 --> 00:08:04,319
so we see here we at least read up to 32

189
00:08:00,900 --> 00:08:07,020
notification to make sure we can bypass

190
00:08:04,319 --> 00:08:09,960
the delayed free list mitigation so if

191
00:08:07,020 --> 00:08:13,199
we if we went over more than the number

192
00:08:09,960 --> 00:08:15,419
of enlistments that we have defined it

193
00:08:13,199 --> 00:08:19,560
means we won't be able to trigger the

194
00:08:15,419 --> 00:08:22,139
the race condition so we exit the loop

195
00:08:19,560 --> 00:08:25,500
after that we see that we start from the

196
00:08:22,139 --> 00:08:29,580
latest enlistment that has been touched

197
00:08:25,500 --> 00:08:31,740
um and we decreased that index so we

198
00:08:29,580 --> 00:08:35,039
free the latest one and then we free at

199
00:08:31,740 --> 00:08:37,260
least 31 other KENLISTMENT to bypass the

200
00:08:35,039 --> 00:08:40,020
delayed for this mitigation and for each

201
00:08:37,260 --> 00:08:42,479
enlistment we not only commit completed

202
00:08:40,020 --> 00:08:45,060
but also close the handle so the

203
00:08:42,479 --> 00:08:47,399
enlistment is finalized but also freed

204
00:08:45,060 --> 00:08:50,040
now we have the code to actually spray

205
00:08:47,399 --> 00:08:51,779
named pipe chunks and we can see we

206
00:08:50,040 --> 00:08:53,580
passed the spray enlistment as an

207
00:08:51,779 --> 00:08:55,620
argument so it's going to actually use

208
00:08:53,580 --> 00:08:58,080
the chain of enlistment that we

209
00:08:55,620 --> 00:09:00,420
previously built then we see additional

210
00:08:58,080 --> 00:09:02,820
instructions so it's telling us to

211
00:09:00,420 --> 00:09:05,940
unpatch the recovery thread and then we

212
00:09:02,820 --> 00:09:08,940
will be able to analyze if our sprayed

213
00:09:05,940 --> 00:09:10,980
enlistment actually replaced the KENLISTMENT

214
00:09:08,940 --> 00:09:13,500
that has just been freed and also

215
00:09:10,980 --> 00:09:15,899
analyze if the different offsets and

216
00:09:13,500 --> 00:09:18,120
fields that we have defined make the

217
00:09:15,899 --> 00:09:21,000
kernel happy and the last thing we see

218
00:09:18,120 --> 00:09:23,640
here is that there is a tests that need

219
00:09:21,000 --> 00:09:26,060
we need to add so basically because the

220
00:09:23,640 --> 00:09:27,839
kernel is going to touch our fake

221
00:09:26,060 --> 00:09:30,060
userland enlistment and it's going to

222
00:09:27,839 --> 00:09:33,120
actually unset the notifiable flag we

223
00:09:30,060 --> 00:09:35,220
can loop here and detect when this

224
00:09:33,120 --> 00:09:38,240
happens and so when this happens we can

225
00:09:35,220 --> 00:09:42,680
just say return true we won the race

226
00:09:38,240 --> 00:09:42,680
okay now it's your turn

