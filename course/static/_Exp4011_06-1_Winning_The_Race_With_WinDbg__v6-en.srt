0
00:00:03,240 --> 00:00:07,799
hi everyone in the next few videos we're

1
00:00:05,640 --> 00:00:09,780
going to look into how to win the race

2
00:00:07,799 --> 00:00:11,940
condition in the debugger we're gonna

3
00:00:09,780 --> 00:00:13,740
see how we can use the debugger to

4
00:00:11,940 --> 00:00:15,540
confirm our mental model and make sure

5
00:00:13,740 --> 00:00:17,100
we don't have to worry about timing

6
00:00:15,540 --> 00:00:19,500
issues to win the race and actually

7
00:00:17,100 --> 00:00:21,240
force winning the race for doing so

8
00:00:19,500 --> 00:00:23,760
we're gonna have to see how we can free

9
00:00:21,240 --> 00:00:26,699
an enlistment in order to trigger the

10
00:00:23,760 --> 00:00:28,680
use-after-free okay let's get started so

11
00:00:26,699 --> 00:00:31,260
from the previous lab we know that we

12
00:00:28,680 --> 00:00:35,100
can hit the vulnerable code and so you

13
00:00:31,260 --> 00:00:37,200
know this is very very far off from the

14
00:00:35,100 --> 00:00:39,300
next goal which is triggering that

15
00:00:37,200 --> 00:00:41,520
actual vulnerability which we know is a

16
00:00:39,300 --> 00:00:44,219
race condition that we don't really know

17
00:00:41,520 --> 00:00:47,340
how to trigger yet and so our approach

18
00:00:44,219 --> 00:00:49,680
to exploiting race conditions has always

19
00:00:47,340 --> 00:00:52,860
been based off triggering its in the

20
00:00:49,680 --> 00:00:55,320
debugger first where you basically force

21
00:00:52,860 --> 00:00:57,660
the race condition to be won and that's

22
00:00:55,320 --> 00:01:00,719
basically what we'll do here and that's

23
00:00:57,660 --> 00:01:02,460
exactly how we approached it while

24
00:01:00,719 --> 00:01:05,339
exploiting the vulnerability in the

25
00:01:02,460 --> 00:01:08,100
first place before even building the

26
00:01:05,339 --> 00:01:11,040
course because it helps proving your

27
00:01:08,100 --> 00:01:13,260
theory of what the race condition is and

28
00:01:11,040 --> 00:01:15,479
that it is actually real and so we'll

29
00:01:13,260 --> 00:01:17,280
use WinDbg to do that we know from the

30
00:01:15,479 --> 00:01:19,860
code analysis we did in a previous video

31
00:01:17,280 --> 00:01:23,400
that the race window is between the time

32
00:01:19,860 --> 00:01:26,460
it checks for that finalized flag on the

33
00:01:23,400 --> 00:01:29,100
enlistment and when it is able to lock

34
00:01:26,460 --> 00:01:31,259
the resource manager mutex and so

35
00:01:29,100 --> 00:01:33,360
basically what you can do is you can

36
00:01:31,259 --> 00:01:35,820
single step inside of the vulnerable

37
00:01:33,360 --> 00:01:38,220
function named TmRecoverResourceManager

38
00:01:35,820 --> 00:01:41,100
that we were previously looking

39
00:01:38,220 --> 00:01:43,259
at until you know you are inside of the

40
00:01:41,100 --> 00:01:45,780
loop and then you can basically step

41
00:01:43,259 --> 00:01:48,479
until the point that the finalized flag

42
00:01:45,780 --> 00:01:51,180
will have been checked and so once that

43
00:01:48,479 --> 00:01:53,340
code is passed then you can patch out

44
00:01:51,180 --> 00:01:55,979
the code that you would normally call

45
00:01:53,340 --> 00:01:58,380
for the KeWaitForSingleObject

46
00:01:55,979 --> 00:02:01,500
function to lock the resource manager

47
00:01:58,380 --> 00:02:04,140
mutex and so instead you can patch the

48
00:02:01,500 --> 00:02:06,840
instructions with an info infinite loop

49
00:02:04,140 --> 00:02:08,640
and basically it will never block the

50
00:02:06,840 --> 00:02:11,340
resource manager and you can do whatever

51
00:02:08,640 --> 00:02:13,500
you want on a different processor to try

52
00:02:11,340 --> 00:02:15,900
to free the enlistment and do whatever

53
00:02:13,500 --> 00:02:18,300
to prove that the this is the actual

54
00:02:15,900 --> 00:02:21,180
race problem and the naive way to do

55
00:02:18,300 --> 00:02:23,760
that is just to try to free all of the

56
00:02:21,180 --> 00:02:26,220
enlistments because at first you don't

57
00:02:23,760 --> 00:02:28,980
necessarily know from userland which

58
00:02:26,220 --> 00:02:30,959
enlistment you are looking at so

59
00:02:28,980 --> 00:02:33,480
basically the idea is that we have the

60
00:02:30,959 --> 00:02:36,060
recovery thread that we are going to be

61
00:02:33,480 --> 00:02:38,760
working in and which is what you are

62
00:02:36,060 --> 00:02:40,800
just debugging in this

63
00:02:38,760 --> 00:02:43,319
TmRecoverResourceManager function and so you

64
00:02:40,800 --> 00:02:44,940
call the recover resource manager

65
00:02:43,319 --> 00:02:47,099
function from userland from that

66
00:02:44,940 --> 00:02:49,200
thread and it will trap to kernel and

67
00:02:47,099 --> 00:02:51,300
reach that vulnerable function and it

68
00:02:49,200 --> 00:02:53,160
will then start looping on all of the

69
00:02:51,300 --> 00:02:54,840
enlistments associated with the

70
00:02:53,160 --> 00:02:57,300
resource manager and so at the beginning

71
00:02:54,840 --> 00:03:00,900
of that loop the resource manager object

72
00:02:57,300 --> 00:03:03,599
gets locked then the code in the loop

73
00:03:00,900 --> 00:03:06,840
tests whether or not the enlistment and

74
00:03:03,599 --> 00:03:09,300
is notifiable and finalized and because

75
00:03:06,840 --> 00:03:12,300
of our preparation from userland it

76
00:03:09,300 --> 00:03:14,760
will be notifiable and not yet finalized

77
00:03:12,300 --> 00:03:16,920
and so it won't skip that an enlistment

78
00:03:14,760 --> 00:03:19,500
and it will continue processing that

79
00:03:16,920 --> 00:03:22,800
enlistment in the loop before looping

80
00:03:19,500 --> 00:03:26,040
again then the code unlocks the resource

81
00:03:22,800 --> 00:03:28,860
manager mutex normally because it queues a

82
00:03:26,040 --> 00:03:31,379
notification to the notification queue

83
00:03:28,860 --> 00:03:33,420
that another thread in kernel is

84
00:03:31,379 --> 00:03:36,000
supposed to handle but because in the

85
00:03:33,420 --> 00:03:39,180
debugger we are going to patch it it

86
00:03:36,000 --> 00:03:42,360
will never relock the mutex and so we'll

87
00:03:39,180 --> 00:03:45,000
be giving you a special WinDbg helper

88
00:03:42,360 --> 00:03:47,580
script written in JavaScript that we

89
00:03:45,000 --> 00:03:51,360
wrote so that you can just type this

90
00:03:47,580 --> 00:03:53,819
command !patch and it will do it for

91
00:03:51,360 --> 00:03:56,580
you because you might end up having to

92
00:03:53,819 --> 00:03:59,040
do it a bunch of times but basically the

93
00:03:56,580 --> 00:04:01,799
idea is that this recovery thread which

94
00:03:59,040 --> 00:04:05,340
you called recover resource manager from

95
00:04:01,799 --> 00:04:07,200
userland is now stuck into

96
00:04:05,340 --> 00:04:10,439
TmRecoveryResourceManager in kernel

97
00:04:07,200 --> 00:04:13,200
forever and that thread will be in this

98
00:04:10,439 --> 00:04:16,139
blocking state so nothing else can

99
00:04:13,200 --> 00:04:18,780
happen until you reboot the machine or

100
00:04:16,139 --> 00:04:21,479
unpatch the memory basically to restore

101
00:04:18,780 --> 00:04:24,419
normal execution so then the idea is

102
00:04:21,479 --> 00:04:27,780
that we want to free the enlistment

103
00:04:24,419 --> 00:04:30,540
somehow and so we have this concept of

104
00:04:27,780 --> 00:04:33,240
assisting thread which is basically just

105
00:04:30,540 --> 00:04:36,060
another thread on a different CPU core

106
00:04:33,240 --> 00:04:38,400
and in the lab code this assisting

107
00:04:36,060 --> 00:04:40,919
thread will just be waiting for you to

108
00:04:38,400 --> 00:04:43,979
press enter and when you do it it will

109
00:04:40,919 --> 00:04:46,440
try to commit all of the enlistments and

110
00:04:43,979 --> 00:04:49,380
close the handles of all the enlistments

111
00:04:46,440 --> 00:04:51,360
too and so in kernel it should free the

112
00:04:49,380 --> 00:04:54,540
enlistments and you will end up having

113
00:04:51,360 --> 00:04:57,060
this case where you now have freed the

114
00:04:54,540 --> 00:04:59,820
enlistments and the vulnerable code is

115
00:04:57,060 --> 00:05:02,160
still awaiting in that stuck mode but

116
00:04:59,820 --> 00:05:04,440
there it's still holding a pointer to

117
00:05:02,160 --> 00:05:06,240
one of the enlist and then basically

118
00:05:04,440 --> 00:05:08,400
once you are sure that the enlistment

119
00:05:06,240 --> 00:05:10,620
is freed and you get confirmation from

120
00:05:08,400 --> 00:05:13,199
the console that it has happened you can

121
00:05:10,620 --> 00:05:16,139
just break back inside of WinDbg and

122
00:05:13,199 --> 00:05:18,840
type the !unpatch command which we

123
00:05:16,139 --> 00:05:22,020
also provide and then it will basically

124
00:05:18,840 --> 00:05:24,780
remove the infinite loop and put the

125
00:05:22,020 --> 00:05:26,940
mutex code back and when you continue

126
00:05:24,780 --> 00:05:29,639
execution in the debugger the recover

127
00:05:26,940 --> 00:05:31,139
thread will continue executing in

128
00:05:29,639 --> 00:05:33,960
developer function and it will

129
00:05:31,139 --> 00:05:36,360
definitely trigger the use-after-free you

130
00:05:33,960 --> 00:05:38,940
know if it matches your assumption about

131
00:05:36,360 --> 00:05:41,460
the bug being real which obviously it is

132
00:05:38,940 --> 00:05:43,500
for the purpose of this course and then

133
00:05:41,460 --> 00:05:45,780
depending on how that memory has been

134
00:05:43,500 --> 00:05:48,180
reused it may trigger an access

135
00:05:45,780 --> 00:05:50,759
violation or whatever it will actually

136
00:05:48,180 --> 00:05:53,820
publish this depend some of you might

137
00:05:50,759 --> 00:05:56,759
end up seeing a bsod AKA blue screen of

138
00:05:53,820 --> 00:06:00,180
death that memory has been reused and

139
00:05:56,759 --> 00:06:02,400
invalid data has replaced fields that

140
00:06:00,180 --> 00:06:05,580
are now reused in the loop some of you

141
00:06:02,400 --> 00:06:08,340
might end up seeing the code continue okay if

142
00:06:05,580 --> 00:06:11,100
that memory hasn't been reused yet and

143
00:06:08,340 --> 00:06:13,500
the old data is still there in the freed

144
00:06:11,100 --> 00:06:15,720
enlistment so basically what

145
00:06:13,500 --> 00:06:18,240
that will do is simulate this

146
00:06:15,720 --> 00:06:20,400
approximate idea of what we had about

147
00:06:18,240 --> 00:06:22,680
simulating winning the race condition

148
00:06:20,400 --> 00:06:26,240
and consequently triggering the use

149
00:06:22,680 --> 00:06:26,240
after free state on the Enlistment

