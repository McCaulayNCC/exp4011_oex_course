1
00:00:00,240 --> 00:00:07,259
so the goal of the next lab is actually

2
00:00:02,580 --> 00:00:08,580
to commit a transaction that has several

3
00:00:07,259 --> 00:00:11,280
enlistments

4
00:00:08,580 --> 00:00:13,799
and so to see what is happening when the

5
00:00:11,280 --> 00:00:16,440
enlistments change state from

6
00:00:13,799 --> 00:00:18,420
pre-prepared to prepared and then to

7
00:00:16,440 --> 00:00:20,460
committed which makes it the whole

8
00:00:18,420 --> 00:00:24,420
transaction committed and so there is

9
00:00:20,460 --> 00:00:26,460
already a call to pre-prepare complete and

10
00:00:24,420 --> 00:00:29,220
so you're going to need to actually do

11
00:00:26,460 --> 00:00:30,359
the same with prepare complete and commit

12
00:00:29,220 --> 00:00:33,600
complete

13
00:00:30,359 --> 00:00:36,180
to change these enlistments into the

14
00:00:33,600 --> 00:00:39,840
additional states of prepared and

15
00:00:36,180 --> 00:00:42,600
committed and then in kernel we'll set

16
00:00:39,840 --> 00:00:45,899
breakpoints to see when the enlistments

17
00:00:42,600 --> 00:00:48,180
are created when they actually change

18
00:00:45,899 --> 00:00:51,840
states it is to say when we call

19
00:00:48,180 --> 00:00:53,940
pre-prepare complete in userland that

20
00:00:51,840 --> 00:00:56,579
it actually calls TmPrePrepareCompleteEx

21
00:00:53,940 --> 00:00:59,699
in kernel and the same thing

22
00:00:56,579 --> 00:01:01,620
for prepare complete and commit completes

23
00:00:59,699 --> 00:01:04,920
the whole point is going to be to

24
00:01:01,620 --> 00:01:07,260
analyze the transaction object in kernel

25
00:01:04,920 --> 00:01:09,240
which is the KTRANSACTION structure and

26
00:01:07,260 --> 00:01:12,299
see the state Transitions and the same

27
00:01:09,240 --> 00:01:13,799
for the enlistment
which is the KENLISTMENT

28
00:01:12,299 --> 00:01:16,080
structure we're going to work with two

29
00:01:13,799 --> 00:01:17,760
enlistments as a way to analyze these

30
00:01:16,080 --> 00:01:20,640
two enlistments and the final

31
00:01:17,760 --> 00:01:23,820
transaction also we'll see that the code

32
00:01:20,640 --> 00:01:27,299
that you provide actually has functions

33
00:01:23,820 --> 00:01:29,220
to monitor notifications when the

34
00:01:27,299 --> 00:01:31,320
changes appear and so we'll be able to

35
00:01:29,220 --> 00:01:33,840
see that in userland okay let's get

36
00:01:31,320 --> 00:01:36,840
started so let's have a look at the

37
00:01:33,840 --> 00:01:39,540
CommitMultiEnTx.c

38
00:01:36,840 --> 00:01:42,659
file so it imports the KTM header

39
00:01:39,540 --> 00:01:45,299
already it has a special mask for

40
00:01:42,659 --> 00:01:47,340
notification it has a structure for

41
00:01:45,299 --> 00:01:50,460
notification that we're going to use

42
00:01:47,340 --> 00:01:54,119
later it has a helper function to print

43
00:01:50,460 --> 00:01:55,740
a GUID such as the actual integers that

44
00:01:54,119 --> 00:01:58,140
are part of the GUID the identifier

45
00:01:55,740 --> 00:02:00,360
global identifier then we have a helper

46
00:01:58,140 --> 00:02:03,060
function that takes an integer which

47
00:02:00,360 --> 00:02:06,060
represents one of these macros and just

48
00:02:03,060 --> 00:02:08,280
print the description the name of the of

49
00:02:06,060 --> 00:02:10,739
the actual notification so for instance

50
00:02:08,280 --> 00:02:14,040
when we'll receive a notification for

51
00:02:10,739 --> 00:02:16,760
prepare state we'll be able to print

52
00:02:14,040 --> 00:02:19,739
transaction notify prepare so then

53
00:02:16,760 --> 00:02:23,160
you'll notice in all the labs in this

54
00:02:19,739 --> 00:02:25,920
course that we use the X prefix to

55
00:02:23,160 --> 00:02:27,959
indicate a wrapper so this is an API

56
00:02:25,920 --> 00:02:29,760
that is public

57
00:02:27,959 --> 00:02:32,520
so if we look for

58
00:02:29,760 --> 00:02:35,280
get notification resource manager here

59
00:02:32,520 --> 00:02:38,640
the NT is just because it's actually a

60
00:02:35,280 --> 00:02:40,560
syscall so this API in Windows will

61
00:02:38,640 --> 00:02:42,959
actually call the syscall

62
00:02:40,560 --> 00:02:45,900
but basically the whole point of this

63
00:02:42,959 --> 00:02:47,400
function is to retrieve the next

64
00:02:45,900 --> 00:02:49,260
transaction notification from a

65
00:02:47,400 --> 00:02:51,540
specified resource manager from the

66
00:02:49,260 --> 00:02:54,360
notification queue so we can see we pass

67
00:02:51,540 --> 00:02:55,739
a resource manager handle and it's going

68
00:02:54,360 --> 00:02:58,800
to actually fill the transaction

69
00:02:55,739 --> 00:03:00,840
notification structure so this is the

70
00:02:58,800 --> 00:03:03,239
handle to the resource manager this is a

71
00:03:00,840 --> 00:03:05,040
pointer to a allocated buffer that

72
00:03:03,239 --> 00:03:07,080
received information about the retrieve

73
00:03:05,040 --> 00:03:09,379
notification we see the size of the

74
00:03:07,080 --> 00:03:12,120
previous structure we specify a timeout

75
00:03:09,379 --> 00:03:14,340
that will be used if there is no

76
00:03:12,120 --> 00:03:17,220
notification in the queue it will just

77
00:03:14,340 --> 00:03:21,300
time out and exit so typically we would

78
00:03:17,220 --> 00:03:25,260
use one second so 1000 millisecond

79
00:03:21,300 --> 00:03:27,659
and we can use additional arguments

80
00:03:25,260 --> 00:03:29,879
which are not really relevant here so

81
00:03:27,659 --> 00:03:31,560
basically here we're providing the

82
00:03:29,879 --> 00:03:33,720
resource manager handle

83
00:03:31,560 --> 00:03:36,959
the transaction notification structure

84
00:03:33,720 --> 00:03:39,239
and this x get notification resource

85
00:03:36,959 --> 00:03:41,400
manager is just a wrapper around this

86
00:03:39,239 --> 00:03:43,260
known API get notification resource

87
00:03:41,400 --> 00:03:45,720
manager and so the reason we do that in

88
00:03:43,260 --> 00:03:48,780
the code is because we handle errors

89
00:03:45,720 --> 00:03:51,659
like if it times out we can print that

90
00:03:48,780 --> 00:03:53,459
it timed out and we can return an error

91
00:03:51,659 --> 00:03:56,819
saying okay it means there is basically

92
00:03:53,459 --> 00:03:58,200
no notification anymore so in our in our

93
00:03:56,819 --> 00:04:00,959
case it's not really an error it's just

94
00:03:58,200 --> 00:04:03,239
say saying okay we've we empty the queue

95
00:04:00,959 --> 00:04:07,200
there's nothing to read anymore and then

96
00:04:03,239 --> 00:04:09,299
we print then what with notification it

97
00:04:07,200 --> 00:04:10,200
is so we use the actual function we've

98
00:04:09,299 --> 00:04:11,939
just seen

99
00:04:10,200 --> 00:04:13,739
which actually

100
00:04:11,939 --> 00:04:16,040
take an integer and return the

101
00:04:13,739 --> 00:04:16,040
description

102
00:04:19,019 --> 00:04:24,120
so this function

103
00:04:21,840 --> 00:04:26,820
is xGetNotificationResourceManager

104
00:04:24,120 --> 00:04:28,919
and this is used into a loop

105
00:04:26,820 --> 00:04:29,940
in get notification resource manager all

106
00:04:28,919 --> 00:04:32,580
available

107
00:04:29,940 --> 00:04:34,680
and this function takes again the

108
00:04:32,580 --> 00:04:36,720
resource manager handle a max number of

109
00:04:34,680 --> 00:04:40,199
attempts so for instance we want to say

110
00:04:36,720 --> 00:04:42,960
oh we want to read up to 10

111
00:04:40,199 --> 00:04:45,180
notifications potentially so it's going

112
00:04:42,960 --> 00:04:48,600
to call our X wrapper

113
00:04:45,180 --> 00:04:51,419
up to 10 times and we specify the

114
00:04:48,600 --> 00:04:53,580
timeout and if one of them at some point

115
00:04:51,419 --> 00:04:56,220
fails it means their identification

116
00:04:53,580 --> 00:04:57,960
queue is empty and basically B rate will

117
00:04:56,220 --> 00:04:59,940
return false and we just exit the loop

118
00:04:57,960 --> 00:05:02,759
otherwise we'll keep looping up to 10

119
00:04:59,940 --> 00:05:05,220
times okay so now we can analyze our

120
00:05:02,759 --> 00:05:08,280
main function so we have handle being

121
00:05:05,220 --> 00:05:09,479
defined we have a default GUID for the

122
00:05:08,280 --> 00:05:13,080
resource manager and then we have

123
00:05:09,479 --> 00:05:15,660
instructions for the actual lab so again

124
00:05:13,080 --> 00:05:17,280
same kind of skeleton we create a

125
00:05:15,660 --> 00:05:19,320
transaction manager we recover the

126
00:05:17,280 --> 00:05:21,419
transaction manager we create a resource

127
00:05:19,320 --> 00:05:23,280
manager that is volatile we recover the

128
00:05:21,419 --> 00:05:25,380
resource manager and then we create a

129
00:05:23,280 --> 00:05:27,780
transaction so here we can see two

130
00:05:25,380 --> 00:05:29,639
enlistments being created that are going

131
00:05:27,780 --> 00:05:31,500
to be part of the same transaction and

132
00:05:29,639 --> 00:05:34,259
for each enlistment we're going to

133
00:05:31,500 --> 00:05:35,880
basically retrieve its GUID using the

134
00:05:34,259 --> 00:05:39,440
get enlistment ID

135
00:05:35,880 --> 00:05:39,440
which is a known API

136
00:05:42,479 --> 00:05:45,660
obtain the identifier of the specified

137
00:05:44,400 --> 00:05:48,360
enlistment it's going to fill the

138
00:05:45,660 --> 00:05:50,520
enlistment ID based on the handle

139
00:05:48,360 --> 00:05:52,680
this is handy because in kernel would be

140
00:05:50,520 --> 00:05:55,020
able to match which enlistment is which

141
00:05:52,680 --> 00:05:57,419
because we can compare it to userland

142
00:05:55,020 --> 00:06:00,960
so this is just printing the GUID in a

143
00:05:57,419 --> 00:06:05,460
beautiful user-friendly manner then we

144
00:06:00,960 --> 00:06:07,680
call the commit transaction async

145
00:06:05,460 --> 00:06:09,300
function which is not documented but

146
00:06:07,680 --> 00:06:11,699
basically it's going to tell the

147
00:06:09,300 --> 00:06:14,280
transaction okay now we are we want to

148
00:06:11,699 --> 00:06:17,460
actually wait until the transaction is

149
00:06:14,280 --> 00:06:20,280
committed and now we go into a state

150
00:06:17,460 --> 00:06:22,440
where we're going to change the state of

151
00:06:20,280 --> 00:06:24,479
the different enlistments so first we

152
00:06:22,440 --> 00:06:26,520
read the default

153
00:06:24,479 --> 00:06:28,380
enlistment notification state so this

154
00:06:26,520 --> 00:06:30,800
function as we saw earlier is going to

155
00:06:28,380 --> 00:06:33,180
read all the notification

156
00:06:30,800 --> 00:06:36,300
from the queue

157
00:06:33,180 --> 00:06:38,759
up to the max attempts

158
00:06:36,300 --> 00:06:42,060
so in our case it's going to read up to

159
00:06:38,759 --> 00:06:43,979
10 notification so we're going to read

160
00:06:42,060 --> 00:06:45,300
up to 10 notification just to empty the

161
00:06:43,979 --> 00:06:48,240
queue then we're going to change the

162
00:06:45,300 --> 00:06:50,639
state of the enlistment both of them the

163
00:06:48,240 --> 00:06:55,080
pre-prepare so it's going to go from the

164
00:06:50,639 --> 00:06:57,120
pre-prepare state to the prepared state and

165
00:06:55,080 --> 00:06:58,919
then to see it in userland we're

166
00:06:57,120 --> 00:07:01,319
gonna again read all the notifications

167
00:06:58,919 --> 00:07:03,419
and we see we use the get character

168
00:07:01,319 --> 00:07:05,880
function to wait so we're going to be

169
00:07:03,419 --> 00:07:07,139
able to break the debugger see what

170
00:07:05,880 --> 00:07:09,300
happens what are the different

171
00:07:07,139 --> 00:07:11,039
structures fields being updated and then

172
00:07:09,300 --> 00:07:13,380
we're going to be able to continue the

173
00:07:11,039 --> 00:07:15,840
debugger and then hit the key in the

174
00:07:13,380 --> 00:07:18,180
target VM to be able to continue and see

175
00:07:15,840 --> 00:07:21,660
each step so what we need to do

176
00:07:18,180 --> 00:07:24,720
basically is add a call to prepare

177
00:07:21,660 --> 00:07:28,819
complete and commit completes which are

178
00:07:24,720 --> 00:07:28,819
two functions to actually

179
00:07:29,520 --> 00:07:34,560
change States so we can see it's not

180
00:07:32,280 --> 00:07:37,800
actually documented on Google at least

181
00:07:34,560 --> 00:07:39,660
but if we look for prepare complete

182
00:07:37,800 --> 00:07:41,699
Maybe

183
00:07:39,660 --> 00:07:44,099
okay you you could you could actually

184
00:07:41,699 --> 00:07:46,460
look for them on the msdn on the kernel

185
00:07:44,099 --> 00:07:46,460
transaction

186
00:07:48,660 --> 00:07:51,319
manager

187
00:07:52,440 --> 00:07:56,240
so you would be able to find them here

188
00:07:57,060 --> 00:08:01,740
but the whole thing is it's basically

189
00:07:59,220 --> 00:08:03,960
the same

190
00:08:01,740 --> 00:08:06,300
but it's basically the same prototype as

191
00:08:03,960 --> 00:08:10,199
pre prepare complete so you can see here

192
00:08:06,300 --> 00:08:12,599
in ktmw32.h that you have

193
00:08:10,199 --> 00:08:16,139
PrePrepareComplete, PrepareComplete,

194
00:08:12,599 --> 00:08:18,680
CommitComplete so these functions are

195
00:08:16,139 --> 00:08:18,680
defined

196
00:08:18,720 --> 00:08:24,060
so you need to add two calls one for

197
00:08:21,660 --> 00:08:25,139
prepare complete and one for commit

198
00:08:24,060 --> 00:08:27,360
completes

199
00:08:25,139 --> 00:08:29,699
in here and each time you call this

200
00:08:27,360 --> 00:08:32,520
function for the two enlistments you can

201
00:08:29,699 --> 00:08:35,159
read the actual notification from the

202
00:08:32,520 --> 00:08:37,560
queue and see what happens the whole

203
00:08:35,159 --> 00:08:40,560
point is to actually commit all the

204
00:08:37,560 --> 00:08:44,539
enlistments and as a consequence make

205
00:08:40,560 --> 00:08:44,539
the transaction committed as well

206
00:08:46,020 --> 00:08:49,640
okay it's your turn now

