1
00:00:00,120 --> 00:00:08,220
so the four KTM objects are noted TM, RM,

2
00:00:04,740 --> 00:00:10,860
Tx, En which stands for transaction

3
00:00:08,220 --> 00:00:13,799
manager, resource manager, transaction and

4
00:00:10,860 --> 00:00:15,540
enlistment and so it's important to just

5
00:00:13,799 --> 00:00:18,779
know roughly that there are four KTM

6
00:00:15,540 --> 00:00:21,000
objects so the transaction manager which

7
00:00:18,779 --> 00:00:22,740
is basically the manager and it's

8
00:00:21,000 --> 00:00:25,740
responsible for managing the transaction

9
00:00:22,740 --> 00:00:28,199
there is the resource manager which is

10
00:00:25,740 --> 00:00:30,720
responsible to handle the resource so

11
00:00:28,199 --> 00:00:32,520
the registry or the file system and then

12
00:00:30,720 --> 00:00:34,620
there are two very important objects

13
00:00:32,520 --> 00:00:36,840
which are the ones related to the

14
00:00:34,620 --> 00:00:40,379
vulnerability which are the transaction

15
00:00:36,840 --> 00:00:42,540
and the enlistment transaction because

16
00:00:40,379 --> 00:00:44,340
we are dealing with the KTM components

17
00:00:42,540 --> 00:00:47,160
which stands for kernel transaction

18
00:00:44,340 --> 00:00:50,399
manager so we're dealing with atomic

19
00:00:47,160 --> 00:00:52,500
operations called transactions and the

20
00:00:50,399 --> 00:00:55,559
enlistments being a specific object

21
00:00:52,500 --> 00:00:57,660
which is related to enlist into a

22
00:00:55,559 --> 00:00:59,640
specific transaction but in our case

23
00:00:57,660 --> 00:01:01,680
it's very important because the

24
00:00:59,640 --> 00:01:03,960
enlistments is the object that is part

25
00:01:01,680 --> 00:01:05,700
of the use-after-free so it's even more

26
00:01:03,960 --> 00:01:06,900
important to have this particular object

27
00:01:05,700 --> 00:01:08,640
in mind

28
00:01:06,900 --> 00:01:12,500
because this is the vulnerable object

29
00:01:08,640 --> 00:01:12,500
that we are going to use-after-free

