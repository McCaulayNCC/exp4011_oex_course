1
00:00:00,780 --> 00:00:05,400
hi everyone in this part you're going to

2
00:00:03,480 --> 00:00:08,160
be the one who does the job you're going

3
00:00:05,400 --> 00:00:10,740
to start by importing the KTM structures

4
00:00:08,160 --> 00:00:12,960
into your project then you'll be able to

5
00:00:10,740 --> 00:00:15,839
improve the reverse engineering of the

6
00:00:12,960 --> 00:00:18,960
vulnerable function in both the vulnerable

7
00:00:15,839 --> 00:00:21,180
file and the patch file and finally

8
00:00:18,960 --> 00:00:23,400
you're going to be able to have a better

9
00:00:21,180 --> 00:00:26,640
binary diffing between the two versions

10
00:00:23,400 --> 00:00:30,180
okay let's get started so the goal of

11
00:00:26,640 --> 00:00:32,700
the lab is to import the KTM types.h

12
00:00:30,180 --> 00:00:35,340
header file that has lots of KTM

13
00:00:32,700 --> 00:00:37,020
structures and to import it in Ghidra for

14
00:00:35,340 --> 00:00:39,120
both the patched file and the vulnerable

15
00:00:37,020 --> 00:00:41,579
file and we're going to do it for the

16
00:00:39,120 --> 00:00:44,160
version of the files that are from the

17
00:00:41,579 --> 00:00:46,620
basic bin diffing where not much has

18
00:00:44,160 --> 00:00:49,260
been defined already and the goal is is

19
00:00:46,620 --> 00:00:51,840
going to reach a good state of reversing

20
00:00:49,260 --> 00:00:54,480
similar to what is in
the better bin diffing

21
00:00:51,840 --> 00:00:57,780
 folder in Ghidra and so once you've

22
00:00:54,480 --> 00:01:00,420
imported the ktmtypes.h

23
00:00:57,780 --> 00:01:02,760
file you can define all the types and

24
00:01:00,420 --> 00:01:05,159
more specifically you can use the the

25
00:01:02,760 --> 00:01:08,340
KENLISTMENT shifted next same RM type

26
00:01:05,159 --> 00:01:11,159
that is useful for any shifted pointer

27
00:01:08,340 --> 00:01:13,560
you can also Define additional function

28
00:01:11,159 --> 00:01:15,900
prototypes if appropriate and after you

29
00:01:13,560 --> 00:01:18,060
have a good documented code for both the

30
00:01:15,900 --> 00:01:21,900
patch file and the vulnerable file you can

31
00:01:18,060 --> 00:01:24,479
basically copy the C decompiled

32
00:01:21,900 --> 00:01:26,640
output into two different files for both

33
00:01:24,479 --> 00:01:29,159
the patch file and the vulnerable file and

34
00:01:26,640 --> 00:01:31,140
diff them with a tool like win merge and

35
00:01:29,159 --> 00:01:33,119
you'll see that it's a lot easier to

36
00:01:31,140 --> 00:01:35,640
understand not only the vulnerable code

37
00:01:33,119 --> 00:01:38,159
but also what the patch is trying to

38
00:01:35,640 --> 00:01:39,979
achieve okay now it's your turn so we

39
00:01:38,159 --> 00:01:45,439
provide a KTM

40
00:01:39,979 --> 00:01:45,439
types.h in the tools reverse folder

41
00:01:46,979 --> 00:01:53,759
so it contains a lot of structures you

42
00:01:50,399 --> 00:01:57,860
can see it contain the kpcr

43
00:01:53,759 --> 00:01:57,860
but also all the KTM structures

44
00:01:58,680 --> 00:02:02,460
so for instance if we look for the

45
00:02:00,479 --> 00:02:06,240
struct KENLISTMENT

46
00:02:02,460 --> 00:02:10,259
we're gonna see it's a structure

47
00:02:06,240 --> 00:02:12,660
taken from Vergilius of 100 and e0 hex

48
00:02:10,259 --> 00:02:15,180
bytes also we see that one field has

49
00:02:12,660 --> 00:02:18,260
been changed to an actual enum the

50
00:02:15,180 --> 00:02:18,260
enlistment flags

51
00:02:18,300 --> 00:02:23,400
because it has been reversed and we kind

52
00:02:20,819 --> 00:02:25,520
of know what the different enums are

53
00:02:23,400 --> 00:02:25,520
about

54
00:02:27,120 --> 00:02:33,000
we also see the actual shifted structure

55
00:02:30,120 --> 00:02:34,620
starting with the next same RM field and

56
00:02:33,000 --> 00:02:36,599
the other fields at the end so we are

57
00:02:34,620 --> 00:02:39,239
going to load both the vulnerable file

58
00:02:36,599 --> 00:02:41,959
as well as the patch file from the basic

59
00:02:39,239 --> 00:02:41,959
bin diffing

60
00:02:43,019 --> 00:02:46,800
and so the goal is going to be to

61
00:02:45,180 --> 00:02:48,420
reverse engineer the TM recovery

62
00:02:46,800 --> 00:02:50,700
resource manager function for both of

63
00:02:48,420 --> 00:02:52,739
these files so we've done it already but

64
00:02:50,700 --> 00:02:56,160
now we're going to load the header file

65
00:02:52,739 --> 00:02:57,540
so we're going to go into file parse C

66
00:02:56,160 --> 00:03:00,239
source file and then we're going to

67
00:02:57,540 --> 00:03:01,739
select the Visual Studio for 64-bit so

68
00:03:00,239 --> 00:03:03,780
we can see there are lots of header

69
00:03:01,739 --> 00:03:06,120
files already related to Windows we're

70
00:03:03,780 --> 00:03:09,959
going to click on the plus sign and look

71
00:03:06,120 --> 00:03:12,900
for the tools, reverse, ktmtypes.h so

72
00:03:09,959 --> 00:03:14,580
after hitting OK we can see it's at the

73
00:03:12,900 --> 00:03:16,200
bottom now it's part of the new

74
00:03:14,580 --> 00:03:18,000
configuration and we're going to click

75
00:03:16,200 --> 00:03:19,560
on parse to program it's going to tell

76
00:03:18,000 --> 00:03:23,300
us that it's going to import that to the

77
00:03:19,560 --> 00:03:23,300
dot sys for the TM vuln

78
00:03:23,760 --> 00:03:28,739
it's basically telling us that the new

79
00:03:26,220 --> 00:03:31,319
configuration and data type that we're

80
00:03:28,739 --> 00:03:35,959
importing are related to the existing

81
00:03:31,319 --> 00:03:35,959
Windows vs 1264 which we agree

82
00:03:38,760 --> 00:03:43,560
so there are a couple of errors but it

83
00:03:41,459 --> 00:03:45,720
should be fine

84
00:03:43,560 --> 00:03:49,040
so now we can close that and if we look

85
00:03:45,720 --> 00:03:49,040
for KRESOURCEMANAGER

86
00:03:56,519 --> 00:04:01,920
so we can see actually the previous one

87
00:03:59,280 --> 00:04:05,420
we had defined with the strings is still

88
00:04:01,920 --> 00:04:05,420
there so we're going to delete it

89
00:04:07,260 --> 00:04:11,459
and by doing so you'll see that the

90
00:04:09,239 --> 00:04:13,319
actual type here will be undefined

91
00:04:11,459 --> 00:04:14,879
because it used to be the KRESOURCEMANAGER

92
00:04:13,319 --> 00:04:17,519
so now we only have one

93
00:04:14,879 --> 00:04:20,479
remaining so we're going to go here and

94
00:04:17,519 --> 00:04:20,479
retype the variable

95
00:04:20,519 --> 00:04:26,940
to the actual KRESOURCEMANAGER

96
00:04:23,940 --> 00:04:30,919
it should be the one from tm_vuln.sys that

97
00:04:26,940 --> 00:04:30,919
says ktmtypes.h

98
00:04:31,800 --> 00:04:37,320
so once you do that you should see a

99
00:04:34,860 --> 00:04:40,080
couple of new interesting things which

100
00:04:37,320 --> 00:04:42,060
is that lots of the states and flags

101
00:04:40,080 --> 00:04:44,220
have been defined as enums so

102
00:04:42,060 --> 00:04:47,040
automatically it's going to replace the

103
00:04:44,220 --> 00:04:48,840
actual numbers with the actual enum so

104
00:04:47,040 --> 00:04:50,940
here it's testing the state is offline

105
00:04:48,840 --> 00:04:54,560
or online here it's testing the states

106
00:04:50,940 --> 00:04:54,560
of the transaction manager

107
00:04:55,139 --> 00:04:57,320


108
00:04:59,520 --> 00:05:04,320
and here's testing that the state is

109
00:05:01,860 --> 00:05:06,960
online so now you should have other

110
00:05:04,320 --> 00:05:09,199
structures like for instance the

111
00:05:06,960 --> 00:05:09,199
KENLISTMENT

112
00:05:09,900 --> 00:05:13,639
as well as the shifted one

113
00:05:13,740 --> 00:05:20,340
so if we look at them

114
00:05:15,900 --> 00:05:23,340
we see there are size 1 e0

115
00:05:20,340 --> 00:05:26,419
and the shifted one

116
00:05:23,340 --> 00:05:26,419
is the same size

