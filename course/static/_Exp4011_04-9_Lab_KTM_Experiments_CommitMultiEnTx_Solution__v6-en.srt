1
00:00:00,480 --> 00:00:05,759
so we haven't modified the

2
00:00:02,879 --> 00:00:07,259
CommitMultiEnTx.c file yet

3
00:00:05,759 --> 00:00:10,760
but we're going to basically set the

4
00:00:07,259 --> 00:00:10,760
breakpoints on the different functions

5
00:00:31,800 --> 00:00:37,020
and we're going to continue execution

6
00:00:34,739 --> 00:00:41,059
so we're going to build the actual

7
00:00:37,020 --> 00:00:41,059
sample unmodified at the moment

8
00:00:44,040 --> 00:00:48,140
so it's been copied on the target VM

9
00:00:48,360 --> 00:00:52,739
and we're going to run it

10
00:00:50,579 --> 00:00:55,739
we see it gives out the instructions and

11
00:00:52,739 --> 00:00:58,559
then it need to hit enter to execute the

12
00:00:55,739 --> 00:01:01,620
rest you can see it stops and because

13
00:00:58,559 --> 00:01:04,019
the the VM is in a blocked state due to

14
00:01:01,620 --> 00:01:06,000
the breakpoint being hit in the debugger

15
00:01:04,019 --> 00:01:07,979
so we see it hits the create enlistments

16
00:01:06,000 --> 00:01:10,439
because obviously

17
00:01:07,979 --> 00:01:12,360
the two enlistments has been created so we

18
00:01:10,439 --> 00:01:14,939
can continue execution it hits a second

19
00:01:12,360 --> 00:01:17,700
time for the second enlistment and then

20
00:01:14,939 --> 00:01:20,159
it goes into PrePrepareCompleteExt which is

21
00:01:17,700 --> 00:01:22,740
due to the fact that PrePrepareComplete

22
00:01:20,159 --> 00:01:25,619
is called in userland but we

23
00:01:22,740 --> 00:01:28,860
can see in the actual VM that we got

24
00:01:25,619 --> 00:01:31,439
notification for the different states the fact

25
00:01:28,860 --> 00:01:34,560
that when we read the GUID first

26
00:01:31,439 --> 00:01:36,360
for the 2 enlistments we have B5 and B6

27
00:01:34,560 --> 00:01:39,000
two different GUID and then when we

28
00:01:36,360 --> 00:01:41,400
read the actual queue
we got the pre prepare

29
00:01:39,000 --> 00:01:43,079
state already so we're going to continue

30
00:01:41,400 --> 00:01:45,600
execution

31
00:01:43,079 --> 00:01:47,340
it's going to hit the

32
00:01:45,600 --> 00:01:49,560
TmPrePrepareCompleteExt for the second enlistment

33
00:01:47,340 --> 00:01:52,259
and then nothing else because we haven't

34
00:01:49,560 --> 00:01:54,540
added the the other APIs and on the

35
00:01:52,259 --> 00:01:57,000
target VM we see that it went from the

36
00:01:54,540 --> 00:01:58,979
pre-prepare state to the prepared state

37
00:01:57,000 --> 00:02:01,259
and if we hit a key nothing happens

38
00:01:58,979 --> 00:02:02,700
because we haven't executed the pre prepare

39
00:02:01,259 --> 00:02:04,680
complete and the commit complete

40
00:02:02,700 --> 00:02:06,479
functions yet it's stating that the

41
00:02:04,680 --> 00:02:08,459
transaction has been committed but it's

42
00:02:06,479 --> 00:02:10,619
probably not the case as we see that we

43
00:02:08,459 --> 00:02:13,140
haven't hit the commit complete and

44
00:02:10,619 --> 00:02:15,000
prepare complete functions yet so we are

45
00:02:13,140 --> 00:02:17,819
going to add code to go from the

46
00:02:15,000 --> 00:02:20,099
pre-prepared to the prepared state and

47
00:02:17,819 --> 00:02:22,440
then from the prepared state to the

48
00:02:20,099 --> 00:02:25,200
committed state so we're going to use code

49
00:02:22,440 --> 00:02:27,060
similar to the one that we provide which

50
00:02:25,200 --> 00:02:30,060
is basically to call

51
00:02:27,060 --> 00:02:32,400
prepare complete

52
00:02:30,060 --> 00:02:34,920
for both enlistments

53
00:02:32,400 --> 00:02:37,020
so that we have moved to the prepared

54
00:02:34,920 --> 00:02:39,180
States and then

55
00:02:37,020 --> 00:02:41,400
we're going to read the notification to

56
00:02:39,180 --> 00:02:43,800
see what happened so we moved from we

57
00:02:41,400 --> 00:02:45,660
see we hit the key to call pre prepare

58
00:02:43,800 --> 00:02:49,640
complete and now we're going to hit the

59
00:02:45,660 --> 00:02:49,640
key to call commit complete

60
00:03:02,700 --> 00:03:06,840
and after we have called commit complete

61
00:03:05,160 --> 00:03:09,480
there is code already to show the

62
00:03:06,840 --> 00:03:11,400
notification okay the code looks valid

63
00:03:09,480 --> 00:03:13,560
let's test it now so we're going to push

64
00:03:11,400 --> 00:03:16,319
the code onto the target VM and we're

65
00:03:13,560 --> 00:03:18,720
gonna run it so we hit the key it

66
00:03:16,319 --> 00:03:21,000
triggers our breakpoints we see it

67
00:03:18,720 --> 00:03:24,480
scores create enlistment once the second

68
00:03:21,000 --> 00:03:27,300
time it calls PrePrepareCompleteExt

69
00:03:24,480 --> 00:03:29,099
then it calls PrePrepareCompleteExt for

70
00:03:27,300 --> 00:03:31,260
the second enlistment then it doesn't

71
00:03:29,099 --> 00:03:33,959
do anything because actually it's

72
00:03:31,260 --> 00:03:35,700
blocked in userland on each key to

73
00:03:33,959 --> 00:03:37,200
continue we have the same state as

74
00:03:35,700 --> 00:03:39,360
before with the two and this one having

75
00:03:37,200 --> 00:03:41,760
different weight and the prepared state

76
00:03:39,360 --> 00:03:43,980
being reached so now we're gonna hit the

77
00:03:41,760 --> 00:03:47,159
key again we're blocking again so here

78
00:03:43,980 --> 00:03:49,319
again we see that we hit the TmPrepareComplete

79
00:03:47,159 --> 00:03:51,659
instead of the PrePrepareComplete

80
00:03:49,319 --> 00:03:53,099
because of the change of states we

81
00:03:51,659 --> 00:03:54,840
hit that for possibly the first

82
00:03:53,099 --> 00:03:57,000
enlistment the second and then again

83
00:03:54,840 --> 00:03:59,220
nothing else it's because again it's

84
00:03:57,000 --> 00:04:01,319
blocked in userland we can see that

85
00:03:59,220 --> 00:04:03,000
after reading the queue we actually got

86
00:04:01,319 --> 00:04:05,280
a new notification related to

87
00:04:03,000 --> 00:04:07,920
transaction not if I commit

88
00:04:05,280 --> 00:04:10,260
and we're gonna hit another key to call

89
00:04:07,920 --> 00:04:13,080
commit complete again we see in kernel

90
00:04:10,260 --> 00:04:14,720
we hit TmCommitCompleteExt and the

91
00:04:13,080 --> 00:04:17,940
second time for the second enlistment

92
00:04:14,720 --> 00:04:19,799
and then nothing else and in userland we

93
00:04:17,940 --> 00:04:21,479
see that we assume the transaction is

94
00:04:19,799 --> 00:04:24,240
committed and we don't read anything

95
00:04:21,479 --> 00:04:26,460
from the queue okay so it looks valid

96
00:04:24,240 --> 00:04:28,740
from the userland perspective now we can

97
00:04:26,460 --> 00:04:30,600
debug it in kernel and analyze the

98
00:04:28,740 --> 00:04:32,699
structures before we actually debug the

99
00:04:30,600 --> 00:04:34,380
actual functions and see the structure

100
00:04:32,699 --> 00:04:37,259
is in kernel I just
want to quickly show

101
00:04:34,380 --> 00:04:38,820
that I actually renamed variables and

102
00:04:37,259 --> 00:04:41,100
function prototypes in the different

103
00:04:38,820 --> 00:04:43,620
functions we hit just to make it clear

104
00:04:41,100 --> 00:04:45,960
what is happening we execute the binary

105
00:04:43,620 --> 00:04:48,840
we hit the TmCreateEnlistment

106
00:04:45,960 --> 00:04:51,300
TmCreateEnlistment

107
00:04:48,840 --> 00:04:53,160
is not actually defined however if we

108
00:04:51,300 --> 00:04:55,560
look at the back trace we see that it's

109
00:04:53,160 --> 00:04:58,199
called by NtCreateEnlistmentExt

110
00:04:55,560 --> 00:05:01,080
and so if we go on TmCreateEnlistment

111
00:04:58,199 --> 00:05:03,419
and look for references we can see that

112
00:05:01,080 --> 00:05:05,100
it's actually called from

113
00:05:03,419 --> 00:05:06,900
NtCreateEnlistment

114
00:05:05,100 --> 00:05:08,820
and this function we've seen it in a

115
00:05:06,900 --> 00:05:10,320
previous lab so we have defined the

116
00:05:08,820 --> 00:05:12,919
prototype because this function is

117
00:05:10,320 --> 00:05:12,919
defined right

118
00:05:14,040 --> 00:05:19,560
so we know the arguments

119
00:05:16,740 --> 00:05:22,199
so we can basically

120
00:05:19,560 --> 00:05:23,580
Define the prototype at least the name

121
00:05:22,199 --> 00:05:25,800
of the argument of the

122
00:05:23,580 --> 00:05:28,380
TmCreateEnlistment function because we know we

123
00:05:25,800 --> 00:05:30,120
have defined the names so going here an

124
00:05:28,380 --> 00:05:31,860
edit function prototype we can actually

125
00:05:30,120 --> 00:05:33,660
Define all the names which I've done

126
00:05:31,860 --> 00:05:36,419
already so then when we analyze this

127
00:05:33,660 --> 00:05:38,580
function we have the actual variables

128
00:05:36,419 --> 00:05:39,840
and we can actually rename things in

129
00:05:38,580 --> 00:05:41,820
this function because everything

130
00:05:39,840 --> 00:05:43,440
propagates if we look at the back trace

131
00:05:41,820 --> 00:05:45,120
again we see that it's called from

132
00:05:43,440 --> 00:05:47,280
CreateEnlistment in userland it's

133
00:05:45,120 --> 00:05:49,620
called the syscall NtCreateEnlistment

134
00:05:47,280 --> 00:05:51,900
and then TmCreateEnlistment

135
00:05:49,620 --> 00:05:55,080
and we've done the same for all the

136
00:05:51,900 --> 00:05:57,440
other calls so for instance for

137
00:05:55,080 --> 00:06:00,720
TmPrePrepareCompleteExt the same thing

138
00:05:57,440 --> 00:06:03,479
references pre prepare complete we look

139
00:06:00,720 --> 00:06:05,539
at the back trace we see that it's

140
00:06:03,479 --> 00:06:08,100
actually from userland we call

141
00:06:05,539 --> 00:06:10,860
pre-prepare complete which called the

142
00:06:08,100 --> 00:06:13,680
syscall NtPrePrepareComplete which ended up

143
00:06:10,860 --> 00:06:16,560
calling TmPrePrepareCompleteExt in

144
00:06:13,680 --> 00:06:19,380
kernel so again if we look for where it's

145
00:06:16,560 --> 00:06:22,199
called we have NtPrePrepareComplete which

146
00:06:19,380 --> 00:06:26,360
we can Define because we know that

147
00:06:22,199 --> 00:06:26,360
function because it's defined publicly

148
00:06:27,479 --> 00:06:32,220
so we can Define the arguments of this

149
00:06:30,300 --> 00:06:34,319
function so in this case actually there

150
00:06:32,220 --> 00:06:36,240
is one more which I had to to figure out

151
00:06:34,319 --> 00:06:38,039
which has which is an access mode which

152
00:06:36,240 --> 00:06:40,919
I figured out because this function

153
00:06:38,039 --> 00:06:42,780
takes an access mode as the fourth

154
00:06:40,919 --> 00:06:45,180
parameter one two three four by

155
00:06:42,780 --> 00:06:47,580
propagated it into the arguments because

156
00:06:45,180 --> 00:06:49,620
the one publicly shown is different from

157
00:06:47,580 --> 00:06:51,660
the one we have in our binary and then I

158
00:06:49,620 --> 00:06:54,060
was able to know that okay it's passing

159
00:06:51,660 --> 00:06:57,419
an enlistment object as well as a

160
00:06:54,060 --> 00:06:59,880
TmVirtualClock to TmPrepareComplete

161
00:06:57,419 --> 00:07:02,100
and then I can see here here it's a

162
00:06:59,880 --> 00:07:04,020
function so actually I call that

163
00:07:02,100 --> 00:07:07,199
callback func

164
00:07:04,020 --> 00:07:10,280
and so we have something similar for the

165
00:07:07,199 --> 00:07:10,280
other functions like

166
00:07:11,819 --> 00:07:17,220
TmPrepareCompleteExt

167
00:07:14,220 --> 00:07:20,160
something similar and for

168
00:07:17,220 --> 00:07:22,319
TmCommitCompleteExt which we can hit by

169
00:07:20,160 --> 00:07:25,699
hitting enter and entering the commit

170
00:07:22,319 --> 00:07:25,699
complete function call

171
00:07:28,620 --> 00:07:33,000
so just to show you that actually

172
00:07:30,419 --> 00:07:36,020
renaming things will help when we try to

173
00:07:33,000 --> 00:07:36,020
understand what is happening

174
00:07:41,580 --> 00:07:46,979
so now let's debug our binary we have

175
00:07:44,280 --> 00:07:48,900
our four breakpoint set in the kernel we

176
00:07:46,979 --> 00:07:51,240
are going to start the binary it is

177
00:07:48,900 --> 00:07:54,000
waiting for us to enter a key we hit a

178
00:07:51,240 --> 00:07:56,039
key and we reach our first breakpoint so

179
00:07:54,000 --> 00:07:58,860
we are in TmCreateEnlistment we're

180
00:07:56,039 --> 00:08:00,900
going to set a breakpoint on the

181
00:07:58,860 --> 00:08:02,400
TmInitializedEnlistment function call so

182
00:08:00,900 --> 00:08:04,979
we're going to use F2 in this case

183
00:08:02,400 --> 00:08:07,319
because we've already used the four

184
00:08:04,979 --> 00:08:09,120
breakpoints that are available on the

185
00:08:07,319 --> 00:08:10,680
hardware side so we're going to use F2

186
00:08:09,120 --> 00:08:12,720
it's going to set the breakpoint with

187
00:08:10,680 --> 00:08:14,759
the BP instruction so we hit that break

188
00:08:12,720 --> 00:08:17,180
point we know the first argument is our

189
00:08:14,759 --> 00:08:17,180
enlistment

190
00:08:27,300 --> 00:08:33,300
so we can see it's not initialized yet

191
00:08:30,120 --> 00:08:36,560
so we're going to step over or use p

192
00:08:33,300 --> 00:08:36,560
and I'm going to print it again

193
00:08:36,719 --> 00:08:42,539
so here we see the cookie is initialized

194
00:08:38,760 --> 00:08:45,300
the GUID and with de46 there is no other

195
00:08:42,539 --> 00:08:46,800
enlistment into the same transaction it

196
00:08:45,300 --> 00:08:48,420
links to the resource manager and the

197
00:08:46,800 --> 00:08:50,899
transaction and it also print the

198
00:08:48,420 --> 00:08:50,899
transaction

199
00:08:56,820 --> 00:09:01,860
so the transaction has the cookie as

200
00:08:59,519 --> 00:09:04,620
well and it's currently active the

201
00:09:01,860 --> 00:09:07,260
enlistment list only has one enlistment

202
00:09:04,620 --> 00:09:08,760
the count is one and the list has two

203
00:09:07,260 --> 00:09:10,620
pointers pointing to the same value

204
00:09:08,760 --> 00:09:12,740
because only one of them and it's

205
00:09:10,620 --> 00:09:15,540
currently in the outcome

206
00:09:12,740 --> 00:09:17,760
undetermined and for the enlistments we

207
00:09:15,540 --> 00:09:19,740
see it's active the notification mask is

208
00:09:17,760 --> 00:09:21,660
the one we provided from userland okay

209
00:09:19,740 --> 00:09:23,339
so now we continue execution we're going

210
00:09:21,660 --> 00:09:24,420
to hit the create enlistment a second

211
00:09:23,339 --> 00:09:26,760
time that we're going to continue

212
00:09:24,420 --> 00:09:28,980
execution until it actually initialize

213
00:09:26,760 --> 00:09:30,660
the enlistment again in rcx we have the

214
00:09:28,980 --> 00:09:33,120
second enlistment

215
00:09:30,660 --> 00:09:34,500
so we're gonna print the enlistment a

216
00:09:33,120 --> 00:09:37,920
second time

217
00:09:34,500 --> 00:09:39,839
it's not going to be filled yet it's not

218
00:09:37,920 --> 00:09:41,940
actually valid yet so we're going to

219
00:09:39,839 --> 00:09:45,920
step over

220
00:09:41,940 --> 00:09:45,920
and now if we print it again

221
00:09:49,019 --> 00:09:54,540
we actually see that it has a valid

222
00:09:51,240 --> 00:09:56,760
cookie a GUID and with de47

223
00:09:54,540 --> 00:10:01,019
and the previous one

224
00:09:56,760 --> 00:10:04,459
ended with the de46 on the enlistment so

225
00:10:01,019 --> 00:10:07,740
we have 46 and

226
00:10:04,459 --> 00:10:09,779
47. now we see that actually there are

227
00:10:07,740 --> 00:10:11,820
two different pointers we see as well

228
00:10:09,779 --> 00:10:13,440
that the transaction is one three three

229
00:10:11,820 --> 00:10:15,300
zero and for the other one the

230
00:10:13,440 --> 00:10:17,040
transaction was one three three zero so

231
00:10:15,300 --> 00:10:18,120
when we print it but it's actually the

232
00:10:17,040 --> 00:10:20,040
same transaction for the previous

233
00:10:18,120 --> 00:10:22,560
enlistment so both enlistments are part

234
00:10:20,040 --> 00:10:26,779
of the same transaction

235
00:10:22,560 --> 00:10:26,779
and if we actually print the transaction

236
00:10:28,740 --> 00:10:34,800
we now see that enlistment head has two

237
00:10:32,339 --> 00:10:37,019
different pointers and the enlistment count

238
00:10:34,800 --> 00:10:39,360
is two so the transaction
has two enlistment

239
00:10:37,019 --> 00:10:41,339
inside it which is exactly what we

240
00:10:39,360 --> 00:10:44,279
wanted

241
00:10:41,339 --> 00:10:47,100
so the outcome is the same so we're

242
00:10:44,279 --> 00:10:50,040
going to continue execution

243
00:10:47,100 --> 00:10:51,360
so now we hit the

244
00:10:50,040 --> 00:10:53,220
TmPrePrepareCompleteExt

245
00:10:51,360 --> 00:10:56,160
we check the userlandwe we can see that

246
00:10:53,220 --> 00:10:59,279
actually it received notification about

247
00:10:56,160 --> 00:11:01,079
the pre-prepare state so if we go inside

248
00:10:59,279 --> 00:11:03,959
that function we know that the argument

249
00:11:01,079 --> 00:11:08,100
is an enlistment object so if we look

250
00:11:03,959 --> 00:11:10,320
back our first enlistment would be 6 7

251
00:11:08,100 --> 00:11:12,420
10 or the address and the second one is

252
00:11:10,320 --> 00:11:13,980
six seven twenty on a different

253
00:11:12,420 --> 00:11:16,620
completely different address so

254
00:11:13,980 --> 00:11:19,200
basically 6710 is the first one so if

255
00:11:16,620 --> 00:11:22,260
you print 6710 now we see it's actually

256
00:11:19,200 --> 00:11:23,940
in the state pre-prepairing but if we

257
00:11:22,260 --> 00:11:27,240
look at the second one

258
00:11:23,940 --> 00:11:30,120
the state is pre-preparing as well so

259
00:11:27,240 --> 00:11:32,700
both are in the same state now let's

260
00:11:30,120 --> 00:11:34,800
actually step over that and see what

261
00:11:32,700 --> 00:11:36,360
happens we're gonna execute the whole

262
00:11:34,800 --> 00:11:37,860
thing and we know this function is going

263
00:11:36,360 --> 00:11:40,579
to be called a second time for the

264
00:11:37,860 --> 00:11:40,579
second enlistment

265
00:11:41,880 --> 00:11:47,459
so what happens is that if we look at

266
00:11:45,360 --> 00:11:50,399
the first enlistment that has been

267
00:11:47,459 --> 00:11:53,040
pre-prepared complete we see that now

268
00:11:50,399 --> 00:11:55,200
the first enlistment is in the

269
00:11:53,040 --> 00:11:59,600
KENLISTMENT pre-prepared the second

270
00:11:55,200 --> 00:11:59,600
enlistment is actually in the

271
00:12:01,579 --> 00:12:06,060
pre-preparing States the previous

272
00:12:04,140 --> 00:12:08,040
enlistment is already in the

273
00:12:06,060 --> 00:12:09,600
pre-prepared state so actually calling

274
00:12:08,040 --> 00:12:11,700
prepare complete in the first

275
00:12:09,600 --> 00:12:14,339
enlistment actually changes state okay

276
00:12:11,700 --> 00:12:16,380
let's continue execution now we are

277
00:12:14,339 --> 00:12:19,019
stuck in userland because both

278
00:12:16,380 --> 00:12:21,300
pre-prepare complete have been called so

279
00:12:19,019 --> 00:12:24,600
if we break in a debugger and again

280
00:12:21,300 --> 00:12:26,640
print the first and second enlistment if

281
00:12:24,600 --> 00:12:29,279
we look at both enlistment

282
00:12:26,640 --> 00:12:31,740
this one is preparing

283
00:12:29,279 --> 00:12:35,040
and this one is preparing so they are

284
00:12:31,740 --> 00:12:36,600
both moved to the preparing States

285
00:12:35,040 --> 00:12:39,060
now we're going to continue execution

286
00:12:36,600 --> 00:12:42,779
in  userland by hitting enter and we

287
00:12:39,060 --> 00:12:45,240
call the PrepareCompleteExt function

288
00:12:42,779 --> 00:12:47,700
so if we look again at both enlistments

289
00:12:45,240 --> 00:12:50,639
the first one and the second one we see

290
00:12:47,700 --> 00:12:52,079
that preparing and preparing they are

291
00:12:50,639 --> 00:12:54,300
both in the same state

292
00:12:52,079 --> 00:12:56,339
so we're going to continue execution we

293
00:12:54,300 --> 00:12:58,500
hit the same function again and if we

294
00:12:56,339 --> 00:13:01,920
look at the first enlistment again it's

295
00:12:58,500 --> 00:13:04,560
changed to prepared state and when the

296
00:13:01,920 --> 00:13:07,500
second one is called and we check again

297
00:13:04,560 --> 00:13:09,600
the second one it has changed to

298
00:13:07,500 --> 00:13:12,180
committed notify as well as the first

299
00:13:09,600 --> 00:13:14,760
one has changed to committed notify so

300
00:13:12,180 --> 00:13:16,500
basically because both enlistments have

301
00:13:14,760 --> 00:13:18,740
evolved they both change to the next

302
00:13:16,500 --> 00:13:18,740
state

303
00:13:19,320 --> 00:13:22,680
and if we look at the actual

304
00:13:20,820 --> 00:13:25,380
notification in userland we can see

305
00:13:22,680 --> 00:13:28,380
that they both got the notify commit

306
00:13:25,380 --> 00:13:31,380
notification received so now we can hit

307
00:13:28,380 --> 00:13:33,660
a key to continue and we see we hit the

308
00:13:31,380 --> 00:13:35,279
TmCommitCompleteExt function and again

309
00:13:33,660 --> 00:13:37,740
very similar code with function

310
00:13:35,279 --> 00:13:40,920
callbacks and if we look again at the

311
00:13:37,740 --> 00:13:43,200
enlistments we have committed notify

312
00:13:40,920 --> 00:13:45,720
still

313
00:13:43,200 --> 00:13:47,820
and committed notify so one thing we

314
00:13:45,720 --> 00:13:49,440
could do here is actually look at the

315
00:13:47,820 --> 00:13:51,959
transaction and we can see the

316
00:13:49,440 --> 00:13:54,620
transaction actually changed to outcome

317
00:13:51,959 --> 00:13:54,620
committed

318
00:13:55,339 --> 00:13:59,779
so

319
00:13:56,940 --> 00:13:59,779
we are

320
00:14:01,139 --> 00:14:05,160
in the commit complete function call

321
00:14:03,180 --> 00:14:06,660
we're going to continue execution

322
00:14:05,160 --> 00:14:08,880
hit it the second time

323
00:14:06,660 --> 00:14:11,459
is still ah

324
00:14:08,880 --> 00:14:15,180
outcome committed still

325
00:14:11,459 --> 00:14:18,139
and the enlistment are both in the

326
00:14:15,180 --> 00:14:18,139
committed state

327
00:14:20,160 --> 00:14:23,540
and committed notify

328
00:14:24,779 --> 00:14:29,040
so now it tells that the transaction

329
00:14:27,120 --> 00:14:31,200
should be committed and we need to hit

330
00:14:29,040 --> 00:14:33,720
enter to exit

331
00:14:31,200 --> 00:14:37,920
but if we go back and look at the actual

332
00:14:33,720 --> 00:14:39,800
States we have committed

333
00:14:37,920 --> 00:14:42,899
for both

334
00:14:39,800 --> 00:14:44,820
enlistments and the transaction as

335
00:14:42,899 --> 00:14:46,199
well is committed so there is one last

336
00:14:44,820 --> 00:14:48,959
thing I want to check is the actual

337
00:14:46,199 --> 00:14:51,120
state of the transaction over time so if

338
00:14:48,959 --> 00:14:53,160
we execute the binary again we're going

339
00:14:51,120 --> 00:14:55,560
to hit that function so we're going to

340
00:14:53,160 --> 00:14:57,720
set a breakpoint but actually the

341
00:14:55,560 --> 00:14:59,639
breakpoints for when the enlistment is

342
00:14:57,720 --> 00:15:02,600
created is already defined so it's going

343
00:14:59,639 --> 00:15:02,600
to be hitting

344
00:15:09,480 --> 00:15:14,579
so we're gonna just look at the first

345
00:15:11,760 --> 00:15:16,199
enlistment and I'm interested in the

346
00:15:14,579 --> 00:15:18,560
actual transaction so we're going to

347
00:15:16,199 --> 00:15:18,560
step over

348
00:15:18,660 --> 00:15:23,000
and now it has defined the transaction

349
00:15:28,980 --> 00:15:32,820
so you can see here the outcome is under

350
00:15:30,959 --> 00:15:35,220
undetermined so I'm just going to

351
00:15:32,820 --> 00:15:37,019
continue and look at the transaction see

352
00:15:35,220 --> 00:15:39,300
undetermined

353
00:15:37,019 --> 00:15:41,839
so now it's hitting the

354
00:15:39,300 --> 00:15:41,839
TmCreateEnlistment

355
00:15:42,360 --> 00:15:47,459
so we see it hasn't changed so it's

356
00:15:44,940 --> 00:15:49,740
going to call the pre preapare complete

357
00:15:47,459 --> 00:15:52,019
function

358
00:15:49,740 --> 00:15:54,240
and for the first Enlistment so once the

359
00:15:52,019 --> 00:15:57,300
first enlistment has changed to the

360
00:15:54,240 --> 00:15:59,459
pre-prepared state the transaction is

361
00:15:57,300 --> 00:16:01,440
still the same state but now if the

362
00:15:59,459 --> 00:16:05,100
second enlistment going into to the same

363
00:16:01,440 --> 00:16:07,680
state now both have changed

364
00:16:05,100 --> 00:16:11,339
the pre preparing states if you look at the

365
00:16:07,680 --> 00:16:13,980
transaction it still undetermine and if

366
00:16:11,339 --> 00:16:15,899
we look at the information however we

367
00:16:13,980 --> 00:16:18,360
see that a transaction is preparing now

368
00:16:15,899 --> 00:16:20,940
that has evolved

369
00:16:18,360 --> 00:16:24,560
so if we continue execution hit enter to

370
00:16:20,940 --> 00:16:24,560
call  prepare complete

371
00:16:25,440 --> 00:16:31,560
the enlistment is still preparing the

372
00:16:28,560 --> 00:16:35,820
transaction is still preparing now one

373
00:16:31,560 --> 00:16:38,040
Enlistment has changed to prepared the

374
00:16:35,820 --> 00:16:40,019
transaction

375
00:16:38,040 --> 00:16:42,300
is still preparing and it's because

376
00:16:40,019 --> 00:16:44,339
basically the second enlistment hasn't

377
00:16:42,300 --> 00:16:46,320
changed to prepare yet because we are

378
00:16:44,339 --> 00:16:47,699
actually calling prepare complete for

379
00:16:46,320 --> 00:16:49,440
the second Enlistment now so I'm just

380
00:16:47,699 --> 00:16:51,000
going to continue execution to do it for

381
00:16:49,440 --> 00:16:53,220
the second enlistment and now that

382
00:16:51,000 --> 00:16:55,920
both enlistments have done it if we

383
00:16:53,220 --> 00:16:59,339
check again the transaction

384
00:16:55,920 --> 00:17:02,480
we see that it has changed to committed

385
00:16:59,339 --> 00:17:02,480
and the enlistment

386
00:17:02,519 --> 00:17:07,559
is in the committed notify so basically

387
00:17:04,740 --> 00:17:09,600
for a transaction to change state we

388
00:17:07,559 --> 00:17:11,579
need every single enlistment that is

389
00:17:09,600 --> 00:17:13,260
part of this transaction to change state

390
00:17:11,579 --> 00:17:15,000
as well which is what we are seeing in

391
00:17:13,260 --> 00:17:16,620
the debugger which is exactly what we

392
00:17:15,000 --> 00:17:18,919
needed to confirm okay thank you for

393
00:17:16,620 --> 00:17:18,919
watching

