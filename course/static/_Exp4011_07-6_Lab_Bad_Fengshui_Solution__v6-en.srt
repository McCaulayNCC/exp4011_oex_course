1
00:00:00,179 --> 00:00:03,840
okay so let's add the code we need for

2
00:00:02,220 --> 00:00:05,400
that lab

3
00:00:03,840 --> 00:00:07,560
so

4
00:00:05,400 --> 00:00:10,019
we need to create two transactions and

5
00:00:07,560 --> 00:00:11,820
spray alternating enlistment

6
00:00:10,019 --> 00:00:12,960
so this is creating transaction number

7
00:00:11,820 --> 00:00:15,179
three

8
00:00:12,960 --> 00:00:18,859
so we're going to actually copy that for

9
00:00:15,179 --> 00:00:18,859
creating transaction number one and two

10
00:00:21,119 --> 00:00:25,279
so we still need for transaction three

11
00:00:32,780 --> 00:00:37,579
transaction number one and two

12
00:00:38,640 --> 00:00:43,260
okay

13
00:00:40,739 --> 00:00:45,360
and then we need to spray alternating enlistments

15
00:00:45,360 --> 00:00:50,160
so

16
00:00:47,820 --> 00:00:52,200
we can actually copy the code

17
00:00:50,160 --> 00:00:54,860
that we're going to use for re-filling

18
00:00:52,200 --> 00:00:54,860
enlistments

19
00:01:01,140 --> 00:01:04,140
and

20
00:01:06,479 --> 00:01:10,680
so we actually need to fill to create an

21
00:01:09,119 --> 00:01:12,320
enlistment associated with transaction

22
00:01:10,680 --> 00:01:16,939
number one

23
00:01:12,320 --> 00:01:16,939
and save that into the set A

24
00:01:19,740 --> 00:01:26,780
but we need to actually also spray

25
00:01:23,820 --> 00:01:26,780
into set B

26
00:01:29,640 --> 00:01:33,619
associated with transaction number two

27
00:01:42,960 --> 00:01:49,920
so we have pushed the binary using

28
00:01:45,840 --> 00:01:52,740
Visual Studio to the actual target VM

29
00:01:49,920 --> 00:01:54,000
and we have attached WinDbg on the

30
00:01:52,740 --> 00:01:57,979
target VM

31
00:01:54,000 --> 00:01:57,979
so I'm gonna run the binary

32
00:01:58,979 --> 00:02:04,380
and now we can see it created two sets

33
00:02:01,860 --> 00:02:06,119
of enlistments for the feng shui and so

34
00:02:04,380 --> 00:02:08,759
it's telling us to use the either the

35
00:02:06,119 --> 00:02:10,679
pool find command or the pool command so

36
00:02:08,759 --> 00:02:12,959
because we haven't set any breakpoints

37
00:02:10,679 --> 00:02:14,340
we can't use the pool command because we

38
00:02:12,959 --> 00:02:15,780
don't know any address at the moment so

39
00:02:14,340 --> 00:02:17,700
we're going to use the poolfind command

40
00:02:15,780 --> 00:02:20,040
but the profile command is quite slow

41
00:02:17,700 --> 00:02:22,700
and so we're going to use actually the

42
00:02:20,040 --> 00:02:22,700
cache command

43
00:02:28,379 --> 00:02:34,200
and by default the cache is quite small

44
00:02:31,980 --> 00:02:36,660
and so the idea is we're going to

45
00:02:34,200 --> 00:02:39,959
increase the cache it speeds up a little

46
00:02:36,660 --> 00:02:42,180
bit the poolfind command if we set it

47
00:02:39,959 --> 00:02:44,819
to this value we can see that it's too

48
00:02:42,180 --> 00:02:47,160
large so we can't do that so we're going

49
00:02:44,819 --> 00:02:49,200
to use that value which seems to be the

50
00:02:47,160 --> 00:02:50,879
largest value I can use on my machine to

51
00:02:49,200 --> 00:02:53,720
speed up and then we're going to use the

52
00:02:50,879 --> 00:02:53,720
poolfind command

53
00:02:54,480 --> 00:03:00,180
with the TmEn tag

54
00:02:58,200 --> 00:03:04,459
so just to get an idea of how long it

55
00:03:00,180 --> 00:03:04,459
takes I'm just going to start the watch

56
00:03:07,980 --> 00:03:11,459
as you can see the addresses are quite

57
00:03:09,720 --> 00:03:14,099
random at first

58
00:03:11,459 --> 00:03:15,420
and it's starting to look for all the

59
00:03:14,099 --> 00:03:18,180
different

60
00:03:15,420 --> 00:03:20,420
KENLISTMENTs  chunks on the non-page

61
00:03:18,180 --> 00:03:20,420
pool

62
00:03:32,879 --> 00:03:40,080
a few moments later

63
00:03:36,480 --> 00:03:42,780
so we can see it finish searching in one

64
00:03:40,080 --> 00:03:45,299
of the region one of the non-page pools

65
00:03:42,780 --> 00:03:49,940
and it's actually searching in another

66
00:03:45,299 --> 00:03:49,940
one right now but it doesn't find anyone

67
00:03:54,959 --> 00:03:58,680
so what I'm going to do is I'm going to

68
00:03:56,760 --> 00:04:02,239
break

69
00:03:58,680 --> 00:04:02,239
and stop the timer

70
00:04:05,400 --> 00:04:10,500
and so if we look back we can see uh

71
00:04:08,580 --> 00:04:13,379
that there are some of them that are

72
00:04:10,500 --> 00:04:14,879
adjacent in such a region and a lot of

73
00:04:13,379 --> 00:04:16,380
them are actually adjacent in different

74
00:04:14,879 --> 00:04:18,239
region

75
00:04:16,380 --> 00:04:20,880
so

76
00:04:18,239 --> 00:04:22,979
we look at some of the one that are

77
00:04:20,880 --> 00:04:27,259
actually located at the beginning of it

78
00:04:22,979 --> 00:04:27,259
of a region right so it's like this one

79
00:04:27,660 --> 00:04:30,860
make the pool command

80
00:04:34,139 --> 00:04:39,180
so here we can see that there are

81
00:04:36,960 --> 00:04:41,580
KENLISTMENTs adjacent to each other so

82
00:04:39,180 --> 00:04:43,440
again the question is are there from

83
00:04:41,580 --> 00:04:44,820
different transaction transaction one

84
00:04:43,440 --> 00:04:47,220
transaction two transition one

85
00:04:44,820 --> 00:04:49,380
transaction two so first we need to

86
00:04:47,220 --> 00:04:50,639
figure out where the enlistment

87
00:04:49,380 --> 00:04:53,280
because we know there are a couple of

88
00:04:50,639 --> 00:04:55,800
headers like the object headers and the

89
00:04:53,280 --> 00:05:00,680
poor header so we need to find the

90
00:04:55,800 --> 00:05:00,680
offset so if we just print the data

91
00:05:03,120 --> 00:05:09,660
and we're looking into the b00b so it

92
00:05:07,979 --> 00:05:12,320
looks like it's

93
00:05:09,660 --> 00:05:12,320
here

94
00:05:12,479 --> 00:05:19,800
ending with 03 it's one two

95
00:05:15,360 --> 00:05:23,960
three four five six so it's 60 hex so if

96
00:05:19,800 --> 00:05:23,960
we actually print as an enlistment

97
00:05:50,460 --> 00:05:53,900
so we're going to add 60

98
00:06:03,180 --> 00:06:09,560
so we can see that it matches the cookie

99
00:06:06,360 --> 00:06:13,080
the cookie is b00b0003

100
00:06:09,560 --> 00:06:16,080
and this everything looks valid

101
00:06:13,080 --> 00:06:20,699
so then the next thing we can see is

102
00:06:16,080 --> 00:06:24,539
that there is 250 hex between two

103
00:06:20,699 --> 00:06:27,919
chunks you go from 420 to 670 and so on

104
00:06:24,539 --> 00:06:27,919
so if we just add

105
00:06:29,460 --> 00:06:32,419
250

106
00:06:36,539 --> 00:06:40,580
it gives us another chunk

107
00:06:41,400 --> 00:06:46,259
with another

108
00:06:43,440 --> 00:06:48,180
valid KENLISTMENT so what we can do now

109
00:06:46,259 --> 00:06:50,699
is actually look at the transaction

110
00:06:48,180 --> 00:06:54,419
associated with each enlistment

111
00:06:50,699 --> 00:06:56,160
so to do that we can just add the field

112
00:06:54,419 --> 00:06:59,000
we want to print so we want to print the

113
00:06:56,160 --> 00:06:59,000
transaction field

114
00:07:01,919 --> 00:07:10,100
and we can do that for the different one

115
00:07:04,919 --> 00:07:10,100
so we want to start from the 1d0

116
00:07:12,360 --> 00:07:15,319
so we're going to add

117
00:07:20,340 --> 00:07:25,160
so we're going to do it for every

118
00:07:22,319 --> 00:07:25,160
enlistment

119
00:07:40,979 --> 00:07:45,840
so yeah it's really interesting because

120
00:07:43,380 --> 00:07:48,300
as you can see the transaction one and

121
00:07:45,840 --> 00:07:51,000
transaction two and so we have actually

122
00:07:48,300 --> 00:07:53,639
managed to realize our goal which is to

123
00:07:51,000 --> 00:07:56,360
have alternated KENLISTMENTs from two

124
00:07:53,639 --> 00:07:56,360
different transactions

125
00:08:01,020 --> 00:08:04,380
so now let's do the same but this time

126
00:08:03,000 --> 00:08:06,780
we're going to actually print the

127
00:08:04,380 --> 00:08:09,539
allocated enlistments over time

128
00:08:06,780 --> 00:08:12,479
so

129
00:08:09,539 --> 00:08:14,660
if we actually look at what is at this

130
00:08:12,479 --> 00:08:14,660
address

131
00:08:15,479 --> 00:08:20,900
we can see it actually calls

132
00:08:18,539 --> 00:08:23,639
TmpInitializeEnlistment and we remember

133
00:08:20,900 --> 00:08:26,280
it's actually passing the KENLISTMENT

134
00:08:23,639 --> 00:08:28,560
pointer at the first argument so we can

135
00:08:26,280 --> 00:08:30,240
actually use a breakpoint to log the

136
00:08:28,560 --> 00:08:33,479
allocated enlistments so we're going to

137
00:08:30,240 --> 00:08:36,200
use the sxd sse command and then the

138
00:08:33,479 --> 00:08:36,200
actual breakpoint

139
00:08:43,020 --> 00:08:47,100
okay

140
00:08:44,240 --> 00:08:49,459
so now we're gonna actually start our

141
00:08:47,100 --> 00:08:49,459
binary

142
00:08:49,800 --> 00:08:54,360
and so we can see all the enlistments

143
00:08:52,500 --> 00:08:58,880
being printed

144
00:08:54,360 --> 00:08:58,880
so we're going to set that a timer

145
00:09:00,660 --> 00:09:04,279
and you can see that initially

146
00:09:02,640 --> 00:09:08,360
all the enlistments being allocated

147
00:09:04,279 --> 00:09:08,360
are at random addresses

148
00:09:08,459 --> 00:09:12,920
is because we haven't filled all the

149
00:09:10,440 --> 00:09:12,920
holes yet

150
00:09:14,880 --> 00:09:18,240
another thing to note is that it's quite

151
00:09:16,440 --> 00:09:20,220
slow at the beginning

152
00:09:18,240 --> 00:09:24,800
but it has to

153
00:09:20,220 --> 00:09:24,800
break on all these allocations

154
00:09:25,860 --> 00:09:31,680
a few moments later we got the idea it's

155
00:09:29,820 --> 00:09:33,959
taking a long time

156
00:09:31,680 --> 00:09:35,880
we're just going to break

157
00:09:33,959 --> 00:09:39,380
the debugger

158
00:09:35,880 --> 00:09:39,380
and just analyze

159
00:09:40,500 --> 00:09:44,779
so if we look for instance at this

160
00:09:42,360 --> 00:09:44,779
region

161
00:09:52,019 --> 00:09:56,720
you can see that actually the layout is

162
00:09:54,180 --> 00:09:56,720
not that great

163
00:10:10,440 --> 00:10:15,980
so if we go and look for certain

164
00:10:13,320 --> 00:10:15,980
addresses

165
00:10:26,160 --> 00:10:31,820
yeah so I'm just gonna disable the

166
00:10:29,220 --> 00:10:31,820
breakpoints

167
00:10:34,680 --> 00:10:38,000
I'll just continue execution

168
00:10:43,740 --> 00:10:46,860
so in order to improve our debugging

169
00:10:45,959 --> 00:10:48,779
session

170
00:10:46,860 --> 00:10:52,320
we're going to add code to actually

171
00:10:48,779 --> 00:10:56,399
block at a certain time so there are

172
00:10:52,320 --> 00:11:00,500
1000 hex enlistments in total so when we

173
00:10:56,399 --> 00:11:00,500
reach for instance C 0 0

174
00:11:10,260 --> 00:11:13,260
.

175
00:11:15,240 --> 00:11:18,860
we could actually wait

176
00:11:20,220 --> 00:11:23,360
that we hit a key

177
00:11:31,140 --> 00:11:34,880
and we're going to push that new binary

178
00:11:36,000 --> 00:11:39,380
on to the target VM

179
00:11:42,420 --> 00:11:48,320
so now we're going to execute it

180
00:11:44,760 --> 00:11:48,320
so we can see it hangs

181
00:11:48,899 --> 00:11:53,880
so probably it reached the get character

182
00:11:51,420 --> 00:11:56,839
now we can re-enable the breakpoints so

183
00:11:53,880 --> 00:11:56,839
we break in here

184
00:11:57,720 --> 00:12:03,440
so you can see our breakpoint was

185
00:11:59,700 --> 00:12:03,440
disabled so we're gonna enable it now

186
00:12:04,500 --> 00:12:08,300
and now it's enabled so we're going to

187
00:12:06,240 --> 00:12:11,880
continue execution

188
00:12:08,300 --> 00:12:13,339
and we're going to hit the key

189
00:12:11,880 --> 00:12:16,740
so it's going to continue print

190
00:12:13,339 --> 00:12:18,779
allocating the enlistments

191
00:12:16,740 --> 00:12:21,380
so here we can see that there is a lot

192
00:12:18,779 --> 00:12:21,380
of adjacency

193
00:12:23,779 --> 00:12:27,200
a little more

194
00:12:28,740 --> 00:12:31,700
so let's break

195
00:12:31,800 --> 00:12:35,779
and we're gonna take one of them

196
00:12:48,300 --> 00:12:53,459
and yeah we get our alternating

197
00:12:51,300 --> 00:12:56,100
enlistments

198
00:12:53,459 --> 00:12:58,560
so now we're going to add code to

199
00:12:56,100 --> 00:13:01,560
actually create the holes we know we

200
00:12:58,560 --> 00:13:03,180
have some adjacency in the

201
00:13:01,560 --> 00:13:06,120
KENLISTMENT with the alternating

202
00:13:03,180 --> 00:13:07,380
allocations but we want to free all the

203
00:13:06,120 --> 00:13:09,959
enlistments associated with

204
00:13:07,380 --> 00:13:12,899
transaction two so we need to check the

205
00:13:09,959 --> 00:13:15,200
states for all of them so we pre prepare

206
00:13:12,899 --> 00:13:15,200
them

207
00:13:16,500 --> 00:13:20,180
then we need to prepare them

208
00:13:22,079 --> 00:13:26,120
then we need to commit them

209
00:13:39,839 --> 00:13:45,740
and finally we need to close the handle

210
00:13:42,180 --> 00:13:45,740
that we have in userland

211
00:14:06,360 --> 00:14:12,120
so in order to free the enlistment we need

212
00:14:08,760 --> 00:14:13,800
to pre-prepare, prepare, commit and then

213
00:14:12,120 --> 00:14:16,200
close the handle for now we're gonna

214
00:14:13,800 --> 00:14:18,000
command draining the notification just

215
00:14:16,200 --> 00:14:20,339
because we don't wanna we wanna see

216
00:14:18,000 --> 00:14:22,440
there is no interference at this if the

217
00:14:20,339 --> 00:14:24,920
layout is correct without reading the

218
00:14:22,440 --> 00:14:24,920
notification

219
00:14:31,980 --> 00:14:35,880
and the last thing I want to do is I

220
00:14:33,779 --> 00:14:39,260
just want to set some text here to make

221
00:14:35,880 --> 00:14:39,260
it more user friendly

222
00:14:57,420 --> 00:15:00,079
okay

223
00:15:00,180 --> 00:15:06,019
so we're going to push the binary

224
00:15:02,639 --> 00:15:06,019
onto the target VM

225
00:15:13,560 --> 00:15:17,040
so now you can see that we have pushed

226
00:15:15,480 --> 00:15:18,360
the binary

227
00:15:17,040 --> 00:15:20,760
and we

228
00:15:18,360 --> 00:15:22,680
don't have any breakpoints enabled at

229
00:15:20,760 --> 00:15:24,720
the moment

230
00:15:22,680 --> 00:15:27,779
so we're going to run the binary it's

231
00:15:24,720 --> 00:15:28,860
telling us to enable the breakpoints

232
00:15:27,779 --> 00:15:30,839
so we're going to be breaking the

233
00:15:28,860 --> 00:15:34,459
debugger

234
00:15:30,839 --> 00:15:34,459
and enable breakpoint 0

235
00:15:35,100 --> 00:15:38,880
it's enabled now so we continue

236
00:15:36,839 --> 00:15:42,980
execution

237
00:15:38,880 --> 00:15:42,980
then we hit enter to continue execution

238
00:15:43,800 --> 00:15:49,459
and now we can see it starts printing

239
00:15:45,959 --> 00:15:49,459
all our KENLISTMENTs

240
00:15:55,160 --> 00:15:59,760
so we have adjacency every two

241
00:15:57,959 --> 00:16:03,019
enlistments which is pretty neat I'm

242
00:15:59,760 --> 00:16:03,019
gonna break in the debugger

243
00:16:05,300 --> 00:16:09,740
it does let me break

244
00:16:11,639 --> 00:16:16,620
okay

245
00:16:13,620 --> 00:16:18,800
so I'm gonna go for instance on this

246
00:16:16,620 --> 00:16:18,800
address

247
00:16:24,779 --> 00:16:31,040
so we can see we have two four

248
00:16:28,500 --> 00:16:31,040
KENLISTMENTs

249
00:16:35,220 --> 00:16:38,120
go here

250
00:16:46,079 --> 00:16:52,079
we have another for instance

251
00:16:49,380 --> 00:16:54,360
so it's pretty good layout so I'm just

252
00:16:52,079 --> 00:16:58,459
gonna run it

253
00:16:54,360 --> 00:16:58,459
but actually disabling the breakpoint

254
00:17:05,040 --> 00:17:08,760
okay

255
00:17:06,600 --> 00:17:10,199
so now we're gonna we have spread

256
00:17:08,760 --> 00:17:12,419
everything so we're gonna actually

257
00:17:10,199 --> 00:17:14,880
trigger the free of the enlistments

258
00:17:12,419 --> 00:17:18,439
and now it's telling us we can break and

259
00:17:14,880 --> 00:17:18,439
check if we have created holes

260
00:17:18,919 --> 00:17:23,720
so let's go back to the one we printed

261
00:17:24,360 --> 00:17:32,660
so if we go check

262
00:17:28,400 --> 00:17:37,520
let me put that address

263
00:17:32,660 --> 00:17:37,520
4060 and e060

264
00:17:40,740 --> 00:17:43,160
okay

265
00:17:44,220 --> 00:17:50,820
ah neat we have an allocated enlistment

266
00:17:46,919 --> 00:17:53,840
free chunk allocated in some free chunk

267
00:17:50,820 --> 00:17:53,840
and if we look at the other one

268
00:17:56,100 --> 00:18:03,020
allocated enlistment free chunk allocated

269
00:17:58,500 --> 00:18:03,020
enlistment free chunk yeah we did it

270
00:18:03,539 --> 00:18:07,860
so I've just hit enter in order to

271
00:18:05,580 --> 00:18:09,840
continue execution after we've analyzed

272
00:18:07,860 --> 00:18:12,179
the holes being created and so remember

273
00:18:09,840 --> 00:18:15,720
there is code already to refill the

274
00:18:12,179 --> 00:18:17,580
holes with new KENLISTMENTs so indeed after

275
00:18:15,720 --> 00:18:20,340
we

276
00:18:17,580 --> 00:18:22,440
created the holes here it's actually

277
00:18:20,340 --> 00:18:24,780
refilling the holes with a new

278
00:18:22,440 --> 00:18:27,480
transaction transition number three and

279
00:18:24,780 --> 00:18:30,860
a new set so now let's reanalyze the

280
00:18:27,480 --> 00:18:30,860
debugger what we have

281
00:18:33,299 --> 00:18:39,320
so we're going to analyze again

282
00:18:36,299 --> 00:18:39,320
the same addresses

283
00:18:40,380 --> 00:18:46,440
so we can see here the holes haven't

284
00:18:43,020 --> 00:18:46,440
been replaced

285
00:18:48,179 --> 00:18:53,280
or on the other one

286
00:18:51,299 --> 00:18:55,559
the holes haven't been replaced either

287
00:18:53,280 --> 00:18:58,440
so it could be due to the fact that

288
00:18:55,559 --> 00:19:01,760
actually I paused for a long time and

289
00:18:58,440 --> 00:19:01,760
something else happened

290
00:19:03,840 --> 00:19:08,820
so because the debugging session can

291
00:19:06,419 --> 00:19:11,280
change the layouts because we stopped

292
00:19:08,820 --> 00:19:13,320
the debugger and so on so what I'm gonna

293
00:19:11,280 --> 00:19:15,840
do is I'm just gonna remove the get

294
00:19:13,320 --> 00:19:19,580
character so it actually executes all in

295
00:19:15,840 --> 00:19:19,580
a row without any pause

296
00:19:20,100 --> 00:19:24,440
so we want it to continue

297
00:19:55,020 --> 00:19:59,000
so I'm just going to rebuild the binary

299
00:20:03,539 --> 00:20:06,980
and I'm going to run it

300
00:20:07,559 --> 00:20:12,000
okay so you should have done everything

301
00:20:10,140 --> 00:20:14,419
so now I'm just gonna break in the

302
00:20:12,000 --> 00:20:14,419
debugger

303
00:20:16,080 --> 00:20:21,740
and so here I don't have a choice I need

304
00:20:17,940 --> 00:20:21,740
to use the poolfind command

305
00:20:46,679 --> 00:20:50,419
a few moments later

306
00:20:52,440 --> 00:20:57,419
okay so I think we've got a good

307
00:20:55,080 --> 00:21:01,080
amount of chunks here

308
00:20:57,419 --> 00:21:04,980
so I'm gonna stop it here

309
00:21:01,080 --> 00:21:07,799
just break the debugger so let's see

310
00:21:04,980 --> 00:21:11,960
what we have we take a given region so

311
00:21:07,799 --> 00:21:11,960
for instance take this one

312
00:21:20,220 --> 00:21:26,160
so the first thing we can notice is that

313
00:21:22,620 --> 00:21:28,320
we do have alternating KENLISTMENTs which

314
00:21:26,160 --> 00:21:31,200
is what we want but the next thing we

315
00:21:28,320 --> 00:21:35,340
want to check is that we indeed have it

316
00:21:31,200 --> 00:21:38,659
into two alternating transaction

317
00:21:35,340 --> 00:21:38,659
that it should in this address

318
00:21:45,179 --> 00:21:51,140
okay so now I have one valid so if I

319
00:21:47,520 --> 00:21:51,140
print the transaction for this one

320
00:21:53,880 --> 00:21:59,940
this transaction and then if I add plus

321
00:21:56,940 --> 00:21:59,940
250

322
00:22:23,700 --> 00:22:27,720
so here we can see they are actually all

323
00:22:25,500 --> 00:22:31,440
part of the same transaction so we got

324
00:22:27,720 --> 00:22:33,900
something wrong or it's just a spray of

325
00:22:31,440 --> 00:22:36,000
all the previous enlistment that were

326
00:22:33,900 --> 00:22:38,240
allocated here so if we do check our

327
00:22:36,000 --> 00:22:38,240
code

328
00:22:40,020 --> 00:22:44,820
and confirm the enlistment of

329
00:22:42,360 --> 00:22:48,080
transaction three

330
00:22:44,820 --> 00:22:48,080
during the previous one

331
00:22:48,659 --> 00:22:53,179
we're in transaction two and one so

332
00:22:50,820 --> 00:22:53,179
that's good

333
00:22:58,260 --> 00:23:03,380
so let's see

334
00:23:00,299 --> 00:23:03,380
what we have now

335
00:23:08,100 --> 00:23:11,299
so we have holes

336
00:23:20,640 --> 00:23:25,940
so we haven't been able to reallocate the

337
00:23:22,919 --> 00:23:29,700
enlistment chunks

338
00:23:25,940 --> 00:23:32,640
so we see that even though we remove in

339
00:23:29,700 --> 00:23:37,140
the code all the pause so everything is

340
00:23:32,640 --> 00:23:40,020
done very quickly we either only see an

341
00:23:37,140 --> 00:23:43,320
enlistment from transaction one or

342
00:23:40,020 --> 00:23:45,659
adjacent so without any holes or or we

343
00:23:43,320 --> 00:23:47,820
see the holes but then we don't see them

344
00:23:45,659 --> 00:23:50,580
replaced by the enlistment from

345
00:23:47,820 --> 00:23:53,700
transaction three so

346
00:23:50,580 --> 00:23:56,400
it doesn't work really well

347
00:23:53,700 --> 00:23:59,760
so the last thing I wanted to show is

348
00:23:56,400 --> 00:24:01,320
that I've already enabled the draining

349
00:23:59,760 --> 00:24:04,380
the notifications

350
00:24:01,320 --> 00:24:07,679
and I wanted to see what was the impact

351
00:24:04,380 --> 00:24:09,120
not only on the coalition but also

352
00:24:07,679 --> 00:24:11,520
on the

353
00:24:09,120 --> 00:24:14,039
hole refilling

354
00:24:11,520 --> 00:24:16,860
and so you can see

355
00:24:14,039 --> 00:24:18,659
all the get character have been

356
00:24:16,860 --> 00:24:21,840
commented out

357
00:24:18,659 --> 00:24:25,679
so everything goes really fast and then

358
00:24:21,840 --> 00:24:27,179
at the very end I drain the notification

359
00:24:25,679 --> 00:24:30,780
and

360
00:24:27,179 --> 00:24:33,360
I actually during the refilling I

361
00:24:30,780 --> 00:24:37,080
actually have a an if condition

362
00:24:33,360 --> 00:24:39,780
to be able to enable breakpoints so I'm

363
00:24:37,080 --> 00:24:42,960
actually able to see what are the

364
00:24:39,780 --> 00:24:44,820
addresses used for the creation of the

365
00:24:42,960 --> 00:24:46,679
enlistments when trying to refill the

366
00:24:44,820 --> 00:24:49,260
holes

367
00:24:46,679 --> 00:24:52,679
and so

368
00:24:49,260 --> 00:24:55,200
the actual execution is here you can see

369
00:24:52,679 --> 00:24:57,659
we drain notification and now we were

370
00:24:55,200 --> 00:25:00,840
waiting for a breakpoint to be enabled

371
00:24:57,659 --> 00:25:02,159
and I actually previously hit enter and

372
00:25:00,840 --> 00:25:04,740
on the debugger

373
00:25:02,159 --> 00:25:06,960
I broke and I enable this breakpoint and

374
00:25:04,740 --> 00:25:08,700
then with this we see the trace of all

375
00:25:06,960 --> 00:25:11,039
the allocated and enlistments so this is

376
00:25:08,700 --> 00:25:13,320
for the hole refilling and the first

377
00:25:11,039 --> 00:25:16,080
thing we we can see that actually it's

378
00:25:13,320 --> 00:25:18,299
using adjacent addresses and which is

379
00:25:16,080 --> 00:25:20,400
pretty bad because it means from a

380
00:25:18,299 --> 00:25:22,620
feng shui perspective we didn't manage

381
00:25:20,400 --> 00:25:25,440
to refill the hole it's just refitting a

382
00:25:22,620 --> 00:25:28,679
new region which is not what we want and

383
00:25:25,440 --> 00:25:30,960
so to confirm that I use the pool

384
00:25:28,679 --> 00:25:32,400
command and try to print the different

385
00:25:30,960 --> 00:25:34,860
transaction

386
00:25:32,400 --> 00:25:36,059
for the one that have been reallocated but

387
00:25:34,860 --> 00:25:38,279
we can see that all the same

388
00:25:36,059 --> 00:25:40,679
transactions so it's it's actually not

389
00:25:38,279 --> 00:25:42,900
fitting the hole another region we can

390
00:25:40,679 --> 00:25:44,820
see that enlistment is in a totally

391
00:25:42,900 --> 00:25:47,820
different region

392
00:25:44,820 --> 00:25:49,919
and again here another region we see the

393
00:25:47,820 --> 00:25:51,299
all the KENLISTMENTs are part of the same

394
00:25:49,919 --> 00:25:54,240
transaction so

395
00:25:51,299 --> 00:25:57,299
so to summarize uh it's this feng shui

396
00:25:54,240 --> 00:25:59,100
method is not working very well and in

397
00:25:57,299 --> 00:26:01,460
this scenario I haven't been able to

398
00:25:59,100 --> 00:26:04,980
show you the actual

399
00:26:01,460 --> 00:26:07,740
notifications being in the way but what

400
00:26:04,980 --> 00:26:10,200
we can see anyway is that the feng shui

401
00:26:07,740 --> 00:26:13,159
doesn't work with KENLISTMENTs okay

402
00:26:10,200 --> 00:26:13,159
thank you for watching

