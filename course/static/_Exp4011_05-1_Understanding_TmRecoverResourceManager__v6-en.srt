1
00:00:02,899 --> 00:00:08,040
so in the next few videos we're going to

2
00:00:05,700 --> 00:00:10,019
look into the vulnerable function which

3
00:00:08,040 --> 00:00:12,059
is called TmRecoverResourceManager

4
00:00:10,019 --> 00:00:14,400
and understand the general logic then

5
00:00:12,059 --> 00:00:16,619
we're going to analyze the actual patch

6
00:00:14,400 --> 00:00:19,800
to understand the vulnerability and

7
00:00:16,619 --> 00:00:23,279
finally we'll look at our initial

8
00:00:19,800 --> 00:00:26,039
strategy for exploitation okay let's get

9
00:00:23,279 --> 00:00:28,260
started so this is the same diagram we

10
00:00:26,039 --> 00:00:30,660
saw earlier and it kind of summarizes

11
00:00:28,260 --> 00:00:33,360
what the TmRecoverResourceManager

12
00:00:30,660 --> 00:00:35,100
function does in a sense because it

13
00:00:33,360 --> 00:00:38,460
accesses the KRESOURCEMANAGER

14
00:00:35,100 --> 00:00:40,440
structure gets the enlistment head to

15
00:00:38,460 --> 00:00:42,360
access the linked list of enlistments

16
00:00:40,440 --> 00:00:45,000
associated with that resource manager

17
00:00:42,360 --> 00:00:47,040
and then it does a bunch of stuff on the

18
00:00:45,000 --> 00:00:49,320
enlistments like checking if they are

19
00:00:47,040 --> 00:00:51,660
notifiable and if so queuing the

20
00:00:49,320 --> 00:00:54,120
notifications so I will explain the

21
00:00:51,660 --> 00:00:56,940
vulnerable code first and then it will

22
00:00:54,120 --> 00:00:59,699
go over the patch in detail but what you

23
00:00:56,940 --> 00:01:01,379
quickly see I guess from the patch diff

24
00:00:59,699 --> 00:01:03,600
is that it's it's not an obvious

25
00:01:01,379 --> 00:01:05,280
vulnerability and so we you have to

26
00:01:03,600 --> 00:01:08,460
really understand the code that we

27
00:01:05,280 --> 00:01:11,159
annotated in Ghidra or IDA to make sense

28
00:01:08,460 --> 00:01:13,200
of what the vulnerability is so we know

29
00:01:11,159 --> 00:01:15,600
the vulnerability occurs in this

30
00:01:13,200 --> 00:01:18,479
TmRecoverResourceManager function and

31
00:01:15,600 --> 00:01:19,979
specifically within this while loop so

32
00:01:18,479 --> 00:01:23,460
basically everything we are going to

33
00:01:19,979 --> 00:01:25,860
talk about for now exists within this

34
00:01:23,460 --> 00:01:28,080
while loop and so what is going to

35
00:01:25,860 --> 00:01:30,920
happen when you recover a resource

36
00:01:28,080 --> 00:01:33,299
manager the idea is that it is trying to

37
00:01:30,920 --> 00:01:37,380
re-synchronize all of the enlistments

38
00:01:33,299 --> 00:01:39,780
so they can sort of agree on being in a

39
00:01:37,380 --> 00:01:42,119
specific state so that you can continue

40
00:01:39,780 --> 00:01:44,460
with the transaction and so basically

41
00:01:42,119 --> 00:01:46,680
what this loop is doing is it's looping

42
00:01:44,460 --> 00:01:49,200
over all the enlistments associated with

43
00:01:46,680 --> 00:01:52,740
the KRESOURCEMANAGER and it is going to

44
00:01:49,200 --> 00:01:54,659
notify them by setting a notification in

45
00:01:52,740 --> 00:01:57,240
the notification queue and so basically

46
00:01:54,659 --> 00:01:59,700
what it does is it starts
with the enlistment

47
00:01:57,240 --> 00:02:02,520
head field of the KRESOURCEMANAGER

48
00:01:59,700 --> 00:02:05,159
and you and you will keep looping until

49
00:02:02,520 --> 00:02:07,680
the current enlistment that is being

50
00:02:05,159 --> 00:02:09,840
accessed points back to the resource

51
00:02:07,680 --> 00:02:12,300
manager and then it is all done because

52
00:02:09,840 --> 00:02:14,819
every enlistment has been touched so in

53
00:02:12,300 --> 00:02:17,099
the loop the first thing it does is it

54
00:02:14,819 --> 00:02:19,819
checks the flags of the enlistments

55
00:02:17,099 --> 00:02:23,280
and if the enlistment is marked as

56
00:02:19,819 --> 00:02:25,440
finalized it just skips it and queries

57
00:02:23,280 --> 00:02:28,440
the next enlistment in the list and we

58
00:02:25,440 --> 00:02:31,680
can see this else condition finishes at

59
00:02:28,440 --> 00:02:33,840
the end of the actual function and so if

60
00:02:31,680 --> 00:02:37,200
that happens if it goes into that else

61
00:02:33,840 --> 00:02:40,620
condition it will just keep looping to

62
00:02:37,200 --> 00:02:42,720
process the next enlistment and so from

63
00:02:40,620 --> 00:02:45,120
an exploitation perspective we don't

64
00:02:42,720 --> 00:02:47,580
want this to happen as it won't touch

65
00:02:45,120 --> 00:02:49,680
any other code so that's just one little

66
00:02:47,580 --> 00:02:53,340
point we know that to trigger the

67
00:02:49,680 --> 00:02:56,519
vulnerability we know enlistments should

68
00:02:53,340 --> 00:02:58,800
not be finalized so then we analyze the

69
00:02:56,519 --> 00:03:00,840
else case we see that the code is going

70
00:02:58,800 --> 00:03:03,360
to do something with the enlistments so

71
00:03:00,840 --> 00:03:06,239
it bumps the ref counts it waits on the

72
00:03:03,360 --> 00:03:09,000
mutex of the enlistments to make sure it

73
00:03:06,239 --> 00:03:12,540
is the exclusive code touching it it

74
00:03:09,000 --> 00:03:15,540
sets some Boolean flag which we called

75
00:03:12,540 --> 00:03:17,580
bSendNotification to zero for now and we

76
00:03:15,540 --> 00:03:20,459
don't know what it is quite yet

77
00:03:17,580 --> 00:03:23,640
next it has some logic that is going to

78
00:03:20,459 --> 00:03:26,099
decide whether or not it sets the send

79
00:03:23,640 --> 00:03:28,080
notification Boolean to one basically

80
00:03:26,099 --> 00:03:30,360
and this is going to be based off

81
00:03:28,080 --> 00:03:33,120
whether or not an enlistment is

82
00:03:30,360 --> 00:03:35,760
considered notifiable and again this is

83
00:03:33,120 --> 00:03:38,040
a flag that is undocumented but we were

84
00:03:35,760 --> 00:03:41,159
able to work it out by looking at what

85
00:03:38,040 --> 00:03:43,620
this code does later and so if the

86
00:03:41,159 --> 00:03:46,019
enlistment is notifiable we can see two

87
00:03:43,620 --> 00:03:48,360
main things of interest one is that it

88
00:03:46,019 --> 00:03:50,760
will set the send notification boolean

89
00:03:48,360 --> 00:03:53,099
to one saying that a notification will

90
00:03:50,760 --> 00:03:55,440
be sent later and another thing of

91
00:03:53,099 --> 00:03:57,720
interest is that it unsets the

92
00:03:55,440 --> 00:04:00,420
notifiable flag and we will see why

93
00:03:57,720 --> 00:04:02,760
later but basically what happens is that

94
00:04:00,420 --> 00:04:06,180
this loop can end up getting a little

95
00:04:02,760 --> 00:04:08,580
bit disoriented because of state changes

96
00:04:06,180 --> 00:04:10,200
and it might have to reprocess all of

97
00:04:08,580 --> 00:04:13,159
the enlistments from the very beginning

98
00:04:10,200 --> 00:04:16,320
at which point you don't want to send

99
00:04:13,159 --> 00:04:19,139
duplicate notifications so from our

100
00:04:16,320 --> 00:04:20,880
perspective we will realize why in a

101
00:04:19,139 --> 00:04:23,520
second but we are basically interested

102
00:04:20,880 --> 00:04:27,240
in getting this send notification flag

103
00:04:23,520 --> 00:04:31,020
set to one so in order for it to be set

104
00:04:27,240 --> 00:04:33,960
to one the enlistment has to not be

105
00:04:31,020 --> 00:04:37,020
superior which is determined by the

106
00:04:33,960 --> 00:04:38,940
absence of the KENLISTMENT superior flag and

107
00:04:37,020 --> 00:04:41,040
the state of the transaction that is

108
00:04:38,940 --> 00:04:43,979
associated with the enlistments that is

109
00:04:41,040 --> 00:04:47,100
currently being processed needs to be in

110
00:04:43,979 --> 00:04:49,919
one of these three states committed in

111
00:04:47,100 --> 00:04:51,960
doubt or prepared and so as we analyze

112
00:04:49,919 --> 00:04:53,940
code like this we are sort of making a

113
00:04:51,960 --> 00:04:56,100
recipe of things that we're going to

114
00:04:53,940 --> 00:04:57,540
need to do in order to reach the

115
00:04:56,100 --> 00:05:00,720
vulnerable code and trigger the

116
00:04:57,540 --> 00:05:03,479
vulnerability next the enlistment mutex

117
00:05:00,720 --> 00:05:06,419
is unlocked which means all the code in

118
00:05:03,479 --> 00:05:08,580
KTM from other threads could be

119
00:05:06,419 --> 00:05:10,860
interacting with the enlistment and then

120
00:05:08,580 --> 00:05:12,780
assuming we managed to set this send

121
00:05:10,860 --> 00:05:15,240
notification flag to work what happens

122
00:05:12,780 --> 00:05:18,120
is that the resource manager itself will

123
00:05:15,240 --> 00:05:19,919
be unlocked by releasing its mutex and

124
00:05:18,120 --> 00:05:22,560
so originally when you're looking at

125
00:05:19,919 --> 00:05:24,539
this it is not necessarily obvious why

126
00:05:22,560 --> 00:05:26,940
this is important but when you are

127
00:05:24,539 --> 00:05:29,100
manipulating enlistments and you are

128
00:05:26,940 --> 00:05:33,900
changing their states what potentially

129
00:05:29,100 --> 00:05:36,180
happens is that they might get freed and

130
00:05:33,900 --> 00:05:39,360
in order to do any sort of modification

131
00:05:36,180 --> 00:05:41,940
the associated resource manager has to

132
00:05:39,360 --> 00:05:44,400
be unlocked so this unlock is fairly

133
00:05:41,940 --> 00:05:47,220
interesting and then the next step is it

134
00:05:44,400 --> 00:05:49,380
sends a notification by calling this

135
00:05:47,220 --> 00:05:51,240
TmpSetNotificationResourceManager

136
00:05:49,380 --> 00:05:54,660
function and this is a public symbol by

137
00:05:51,240 --> 00:05:57,720
Microsoft and this kind of explains why

138
00:05:54,660 --> 00:05:59,940
we call the the flag bSendNotification

139
00:05:57,720 --> 00:06:01,860
and basically what this

140
00:05:59,940 --> 00:06:05,400
TmpSetNotificationResourceManager function

141
00:06:01,860 --> 00:06:07,860
does is just queue a structure into the

142
00:06:05,400 --> 00:06:10,860
notification queue which then correlates

143
00:06:07,860 --> 00:06:13,080
to the state's changes that we were

144
00:06:10,860 --> 00:06:15,840
observing from userland in a previous

145
00:06:13,080 --> 00:06:18,539
lab that's basically all it does so then

146
00:06:15,840 --> 00:06:21,419
after TmpSetNotificationResourceManager

147
00:06:18,539 --> 00:06:23,819
returns all of a sudden it

148
00:06:21,419 --> 00:06:27,180
queries the enlistment to see if the

149
00:06:23,819 --> 00:06:28,979
flag saying it was finalized is set and

150
00:06:27,180 --> 00:06:32,280
so our guess here is that when an

151
00:06:28,979 --> 00:06:35,039
enlistments is prepared to be freed its

152
00:06:32,280 --> 00:06:38,160
state is changed to finalized and so

153
00:06:35,039 --> 00:06:40,620
here if the finalized flag is set it

154
00:06:38,160 --> 00:06:43,440
sets a new Boolean to one and we call

155
00:06:40,620 --> 00:06:46,380
that Boolean bEnlistmentIsFinalized

156
00:06:43,440 --> 00:06:49,380
and then it references the enlistment

157
00:06:46,380 --> 00:06:51,660
object which effectively decreases the

158
00:06:49,380 --> 00:06:53,819
reference count and then it waits again

159
00:06:51,660 --> 00:06:56,340
on the resource manager mutex to get

160
00:06:53,819 --> 00:06:58,440
exclusive access so it won't be obvious

161
00:06:56,340 --> 00:07:02,220
but this is actually the vulnerability

162
00:06:58,440 --> 00:07:05,340
and the problem is that this code here

163
00:07:02,220 --> 00:07:07,380
is trying to detect a state change of

164
00:07:05,340 --> 00:07:10,020
the enlistment switching to a state

165
00:07:07,380 --> 00:07:12,120
where it might be finalized and so you

166
00:07:10,020 --> 00:07:14,520
would get freed if the reference count

167
00:07:12,120 --> 00:07:17,699
goes down to zero but what we saw here

168
00:07:14,520 --> 00:07:19,500
is that it releases the mutex of the

169
00:07:17,699 --> 00:07:21,780
enlistment and of the associated

170
00:07:19,500 --> 00:07:24,060
resource manager before queuing the

171
00:07:21,780 --> 00:07:27,479
notification into the notification queue

172
00:07:24,060 --> 00:07:29,639
and so at this point it might check the

173
00:07:27,479 --> 00:07:32,400
enlistment flag and the finalized flag

174
00:07:29,639 --> 00:07:34,440
will not be set but between the time you

175
00:07:32,400 --> 00:07:37,139
check that fly and the time it actually

176
00:07:34,440 --> 00:07:39,720
relocks the resource manager mutex in

177
00:07:37,139 --> 00:07:42,720
here that finalized flag might become

178
00:07:39,720 --> 00:07:44,580
set at which point the enlistments is

179
00:07:42,720 --> 00:07:46,380
actually finalized and so what can

180
00:07:44,580 --> 00:07:49,020
happen is that because the enlistment

181
00:07:46,380 --> 00:07:51,599
has the up the reference object function

182
00:07:49,020 --> 00:07:53,880
call on it its reference counts will be

183
00:07:51,599 --> 00:07:55,979
lowered and if in the process of being

184
00:07:53,880 --> 00:07:58,440
finalized by another thread the

185
00:07:55,979 --> 00:08:00,780
reference count hits zero the enlistment

186
00:07:58,440 --> 00:08:02,880
will be freed by the other thread or

187
00:08:00,780 --> 00:08:05,280
alternatively if the finalized operation

188
00:08:02,880 --> 00:08:09,000
happens before the ObfDereferenceObject

189
00:08:05,280 --> 00:08:11,639
ObfDereferenceObject call this

190
00:08:09,000 --> 00:08:13,919
ObfDereferenceObject itself will free the

191
00:08:11,639 --> 00:08:16,080
enlistment and so basically because this

192
00:08:13,919 --> 00:08:18,419
code is trying to get exclusive access

193
00:08:16,080 --> 00:08:21,060
on the resource manager by waiting on

194
00:08:18,419 --> 00:08:22,800
its mutex it might potentially take a

195
00:08:21,060 --> 00:08:24,479
while for it to lock that resource

196
00:08:22,800 --> 00:08:26,220
manager and so there is effectively

197
00:08:24,479 --> 00:08:28,979
a race condition in based on mutex

198
00:08:26,220 --> 00:08:31,259
condition and so what happens later he

199
00:08:28,979 --> 00:08:33,959
tries to use this Boolean that was set

200
00:08:31,259 --> 00:08:35,159
based off whether or not he detected

201
00:08:33,959 --> 00:08:37,320
that the enlistment might have been

202
00:08:35,159 --> 00:08:39,599
freed and so if he thinks that the

203
00:08:37,320 --> 00:08:42,180
alignment was finalized then it might

204
00:08:39,599 --> 00:08:43,740
have been freed and what it will do is

205
00:08:42,180 --> 00:08:46,140
that it will start from the enlistment head

206
00:08:43,740 --> 00:08:47,880
again by referencing it from the

207
00:08:46,140 --> 00:08:50,040
KRESOURCEMANAGER structure so it

208
00:08:47,880 --> 00:08:52,200
basically decides to rework the entire

209
00:08:50,040 --> 00:08:54,959
enlistment list from scratch which is

210
00:08:52,200 --> 00:08:57,779
exactly why earlier we saw that if it

211
00:08:54,959 --> 00:09:00,300
notifies the enlistments based off the

212
00:08:57,779 --> 00:09:03,480
KENLISTMENT_IS_NOTIFIABLE flag being

213
00:09:00,300 --> 00:09:05,760
said it and unsets it right after and so

214
00:09:03,480 --> 00:09:08,339
it is specifically to cope with this

215
00:09:05,760 --> 00:09:10,920
scenario where an enlistment was freed

216
00:09:08,339 --> 00:09:12,899
from the underneath logic and it needs

217
00:09:10,920 --> 00:09:15,300
to rework the entire list you won't

218
00:09:12,899 --> 00:09:17,519
actually re-notify all of the enlistment

219
00:09:15,300 --> 00:09:19,740
but if the enlistment wasn't detected as

220
00:09:17,519 --> 00:09:22,560
finalized for instance you won the race

221
00:09:19,740 --> 00:09:24,540
condition and so it didn't actually set

222
00:09:22,560 --> 00:09:26,880
the enlistments is finalized flag it

223
00:09:24,540 --> 00:09:29,459
will a reference the current enlistment

224
00:09:26,880 --> 00:09:32,580
pointer and accept the next in the

225
00:09:29,459 --> 00:09:35,640
linked list by referencing the

226
00:09:32,580 --> 00:09:37,680
NextSameRm.Flink pointer so at this point it

227
00:09:35,640 --> 00:09:40,200
that enlistment was freed it will not

228
00:09:37,680 --> 00:09:42,899
access freed memory or some other memory

229
00:09:40,200 --> 00:09:44,820
that replaced that enlistment and it

230
00:09:42,899 --> 00:09:47,700
will go back into the beginning of the

231
00:09:44,820 --> 00:09:49,800
loop and start using it and so we'll

232
00:09:47,700 --> 00:09:52,019
analyze what you can do with that type

233
00:09:49,800 --> 00:09:54,300
of thing later so just to summarize the

234
00:09:52,019 --> 00:09:56,040
mental model of the TmRecoverResourceManager

235
00:09:54,300 --> 00:09:58,920
function you've got the while

236
00:09:56,040 --> 00:10:00,600
loop touching the enlistments head it is

237
00:09:58,920 --> 00:10:04,380
starting to parse all of these

238
00:10:00,600 --> 00:10:06,420
enlistments based of the NextSameRm

239
00:10:04,380 --> 00:10:09,000
flink pointer maybe it suddenly

240
00:10:06,420 --> 00:10:11,580
encounters one that got Freed From

241
00:10:09,000 --> 00:10:13,800
Another thread so the finalized flag is

242
00:10:11,580 --> 00:10:16,320
set for that particular enlistment and

243
00:10:13,800 --> 00:10:18,899
basically what it will do is it will

244
00:10:16,320 --> 00:10:21,060
re-walk the linked list of  enlistments

245
00:10:18,899 --> 00:10:23,820
from the enlistment head and it won't

246
00:10:21,060 --> 00:10:25,560
actually be sending notifications for

247
00:10:23,820 --> 00:10:27,959
the enlistments the that have already

248
00:10:25,560 --> 00:10:30,660
been notified and eventually when it

249
00:10:27,959 --> 00:10:32,580
re-parses them all until it gets to a

250
00:10:30,660 --> 00:10:35,160
new enlistment that hasn't been touched

251
00:10:32,580 --> 00:10:37,920
yet it will continue from this point and

252
00:10:35,160 --> 00:10:40,560
notify them and so on until it reaches

253
00:10:37,920 --> 00:10:42,600
the the end of the list which is the

254
00:10:40,560 --> 00:10:46,519
KRESOURCEMANAGER enlistments head and

255
00:10:42,600 --> 00:10:46,519
so at that point it exits the loop

