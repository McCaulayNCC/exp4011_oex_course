1
00:00:00,000 --> 00:00:04,980
so we'll go over the second lab now and

2
00:00:02,700 --> 00:00:08,820
it will be about experimenting feng shui

3
00:00:04,980 --> 00:00:11,160
using named pipes and so we will see how

4
00:00:08,820 --> 00:00:13,799
efficient it is to use named pipes and

5
00:00:11,160 --> 00:00:16,020
more specifically the named pipe writes

6
00:00:13,799 --> 00:00:20,100
for the feng shui and so as we've seen

7
00:00:16,020 --> 00:00:22,439
before again unfortunately the poolfind

8
00:00:20,100 --> 00:00:24,420
command can be quite slow but also

9
00:00:22,439 --> 00:00:27,660
setting a breakpoint on the actual

10
00:00:24,420 --> 00:00:28,760
allocation is not only slow but in the

11
00:00:27,660 --> 00:00:32,279
case of the

12
00:00:28,760 --> 00:00:34,620
NpFr.sys kernel module and that

13
00:00:32,279 --> 00:00:36,180
particular breakpoints setting this

14
00:00:34,620 --> 00:00:38,880
breakpoint on the actual allocation

15
00:00:36,180 --> 00:00:41,879
would be too verbose and too noisy

16
00:00:38,880 --> 00:00:43,680
because of many paths reaching that code

17
00:00:41,879 --> 00:00:46,559
and so we basically have to set the

18
00:00:43,680 --> 00:00:49,280
breakpoint on a single process and to do

19
00:00:46,559 --> 00:00:51,899
so we have to reverse engineer the

20
00:00:49,280 --> 00:00:54,120
NpFr.sys driver so this slide

21
00:00:51,899 --> 00:00:56,760
demonstrates how to set a breakpoint for

22
00:00:54,120 --> 00:00:59,940
a single process and so we get the

23
00:00:56,760 --> 00:01:02,820
KPROCESS structure address using the

24
00:00:59,940 --> 00:01:05,460
process command and then we set the

25
00:01:02,820 --> 00:01:09,479
breakpoints and we specify the KPROCESS

26
00:01:05,460 --> 00:01:11,220
address using the /p flag and once

27
00:01:09,479 --> 00:01:13,439
we're done we have to disable the

28
00:01:11,220 --> 00:01:15,600
breakpoints because otherwise it's too

29
00:01:13,439 --> 00:01:19,680
noisy and so we see that the breakpoint

30
00:01:15,600 --> 00:01:21,900
is inside the NpAddDataQueueEntry

31
00:01:19,680 --> 00:01:26,280
function we can see that the call to

32
00:01:21,900 --> 00:01:28,500
ExAllocatePoolWithQuotaTag uses the NpFr tag

33
00:01:26,280 --> 00:01:31,020
as we would expect and so we set the

34
00:01:28,500 --> 00:01:34,860
breakpoint on the instruction just after

35
00:01:31,020 --> 00:01:37,320
the call and the RAX register will hold

36
00:01:34,860 --> 00:01:39,659
the allocated chunk so the lab we are

37
00:01:37,320 --> 00:01:42,119
going to work on is called named pipe

38
00:01:39,659 --> 00:01:44,579
spray and it is going to rely on two

39
00:01:42,119 --> 00:01:47,280
named pipes to do alternate allocations

40
00:01:44,579 --> 00:01:50,040
and then freeing all the chunks from the

41
00:01:47,280 --> 00:01:53,040
second named pipe to create holes and so

42
00:01:50,040 --> 00:01:55,020
if you want you can then add code to

43
00:01:53,040 --> 00:01:58,380
reallocate enlistments into the

44
00:01:55,020 --> 00:02:00,899
holes so the KENLISTMENT chunks will be between

45
00:01:58,380 --> 00:02:03,540
two named pipe chunks and so the

46
00:02:00,899 --> 00:02:06,240
advantage of doing so is that then you

47
00:02:03,540 --> 00:02:08,220
can set the breakpoint on the

48
00:02:06,240 --> 00:02:10,259
TmCreateEnlistment function and when the

49
00:02:08,220 --> 00:02:12,660
enlistments are created it allows

50
00:02:10,259 --> 00:02:14,640
analyzing the heap area and we would be

51
00:02:12,660 --> 00:02:17,520
able to see the previously allocated

52
00:02:14,640 --> 00:02:20,220
named pipes and it avoids having to rely

53
00:02:17,520 --> 00:02:22,200
on the named pipe breakpoint which is

54
00:02:20,220 --> 00:02:25,319
really noisy so this is the output from

55
00:02:22,200 --> 00:02:28,860
WinDbg after we allocated alternating

56
00:02:25,319 --> 00:02:31,200
named pipes chunks and so using the pool

57
00:02:28,860 --> 00:02:34,080
command we can see the named pipe are

58
00:02:31,200 --> 00:02:36,300
adjacent to each other's with the NpFr

59
00:02:34,080 --> 00:02:38,940
tag and it's pretty reliable and then

60
00:02:36,300 --> 00:02:41,640
for instance we show the contents of two

61
00:02:38,940 --> 00:02:44,280
adjacent chunks so for instance we took

62
00:02:41,640 --> 00:02:47,519
the first one ending with 000

63
00:02:44,280 --> 00:02:49,080
and the second one ending with 250 and

64
00:02:47,519 --> 00:02:51,360
we can see that there are some

65
00:02:49,080 --> 00:02:53,640
uncontrolled header and then we see the

66
00:02:51,360 --> 00:02:57,180
414141 in the first

67
00:02:53,640 --> 00:02:58,980
allocation and we see our 424242 in the

68
00:02:57,180 --> 00:03:01,800
second allocation which as you can

69
00:02:58,980 --> 00:03:04,319
imagine corresponds to the A's we wrote

70
00:03:01,800 --> 00:03:06,180
in the first pipe and to the B's we

71
00:03:04,319 --> 00:03:09,420
wrote in the second pipe and so after

72
00:03:06,180 --> 00:03:11,519
the named pipe writes are alternating in

73
00:03:09,420 --> 00:03:13,440
memory you can read the data from the

74
00:03:11,519 --> 00:03:15,659
second named pipe and it will create the

75
00:03:13,440 --> 00:03:19,140
holes we want and so this is the output

76
00:03:15,659 --> 00:03:21,239
after we actually reallocate the holes

77
00:03:19,140 --> 00:03:24,120
at least we try to relocate the holes

78
00:03:21,239 --> 00:03:26,580
with KENLISTMENTs and we can see that

79
00:03:24,120 --> 00:03:29,760
one of the KENLISTMENTs got allocated

80
00:03:26,580 --> 00:03:32,220
right after a named pipe which is what we

81
00:03:29,760 --> 00:03:34,680
would want and so we mentioned that for

82
00:03:32,220 --> 00:03:36,959
every enlistment we create there is a

83
00:03:34,680 --> 00:03:40,440
notification chunk that gets allocated

84
00:03:36,959 --> 00:03:41,879
as well which has the TmFn tag as you

85
00:03:40,440 --> 00:03:43,860
see here and so we can use the

86
00:03:41,879 --> 00:03:47,220
breakpoint that particular breakpoint

87
00:03:43,860 --> 00:03:49,620
below to track where this allocations go

88
00:03:47,220 --> 00:03:52,739
and it helps to understand if there is a

89
00:03:49,620 --> 00:03:55,500
chance that this allocation mess up with

90
00:03:52,739 --> 00:03:58,260
our actually interesting feng shui so

91
00:03:55,500 --> 00:04:00,720
let's analyze the source code of the lab

92
00:03:58,260 --> 00:04:03,299
which is the named pipe spray lab so

93
00:04:00,720 --> 00:04:06,420
there are many similar things to

94
00:04:03,299 --> 00:04:08,519
previously analyze Labs so we're just

95
00:04:06,420 --> 00:04:10,500
going to detail the new things so we see

96
00:04:08,519 --> 00:04:12,659
there is a new structure called feng

97
00:04:10,500 --> 00:04:15,239
shui and so it details two groups

98
00:04:12,659 --> 00:04:19,380
identify by number one and two and we

99
00:04:15,239 --> 00:04:22,199
see there is all the KTM objects related

100
00:04:19,380 --> 00:04:24,479
to the enlistments being sprayed for

101
00:04:22,199 --> 00:04:26,639
transaction one resource manager 1 and

102
00:04:24,479 --> 00:04:28,800
transaction manager one and then there

103
00:04:26,639 --> 00:04:31,139
is a second group identified by number

104
00:04:28,800 --> 00:04:33,120
two which is again enlistments,

105
00:04:31,139 --> 00:04:35,280
transaction, transaction manager and

106
00:04:33,120 --> 00:04:38,040
resource manager and we track different

107
00:04:35,280 --> 00:04:41,280
counts for both groups because in the

108
00:04:38,040 --> 00:04:45,000
first group we'll have extra enlistments

109
00:04:41,280 --> 00:04:47,520
being sprayed in order to get a

110
00:04:45,000 --> 00:04:50,280
deterministic layout

111
00:04:47,520 --> 00:04:53,580
then we have the named pipe structure so

112
00:04:50,280 --> 00:04:55,440
this structure is used to track all the

113
00:04:53,580 --> 00:04:58,380
elements related to the named pipe spray

114
00:04:55,440 --> 00:05:01,320
so we see there is a pipe name there is

115
00:04:58,380 --> 00:05:03,540
a drain size which is basically the size

116
00:05:01,320 --> 00:05:06,180
of the chunk we want to allocate and

117
00:05:03,540 --> 00:05:09,180
then drain in order to either allocate

118
00:05:06,180 --> 00:05:12,000
specific chunks or free them. A count so

119
00:05:09,180 --> 00:05:14,580
usually we would want to sprain drain

120
00:05:12,000 --> 00:05:16,860
count of size drain size and then drain

121
00:05:14,580 --> 00:05:19,080
them to free them then we have handles

122
00:05:16,860 --> 00:05:21,000
for different events that are used

123
00:05:19,080 --> 00:05:23,280
because we're going to use two threads to

124
00:05:21,000 --> 00:05:25,740
spray the named pipe because obviously we

125
00:05:23,280 --> 00:05:28,440
need one to read the chunk and the other

126
00:05:25,740 --> 00:05:29,699
one to write them in the first place so

127
00:05:28,440 --> 00:05:32,639
they're going to have to synchronize

128
00:05:29,699 --> 00:05:35,880
with each other's and so we have one

129
00:05:32,639 --> 00:05:37,860
handle to say okay I'm ready the name

130
00:05:35,880 --> 00:05:40,680
pipe has been created another one to say

131
00:05:37,860 --> 00:05:42,360
okay I'm ready to drain the the actual

132
00:05:40,680 --> 00:05:44,639
chunks and then the last one say okay

133
00:05:42,360 --> 00:05:46,199
the the drain is finished and then we

134
00:05:44,639 --> 00:05:49,259
have a CPU core because we're gonna

135
00:05:46,199 --> 00:05:52,620
actually add attach the thread to a

136
00:05:49,259 --> 00:05:54,180
specific core for spraying the named pipe

137
00:05:52,620 --> 00:05:57,020
chunks and then we have a buff size

138
00:05:54,180 --> 00:05:57,020
which we'll use

139
00:06:01,199 --> 00:06:05,520
okay

140
00:06:02,940 --> 00:06:07,500
so this is the function that will be

141
00:06:05,520 --> 00:06:10,259
executed into another thread which ends

142
00:06:07,500 --> 00:06:12,840
with underscore thread and so this main

143
00:06:10,259 --> 00:06:14,940
pipe server thread function is taken as

144
00:06:12,840 --> 00:06:18,500
an argument the named pipes server

145
00:06:14,940 --> 00:06:18,500
structure that we've just seen before

146
00:06:19,680 --> 00:06:22,340
this one

147
00:06:24,300 --> 00:06:29,220
and it's going to use all the variables

148
00:06:26,160 --> 00:06:31,560
stored into the structure in order to do

149
00:06:29,220 --> 00:06:34,020
its job so it's going to actually pin to

150
00:06:31,560 --> 00:06:36,539
the CPU core it's going to actually use

151
00:06:34,020 --> 00:06:39,539
the buff size which is basically an

152
00:06:36,539 --> 00:06:41,160
internal buffer used for storing the

153
00:06:39,539 --> 00:06:43,440
data of the named pipe so we're

154
00:06:41,160 --> 00:06:45,419
allocating the

155
00:06:43,440 --> 00:06:48,060
the buffer here and it's going to be

156
00:06:45,419 --> 00:06:50,580
used internally inside the named pipe so

157
00:06:48,060 --> 00:06:53,400
the CreateNamePipe function takes the

158
00:06:50,580 --> 00:06:55,500
input buffer size the output buffer size

159
00:06:53,400 --> 00:06:57,479
and here we create the named pipe as a

160
00:06:55,500 --> 00:07:00,319
duplex which allows us to read and write

161
00:06:57,479 --> 00:07:00,319
from the server

162
00:07:02,580 --> 00:07:07,740
once we have created the named pipe we

163
00:07:04,800 --> 00:07:10,259
set the events so the client knows the

164
00:07:07,740 --> 00:07:12,479
named pipe has been created and then we

165
00:07:10,259 --> 00:07:14,220
connect on the named pipe the server so

166
00:07:12,479 --> 00:07:16,800
then we go into a loop and we can see

167
00:07:14,220 --> 00:07:19,199
the first thing we do is we wait on the

168
00:07:16,800 --> 00:07:22,199
actual event to tell us that we are

169
00:07:19,199 --> 00:07:24,960
ready to drain to read the data so the

170
00:07:22,199 --> 00:07:27,360
client will actually write the data and

171
00:07:24,960 --> 00:07:29,220
the server will just read the data once

172
00:07:27,360 --> 00:07:32,160
it's told to read the data and we can

173
00:07:29,220 --> 00:07:34,620
see that basically it's going to loop a

174
00:07:32,160 --> 00:07:36,840
drain count times and each time it's

175
00:07:34,620 --> 00:07:38,940
going to read drain size and once it has

176
00:07:36,840 --> 00:07:41,580
done all of that it will actually set

177
00:07:38,940 --> 00:07:43,940
the event to say okay the drain has been

178
00:07:41,580 --> 00:07:43,940
done

179
00:07:46,740 --> 00:07:52,080
so the next function is the actual

180
00:07:48,780 --> 00:07:54,180
function initializing the named pipe

181
00:07:52,080 --> 00:07:56,160
thread itself so we can see it's

182
00:07:54,180 --> 00:07:58,919
creating the thread and passing the

183
00:07:56,160 --> 00:08:01,199
handler which we've just described and

184
00:07:58,919 --> 00:08:03,860
it's just initializing the variables on

185
00:08:01,199 --> 00:08:03,860
that thread

186
00:08:07,440 --> 00:08:13,800
so this function is used to spray name

187
00:08:11,699 --> 00:08:16,500
pipe chunks so this will be done by the

188
00:08:13,800 --> 00:08:20,280
client is taking the handle to the pipe

189
00:08:16,500 --> 00:08:23,699
to write the data and it's going to

190
00:08:20,280 --> 00:08:26,340
write spray counts chunks with the data

191
00:08:23,699 --> 00:08:28,740
pointed by chunk data and of the actual

192
00:08:26,340 --> 00:08:30,840
size length and so we can see it loops

193
00:08:28,740 --> 00:08:32,940
spray count times and it's going to

194
00:08:30,840 --> 00:08:36,680
write the data into the pipe and we have

195
00:08:32,940 --> 00:08:38,880
to do it by chunk len

196
00:08:36,680 --> 00:08:41,399
granularity just to make sure that the

197
00:08:38,880 --> 00:08:43,940
actual chunks are allocated at the of

198
00:08:41,399 --> 00:08:43,940
the right size

199
00:08:45,540 --> 00:08:50,459
so this is the client part to actually

200
00:08:47,720 --> 00:08:52,080
connect to the named pipe so we see it's

201
00:08:50,459 --> 00:08:54,300
going to actually call the create file

202
00:08:52,080 --> 00:08:56,760
function with the read write permissions

203
00:08:54,300 --> 00:08:59,640
even though we the client will only use

204
00:08:56,760 --> 00:09:01,620
the write permission

205
00:08:59,640 --> 00:09:04,200
so now we have the named pipe feng shui

206
00:09:01,620 --> 00:09:09,120
function so this function is going to

207
00:09:04,200 --> 00:09:12,120
actually allocate adjacent named pipes

208
00:09:09,120 --> 00:09:15,420
and it's going to then create holes and

209
00:09:12,120 --> 00:09:18,180
then the goal is going to be to refill

210
00:09:15,420 --> 00:09:20,160
the holes with actual KENLISTMENTs but

211
00:09:18,180 --> 00:09:22,080
this function is just about manipulating

212
00:09:20,160 --> 00:09:24,480
the heap to actually have this

213
00:09:22,080 --> 00:09:26,700
interweaving named pipes and then

214
00:09:24,480 --> 00:09:29,640
creating holes

215
00:09:26,700 --> 00:09:31,860
so we see it takes a count which is the

216
00:09:29,640 --> 00:09:34,399
number of named pipes that we want to

217
00:09:31,860 --> 00:09:37,140
spray and it's taking also an extra

218
00:09:34,399 --> 00:09:39,720
count that will be used to actually

219
00:09:37,140 --> 00:09:42,240
initially spray named pipes in order to

220
00:09:39,720 --> 00:09:43,560
fill all the holes so we see lots of

221
00:09:42,240 --> 00:09:46,620
different variables we see the different

222
00:09:43,560 --> 00:09:51,060
pipe names for both pipes

223
00:09:46,620 --> 00:09:53,880
it's using a spray len of 200 hex

224
00:09:51,060 --> 00:09:56,640
so we can see it uses the previously

225
00:09:53,880 --> 00:09:59,940
described functions to initialize an

226
00:09:56,640 --> 00:10:01,980
named pipe and then connect to it as a

227
00:09:59,940 --> 00:10:04,080
client so this will be the server part

228
00:10:01,980 --> 00:10:06,420
and this will be the client part and

229
00:10:04,080 --> 00:10:10,500
obviously the client part will wait for

230
00:10:06,420 --> 00:10:12,120
the ready event to be initialized before

231
00:10:10,500 --> 00:10:14,580
connecting to it

232
00:10:12,120 --> 00:10:18,300
so here the goal is going to be to

233
00:10:14,580 --> 00:10:20,339
actually write data into the buffers so

234
00:10:18,300 --> 00:10:22,320
the one we see here so we're going to

235
00:10:20,339 --> 00:10:26,420
have to allocate these buffers and just

236
00:10:22,320 --> 00:10:26,420
write some data like A's or B's

237
00:10:27,959 --> 00:10:34,800
then we can see here that this loop will

238
00:10:31,740 --> 00:10:36,600
actually use only the first byte in

239
00:10:34,800 --> 00:10:39,480
order to fill all the holes on the heap

240
00:10:36,600 --> 00:10:42,360
so we're using the extra count in order

241
00:10:39,480 --> 00:10:44,700
to loop extra times in order to fill all

242
00:10:42,360 --> 00:10:47,600
the holes at least most of the holes as

243
00:10:44,700 --> 00:10:47,600
much as we can

244
00:10:47,760 --> 00:10:53,519
and then we see that it's printing some

245
00:10:51,180 --> 00:10:55,500
message to say okay we've just sprayed a

246
00:10:53,519 --> 00:10:57,300
bunch of named pipe writes and so this is

247
00:10:55,500 --> 00:10:59,459
what we've just done so we can analyze

248
00:10:57,300 --> 00:11:01,860
the layout on in the debugger so then

249
00:10:59,459 --> 00:11:04,620
the goal is going to be to actually loop

250
00:11:01,860 --> 00:11:07,980
count times and use the write file

251
00:11:04,620 --> 00:11:10,740
function in order to spray chunks using

252
00:11:07,980 --> 00:11:12,839
named pipes and doing it alternatively on

253
00:11:10,740 --> 00:11:14,940
the first named pipe and the second name

254
00:11:12,839 --> 00:11:18,120
pipe and then we see that there is a

255
00:11:14,940 --> 00:11:21,060
pause and it gives us time to analyze the

256
00:11:18,120 --> 00:11:23,940
sprayed alternative named pipes chunks on

257
00:11:21,060 --> 00:11:26,640
the heap in the debugger

258
00:11:23,940 --> 00:11:31,140
finally we can see some code to actually

259
00:11:26,640 --> 00:11:34,500
set an event to drain the chunks the

260
00:11:31,140 --> 00:11:36,720
data from the named pipe 2. so by setting

261
00:11:34,500 --> 00:11:40,579
these events the server thread will

262
00:11:36,720 --> 00:11:40,579
actually loop as we've seen before

263
00:11:47,820 --> 00:11:52,019
because here it was waiting on the drain

264
00:11:50,279 --> 00:11:54,060
event so it's going to loop and read the

265
00:11:52,019 --> 00:11:57,560
data and so then it's going to set the

266
00:11:54,060 --> 00:11:57,560
drain done event

267
00:12:02,579 --> 00:12:08,279
and so here the code is actually waiting

268
00:12:05,160 --> 00:12:09,300
for the drain done event handle so once

269
00:12:08,279 --> 00:12:11,700
it's done it's going to continue

270
00:12:09,300 --> 00:12:13,980
execution and so it can just print that

271
00:12:11,700 --> 00:12:16,920
it has created a bunch of named pipe

272
00:12:13,980 --> 00:12:18,720
holes by reading the chunks from the

273
00:12:16,920 --> 00:12:21,860
second named pipe so we should be able to

274
00:12:18,720 --> 00:12:21,860
analyze the holes

275
00:12:22,800 --> 00:12:27,120
so then we can analyze the main function

276
00:12:25,079 --> 00:12:30,420
so we see we define an enlistment

277
00:12:27,120 --> 00:12:32,820
count of 5000 hex

278
00:12:30,420 --> 00:12:34,620
and the code is very similar to what

279
00:12:32,820 --> 00:12:36,839
we've seen so far where we create all

280
00:12:34,620 --> 00:12:39,740
the KTM objects to have something we can

281
00:12:36,839 --> 00:12:39,740
use to work with

282
00:12:39,899 --> 00:12:44,220
so obviously the goal is to spray

283
00:12:41,640 --> 00:12:46,079
alternating named pipe structures free

284
00:12:44,220 --> 00:12:49,139
one set of structures in one of the name

285
00:12:46,079 --> 00:12:53,180
pipe and then leave the other intact and

286
00:12:49,139 --> 00:12:53,180
then observe the resulting hole layout

287
00:12:53,339 --> 00:12:58,680
so all the objects are very similar

288
00:12:56,639 --> 00:13:02,100
here we can see it called the function

289
00:12:58,680 --> 00:13:05,160
doing the spray and the actual holes and

290
00:13:02,100 --> 00:13:08,399
it's using enlistment count and only 100

291
00:13:05,160 --> 00:13:10,380
hex extra in the first place to fill all

292
00:13:08,399 --> 00:13:12,240
the holes and so once you've done that

293
00:13:10,380 --> 00:13:15,180
and you've analyzed the layout is is

294
00:13:12,240 --> 00:13:18,180
good you can actually also add code to

295
00:13:15,180 --> 00:13:19,800
allocate a bunch of enlistments

296
00:13:18,180 --> 00:13:21,360
um hoping to feel all the holes that

297
00:13:19,800 --> 00:13:23,279
have just been created and then you

298
00:13:21,360 --> 00:13:26,639
should be able to analyze a layout with

299
00:13:23,279 --> 00:13:31,160
KENLISTMENTs adjacent to named pipe

300
00:13:26,639 --> 00:13:31,160
chunks and so yeah now it's your turn

