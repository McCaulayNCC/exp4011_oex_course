1
00:00:01,979 --> 00:00:06,660
in this part we are going to look into

2
00:00:04,440 --> 00:00:08,580
how we can effectively win the race

3
00:00:06,660 --> 00:00:12,240
condition without the help of the

4
00:00:08,580 --> 00:00:15,120
debugger well finally Yeah so basically

5
00:00:12,240 --> 00:00:17,460
we still want to target the same

6
00:00:15,120 --> 00:00:20,279
function that we were patching the

7
00:00:17,460 --> 00:00:23,400
debugger which is ke wait for single

8
00:00:20,279 --> 00:00:26,039
objects and in order to maximize success

9
00:00:23,400 --> 00:00:29,400
for winning the race condition we want

10
00:00:26,039 --> 00:00:33,000
to be able to congest the mutex that

11
00:00:29,400 --> 00:00:36,000
this API call is trying to log on which

12
00:00:33,000 --> 00:00:39,059
is the resource manager of mutex and

13
00:00:36,000 --> 00:00:40,980
there is basically a trick that we ended

14
00:00:39,059 --> 00:00:43,559
up finding out about which is

15
00:00:40,980 --> 00:00:46,739
interesting to know on Windows which is

16
00:00:43,559 --> 00:00:50,640
that if a thread in the Windows kernel

17
00:00:46,739 --> 00:00:53,640
is blocked on something like a mutex and

18
00:00:50,640 --> 00:00:56,399
you request from username that this

19
00:00:53,640 --> 00:00:59,640
thread is suspected the thread will

20
00:00:56,399 --> 00:01:02,760
basically enter into a suspended state

21
00:00:59,640 --> 00:01:06,420
while it's blocked on whatever it was

22
00:01:02,760 --> 00:01:09,180
waiting for until you explicitly wake it

23
00:01:06,420 --> 00:01:12,000
back up and so that's basically the idea

24
00:01:09,180 --> 00:01:15,240
we will try to figure out a way to make

25
00:01:12,000 --> 00:01:16,740
it hard for the recovery thread in the

26
00:01:15,240 --> 00:01:20,159
vulnerable function

27
00:01:16,740 --> 00:01:23,040
to actually lock the mutex when calling

28
00:01:20,159 --> 00:01:26,159
ke wait for a single object so that it's

29
00:01:23,040 --> 00:01:29,400
waiting for it and at the same time we

30
00:01:26,159 --> 00:01:32,100
will try to suspend that strand so that

31
00:01:29,400 --> 00:01:35,400
it just hits a point where it will just

32
00:01:32,100 --> 00:01:39,299
be asleep and at that point

33
00:01:35,400 --> 00:01:42,240
we free decanishments and think we might

34
00:01:39,299 --> 00:01:45,659
have won the race and then we only wake

35
00:01:42,240 --> 00:01:48,060
the shred up after so at the end it

36
00:01:45,659 --> 00:01:51,540
actually becomes fairly similar to what

37
00:01:48,060 --> 00:01:54,540
we were doing with patching with WinDbg

38
00:01:51,540 --> 00:01:56,640
which is yet another reason why it was

39
00:01:54,540 --> 00:01:59,100
worth using that approach in the

40
00:01:56,640 --> 00:02:01,979
debugger in the first place and so

41
00:01:59,100 --> 00:02:04,320
interestingly if we suspend the recovery

42
00:02:01,979 --> 00:02:07,439
thread and we fail to win the race

43
00:02:04,320 --> 00:02:10,140
condition insofar as this thread

44
00:02:07,439 --> 00:02:13,680
suspended at a different blocking points

45
00:02:10,140 --> 00:02:15,900
and not at the KE wait for single object

46
00:02:13,680 --> 00:02:20,280
function we want to actually block on

47
00:02:15,900 --> 00:02:22,739
it's okay because we will just free an

48
00:02:20,280 --> 00:02:25,020
enlistment from user land that is not

49
00:02:22,739 --> 00:02:28,379
going to result into a use after three

50
00:02:25,020 --> 00:02:30,239
and it won't have any side effects and

51
00:02:28,379 --> 00:02:33,060
the vulnerable loop will just keep

52
00:02:30,239 --> 00:02:34,680
looping and process another announcement

53
00:02:33,060 --> 00:02:37,680
so

54
00:02:34,680 --> 00:02:38,879
we're gonna go over actually how it

55
00:02:37,680 --> 00:02:41,280
works

56
00:02:38,879 --> 00:02:43,680
we are going to look into where we can

57
00:02:41,280 --> 00:02:46,080
actually have the recovery thread

58
00:02:43,680 --> 00:02:48,120
blocked so we can effectively win the

59
00:02:46,080 --> 00:02:50,280
race condition we are going to see how

60
00:02:48,120 --> 00:02:51,840
we can actually congest the resource

61
00:02:50,280 --> 00:02:55,019
measure mutex

62
00:02:51,840 --> 00:02:57,060
so we increase the chance of winning the

63
00:02:55,019 --> 00:03:00,300
race condition we are going to see how

64
00:02:57,060 --> 00:03:02,280
we can actually block the recovery

65
00:03:00,300 --> 00:03:04,920
thread forever

66
00:03:02,280 --> 00:03:07,980
until we decide to unblock it

67
00:03:04,920 --> 00:03:11,099
which is obviously ideal from an

68
00:03:07,980 --> 00:03:12,540
attacker perspective and then we'll do a

69
00:03:11,099 --> 00:03:15,420
quick animation

70
00:03:12,540 --> 00:03:17,519
to summarize how everything works okay

71
00:03:15,420 --> 00:03:21,000
let's get started

72
00:03:17,519 --> 00:03:23,340
so this is the important code in the TM

73
00:03:21,000 --> 00:03:25,680
recover resource manager vulnerable

74
00:03:23,340 --> 00:03:28,319
function so we basically want to be

75
00:03:25,680 --> 00:03:31,440
suspending the recovery thread in this

76
00:03:28,319 --> 00:03:34,620
yellow region and the reason for this is

77
00:03:31,440 --> 00:03:38,640
we want the B management is finalized

78
00:03:34,620 --> 00:03:41,819
flag to be set to zero still because the

79
00:03:38,640 --> 00:03:44,879
recovery thread hasn't detected that the

80
00:03:41,819 --> 00:03:48,180
alicement is finalized during this if

81
00:03:44,879 --> 00:03:50,220
condition due to it not being final at

82
00:03:48,180 --> 00:03:52,860
yet and the whole point of trying to

83
00:03:50,220 --> 00:03:55,799
suspend the recovery thread in this

84
00:03:52,860 --> 00:03:58,680
yellow region is that if then we are

85
00:03:55,799 --> 00:04:01,319
able to finalize the announcements from

86
00:03:58,680 --> 00:04:03,959
another thread which will free the

87
00:04:01,319 --> 00:04:06,840
amusement and then we unblock the

88
00:04:03,959 --> 00:04:09,480
recovery thread from somewhere in this

89
00:04:06,840 --> 00:04:11,939
Regional region then the B and

90
00:04:09,480 --> 00:04:15,959
instrument is finalized flight will be

91
00:04:11,939 --> 00:04:19,139
zero still due to the if condition being

92
00:04:15,959 --> 00:04:21,959
done before the year region and so

93
00:04:19,139 --> 00:04:26,400
during the following if condition the

94
00:04:21,959 --> 00:04:29,060
execution will go into the else case and

95
00:04:26,400 --> 00:04:32,820
so it will the reference free memory

96
00:04:29,060 --> 00:04:35,759
resisting in a user3 and so in the

97
00:04:32,820 --> 00:04:38,040
yellow region the most promising line to

98
00:04:35,759 --> 00:04:41,280
actually Force the recovery thread to

99
00:04:38,040 --> 00:04:44,100
block is actually the KE word for single

100
00:04:41,280 --> 00:04:47,880
object call and so the way to check that

101
00:04:44,100 --> 00:04:51,360
to look at all the possible lines the

102
00:04:47,880 --> 00:04:53,759
first call in this yellow region is up

103
00:04:51,360 --> 00:04:56,880
the reference object which is basically

104
00:04:53,759 --> 00:04:59,160
going to change the object header before

105
00:04:56,880 --> 00:05:01,680
the K management allocation which is

106
00:04:59,160 --> 00:05:04,740
kind of a basic operation and doesn't

107
00:05:01,680 --> 00:05:07,620
typically rely on any blocking States

108
00:05:04,740 --> 00:05:08,940
then there is the KE wait for a single

109
00:05:07,620 --> 00:05:11,699
object function

110
00:05:08,940 --> 00:05:14,699
which is typically useful because it

111
00:05:11,699 --> 00:05:16,979
means the thread might be interrupted by

112
00:05:14,699 --> 00:05:19,800
the scheduler in order to execute other

113
00:05:16,979 --> 00:05:22,620
threads and finally there is this if

114
00:05:19,800 --> 00:05:25,320
condition which again is kind of a basic

115
00:05:22,620 --> 00:05:27,419
and fast operation and so typically we

116
00:05:25,320 --> 00:05:30,600
would want to congest the resource

117
00:05:27,419 --> 00:05:33,539
manager mutex so the actual ke waveform

118
00:05:30,600 --> 00:05:35,820
single object call takes more time and

119
00:05:33,539 --> 00:05:38,100
is more likely to be interrupted by the

120
00:05:35,820 --> 00:05:40,259
scheduler so how can we convert the

121
00:05:38,100 --> 00:05:42,240
resource manager's mutex there is a

122
00:05:40,259 --> 00:05:45,240
function that is exposed to username

123
00:05:42,240 --> 00:05:49,440
called NT query information resource

124
00:05:45,240 --> 00:05:51,000
manager and you might not recall but in

125
00:05:49,440 --> 00:05:53,699
the Care Resource Management structure

126
00:05:51,000 --> 00:05:57,000
which we described in the KTM section

127
00:05:53,699 --> 00:05:58,919
there is a description field associated

128
00:05:57,000 --> 00:06:00,900
with the resource manager and so

129
00:05:58,919 --> 00:06:04,080
basically when you create the resource

130
00:06:00,900 --> 00:06:07,320
manager from username you can provide a

131
00:06:04,080 --> 00:06:09,780
Unicode string and say that this is the

132
00:06:07,320 --> 00:06:12,120
name of the resource manager and this

133
00:06:09,780 --> 00:06:14,580
name will be saved into the description

134
00:06:12,120 --> 00:06:17,160
field of the Care Resource manager

135
00:06:14,580 --> 00:06:18,600
structure and so what this NT query

136
00:06:17,160 --> 00:06:22,080
information resource manager function

137
00:06:18,600 --> 00:06:24,600
does is it actually queries that

138
00:06:22,080 --> 00:06:27,180
description string from the UK resource

139
00:06:24,600 --> 00:06:29,639
manager structure and pass it back to

140
00:06:27,180 --> 00:06:32,220
username but as we can see highlighted

141
00:06:29,639 --> 00:06:35,039
in yellow one of the first things that

142
00:06:32,220 --> 00:06:38,340
this function does is it lots that

143
00:06:35,039 --> 00:06:41,699
resource manager's mutex and then once

144
00:06:38,340 --> 00:06:44,819
it has locked it it tries to grab the

145
00:06:41,699 --> 00:06:46,979
description information and pass it back

146
00:06:44,819 --> 00:06:49,139
to user land and so because we can log

147
00:06:46,979 --> 00:06:52,139
the mutex from this call we can

148
00:06:49,139 --> 00:06:54,780
basically have a dedicated thread in our

149
00:06:52,139 --> 00:06:57,479
exploits that is running on a different

150
00:06:54,780 --> 00:06:59,880
processor core that the one that is in

151
00:06:57,479 --> 00:07:02,759
the vulnerable loop in the kernel and we

152
00:06:59,880 --> 00:07:05,039
can also adjust the sort of execution

153
00:07:02,759 --> 00:07:06,960
priority of this other thread and

154
00:07:05,039 --> 00:07:09,780
basically just have it in a really tight

155
00:07:06,960 --> 00:07:11,759
loop that keeps asking the camera what

156
00:07:09,780 --> 00:07:14,880
is the description of this resource

157
00:07:11,759 --> 00:07:18,479
manager and so what's going to happen is

158
00:07:14,880 --> 00:07:20,940
it's just going to constantly be locking

159
00:07:18,479 --> 00:07:23,819
the resource manager mutex and also

160
00:07:20,940 --> 00:07:25,860
because it's at higher priority it's

161
00:07:23,819 --> 00:07:28,259
going to be getting scheduled

162
00:07:25,860 --> 00:07:30,360
potentially more often than the thread

163
00:07:28,259 --> 00:07:32,880
in the vulnerable function and so there

164
00:07:30,360 --> 00:07:35,220
is a higher probability that it will

165
00:07:32,880 --> 00:07:37,259
actually be congesting the thread in the

166
00:07:35,220 --> 00:07:39,840
renewable function and basically what

167
00:07:37,259 --> 00:07:42,060
this does is it increases the

168
00:07:39,840 --> 00:07:44,699
probability that the vulnerable stripe

169
00:07:42,060 --> 00:07:47,880
won't be able to get the lock and so it

170
00:07:44,699 --> 00:07:51,000
will just be blocked waiting on it there

171
00:07:47,880 --> 00:07:53,759
is a separate API that is exposed on

172
00:07:51,000 --> 00:07:55,620
Windows called suspense thread and it's

173
00:07:53,759 --> 00:07:59,460
interesting because you can basically

174
00:07:55,620 --> 00:08:01,680
just close a thread to stop running at

175
00:07:59,460 --> 00:08:04,680
the next sort of available blocking

176
00:08:01,680 --> 00:08:07,139
point and so inside the kernel these are

177
00:08:04,680 --> 00:08:09,599
typically things like new taxes where

178
00:08:07,139 --> 00:08:12,120
you have to wait for some reasons

179
00:08:09,599 --> 00:08:14,580
available anyway so why not just

180
00:08:12,120 --> 00:08:16,800
schedule another thread at the same time

181
00:08:14,580 --> 00:08:19,680
right and so the channel is very likely

182
00:08:16,800 --> 00:08:22,500
to sustain the recovery thread if we are

183
00:08:19,680 --> 00:08:24,000
able to congest the mutex by creating

184
00:08:22,500 --> 00:08:26,879
the description field from another

185
00:08:24,000 --> 00:08:29,580
thread and then we call suspense right

186
00:08:26,879 --> 00:08:32,159
however it would be nice if we had a way

187
00:08:29,580 --> 00:08:35,339
to actually detect if this thread was

188
00:08:32,159 --> 00:08:37,500
successfully suspended I guess it's

189
00:08:35,339 --> 00:08:39,000
worth noting that the antique query

190
00:08:37,500 --> 00:08:41,219
information resource manager function

191
00:08:39,000 --> 00:08:45,000
really tell previously was kind of

192
00:08:41,219 --> 00:08:47,940
hinted at in the prosperous key blog

193
00:08:45,000 --> 00:08:50,940
originally we didn't really know what it

194
00:08:47,940 --> 00:08:53,640
did but we reversed it and then we saw

195
00:08:50,940 --> 00:08:55,860
okay it's blocking the mutex and this

196
00:08:53,640 --> 00:08:57,899
seems to be useful for congesting the

197
00:08:55,860 --> 00:08:59,640
recovery thread at the right place and

198
00:08:57,899 --> 00:09:02,880
so there was actually another function

199
00:08:59,640 --> 00:09:05,459
that they mentioned which I first didn't

200
00:09:02,880 --> 00:09:07,920
really make a lot of sense to us

201
00:09:05,459 --> 00:09:09,959
because we originally didn't realize

202
00:09:07,920 --> 00:09:12,120
that we would have to rely on straight

203
00:09:09,959 --> 00:09:14,760
suspension in order to win the race

204
00:09:12,120 --> 00:09:17,580
effectively but basically there's a

205
00:09:14,760 --> 00:09:20,040
trick that you can use to tell once a

206
00:09:17,580 --> 00:09:23,580
thread is actually suspended which means

207
00:09:20,040 --> 00:09:27,779
that you know it's blocked somewhere and

208
00:09:23,580 --> 00:09:30,060
so it is using a system called called LT

209
00:09:27,779 --> 00:09:34,080
query straight information and you can

210
00:09:30,060 --> 00:09:37,800
pass a special field to it called thread

211
00:09:34,080 --> 00:09:40,320
last syscall and basically what it does is

212
00:09:37,800 --> 00:09:42,779
it will tell you the last system call

213
00:09:40,320 --> 00:09:45,240
that was executed in a thread that is

214
00:09:42,779 --> 00:09:47,820
suspended and so if you call it on a

215
00:09:45,240 --> 00:09:49,500
thread that it is not suspended you'll

216
00:09:47,820 --> 00:09:52,320
get an error and if the thread is

217
00:09:49,500 --> 00:09:55,680
suspended it will actually tell you the

218
00:09:52,320 --> 00:09:59,160
last syscall and so we can see here that

219
00:09:55,680 --> 00:10:01,560
inside of the first part in yellow in NT

220
00:09:59,160 --> 00:10:04,800
grade straight information there is this

221
00:10:01,560 --> 00:10:06,839
PSP query last call thread function

222
00:10:04,800 --> 00:10:09,060
being called and then inside that

223
00:10:06,839 --> 00:10:11,580
function basically there is some logic

224
00:10:09,060 --> 00:10:14,300
that is checking the e-stret structure

225
00:10:11,580 --> 00:10:16,920
TCP field which is the case rate

226
00:10:14,300 --> 00:10:20,940
embedded into each red and then you

227
00:10:16,920 --> 00:10:23,880
check the state to see if the thread is

228
00:10:20,940 --> 00:10:25,980
actually suspended and if not it's going

229
00:10:23,880 --> 00:10:28,500
to return the status and successful

230
00:10:25,980 --> 00:10:31,980
error and if the thread is effectively

231
00:10:28,500 --> 00:10:35,100
suspended it will actually update the

232
00:10:31,980 --> 00:10:37,800
last executed syscall in the return shred

233
00:10:35,100 --> 00:10:40,800
information structure and the actual

234
00:10:37,800 --> 00:10:42,779
syscall will return zero indicating CSX

235
00:10:40,800 --> 00:10:45,240
and so basically now we need three

236
00:10:42,779 --> 00:10:47,640
strides what we have is we have the

237
00:10:45,240 --> 00:10:49,740
original thread that is looping in the

238
00:10:47,640 --> 00:10:52,740
vulnerable function we can have a second

239
00:10:49,740 --> 00:10:55,500
thread can ingesting the mutex with NT

240
00:10:52,740 --> 00:10:57,779
query information resource manager and a

241
00:10:55,500 --> 00:11:01,620
third thread telling the recovery

242
00:10:57,779 --> 00:11:04,740
reliable thread to suspend with these

243
00:11:01,620 --> 00:11:07,320
suspense thread API and then just

244
00:11:04,740 --> 00:11:10,560
waiting until the thread is actually

245
00:11:07,320 --> 00:11:12,660
suspended by calling this NT query

246
00:11:10,560 --> 00:11:16,200
straight information function over an

247
00:11:12,660 --> 00:11:19,079
order until we get the system call back

248
00:11:16,200 --> 00:11:21,899
without error and then only at that

249
00:11:19,079 --> 00:11:24,180
point we try to finalize and free the

250
00:11:21,899 --> 00:11:26,880
announcements because we're hoping that

251
00:11:24,180 --> 00:11:29,279
the recovery thread is blocked on the KE

252
00:11:26,880 --> 00:11:31,980
wait for single object function that

253
00:11:29,279 --> 00:11:34,680
makes it weight on the resource manager

254
00:11:31,980 --> 00:11:36,839
mutex and so basically what this looks

255
00:11:34,680 --> 00:11:38,579
like is at the bottom we have the

256
00:11:36,839 --> 00:11:41,339
different threads we create in user

257
00:11:38,579 --> 00:11:44,279
land that we name congest threads

258
00:11:41,339 --> 00:11:46,860
suspense thread and Recovery thread and

259
00:11:44,279 --> 00:11:49,920
so for now we have the recovery thread

260
00:11:46,860 --> 00:11:52,200
that triggered the syscall and reached the

261
00:11:49,920 --> 00:11:54,480
vulnerable function in the kernel and so

262
00:11:52,200 --> 00:11:56,399
this thread is stuck in the kernel and

263
00:11:54,480 --> 00:11:59,640
this recovery thread is going to be

264
00:11:56,399 --> 00:12:02,160
running and passing those kneespins

265
00:11:59,640 --> 00:12:04,620
starting from the Edison head and so

266
00:12:02,160 --> 00:12:08,100
we've got our normal sort of linked list

267
00:12:04,620 --> 00:12:10,500
of candisements based on the K resource

268
00:12:08,100 --> 00:12:13,320
manager that we've been dealing with in

269
00:12:10,500 --> 00:12:16,200
most graphs now so it should be familiar

270
00:12:13,320 --> 00:12:19,079
to you yeah and so at some points we

271
00:12:16,200 --> 00:12:21,480
have the normal operation where the

272
00:12:19,079 --> 00:12:24,959
recovery thread is locking the care

273
00:12:21,480 --> 00:12:28,800
resource manager's mutex and so it tests

274
00:12:24,959 --> 00:12:31,620
the current Amusement is notifiable and

275
00:12:28,800 --> 00:12:35,220
finalized and then it unlocks the mutex

276
00:12:31,620 --> 00:12:37,680
in order to cue the notification and

277
00:12:35,220 --> 00:12:41,399
when the notification is cured we just

278
00:12:37,680 --> 00:12:43,680
represent it by a little gray block on

279
00:12:41,399 --> 00:12:47,399
the queue and then the thread will

280
00:12:43,680 --> 00:12:50,040
attempt to relog the mutex so if while

281
00:12:47,399 --> 00:12:53,339
this recovery thread is doing all of

282
00:12:50,040 --> 00:12:55,800
that normal operation in the candle we

283
00:12:53,339 --> 00:12:58,440
have another thread that we called the

284
00:12:55,800 --> 00:13:01,200
congestrate and this congestion thread

285
00:12:58,440 --> 00:13:04,680
is just spamming NT query information

286
00:13:01,200 --> 00:13:07,860
resource measure syscall asking what is

287
00:13:04,680 --> 00:13:11,760
the description of that resource manager

288
00:13:07,860 --> 00:13:15,180
and so what's going to happen is it is

289
00:13:11,760 --> 00:13:18,300
just going to be locking that exact same

290
00:13:15,180 --> 00:13:21,660
mutex that the recovery thread is also

291
00:13:18,300 --> 00:13:24,300
trying to get a hold of and again at the

292
00:13:21,660 --> 00:13:26,579
same time we have another thread that we

293
00:13:24,300 --> 00:13:29,519
call the suspense trap that is telling

294
00:13:26,579 --> 00:13:31,920
the recovery threat to go to sleep and

295
00:13:29,519 --> 00:13:35,880
to actually suspend by calling the

296
00:13:31,920 --> 00:13:38,519
suspense thread API and then just asking

297
00:13:35,880 --> 00:13:41,339
the kernel over and over is it suspended

298
00:13:38,519 --> 00:13:44,459
yet is it suspended yet by calling the

299
00:13:41,339 --> 00:13:47,760
NT query thread information API and so

300
00:13:44,459 --> 00:13:50,459
what will happen is like the recovery

301
00:13:47,760 --> 00:13:52,980
thread will execute a few times

302
00:13:50,459 --> 00:13:55,740
in the loop and it will be queuing more

303
00:13:52,980 --> 00:13:59,100
and more notifications and then suddenly

304
00:13:55,740 --> 00:14:00,899
the thread will actually be suspended at

305
00:13:59,100 --> 00:14:03,779
some point in loop we don't know where

306
00:14:00,899 --> 00:14:06,120
we don't know for sure if it's where we

307
00:14:03,779 --> 00:14:09,079
want it to be but we can detect from

308
00:14:06,120 --> 00:14:12,420
username that the recovery thread

309
00:14:09,079 --> 00:14:15,240
suspended using the NT query thread

310
00:14:12,420 --> 00:14:19,380
information syscall and asking for the

311
00:14:15,240 --> 00:14:22,139
threadless syscall and then from there we

312
00:14:19,380 --> 00:14:24,839
do what we were doing earlier when we're

313
00:14:22,139 --> 00:14:27,980
patching things we are going back which

314
00:14:24,839 --> 00:14:30,839
is we simply just count the number of

315
00:14:27,980 --> 00:14:32,519
notifications in the notification queue

316
00:14:30,839 --> 00:14:35,519
since we can do that from user land

317
00:14:32,519 --> 00:14:38,279
and it allows us to correlate to a

318
00:14:35,519 --> 00:14:41,279
specific care instrument so in this case

319
00:14:38,279 --> 00:14:44,880
it would be the fourth Key Management

320
00:14:41,279 --> 00:14:47,519
due to four notifications and then we

321
00:14:44,880 --> 00:14:50,160
free this specific care instruments from

322
00:14:47,519 --> 00:14:52,320
user land again the same as what we

323
00:14:50,160 --> 00:14:54,959
were doing previously when we were

324
00:14:52,320 --> 00:14:58,320
patching things via went back using

325
00:14:54,959 --> 00:15:01,079
commit completes and close handle and

326
00:14:58,320 --> 00:15:03,600
now what we can do is we we can replace

327
00:15:01,079 --> 00:15:07,920
the freight canisterment

328
00:15:03,600 --> 00:15:10,440
with some fake enlistment data using the

329
00:15:07,920 --> 00:15:13,500
named pipe rights and basically what

330
00:15:10,440 --> 00:15:16,199
will happen is either it was the correct

331
00:15:13,500 --> 00:15:19,860
spots that we suspended the recovery

332
00:15:16,199 --> 00:15:23,160
thread on and we won the race and so we

333
00:15:19,860 --> 00:15:27,060
can detect it as the kernel modified our

334
00:15:23,160 --> 00:15:29,399
user land fake amusements or it was

335
00:15:27,060 --> 00:15:31,680
the wrong spot where we suspended the

336
00:15:29,399 --> 00:15:34,740
recovery straight on we still have some

337
00:15:31,680 --> 00:15:37,320
enlistments that was finalized and freed

338
00:15:34,740 --> 00:15:39,839
but it didn't trigger any use after free

339
00:15:37,320 --> 00:15:42,540
and so in this case basically what will

340
00:15:39,839 --> 00:15:45,240
have happened is the care instrument

341
00:15:42,540 --> 00:15:48,839
will just be removed from the linked

342
00:15:45,240 --> 00:15:50,220
list or detected as finalized as at a

343
00:15:48,839 --> 00:15:53,399
different stage

344
00:15:50,220 --> 00:15:55,980
and this enlistment will just be skipped

345
00:15:53,399 --> 00:15:58,500
from that moment and so we'll just do

346
00:15:55,980 --> 00:16:00,839
this attire process with the different

347
00:15:58,500 --> 00:16:03,660
strength over and over and over again

348
00:16:00,839 --> 00:16:06,540
until we actually detect from usermen

349
00:16:03,660 --> 00:16:09,740
that we want the race condition and that

350
00:16:06,540 --> 00:16:12,480
the kernel modified our fake

351
00:16:09,740 --> 00:16:15,540
candacements in user land and in the

352
00:16:12,480 --> 00:16:18,180
course of exploitation sometimes that

353
00:16:15,540 --> 00:16:20,760
entire process can happen hundreds of

354
00:16:18,180 --> 00:16:23,459
times that you go through this and other

355
00:16:20,760 --> 00:16:26,639
times you'll account and you get the

356
00:16:23,459 --> 00:16:32,180
exploit working after like two trials or

357
00:16:26,639 --> 00:16:32,180
one try even so your mild age may vary

