1
00:00:00,060 --> 00:00:05,220
in this video we're going to talk about

2
00:00:02,159 --> 00:00:07,680
the delayed free mechanism which is a

3
00:00:05,220 --> 00:00:10,260
mitigation in the kernel and how to

4
00:00:07,680 --> 00:00:12,660
bypass it so basically there is like a

5
00:00:10,260 --> 00:00:16,139
special list of free chunks which they

6
00:00:12,660 --> 00:00:18,960
call lookaside list and it's really just

7
00:00:16,139 --> 00:00:21,180
a cache of chunks that have recently

8
00:00:18,960 --> 00:00:24,060
been requested to be freed by some code

9
00:00:21,180 --> 00:00:26,519
but these chunks aren't actually freed

10
00:00:24,060 --> 00:00:28,619
in the real sense which is that they

11
00:00:26,519 --> 00:00:30,779
will be treated like any other free

12
00:00:28,619 --> 00:00:33,180
chunk basically the kernel heap

13
00:00:30,779 --> 00:00:35,640
algorithm can say oh someone is

14
00:00:33,180 --> 00:00:38,280
requesting a new allocation let me first

15
00:00:35,640 --> 00:00:40,379
check this look aside list to see if

16
00:00:38,280 --> 00:00:44,520
something that was recently freed

17
00:00:40,379 --> 00:00:48,180
matches the size requested and

18
00:00:44,520 --> 00:00:50,520
this algorithm might pick a chain

19
00:00:48,180 --> 00:00:52,980
candidate from that list instead of

20
00:00:50,520 --> 00:00:55,739
using an actual free Chunk from the

21
00:00:52,980 --> 00:00:58,680
other regular free list so in that sense

22
00:00:55,739 --> 00:01:01,079
it is like a normal free list except it

23
00:00:58,680 --> 00:01:03,660
has a special role but there are other

24
00:01:01,079 --> 00:01:06,060
scenarios where it acts totally

25
00:01:03,660 --> 00:01:08,580
differently from a regular list for

26
00:01:06,060 --> 00:01:09,900
instance if the lookaside list ends up

27
00:01:08,580 --> 00:01:12,540
getting full

28
00:01:09,900 --> 00:01:15,420
and a new check is freed in a normal

29
00:01:12,540 --> 00:01:17,700
scenario the chunk would be added to the

30
00:01:15,420 --> 00:01:20,939
lockaside list but because here there is

31
00:01:17,700 --> 00:01:23,340
no space left on the lockaside list what

32
00:01:20,939 --> 00:01:26,220
will happen is that the heap allocator

33
00:01:23,340 --> 00:01:29,100
will finally actually free all of the

34
00:01:26,220 --> 00:01:31,979
chunks on that lockaside list and only

35
00:01:29,100 --> 00:01:35,100
at that point can things like coalescing

36
00:01:31,979 --> 00:01:37,320
and stuff occur and this mechanism is

37
00:01:35,100 --> 00:01:39,180
called delayed free

38
00:01:37,320 --> 00:01:43,200
because of the free

39
00:01:39,180 --> 00:01:45,360
of the chunks is delayed until the

40
00:01:43,200 --> 00:01:47,280
lockaside list is full so if you

41
00:01:45,360 --> 00:01:49,380
actually want to create a whole in

42
00:01:47,280 --> 00:01:52,560
memory which is basically a free chunk

43
00:01:49,380 --> 00:01:55,200
when this delayed free mechanism is in

44
00:01:52,560 --> 00:01:57,659
place you have to free the chunk that

45
00:01:55,200 --> 00:02:02,340
you actually want to free and then free

46
00:01:57,659 --> 00:02:04,860
31 other chunks and only then once they

47
00:02:02,340 --> 00:02:07,200
are all actually freed from filling all

48
00:02:04,860 --> 00:02:09,840
the slots in the lockaside list will it

49
00:02:07,200 --> 00:02:12,300
actually free the one chunk you are

50
00:02:09,840 --> 00:02:15,599
interested to free so here I have a

51
00:02:12,300 --> 00:02:18,239
smaller animation to explain how the

52
00:02:15,599 --> 00:02:21,300
delayed free mitigation works so

53
00:02:18,239 --> 00:02:23,160
basically it just looks like this if you

54
00:02:21,300 --> 00:02:25,440
have a whole bunch of KENLISTMENT made in

55
00:02:23,160 --> 00:02:28,020
memory when you actually free it the

56
00:02:25,440 --> 00:02:31,500
first time it's technically delayed free

57
00:02:28,020 --> 00:02:33,720
and it's just on that slot and then

58
00:02:31,500 --> 00:02:36,540
let's say a whole bunch of other free's

59
00:02:33,720 --> 00:02:39,540
happen that are not related to us at all

60
00:02:36,540 --> 00:02:42,780
and the delayed free list slots are

61
00:02:39,540 --> 00:02:44,879
exhausted then only will it free them

62
00:02:42,780 --> 00:02:47,760
all and then your check is actually

63
00:02:44,879 --> 00:02:49,680
freed in practice the delayed free and

64
00:02:47,760 --> 00:02:51,540
flushing of the lockaside list is

65
00:02:49,680 --> 00:02:53,640
happening very frequently because the

66
00:02:51,540 --> 00:02:55,920
kernel is actively using the non-page

67
00:02:53,640 --> 00:02:58,560
pool and freeing stuff all the time so

68
00:02:55,920 --> 00:03:00,660
when a KENLISTMENT is freed it will go

69
00:02:58,560 --> 00:03:03,120
through the delayed free list and soon

70
00:03:00,660 --> 00:03:06,720
enough that delayed free list is flushed

71
00:03:03,120 --> 00:03:09,239
and so the chunk gets freed but in a use

72
00:03:06,720 --> 00:03:11,760
after free scenario we not only need to

73
00:03:09,239 --> 00:03:14,220
win the race condition but also we need

74
00:03:11,760 --> 00:03:17,159
to make sure we effectively replace that

75
00:03:14,220 --> 00:03:19,560
KENLISTMENT free chunk with data we

76
00:03:17,159 --> 00:03:22,200
control so having complete understanding

77
00:03:19,560 --> 00:03:24,959
of when the KENLISTMENT chunk is more

78
00:03:22,200 --> 00:03:27,120
likely to be freed versus not is

79
00:03:24,959 --> 00:03:30,239
actually useful so we are able to

80
00:03:27,120 --> 00:03:32,700
replace the chunk efficiently with data

81
00:03:30,239 --> 00:03:35,400
we control so as a general rule it's

82
00:03:32,700 --> 00:03:37,860
worth knowing that this delayed free

83
00:03:35,400 --> 00:03:40,019
mechanism exists you can add in your own

84
00:03:37,860 --> 00:03:43,200
functionality in userland to ensure

85
00:03:40,019 --> 00:03:45,480
that when you do a free you actually also

86
00:03:43,200 --> 00:03:47,879
free lots of other times all that stuff

87
00:03:45,480 --> 00:03:50,700
so your initial chunk you want to be

88
00:03:47,879 --> 00:03:53,159
freed gets freed as soon as possible and

89
00:03:50,700 --> 00:03:55,440
when you trigger allocations with data

90
00:03:53,159 --> 00:03:57,659
you control these allocations are more

91
00:03:55,440 --> 00:04:00,060
likely to replace your interesting chunk

92
00:03:57,659 --> 00:04:02,519
and the use after free happens with data

93
00:04:00,060 --> 00:04:04,560
you control and so basically anytime you

94
00:04:02,519 --> 00:04:07,799
have a userland function that you want

95
00:04:04,560 --> 00:04:10,560
to use to free a given kernel object you

96
00:04:07,799 --> 00:04:13,799
can just have it free like 31 other

97
00:04:10,560 --> 00:04:16,859
kernel objects at the same time

98
00:04:13,799 --> 00:04:20,160
and so in practice you can pre-allocate

99
00:04:16,859 --> 00:04:22,560
this helper kernel objects so you can

100
00:04:20,160 --> 00:04:24,720
easily free them whenever you need to

101
00:04:22,560 --> 00:04:28,560
trigger the delayed free when you need

102
00:04:24,720 --> 00:04:31,979
and so from now on anytime you see in a

103
00:04:28,560 --> 00:04:36,419
diagram a free hole you can think of it

104
00:04:31,979 --> 00:04:38,759
as a free followed by 31 other free's to

105
00:04:36,419 --> 00:04:42,060
take into account the delayed free

106
00:04:38,759 --> 00:04:44,460
mechanism so yeah I mean the risk of you

107
00:04:42,060 --> 00:04:47,220
not accommodating something like the

108
00:04:44,460 --> 00:04:49,860
delayed freed list does exist in theory

109
00:04:47,220 --> 00:04:51,960
you might think you trigger the free and

110
00:04:49,860 --> 00:04:54,840
then you try to replace that free

111
00:04:51,960 --> 00:04:57,600
KENLISTMENT chunk but because it's not

112
00:04:54,840 --> 00:04:59,820
actually freed freed it's delayed

113
00:04:57,600 --> 00:05:02,400
freed you won't be able to replace the

114
00:04:59,820 --> 00:05:04,620
contents and then let's say you have won

115
00:05:02,400 --> 00:05:06,840
the rate condition and the use after

116
00:05:04,620 --> 00:05:10,199
free occurs in the vulnerable loop

117
00:05:06,840 --> 00:05:13,320
basically it will just use the NextSameRm

118
00:05:10,199 --> 00:05:16,199
Flink pointer which is still valid

119
00:05:13,320 --> 00:05:19,500
from the the old KENLISTMENT contents

120
00:05:16,199 --> 00:05:21,720
because it is just a chunk that is in

121
00:05:19,500 --> 00:05:24,479
the delayed free list and so its

122
00:05:21,720 --> 00:05:26,280
contents is still untouched and so

123
00:05:24,479 --> 00:05:28,800
basically you were able to win the race

124
00:05:26,280 --> 00:05:31,500
condition trigger the use after free but

125
00:05:28,800 --> 00:05:33,539
no crash is going to occur and so it is

126
00:05:31,500 --> 00:05:35,699
going to loop again as if nothing

127
00:05:33,539 --> 00:05:37,919
happened and you'll have to redo the

128
00:05:35,699 --> 00:05:40,259
work of winning the race condition so

129
00:05:37,919 --> 00:05:42,419
you can actually use the user to free

130
00:05:40,259 --> 00:05:43,680
for an exploit primitive and stuff and

131
00:05:42,419 --> 00:05:46,199
when you're dealing with something like

132
00:05:43,680 --> 00:05:48,419
a race condition that you have to

133
00:05:46,199 --> 00:05:51,000
potentially trigger thousands of times

134
00:05:48,419 --> 00:05:54,000
before you actually win it having code

135
00:05:51,000 --> 00:05:55,860
that is relatively sure it's going to

136
00:05:54,000 --> 00:05:58,620
trigger the free as soon as possible

137
00:05:55,860 --> 00:06:01,759
actually helps because it might save you

138
00:05:58,620 --> 00:06:01,759
Horrors in the long run

