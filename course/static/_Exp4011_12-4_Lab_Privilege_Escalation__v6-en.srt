1
00:00:03,540 --> 00:00:08,220
hi everyone

2
00:00:04,920 --> 00:00:09,660
you've been waiting for this last lab

3
00:00:08,220 --> 00:00:13,620
for a long time

4
00:00:09,660 --> 00:00:16,500
it's finally ready for you in this lab

5
00:00:13,620 --> 00:00:19,440
you're going to pop a high privileged

6
00:00:16,500 --> 00:00:22,080
shell finally and for doing so you're

7
00:00:19,440 --> 00:00:26,160
gonna have to patch your own process

8
00:00:22,080 --> 00:00:27,300
token with a system token okay let's get

9
00:00:26,160 --> 00:00:29,820
started

10
00:00:27,300 --> 00:00:31,980
for this lab most of the code is the

11
00:00:29,820 --> 00:00:34,559
same as the previous lab we are already

12
00:00:31,980 --> 00:00:38,100
close to finishing the exploit actually

13
00:00:34,559 --> 00:00:40,200
which feels nice and so in this lab we

14
00:00:38,100 --> 00:00:43,140
actually have the solution for the

15
00:00:40,200 --> 00:00:45,660
previous lab which means it contains the

16
00:00:43,140 --> 00:00:48,899
code to build the code mode and instance

17
00:00:45,660 --> 00:00:51,420
that allows us to patch our recovery

18
00:00:48,899 --> 00:00:55,140
threads PreviousMode to zero and then

19
00:00:51,420 --> 00:00:57,719
call the NT read virtual memory and NT

20
00:00:55,140 --> 00:01:00,539
write virtual memory functions from user

21
00:00:57,719 --> 00:01:03,059
land to confirm we have kernel

22
00:01:00,539 --> 00:01:05,760
arbitrary read write primitive and so we

23
00:01:03,059 --> 00:01:08,040
provide code to find the e-process

24
00:01:05,760 --> 00:01:11,520
structure associated with the system

25
00:01:08,040 --> 00:01:14,520
process assuming we have a pointer to

26
00:01:11,520 --> 00:01:16,439
any e-process structure this code is

27
00:01:14,520 --> 00:01:20,280
basically going to go over all the

28
00:01:16,439 --> 00:01:23,159
processes by working the lists until it

29
00:01:20,280 --> 00:01:26,280
finds the system process and it is the

30
00:01:23,159 --> 00:01:29,580
process that contains a high privileged

31
00:01:26,280 --> 00:01:32,700
token we want to steal and also use for

32
00:01:29,580 --> 00:01:35,640
our own exploit process and finally we

33
00:01:32,700 --> 00:01:38,100
provide code to spawn a shell assuming

34
00:01:35,640 --> 00:01:41,400
we have administrator privileges so

35
00:01:38,100 --> 00:01:44,520
assuming we patch our own process token

36
00:01:41,400 --> 00:01:47,700
to be the same as the system process we

37
00:01:44,520 --> 00:01:50,340
should be able to spawn NT Authority

38
00:01:47,700 --> 00:01:52,979
system shell in user land so most of

39
00:01:50,340 --> 00:01:55,320
the files in the lab are exactly the

40
00:01:52,979 --> 00:01:59,040
same as the previous lab but we

41
00:01:55,320 --> 00:02:01,500
introduce a new file called prevask.c

42
00:01:59,040 --> 00:02:04,020
which basically contains the code to

43
00:02:01,500 --> 00:02:06,420
inject the gold mode and instruments and

44
00:02:04,020 --> 00:02:09,539
the code to work the linked list of

45
00:02:06,420 --> 00:02:11,099
e-process to find the system process so

46
00:02:09,539 --> 00:02:14,520
the goal of this lab is to actually

47
00:02:11,099 --> 00:02:17,220
spawn a shell with system privileges and

48
00:02:14,520 --> 00:02:20,280
to do so we are going to swap our

49
00:02:17,220 --> 00:02:22,800
exploit e-process token to hold the

50
00:02:20,280 --> 00:02:25,500
system token instead and so there is

51
00:02:22,800 --> 00:02:28,080
this God mode system token swap function

52
00:02:25,500 --> 00:02:30,360
that we need to modify and so we are

53
00:02:28,080 --> 00:02:32,940
going to use our arbitrary read write

54
00:02:30,360 --> 00:02:35,819
primitive in the kernel and so first

55
00:02:32,940 --> 00:02:38,879
we're going to use our arbitrary read

56
00:02:35,819 --> 00:02:42,720
primitive to read multiple structures

57
00:02:38,879 --> 00:02:45,300
and then we can use our arbitrary right

58
00:02:42,720 --> 00:02:49,319
primitive to update some of the

59
00:02:45,300 --> 00:02:52,560
structure fields we kind of know a case

60
00:02:49,319 --> 00:02:54,840
thread address because we leaked it for

61
00:02:52,560 --> 00:02:57,599
the recovery thread as part of the

62
00:02:54,840 --> 00:03:00,300
initial Canal leak and so we can deduce

63
00:02:57,599 --> 00:03:03,300
the e-process address holding that

64
00:03:00,300 --> 00:03:07,019
thread because the process structure has

65
00:03:03,300 --> 00:03:08,819
a linked list of all of its threads and

66
00:03:07,019 --> 00:03:11,220
then we can work the linked list of

67
00:03:08,819 --> 00:03:14,220
e-process by calling a helper function

68
00:03:11,220 --> 00:03:17,099
that we provide and that is called find

69
00:03:14,220 --> 00:03:19,680
eprocess good mode stream and this

70
00:03:17,099 --> 00:03:22,319
sanction allows us to get the system

71
00:03:19,680 --> 00:03:25,019
e-process address and so from the system

72
00:03:22,319 --> 00:03:27,599
eprocess structure we can just get the

73
00:03:25,019 --> 00:03:30,000
pointer to the associated token and then

74
00:03:27,599 --> 00:03:33,120
we can just use that token pointer to

75
00:03:30,000 --> 00:03:36,180
override our old token inside our

76
00:03:33,120 --> 00:03:39,180
e-process for our exploits I guess it's

77
00:03:36,180 --> 00:03:42,060
worth noting that for real world

78
00:03:39,180 --> 00:03:45,540
exploits we should actually increase the

79
00:03:42,060 --> 00:03:47,640
point accounts in the object header that

80
00:03:45,540 --> 00:03:50,459
is in memory before the actual token

81
00:03:47,640 --> 00:03:53,400
structure and this is to avoid any crash

82
00:03:50,459 --> 00:03:55,680
if the token structure is Freed at some

83
00:03:53,400 --> 00:03:58,620
point for instance when we exit the

84
00:03:55,680 --> 00:04:01,799
exploit several times after vertical

85
00:03:58,620 --> 00:04:05,099
runs since since each run will actually

86
00:04:01,799 --> 00:04:07,680
decrease the ref counts of the original

87
00:04:05,099 --> 00:04:09,900
system token and so if we run the

88
00:04:07,680 --> 00:04:12,599
exploit several times the ref count

89
00:04:09,900 --> 00:04:16,019
might end up reaching zero at some point

90
00:04:12,599 --> 00:04:18,959
which we don't want because the system e

91
00:04:16,019 --> 00:04:22,079
process would still hold a stale red

92
00:04:18,959 --> 00:04:25,620
reference to that token and the system

93
00:04:22,079 --> 00:04:27,900
would crash either cannot accessed that

94
00:04:25,620 --> 00:04:31,020
legitimate token through the system

95
00:04:27,900 --> 00:04:33,479
process and finally we can use our piano

96
00:04:31,020 --> 00:04:35,880
arbitrary redride primitive to actually

97
00:04:33,479 --> 00:04:38,520
restore our recovery thread previous

98
00:04:35,880 --> 00:04:40,320
mode to one since we don't need to

99
00:04:38,520 --> 00:04:43,199
actually modify the kernel anymore

100
00:04:40,320 --> 00:04:45,180
obviously once we have restored our

101
00:04:43,199 --> 00:04:47,759
PreviousMode to one we won't be able to

102
00:04:45,180 --> 00:04:49,860
check if this particular right worked

103
00:04:47,759 --> 00:04:51,600
but it doesn't matter well actually

104
00:04:49,860 --> 00:04:54,120
there is one way we could actually

105
00:04:51,600 --> 00:04:56,520
confirm it word which is that we

106
00:04:54,120 --> 00:04:58,919
shouldn't be able to read any kernel

107
00:04:56,520 --> 00:05:01,560
memory anymore and so finally we provide

108
00:04:58,919 --> 00:05:04,500
a helper function called try admin shell

109
00:05:01,560 --> 00:05:07,199
which allows us to spawn a shell and so

110
00:05:04,500 --> 00:05:10,919
we should get like system privileges

111
00:05:07,199 --> 00:05:14,820
into a new cnd.exe which you can confirm

112
00:05:10,919 --> 00:05:17,280
using the who am I command in the spawn

113
00:05:14,820 --> 00:05:20,639
shell you can use the offsets we provide

114
00:05:17,280 --> 00:05:23,639
in the global possible structure which

115
00:05:20,639 --> 00:05:27,360
stands for pointer operating system

116
00:05:23,639 --> 00:05:29,520
variables and similarly to before we

117
00:05:27,360 --> 00:05:32,759
have the recovery straight K shred

118
00:05:29,520 --> 00:05:35,340
address in pxvar which stands for

119
00:05:32,759 --> 00:05:38,580
pointer export variables and the reason

120
00:05:35,340 --> 00:05:40,620
we like to have this global structure is

121
00:05:38,580 --> 00:05:43,740
it allows us to support different

122
00:05:40,620 --> 00:05:46,139
operative system versions quite easily

123
00:05:43,740 --> 00:05:48,060
and so this slide is just to show a

124
00:05:46,139 --> 00:05:50,160
successful right of the Explorer that

125
00:05:48,060 --> 00:05:52,560
you should be able to see once you have

126
00:05:50,160 --> 00:05:55,080
implemented the the last few steps you

127
00:05:52,560 --> 00:05:57,539
can see that running who am I shows that

128
00:05:55,080 --> 00:05:59,280
we are NT Authority system so let's have

129
00:05:57,539 --> 00:06:03,900
a look at the code for this particular

130
00:05:59,280 --> 00:06:05,699
lab we know we have the previous.c that

131
00:06:03,900 --> 00:06:07,680
contains helper functions and we have

132
00:06:05,699 --> 00:06:09,720
the privileged escalation.c which

133
00:06:07,680 --> 00:06:12,360
contained the code for audience entry

134
00:06:09,720 --> 00:06:16,380
point on this lab so just looking at the

135
00:06:12,360 --> 00:06:18,600
previous.c there is a good mode confirm

136
00:06:16,380 --> 00:06:20,639
which is basically the function used to

137
00:06:18,600 --> 00:06:22,620
read and write the kernel memory just to

138
00:06:20,639 --> 00:06:24,380
confirm we have arbitrary read write so

139
00:06:22,620 --> 00:06:27,840
this is basically just

140
00:06:24,380 --> 00:06:29,880
retrieving the first keyword of the K

141
00:06:27,840 --> 00:06:32,880
resource manager structure and then

142
00:06:29,880 --> 00:06:35,220
overwriting it with that beef before we

143
00:06:32,880 --> 00:06:36,840
actually read it back there is this init

144
00:06:35,220 --> 00:06:40,380
card mode amusement

145
00:06:36,840 --> 00:06:41,940
which is basically creating the fake god

146
00:06:40,380 --> 00:06:42,860
mode Management in user land in order

147
00:06:41,940 --> 00:06:46,500
to

148
00:06:42,860 --> 00:06:48,720
override PreviousMode so yeah building

149
00:06:46,500 --> 00:06:51,180
the adjusted right address

150
00:06:48,720 --> 00:06:54,539
based on previous modes thread lock and

151
00:06:51,180 --> 00:06:58,680
adjusting it so so we can pass the spin

152
00:06:54,539 --> 00:07:00,120
lock and override the 464-bit after the

153
00:06:58,680 --> 00:07:03,720
lower bits

154
00:07:00,120 --> 00:07:06,360
um is actually switched from zero to one

155
00:07:03,720 --> 00:07:09,840
and then we have the fine e-process good

156
00:07:06,360 --> 00:07:13,199
mode Prem function which allows us to

157
00:07:09,840 --> 00:07:15,360
from a given e-process it's going to go

158
00:07:13,199 --> 00:07:17,160
through the link list of e-process in

159
00:07:15,360 --> 00:07:19,979
order to find the what the one that has

160
00:07:17,160 --> 00:07:22,440
system Bridges it does it by looking at

161
00:07:19,979 --> 00:07:25,220
the system PID because we know it's

162
00:07:22,440 --> 00:07:25,220
going to be four

163
00:07:27,240 --> 00:07:32,099
yeah you can see we provide wanted PID

164
00:07:30,300 --> 00:07:35,520
as an argument so we're going to ask

165
00:07:32,099 --> 00:07:37,759
that we need the system pie and so we we

166
00:07:35,520 --> 00:07:41,099
make sure we we save the one that

167
00:07:37,759 --> 00:07:43,380
corresponds to the system process okay

168
00:07:41,099 --> 00:07:45,120
and so going into privileges

169
00:07:43,380 --> 00:07:47,940
collection.c

170
00:07:45,120 --> 00:07:50,160
um we can go from the entry points so

171
00:07:47,940 --> 00:07:52,500
really the code is very similar we have

172
00:07:50,160 --> 00:07:54,240
the the main function that we call you

173
00:07:52,500 --> 00:07:55,860
know to do the try to win the race

174
00:07:54,240 --> 00:07:59,900
condition

175
00:07:55,860 --> 00:07:59,900
all the code is very similar

176
00:08:00,000 --> 00:08:04,220
it's just that once we win the race

177
00:08:01,860 --> 00:08:04,220
condition

178
00:08:09,300 --> 00:08:15,060
we're going to basically want to see in

179
00:08:11,940 --> 00:08:17,280
the recovery thread what we do so

180
00:08:15,060 --> 00:08:19,860
basically you need to recovery thread

181
00:08:17,280 --> 00:08:22,199
calls this recover thread function which

182
00:08:19,860 --> 00:08:25,280
we know calls this functional to call

183
00:08:22,199 --> 00:08:25,280
the variable function

184
00:08:25,379 --> 00:08:31,560
and once it's returned it means we

185
00:08:28,919 --> 00:08:33,659
managed to escape the vulnerable loop so

186
00:08:31,560 --> 00:08:35,459
we're gonna basically have to call god

187
00:08:33,659 --> 00:08:37,440
mode confirm which is the new helper

188
00:08:35,459 --> 00:08:39,539
function from previous.c that I've just

189
00:08:37,440 --> 00:08:41,520
described that's going to confirm that

190
00:08:39,539 --> 00:08:43,680
we have actually read write in the to

191
00:08:41,520 --> 00:08:46,880
the channel and finally we need to call

192
00:08:43,680 --> 00:08:50,519
god mode system token swap you know to

193
00:08:46,880 --> 00:08:52,260
swap our e-process token with the actual

194
00:08:50,519 --> 00:08:54,420
system token and I'm going to show this

195
00:08:52,260 --> 00:08:56,160
function in a minute because we need to

196
00:08:54,420 --> 00:08:58,440
modify this function but then after that

197
00:08:56,160 --> 00:09:01,260
we're going to basically be able to call

198
00:08:58,440 --> 00:09:04,740
try admin shell which is going to allow

199
00:09:01,260 --> 00:09:07,620
us to spawn a shell with

200
00:09:04,740 --> 00:09:09,420
um with this temperatures so yeah let's

201
00:09:07,620 --> 00:09:10,860
look at the cardboard system token swap

202
00:09:09,420 --> 00:09:13,320
function this is the function we need to

203
00:09:10,860 --> 00:09:15,660
modify so you have instructions on what

204
00:09:13,320 --> 00:09:18,839
you you need to do but basically I'm

205
00:09:15,660 --> 00:09:21,180
finding our e-process based on the

206
00:09:18,839 --> 00:09:23,339
recovery thread case thread then calling

207
00:09:21,180 --> 00:09:26,279
this fine e-process god mode Prem you

208
00:09:23,339 --> 00:09:28,560
can just uncomment this and then finding

209
00:09:26,279 --> 00:09:30,959
the system token inside the e-process

210
00:09:28,560 --> 00:09:33,720
structure and and you can also

211
00:09:30,959 --> 00:09:36,839
optionally patch the system token to

212
00:09:33,720 --> 00:09:40,260
have a higher ref count and finally you

213
00:09:36,839 --> 00:09:42,839
can patch into your own in process so it

214
00:09:40,260 --> 00:09:45,779
holds the new token and we provide code

215
00:09:42,839 --> 00:09:49,940
already to restore PreviousMode two to

216
00:09:45,779 --> 00:09:49,940
one okay now it's your turn

