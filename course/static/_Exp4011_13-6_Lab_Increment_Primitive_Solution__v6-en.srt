1
00:00:02,940 --> 00:00:08,340
okay so we know we need to implement the

2
00:00:06,060 --> 00:00:11,400
increment read kernel pointer function

3
00:00:08,340 --> 00:00:14,400
that allows us to get an arbitrary read

4
00:00:11,400 --> 00:00:17,039
primitive and it's going to rely on the

5
00:00:14,400 --> 00:00:21,180
increment right Val function that allows

6
00:00:17,039 --> 00:00:24,000
us to uh actually uh execute several

7
00:00:21,180 --> 00:00:27,359
increment primitives in order to

8
00:00:24,000 --> 00:00:30,240
override the description buffer and

9
00:00:27,359 --> 00:00:33,739
length fields for the K resource manager

10
00:00:30,240 --> 00:00:36,899
structure so we can actually build our

11
00:00:33,739 --> 00:00:40,219
obviously read primitive and so if we

12
00:00:36,899 --> 00:00:42,840
look at where this function is used

13
00:00:40,219 --> 00:00:46,620
so we can see it's using a couple of

14
00:00:42,840 --> 00:00:46,620
places into increment.c

15
00:00:48,260 --> 00:00:55,199
and so the first use is in this reset

16
00:00:52,980 --> 00:00:58,199
read primitive function which basically

17
00:00:55,199 --> 00:01:00,840
is used at the very end of the exploits

18
00:00:58,199 --> 00:01:05,580
when we actually want to reset the

19
00:01:00,840 --> 00:01:08,040
buffer and length fields to nil and zero

20
00:01:05,580 --> 00:01:10,619
respectively so so when the care

21
00:01:08,040 --> 00:01:12,479
resource manager structure is frayed

22
00:01:10,619 --> 00:01:15,000
nothing bad is going to happen because

23
00:01:12,479 --> 00:01:18,000
it's not going to try to free a pointer

24
00:01:15,000 --> 00:01:20,640
that we crafted with our exploits and so

25
00:01:18,000 --> 00:01:23,159
here the conversion is that we passed

26
00:01:20,640 --> 00:01:25,799
neural to this function and so we're

27
00:01:23,159 --> 00:01:28,140
gonna have to code when we pass null

28
00:01:25,799 --> 00:01:29,880
that we want to reset the buffer and the

29
00:01:28,140 --> 00:01:33,119
length field of the description field

30
00:01:29,880 --> 00:01:35,040
then we have some other use of this

31
00:01:33,119 --> 00:01:38,759
increment read Canal pointer function

32
00:01:35,040 --> 00:01:41,280
inside find eprocess increment primitive

33
00:01:38,759 --> 00:01:44,180
so this is basically using the increment

34
00:01:41,280 --> 00:01:47,040
primitive in order to find the e-process

35
00:01:44,180 --> 00:01:49,860
associated with even PID so in this case

36
00:01:47,040 --> 00:01:52,020
it would be the system process and so

37
00:01:49,860 --> 00:01:55,200
obviously we need to read the process

38
00:01:52,020 --> 00:01:57,720
pointers until we find the right one so

39
00:01:55,200 --> 00:01:59,700
yeah so we read the e-process and then

40
00:01:57,720 --> 00:02:02,579
the PID using this function and each

41
00:01:59,700 --> 00:02:05,360
time we pass PX bars and the address we

42
00:02:02,579 --> 00:02:05,360
want to read

43
00:02:05,420 --> 00:02:12,060
and finally there are a few use as well

44
00:02:09,660 --> 00:02:15,959
in the increment system token swap which

45
00:02:12,060 --> 00:02:19,920
allows us to actually swap the our own

46
00:02:15,959 --> 00:02:22,379
e-process with the system process

47
00:02:19,920 --> 00:02:25,319
and so we can see we're reading the

48
00:02:22,379 --> 00:02:27,180
current e-process for the recovery

49
00:02:25,319 --> 00:02:28,920
thread then we're calling the function

50
00:02:27,180 --> 00:02:30,720
that we've just mentioned and then we

51
00:02:28,920 --> 00:02:34,160
have a few other reads in order to read

52
00:02:30,720 --> 00:02:38,420
the system token pointer and the count

53
00:02:34,160 --> 00:02:38,420
for the system token

54
00:02:39,060 --> 00:02:43,080
and then we're back to

55
00:02:41,040 --> 00:02:45,239
the actual implementation of this

56
00:02:43,080 --> 00:02:47,160
function and so before we look at the

57
00:02:45,239 --> 00:02:48,239
actual solution for this lab we know

58
00:02:47,160 --> 00:02:50,819
we're going to have to use this

59
00:02:48,239 --> 00:02:53,879
increment right Val function which

60
00:02:50,819 --> 00:02:56,760
actually change several increment

61
00:02:53,879 --> 00:02:59,700
primitives in order to get an arbitrary

62
00:02:56,760 --> 00:03:01,980
write primitive and so the prototype of

63
00:02:59,700 --> 00:03:04,680
this function is it takes the address

64
00:03:01,980 --> 00:03:06,900
where there is the value we want to

65
00:03:04,680 --> 00:03:10,680
change and because we use an increment

66
00:03:06,900 --> 00:03:13,260
it's actually having two arguments all

67
00:03:10,680 --> 00:03:14,940
value a new value so we're going to

68
00:03:13,260 --> 00:03:16,800
basically increment from the old value

69
00:03:14,940 --> 00:03:19,620
to the new value so we need to track

70
00:03:16,800 --> 00:03:22,019
what previous value was at the specific

71
00:03:19,620 --> 00:03:23,580
address that we want to change from the

72
00:03:22,019 --> 00:03:25,500
old value to the new value and then

73
00:03:23,580 --> 00:03:28,140
there is the length so typically for a

74
00:03:25,500 --> 00:03:30,180
pointer it would be 8 bytes in 64 bits

75
00:03:28,140 --> 00:03:32,879
or for length field it could be four

76
00:03:30,180 --> 00:03:35,480
byte or similar so let let's have a look

77
00:03:32,879 --> 00:03:39,180
at the actual solution for this function

78
00:03:35,480 --> 00:03:42,360
so basically we know we're going to use

79
00:03:39,180 --> 00:03:44,900
the last read address and last length

80
00:03:42,360 --> 00:03:48,480
value to track what are the current

81
00:03:44,900 --> 00:03:50,220
values for the description buffer and

82
00:03:48,480 --> 00:03:52,680
the description length field in the

83
00:03:50,220 --> 00:03:55,920
kernel so we know we can increment them

84
00:03:52,680 --> 00:03:57,959
from the last value to the new value and

85
00:03:55,920 --> 00:03:59,700
then we just update the last value

86
00:03:57,959 --> 00:04:02,459
um we know we leave the carry resource

87
00:03:59,700 --> 00:04:05,760
manager address so we can actually get

88
00:04:02,459 --> 00:04:09,000
the address where the buffer pointer is

89
00:04:05,760 --> 00:04:11,280
stored as well as the length field that

90
00:04:09,000 --> 00:04:13,200
is stored so this is the this is the two

91
00:04:11,280 --> 00:04:14,879
Fields we're gonna have to change like

92
00:04:13,200 --> 00:04:17,340
the addresses of the two fields in the

93
00:04:14,879 --> 00:04:21,000
kernel and so we use our

94
00:04:17,340 --> 00:04:23,280
write primitive or increments based

95
00:04:21,000 --> 00:04:27,240
obviously write primitive to basically

96
00:04:23,280 --> 00:04:29,699
change both the debuff write address and

97
00:04:27,240 --> 00:04:31,860
the links right at rest so we have a few

98
00:04:29,699 --> 00:04:34,500
specific cases to deal with so the first

99
00:04:31,860 --> 00:04:37,320
case is when we it's actually the the

100
00:04:34,500 --> 00:04:39,060
one we will use last but we we handle it

101
00:04:37,320 --> 00:04:41,220
first because we're gonna have to return

102
00:04:39,060 --> 00:04:43,500
and we won't do much in this case but

103
00:04:41,220 --> 00:04:47,880
basically we set the convention is if we

104
00:04:43,500 --> 00:04:50,280
pass a read address of null like we it's

105
00:04:47,880 --> 00:04:53,340
a convention it means we have to reset

106
00:04:50,280 --> 00:04:55,199
the buffer and length Fields so in this

107
00:04:53,340 --> 00:04:57,300
case we just call increment right battle

108
00:04:55,199 --> 00:04:59,340
saying okay we want to change the buffer

109
00:04:57,300 --> 00:05:02,100
field so we specify the buffer right

110
00:04:59,340 --> 00:05:04,199
address and we say change it from the

111
00:05:02,100 --> 00:05:06,780
last address we used which we previously

112
00:05:04,199 --> 00:05:10,860
stored and change it back to normal and

113
00:05:06,780 --> 00:05:12,720
it's going to be size 8 bytes in 64 bits

114
00:05:10,860 --> 00:05:14,880
and then we do the same thing for the

115
00:05:12,720 --> 00:05:17,340
length field so we specify the address

116
00:05:14,880 --> 00:05:20,660
in the kernel where the length field is

117
00:05:17,340 --> 00:05:23,460
and then it will basically change it to

118
00:05:20,660 --> 00:05:26,580
zero and specifying the previous value

119
00:05:23,460 --> 00:05:27,620
was last like value and it's going to

120
00:05:26,580 --> 00:05:30,840
specify

121
00:05:27,620 --> 00:05:34,620
the actual size and then we just reset

122
00:05:30,840 --> 00:05:37,560
what I just sorry we just update the two

123
00:05:34,620 --> 00:05:39,360
values for the buffer and the length

124
00:05:37,560 --> 00:05:41,820
field that we tracked because we've just

125
00:05:39,360 --> 00:05:45,060
jumped them to null and zero

126
00:05:41,820 --> 00:05:46,919
respectively so then we handle the the

127
00:05:45,060 --> 00:05:49,919
first case where we're going to actually

128
00:05:46,919 --> 00:05:52,080
reuse our average read primitive and so

129
00:05:49,919 --> 00:05:54,660
we know initially that the buffer is

130
00:05:52,080 --> 00:05:57,180
going to be set to null but and the

131
00:05:54,660 --> 00:06:00,060
length field is going to be set to zero

132
00:05:57,180 --> 00:06:02,759
because our read primitive arbitrary

133
00:06:00,060 --> 00:06:04,860
read primitive assumes that we know the

134
00:06:02,759 --> 00:06:06,900
original values so originally we know

135
00:06:04,860 --> 00:06:09,600
there are set to zero unreal

136
00:06:06,900 --> 00:06:12,780
respectively and so and so if that's the

137
00:06:09,600 --> 00:06:15,840
case if the length last length value is

138
00:06:12,780 --> 00:06:18,180
set to zero it means the it's the first

139
00:06:15,840 --> 00:06:21,240
time we actually do an arbitrary read

140
00:06:18,180 --> 00:06:23,639
preview so what we do is we actually set

141
00:06:21,240 --> 00:06:26,900
that length to

142
00:06:23,639 --> 00:06:30,180
um 8 bytes so so from now on

143
00:06:26,900 --> 00:06:32,960
this length field is set to 8 which

144
00:06:30,180 --> 00:06:37,740
ended from 0 to 8 and we update the

145
00:06:32,960 --> 00:06:40,380
tracking variable for the length failed

146
00:06:37,740 --> 00:06:42,840
to be eight bytes and so now we are in

147
00:06:40,380 --> 00:06:46,199
the general case so in the general case

148
00:06:42,840 --> 00:06:49,259
the length field has already been set or

149
00:06:46,199 --> 00:06:51,900
first set from this if condition

150
00:06:49,259 --> 00:06:53,940
previously or it was set from this if

151
00:06:51,900 --> 00:06:55,860
condition but from a previous read and

152
00:06:53,940 --> 00:06:59,400
so if we're trying to read an arbitrary

153
00:06:55,860 --> 00:07:02,100
address we just say that if you want to

154
00:06:59,400 --> 00:07:03,600
do that we call the wrapper for the

155
00:07:02,100 --> 00:07:05,280
increment right file again but

156
00:07:03,600 --> 00:07:08,220
specifying that we want to change it

157
00:07:05,280 --> 00:07:11,220
from The Last Read address to the read

158
00:07:08,220 --> 00:07:13,020
address the new one that we want to

159
00:07:11,220 --> 00:07:16,860
actually read and again it's going to be

160
00:07:13,020 --> 00:07:19,500
a poetry so size 8 and we update the

161
00:07:16,860 --> 00:07:21,960
global variable to say okay now the last

162
00:07:19,500 --> 00:07:25,460
Red address is actually this read

163
00:07:21,960 --> 00:07:29,759
address that we've just read and finally

164
00:07:25,460 --> 00:07:32,220
we once the buffer field and the length

165
00:07:29,759 --> 00:07:34,919
field of the description field of the

166
00:07:32,220 --> 00:07:37,259
Care Resource manager are set as we've

167
00:07:34,919 --> 00:07:39,180
just done we can actually call the leak

168
00:07:37,259 --> 00:07:41,699
resource major description to call the

169
00:07:39,180 --> 00:07:44,220
The syscall to actually read the

170
00:07:41,699 --> 00:07:47,280
description field and it can actually

171
00:07:44,220 --> 00:07:49,440
return what is pointed in in the kernel

172
00:07:47,280 --> 00:07:52,199
by the buffer feel that we have just

173
00:07:49,440 --> 00:07:54,840
changed and so yeah just calling this

174
00:07:52,199 --> 00:07:56,520
syscall will just retrieve arbitrary data

175
00:07:54,840 --> 00:08:00,060
from the kernel so this is what we get

176
00:07:56,520 --> 00:08:03,000
here and because we said that the syscall

177
00:08:00,060 --> 00:08:05,039
will actually need to acquire the care

178
00:08:03,000 --> 00:08:07,380
resource manager's mutex before it can

179
00:08:05,039 --> 00:08:08,300
actually copy the description field back

180
00:08:07,380 --> 00:08:13,860
to us

181
00:08:08,300 --> 00:08:17,220
we need to use the set notifiable thread

182
00:08:13,860 --> 00:08:19,800
and so what we do is we set the set

183
00:08:17,220 --> 00:08:23,460
notifiable enlistment while the

184
00:08:19,800 --> 00:08:26,280
enlistment that needs to be spanned for

185
00:08:23,460 --> 00:08:28,080
the notifiable flag we set it to the

186
00:08:26,280 --> 00:08:30,840
current trap enlistments because we know

187
00:08:28,080 --> 00:08:34,260
that the it's going to basically unblock

188
00:08:30,840 --> 00:08:36,080
the recovery thread and makes it go into

189
00:08:34,260 --> 00:08:39,300
a certain path that's going to basically

190
00:08:36,080 --> 00:08:41,279
unlock the K resource Badger mutex so we

191
00:08:39,300 --> 00:08:43,440
set this the current spam notifiable

192
00:08:41,279 --> 00:08:46,440
announcements then we set at the event

193
00:08:43,440 --> 00:08:49,320
to say okay now please the set

194
00:08:46,440 --> 00:08:52,620
notifiable thread starts spamming the

195
00:08:49,320 --> 00:08:55,320
notifiable flag and and then we can call

196
00:08:52,620 --> 00:09:01,500
our function and when it returns it

197
00:08:55,320 --> 00:09:03,480
means it worked we our our syscall managed

198
00:09:01,500 --> 00:09:05,600
to acquire the mutex and return from to

199
00:09:03,480 --> 00:09:08,839
uslan and so now we can just stop

200
00:09:05,600 --> 00:09:12,060
spamming the notifiable flag

201
00:09:08,839 --> 00:09:14,760
in the set notifiable thread and that's

202
00:09:12,060 --> 00:09:18,180
that's basically it we can return the

203
00:09:14,760 --> 00:09:21,800
leaks data to the caller and this

204
00:09:18,180 --> 00:09:21,800
function is done now

205
00:09:23,040 --> 00:09:28,620
so I have pushed the increment exploit

206
00:09:26,700 --> 00:09:30,839
lab after building it from the debugger

207
00:09:28,620 --> 00:09:33,480
VM and I'm going to run it and so it's

208
00:09:30,839 --> 00:09:35,279
initializing all the KTM objects and

209
00:09:33,480 --> 00:09:38,580
then it's going to start trying to win

210
00:09:35,279 --> 00:09:41,880
the race so we see that it took 25 hex

211
00:09:38,580 --> 00:09:46,459
at times and you know in two different

212
00:09:41,880 --> 00:09:46,459
attempts so we detected we win the race

213
00:09:47,600 --> 00:09:54,200
and you can see it's actually slower

214
00:09:50,220 --> 00:09:54,200
than the uh

215
00:09:54,779 --> 00:09:59,839
PreviousMode based exploits

216
00:10:03,060 --> 00:10:09,300
and we got system so here is another

217
00:10:06,060 --> 00:10:11,839
example of running the exploit so we see

218
00:10:09,300 --> 00:10:14,339
that it took up to 56

219
00:10:11,839 --> 00:10:16,800
enlistments over two attempts to

220
00:10:14,339 --> 00:10:20,820
actually win the race condition and then

221
00:10:16,800 --> 00:10:23,100
we had to pass the processes from the

222
00:10:20,820 --> 00:10:25,740
e-process structure that we lead from

223
00:10:23,100 --> 00:10:27,660
our recovery thread and so we see that

224
00:10:25,740 --> 00:10:29,580
we have to pass one e Pro Set to

225
00:10:27,660 --> 00:10:31,980
e-process and then we found the system

226
00:10:29,580 --> 00:10:34,740
process and finally we were able to

227
00:10:31,980 --> 00:10:38,000
locate the system token structure and we

228
00:10:34,740 --> 00:10:38,000
got the system shell

229
00:10:38,839 --> 00:10:44,940
so here I want to rerun the exploits but

230
00:10:42,720 --> 00:10:48,240
this time using a process Explorer to

231
00:10:44,940 --> 00:10:49,380
see information about the exploit

232
00:10:48,240 --> 00:10:52,399
running

233
00:10:49,380 --> 00:10:52,399
so let's see what happens

234
00:10:54,720 --> 00:10:58,940
and I'm just going to look at the

235
00:10:55,980 --> 00:10:58,940
properties for the

236
00:10:59,339 --> 00:11:02,959
the

237
00:11:00,180 --> 00:11:02,959
process running

238
00:11:21,180 --> 00:11:24,180
ah

239
00:11:29,880 --> 00:11:34,079
so as you can see it looks like it

240
00:11:32,399 --> 00:11:35,399
actually crashed like the VM is not

241
00:11:34,079 --> 00:11:38,279
responding anymore

242
00:11:35,399 --> 00:11:41,640
and it's an interesting scenario because

243
00:11:38,279 --> 00:11:44,700
as you can see the exploits managed to

244
00:11:41,640 --> 00:11:46,620
win the race condition leak the cash

245
00:11:44,700 --> 00:11:49,860
rate and Care Resource measure addresses

246
00:11:46,620 --> 00:11:53,339
then it started to go over all the

247
00:11:49,860 --> 00:11:55,560
e-process structures and it located the

248
00:11:53,339 --> 00:11:58,140
system e process it found the

249
00:11:55,560 --> 00:12:00,600
associative token for the system process

250
00:11:58,140 --> 00:12:04,800
and then it was basically going to

251
00:12:00,600 --> 00:12:06,959
replace our own e-process token with the

252
00:12:04,800 --> 00:12:07,980
system token

253
00:12:06,959 --> 00:12:10,620
um but because it's actually

254
00:12:07,980 --> 00:12:13,320
incrementing the pointer and possibly

255
00:12:10,620 --> 00:12:16,079
when I'm using process Explorer I'm

256
00:12:13,320 --> 00:12:18,860
actually doing userland operation that

257
00:12:16,079 --> 00:12:22,800
potentially would actually try to read

258
00:12:18,860 --> 00:12:25,399
my current e-process token potentially

259
00:12:22,800 --> 00:12:28,500
it's gonna actually difference

260
00:12:25,399 --> 00:12:32,220
an invalid pointer it could be increment

261
00:12:28,500 --> 00:12:34,980
operations that we do to go from the my

262
00:12:32,220 --> 00:12:38,220
current token pointer to the actual

263
00:12:34,980 --> 00:12:40,560
system token boiler won't be Atomic and

264
00:12:38,220 --> 00:12:42,480
so it's going to take some time and so

265
00:12:40,560 --> 00:12:45,060
we had a debugger attached so I'm just

266
00:12:42,480 --> 00:12:47,720
going to analyze what happened and where

267
00:12:45,060 --> 00:12:47,720
it crashed

268
00:12:54,540 --> 00:12:58,459
a few moments later

269
00:12:59,060 --> 00:13:05,940
so what we can see is that it actually

270
00:13:03,240 --> 00:13:08,160
crashed into one of the functions in

271
00:13:05,940 --> 00:13:10,200
account called SC security attribute

272
00:13:08,160 --> 00:13:12,360
present so it looks like it was actually

273
00:13:10,200 --> 00:13:16,459
trying to check if the security

274
00:13:12,360 --> 00:13:19,500
attribute was present into a specific

275
00:13:16,459 --> 00:13:23,899
pointer and then here there is a it's

276
00:13:19,500 --> 00:13:23,899
trying to difference a neural pointer

277
00:13:24,839 --> 00:13:29,519
so if we click here and look at the back

278
00:13:28,560 --> 00:13:32,279
trace

279
00:13:29,519 --> 00:13:35,519
we see that indeed was due to process

280
00:13:32,279 --> 00:13:39,000
Explorer trying to get process

281
00:13:35,519 --> 00:13:42,000
information and then trying to read the

282
00:13:39,000 --> 00:13:43,740
security attribute and so yeah I mean we

283
00:13:42,000 --> 00:13:46,560
can't really do anything about this

284
00:13:43,740 --> 00:13:49,260
problem and it's really a limitation of

285
00:13:46,560 --> 00:13:51,600
the increment primitive with the exploit

286
00:13:49,260 --> 00:13:55,200
so it's a matter of making it as quick

287
00:13:51,600 --> 00:13:57,899
as possible and if we actually control

288
00:13:55,200 --> 00:14:00,720
the target where we run the exploit

289
00:13:57,899 --> 00:14:03,480
making sure we reduce the likelihood of

290
00:14:00,720 --> 00:14:05,459
any process trying to read the pointer

291
00:14:03,480 --> 00:14:10,019
we're trying to change so I hope you

292
00:14:05,459 --> 00:14:12,959
like this lab and see you in a future

293
00:14:10,019 --> 00:14:15,380
video of future training thank you for

294
00:14:12,959 --> 00:14:15,380
watching

