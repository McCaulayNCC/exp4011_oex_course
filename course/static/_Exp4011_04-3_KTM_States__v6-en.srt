1
00:00:00,299 --> 00:00:04,980
hi everyone in this part we're going to

2
00:00:02,760 --> 00:00:07,020
talk about states in the kernel

3
00:00:04,980 --> 00:00:10,080
transaction manager we're going to see

4
00:00:07,020 --> 00:00:13,080
how the transactions States evolve over

5
00:00:10,080 --> 00:00:14,880
time and how the enlistment States need

6
00:00:13,080 --> 00:00:17,880
to synchronize together so the actual

7
00:00:14,880 --> 00:00:20,880
transactions evolves as well we're going

8
00:00:17,880 --> 00:00:22,619
to look into the API in userland that

9
00:00:20,880 --> 00:00:25,560
allows you to get notifications about

10
00:00:22,619 --> 00:00:28,380
the different state changes we are going

11
00:00:25,560 --> 00:00:31,320
to look into the superior enlistment and

12
00:00:28,380 --> 00:00:33,719
how the impact the APIs that you can use

13
00:00:31,320 --> 00:00:37,140
from userland and finally we'll talk

14
00:00:33,719 --> 00:00:38,940
more about the rollback and recovery and

15
00:00:37,140 --> 00:00:41,340
what APIs you can use from userland

16
00:00:38,940 --> 00:00:44,280
to trigger these behaviors okay let's

17
00:00:41,340 --> 00:00:45,960
get started so the state in KTM are very

18
00:00:44,280 --> 00:00:48,360
important in general basically a

19
00:00:45,960 --> 00:00:50,399
transaction won't be completed until all

20
00:00:48,360 --> 00:00:52,680
of the enlistments have been in the

21
00:00:50,399 --> 00:00:55,079
committed States so even though one

22
00:00:52,680 --> 00:00:56,760
enlistment can be committed and is a

23
00:00:55,079 --> 00:00:58,680
candidate for being freed the

24
00:00:56,760 --> 00:01:01,920
transaction is not actually finished and

25
00:00:58,680 --> 00:01:04,260
committed itself until all the workers

26
00:01:01,920 --> 00:01:06,720
have committed to the work that they did

27
00:01:04,260 --> 00:01:10,320
and basically the states when you start

28
00:01:06,720 --> 00:01:12,299
out is called pre-preparing and when all

29
00:01:10,320 --> 00:01:14,640
the enlistments have reached the

30
00:01:12,299 --> 00:01:17,880
pre-prepared states the transaction

31
00:01:14,640 --> 00:01:19,560
itself transitions to the pre-prepared

32
00:01:17,880 --> 00:01:21,780
state as well and then all the

33
00:01:19,560 --> 00:01:24,479
enlistments become in the preparing

34
00:01:21,780 --> 00:01:27,240
state and when they are all done let's

35
00:01:24,479 --> 00:01:29,939
just say they are all prepared the the

36
00:01:27,240 --> 00:01:33,000
transaction also becomes prepared and

37
00:01:29,939 --> 00:01:35,159
again when all the anticipants become in

38
00:01:33,000 --> 00:01:37,320
the committed States the transactions

39
00:01:35,159 --> 00:01:39,060
also is finally actually committed this

40
00:01:37,320 --> 00:01:41,759
can be fairly confusing at first because

41
00:01:39,060 --> 00:01:43,619
the naming is a little bit weird and it

42
00:01:41,759 --> 00:01:45,119
is not that well documented but you can

43
00:01:43,619 --> 00:01:48,060
basically Work It Out by playing with

44
00:01:45,119 --> 00:01:50,280
the APIs using the notification queue to

45
00:01:48,060 --> 00:01:52,500
see what state transitions are happening

46
00:01:50,280 --> 00:01:54,659
because at first it may not be clear

47
00:01:52,500 --> 00:01:57,420
that all of the enlistments need to

48
00:01:54,659 --> 00:01:59,640
synchronize across states to code the

49
00:01:57,420 --> 00:02:01,200
state changes so reversing and poking

50
00:01:59,640 --> 00:02:04,140
around the API is going to help with

51
00:02:01,200 --> 00:02:06,000
that fortunately all of the states that

52
00:02:04,140 --> 00:02:08,819
enlistments go through are documented

53
00:02:06,000 --> 00:02:11,459
publicly into an enum called KENLISTMENT_STATE

54
00:02:08,819 --> 00:02:13,620
and yes so basically when you have

55
00:02:11,459 --> 00:02:16,080
the enlistment transitioning into

56
00:02:13,620 --> 00:02:18,180
States the state changes result in

57
00:02:16,080 --> 00:02:20,340
notification being queued into that

58
00:02:18,180 --> 00:02:22,440
notification queue associated with the

59
00:02:20,340 --> 00:02:24,540
resource manager and whether or not the

60
00:02:22,440 --> 00:02:27,480
notification will actually be queued for

61
00:02:24,540 --> 00:02:30,300
the enlistment is dictated based on a

62
00:02:27,480 --> 00:02:32,760
mask called notification mask field that

63
00:02:30,300 --> 00:02:35,280
was passed to the create enlistment

64
00:02:32,760 --> 00:02:37,260
function when you actually created the

65
00:02:35,280 --> 00:02:40,800
enlistment in the first place and it

66
00:02:37,260 --> 00:02:43,019
basically says I am interested in these

67
00:02:40,800 --> 00:02:45,660
types of state changes for these

68
00:02:43,019 --> 00:02:48,000
enlistment or not and so by playing

69
00:02:45,660 --> 00:02:50,879
around this with this mask you can find

70
00:02:48,000 --> 00:02:52,500
out sort of the best mask to use to

71
00:02:50,879 --> 00:02:55,200
receive as many notifications as

72
00:02:52,500 --> 00:02:57,720
possible so you can see what is actually

73
00:02:55,200 --> 00:03:00,239
happening in KTM from userland and so

74
00:02:57,720 --> 00:03:02,580
there is this userland API called get

75
00:03:00,239 --> 00:03:04,019
notification resource manager which lets

76
00:03:02,580 --> 00:03:06,239
you just get notification information

77
00:03:04,019 --> 00:03:08,700
from the resource manager and it tells

78
00:03:06,239 --> 00:03:10,620
you exactly about the state changes and

79
00:03:08,700 --> 00:03:12,959
give you that information in a structure

80
00:03:10,620 --> 00:03:15,720
called transaction notification which is

81
00:03:12,959 --> 00:03:18,420
publicly documented interestingly and

82
00:03:15,720 --> 00:03:22,140
confusingly the maximum masks that is

83
00:03:18,420 --> 00:03:24,720
documented on the msdn is 3fffffff and

84
00:03:22,140 --> 00:03:27,000
if you pass that mass value to the API

85
00:03:24,720 --> 00:03:29,459
it doesn't work and the enlistment is

86
00:03:27,000 --> 00:03:31,500
not created at all so we had to reverse

87
00:03:29,459 --> 00:03:34,019
engineer the functionality in the kernel

88
00:03:31,500 --> 00:03:36,840
to find the exact sort of value that is

89
00:03:34,019 --> 00:03:38,760
that includes all possible notification

90
00:03:36,840 --> 00:03:41,159
because basically we wanted to get as

91
00:03:38,760 --> 00:03:42,659
many notification as possible and the

92
00:03:41,159 --> 00:03:44,940
value was different from the mask that

93
00:03:42,659 --> 00:03:47,580
was specified by Microsoft publicly on

94
00:03:44,940 --> 00:03:50,700
the msdn so it's not that important but

95
00:03:47,580 --> 00:03:52,620
I guess the main takeaway is don't be

96
00:03:50,700 --> 00:03:54,480
surprised if when you're playing around

97
00:03:52,620 --> 00:03:56,640
with kernel components and stuff the

98
00:03:54,480 --> 00:04:00,060
documentation is not correct and always

99
00:03:56,640 --> 00:04:02,459
trust IDA or Ghidra or WinDbg as they

100
00:04:00,060 --> 00:04:05,220
were will tell you the real truth and so

101
00:04:02,459 --> 00:04:07,200
this is the code in kernel in KTM that

102
00:04:05,220 --> 00:04:09,599
checks the notification mask and so we

103
00:04:07,200 --> 00:04:11,760
need to pass all these checks to return

104
00:04:09,599 --> 00:04:13,620
true at the end of the function so the

105
00:04:11,760 --> 00:04:15,780
mask is considered valid and in

106
00:04:13,620 --> 00:04:17,959
particular there is a check against this

107
00:04:15,780 --> 00:04:21,959
value

108
00:04:17,959 --> 00:04:24,360
0x6000F0 that we need to pass and so

109
00:04:21,959 --> 00:04:26,400
if you do the math the end result we

110
00:04:24,360 --> 00:04:31,040
found through reversing is that passing

111
00:04:26,400 --> 00:04:34,560
the notification mask value of

112
00:04:31,040 --> 00:04:36,840
0x39ffff0f actually allows us to get all the

113
00:04:34,560 --> 00:04:39,479
notifications and still pass the little

114
00:04:36,840 --> 00:04:41,040
sanity checks from the previous slide so

115
00:04:39,479 --> 00:04:43,440
another thing is there is this concept

116
00:04:41,040 --> 00:04:45,840
of superior enlistments which was kind

117
00:04:43,440 --> 00:04:48,120
of confusing at first and it seems to be

118
00:04:45,840 --> 00:04:50,280
used on distributed systems for

119
00:04:48,120 --> 00:04:53,220
transactions that have enlistments

120
00:04:50,280 --> 00:04:55,860
across multiple systems at first because

121
00:04:53,220 --> 00:04:58,259
of confusing API names we were doing

122
00:04:55,860 --> 00:05:00,600
everything using superior enlistments

123
00:04:58,259 --> 00:05:02,699
without even realizing it but the

124
00:05:00,600 --> 00:05:05,460
problem is there are certain things you

125
00:05:02,699 --> 00:05:08,580
can't do with Superior enlistments and so

126
00:05:05,460 --> 00:05:11,400
we ended up with all these weird errors

127
00:05:08,580 --> 00:05:13,440
and it turned out we had to not use

128
00:05:11,400 --> 00:05:15,479
Superior enlistments to trigger the bug

129
00:05:13,440 --> 00:05:18,840
but again this is the the type of things

130
00:05:15,479 --> 00:05:21,240
that playing around will teach you and

131
00:05:18,840 --> 00:05:23,759
yeah so we said we wanted to transition

132
00:05:21,240 --> 00:05:26,580
say for our enlistment like make the

133
00:05:23,759 --> 00:05:29,820
enlistment move from pre-prepared

134
00:05:26,580 --> 00:05:33,419
prepared and so on until committed and

135
00:05:29,820 --> 00:05:37,199
some API names that exist are pre-prepare

136
00:05:33,419 --> 00:05:38,460
enlistment prepare enlistment commit

137
00:05:37,199 --> 00:05:42,660
Enlistment

138
00:05:38,460 --> 00:05:45,300
so we thought yeah sure this must be the

139
00:05:42,660 --> 00:05:48,600
APIs we want to use right but it turns

140
00:05:45,300 --> 00:05:51,479
out the ones with enlistments at the end

141
00:05:48,600 --> 00:05:53,820
in the name like commit enlistment are

142
00:05:51,479 --> 00:05:56,340
for Superior enlistments which severally

143
00:05:53,820 --> 00:05:59,460
limit what you can do so actually the

144
00:05:56,340 --> 00:06:01,740
ones you want to use are the ones with

145
00:05:59,460 --> 00:06:05,280
complete at the end of the name like

146
00:06:01,740 --> 00:06:07,740
pre-prepare complete prepare complete or

147
00:06:05,280 --> 00:06:10,800
commit completes so yeah again trial

148
00:06:07,740 --> 00:06:12,539
and error taught us this type of thing this

149
00:06:10,800 --> 00:06:15,180
is basically a summary of the functions

150
00:06:12,539 --> 00:06:17,340
we want to use on the left column in

151
00:06:15,180 --> 00:06:20,580
order to transition from one state to

152
00:06:17,340 --> 00:06:22,979
another for non-superior enlistments and

153
00:06:20,580 --> 00:06:24,720
we want to avoid using the ones on the

154
00:06:22,979 --> 00:06:27,479
right column which are specific to

155
00:06:24,720 --> 00:06:29,400
Superior enlistment so I mentioned in

156
00:06:27,479 --> 00:06:32,340
a previous video the concept of rollback

157
00:06:29,400 --> 00:06:34,800
and so roll back easy for transaction is

158
00:06:32,340 --> 00:06:36,780
aborted because something goes wrong so

159
00:06:34,800 --> 00:06:38,940
KTM will just revert the entire

160
00:06:36,780 --> 00:06:41,340
transaction and so it initiate that

161
00:06:38,940 --> 00:06:43,380
rollback you can typically call the a

162
00:06:41,340 --> 00:06:46,259
rollback transaction function from

163
00:06:43,380 --> 00:06:48,419
userland I also mentioned the concept of

164
00:06:46,259 --> 00:06:51,000
recovery this is interesting to us

165
00:06:48,419 --> 00:06:54,419
because again the vulnerable function in

166
00:06:51,000 --> 00:06:57,479
KTM we want to analyze is called

167
00:06:54,419 --> 00:07:00,360
TmRecoverResourceManagerExt and there is

168
00:06:57,479 --> 00:07:03,300
this API in userland called recover

169
00:07:00,360 --> 00:07:06,960
resource manager which you can guess might

170
00:07:03,300 --> 00:07:09,060
end up calling TmRecoverResourceManagerExt

171
00:07:06,960 --> 00:07:11,400
in the kernel and basically the

172
00:07:09,060 --> 00:07:13,680
documentation states that if a

173
00:07:11,400 --> 00:07:16,680
transaction is in a state that sort of

174
00:07:13,680 --> 00:07:19,080
fails or temporarily interrupts but is

175
00:07:16,680 --> 00:07:21,599
in a way that it recovers and one

176
00:07:19,080 --> 00:07:24,479
example is that a resource manager goes

177
00:07:21,599 --> 00:07:27,000
down and then it comes back up you can

178
00:07:24,479 --> 00:07:29,580
recover the resource manager and all of

179
00:07:27,000 --> 00:07:32,099
the enlistments associated with it can

180
00:07:29,580 --> 00:07:34,560
be re-synchronized on a state and then

181
00:07:32,099 --> 00:07:36,360
it is in a recovered state and you can

182
00:07:34,560 --> 00:07:40,220
continue without aborting the

183
00:07:36,360 --> 00:07:40,220
transaction and having to roll back

