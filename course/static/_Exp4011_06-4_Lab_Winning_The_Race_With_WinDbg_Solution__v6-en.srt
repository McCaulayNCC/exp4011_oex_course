1
00:00:00,960 --> 00:00:06,240
so here I have WinDbg attached on the

2
00:00:03,419 --> 00:00:08,040
target VM however the actual patch and

3
00:00:06,240 --> 00:00:10,860
unpatched commands are not available yet

4
00:00:08,040 --> 00:00:15,020
the reason is because the patch file

5
00:00:10,860 --> 00:00:18,480
actually effectively loads the

6
00:00:15,020 --> 00:00:20,460
dbg-prep.cmd however the gdb-preps.cmd

7
00:00:18,480 --> 00:00:22,859
file

8
00:00:20,460 --> 00:00:26,220
in this file we can see it's commented

9
00:00:22,859 --> 00:00:29,760
out so we're gonna have to uncomment

10
00:00:26,220 --> 00:00:32,060
this and that's pretty much all we need

11
00:00:29,760 --> 00:00:32,060
to do

12
00:00:33,120 --> 00:00:38,520
so once you've done that you can detach

13
00:00:39,480 --> 00:00:43,040
and then reload it

14
00:00:47,219 --> 00:00:50,840
now we should be able to break

15
00:00:52,100 --> 00:00:58,579
and we can see that the JavaScript file

16
00:00:55,860 --> 00:00:58,579
has been loaded

17
00:01:00,180 --> 00:01:06,500
so now we're going to push the debugger

18
00:01:02,219 --> 00:01:06,500
race win binary onto the target VM

19
00:01:10,439 --> 00:01:13,520
and we're going to run it

20
00:01:18,479 --> 00:01:23,700
so we can see a couple of things we have

21
00:01:20,640 --> 00:01:26,400
the goals of this particular lab we can

22
00:01:23,700 --> 00:01:29,159
see it created all the objects that are

23
00:01:26,400 --> 00:01:32,280
required and the two enlistments that

24
00:01:29,159 --> 00:01:35,700
are specific GUID ending with 26 and 27

25
00:01:32,280 --> 00:01:37,799
then it goes into pre-preparing the

26
00:01:35,700 --> 00:01:40,200
enlistments and then we have the lab

27
00:01:37,799 --> 00:01:43,439
instructions we can see that it asks us

28
00:01:40,200 --> 00:01:45,299
to set two breakpoints one when

29
00:01:43,439 --> 00:01:47,400
TmRecoverResourceManagerExt is called and

30
00:01:45,299 --> 00:01:49,380
another one when the

31
00:01:47,400 --> 00:01:53,000
TmpSetNotificationResourceManager is called

32
00:01:49,380 --> 00:01:53,000
so we're going to set these breakpoints

33
00:02:04,380 --> 00:02:07,619
for now I'm just going to set this

34
00:02:05,820 --> 00:02:08,940
breakpoint because I know the second

35
00:02:07,619 --> 00:02:11,160
function is called when the first

36
00:02:08,940 --> 00:02:13,260
function is called and then we know

37
00:02:11,160 --> 00:02:15,840
we're going to have to use the patch

38
00:02:13,260 --> 00:02:18,540
command to block the recovery thread

39
00:02:15,840 --> 00:02:20,340
into the the function before we can free

40
00:02:18,540 --> 00:02:22,440
the enlistment from a second thread

41
00:02:20,340 --> 00:02:24,560
okay so we're going to hit the key to

42
00:02:22,440 --> 00:02:24,560
continue

43
00:02:25,319 --> 00:02:30,959
so here what we can see is that prepare

44
00:02:28,980 --> 00:02:34,739
complete has been called and then the

45
00:02:30,959 --> 00:02:36,780
instructions of the assisting thread are

46
00:02:34,739 --> 00:02:38,340
shown and now you see that the VM

47
00:02:36,780 --> 00:02:40,140
doesn't reply anymore because it's stuck

48
00:02:38,340 --> 00:02:42,480
into a breakpoint but basically this

49
00:02:40,140 --> 00:02:45,540
will only be executed when we hit enter

50
00:02:42,480 --> 00:02:49,140
when we resume the debugger so we're

51
00:02:45,540 --> 00:02:53,060
going to print the KRESOURCEMANAGER

52
00:02:49,140 --> 00:02:53,060
we know it's in the first arguments

53
00:03:04,739 --> 00:03:11,040
so we can see the resource manager has

54
00:03:08,459 --> 00:03:14,700
two enlistments so if we go on

55
00:03:11,040 --> 00:03:17,599
Vergilius for KENLISTMENT we have the

56
00:03:14,700 --> 00:03:17,599
KRESOURCEMANAGER

57
00:03:27,540 --> 00:03:32,459
so the KRESOURCEMANAGER we have the

58
00:03:29,819 --> 00:03:34,200
enlistments head which is a linked list

59
00:03:32,459 --> 00:03:36,239
of enlistments

60
00:03:34,200 --> 00:03:39,720
and so as you can imagine the

61
00:03:36,239 --> 00:03:42,180
enlistments are as we said uh linked

62
00:03:39,720 --> 00:03:45,540
together with the NextSameRm when they

63
00:03:42,180 --> 00:03:51,000
point from the resource manager so we

64
00:03:45,540 --> 00:03:55,159
have to subtract 88 to the pointer

65
00:03:51,000 --> 00:03:55,159
so these are the two enlistments

66
00:04:09,780 --> 00:04:14,299
so if we print the first enlistments

67
00:04:20,639 --> 00:04:27,720
so we can see it's valid because it's at

68
00:04:22,740 --> 00:04:30,660
the cookie and the GUID with 26.

69
00:04:27,720 --> 00:04:32,160
which is the value we saw earlier 26 and

70
00:04:30,660 --> 00:04:33,540
27

71
00:04:32,160 --> 00:04:36,080
so now if we look at the second

72
00:04:33,540 --> 00:04:36,080
enlistment

73
00:04:47,040 --> 00:04:51,360
you see the second enlistment ends with

74
00:04:48,900 --> 00:04:53,040
the GUID 27 so we have our two

75
00:04:51,360 --> 00:04:55,620
enlistments that are part of the same

76
00:04:53,040 --> 00:04:59,400
transaction 8f0 and

77
00:04:55,620 --> 00:05:04,139
8f0 okay so if you go in Ghidra

78
00:04:59,400 --> 00:05:06,540
and look at when the

79
00:05:04,139 --> 00:05:07,979
TmpSetNotificationResourceManager

80
00:05:06,540 --> 00:05:09,800
function is called

81
00:05:07,979 --> 00:05:14,220
I'm going to set a breakpoint on that

82
00:05:09,800 --> 00:05:15,540
function call with CTRL+F2

83
00:05:14,220 --> 00:05:17,340
you can see the breakpoint has been

84
00:05:15,540 --> 00:05:20,240
added in WinDbg so we're going to

85
00:05:17,340 --> 00:05:20,240
continue execution

86
00:05:20,280 --> 00:05:25,500
okay so now we hit the first call for

87
00:05:23,160 --> 00:05:27,660
the first enlistment so the enlistment

88
00:05:25,500 --> 00:05:29,160
is in

89
00:05:27,660 --> 00:05:34,259
um

90
00:05:29,160 --> 00:05:37,039
is in R14 because it's accessed after

91
00:05:34,259 --> 00:05:37,039
the call

92
00:05:37,380 --> 00:05:40,380
so

93
00:05:44,639 --> 00:05:48,780
yeah so that's our first enlistment so

94
00:05:47,340 --> 00:05:51,120
we're going to continue execution I want

95
00:05:48,780 --> 00:05:53,160
to reach the second enlistments because

96
00:05:51,120 --> 00:05:55,199
if we go back to the Visual Studio code

97
00:05:53,160 --> 00:05:56,460
remember that thread plus enlistment

98
00:05:55,199 --> 00:05:59,180
function is called for the second

99
00:05:56,460 --> 00:05:59,180
enlistment

100
00:06:02,460 --> 00:06:06,120
here we pass this handle to the second

101
00:06:04,560 --> 00:06:08,639
enlistment so that's the one that's going

102
00:06:06,120 --> 00:06:11,100
to be freed

103
00:06:08,639 --> 00:06:13,800
so if we go back here we continue

104
00:06:11,100 --> 00:06:15,360
execution we want to hit this call

105
00:06:13,800 --> 00:06:17,580
TmpSetNotificationResourceManager a

106
00:06:15,360 --> 00:06:20,340
second time

107
00:06:17,580 --> 00:06:23,759
okay so we hit it a second time

108
00:06:20,340 --> 00:06:25,440
here we indeed are dealing with our

109
00:06:23,759 --> 00:06:26,819
second enlistments so what we're going

110
00:06:25,440 --> 00:06:29,180
to do here is going to use the patch

111
00:06:26,819 --> 00:06:29,180
command

112
00:06:29,280 --> 00:06:33,060
but before we do remember

113
00:06:31,480 --> 00:06:37,340


114
00:06:33,060 --> 00:06:37,340
it's going to patch the call

115
00:06:39,720 --> 00:06:43,520
KeWaitForSingleObject

116
00:06:44,340 --> 00:06:49,880
okay so let's actually

117
00:06:46,759 --> 00:06:49,880
patch it

118
00:06:52,080 --> 00:06:55,860
and then we continue execution so if

119
00:06:54,180 --> 00:06:57,600
you're not just setting a breakpoint to

120
00:06:55,860 --> 00:06:59,639
automatically patch it once the

121
00:06:57,600 --> 00:07:01,860
breakpoint hits

122
00:06:59,639 --> 00:07:04,440
continue execution okay

123
00:07:01,860 --> 00:07:06,539
so what we see here is that our

124
00:07:04,440 --> 00:07:09,300
breakpoints hit

125
00:07:06,539 --> 00:07:11,100
and it automatically replace the

126
00:07:09,300 --> 00:07:13,440
instruction at that address

127
00:07:11,100 --> 00:07:17,160
with a forever loop so if we go back

128
00:07:13,440 --> 00:07:19,620
here and print the code

129
00:07:17,160 --> 00:07:22,199
as you can see here there is a jump

130
00:07:19,620 --> 00:07:23,880
to the same address and then an invalid

131
00:07:22,199 --> 00:07:27,060
instruction because we just patched two

132
00:07:23,880 --> 00:07:29,340
bytes so here it's stucked

133
00:07:27,060 --> 00:07:34,080
so we continue execution so now if we go

134
00:07:29,340 --> 00:07:36,180
back to the target VM we see that we can

135
00:07:34,080 --> 00:07:38,419
now hit the key to continue it's going

136
00:07:36,180 --> 00:07:41,400
to actually execute the code from our

137
00:07:38,419 --> 00:07:43,919
assisting thread and then we're going to

138
00:07:41,400 --> 00:07:46,259
be able to free the enlistments so we hit

139
00:07:43,919 --> 00:07:48,960
a key we see enlistment should be

140
00:07:46,259 --> 00:07:50,759
freed so now we go back in the debugger

141
00:07:48,960 --> 00:07:54,060
and now what we want to do is we want to

142
00:07:50,759 --> 00:07:57,560
unblock the recovery thread so we can do

143
00:07:54,060 --> 00:07:57,560
that and do unpatch

144
00:08:04,080 --> 00:08:09,300
so now if we print the code again

145
00:08:06,960 --> 00:08:11,280
we can see it's restored we have the

146
00:08:09,300 --> 00:08:14,759
move rcx, rbx

147
00:08:11,280 --> 00:08:18,660
so now before we actually let it go

148
00:08:14,759 --> 00:08:20,580
we may want to set a breakpoint on a

149
00:08:18,660 --> 00:08:24,800
couple of instructions after so after

150
00:08:20,580 --> 00:08:24,800
it's restored so let's do it on this

151
00:08:27,720 --> 00:08:33,779
and we continue execution

152
00:08:30,539 --> 00:08:36,360
okay so now our recovery thread was

153
00:08:33,779 --> 00:08:38,520
restored let's look at the actual

154
00:08:36,360 --> 00:08:40,620
enlistment we have

155
00:08:38,520 --> 00:08:43,520
so this is the first enlistments this

156
00:08:40,620 --> 00:08:43,520
is the second enlistments

157
00:08:44,580 --> 00:08:51,959
427 so is this memory valid

158
00:08:49,740 --> 00:08:53,100
it looks like it's still containing the

159
00:08:51,959 --> 00:08:57,019
data

160
00:08:53,100 --> 00:08:57,019
from our enlistments

161
00:09:02,760 --> 00:09:07,320
so this is going to take a couple of

162
00:09:04,560 --> 00:09:10,500
seconds okay so it did take a few

163
00:09:07,320 --> 00:09:13,740
minutes actually to finish parsing all

164
00:09:10,500 --> 00:09:17,760
the verifier pool information

165
00:09:13,740 --> 00:09:21,000
so we can see uh it passed all the kernel

166
00:09:17,760 --> 00:09:23,399
pool allocate and free operations

167
00:09:21,000 --> 00:09:25,500
and it actually found one that is very

168
00:09:23,399 --> 00:09:29,940
interesting because it's telling us that

169
00:09:25,500 --> 00:09:31,100
the the address we try to verify was

170
00:09:29,940 --> 00:09:34,680
actually

171
00:09:31,100 --> 00:09:37,740
freed using the NtClose which

172
00:09:34,680 --> 00:09:40,620
corresponds to our call from userland to

173
00:09:37,740 --> 00:09:44,040
close the handle so indeed we've just

174
00:09:40,620 --> 00:09:46,320
verified that the address we try to

175
00:09:44,040 --> 00:09:49,500
print as a KENLISTMENT is actually free

176
00:09:46,320 --> 00:09:52,740
memory so we made it we managed to prove

177
00:09:49,500 --> 00:09:56,399
our mental model that the race condition

178
00:09:52,740 --> 00:09:59,339
can be won using the debugger and the

179
00:09:56,399 --> 00:10:02,160
verifier one thing to note is that this

180
00:09:59,339 --> 00:10:04,019
is still valid memory because of the

181
00:10:02,160 --> 00:10:06,540
verifier but the verifier is telling us

182
00:10:04,019 --> 00:10:08,700
it's freed which is great

183
00:10:06,540 --> 00:10:10,080
one last thing is that if we continue

184
00:10:08,700 --> 00:10:12,899
execution

185
00:10:10,080 --> 00:10:15,560
you'll notice that it doesn't crash

186
00:10:12,899 --> 00:10:18,180
and it's basically because the verifier

187
00:10:15,560 --> 00:10:20,700
detected the

188
00:10:18,180 --> 00:10:22,680
the free and it's not going to reuse the

189
00:10:20,700 --> 00:10:25,800
memory right away

190
00:10:22,680 --> 00:10:28,740
so yeah so I just want to show one

191
00:10:25,800 --> 00:10:32,339
last thing which is to see now that

192
00:10:28,740 --> 00:10:34,500
we've confirmed the verifier shows that

193
00:10:32,339 --> 00:10:36,240
we win the race that we want to trigger

194
00:10:34,500 --> 00:10:38,720
the same thing without verifier so we're

195
00:10:36,240 --> 00:10:41,820
going to detach

196
00:10:38,720 --> 00:10:43,680
and we're just gonna restore the

197
00:10:41,820 --> 00:10:45,779
snapshots

198
00:10:43,680 --> 00:10:47,640
without verifier so we don't want to

199
00:10:45,779 --> 00:10:51,740
restore that snapshot we want to restore

200
00:10:47,640 --> 00:10:51,740
the snapshot without verifier

201
00:10:53,940 --> 00:10:58,440
also this snapshot is the one we want to

202
00:10:56,640 --> 00:11:00,720
use for the rest of the training anyway

203
00:10:58,440 --> 00:11:04,500
so we may as well restore that snapshot

204
00:11:00,720 --> 00:11:07,220
and now we can reattach with WinDbg

205
00:11:04,500 --> 00:11:11,360
so now we're going to push the

206
00:11:07,220 --> 00:11:11,360
binary onto the target VM

207
00:11:19,680 --> 00:11:23,120
and we're going to execute it

208
00:11:26,760 --> 00:11:33,140
so we can see here both of our

209
00:11:28,680 --> 00:11:33,140
enlistments end with C7 and C8

210
00:11:33,360 --> 00:11:39,079
so we're going to break same thing as

211
00:11:35,579 --> 00:11:39,079
before on this function

212
00:11:45,000 --> 00:11:49,980
and continue execution

213
00:11:47,579 --> 00:11:52,380
now I'm going to hit the key

214
00:11:49,980 --> 00:11:54,420
to trigger our

215
00:11:52,380 --> 00:11:56,339
call on the vulnerable function here

216
00:11:54,420 --> 00:12:00,360
it's waiting to free the enlistment so we

217
00:11:56,339 --> 00:12:03,800
don't do it yet just gonna see

218
00:12:00,360 --> 00:12:03,800
the KRESOURCEMANAGER

219
00:12:10,680 --> 00:12:15,500
and both of our enlistments are here

220
00:12:25,440 --> 00:12:30,779
so the first enlistment

221
00:12:28,200 --> 00:12:34,160
end with C7

222
00:12:30,779 --> 00:12:34,160
and the second enlistments

223
00:12:36,899 --> 00:12:43,380
and with C8 so we're gonna set the break

224
00:12:40,200 --> 00:12:46,019
points on the call

225
00:12:43,380 --> 00:12:48,839
TmpSet

226
00:12:46,019 --> 00:12:51,420
CTRL+F2

227
00:12:48,839 --> 00:12:53,040
and then continue execution so we know

228
00:12:51,420 --> 00:12:55,139
the first hit is for the first

229
00:12:53,040 --> 00:12:58,500
enlistment so we skip it

230
00:12:55,139 --> 00:13:03,540
I'll go to the second enlistments so now

231
00:12:58,500 --> 00:13:06,980
in R14 we have bc0 which is

232
00:13:03,540 --> 00:13:06,980
um our second enlistment

233
00:13:10,740 --> 00:13:15,600
indeed

234
00:13:13,139 --> 00:13:18,180
um so now we're gonna set

235
00:13:15,600 --> 00:13:21,320
the patch

236
00:13:18,180 --> 00:13:21,320
and continue execution

237
00:13:22,380 --> 00:13:24,920
okay

238
00:13:25,019 --> 00:13:29,700
so it did hit the patch so now we can

239
00:13:27,300 --> 00:13:32,579
free the enlistments

240
00:13:29,700 --> 00:13:36,899
by hitting a key so an enlistment should be

241
00:13:32,579 --> 00:13:39,240
freed and now we're gonna restore

242
00:13:36,899 --> 00:13:42,560
patch so

243
00:13:39,240 --> 00:13:42,560
if we do unpatch

244
00:13:43,740 --> 00:13:47,220
and now we're just going to set the

245
00:13:45,360 --> 00:13:51,560
breakpoints

246
00:13:47,220 --> 00:13:51,560
just after it restores

247
00:13:58,079 --> 00:14:01,820
okay continue execution

248
00:14:02,399 --> 00:14:07,760
okay so now if we look at the second

249
00:14:04,980 --> 00:14:07,760
enlistments

250
00:14:08,880 --> 00:14:15,380
which was this address

251
00:14:12,600 --> 00:14:15,380
bc0

252
00:14:24,480 --> 00:14:28,740
what do we have here is it an enlistment

253
00:14:26,700 --> 00:14:31,800
so it still still looks like a valid

254
00:14:28,740 --> 00:14:34,880
enlistment and if we do pool

255
00:14:31,800 --> 00:14:34,880
on this address

256
00:14:38,120 --> 00:14:45,600
so here without verifier basically the

257
00:14:42,540 --> 00:14:48,300
memory is shown as freed but as what is

258
00:14:45,600 --> 00:14:50,760
really interesting is that the size is

259
00:14:48,300 --> 00:14:53,459
not the size of an an enlistments anymore

260
00:14:50,760 --> 00:14:57,240
because it should be just around 2c0

261
00:14:53,459 --> 00:15:00,300
based on the size sorry 1e0 based on

262
00:14:57,240 --> 00:15:01,980
the size of Vergilius however it's 490

263
00:15:00,300 --> 00:15:03,480
for the free check because of collation

264
00:15:01,980 --> 00:15:07,260
but

265
00:15:03,480 --> 00:15:11,100
here the memory is still valid so if we

266
00:15:07,260 --> 00:15:12,540
don't show it using the pool command we

267
00:15:11,100 --> 00:15:14,399
wouldn't see that we won the race

268
00:15:12,540 --> 00:15:16,620
because the memory hasn't been replaced

269
00:15:14,399 --> 00:15:19,199
yet so it's still valid and it's not

270
00:15:16,620 --> 00:15:22,440
going to crash even though it's freed so

271
00:15:19,199 --> 00:15:25,160
now I'm just gonna step uh just to see

272
00:15:22,440 --> 00:15:25,160
where it goes

273
00:15:25,500 --> 00:15:28,459
so we see

274
00:15:28,860 --> 00:15:33,320
it's actually testing some flags

275
00:15:33,660 --> 00:15:41,279
and it's actually going to fetch the

276
00:15:37,019 --> 00:15:43,740
next enlistment using the move RDI

277
00:15:41,279 --> 00:15:48,019
[RDI]

278
00:15:43,740 --> 00:15:48,019
and here if you look at RDI

279
00:15:51,000 --> 00:15:58,800
we are basically in the middle of our

280
00:15:54,420 --> 00:16:01,920
enlistment and so basically the DC 48

281
00:15:58,800 --> 00:16:04,860
pointer is fetched from a free chunk so

282
00:16:01,920 --> 00:16:07,260
it's actually accessing free memory we

283
00:16:04,860 --> 00:16:09,959
have our use-after-free we managed to do

284
00:16:07,260 --> 00:16:12,300
it and this is actually very exciting to

285
00:16:09,959 --> 00:16:14,160
see a free chunk being used for so many

286
00:16:12,300 --> 00:16:16,620
people the part of exploitation they

287
00:16:14,160 --> 00:16:19,320
want to chase is this idea of popping

288
00:16:16,620 --> 00:16:22,079
shells and stuff which obviously is nice

289
00:16:19,320 --> 00:16:24,300
but when you spend several days or weeks

290
00:16:22,079 --> 00:16:27,540
reversing code and doing all of this

291
00:16:24,300 --> 00:16:29,459
work and theory crafting and banging

292
00:16:27,540 --> 00:16:31,560
your head trying to understand how

293
00:16:29,459 --> 00:16:33,899
everything works to hit something like

294
00:16:31,560 --> 00:16:37,139
this that shows that all of your work

295
00:16:33,899 --> 00:16:39,060
was legit is as awesome as popping

296
00:16:37,139 --> 00:16:41,940
shells sometimes and it's good to

297
00:16:39,060 --> 00:16:43,920
remember it and enjoy that feeling as

298
00:16:41,940 --> 00:16:46,500
there will be other challenges to solve

299
00:16:43,920 --> 00:16:49,079
later on so really I advise that you

300
00:16:46,500 --> 00:16:50,940
just enjoy that moment without saying

301
00:16:49,079 --> 00:16:53,519
anything for 10 seconds and to go

302
00:16:50,940 --> 00:16:55,620
conclude I hope you agree that it is

303
00:16:53,519 --> 00:16:58,259
worthwhile to confirm we can win the

304
00:16:55,620 --> 00:17:01,259
race with the assistance of the debugger

305
00:16:58,259 --> 00:17:02,940
either with something like verifier or

306
00:17:01,259 --> 00:17:05,760
manually stepping in the debugger

307
00:17:02,940 --> 00:17:09,000
because if you didn't do this route and

308
00:17:05,760 --> 00:17:11,040
you had some idea how to trigger the bug

309
00:17:09,000 --> 00:17:13,919
the race condition and you just wrote

310
00:17:11,040 --> 00:17:16,380
the code in userland and run the

311
00:17:13,919 --> 00:17:18,720
program hoping it would crash it may be

312
00:17:16,380 --> 00:17:21,179
that as you've just seen you trigger the

313
00:17:18,720 --> 00:17:23,699
actual vulnerability but nothing bad

314
00:17:21,179 --> 00:17:27,000
actually happens which is you don't see

315
00:17:23,699 --> 00:17:29,820
a crash because the code is just using

316
00:17:27,000 --> 00:17:32,340
stale memory that is still valid and you

317
00:17:29,820 --> 00:17:34,500
might incorrectly think that you are not

318
00:17:32,340 --> 00:17:36,600
triggering the vulnerability when you

319
00:17:34,500 --> 00:17:39,000
are actually are and you could be spending

320
00:17:36,600 --> 00:17:41,940
hours trying to figure out what is

321
00:17:39,000 --> 00:17:44,340
unusual in your userland code is wrong

322
00:17:41,940 --> 00:17:46,799
or reversing more and more part of the

323
00:17:44,340 --> 00:17:49,559
kernel code hunting for actual

324
00:17:46,799 --> 00:17:51,720
hints of why you don't seem to trigger

325
00:17:49,559 --> 00:17:54,299
the bug so yes the the methodology

326
00:17:51,720 --> 00:17:56,640
relying on using the debugger to confirm

327
00:17:54,299 --> 00:17:59,100
the mental model is generally a good

328
00:17:56,640 --> 00:18:02,000
idea and just to prove my points let's

329
00:17:59,100 --> 00:18:02,000
continue execution

330
00:18:05,760 --> 00:18:10,679
as you can see

331
00:18:07,919 --> 00:18:12,600
it didn't crash

332
00:18:10,679 --> 00:18:15,539
because the memory even though it was

333
00:18:12,600 --> 00:18:17,760
freed still contained the previous data

334
00:18:15,539 --> 00:18:19,880
hope you enjoyed that part and see you

335
00:18:17,760 --> 00:18:19,880
later

