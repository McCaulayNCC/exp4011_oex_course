1
00:00:00,120 --> 00:00:06,240
so the variable function is called

2
00:00:03,959 --> 00:00:09,059
TmRecoverResourceManager and it is

3
00:00:06,240 --> 00:00:11,160
trying to do a recovery of something and

4
00:00:09,059 --> 00:00:12,719
if we look at the code we see it walks

5
00:00:11,160 --> 00:00:16,460
up a linked list,

6
00:00:12,719 --> 00:00:19,980
check some flags, releases some mutex

7
00:00:16,460 --> 00:00:22,140
and sends notifications the fact that it

8
00:00:19,980 --> 00:00:24,240
sends notification we're actually going

9
00:00:22,140 --> 00:00:26,880
to use it from userland because we're

10
00:00:24,240 --> 00:00:29,220
actually going to abuse the fact that it

11
00:00:26,880 --> 00:00:31,740
sends notification to read them from

12
00:00:29,220 --> 00:00:34,739
userland and know exactly what

13
00:00:31,740 --> 00:00:37,260
enlistment was last touched so that's very

14
00:00:34,739 --> 00:00:40,379
important for us this function is

15
00:00:37,260 --> 00:00:42,960
parsing enlistments and so you should

16
00:00:40,379 --> 00:00:44,820
probably try to remember this graph and

17
00:00:42,960 --> 00:00:46,200
I think it's very important to visualize

18
00:00:44,820 --> 00:00:48,719
this graph when you're actually

19
00:00:46,200 --> 00:00:50,520
exploiting this bug because I think it

20
00:00:48,719 --> 00:00:52,980
helps especially if you're a visual

21
00:00:50,520 --> 00:00:54,899
person so there is the KRESOURCEMANAGER

22
00:00:52,980 --> 00:00:57,300
which is the argument of the

23
00:00:54,899 --> 00:01:00,300
function and from this resource manager

24
00:00:57,300 --> 00:01:01,920
it is going to take the enlistment head then

25
00:01:00,300 --> 00:01:04,680
it's going to go through the different

26
00:01:01,920 --> 00:01:07,020
enlistments each one part of one

27
00:01:04,680 --> 00:01:08,939
transaction it could be a different

28
00:01:07,020 --> 00:01:11,040
transaction or the same it doesn't

29
00:01:08,939 --> 00:01:13,200
really matter but what matters is that

30
00:01:11,040 --> 00:01:15,119
it is parsing the enlistments at for

31
00:01:13,200 --> 00:01:18,180
each enlistments it is checking the

32
00:01:15,119 --> 00:01:21,659
flags it is either acquiring the locking

33
00:01:18,180 --> 00:01:24,479
the mutex of objects what it needs to

34
00:01:21,659 --> 00:01:27,240
modify them or the code is releasing

35
00:01:24,479 --> 00:01:29,340
which is unlocking the mutex when it

36
00:01:27,240 --> 00:01:31,320
wants to allow other code to modify

37
00:01:29,340 --> 00:01:34,080
these objects and so it's just like

38
00:01:31,320 --> 00:01:36,600
usual coding kernel but most importantly

39
00:01:34,080 --> 00:01:38,939
the goal of the function is to notify

40
00:01:36,600 --> 00:01:40,979
the enlistments in order to let them

41
00:01:38,939 --> 00:01:43,380
know of the different states that have

42
00:01:40,979 --> 00:01:45,659
changed and so from a high level that's

43
00:01:43,380 --> 00:01:48,299
pretty all we need to remember the fact

44
00:01:45,659 --> 00:01:51,479
that it locked the mutex or release them

45
00:01:48,299 --> 00:01:55,280
is important for the bug but it's just a

46
00:01:51,479 --> 00:01:55,280
detail in terms of functionality

