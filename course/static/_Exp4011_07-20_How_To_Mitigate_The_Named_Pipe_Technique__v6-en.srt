1
00:00:00,179 --> 00:00:05,700
I suspect that it's gonna be really hard

2
00:00:02,700 --> 00:00:07,740
to harden the named pipe system to avoid

3
00:00:05,700 --> 00:00:10,740
attackers to use it

4
00:00:07,740 --> 00:00:12,240
for feng shui primitives and that if

5
00:00:10,740 --> 00:00:14,580
they want to avoid this kind of

6
00:00:12,240 --> 00:00:17,279
technique it will be more based off the

7
00:00:14,580 --> 00:00:19,740
changes to the heaps themselves so like

8
00:00:17,279 --> 00:00:22,500
replacing the pool algorithm that we see

9
00:00:19,740 --> 00:00:25,019
on Windows 10 1809 with something like

10
00:00:22,500 --> 00:00:27,900
the low fragmentation heap also known as

11
00:00:25,019 --> 00:00:30,840
LFH because it is specifically designed

12
00:00:27,900 --> 00:00:33,840
to be like actual pool where lots of

13
00:00:30,840 --> 00:00:36,059
chunks of the same type are all stored

14
00:00:33,840 --> 00:00:38,640
together I believe it has delayed free

15
00:00:36,059 --> 00:00:41,520
but it also has things like a random

16
00:00:38,640 --> 00:00:44,399
allocation order so you can't predict

17
00:00:41,520 --> 00:00:46,860
the order in which things are allocated

18
00:00:44,399 --> 00:00:49,620
and what adjacency we will have and

19
00:00:46,860 --> 00:00:51,719
stuff it is specifically meant to just

20
00:00:49,620 --> 00:00:54,719
hinder this type of predictable layout

21
00:00:51,719 --> 00:00:56,879
so I suspect they will favor that kind

22
00:00:54,719 --> 00:00:59,699
of thing because if they just target the

23
00:00:56,879 --> 00:01:02,520
named pipe stuff they will probably be

24
00:00:59,699 --> 00:01:06,299
something else and so Microsoft prefer

25
00:01:02,520 --> 00:01:08,460
to have a more broad mitigation than

26
00:01:06,299 --> 00:01:10,740
targeting specific components

27
00:01:08,460 --> 00:01:13,140
because traditionally that's what

28
00:01:10,740 --> 00:01:16,320
Microsoft did and then people would just

29
00:01:13,140 --> 00:01:19,439
have other techniques that are zero day

30
00:01:16,320 --> 00:01:22,020
techniques and then they would just use

31
00:01:19,439 --> 00:01:25,080
them as soon as the one trick is patched

32
00:01:22,020 --> 00:01:27,600
for instance in the browser that's what

33
00:01:25,080 --> 00:01:29,820
the browser's company did like they used

34
00:01:27,600 --> 00:01:32,820
dedicated pools for different objects

35
00:01:29,820 --> 00:01:35,040
the array objects were used a lot by

36
00:01:32,820 --> 00:01:37,020
browsers exploiters to actually replace

37
00:01:35,040 --> 00:01:39,900
previous objects with data you control

38
00:01:37,020 --> 00:01:42,600
because in an array you can just put any

39
00:01:39,900 --> 00:01:44,820
byte value to any position and in flash

40
00:01:42,600 --> 00:01:47,280
as well attackers would typically use

41
00:01:44,820 --> 00:01:49,320
the vectors for the same purpose so to

42
00:01:47,280 --> 00:01:52,320
avoid that the software companies

43
00:01:49,320 --> 00:01:54,960
basically dedicate specific pools for

44
00:01:52,320 --> 00:01:57,840
these objects that are like the magical

45
00:01:54,960 --> 00:02:00,600
object that allow you to control all the

46
00:01:57,840 --> 00:02:02,579
data and so if you have a use-after-free

47
00:02:00,600 --> 00:02:04,259
you would not be able to replace your

48
00:02:02,579 --> 00:02:06,659
object with this magical powerful

49
00:02:04,259 --> 00:02:09,000
primitive anymore because they are on

50
00:02:06,659 --> 00:02:11,400
other heaps and so they can't be used

51
00:02:09,000 --> 00:02:14,160
anymore so that's definitely going to be

52
00:02:11,400 --> 00:02:18,020
the way to go to avoid techniques such

53
00:02:14,160 --> 00:02:18,020
as using the named pipes for feng shui

