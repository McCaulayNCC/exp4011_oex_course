0
00:00:03,415 --> 00:00:07,930
the goal of this part is to check the debug environment we have

1
00:00:07,955 --> 00:00:10,938
so we have it available for the whole training we're

2
00:00:11,079 --> 00:00:16,440
going to see how we configured the two

3
00:00:13,440 --> 00:00:19,680
virtual machines how we were able to

4
00:00:16,440 --> 00:00:21,539
configure the debugger WinDbg as well

5
00:00:19,680 --> 00:00:24,420
as the disassembler and the decompiler

6
00:00:21,539 --> 00:00:26,939
which is Ghidra and synchronize them

7
00:00:24,420 --> 00:00:29,400
together with ret-sync we are also going

8
00:00:26,939 --> 00:00:32,880
to see how we configure the visual

9
00:00:29,400 --> 00:00:35,640
studio project at SSH to push files from

10
00:00:32,880 --> 00:00:38,399
the debugger VM to the target VM

11
00:00:35,640 --> 00:00:40,980
we are going to see all the steps that

12
00:00:38,399 --> 00:00:43,800
are required in this slide deck and then

13
00:00:40,980 --> 00:00:47,100
at the end I'll go over all the steps

14
00:00:43,800 --> 00:00:50,280
one by one and show you how to do them

15
00:00:47,100 --> 00:00:52,320
okay let's get started these tools are

16
00:00:50,280 --> 00:00:54,559
basically what we use every day when we

17
00:00:52,320 --> 00:00:58,020
are writing exploits so for the course

18
00:00:54,559 --> 00:01:00,120
we will basically use VMware to run the

19
00:00:58,020 --> 00:01:02,399
Windows target environment where we run

20
00:01:00,120 --> 00:01:05,280
the exploits and then a second virtual

21
00:01:02,399 --> 00:01:07,979
machine to run a debug environment where

22
00:01:05,280 --> 00:01:09,960
we compile our exploits and we will use

23
00:01:07,979 --> 00:01:12,420
Ghidra to reverse engineer the Windows

24
00:01:09,960 --> 00:01:15,119
binaries or alternatively you can use

25
00:01:12,420 --> 00:01:17,939
IDA Pro and then we'll use WinDbg

26
00:01:15,119 --> 00:01:20,400
preview to debug the target VM from the

27
00:01:17,939 --> 00:01:23,100
debugger VM and we will use ret-sync

28
00:01:20,400 --> 00:01:24,960
which is a plugin both in Ghidra and

29
00:01:23,100 --> 00:01:27,119
WinDbg in order to synchronize the

30
00:01:24,960 --> 00:01:28,799
disassembler and the debugger we

31
00:01:27,119 --> 00:01:30,960
basically provide a bunch of files for

32
00:01:28,799 --> 00:01:33,840
this course there is a Ghidra project

33
00:01:30,960 --> 00:01:35,579
archive file that contains some reverse

34
00:01:33,840 --> 00:01:38,820
files with different level of reversing

35
00:01:35,579 --> 00:01:41,880
to show you how far you can go with

36
00:01:38,820 --> 00:01:45,000
reversing and you will try to replicate

37
00:01:41,880 --> 00:01:46,920
that there are Labs source code that you

38
00:01:45,000 --> 00:01:49,320
can use as a starting point when you're

39
00:01:46,920 --> 00:01:52,079
going to be dealing with solving the

40
00:01:49,320 --> 00:01:55,320
challenges there are dlls and

41
00:01:52,079 --> 00:01:57,600
executables used in this course and they

42
00:01:55,320 --> 00:02:00,180
match the versions used in the Ghidra

43
00:01:57,600 --> 00:02:02,460
projects also it includes the runnable

44
00:02:00,180 --> 00:02:06,299
driver for the kernel transaction

45
00:02:02,460 --> 00:02:08,280
manager it is named tm.sys and we're

46
00:02:06,299 --> 00:02:12,540
going to need that in order to replace

47
00:02:08,280 --> 00:02:14,819
the old driver on the target VM is

48
00:02:12,540 --> 00:02:16,620
because at the moment the target VM is

49
00:02:14,819 --> 00:02:18,420
patched to the vulnerability we want to

50
00:02:16,620 --> 00:02:19,920
exploit in this course so we're going to

51
00:02:18,420 --> 00:02:21,780
need to replace that file with the

52
00:02:19,920 --> 00:02:23,760
actual vulnerable one there are also

53
00:02:21,780 --> 00:02:27,480
interesting files such as configuration

54
00:02:23,760 --> 00:02:30,000
files for Ghidra or helper scripts for

55
00:02:27,480 --> 00:02:33,060
WinDbg to help automate debugging

56
00:02:30,000 --> 00:02:36,180
experience and some cheat sheets to use

57
00:02:33,060 --> 00:02:38,959
Ghidra or WinDbg as a requirement for

58
00:02:36,180 --> 00:02:42,420
this course you should follow the dbg

59
00:02:38,959 --> 00:02:45,120
3011 course for building two virtual

60
00:02:42,420 --> 00:02:47,099
machines one target VM where we run the

61
00:02:45,120 --> 00:02:49,080
exploits we can crash the virtual

62
00:02:47,099 --> 00:02:52,800
machine and restore each from a

63
00:02:49,080 --> 00:02:54,720
snapshots and a target VM where we write

64
00:02:52,800 --> 00:02:57,959
the exploit source code in C language

65
00:02:54,720 --> 00:02:59,879
build the exploit with Visual Studio and

66
00:02:57,959 --> 00:03:02,040
automatically push the compiled exploit

67
00:02:59,879 --> 00:03:04,080
onto the target VM if you don't have

68
00:03:02,040 --> 00:03:05,700
these two virtual machines ready you

69
00:03:04,080 --> 00:03:08,519
must refer to this other course first

70
00:03:05,700 --> 00:03:10,800
for multiple reasons the VM will be in a

71
00:03:08,519 --> 00:03:13,379
state that allows for working debugging

72
00:03:10,800 --> 00:03:17,760
and if you don't follow that tutorial

73
00:03:13,379 --> 00:03:19,620
WinDbg sometimes break features and we

74
00:03:17,760 --> 00:03:22,140
tested that version so we know it will

75
00:03:19,620 --> 00:03:24,959
work for you we also provide reversed

76
00:03:22,140 --> 00:03:29,159
engineered binaries in a GHIDRA project

77
00:03:24,959 --> 00:03:34,860
that match specific versions that we

78
00:03:29,159 --> 00:03:37,860
know were used in the dbg 3011 course

79
00:03:34,860 --> 00:03:40,920
we will be providing actual Labs that is

80
00:03:37,860 --> 00:03:43,799
exercises in a format that can be easily

81
00:03:40,920 --> 00:03:46,860
compiled on the debugger VM so the

82
00:03:43,799 --> 00:03:49,500
debugger VM can be named development VM

83
00:03:46,860 --> 00:03:53,220
and the target VM can be named

84
00:03:49,500 --> 00:03:56,099
vulnerable VM or debug VM we are going

85
00:03:53,220 --> 00:03:58,200
to replace the tm.sys kernel driver

86
00:03:56,099 --> 00:04:00,480
related to the kernel transaction

87
00:03:58,200 --> 00:04:02,580
manager in the target VM with the

88
00:04:00,480 --> 00:04:04,260
vulnerable one the procedure is

89
00:04:02,580 --> 00:04:06,659
basically describing the provided link

90
00:04:04,260 --> 00:04:11,040
but it requires changing the ownership

91
00:04:06,659 --> 00:04:14,400
of the tm.sys file into C Windows system

92
00:04:11,040 --> 00:04:15,540
32 drivers folder so you can then rename

93
00:04:14,400 --> 00:04:18,000
it because you have the right

94
00:04:15,540 --> 00:04:20,880
permissions and then you will basically

95
00:04:18,000 --> 00:04:22,979
have to replace the old file with the

96
00:04:20,880 --> 00:04:24,600
reliable copy we provide in the tools

97
00:04:22,979 --> 00:04:27,840
reverse folder

98
00:04:24,600 --> 00:04:29,820
from the dbg 3011 course you should have

99
00:04:27,840 --> 00:04:31,919
configured Windows cannot debugging

100
00:04:29,820 --> 00:04:33,960
already and you should be able to debug

101
00:04:31,919 --> 00:04:36,300
the target VM using the debugger VM

102
00:04:33,960 --> 00:04:37,800
using a batch script to automate the

103
00:04:36,300 --> 00:04:40,020
debugging experience

104
00:04:37,800 --> 00:04:42,840
Ghidra should already be installed from

105
00:04:40,020 --> 00:04:44,400
the dbg3011 course as well but you are

106
00:04:42,840 --> 00:04:47,100
going to need to import the Ghidra

107
00:04:44,400 --> 00:04:50,100
project related to this exploit course

108
00:04:47,100 --> 00:04:52,560
which contained the dlls and executables

109
00:04:50,100 --> 00:04:55,259
required in this course after we import

110
00:04:52,560 --> 00:04:58,440
it you should have created a gar project

111
00:04:55,259 --> 00:05:01,259
file as well as a .rep folder

112
00:04:58,440 --> 00:05:03,120
containing all the project files as a

113
00:05:01,259 --> 00:05:04,919
reminder we are going to need to open

114
00:05:03,120 --> 00:05:08,699
the different kernel binaries and

115
00:05:04,919 --> 00:05:11,040
drivers into the same code browser so we

116
00:05:08,699 --> 00:05:13,740
can use ret-sync correctly as it was

117
00:05:11,040 --> 00:05:16,199
also explained in the dbg 3011 course

118
00:05:13,740 --> 00:05:18,600
we'll use the .rep folder

119
00:05:16,199 --> 00:05:20,639
associated with the imported guidra

120
00:05:18,600 --> 00:05:22,560
project when we are going to need to

121
00:05:20,639 --> 00:05:24,600
copy a ret-sync configuration file in

122
00:05:22,560 --> 00:05:28,020
that folder the ret-sync configuration

123
00:05:24,600 --> 00:05:30,660
file sets up aliases for the dlls and

124
00:05:28,020 --> 00:05:33,720
executables so the synchronization with

125
00:05:30,660 --> 00:05:35,340
the WinDbg debugger works properly and

126
00:05:33,720 --> 00:05:38,220
this is basically the environment we are

127
00:05:35,340 --> 00:05:40,500
going to be using on the left we we have

128
00:05:38,220 --> 00:05:43,020
the Ghidra disassembler we can see the

129
00:05:40,500 --> 00:05:44,880
disassembler window with assembly

130
00:05:43,020 --> 00:05:47,600
instruction

131
00:05:44,880 --> 00:05:50,699
and the decompiler window with actual

132
00:05:47,600 --> 00:05:52,860
decompiled C code we see the ret-sync

133
00:05:50,699 --> 00:05:54,000
plugin is started and the configuration

134
00:05:52,860 --> 00:05:55,560
file

135
00:05:54,000 --> 00:05:57,900
was found

136
00:05:55,560 --> 00:06:00,660
on the right we have the WinDbg

137
00:05:57,900 --> 00:06:03,300
debugger we can see the ret-sync plugin

138
00:06:00,660 --> 00:06:05,580
was also loaded and the !sync

139
00:06:03,300 --> 00:06:07,560
command was used to synchronize with the

140
00:06:05,580 --> 00:06:10,620
Ghidra disassembler we can see both

141
00:06:07,560 --> 00:06:12,360
WinDbg and Ghidra are pointing to the

142
00:06:10,620 --> 00:06:14,039
same piece of code thanks to the

143
00:06:12,360 --> 00:06:18,680
ret-sync plugin

144
00:06:14,039 --> 00:06:18,680
in TmRecoverResourceManagerExt

145
00:06:18,720 --> 00:06:22,639
at offset 8C

146
00:06:23,039 --> 00:06:27,360
in order to confirm ret-sync works

147
00:06:25,319 --> 00:06:29,340
properly in your environment you can set

148
00:06:27,360 --> 00:06:32,280
a breakpoint in WinDbg on a function

149
00:06:29,340 --> 00:06:34,740
like NT read file and when the

150
00:06:32,280 --> 00:06:36,780
breakpoint hits it should synchronize

151
00:06:34,740 --> 00:06:38,400
with Ghidra also you should be able to

152
00:06:36,780 --> 00:06:40,680
step in WinDbg and it should

153
00:06:38,400 --> 00:06:43,560
synchronize with Ghidra we provide the

154
00:06:40,680 --> 00:06:45,060
labs in the tools Labs folder you're

155
00:06:43,560 --> 00:06:46,340
going to need to extract the visual

156
00:06:45,060 --> 00:06:48,600
studio

157
00:06:46,340 --> 00:06:51,360
labs.zip archive and then run the

158
00:06:48,600 --> 00:06:53,100
build.bat script once in order to

159
00:06:51,360 --> 00:06:55,319
generate the Visual Studio solution and

160
00:06:53,100 --> 00:06:57,960
projects this is because we rely on

161
00:06:55,319 --> 00:06:59,639
cmake to generate them after that you'll

162
00:06:57,960 --> 00:07:01,560
be able to open the solution and

163
00:06:59,639 --> 00:07:03,900
directly build the project from Visual

164
00:07:01,560 --> 00:07:08,580
Studio later we will use the additional

165
00:07:03,900 --> 00:07:10,740
zip Labs named part 2 part 3.zip but you

166
00:07:08,580 --> 00:07:12,419
should not use them at the moment to

167
00:07:10,740 --> 00:07:14,759
check Visual Studio and SSH work

168
00:07:12,419 --> 00:07:16,860
properly to push the compiled binary

169
00:07:14,759 --> 00:07:19,979
from the debugger VM onto the target VM

170
00:07:16,860 --> 00:07:22,440
you can learn the build that hello

171
00:07:19,979 --> 00:07:24,419
command or build it directly from the

172
00:07:22,440 --> 00:07:25,919
Visual Studio project then you should be

173
00:07:24,419 --> 00:07:28,259
able to run the hello world accessible

174
00:07:25,919 --> 00:07:31,020
from the target VM this is what it

175
00:07:28,259 --> 00:07:32,520
should look like on your debugger VM you

176
00:07:31,020 --> 00:07:35,639
can see the different Labs on the left

177
00:07:32,520 --> 00:07:37,080
and when you compile a given project you

178
00:07:35,639 --> 00:07:39,979
should be able to see in the output

179
00:07:37,080 --> 00:07:42,360
window that it copies the file over SSH

180
00:07:39,979 --> 00:07:44,639
on the target VM

181
00:07:42,360 --> 00:07:46,500
this is the final checklist that you

182
00:07:44,639 --> 00:07:48,539
need to go over to be pretty much ready

183
00:07:46,500 --> 00:07:50,880
for the entire course so we want to make

184
00:07:48,539 --> 00:07:52,860
sure WinDbg is configured properly and

185
00:07:50,880 --> 00:07:55,080
that the symbols are loaded and we can

186
00:07:52,860 --> 00:07:57,300
print a function from the tm.sys driver

187
00:07:55,080 --> 00:07:59,880
we want to make sure the Ghidra project

188
00:07:57,300 --> 00:08:02,099
is imported and that the ret-sync plugin

189
00:07:59,880 --> 00:08:04,740
is configured properly to allow

190
00:08:02,099 --> 00:08:06,419
synchronization between WinDbg and Ghidra

191
00:08:04,740 --> 00:08:09,000
and finally we want to make sure the

192
00:08:06,419 --> 00:08:11,099
Visual Studio solution and PuTTY are

193
00:08:09,000 --> 00:08:13,919
configured properly to compile a project

194
00:08:11,099 --> 00:08:18,120
and push the compiled binary onto the

195
00:08:13,919 --> 00:08:21,360
target VM so from the dbg 3011 course

196
00:08:18,120 --> 00:08:22,860
you should have two virtual machines one

197
00:08:21,360 --> 00:08:25,319
target VM

198
00:08:22,860 --> 00:08:28,440
and one debugger VM

199
00:08:25,319 --> 00:08:31,319
if you do still have some files from the

200
00:08:28,440 --> 00:08:34,200
dbg 3011 course we're just going to put

201
00:08:31,319 --> 00:08:35,700
them into a subfolder as we won't need

202
00:08:34,200 --> 00:08:40,700
them and we're going to replace them

203
00:08:35,700 --> 00:08:40,700
with the actual files from this training

204
00:08:47,100 --> 00:08:53,519
the only folders that we gonna keep in

205
00:08:51,300 --> 00:08:56,160
these tools is Ghidra which we're going

206
00:08:53,519 --> 00:08:59,160
to use Gradle

207
00:08:56,160 --> 00:09:01,920
ret-sync installation and the

208
00:08:59,160 --> 00:09:04,680
SysInternals all the other things are related

209
00:09:01,920 --> 00:09:12,000
to the hello word

210
00:09:04,680 --> 00:09:12,000
projects as well as the dbg 3011 course

211
00:09:15,180 --> 00:09:20,700
so you should have just this four

212
00:09:17,339 --> 00:09:23,279
folders in your tools folder so now

213
00:09:20,700 --> 00:09:27,560
let's copy all the tools from the

214
00:09:23,279 --> 00:09:27,560
exploits 4011 course

215
00:09:28,680 --> 00:09:35,820
so we can see we have

216
00:09:31,380 --> 00:09:38,000
some Labs into zip files

217
00:09:35,820 --> 00:09:44,300
we have some patch diffing

218
00:09:38,000 --> 00:09:44,300
files in C we have some references

219
00:09:44,519 --> 00:09:52,380
we have some files that are the versions

220
00:09:49,339 --> 00:09:55,519
for the one we're going to target so we

221
00:09:52,380 --> 00:09:55,519
can reverse engineer them

222
00:09:55,800 --> 00:10:02,519
and we have the Ghidra project and some

223
00:09:59,040 --> 00:10:05,279
win WinDbg scripts that we can use to

224
00:10:02,519 --> 00:10:06,360
automate debugging as well as some cheat

225
00:10:05,279 --> 00:10:08,519
sheets

226
00:10:06,360 --> 00:10:10,800
so the first thing we want to check is

227
00:10:08,519 --> 00:10:13,800
that we can actually debug from the

228
00:10:10,800 --> 00:10:16,860
debugger VM the actual target VM so if

229
00:10:13,800 --> 00:10:20,279
you have configured it correctly using

230
00:10:16,860 --> 00:10:22,500
the dbg 3011 course and you have booted

231
00:10:20,279 --> 00:10:25,080
the target VM you should be able to use

232
00:10:22,500 --> 00:10:27,680
the batch script to actually debug the

233
00:10:25,080 --> 00:10:27,680
target VM

234
00:10:28,140 --> 00:10:31,220
so let's start it

235
00:10:39,660 --> 00:10:44,660
as you can see it's connected to the

236
00:10:42,120 --> 00:10:44,660
targets

237
00:10:47,880 --> 00:10:55,399
and it's working the next thing is to

238
00:10:50,339 --> 00:10:55,399
check the version of the tm.sys module

239
00:11:13,500 --> 00:11:19,079
so we can write a note of this

240
00:11:16,339 --> 00:11:21,240
tm.sys file

241
00:11:19,079 --> 00:11:24,920
so we know later after we've patched it

242
00:11:21,240 --> 00:11:24,920
that it actually changed

243
00:11:27,959 --> 00:11:31,279
so let's detach from it

244
00:11:40,079 --> 00:11:45,120
so the next step is to replace the

245
00:11:42,800 --> 00:11:46,620
tm.sys kernel module with the actual

246
00:11:45,120 --> 00:11:49,200
reliable one

247
00:11:46,620 --> 00:11:52,019
so if you go into tools reverse you'll

248
00:11:49,200 --> 00:11:53,940
see the TM_vuln.sys

249
00:11:52,019 --> 00:11:57,360
which we're going to need to copy

250
00:11:53,940 --> 00:12:00,500
outside of the virtual machine and paste

251
00:11:57,360 --> 00:12:00,500
into our target VM

252
00:12:04,800 --> 00:12:10,040
so for now it's on the desktop on the

253
00:12:07,380 --> 00:12:10,040
target VM

254
00:12:10,140 --> 00:12:16,459
so we're going to need to go into

255
00:12:11,820 --> 00:12:16,459
Windows system 32 drivers

256
00:12:26,160 --> 00:12:30,060
and here we should be able to find our

257
00:12:28,079 --> 00:12:32,220
tm.sys

258
00:12:30,060 --> 00:12:36,560
this is the old version

259
00:12:32,220 --> 00:12:36,560
well actually this is the recent version

260
00:12:38,940 --> 00:12:42,839
and we want to actually replace that

261
00:12:41,040 --> 00:12:45,000
with an actual older version that is

262
00:12:42,839 --> 00:12:48,779
vulnerable to the bug

263
00:12:45,000 --> 00:12:50,820
so if you do try to replay that

264
00:12:48,779 --> 00:12:53,600
and rename it it will tell you you don't

265
00:12:50,820 --> 00:12:53,600
have the permissions

266
00:12:56,820 --> 00:13:03,320
because we are not owning that file so

267
00:13:00,540 --> 00:13:03,320
we can't do it

268
00:13:04,980 --> 00:13:09,380
so in order to do so you can follow the

269
00:13:06,959 --> 00:13:09,380
tutorial

270
00:13:10,500 --> 00:13:14,399
from here

271
00:13:12,420 --> 00:13:17,519
which is basically to replace the owner

272
00:13:14,399 --> 00:13:21,380
of the file and change it from trusted

273
00:13:17,519 --> 00:13:21,380
installer to our own user

274
00:13:21,660 --> 00:13:26,519
so basically if you right click on

275
00:13:23,639 --> 00:13:30,440
tm.sys properties

276
00:13:26,519 --> 00:13:30,440
and then go into the security tab

277
00:13:31,019 --> 00:13:34,579
then you go into advanced

278
00:13:35,880 --> 00:13:40,260
and then in the owner you can see it's

279
00:13:38,519 --> 00:13:42,660
trusted installer

280
00:13:40,260 --> 00:13:45,899
so you can change that

281
00:13:42,660 --> 00:13:49,220
here you can look for IE user

282
00:13:45,899 --> 00:13:49,220
which is our username

283
00:13:50,220 --> 00:13:56,639
then click ok

284
00:13:53,160 --> 00:13:59,040
you can see it's been changed now

285
00:13:56,639 --> 00:14:00,779
so you need to apply

286
00:13:59,040 --> 00:14:03,180
it tells you you need to actually close

287
00:14:00,779 --> 00:14:04,560
and reopen the objects property before

288
00:14:03,180 --> 00:14:06,240
you can see

289
00:14:04,560 --> 00:14:09,200
the changes

290
00:14:06,240 --> 00:14:09,200
so we click ok

291
00:14:10,320 --> 00:14:16,800
and now we go back to properties

292
00:14:14,639 --> 00:14:18,420
security

293
00:14:16,800 --> 00:14:22,700
advanced

294
00:14:18,420 --> 00:14:22,700
and we can see we are the IE user

295
00:14:24,120 --> 00:14:31,380
so if you go into the users here

296
00:14:27,959 --> 00:14:33,720
we can edit the permission

297
00:14:31,380 --> 00:14:35,700
for the users and just allow full

298
00:14:33,720 --> 00:14:36,899
control

299
00:14:35,700 --> 00:14:38,399
apply

300
00:14:36,899 --> 00:14:40,980
just make sure you're doing it for

301
00:14:38,399 --> 00:14:43,760
tm.sys

302
00:14:40,980 --> 00:14:43,760
and apply

303
00:14:44,399 --> 00:14:51,199
now that you have done that you should be able

304
00:14:46,440 --> 00:14:51,199
to rename the tm.sys to an old name

305
00:14:55,579 --> 00:15:03,680
and now we should be able to

306
00:14:59,720 --> 00:15:03,680
copy the old

307
00:15:03,839 --> 00:15:08,760
file

308
00:15:06,060 --> 00:15:11,760
make sure to rename it

309
00:15:08,760 --> 00:15:11,760
tm.sys

310
00:15:12,720 --> 00:15:18,680
so you should have the tm.sys

311
00:15:15,360 --> 00:15:18,680
from 2021

312
00:15:18,720 --> 00:15:25,440
and the old one being renamed from 2022

313
00:15:21,660 --> 00:15:28,040
or above okay now we should restart the

314
00:15:25,440 --> 00:15:28,040
virtual machine

315
00:15:31,800 --> 00:15:36,600
when the target VM reboots it should

316
00:15:33,959 --> 00:15:40,139
hang during reboot because of the

317
00:15:36,600 --> 00:15:43,380
debugger being not attached yet

318
00:15:40,139 --> 00:15:45,480
so we can see on our debugger VM we only

319
00:15:43,380 --> 00:15:49,139
have one TM.sys

320
00:15:45,480 --> 00:15:52,320
 symbols downloaded for the pdb so

321
00:15:49,139 --> 00:15:55,579
we are going to start the debugger and

322
00:15:52,320 --> 00:15:55,579
attach to the target VM

323
00:15:59,279 --> 00:16:04,040
on the target VM side we can see it

324
00:16:01,560 --> 00:16:04,040
hangs

325
00:16:04,740 --> 00:16:09,839
and on the debugger VM we can see it's

326
00:16:07,500 --> 00:16:13,040
not connected yet

327
00:16:09,839 --> 00:16:13,040
now it's connected

328
00:16:17,220 --> 00:16:21,839
so we see we don't have additional

329
00:16:18,839 --> 00:16:25,100
symbols yet if we break

330
00:16:21,839 --> 00:16:25,100
and do a Reload

331
00:16:29,820 --> 00:16:34,139
where you can see now that it's

332
00:16:31,440 --> 00:16:36,060
downloading a second tm.sys

333
00:16:34,139 --> 00:16:39,740
because it's actually using the

334
00:16:36,060 --> 00:16:39,740
vulnerable version now

335
00:16:44,699 --> 00:16:48,259
and we can continue execution

336
00:16:49,560 --> 00:16:54,779
to let the target VM boot

337
00:16:52,500 --> 00:16:56,160
so anyway dbg you should be able to

338
00:16:54,779 --> 00:16:59,040
break

339
00:16:56,160 --> 00:17:02,579
and then start disassembling

340
00:16:59,040 --> 00:17:04,199
a function from tm.sys that confirms

341
00:17:02,579 --> 00:17:05,520
that you have the symbols downloaded

342
00:17:04,199 --> 00:17:07,860
correctly

343
00:17:05,520 --> 00:17:10,620
so while you are attached with the

344
00:17:07,860 --> 00:17:12,900
debugger VM to the target VM

345
00:17:10,620 --> 00:17:15,120
you may want to do a snapshot of the

346
00:17:12,900 --> 00:17:19,620
actual

347
00:17:15,120 --> 00:17:21,740
target VM in that state so you do Ctrl m

348
00:17:19,620 --> 00:17:28,699
and then

349
00:17:21,740 --> 00:17:28,699
tm.sys replaced plus booted with debugger

350
00:17:36,240 --> 00:17:42,120
now basically if you look at the actual

351
00:17:39,660 --> 00:17:45,660
files in the reverse folder in tools

352
00:17:42,120 --> 00:17:48,539
reverse you'll see the npfs.sys and ntdll.dll

353
00:17:45,660 --> 00:17:50,220
and ntoskrnl.exe on top of the of the

354
00:17:48,539 --> 00:17:51,960
tm.sys

355
00:17:50,220 --> 00:17:54,299
so we want to check that we have this

356
00:17:51,960 --> 00:17:55,380
version as well installed in the target

357
00:17:54,299 --> 00:17:57,900
VM

358
00:17:55,380 --> 00:17:59,520
so to install the additional files on

359
00:17:57,900 --> 00:18:00,900
the target VA we need to copy the

360
00:17:59,520 --> 00:18:02,700
reverse folder

361
00:18:00,900 --> 00:18:05,820
on the target VM

362
00:18:02,700 --> 00:18:07,020
for instance if we look at the ntdll.dll

363
00:18:05,820 --> 00:18:09,960
file

364
00:18:07,020 --> 00:18:13,260
we see in properties detail and we check

365
00:18:09,960 --> 00:18:15,539
the one in C Windows system 32.

366
00:18:13,260 --> 00:18:16,620
we can see that the versions are

367
00:18:15,539 --> 00:18:18,360
different

368
00:18:16,620 --> 00:18:21,840
so we have to replicate the same

369
00:18:18,360 --> 00:18:22,860
technique for all the dlls and kernel

370
00:18:21,840 --> 00:18:24,780
modules

371
00:18:22,860 --> 00:18:28,140
so you can see that I have successfully

372
00:18:24,780 --> 00:18:31,380
replaced ntdll.dll with the old versions

373
00:18:28,140 --> 00:18:33,840
I have replaced ntos krnl.exe

374
00:18:31,380 --> 00:18:37,980
with the old versions

375
00:18:33,840 --> 00:18:41,820
if I go into system 32 drivers

376
00:18:37,980 --> 00:18:41,820
and I look for npfs.sys

377
00:18:41,940 --> 00:18:47,580
you'll see that in my case

378
00:18:44,340 --> 00:18:50,340
the actual details for this version

379
00:18:47,580 --> 00:18:52,320
actually match the one

380
00:18:50,340 --> 00:18:55,500
from here so I didn't actually replace

381
00:18:52,320 --> 00:18:57,419
it because it's the same version so that

382
00:18:55,500 --> 00:18:59,760
it was not needed

383
00:18:57,419 --> 00:19:01,980
that I have replaced all the files I'm

384
00:18:59,760 --> 00:19:04,220
gonna actually restart the virtual

385
00:19:01,980 --> 00:19:04,220
machine

386
00:19:07,799 --> 00:19:14,100
so now the virtual machine is rebooting

387
00:19:11,460 --> 00:19:16,140
we can see on the debugger side that it

388
00:19:14,100 --> 00:19:18,500
automatically reattached after the

389
00:19:16,140 --> 00:19:18,500
shutdown

390
00:19:30,299 --> 00:19:34,160
and we're going to continue execution

391
00:19:44,160 --> 00:19:48,200
and the target VM is booting

392
00:20:02,160 --> 00:20:06,440
now if you're going to C

393
00:20:06,660 --> 00:20:11,419
symbols and ntoskrnl

394
00:20:12,660 --> 00:20:15,980
or ntdll

395
00:20:17,880 --> 00:20:21,740
you see you see there are two now

396
00:20:24,900 --> 00:20:28,440
so now all the files have been replaced

397
00:20:26,760 --> 00:20:31,140
on the target VM

398
00:20:28,440 --> 00:20:33,419
so we need to do a snapshot of the

399
00:20:31,140 --> 00:20:35,340
virtual machine if you do see a message

400
00:20:33,419 --> 00:20:37,500
like this it seems it can just be

401
00:20:35,340 --> 00:20:40,140
ignored and it's probably related to the

402
00:20:37,500 --> 00:20:43,380
fact that we've changed the versions of

403
00:20:40,140 --> 00:20:45,660
the dlls and executables

404
00:20:43,380 --> 00:20:48,660
as long as we don't use these

405
00:20:45,660 --> 00:20:52,640
executables it's fine

406
00:20:48,660 --> 00:20:52,640
and we can use the virtual machine

407
00:20:55,620 --> 00:20:59,820
so let's shut down the switch one

408
00:20:57,600 --> 00:21:02,880
machine

409
00:20:59,820 --> 00:21:04,860
and before doing that

410
00:21:02,880 --> 00:21:06,480
we're going to do a snapshot of the

411
00:21:04,860 --> 00:21:10,640
booted VM

412
00:21:06,480 --> 00:21:10,640
so the debugger is attached

413
00:21:11,760 --> 00:21:17,640
and we're going to basically just snatch

414
00:21:14,640 --> 00:21:20,660
up Snapshot that virtual machine

415
00:21:17,640 --> 00:21:20,660
with Ctrl m

416
00:21:21,419 --> 00:21:29,900
all dll exe replaced + booted

417
00:21:27,179 --> 00:21:29,900
debugger

418
00:21:34,440 --> 00:21:40,980
now we want to import the Ghidra projects

419
00:21:37,380 --> 00:21:43,440
with the gar extension

420
00:21:40,980 --> 00:21:46,940
so we're going to start Ghidra

421
00:21:43,440 --> 00:21:46,940
with the ghidraRun.bat

422
00:21:52,799 --> 00:21:56,520
make sure no random project is actually

423
00:21:55,200 --> 00:21:58,919
loaded

424
00:21:56,520 --> 00:22:01,820
then go into

425
00:21:58,919 --> 00:22:01,820
restore project

426
00:22:02,280 --> 00:22:09,480
then look for the actual

427
00:22:05,280 --> 00:22:09,480
CVE 2018 8611.gar

428
00:22:14,220 --> 00:22:17,659
you should import that project

429
00:22:26,220 --> 00:22:32,280
once you've done that

430
00:22:28,740 --> 00:22:33,600
you should have on top of the actual gar

431
00:22:32,280 --> 00:22:38,580
folder

432
00:22:33,600 --> 00:22:41,039
you should have an actual GPR file as

433
00:22:38,580 --> 00:22:44,960
well as a  .rep

434
00:22:41,039 --> 00:22:44,960
with all the actual project information

435
00:22:45,120 --> 00:22:50,039
in the actual Ghidra project

436
00:22:47,220 --> 00:22:52,620
you should see the different labs

437
00:22:50,039 --> 00:22:55,020
the different files

438
00:22:52,620 --> 00:22:58,440
that we can load into Ghidra for instance

439
00:22:55,020 --> 00:23:02,419
we can double click on tm_vuln.sys

440
00:22:58,440 --> 00:23:02,419
to open it into the code browser

441
00:23:10,620 --> 00:23:16,140
before you can actually use ret-sync to

442
00:23:13,140 --> 00:23:20,039
synchronize WinDbg with Ghidra you should

443
00:23:16,140 --> 00:23:25,200
actually copy the ret-sync config file

444
00:23:20,039 --> 00:23:27,600
into the new project we've just imported

445
00:23:25,200 --> 00:23:30,720
so you can see in this .rep folder

446
00:23:27,600 --> 00:23:32,960
there is no .sync file so if you go into

447
00:23:30,720 --> 00:23:36,600
Ghidra project

448
00:23:32,960 --> 00:23:38,100
folder.rep and you look into the

449
00:23:36,600 --> 00:23:39,960
.sync file

450
00:23:38,100 --> 00:23:42,000
if you have the right one you should see

451
00:23:39,960 --> 00:23:43,200
that there is a match for different

452
00:23:42,000 --> 00:23:45,620
files

453
00:23:43,200 --> 00:23:49,620
from Ntkrnl

454
00:23:45,620 --> 00:23:51,840
npfs.sys and tm.sys

455
00:23:49,620 --> 00:23:54,860
so you're going to want to actually copy

456
00:23:51,840 --> 00:23:54,860
that .sync file

457
00:23:55,440 --> 00:24:04,340
into the actual .rep for the actual

458
00:23:59,220 --> 00:24:04,340
CVE-2018-8611.

459
00:24:05,340 --> 00:24:10,080
now that you've done that

460
00:24:07,679 --> 00:24:12,539
you should be able to start Ghidra so

461
00:24:10,080 --> 00:24:16,400
we're going to Ghidra

462
00:24:12,539 --> 00:24:16,400
and run ghidraRun.bat,

463
00:24:23,220 --> 00:24:30,260
now we can load our tm_vuln.sys

464
00:24:27,179 --> 00:24:30,260
into the code browser

465
00:24:34,620 --> 00:24:39,360
as you can see the configuration file

466
00:24:37,140 --> 00:24:43,100
was loaded

467
00:24:39,360 --> 00:24:43,100
and the aliases were found

468
00:24:43,200 --> 00:24:49,740
we're going to also load the

469
00:24:45,900 --> 00:24:49,740
ntoskernel.exe

470
00:24:52,020 --> 00:24:56,960
now we're going to start the ret-sync

471
00:24:53,460 --> 00:24:56,960
plugin with ALT S

472
00:24:59,520 --> 00:25:03,659
in WinDbg

473
00:25:01,260 --> 00:25:06,600
you should be able to just break and

474
00:25:03,659 --> 00:25:09,200
then load the ret-sync plugin

475
00:25:06,600 --> 00:25:09,200
as well

476
00:25:13,320 --> 00:25:17,039
then using the !sync command you

477
00:25:15,720 --> 00:25:20,659
should be able to sync with the actual

478
00:25:17,039 --> 00:25:20,659
ret-sync plugin in Ghidra

479
00:25:21,659 --> 00:25:25,919
as you can see

480
00:25:23,400 --> 00:25:28,559
we are synced with the right function

481
00:25:25,919 --> 00:25:31,679
which indicates that the right

482
00:25:28,559 --> 00:25:33,779
executable is in the target VM and it

483
00:25:31,679 --> 00:25:37,460
matches the one from the actual Ghidra

484
00:25:33,779 --> 00:25:37,460
project that we have already imported

485
00:25:39,539 --> 00:25:45,799
we can also set a breakpoint on one

486
00:25:41,760 --> 00:25:45,799
function like NtReadFile

487
00:25:48,779 --> 00:25:53,340
and continue execution

488
00:25:51,059 --> 00:25:56,580
when it hits

489
00:25:53,340 --> 00:25:59,360
you can confirm it actually also matches

490
00:25:56,580 --> 00:25:59,360
the function

491
00:25:59,640 --> 00:26:04,919
now we're going to check that we can

492
00:26:01,320 --> 00:26:09,059
actually build our Labs with visual

493
00:26:04,919 --> 00:26:11,279
studio and push them onto the target VM

494
00:26:09,059 --> 00:26:13,620
so if we look at the actual Visual

495
00:26:11,279 --> 00:26:16,380
Studio Labs archive we see it has a

496
00:26:13,620 --> 00:26:18,919
folder inside so we extract that folder

497
00:26:16,380 --> 00:26:18,919
here

498
00:26:20,220 --> 00:26:27,559
and we can just

499
00:26:22,980 --> 00:26:27,559
run build.bat so to do so

500
00:26:29,400 --> 00:26:35,580
we open our CMD

501
00:26:32,940 --> 00:26:37,559
go into this folder before we actually

502
00:26:35,580 --> 00:26:40,919
run the build.bat

503
00:26:37,559 --> 00:26:44,580
we have to remember that it relies on

504
00:26:40,919 --> 00:26:46,559
SCP to copy files onto the target VM and

505
00:26:44,580 --> 00:26:48,860
an env.bat

506
00:26:46,559 --> 00:26:52,679
so you have to make sure the IP address

507
00:26:48,860 --> 00:26:56,179
in SCP remote IP for both actually

508
00:26:52,679 --> 00:26:56,179
matches the target VM

509
00:26:58,020 --> 00:27:04,500
so here you can see it ends with 92.131

510
00:27:01,980 --> 00:27:05,460
this corresponds to the actual IP

511
00:27:04,500 --> 00:27:07,440
address

512
00:27:05,460 --> 00:27:10,200
here

513
00:27:07,440 --> 00:27:12,419
on top of that you need to remember that

514
00:27:10,200 --> 00:27:14,880
a profile need to exist with the actual

515
00:27:12,419 --> 00:27:17,900
IP address and you need to be able to

516
00:27:14,880 --> 00:27:17,900
connect to it

517
00:27:18,120 --> 00:27:21,900
as you can see here

518
00:27:19,860 --> 00:27:23,340
it doesn't work because SSH is not

519
00:27:21,900 --> 00:27:26,480
enabled

520
00:27:23,340 --> 00:27:26,480
so we're going to go here

521
00:27:28,020 --> 00:27:31,100
and start SSH

522
00:27:38,460 --> 00:27:41,960
now if we restart the session

523
00:27:42,720 --> 00:27:47,700
we see SSH works

524
00:27:45,240 --> 00:27:49,200
the IP address matches the profile in

525
00:27:47,700 --> 00:27:53,520
PuTTY

526
00:27:49,200 --> 00:27:56,220
so now we can go on the CMD

527
00:27:53,520 --> 00:27:58,940
and start our build.bat

528
00:27:56,220 --> 00:27:58,940
with hello

529
00:28:10,380 --> 00:28:15,179
as you can see

530
00:28:12,539 --> 00:28:20,059
the build succeeded and the files were

531
00:28:15,179 --> 00:28:20,059
was copied onto the actual target VM

532
00:28:20,820 --> 00:28:24,080
and it worked

533
00:28:24,240 --> 00:28:28,919
so now what we can do is we can go into

534
00:28:26,340 --> 00:28:30,360
Visual Studio labs I'm going to the

535
00:28:28,919 --> 00:28:33,799
build folder

536
00:28:30,360 --> 00:28:33,799
and we can start the solution

537
00:28:40,919 --> 00:28:46,320
as you can see the hello world is there

538
00:28:43,919 --> 00:28:49,679
and if you want to rebuild

539
00:28:46,320 --> 00:28:52,260
the actual solution from Visual Studio

540
00:28:49,679 --> 00:28:56,059
it will be possible so you don't have to

541
00:28:52,260 --> 00:28:56,059
use the build.bat script anymore

542
00:29:05,779 --> 00:29:11,460
so now we have everything

543
00:29:08,400 --> 00:29:15,299
we have WinDbg to debug

544
00:29:11,460 --> 00:29:19,200
we have Ghidra to reverse engineer and we

545
00:29:15,299 --> 00:29:22,220
have Visual Studio to build our exploits

546
00:29:19,200 --> 00:29:22,220
thank you for watching

