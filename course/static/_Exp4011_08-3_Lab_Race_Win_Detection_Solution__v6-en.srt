1
00:00:00,000 --> 00:00:04,560
so the first thing we do is we actually

2
00:00:02,280 --> 00:00:06,299
execute the binary and modified and to

3
00:00:04,560 --> 00:00:10,280
see what we get so we can see it's

4
00:00:06,299 --> 00:00:10,280
actually draining the named pipes

5
00:00:12,480 --> 00:00:18,480
okay so we can see we have some goals

6
00:00:15,839 --> 00:00:21,180
that are printed so we need to follow

7
00:00:18,480 --> 00:00:23,220
these goals and it's telling us now that

8
00:00:21,180 --> 00:00:25,439
we've reached the state where we can set

9
00:00:23,220 --> 00:00:27,840
breakpoint in the debugger in order to

10
00:00:25,439 --> 00:00:32,240
block the recovery thread

11
00:00:27,840 --> 00:00:32,240
so I'm going to break in the debugger

12
00:00:32,820 --> 00:00:37,559
and now I'm going to enable the two

13
00:00:35,520 --> 00:00:39,360
first breakpoint which are at the

14
00:00:37,559 --> 00:00:42,239
beginning of the vulnerable function and

15
00:00:39,360 --> 00:00:44,520
when it's actually calling the function

16
00:00:42,239 --> 00:00:46,559
to send a notification and then we can

17
00:00:44,520 --> 00:00:48,780
enable the breakpoint afterwards because

18
00:00:46,559 --> 00:00:50,399
it's one breakpoint that is after we

19
00:00:48,780 --> 00:00:51,960
block the recovery thread and it may

20
00:00:50,399 --> 00:00:54,620
mess up with the memory if we set the

21
00:00:51,960 --> 00:00:54,620
breakpoint now

22
00:00:57,059 --> 00:01:02,640
um it's also telling us to continue

23
00:00:59,940 --> 00:01:05,960
execution so now we can see after

24
00:01:02,640 --> 00:01:08,820
hitting enter that the VM hangs

25
00:01:05,960 --> 00:01:10,320
so I'm going to go back we we're in

26
00:01:08,820 --> 00:01:12,119
vunerable function so the recovery

27
00:01:10,320 --> 00:01:15,560
thread has reached the vulnerable function

28
00:01:12,119 --> 00:01:15,560
so we'll continue execution

29
00:01:15,840 --> 00:01:20,880
and now we're hitting the the call to

30
00:01:18,900 --> 00:01:24,540
the CMP certification resource manager

31
00:01:20,880 --> 00:01:26,939
so now we want to skip at least 32 of

32
00:01:24,540 --> 00:01:30,200
them so I'm just gonna

33
00:01:26,939 --> 00:01:30,200
hit go many times

34
00:01:32,040 --> 00:01:35,780
a few moments later

35
00:01:37,860 --> 00:01:41,880
okay so now I'm gonna

36
00:01:39,659 --> 00:01:45,380
patch the memory so actually the

37
00:01:41,880 --> 00:01:45,380
recovery thread gets stuck

38
00:01:47,340 --> 00:01:50,600
and I'm continuing execution

39
00:01:51,360 --> 00:01:55,979
so we can see the memory has been

40
00:01:53,640 --> 00:01:59,220
patched and now if we go back to the

41
00:01:55,979 --> 00:02:01,020
target VM make sure the recovery

42
00:01:59,220 --> 00:02:02,939
strategy is stacked which is the case so

43
00:02:01,020 --> 00:02:05,820
we can continue execution

44
00:02:02,939 --> 00:02:09,720
okay so we can confirm that 34

45
00:02:05,820 --> 00:02:12,420
enlistments which is 22 hex have been

46
00:02:09,720 --> 00:02:15,420
touched which is more than 32 so that's

47
00:02:12,420 --> 00:02:17,459
good so we it's telling us to unpatch

48
00:02:15,420 --> 00:02:20,459
the memory to unblock the recovery

49
00:02:17,459 --> 00:02:23,540
thread so I'm just gonna go

50
00:02:20,459 --> 00:02:23,540
and unpatch it

51
00:02:25,440 --> 00:02:29,520
and after unpatching it I'm going to set

52
00:02:27,540 --> 00:02:32,280
the breakpoints on the actual

53
00:02:29,520 --> 00:02:34,319
instruction after the recovery thread

54
00:02:32,280 --> 00:02:37,319
thread is restored

55
00:02:34,319 --> 00:02:37,319
and I don't need these 2 breakpoints

56
00:02:37,680 --> 00:02:40,680
points

57
00:02:43,200 --> 00:02:47,060
so I'm continuing execution now

58
00:02:47,099 --> 00:02:51,599
we can see it has returned from the

59
00:02:49,440 --> 00:02:54,480
blocking state so we're going to step

60
00:02:51,599 --> 00:02:58,459
over ????? is the actual resource manager

61
00:02:54,480 --> 00:02:58,459
is testing the chat the state here

62
00:03:09,180 --> 00:03:13,560
so now if we look where we are

63
00:03:11,599 --> 00:03:15,540
we are

64
00:03:13,560 --> 00:03:18,420
here

65
00:03:15,540 --> 00:03:20,879
so the decompiler hasn't been updated

66
00:03:18,420 --> 00:03:23,519
but basically it's in B next

67
00:03:20,879 --> 00:03:26,280
enlistment

68
00:03:23,519 --> 00:03:28,680
so it's actually trying to fetch the

69
00:03:26,280 --> 00:03:30,659
next mrm flame pointer from the shifted

70
00:03:28,680 --> 00:03:33,120
pointer according to the enlistment and

71
00:03:30,659 --> 00:03:36,920
updating this shifted pointer

72
00:03:33,120 --> 00:03:36,920
so if we look at our RDI

73
00:03:45,599 --> 00:03:50,819
so we can see the enlistment memory has

74
00:03:48,540 --> 00:03:53,280
been replaced by a named pipe chunk which

75
00:03:50,819 --> 00:03:54,980
is marked by the npfr tag so that's

76
00:03:53,280 --> 00:03:58,260
great

77
00:03:54,980 --> 00:04:01,440
and we know the offset or the

78
00:03:58,260 --> 00:04:03,299
?????? pointer is at offset 88

79
00:04:01,440 --> 00:04:05,280
because if we look at the kenlistment

80
00:04:03,299 --> 00:04:08,459
it's actually pointing to the next same

81
00:04:05,280 --> 00:04:13,340
RM field which is at offset 88.

82
00:04:08,459 --> 00:04:13,340
so here if we look at RDI

83
00:04:13,560 --> 00:04:17,699
we can see that unfortunately it's gonna

84
00:04:15,780 --> 00:04:21,780
get a null pointer so the offset is

85
00:04:17,699 --> 00:04:25,699
wrong but if we actually look at

86
00:04:21,780 --> 00:04:25,699
rdi- 88

87
00:04:28,320 --> 00:04:32,280
okay

88
00:04:30,000 --> 00:04:35,460
so what we can see is that

89
00:04:32,280 --> 00:04:38,340
what should happen is that we want rdi

90
00:04:35,460 --> 00:04:42,000
-88 to be a valid kenlistment means and we

91
00:04:38,340 --> 00:04:44,580
want that at index 88 there is the next

92
00:04:42,000 --> 00:04:47,280
samerm flame pointer but as you can see the

93
00:04:44,580 --> 00:04:50,280
pointer that we've set is actually

94
00:04:47,280 --> 00:04:53,520
before so the offset is wrong so if

95
00:04:50,280 --> 00:04:55,820
we actually print that as a kenlistment

96
00:05:12,840 --> 00:05:20,300
you can see that at offset 88 actually

97
00:05:15,840 --> 00:05:20,300
we don't have the the right value

98
00:05:23,759 --> 00:05:28,500
so here it's at offset 78.

99
00:05:30,539 --> 00:05:33,620
so actually

100
00:05:36,199 --> 00:05:40,580
if we do a little bit of math

101
00:05:46,320 --> 00:05:50,660
so we want to subtract

102
00:05:52,620 --> 00:05:55,340
see where

103
00:06:22,759 --> 00:06:30,720
okay so basically at the moment the

104
00:06:27,539 --> 00:06:33,360
pointer that we set isat offset 68

105
00:06:30,720 --> 00:06:37,080
from the start of the kenlistment but we

106
00:06:33,360 --> 00:06:41,940
need it to be at offset 88 so we need to

107
00:06:37,080 --> 00:06:45,840
offset 20 hex so we want it to be +

108
00:06:41,940 --> 00:06:49,560
20 hex so it's actually at offset 88

109
00:06:45,840 --> 00:06:52,560
instead of 68. obviously here if we

110
00:06:49,560 --> 00:06:55,560
continue execution and we step over

111
00:06:52,560 --> 00:06:57,780
now rdi

112
00:06:55,560 --> 00:07:00,080
is null and so obviously it's going to

113
00:06:57,780 --> 00:07:00,080
crash

114
00:07:06,120 --> 00:07:12,660
so here it's retrieving a pointer and

115
00:07:09,900 --> 00:07:17,280
here are 14.

116
00:07:12,660 --> 00:07:20,840
is 0 - 88.

117
00:07:17,280 --> 00:07:20,840
so obviously it's gonna crash

118
00:07:22,919 --> 00:07:29,819
so let's modify the offset of the Trap

119
00:07:27,360 --> 00:07:31,860
enlistment when it's pointed from these

120
00:07:29,819 --> 00:07:34,740
spray enlistment so we know it's related

121
00:07:31,860 --> 00:07:36,660
to the kenlistment offset that is given to

122
00:07:34,740 --> 00:07:40,280
init_spray_enlistment  function because

123
00:07:36,660 --> 00:07:40,280
when we pass this offset here

124
00:07:41,220 --> 00:07:47,400
it's going to actually be used to figure

125
00:07:44,039 --> 00:07:49,919
out where the kenlistment starts inside

126
00:07:47,400 --> 00:07:52,020
the chunk and we know we need to add 20

127
00:07:49,919 --> 00:07:55,099
hex

128
00:07:52,020 --> 00:07:58,199
so we're just going to add 20x here

129
00:07:55,099 --> 00:08:00,979
and we're going to push the binary onto

130
00:07:58,199 --> 00:08:00,979
the target VM

131
00:08:05,280 --> 00:08:10,259
so we're executing the binary again and

132
00:08:07,919 --> 00:08:12,780
so here we want to check that the next

133
00:08:10,259 --> 00:08:15,319
samerm flink pointer retrieve is at the

134
00:08:12,780 --> 00:08:15,319
right offset

135
00:08:20,940 --> 00:08:25,280
so let's enable our breakpoints

136
00:08:30,360 --> 00:08:35,580
we continue execution

137
00:08:32,640 --> 00:08:38,399
it hangs

138
00:08:35,580 --> 00:08:41,039
and now we're gonna continue execution

139
00:08:38,399 --> 00:08:45,380
to hit the our second break points and

140
00:08:41,039 --> 00:08:45,380
I'm gonna skip 32 times at least

141
00:08:47,040 --> 00:08:54,019
a few moments later

142
00:08:50,160 --> 00:08:54,019
okay now we're gonna attach the memory

143
00:09:00,360 --> 00:09:03,620
and continue execution

144
00:09:06,660 --> 00:09:14,480
so now our recovery thread is stuck

145
00:09:10,459 --> 00:09:14,480
so we're going to continue execution

146
00:09:14,700 --> 00:09:19,860
we can see it's freed more than 32

147
00:09:17,580 --> 00:09:21,420
chunks which is good

148
00:09:19,860 --> 00:09:24,680
so now we can

149
00:09:21,420 --> 00:09:24,680
add patch

150
00:09:30,240 --> 00:09:36,180
and now we can enable

151
00:09:32,700 --> 00:09:38,459
our breakpoint posts

152
00:09:36,180 --> 00:09:41,940
being stuck okay

153
00:09:38,459 --> 00:09:44,959
so we know rdi holds the shifted pointer

154
00:09:41,940 --> 00:09:44,959
to the kenlistment

155
00:09:53,279 --> 00:09:58,580
okay and now if we look at what is

156
00:09:55,440 --> 00:09:58,580
pointed by rdi

157
00:09:59,700 --> 00:10:04,740
ah nice so this is our useland pointer

158
00:10:03,000 --> 00:10:07,620
as you can see because it starts with

159
00:10:04,740 --> 00:10:09,720
zeros so now if we actually print the

160
00:10:07,620 --> 00:10:12,440
kenlistment starting from rdi-88

161
00:10:22,380 --> 00:10:26,760
now you can see that the next samerm

162
00:10:24,420 --> 00:10:31,160
actually point our userland enlistment

163
00:10:26,760 --> 00:10:31,160
address so let's step over

164
00:10:36,240 --> 00:10:41,720
so we can use F10 to do it from Ghidra

165
00:10:39,480 --> 00:10:41,720
itself

166
00:10:51,660 --> 00:10:58,980
nice so now rdi has been retrieved

167
00:10:56,519 --> 00:11:00,240
so we can see that the kernel is

168
00:10:58,980 --> 00:11:02,640
manipulating

169
00:11:00,240 --> 00:11:04,920
what it thinks is a kenlistment but it's

170
00:11:02,640 --> 00:11:06,839
actually the userland address that we

171
00:11:04,920 --> 00:11:09,560
provided and so if we interpret that as

172
00:11:06,839 --> 00:11:09,560
a kenlistment

173
00:11:21,839 --> 00:11:29,899
well ????????? valid at the moment

174
00:11:25,680 --> 00:11:29,899
you can see none of the fields are set

175
00:11:30,300 --> 00:11:33,380
so let's see what happens

176
00:11:34,700 --> 00:11:41,640
okay so when it's comparing rdi to r12

177
00:11:38,579 --> 00:11:43,380
it's when actually it's testing at the

178
00:11:41,640 --> 00:11:46,800
beginning of the loop give the

179
00:11:43,380 --> 00:11:48,720
enlistment address equal the enlistment

180
00:11:46,800 --> 00:11:50,220
head and so it's testing if it's at the

181
00:11:48,720 --> 00:11:51,899
end of the linked list but obviously

182
00:11:50,220 --> 00:11:53,700
it's never going to happen because now

183
00:11:51,899 --> 00:11:55,800
it's manipulating a userland enlistment

184
00:11:53,700 --> 00:11:58,519
and it's comparing that to a

185
00:11:55,800 --> 00:11:58,519
kernel address

186
00:12:04,380 --> 00:12:10,019
so yeah sure userland enlistment and an

187
00:12:07,860 --> 00:12:12,860
enlistment head address okay so let's see

188
00:12:10,019 --> 00:12:12,860
where the rate goes

189
00:12:27,480 --> 00:12:33,600
so here it's testing the enlistment

190
00:12:30,600 --> 00:12:36,420
flags

191
00:12:33,600 --> 00:12:39,300
so because we haven't set it actually

192
00:12:36,420 --> 00:12:42,420
here it's never going to be finalized so

193
00:12:39,300 --> 00:12:45,079
what do we want to do

194
00:12:42,420 --> 00:12:45,079
let's start

195
00:12:52,459 --> 00:12:58,860
so now it's calling the up reference

196
00:12:55,440 --> 00:13:01,220
objects on the enlistment

197
00:12:58,860 --> 00:13:04,260
okay

198
00:13:01,220 --> 00:13:07,940
so we know r14

199
00:13:04,260 --> 00:13:07,940
is our enlistments

200
00:13:10,320 --> 00:13:14,279
so now basically what he's going to do

201
00:13:11,880 --> 00:13:17,220
is going to actually increment in the

202
00:13:14,279 --> 00:13:20,639
object header the actual counts

203
00:13:17,220 --> 00:13:23,100
so let's look at our

204
00:13:20,639 --> 00:13:25,800
trap enlistment

205
00:13:23,100 --> 00:13:30,300
so we should have set the pointer count

206
00:13:25,800 --> 00:13:34,380
to the value a okay so the object header

207
00:13:30,300 --> 00:13:37,760
is 30 bytes hex before

208
00:13:34,380 --> 00:13:37,760
so if we go back

209
00:13:41,459 --> 00:13:47,360
here and we do

210
00:13:43,920 --> 00:13:47,360
dt_nt

211
00:14:00,420 --> 00:14:05,279
okay so we can see our pointer counts

212
00:14:03,000 --> 00:14:06,959
equal 10 okay

213
00:14:05,279 --> 00:14:10,459
so now it's going to actually reference

214
00:14:06,959 --> 00:14:10,459
the object so let's see what happens

215
00:14:17,339 --> 00:14:21,000
okay so now you can see it's

216
00:14:19,079 --> 00:14:24,240
incrementing the point account to 11

217
00:14:21,000 --> 00:14:28,940
okay so let's continue execution step

218
00:14:24,240 --> 00:14:28,940
over on the KE way for single objects

219
00:14:34,079 --> 00:14:39,320
then it's setting the ??????????

220
00:14:36,899 --> 00:14:39,320
false

221
00:14:39,500 --> 00:14:42,860
by default

222
00:14:47,639 --> 00:14:53,540
then it's retrieving the enlistment

223
00:14:50,220 --> 00:14:53,540
shifted flags

224
00:14:56,940 --> 00:15:01,760
where are we

225
00:14:58,740 --> 00:15:01,760
yeah we are here

226
00:15:04,459 --> 00:15:08,420
okay so that's an interesting

227
00:15:07,079 --> 00:15:12,440
instruction

228
00:15:08,420 --> 00:15:12,440
basically what is CL

229
00:15:12,959 --> 00:15:20,839
????? zero so it's
retrieving the enlistment

230
00:15:16,680 --> 00:15:24,540
flag from here putting that into ecx

231
00:15:20,839 --> 00:15:26,279
and it's testing the lower byte CL and

232
00:15:24,540 --> 00:15:28,320
then it's going to Branch depending on

233
00:15:26,279 --> 00:15:30,839
if it's signed or not and you can see

234
00:15:28,320 --> 00:15:33,779
the decompiler is saying if enlistment

235
00:15:30,839 --> 00:15:35,760
flags is less than zero so because it's

236
00:15:33,779 --> 00:15:39,480
testing a byte so it's actually

237
00:15:35,760 --> 00:15:43,320
testing if the higher bytes is actually

238
00:15:39,480 --> 00:15:47,399
set which is like testing effectively if

239
00:15:43,320 --> 00:15:50,639
the enlistment flags has the byte

240
00:15:47,399 --> 00:15:53,880
corresponding to the hex 80 value set

241
00:15:50,639 --> 00:15:56,880
let's look at the actual flags

242
00:16:04,579 --> 00:16:11,160
flags we can see that the flag

243
00:16:07,500 --> 00:16:13,459
corresponding to 80 is the notifiable

244
00:16:11,160 --> 00:16:13,459
flag

245
00:16:14,100 --> 00:16:19,199
so basically this

246
00:16:16,800 --> 00:16:24,060
code is equivalent to say if enlistment

247
00:16:19,199 --> 00:16:26,579
flags and notifiable flag is non-zero so

248
00:16:24,060 --> 00:16:27,899
it's testing if it's notifiable

249
00:16:26,579 --> 00:16:30,120
okay

250
00:16:27,899 --> 00:16:34,139
and so then

251
00:16:30,120 --> 00:16:36,660
what we want now is basically to figure

252
00:16:34,139 --> 00:16:40,259
out where we can set the breakpoints

253
00:16:36,660 --> 00:16:42,480
that will allow us to debug post rate

254
00:16:40,259 --> 00:16:45,600
condition when we have won the race

255
00:16:42,480 --> 00:16:48,600
condition and so we're going to use a

256
00:16:45,600 --> 00:16:51,660
superior kenlistment and so basically

257
00:16:48,600 --> 00:16:54,360
here we need to be notifiable then we

258
00:16:51,660 --> 00:16:57,240
need a valid transaction pointer and

259
00:16:54,360 --> 00:16:58,920
then the state needs to be something

260
00:16:57,240 --> 00:17:01,620
that we need to figure out and then we

261
00:16:58,920 --> 00:17:03,899
need to reach that specific case where

262
00:17:01,620 --> 00:17:06,299
it says that the transaction Notifier

263
00:17:03,899 --> 00:17:08,579
recover is used for the notification

264
00:17:06,299 --> 00:17:10,919
mask so if we actually look at the

265
00:17:08,579 --> 00:17:15,240
disassembly for that so the decompile

266
00:17:10,919 --> 00:17:17,819
code is quite confusing so where are we

267
00:17:15,240 --> 00:17:21,140
so at the moment we're here

268
00:17:17,819 --> 00:17:21,140
so let's see where we go

269
00:17:25,020 --> 00:17:31,260
okay so here we just exited the if

270
00:17:28,500 --> 00:17:34,500
condition entirely obviously because

271
00:17:31,260 --> 00:17:37,700
the enlistment is not notifiable okay so

272
00:17:34,500 --> 00:17:37,700
let's go back where we were

273
00:17:39,600 --> 00:17:44,700
so we are here

274
00:17:41,460 --> 00:17:46,860
so let's see what we want

275
00:17:44,700 --> 00:17:49,200
so here it's testing the enlistment

276
00:17:46,860 --> 00:17:52,460
flags to make sure it's notifiable so we

277
00:17:49,200 --> 00:17:55,380
want this to be the case okay

278
00:17:52,460 --> 00:17:57,000
then it's not gonna jump it's going to

279
00:17:55,380 --> 00:18:00,120
continue here

280
00:17:57,000 --> 00:18:04,200
put the value 1 it's testing if the

281
00:18:00,120 --> 00:18:06,419
enlistment is superior so if we go here

282
00:18:04,200 --> 00:18:09,720
the superior is testing the value one

283
00:18:06,419 --> 00:18:11,039
okay so we want this to be superior so

284
00:18:09,720 --> 00:18:13,200
that's that's going to be a break point

285
00:18:11,039 --> 00:18:16,380
in an area of the code that is never

286
00:18:13,200 --> 00:18:19,740
Rich usually so then this jump won't be

287
00:18:16,380 --> 00:18:22,320
taken because the end of the flag these

288
00:18:19,740 --> 00:18:24,840
kenlistment Superior flag will be non-zero

289
00:18:22,320 --> 00:18:27,000
so we're going to continue execution

290
00:18:24,840 --> 00:18:30,419
and then he's going to test the

291
00:18:27,000 --> 00:18:31,940
transaction States so Rex is going to be

292
00:18:30,419 --> 00:18:35,640
a transaction

293
00:18:31,940 --> 00:18:38,340
as can be retrieved from here

294
00:18:35,640 --> 00:18:40,380
and then it's going to retrieve the

295
00:18:38,340 --> 00:18:43,020
transaction state and it's going to test

296
00:18:40,380 --> 00:18:44,880
the states with two values three and

297
00:18:43,020 --> 00:18:47,299
four so let's look at the K transaction

298
00:18:44,880 --> 00:18:47,299
States

299
00:18:50,360 --> 00:18:56,120
so three and four are prepared and in

300
00:18:53,760 --> 00:18:56,120
doubt

301
00:18:56,460 --> 00:19:01,620
so what do we want we want we said we

302
00:18:58,980 --> 00:19:06,780
want to reach that specific instruction

303
00:19:01,620 --> 00:19:09,720
so we want this jump to be not taken and

304
00:19:06,780 --> 00:19:14,160
we also want this gem to be not taken

305
00:19:09,720 --> 00:19:16,380
so here it's testing if the state is

306
00:19:14,160 --> 00:19:19,679
three and so we want this to be not the

307
00:19:16,380 --> 00:19:22,919
case because we want this jump to not be

308
00:19:19,679 --> 00:19:25,380
taken so we don't want this to be

309
00:19:22,919 --> 00:19:29,400
prepared because otherwise this jump

310
00:19:25,380 --> 00:19:33,360
will be taken and then we want the state

311
00:19:29,400 --> 00:19:35,820
to be 4 because if it's not 4 then it

312
00:19:33,360 --> 00:19:38,160
will be non-zero and so it will actually

313
00:19:35,820 --> 00:19:41,700
take the jump so we actually want the

314
00:19:38,160 --> 00:19:43,679
state to be in doubt okay and then it

315
00:19:41,700 --> 00:19:46,080
will be able to reach that states

316
00:19:43,679 --> 00:19:47,700
the other thing we can do here is

317
00:19:46,080 --> 00:19:50,100
actually because the debugger is

318
00:19:47,700 --> 00:19:53,160
attached with ret-sync we can actually

319
00:19:50,100 --> 00:19:56,240
see where this breakpoint would be so if

320
00:19:53,160 --> 00:19:56,240
we do Ctrl F2

321
00:19:58,860 --> 00:20:03,000
and then BL

322
00:20:01,080 --> 00:20:05,400
it's actually telling us that the

323
00:20:03,000 --> 00:20:08,700
breakpoint is that the TM recovery

324
00:20:05,400 --> 00:20:10,919
resourcemanagerExt+0*18c so

325
00:20:08,700 --> 00:20:13,980
we can set that breakpoint in the in the

326
00:20:10,919 --> 00:20:17,280
next D-backs in the session

327
00:20:13,980 --> 00:20:21,480
so to summarize here we know that we

328
00:20:17,280 --> 00:20:24,000
need the flags to be not only notifiable

329
00:20:21,480 --> 00:20:27,179
but also Superior we need the

330
00:20:24,000 --> 00:20:29,160
transaction field to be set to a fake

331
00:20:27,179 --> 00:20:31,799
transaction and the state of the

332
00:20:29,160 --> 00:20:34,320
transaction needs to be transaction in

333
00:20:31,799 --> 00:20:36,240
doubt so we actually reach that

334
00:20:34,320 --> 00:20:40,140
instruction and the last thing we need

335
00:20:36,240 --> 00:20:44,100
is we also need the kenlistment next samerm

336
00:20:40,140 --> 00:20:46,500
flame pointer to be set to itself so

337
00:20:44,100 --> 00:20:49,020
it's actually a trap enlistment

338
00:20:46,500 --> 00:20:51,240
so I've modified the source code in

339
00:20:49,020 --> 00:20:54,480
order to reach our goal so if we look at

340
00:20:51,240 --> 00:20:57,780
how we initialize the
fake enlistment now we

341
00:20:54,480 --> 00:20:59,920
see for the Trap enlistment

342
00:20:57,780 --> 00:21:01,100
we said we need the trap enlistment

343
00:21:01,100 --> 00:21:08,039
to be superior so we can set the break

344
00:21:05,400 --> 00:21:10,260
point in an area of the code that is not

345
00:21:08,039 --> 00:21:12,900
reachable in the normal scenario we need

346
00:21:10,260 --> 00:21:15,960
it to also be notifiable so it goes into

347
00:21:12,900 --> 00:21:18,059
this if condition and we need its

348
00:21:15,960 --> 00:21:20,400
transaction field to point to a real

349
00:21:18,059 --> 00:21:23,220
transaction

350
00:21:20,400 --> 00:21:27,240
and this transaction need to have its

351
00:21:23,220 --> 00:21:30,059
state defined as K transaction in doubt

352
00:21:27,240 --> 00:21:32,280
this is so we entered this specific case

353
00:21:30,059 --> 00:21:34,980
that is not reachable and we can set the

354
00:21:32,280 --> 00:21:37,380
breakpoints and for the kernel to be

355
00:21:34,980 --> 00:21:40,200
stuck indefinitely we need the Trap

356
00:21:37,380 --> 00:21:43,260
enlistment to point to itself which is

357
00:21:40,200 --> 00:21:45,539
indicated by this next samerm Flink points

358
00:21:43,260 --> 00:21:48,000
to itself so it's effectively a trap on

359
00:21:45,539 --> 00:21:51,000
this one because the kernel is trapped

360
00:21:56,159 --> 00:22:03,419
and then when we are going to parse the

361
00:22:00,120 --> 00:22:03,419
different notification

362
00:22:05,820 --> 00:22:11,700
we have added some code in order to

363
00:22:08,700 --> 00:22:14,340
detect that our trap enlistment has been

364
00:22:11,700 --> 00:22:16,740
touched by the kernel and so after we

365
00:22:14,340 --> 00:22:19,080
have potentially won the race we

366
00:22:16,740 --> 00:22:21,659
basically look here and we're gonna

367
00:22:19,080 --> 00:22:24,900
basically test that our trap enlistment

368
00:22:21,659 --> 00:22:27,539
flies potentially have their notifiable

369
00:22:24,900 --> 00:22:30,659
flag removed and so if that's the case

370
00:22:27,539 --> 00:22:32,940
it means the kernel touch
our amusemeenlistmentnt

371
00:22:30,659 --> 00:22:34,799
and removed our flag that we just set

372
00:22:32,940 --> 00:22:36,240
initially in the previous function I

373
00:22:34,799 --> 00:22:40,400
showed you

374
00:22:36,240 --> 00:22:40,400
so now I'm executing the binary again

375
00:22:43,919 --> 00:22:47,340
so now go in the debugger and I'm going

376
00:22:46,080 --> 00:22:50,720
to set

377
00:22:47,340 --> 00:22:50,720
the breakpoint I need

378
00:22:57,539 --> 00:23:00,980
I'm going to continue execution

379
00:23:01,559 --> 00:23:05,120
so I'm in the variable function

380
00:23:06,000 --> 00:23:11,280
I hit the breakpoints when it sends a

381
00:23:08,280 --> 00:23:13,380
notification so I skip it at least 32

382
00:23:11,280 --> 00:23:15,960
times

383
00:23:13,380 --> 00:23:16,980
a few moments later

384
00:23:16,980 --> 00:23:21,200
now I'm going to patch the memory

385
00:23:21,360 --> 00:23:24,620
so the actual

386
00:23:25,200 --> 00:23:28,700
recovery thread is stuck

387
00:23:30,240 --> 00:23:35,900
and I'm continuing execution and I can

388
00:23:32,400 --> 00:23:35,900
see the recovery thread is stuck

389
00:23:36,419 --> 00:23:39,919
so I can continue education

390
00:23:40,440 --> 00:23:46,640
we can see more than 32 enlistments have

391
00:23:44,039 --> 00:23:46,640
been notified

392
00:23:49,260 --> 00:23:53,659
so now I break and I patch the memory

393
00:23:58,380 --> 00:24:02,820
so now I'm going to disable these

394
00:24:00,960 --> 00:24:06,000
breakpoints I'm going to enable the

395
00:24:02,820 --> 00:24:07,679
breakpoint post post patch however in

396
00:24:06,000 --> 00:24:11,120
this case I actually don't need to

397
00:24:07,679 --> 00:24:11,120
enable that particular breakpoint

398
00:24:14,820 --> 00:24:21,780
because basically I just need to set the

399
00:24:18,240 --> 00:24:25,159
breakpoint for the actual superior case

400
00:24:21,780 --> 00:24:25,159
so that's what I'm gonna do

401
00:24:27,900 --> 00:24:32,760
so as you can see only the breakpoint

402
00:24:29,880 --> 00:24:33,840
post rays in the superior case has been

403
00:24:32,760 --> 00:24:37,580
set

404
00:24:33,840 --> 00:24:37,580
so we're going to continue education

405
00:24:37,980 --> 00:24:43,740
and boom it hits so the fact it hits

406
00:24:41,039 --> 00:24:46,140
means that it actually triggered the

407
00:24:43,740 --> 00:24:48,679
code path for our userland enlistments

408
00:24:53,280 --> 00:24:58,260
so in this case

409
00:24:55,679 --> 00:25:01,260
we can see

410
00:24:58,260 --> 00:25:01,260
r14

411
00:25:02,220 --> 00:25:06,740
is our

412
00:25:03,780 --> 00:25:06,740
enlistment

413
00:25:08,460 --> 00:25:12,260
and we can see it's a userland address

414
00:25:44,460 --> 00:25:49,020
as we can see our userland fake

415
00:25:47,100 --> 00:25:51,779
enlistments the actual trap enlistmentnd

416
00:25:49,020 --> 00:25:53,700
means points to itself here it points to

417
00:25:51,779 --> 00:25:56,279
a k transaction that is also defined

418
00:25:53,700 --> 00:25:58,740
in userland it has the flags for

419
00:25:56,279 --> 00:26:00,600
night has the good fiber flag and the

420
00:25:58,740 --> 00:26:03,559
superior flag sets

421
00:26:00,600 --> 00:26:03,559
foreign

422
00:26:04,039 --> 00:26:12,360
to itself because r18

423
00:26:07,919 --> 00:26:15,539
is c8d1e0

424
00:26:12,360 --> 00:26:16,740
c 8d1 so we know it's actually an

425
00:26:15,539 --> 00:26:18,720
offset from the beginning of

426
00:26:16,740 --> 00:26:21,380
kenlistment and then if we look at the

427
00:26:18,720 --> 00:26:21,380
K transaction

428
00:26:24,120 --> 00:26:28,799
we can see that the state is defined as

429
00:26:26,279 --> 00:26:30,720
K transaction in doubt okay so I'll

430
00:26:28,799 --> 00:26:32,960
continue execution no other breakpoint

431
00:26:30,720 --> 00:26:32,960
hits

432
00:26:33,720 --> 00:26:39,539
I go back here nice the userland code

433
00:26:36,779 --> 00:26:42,299
actually detected the kenlistment is

434
00:26:39,539 --> 00:26:44,820
notifiable flag was unset so we were

435
00:26:42,299 --> 00:26:47,220
able to detect from userland that we won

436
00:26:44,820 --> 00:26:49,700
the race hope you enjoyed it thank you

437
00:26:47,220 --> 00:26:49,700
for watching

