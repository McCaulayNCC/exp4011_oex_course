1
00:00:00,179 --> 00:00:04,799
so a main pipe is a channel to

2
00:00:02,639 --> 00:00:08,160
communicate between two processes so it

3
00:00:04,799 --> 00:00:10,740
is like an IPC this is transmit

4
00:00:08,160 --> 00:00:13,019
arbitrary messages and it is generally

5
00:00:10,740 --> 00:00:16,680
bi-directional so usually when you have

6
00:00:13,019 --> 00:00:19,920
an IPC channel the endpoint one can send

7
00:00:16,680 --> 00:00:23,460
data to the endpoint two the named pipes

8
00:00:19,920 --> 00:00:26,100
themselves are a Windows thing and

9
00:00:23,460 --> 00:00:28,500
Windows supports both pipes and name

10
00:00:26,100 --> 00:00:30,900
pipes and the only difference is that

11
00:00:28,500 --> 00:00:32,640
the named pipes have a name as you can

12
00:00:30,900 --> 00:00:34,559
imagine so when you are in a real

13
00:00:32,640 --> 00:00:36,960
scenario you would have two different

14
00:00:34,559 --> 00:00:38,760
processes and each one would open the

15
00:00:36,960 --> 00:00:40,800
same named pipe and then they would be

16
00:00:38,760 --> 00:00:43,559
able to send data to each others so

17
00:00:40,800 --> 00:00:45,780
usually it's between processes but it

18
00:00:43,559 --> 00:00:48,360
could also be between threads and so it

19
00:00:45,780 --> 00:00:51,239
uses the heap to store sent messages

20
00:00:48,360 --> 00:00:53,760
and these messages are removed from the

21
00:00:51,239 --> 00:00:56,340
heap when they are read from the other

22
00:00:53,760 --> 00:00:59,160
endpoints so when we create named pipes

23
00:00:56,340 --> 00:01:01,559
when the client and the server open the

24
00:00:59,160 --> 00:01:03,840
two sides of the name types the kernel is

25
00:01:01,559 --> 00:01:05,519
going to allocate structures in the

26
00:01:03,840 --> 00:01:08,159
kernel to deal with it but what is

27
00:01:05,519 --> 00:01:11,159
important for our feng shui is that when

28
00:01:08,159 --> 00:01:12,900
they are going to actually send data on

29
00:01:11,159 --> 00:01:15,000
the named pipe the kernel is going to

30
00:01:12,900 --> 00:01:18,240
store this data on the non-page pool

31
00:01:15,000 --> 00:01:20,040
and that that's the message that we send

32
00:01:18,240 --> 00:01:22,439
over the named pipes that we are

33
00:01:20,040 --> 00:01:24,840
interested in so the writes that we do

34
00:01:22,439 --> 00:01:27,000
that are going to be allocated and it's

35
00:01:24,840 --> 00:01:29,520
easy to control the allocation and

36
00:01:27,000 --> 00:01:31,740
they're free because we can read from

37
00:01:29,520 --> 00:01:35,159
the other endpoints to free it and you

38
00:01:31,740 --> 00:01:37,460
can just not read to make it remain in

39
00:01:35,159 --> 00:01:37,460
memory

