1
00:00:00,160 --> 00:00:05,140
so I think you can be really happy about

2
00:00:02,800 --> 00:00:09,220
yourself because you've done all of that

3
00:00:05,140 --> 00:00:11,260
up until the ketnel address Revelation in

4
00:00:09,220 --> 00:00:12,700
the Following parts we're going to see

5
00:00:11,260 --> 00:00:15,219
how we can actually win the race

6
00:00:12,700 --> 00:00:18,100
condition as for now we use the debugger

7
00:00:15,219 --> 00:00:20,260
to help us win the race and we're going

8
00:00:18,100 --> 00:00:22,420
to see how we can build an arbitrary

9
00:00:20,260 --> 00:00:25,060
read write primitive because for now we

10
00:00:22,420 --> 00:00:27,400
just leaked two kernel addresses the K

11
00:00:25,060 --> 00:00:29,740
shred and the K resource manager so we

12
00:00:27,400 --> 00:00:31,900
don't even have an arbitrary ??? primitive

13
00:00:29,740 --> 00:00:34,660
yet and so we're going to see why

14
00:00:31,900 --> 00:00:37,239
leaking these two kernel addresses will

15
00:00:34,660 --> 00:00:39,579
allow us to actually
build an arbitary right

16
00:00:37,239 --> 00:00:42,340
primitive and once we get the arbitrary

17
00:00:39,579 --> 00:00:45,340
ride primitive we are going to see how

18
00:00:42,340 --> 00:00:47,860
we can swap our own process token to get

19
00:00:45,340 --> 00:00:51,460
system privileges and so you will be

20
00:00:47,860 --> 00:00:54,100
able to finally pop a system shell I

21
00:00:51,460 --> 00:00:56,680
really hope you enjoyed the training so

22
00:00:54,100 --> 00:00:59,100
far and I'll see you next in another

23
00:00:56,680 --> 00:00:59,100
video

