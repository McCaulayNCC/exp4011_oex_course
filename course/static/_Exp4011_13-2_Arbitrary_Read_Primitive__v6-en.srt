1
00:00:00,000 --> 00:00:04,980
hi everyone in this video we're going to

2
00:00:02,280 --> 00:00:08,519
look into how we can turn the increment

3
00:00:04,980 --> 00:00:10,500
primitive into an arbitrary primitive

4
00:00:08,519 --> 00:00:12,780
and we're going to see how we can

5
00:00:10,500 --> 00:00:15,719
actually use a care resource manager

6
00:00:12,780 --> 00:00:18,000
field known as description field which

7
00:00:15,719 --> 00:00:19,920
we've seen before and how we can use

8
00:00:18,000 --> 00:00:23,340
that field to build an arbitrary

9
00:00:19,920 --> 00:00:25,920
committee okay let's get started

10
00:00:23,340 --> 00:00:27,779
you might be familiar with this NT query

11
00:00:25,920 --> 00:00:29,640
information resource manager function by

12
00:00:27,779 --> 00:00:32,460
now because we've used it to actually

13
00:00:29,640 --> 00:00:34,380
congest the resource manager's mutex

14
00:00:32,460 --> 00:00:36,840
where we are trying to win the race

15
00:00:34,380 --> 00:00:39,360
condition but internally this function

16
00:00:36,840 --> 00:00:42,360
allows us to query the description

17
00:00:39,360 --> 00:00:45,239
fields of the Care Resource manager and

18
00:00:42,360 --> 00:00:48,899
so while it is in the kernel is that it

19
00:00:45,239 --> 00:00:51,899
retrieves the description Unicode string

20
00:00:48,899 --> 00:00:55,140
of the K resource manager object and

21
00:00:51,899 --> 00:00:58,379
then it copies the length bytes pointed

22
00:00:55,140 --> 00:01:00,960
by the buffer field back to userline and

23
00:00:58,379 --> 00:01:04,860
so it directly looks like an arbitrary

24
00:01:00,960 --> 00:01:07,439
primitive if we can set both the buffer

25
00:01:04,860 --> 00:01:10,680
and the length fields of the description

26
00:01:07,439 --> 00:01:13,080
field with arbitrary values we want and

27
00:01:10,680 --> 00:01:15,659
so the question is how do we know the

28
00:01:13,080 --> 00:01:18,960
buffer and length Fields originally

29
00:01:15,659 --> 00:01:21,479
before we increment them with our

30
00:01:18,960 --> 00:01:23,939
increment primitive I.E how do we know

31
00:01:21,479 --> 00:01:26,640
their original values remember the the

32
00:01:23,939 --> 00:01:30,479
chicken and egg problem well it turns

33
00:01:26,640 --> 00:01:32,400
out that all we have to do is create a

34
00:01:30,479 --> 00:01:34,560
resource manager with an empty

35
00:01:32,400 --> 00:01:37,380
description when we actually call the

36
00:01:34,560 --> 00:01:40,259
create resource manager and we pass the

37
00:01:37,380 --> 00:01:42,960
empty description argument to this

38
00:01:40,259 --> 00:01:45,299
function and then the buffer field will

39
00:01:42,960 --> 00:01:47,880
be set to no and the length field will

40
00:01:45,299 --> 00:01:50,759
be set to zero and so by Design we know

41
00:01:47,880 --> 00:01:53,460
the original values so from now on we

42
00:01:50,759 --> 00:01:55,799
can increase then to any value we want

43
00:01:53,460 --> 00:01:58,680
using the increment primitive without

44
00:01:55,799 --> 00:02:01,439
relying on any read primitive in the

45
00:01:58,680 --> 00:02:04,140
first place and so we can set the buffer

46
00:02:01,439 --> 00:02:06,840
to an arbitrary kernel address and also

47
00:02:04,140 --> 00:02:09,599
the length to the number of bytes we

48
00:02:06,840 --> 00:02:11,459
want to read from that kernel address and

49
00:02:09,599 --> 00:02:13,980
then we can just call NT query

50
00:02:11,459 --> 00:02:16,680
information resource manager to read the

51
00:02:13,980 --> 00:02:20,040
data back to userlan and then if we want

52
00:02:16,680 --> 00:02:22,620
to read another kernel address and

53
00:02:20,040 --> 00:02:24,720
another length we can just change both

54
00:02:22,620 --> 00:02:27,060
the buffer and length Fields

55
00:02:24,720 --> 00:02:29,280
respectively to the new address and the

56
00:02:27,060 --> 00:02:32,099
new length we need because we know the

57
00:02:29,280 --> 00:02:34,739
previous values we used previously when

58
00:02:32,099 --> 00:02:37,140
we were leaking the previous kernel

59
00:02:34,739 --> 00:02:39,060
region and we can keep repeating that

60
00:02:37,140 --> 00:02:41,819
again and again the only thing we have

61
00:02:39,060 --> 00:02:44,340
to be careful is to set the buffer field

62
00:02:41,819 --> 00:02:46,560
back to no when we are done with the

63
00:02:44,340 --> 00:02:49,379
exploit and this is to avoid the kernel

64
00:02:46,560 --> 00:02:51,480
to actually free the buffer pointer when

65
00:02:49,379 --> 00:02:54,300
destroying the resource manager but

66
00:02:51,480 --> 00:02:57,120
again this is easy to do because we know

67
00:02:54,300 --> 00:02:59,580
the latest value we set it to so we can

68
00:02:57,120 --> 00:03:01,620
just set it back to zero using the

69
00:02:59,580 --> 00:03:03,840
increments and so this is the code for

70
00:03:01,620 --> 00:03:05,700
the entity query information resource

71
00:03:03,840 --> 00:03:07,680
manager function that we can call from

72
00:03:05,700 --> 00:03:10,200
username in order to read the

73
00:03:07,680 --> 00:03:12,659
description field back and so we can see

74
00:03:10,200 --> 00:03:15,420
that all it does after checking the

75
00:03:12,659 --> 00:03:18,120
arguments is basically it copies the

76
00:03:15,420 --> 00:03:20,760
description field back to username and

77
00:03:18,120 --> 00:03:24,239
it does it by using the memoof copy

78
00:03:20,760 --> 00:03:27,120
functions and copying Lang length bytes

79
00:03:24,239 --> 00:03:29,640
from the buffer pointer and so if you

80
00:03:27,120 --> 00:03:32,099
try to summarize what the arbitrary read

81
00:03:29,640 --> 00:03:34,440
primitive looks like we basically

82
00:03:32,099 --> 00:03:36,959
initially had the recovery thread

83
00:03:34,440 --> 00:03:39,659
trapped in the kernel using a fake

84
00:03:36,959 --> 00:03:42,239
userline trap enhancement and so we

85
00:03:39,659 --> 00:03:44,879
created the resource manager with an

86
00:03:42,239 --> 00:03:47,940
empty description field and so what we

87
00:03:44,879 --> 00:03:51,720
do is we inject several increment and

88
00:03:47,940 --> 00:03:55,319
distance like lwp and dispense and what

89
00:03:51,720 --> 00:03:58,319
each of these lwp enlistments will do is

90
00:03:55,319 --> 00:04:01,019
just increment the description buffer

91
00:03:58,319 --> 00:04:03,720
field so we reach the kernel address we

92
00:04:01,019 --> 00:04:05,760
want and this series of enlistments will

93
00:04:03,720 --> 00:04:07,980
be followed by another crap enlistment

94
00:04:05,760 --> 00:04:10,080
and so now we can just read the

95
00:04:07,980 --> 00:04:11,700
description field from username using

96
00:04:10,080 --> 00:04:14,099
the NT query information resource

97
00:04:11,700 --> 00:04:16,199
measure function and later when we're

98
00:04:14,099 --> 00:04:18,900
done with the exploits we can just

99
00:04:16,199 --> 00:04:21,419
re-inject a series of limited right

100
00:04:18,900 --> 00:04:24,840
primitive enlistments in order to reset

101
00:04:21,419 --> 00:04:26,880
the description brush for field back to

102
00:04:24,840 --> 00:04:29,100
normal and then we can escape the loop

103
00:04:26,880 --> 00:04:31,080
and when the care resource manager

104
00:04:29,100 --> 00:04:33,560
object will be destroyed nothing bad

105
00:04:31,080 --> 00:04:33,560
will happen

