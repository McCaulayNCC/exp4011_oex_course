1
00:00:02,580 --> 00:00:07,980
hi everyone in the next few videos we're

2
00:00:05,640 --> 00:00:11,700
going to look into an alternative method

3
00:00:07,980 --> 00:00:14,519
to exploit this KTM vulnerability and instead

4
00:00:11,700 --> 00:00:16,260
of using the write zero primitive we are

5
00:00:14,519 --> 00:00:19,320
going to use an increment primitive

6
00:00:16,260 --> 00:00:21,720
which is basically a way to increment a

7
00:00:19,320 --> 00:00:24,660
value by one at an arbitrary address

8
00:00:21,720 --> 00:00:26,699
we're going to see how we can turn the

9
00:00:24,660 --> 00:00:28,740
increment primitive into an arbitrary

10
00:00:26,699 --> 00:00:31,800
read primitive then we're going into

11
00:00:28,740 --> 00:00:34,440
detail how to actually use the increment

12
00:00:31,800 --> 00:00:37,200
primitive because incrementing a value

13
00:00:34,440 --> 00:00:39,180
by one takes a lot of time if you want

14
00:00:37,200 --> 00:00:41,879
to reach a given value so we're going to

15
00:00:39,180 --> 00:00:44,160
see what trick we can use to actually

16
00:00:41,879 --> 00:00:46,980
make it visible and finally we'll

17
00:00:44,160 --> 00:00:50,100
compare the write zero primitive

18
00:00:46,980 --> 00:00:52,680
combined with the PreviousMode and the

19
00:00:50,100 --> 00:00:55,980
increment primitive and when can we

20
00:00:52,680 --> 00:00:58,680
combine one or the other and what works

21
00:00:55,980 --> 00:01:02,100
on different operating system versions

22
00:00:58,680 --> 00:01:05,519
and architecture okay let's get started

23
00:01:02,100 --> 00:01:07,560
I am sure you recall this Ki try and

24
00:01:05,519 --> 00:01:09,840
weight thread function by now and there

25
00:01:07,560 --> 00:01:13,260
there was this weight block that we

26
00:01:09,840 --> 00:01:16,380
controlled and then we crafted a fake

27
00:01:13,260 --> 00:01:19,380
owner's thread and then we had to pass

28
00:01:16,380 --> 00:01:23,280
the spinlock so we needed the lower

29
00:01:19,380 --> 00:01:25,619
bits of the thread lock to be zero

30
00:01:23,280 --> 00:01:28,979
initially so it could be changed to one

31
00:01:25,619 --> 00:01:32,820
in this loop and then we would craft a

32
00:01:28,979 --> 00:01:35,400
fake state field to be not suspended so

33
00:01:32,820 --> 00:01:37,979
we could skip this if loop and then we

34
00:01:35,400 --> 00:01:40,200
would reach this write zero primitive

35
00:01:37,979 --> 00:01:42,900
that would allow us to change previous

36
00:01:40,200 --> 00:01:45,960
mode to zero and that's pretty much what

37
00:01:42,900 --> 00:01:48,900
we had to do because we reached the end

38
00:01:45,960 --> 00:01:52,500
of the Ki Tri and white thread function

39
00:01:48,900 --> 00:01:54,899
and so we would just return back to the

40
00:01:52,500 --> 00:01:57,899
vulnerable function quite easily however

41
00:01:54,899 --> 00:02:00,299
there is an interesting other code path

42
00:01:57,899 --> 00:02:02,520
in this function that gives us another

43
00:02:00,299 --> 00:02:04,920
primitive which is basically and

44
00:02:02,520 --> 00:02:07,979
increment primitive and this is shown is

45
00:02:04,920 --> 00:02:10,500
in yellow but basically it's a way to

46
00:02:07,979 --> 00:02:13,739
increment the value at a specific

47
00:02:10,500 --> 00:02:16,319
controlled address by one and so to

48
00:02:13,739 --> 00:02:19,020
reach that code again we provide a

49
00:02:16,319 --> 00:02:21,000
weight block and we provide an owner's

50
00:02:19,020 --> 00:02:23,280
thread and again we have to pass the

51
00:02:21,000 --> 00:02:26,099
spinlock but then we are not trying to

52
00:02:23,280 --> 00:02:27,959
skip the if condition and instead we are

53
00:02:26,099 --> 00:02:30,959
trying to go inside that if condition

54
00:02:27,959 --> 00:02:34,379
and so we need to actually make these

55
00:02:30,959 --> 00:02:37,140
threads state field to be suspended and

56
00:02:34,379 --> 00:02:40,800
then we need to set a queue for the on a

57
00:02:37,140 --> 00:02:43,980
thread and the previous value at an

58
00:02:40,800 --> 00:02:45,959
offset for that address we provide for

59
00:02:43,980 --> 00:02:48,300
the queue field will actually be

60
00:02:45,959 --> 00:02:50,879
incremented by one it's probably worth

61
00:02:48,300 --> 00:02:55,500
saying that the actual increment

62
00:02:50,879 --> 00:02:58,260
primitive works on a 32-bit value so you

63
00:02:55,500 --> 00:03:01,140
can see the use of the interlocked add

64
00:02:58,260 --> 00:03:04,319
function used by IDA to indicate a

65
00:03:01,140 --> 00:03:07,379
32-bit value and that in the case of the

66
00:03:04,319 --> 00:03:09,360
red zero primitive it was it was using a

67
00:03:07,379 --> 00:03:13,620
64-bit value because it was using

68
00:03:09,360 --> 00:03:16,739
interlock and 64 with the actual 64

69
00:03:13,620 --> 00:03:19,440
suffix the main challenge we have is

70
00:03:16,739 --> 00:03:22,980
that as you can see there are three dots

71
00:03:19,440 --> 00:03:24,959
after the increment code and basically

72
00:03:22,980 --> 00:03:27,000
there is a lot of code that we need to

73
00:03:24,959 --> 00:03:30,060
deal with after the increment happens

74
00:03:27,000 --> 00:03:32,400
this is so we can actually return to the

75
00:03:30,060 --> 00:03:34,980
vulnerable function and we don't crash

76
00:03:32,400 --> 00:03:36,659
or something bad happens because we do

77
00:03:34,980 --> 00:03:39,180
need to return to the vulnerable

78
00:03:36,659 --> 00:03:40,920
function at some point anyway right and

79
00:03:39,180 --> 00:03:43,799
so we will see that the big difference

80
00:03:40,920 --> 00:03:46,080
between the red zero primitive and the

81
00:03:43,799 --> 00:03:48,060
increment primitive is that for the

82
00:03:46,080 --> 00:03:51,360
increment primitive because we're going

83
00:03:48,060 --> 00:03:54,480
into this if loop we actually need to

84
00:03:51,360 --> 00:03:57,360
set lots of fields in the case thread

85
00:03:54,480 --> 00:04:00,480
structure so the owner's thread is valid

86
00:03:57,360 --> 00:04:03,060
and we have to do that because of all

87
00:04:00,480 --> 00:04:05,580
the stuff that are happening after the

88
00:04:03,060 --> 00:04:09,420
increment primitive also it's probably

89
00:04:05,580 --> 00:04:11,939
worth noticing that we might want to

90
00:04:09,420 --> 00:04:14,580
turn this increment primitive into an

91
00:04:11,939 --> 00:04:17,100
arbitrary write primitive so we can

92
00:04:14,580 --> 00:04:20,160
write any value we want to any address

93
00:04:17,100 --> 00:04:23,220
and so one way to do that would be to

94
00:04:20,160 --> 00:04:25,380
chain several fake increment care

95
00:04:23,220 --> 00:04:27,479
instruments and Trigger this increment

96
00:04:25,380 --> 00:04:29,460
primitive several times because then

97
00:04:27,479 --> 00:04:32,820
this would allow us to write any value

98
00:04:29,460 --> 00:04:34,740
to any address we want and so this is

99
00:04:32,820 --> 00:04:37,740
the list of function call that we need

100
00:04:34,740 --> 00:04:39,960
to go into in order to reach these Ki

101
00:04:37,740 --> 00:04:43,440
Tri and white thread so going from

102
00:04:39,960 --> 00:04:45,840
keeries mutex up to this function but we

103
00:04:43,440 --> 00:04:49,740
can see that other functions are

104
00:04:45,840 --> 00:04:51,300
actually being called after okay I tried

105
00:04:49,740 --> 00:04:53,820
and white thread and this is because

106
00:04:51,300 --> 00:04:57,180
after we reach our increment primitive

107
00:04:53,820 --> 00:05:00,419
inside Ki try and weight thread our face

108
00:04:57,180 --> 00:05:02,639
thread remember the weight block thread

109
00:05:00,419 --> 00:05:05,100
like the owner's thread from the

110
00:05:02,639 --> 00:05:07,880
previous slide So This Thread this fake

111
00:05:05,100 --> 00:05:10,979
thread will be added to a list called

112
00:05:07,880 --> 00:05:14,639
deferred ready list head and the problem

113
00:05:10,979 --> 00:05:17,699
is that this Ki process shred waitlist

114
00:05:14,639 --> 00:05:20,160
function will be called and a thread

115
00:05:17,699 --> 00:05:22,919
might be prepared for scheduling which

116
00:05:20,160 --> 00:05:25,979
we don't really want because it adds

117
00:05:22,919 --> 00:05:28,740
other complexity remember that the code

118
00:05:25,979 --> 00:05:31,020
is passing our fake case thread

119
00:05:28,740 --> 00:05:33,660
structure from username and our

120
00:05:31,020 --> 00:05:36,060
increment primitive has already executed

121
00:05:33,660 --> 00:05:38,340
in this function so we are not

122
00:05:36,060 --> 00:05:40,860
interested in trying to construct a real

123
00:05:38,340 --> 00:05:44,280
thread that will actually run our goal

124
00:05:40,860 --> 00:05:47,220
is just to exit and return to the

125
00:05:44,280 --> 00:05:49,740
vulnerable function TM recovery resource

126
00:05:47,220 --> 00:05:52,919
manager as soon as possible and it turns

127
00:05:49,740 --> 00:05:56,100
out by setting lots of fields in various

128
00:05:52,919 --> 00:05:58,979
fake userline structures up to the Ki

129
00:05:56,100 --> 00:06:01,320
request process in swap function that is

130
00:05:58,979 --> 00:06:04,320
called we are actually able to return

131
00:06:01,320 --> 00:06:06,780
all all the way back into ke release

132
00:06:04,320 --> 00:06:09,780
mutants and back to the vulnerable

133
00:06:06,780 --> 00:06:12,180
function without scheduling a new fixed

134
00:06:09,780 --> 00:06:15,660
thread which to be honest is a big

135
00:06:12,180 --> 00:06:18,419
relief and so this diagram shows all the

136
00:06:15,660 --> 00:06:20,940
structures Fields you need to have in

137
00:06:18,419 --> 00:06:23,819
order to reach that increment primitive

138
00:06:20,940 --> 00:06:26,580
but most importantly how to survive

139
00:06:23,819 --> 00:06:29,460
after this increment primitive is

140
00:06:26,580 --> 00:06:32,520
executed so similarly to the write zero

141
00:06:29,460 --> 00:06:36,180
primitive we need to set a k mutant

142
00:06:32,520 --> 00:06:39,780
object into the K management and these K

143
00:06:36,180 --> 00:06:44,039
mutants has a weight list head that

144
00:06:39,780 --> 00:06:47,340
points to a k Wade block but now instead

145
00:06:44,039 --> 00:06:49,440
of just having the thread field being an

146
00:06:47,340 --> 00:06:51,660
address that will actually be used for

147
00:06:49,440 --> 00:06:56,039
the write zero primitive we need to

148
00:06:51,660 --> 00:07:00,240
craft a fake case red structure and have

149
00:06:56,039 --> 00:07:03,000
lot of its Fields set to random like not

150
00:07:00,240 --> 00:07:05,819
random but like specific value use and

151
00:07:03,000 --> 00:07:09,300
we need that case thread to point to

152
00:07:05,819 --> 00:07:13,620
another K Wade block and a k-process

153
00:07:09,300 --> 00:07:16,440
structure as well and now the queue

154
00:07:13,620 --> 00:07:19,319
field indicates what address is actually

155
00:07:16,440 --> 00:07:21,900
used for the increment primitive so by

156
00:07:19,319 --> 00:07:23,699
setting you to a specific address we can

157
00:07:21,900 --> 00:07:26,400
increment the value at that specific

158
00:07:23,699 --> 00:07:29,400
address I guess one thing we're saying

159
00:07:26,400 --> 00:07:32,880
is that we also name our increment

160
00:07:29,400 --> 00:07:36,180
primitive a limited write primitive like

161
00:07:32,880 --> 00:07:39,240
lwp and so yeah so the increment

162
00:07:36,180 --> 00:07:42,300
primitive enlistment we call it the lwp

163
00:07:39,240 --> 00:07:44,819
enlistment as well because it gives us a

164
00:07:42,300 --> 00:07:47,880
limited write primitive which in our

165
00:07:44,819 --> 00:07:51,360
case is just an increment by one this

166
00:07:47,880 --> 00:07:53,460
diagram is here to remind us what kind

167
00:07:51,360 --> 00:07:55,440
of primitive we have in in the

168
00:07:53,460 --> 00:07:58,680
vulnerable function with the fake

169
00:07:55,440 --> 00:08:01,440
userland and carelessness and the use of

170
00:07:58,680 --> 00:08:04,620
trap enhancements so the kernel is just

171
00:08:01,440 --> 00:08:06,479
looping over this next same RM Flink

172
00:08:04,620 --> 00:08:09,120
pointer in the Trap enlistment

173
00:08:06,479 --> 00:08:12,360
indefinitely and at any time we can

174
00:08:09,120 --> 00:08:15,000
inject a new enlistment that we call A

175
00:08:12,360 --> 00:08:17,099
Primitive enlistment and it gives us the

176
00:08:15,000 --> 00:08:19,740
opportunity to do things and so this

177
00:08:17,099 --> 00:08:22,440
primitive enlistment can be any type of

178
00:08:19,740 --> 00:08:24,479
announcements we used it for a leak

179
00:08:22,440 --> 00:08:27,500
Amusement a write zero primitive

180
00:08:24,479 --> 00:08:30,599
enlistment and now we know we can use it

181
00:08:27,500 --> 00:08:33,659
for an incremental instruments like The

182
00:08:30,599 --> 00:08:35,580
Limited primitive enlistments and so now

183
00:08:33,659 --> 00:08:38,459
that we have this increment primitive

184
00:08:35,580 --> 00:08:41,640
enlistment that gives us an increment by

185
00:08:38,459 --> 00:08:45,600
one primitive we can chain several of

186
00:08:41,640 --> 00:08:48,300
them in order to increment the value at

187
00:08:45,600 --> 00:08:51,240
a given address as many times as we want

188
00:08:48,300 --> 00:08:53,940
and so one big constraint we have to

189
00:08:51,240 --> 00:08:56,339
deal with if we actually want to be able

190
00:08:53,940 --> 00:08:58,820
to use this increment primitive is that

191
00:08:56,339 --> 00:09:01,680
we need to know the original value

192
00:08:58,820 --> 00:09:05,279
stored at the address we want to modify

193
00:09:01,680 --> 00:09:08,220
so we know how many times we need to

194
00:09:05,279 --> 00:09:10,860
increment it more in order to reach the

195
00:09:08,220 --> 00:09:13,440
value we want to target and so

196
00:09:10,860 --> 00:09:16,380
immediately we need an arbitrary

197
00:09:13,440 --> 00:09:18,899
primitive before we can even use this

198
00:09:16,380 --> 00:09:21,660
increment primitive it is basically the

199
00:09:18,899 --> 00:09:23,880
chicken and egg problem right except

200
00:09:21,660 --> 00:09:28,620
there is one case where we don't really

201
00:09:23,880 --> 00:09:31,620
need to know the value beforehand is if

202
00:09:28,620 --> 00:09:34,200
we actually know the original value of

203
00:09:31,620 --> 00:09:36,779
certain fields in some kernel structures

204
00:09:34,200 --> 00:09:39,600
that we know the address of and we

205
00:09:36,779 --> 00:09:43,019
modify these fields to build a re

206
00:09:39,600 --> 00:09:45,660
primitive by just blindly incrementing

207
00:09:43,019 --> 00:09:48,540
certain Fields because yeah if we

208
00:09:45,660 --> 00:09:51,420
already know the original values of

209
00:09:48,540 --> 00:09:54,600
certain structures fields in the kernel

210
00:09:51,420 --> 00:09:56,700
then we don't need any leak or whatever

211
00:09:54,600 --> 00:09:58,620
because we just know the values like

212
00:09:56,700 --> 00:10:01,320
default values and then we can just

213
00:09:58,620 --> 00:10:04,680
blindly increment them to the value we

214
00:10:01,320 --> 00:10:06,720
want without requiring to read them in

215
00:10:04,680 --> 00:10:09,420
the first place and so if we find

216
00:10:06,720 --> 00:10:11,760
certain structures we can abuse in such

217
00:10:09,420 --> 00:10:14,160
a way that would give us an arbitrary

218
00:10:11,760 --> 00:10:15,959
read primitive then that will be good

219
00:10:14,160 --> 00:10:18,300
enough to use the increment primitive

220
00:10:15,959 --> 00:10:21,060
for other addresses we don't know the

221
00:10:18,300 --> 00:10:23,700
address of initially the only two kernel

222
00:10:21,060 --> 00:10:26,820
objects we know the address of initially

223
00:10:23,700 --> 00:10:28,620
are the case thread and K resource

224
00:10:26,820 --> 00:10:32,459
manager and so if we could actually

225
00:10:28,620 --> 00:10:35,100
build an arbitrary primitive based on

226
00:10:32,459 --> 00:10:37,200
one of these objects and using our

227
00:10:35,100 --> 00:10:39,420
increment primitive that would be enough

228
00:10:37,200 --> 00:10:42,600
and so before you move to the next video

229
00:10:39,420 --> 00:10:44,940
if you want to have a look at both of

230
00:10:42,600 --> 00:10:47,339
these structures in virtually list and

231
00:10:44,940 --> 00:10:50,459
try to find one way to build and

232
00:10:47,339 --> 00:10:52,320
arbitrary primitive that's actually a

233
00:10:50,459 --> 00:10:55,079
very good exercise because this is the

234
00:10:52,320 --> 00:10:58,579
kind of problem we have to solve all the

235
00:10:55,079 --> 00:10:58,579
time in exploit development

