1
00:00:03,200 --> 00:00:07,919
hi everyone

2
00:00:05,160 --> 00:00:11,820
in this part we're going to analyze

3
00:00:07,919 --> 00:00:15,120
how we are able to leak kernel pointers

4
00:00:11,820 --> 00:00:18,359
to userland and why it is happening

5
00:00:15,120 --> 00:00:21,119
where we could have found it

6
00:00:18,359 --> 00:00:22,140
and how generally you approach this kind

7
00:00:21,119 --> 00:00:24,240
of problem

8
00:00:22,140 --> 00:00:25,380
when you want to lead kernel data to

9
00:00:24,240 --> 00:00:27,779
userland

10
00:00:25,380 --> 00:00:30,420
and once we've done that

11
00:00:27,779 --> 00:00:31,980
will easily show how we can return to

12
00:00:30,420 --> 00:00:33,360
userland

13
00:00:31,980 --> 00:00:35,399
okay

14
00:00:33,360 --> 00:00:38,219
let's get started

15
00:00:35,399 --> 00:00:40,440
so we have seen it in the previous lab

16
00:00:38,219 --> 00:00:43,559
but we are going to summarize how the

17
00:00:40,440 --> 00:00:46,020
kernel information leak works and so we

18
00:00:43,559 --> 00:00:48,180
have the kernel processing our fake

19
00:00:46,020 --> 00:00:50,760
userland enlistments and at this point

20
00:00:48,180 --> 00:00:54,300
because there is nothing like the snap

21
00:00:50,760 --> 00:00:56,579
mitigation the kernel just has no way of

22
00:00:54,300 --> 00:00:59,100
knowing that it's no longer accessing

23
00:00:56,579 --> 00:01:02,160
kernel memory so it's just going to be

24
00:00:59,100 --> 00:01:05,519
modifying things as if it was normal

25
00:01:02,160 --> 00:01:07,680
kernel memory but now it's not and the

26
00:01:05,519 --> 00:01:10,260
kernel will just be modifying user and

27
00:01:07,680 --> 00:01:14,100
memory that we can access to from

28
00:01:10,260 --> 00:01:17,400
userland also keep in mind that the KTM

29
00:01:14,100 --> 00:01:19,619
structures have inline structures which

30
00:01:17,400 --> 00:01:24,860
is basically that the K enlistment has

31
00:01:19,619 --> 00:01:28,500
embedded structures like the K mutants

32
00:01:24,860 --> 00:01:30,960
and so initially most of the fields will

33
00:01:28,500 --> 00:01:33,420
be zeros because we provided them from

34
00:01:30,960 --> 00:01:35,939
userland and we didn't specify any value

35
00:01:33,420 --> 00:01:39,479
and so once they are changed by the

36
00:01:35,939 --> 00:01:42,479
kernel it will be easy to notice uh

37
00:01:39,479 --> 00:01:45,540
suddenly there are not zeros anymore and

38
00:01:42,479 --> 00:01:49,079
if you look closely at them you should

39
00:01:45,540 --> 00:01:51,479
see some kernel pointers and so as you

40
00:01:49,079 --> 00:01:52,799
can see we have our fake userland

41
00:01:51,479 --> 00:01:55,439
kenlistments

42
00:01:52,799 --> 00:01:58,619
K mutants that is touched by the kernel

43
00:01:55,439 --> 00:02:01,619
and so it's mutant list entry is

44
00:01:58,619 --> 00:02:05,460
leaking as two kernel addresses and so we

45
00:02:01,619 --> 00:02:09,539
can see one ending with ae58 and the

46
00:02:05,460 --> 00:02:12,180
other one ending with eb30 and so if we

47
00:02:09,539 --> 00:02:14,700
look at what objects these addresses

48
00:02:12,180 --> 00:02:17,340
correspond to using the !pool

49
00:02:14,700 --> 00:02:20,580
command we see that the first leaked

50
00:02:17,340 --> 00:02:24,180
address points to a case thread which is

51
00:02:20,580 --> 00:02:26,940
actually the thread calling TM recovery

52
00:02:24,180 --> 00:02:28,080
resource manager and the other entry in

53
00:02:26,940 --> 00:02:31,680
this linked list

54
00:02:28,080 --> 00:02:33,720
points to a k resource manager which

55
00:02:31,680 --> 00:02:36,360
is actually the K resource manager

56
00:02:33,720 --> 00:02:38,520
pointer passed as an argument to the

57
00:02:36,360 --> 00:02:41,819
same TM recovery resource manager

58
00:02:38,520 --> 00:02:43,680
function and so why does this happen so

59
00:02:41,819 --> 00:02:46,560
basically when entering the vulnerable

60
00:02:43,680 --> 00:02:49,800
function it will log the K resource

61
00:02:46,560 --> 00:02:51,480
manager mutex and then when the main

62
00:02:49,800 --> 00:02:54,540
loop is processing the userland

63
00:02:51,480 --> 00:02:57,780
enlistment it will actually lock the

64
00:02:54,540 --> 00:03:00,239
kenlistments mutex and later before sending

65
00:02:57,780 --> 00:03:02,580
the notification it will unlock the k

66
00:03:00,239 --> 00:03:05,760
resource measure mutex and after it

67
00:03:02,580 --> 00:03:08,340
returns it will relock the K resource

68
00:03:05,760 --> 00:03:09,959
manager mutex and so basically each time

69
00:03:08,340 --> 00:03:12,599
it locks a mutex

70
00:03:09,959 --> 00:03:15,000
for the k instruments at a given

71
00:03:12,599 --> 00:03:18,120
iteration of the loop it would have just

72
00:03:15,000 --> 00:03:20,220
locked a k resource manager mutex at a

73
00:03:18,120 --> 00:03:22,800
previous iteration of the loop so the

74
00:03:20,220 --> 00:03:25,019
question is the mutexes we are talking

75
00:03:22,800 --> 00:03:28,620
about are from two different objects

76
00:03:25,019 --> 00:03:31,080
right a K resource manager and a k and

77
00:03:28,620 --> 00:03:34,019
this means so where is the k thread

78
00:03:31,080 --> 00:03:37,440
address we managed to leak and so what

79
00:03:34,019 --> 00:03:40,799
happens is that a given thread will have

80
00:03:37,440 --> 00:03:43,379
its own internal leaked list of all of

81
00:03:40,799 --> 00:03:47,099
the mutexes that it holds at a given

82
00:03:43,379 --> 00:03:49,620
time and so by first locking the

83
00:03:47,099 --> 00:03:52,080
resource manager and address relative to

84
00:03:49,620 --> 00:03:54,360
the k resource manager is added at

85
00:03:52,080 --> 00:03:57,360
the end of the thread doubly linked list

86
00:03:54,360 --> 00:03:59,879
and because it it will be added to the

87
00:03:57,360 --> 00:04:02,459
end of the list the next pointer of that

88
00:03:59,879 --> 00:04:04,620
list which is the flink boiler will

89
00:04:02,459 --> 00:04:07,140
point to the thread itself and the

90
00:04:04,620 --> 00:04:09,540
previous pointer the blink pointer will

91
00:04:07,140 --> 00:04:11,700
be pointing to another mutex object that

92
00:04:09,540 --> 00:04:13,560
we don't know anything about but that

93
00:04:11,700 --> 00:04:16,560
was previously at the end of the linked

94
00:04:13,560 --> 00:04:19,260
list and then by locking the userland

95
00:04:16,560 --> 00:04:22,019
enlistments an address relative to the

96
00:04:19,260 --> 00:04:24,780
userland kenlistments gets inserted

97
00:04:22,019 --> 00:04:26,940
onto the same doubly linked list of

98
00:04:24,780 --> 00:04:29,100
other mutexes that are had by that

99
00:04:26,940 --> 00:04:31,380
thread and because it will again be

100
00:04:29,100 --> 00:04:33,840
added to the end of the list the next

101
00:04:31,380 --> 00:04:36,180
pointer of that lists the the flink

102
00:04:33,840 --> 00:04:39,060
pointer will point to the thread itself

103
00:04:36,180 --> 00:04:41,100
and the previous pointer the blink

104
00:04:39,060 --> 00:04:43,500
border will be pointing to the previous

105
00:04:41,100 --> 00:04:46,380
object that was at the end of the linked

106
00:04:43,500 --> 00:04:48,540
list which happens to be the K resource

107
00:04:46,380 --> 00:04:52,139
manager because that is the last object

108
00:04:48,540 --> 00:04:54,900
that had its mutex acquired for that

109
00:04:52,139 --> 00:04:57,840
particular thread so one thing we can

110
00:04:54,900 --> 00:05:01,259
see in the loop is that actually the K

111
00:04:57,840 --> 00:05:03,960
enlistment mutex is unlocked and so the

112
00:05:01,259 --> 00:05:06,419
K enlistment will be removed from the

113
00:05:03,960 --> 00:05:09,120
threads doubly linked lists so the

114
00:05:06,419 --> 00:05:11,639
kenlistment mutex
won't appear in the thread

115
00:05:09,120 --> 00:05:14,100
dis anymore but but what matters here is

116
00:05:11,639 --> 00:05:16,380
that our userland enlistments is still

117
00:05:14,100 --> 00:05:19,080
going to have its mutex field pointing

118
00:05:16,380 --> 00:05:21,660
to the previous mutex which is the K

119
00:05:19,080 --> 00:05:24,600
resource manager mutex and pointing to the

120
00:05:21,660 --> 00:05:26,940
next mutex which is the k thread mutex

121
00:05:24,600 --> 00:05:28,860
and this is because removing the

122
00:05:26,940 --> 00:05:31,740
kenlistment mutex from the thread list

123
00:05:28,860 --> 00:05:33,660
only updates the adjacent elements in

124
00:05:31,740 --> 00:05:36,600
the list but not the actual

125
00:05:33,660 --> 00:05:39,180
enlistments elements that is removed

126
00:05:36,600 --> 00:05:41,520
and so basically if we look at the

127
00:05:39,180 --> 00:05:43,680
implementation of ke wait for single

128
00:05:41,520 --> 00:05:46,020
objects we will see that it ends up

129
00:05:43,680 --> 00:05:48,660
modifying a bunch of the fields inside

130
00:05:46,020 --> 00:05:51,180
of the mutex object that is passed and

131
00:05:48,660 --> 00:05:55,259
in our case we are passing a controlled

132
00:05:51,180 --> 00:05:58,979
userland candacement mutex and so we

133
00:05:55,259 --> 00:06:02,580
control the entire new text fields and

134
00:05:58,979 --> 00:06:04,860
so we can specify certain Fields flags

135
00:06:02,580 --> 00:06:07,500
inside of that memory to make sure that

136
00:06:04,860 --> 00:06:10,620
certain code path are hit and so the

137
00:06:07,500 --> 00:06:13,860
mutex argument that is passed to ke wait

138
00:06:10,620 --> 00:06:15,539
for single object is called objects and

139
00:06:13,860 --> 00:06:18,840
so there are different types of mutex

140
00:06:15,539 --> 00:06:21,840
and so we can specify that the mutex is

141
00:06:18,840 --> 00:06:25,139
of the mutant type specifically for

142
00:06:21,840 --> 00:06:28,080
instance to skip the first if loop we

143
00:06:25,139 --> 00:06:32,280
can also set the object header signal

144
00:06:28,080 --> 00:06:35,699
state to be more than zero to enter the

145
00:06:32,280 --> 00:06:40,080
second if condition and for instance

146
00:06:35,699 --> 00:06:44,039
we can set the signal state to 1 because

147
00:06:40,080 --> 00:06:47,360
then signal state becomes zero so we'll

148
00:06:44,039 --> 00:06:51,060
avoid the go to Branch I will continue

149
00:06:47,360 --> 00:06:54,240
then we see additional code and in this

150
00:06:51,060 --> 00:06:57,060
code current thread is a pointer to the

151
00:06:54,240 --> 00:06:59,520
current case thread associated with the

152
00:06:57,060 --> 00:07:02,100
recovery thread executing in the TM

153
00:06:59,520 --> 00:07:05,880
recovery resource measure loop and so

154
00:07:02,100 --> 00:07:08,520
the code sets the object on a thread to

155
00:07:05,880 --> 00:07:11,039
the current thread to this thread which

156
00:07:08,520 --> 00:07:13,860
means we can leak it from userland what

157
00:07:11,039 --> 00:07:16,680
is interesting is that the owner thread

158
00:07:13,860 --> 00:07:19,139
of this mutex is set to the current

159
00:07:16,680 --> 00:07:22,620
thread and this is different from the

160
00:07:19,139 --> 00:07:26,160
actual leak that we saw earlier in the W

161
00:07:22,620 --> 00:07:30,120
linked list so there are technically two

162
00:07:26,160 --> 00:07:33,000
ways to leak the K trade object and

163
00:07:30,120 --> 00:07:36,780
later in the code our K mutant pointer

164
00:07:33,000 --> 00:07:40,080
which is the object variable is inserted

165
00:07:36,780 --> 00:07:43,560
into the current threads mutant linked

166
00:07:40,080 --> 00:07:45,300
list which is the mutant list head and

167
00:07:43,560 --> 00:07:48,060
it happens to be the case that after

168
00:07:45,300 --> 00:07:51,539
insertion the object

169
00:07:48,060 --> 00:07:54,479
Flink entry will point to the address of

170
00:07:51,539 --> 00:07:57,780
the k thread mutant list head which is

171
00:07:54,479 --> 00:08:01,680
the head of the mutants being held and

172
00:07:57,780 --> 00:08:04,099
the object blink entry will always be

173
00:08:01,680 --> 00:08:07,319
the k resource manager structure

174
00:08:04,099 --> 00:08:10,560
associated with our resource manager as

175
00:08:07,319 --> 00:08:13,139
it was the last mutant locked by this

176
00:08:10,560 --> 00:08:15,660
thread and so one question that could

177
00:08:13,139 --> 00:08:19,440
come to your mind would be how do you

178
00:08:15,660 --> 00:08:22,500
know how to set all these fields to

179
00:08:19,440 --> 00:08:26,160
trigger the information leak and I think

180
00:08:22,500 --> 00:08:30,360
the answer to this is a mixture of trial

181
00:08:26,160 --> 00:08:33,240
and error but also lack and so by trial

182
00:08:30,360 --> 00:08:35,159
and error I basically mean to reverse

183
00:08:33,240 --> 00:08:37,200
engineer the code around the vulnerable

184
00:08:35,159 --> 00:08:39,240
function as well as the function being

185
00:08:37,200 --> 00:08:42,240
called by the vulnerable function

186
00:08:39,240 --> 00:08:45,600
looking for certain cut path that would

187
00:08:42,240 --> 00:08:48,660
allow to either leak kernel pointers and

188
00:08:45,600 --> 00:08:51,300
or get a write primitive and I think in

189
00:08:48,660 --> 00:08:54,420
our case all of the fields that needed

190
00:08:51,300 --> 00:08:57,600
to be set happened to be set anyways

191
00:08:54,420 --> 00:09:01,080
because originally what we're trying to

192
00:08:57,600 --> 00:09:02,880
do was to use one of these functions to

193
00:09:01,080 --> 00:09:05,880
find a way to get the write primitive

194
00:09:02,880 --> 00:09:09,000
and so we had set a bunch of these

195
00:09:05,880 --> 00:09:12,839
fields that you can see here and here

196
00:09:09,000 --> 00:09:15,180
saying that the mutex was signaled and

197
00:09:12,839 --> 00:09:18,360
we were following path and then you

198
00:09:15,180 --> 00:09:20,940
basically end up in this code path that

199
00:09:18,360 --> 00:09:23,880
ends up writing a lot of data into the

200
00:09:20,940 --> 00:09:26,040
mutex and so in this case we only

201
00:09:23,880 --> 00:09:28,200
noticed that it was happening in the

202
00:09:26,040 --> 00:09:30,839
debugger when we were printing the

203
00:09:28,200 --> 00:09:33,959
contents of our fake userland enlistments

204
00:09:30,839 --> 00:09:36,839
with the DT command as we could see

205
00:09:33,959 --> 00:09:40,260
there was a kernel pointer written to our

206
00:09:36,839 --> 00:09:43,019
userland knlistments
so it is just to show that

207
00:09:40,260 --> 00:09:46,260
there is not like a written methodology

208
00:09:43,019 --> 00:09:48,480
and just sometimes you end up finding

209
00:09:46,260 --> 00:09:52,680
things on the way and so the third

210
00:09:48,480 --> 00:09:54,600
process is basically just okay we know

211
00:09:52,680 --> 00:09:57,120
we control again an kenlistments in

212
00:09:54,600 --> 00:10:00,060
userrland and we are stuck inside this

213
00:09:57,120 --> 00:10:01,620
loop so we just want to start working as

214
00:10:00,060 --> 00:10:04,920
many different pieces of the code as

215
00:10:01,620 --> 00:10:07,380
possible either in  WinDbg or IDA or

216
00:10:04,920 --> 00:10:10,500
Ghidra and sometimes you see something

217
00:10:07,380 --> 00:10:12,899
you dig around and you see oh this may

218
00:10:10,500 --> 00:10:14,880
look like a write primitive or something

219
00:10:12,899 --> 00:10:17,100
kind of cooling Ghidra but you want to

220
00:10:14,880 --> 00:10:19,440
make sure that it's real or you want to

221
00:10:17,100 --> 00:10:21,600
see how that actually manifests in the

222
00:10:19,440 --> 00:10:24,660
debugger and if you can actually hit

223
00:10:21,600 --> 00:10:27,779
that that area of code and so a lot of

224
00:10:24,660 --> 00:10:30,120
the time you'll just set up memory and

225
00:10:27,779 --> 00:10:32,459
then just single step to see where you

226
00:10:30,120 --> 00:10:35,279
go and then for instance in this case we

227
00:10:32,459 --> 00:10:37,620
wanted this code path to heat and then we

228
00:10:35,279 --> 00:10:40,800
just see all of a sudden we are leaking

229
00:10:37,620 --> 00:10:43,440
addresses and so yeah this is basically

230
00:10:40,800 --> 00:10:46,140
what it looks like to trigger the leak

231
00:10:43,440 --> 00:10:48,660
basically you just need to specify in

232
00:10:46,140 --> 00:10:51,899
the dispatch header of the k mutant

233
00:10:48,660 --> 00:10:55,440
structure in your k enlistments that the

234
00:10:51,899 --> 00:10:58,320
type of the mutex is a mutant object you

235
00:10:55,440 --> 00:11:01,200
want to set it to the signal state by

236
00:10:58,320 --> 00:11:03,959
sitting setting signal state to 1 and

237
00:11:01,200 --> 00:11:06,899
then it will not only set the owner

238
00:11:03,959 --> 00:11:09,360
thread field of the k mutant to a

239
00:11:06,899 --> 00:11:12,019
value relative to the k thread but it

240
00:11:09,360 --> 00:11:15,300
will also set the mutant list entry

241
00:11:12,019 --> 00:11:19,019
Flink and blink pointers respectively to

242
00:11:15,300 --> 00:11:21,540
a value relative to the k thread and

243
00:11:19,019 --> 00:11:24,120
to the K resource manager and then you

244
00:11:21,540 --> 00:11:25,740
can fairly easily compute the base

245
00:11:24,120 --> 00:11:26,940
address of the Care Resource manager

246
00:11:25,740 --> 00:11:29,220
from userland

247
00:11:26,940 --> 00:11:31,500
since you can read the leaked data from

248
00:11:29,220 --> 00:11:34,440
userland so again we can test the

249
00:11:31,500 --> 00:11:36,720
notifiable flag has been unset to know

250
00:11:34,440 --> 00:11:38,820
that this particular leak enlistment has

251
00:11:36,720 --> 00:11:41,220
been parsed by the kernel which means

252
00:11:38,820 --> 00:11:43,560
that it will have the leaked data and

253
00:11:41,220 --> 00:11:45,740
then you just pull out the pointer which

254
00:11:43,560 --> 00:11:48,600
points into the linked list entry

255
00:11:45,740 --> 00:11:50,399
subtract a certain amount to get the K

256
00:11:48,600 --> 00:11:53,160
resource manager base address

257
00:11:50,399 --> 00:11:55,500
and then add another amount to get the

258
00:11:53,160 --> 00:11:57,240
enlistment head and then you can use the

259
00:11:55,500 --> 00:11:59,940
enlistment head address you have just

260
00:11:57,240 --> 00:12:02,519
computed into a new escape enlistment

261
00:11:59,940 --> 00:12:05,519
you can just basically inject an escape

262
00:12:02,519 --> 00:12:08,220
enlistment into the Trap enlistments

263
00:12:05,519 --> 00:12:10,500
and the recovery thread will escape the

264
00:12:08,220 --> 00:12:13,860
loop at the next iteration of the loop

265
00:12:10,500 --> 00:12:16,380
this diagram is exactly the same as an

266
00:12:13,860 --> 00:12:18,360
earlier diagram we showed but now you

267
00:12:16,380 --> 00:12:21,959
should be able to understand it entirely

268
00:12:18,360 --> 00:12:25,560
so now in theory what we have is at

269
00:12:21,959 --> 00:12:29,040
least one trap enlistment that points to

270
00:12:25,560 --> 00:12:31,339
the Primitive enlistments and in this

271
00:12:29,040 --> 00:12:34,920
case one of the Primitive enlistment

272
00:12:31,339 --> 00:12:37,079
would be the leak enlistment and then

273
00:12:34,920 --> 00:12:39,600
that leak enlistment would point to

274
00:12:37,079 --> 00:12:42,660
another trap enlistment

275
00:12:39,600 --> 00:12:44,760
which then eventually will point to

276
00:12:42,660 --> 00:12:48,120
other primitive enlistments

277
00:12:44,760 --> 00:12:50,459
to possibly build a write primitive and

278
00:12:48,120 --> 00:12:53,399
Elevate privileges and then at the very

279
00:12:50,459 --> 00:12:56,399
end you will inject your escape

280
00:12:53,399 --> 00:12:58,700
enlistments in order to exit the loop

281
00:12:56,399 --> 00:12:58,700
cleanly

