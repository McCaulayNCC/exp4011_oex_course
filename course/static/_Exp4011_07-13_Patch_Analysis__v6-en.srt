1
00:00:00,420 --> 00:00:06,420
so the patch is actually removing the

2
00:00:03,120 --> 00:00:08,580
KENLISTMENT_FINALIZED check so it is never

3
00:00:06,420 --> 00:00:11,340
going to do that check anymore the

4
00:00:08,580 --> 00:00:13,740
variable does not even exist anymore and

5
00:00:11,340 --> 00:00:15,719
it does all the logic straightforwardly

6
00:00:13,740 --> 00:00:18,600
so now they've got a different way of

7
00:00:15,719 --> 00:00:21,000
doing it so they are never relying on

8
00:00:18,600 --> 00:00:23,400
detecting if the KENLISTMENT is

9
00:00:21,000 --> 00:00:25,500
finalized or because they are not

10
00:00:23,400 --> 00:00:27,779
trusting that information and so they

11
00:00:25,500 --> 00:00:30,779
are not used using this validation at

12
00:00:27,779 --> 00:00:33,180
all and what they are doing instead is

13
00:00:30,779 --> 00:00:35,700
they start the list always from the

14
00:00:33,180 --> 00:00:39,120
beginning so what problem is the best

15
00:00:35,700 --> 00:00:41,340
trying to avoid so before on the left we

16
00:00:39,120 --> 00:00:44,520
can see the vulnerable code that we had

17
00:00:41,340 --> 00:00:46,860
we're checking the enlistment and if it

18
00:00:44,520 --> 00:00:49,260
was finalized it was setting a Boolean

19
00:00:46,860 --> 00:00:52,860
and then later depending on the Boolean

20
00:00:49,260 --> 00:00:55,320
it was either taking the Flink pointer or

21
00:00:52,860 --> 00:00:58,199
if the flag was set previously it would

22
00:00:55,320 --> 00:01:01,980
take the enlistment head so that was not

23
00:00:58,199 --> 00:01:03,420
deterministic but now what we see is

24
00:01:01,980 --> 00:01:05,519
that we're not doing any check anymore

25
00:01:03,420 --> 00:01:08,100
in the patched code so we're not going to

26
00:01:05,519 --> 00:01:10,020
test the finalized flag early and then

27
00:01:08,100 --> 00:01:12,840
do an assumption later we're just going

28
00:01:10,020 --> 00:01:15,299
to always definitely take the head of

29
00:01:12,840 --> 00:01:17,820
the list and so technically it's less

30
00:01:15,299 --> 00:01:20,340
efficient but we are trying to avoid the

31
00:01:17,820 --> 00:01:22,020
fact that in the vulnerable code if when

32
00:01:20,340 --> 00:01:25,020
we're testing the flag it wasn't

33
00:01:22,020 --> 00:01:27,600
finalized yet but later it happens to be

34
00:01:25,020 --> 00:01:30,060
finalized in between then we would go

35
00:01:27,600 --> 00:01:32,280
into the state where it would just take

36
00:01:30,060 --> 00:01:35,220
a Flink pointer which is not valid

37
00:01:32,280 --> 00:01:37,799
because there is a time of check time of

38
00:01:35,220 --> 00:01:40,200
use problem basically and in the patch

39
00:01:37,799 --> 00:01:42,540
code it is always going to take the

40
00:01:40,200 --> 00:01:46,280
deterministic method, which is less

41
00:01:42,540 --> 00:01:46,280
efficient but more secure

