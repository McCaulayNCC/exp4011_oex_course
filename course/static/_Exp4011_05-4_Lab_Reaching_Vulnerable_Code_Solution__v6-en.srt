1
00:00:00,000 --> 00:00:05,700
so we know we want to reach the prepared

2
00:00:03,240 --> 00:00:08,099
state and so we need to actually call

3
00:00:05,700 --> 00:00:10,760
the prepare complete function on both of

4
00:00:08,099 --> 00:00:10,760
the enlistments

5
00:00:21,380 --> 00:00:26,820
and we know we don't want to be calling

6
00:00:24,539 --> 00:00:30,000
commit complete though just because we

7
00:00:26,820 --> 00:00:32,759
want the enlistments to still be in the

8
00:00:30,000 --> 00:00:36,239
prepared state because we want them to

9
00:00:32,759 --> 00:00:39,480
be entering this function and reach this

10
00:00:36,239 --> 00:00:44,239
if case so we agree that we'll use the

11
00:00:39,480 --> 00:00:44,239
prepared state so here on purpose

12
00:01:09,540 --> 00:01:14,580
and then we're gonna

13
00:01:11,640 --> 00:01:16,260
add a call to

14
00:01:14,580 --> 00:01:19,400
recover

15
00:01:16,260 --> 00:01:19,400
resource manager

16
00:01:21,360 --> 00:01:27,420
and we're going to give it the

17
00:01:24,240 --> 00:01:29,640
hand on to the resource manager

18
00:01:27,420 --> 00:01:32,280
and just to make it easier to debug

19
00:01:29,640 --> 00:01:34,259
we're going to just have a

20
00:01:32,280 --> 00:01:36,979
get character

21
00:01:34,259 --> 00:01:36,979
just before

22
00:01:53,340 --> 00:01:57,540
okay so we have the code to change the

23
00:01:55,740 --> 00:01:59,939
state of the enlistment and we have

24
00:01:57,540 --> 00:02:02,399
the code to call the vulnerable function

25
00:01:59,939 --> 00:02:06,600
so we have attached WinDbg to the

26
00:02:02,399 --> 00:02:09,840
target VM and we have synced it with

27
00:02:06,600 --> 00:02:11,819
ret-sync to the Ghidra instance so we're

28
00:02:09,840 --> 00:02:16,220
gonna break in the debugger and set the

29
00:02:11,819 --> 00:02:16,220
breakpoint on the vulnerable function

30
00:02:17,879 --> 00:02:21,360
as well as the

31
00:02:19,620 --> 00:02:23,660
function being called from the vulnerable

32
00:02:21,360 --> 00:02:23,660
function

33
00:02:27,900 --> 00:02:34,080
so now we have pushed the binary onto

34
00:02:31,560 --> 00:02:34,980
the target VM already so we're going to

35
00:02:34,080 --> 00:02:38,220
run it

36
00:02:34,980 --> 00:02:40,860
from the target VM so we can see that it

37
00:02:38,220 --> 00:02:43,140
hangs so if we go in the debugger we see

38
00:02:40,860 --> 00:02:45,660
that it's calling the

39
00:02:43,140 --> 00:02:47,580
TmResourceManagerExt function however it's

40
00:02:45,660 --> 00:02:49,860
not the right one because we see that

41
00:02:47,580 --> 00:02:52,379
it's actually called early in the code

42
00:02:49,860 --> 00:02:54,480
but that's not the one we are interested

43
00:02:52,379 --> 00:02:56,640
in we are interested in the one where we

44
00:02:54,480 --> 00:02:58,500
have changed the state of the

45
00:02:56,640 --> 00:03:00,720
enlistments already

46
00:02:58,500 --> 00:03:02,940
so we can continue execution another

47
00:03:00,720 --> 00:03:05,280
time we see that TmpSetNotificationResourceManager

48
00:03:02,940 --> 00:03:09,319
is called and again

49
00:03:05,280 --> 00:03:09,319
it's not the one we're interested in

50
00:03:14,580 --> 00:03:19,019
it's been called through recover

51
00:03:16,500 --> 00:03:21,860
transaction manager

52
00:03:19,019 --> 00:03:21,860
so we keep going

53
00:03:23,519 --> 00:03:27,420
so another way of doing it would be to

54
00:03:25,379 --> 00:03:29,580
disable the breakpoints and re-enable

55
00:03:27,420 --> 00:03:30,540
them later

56
00:03:29,580 --> 00:03:33,140
so we're going to disable the

57
00:03:30,540 --> 00:03:33,140
breakpoints

58
00:03:39,480 --> 00:03:44,099
so here we are changing the state of the

59
00:03:42,000 --> 00:03:46,620
enlistments we've changed them by

60
00:03:44,099 --> 00:03:49,860
calling pre-prepare complete and now we're

61
00:03:46,620 --> 00:03:52,620
going to call prepare completes

62
00:03:49,860 --> 00:03:54,060
so we've called paper complete now so

63
00:03:52,620 --> 00:03:55,560
now we're going to call the recovery

64
00:03:54,060 --> 00:03:56,940
resource manager and hopefully we're

65
00:03:55,560 --> 00:04:00,739
going to reach the vulnerable function

66
00:03:56,940 --> 00:04:00,739
so we can re-enable the breakpoints

67
00:04:05,940 --> 00:04:09,319
I'm going to continue execution

68
00:04:11,459 --> 00:04:14,760
so now we are in the vulnerable function

69
00:04:13,080 --> 00:04:18,859
let's debug it

70
00:04:14,760 --> 00:04:18,859
we are going to use the F10 to step over

71
00:04:20,820 --> 00:04:24,740
and we can do it from Ghidra itself

72
00:04:52,800 --> 00:04:57,720
okay so in the debugger we have stepped

73
00:04:56,160 --> 00:04:59,699
a couple of times we see that it's

74
00:04:57,720 --> 00:05:03,680
actually going to test these states of

75
00:04:59,699 --> 00:05:03,680
the resource manager so then

76
00:05:04,199 --> 00:05:07,580
he's doing a couple of tests

77
00:05:08,820 --> 00:05:15,960
so here we actually want to

78
00:05:13,620 --> 00:05:17,460
skip that and just reach the beginning

79
00:05:15,960 --> 00:05:20,220
of the while loop so we're going to set

80
00:05:17,460 --> 00:05:23,360
the break points here with F2

81
00:05:20,220 --> 00:05:23,360
and continue execution

82
00:05:29,580 --> 00:05:36,080
so we can see we

83
00:05:31,860 --> 00:05:36,080
enter that while loop

84
00:05:46,860 --> 00:05:51,780
so here we can see we actually inside

85
00:05:49,080 --> 00:05:55,639
the case where the enlistment is

86
00:05:51,780 --> 00:05:55,639
actually not finalized

87
00:06:16,460 --> 00:06:21,860
and now we're going to be testing the

88
00:06:19,020 --> 00:06:21,860
enlistment flag

89
00:06:34,080 --> 00:06:36,259


90
00:06:41,639 --> 00:06:48,620
so state is at offset c0 so RAX is

91
00:06:45,840 --> 00:06:48,620
actually the transaction

92
00:07:05,100 --> 00:07:10,440
so we can confirm this is a valid cookie

93
00:07:07,860 --> 00:07:12,600
the transaction points to itself it has

94
00:07:10,440 --> 00:07:15,080
two enlistments which is what we are

95
00:07:12,600 --> 00:07:15,080
expecting

96
00:07:21,780 --> 00:07:27,180
and it is in the KTRANSACTION committed

97
00:07:24,180 --> 00:07:29,340
state so even though we prepared the

98
00:07:27,180 --> 00:07:31,800
enlistment the transaction itself is

99
00:07:29,340 --> 00:07:34,520
already committed so let's have a look

100
00:07:31,800 --> 00:07:34,520
at the enlistment

101
00:07:39,780 --> 00:07:45,240
so if we go back to Vergilius

102
00:07:43,139 --> 00:07:48,139
and look at the

103
00:07:45,240 --> 00:07:48,139
KTRANSACTION

104
00:07:57,060 --> 00:08:03,360
we see it points to a list of enlistments

105
00:08:00,720 --> 00:08:06,419
and then each enlistment is going to be

106
00:08:03,360 --> 00:08:09,120
part of this next same transaction so if

107
00:08:06,419 --> 00:08:12,139
we just subtract 78 we're going to go to

108
00:08:09,120 --> 00:08:12,139
the beginning of the enlistment

109
00:08:19,199 --> 00:08:23,360
okay this is valid the cookie is valid

110
00:08:23,699 --> 00:08:27,720
so this is our first enlistment and

111
00:08:25,979 --> 00:08:30,620
the second enlistment

112
00:08:27,720 --> 00:08:30,620
will be

113
00:08:35,219 --> 00:08:42,060
this one we should be able to confirm

114
00:08:37,800 --> 00:08:44,580
the grid are 02C4 and 02C5 which

115
00:08:42,060 --> 00:08:47,420
here here we have is 02C4 and

116
00:08:44,580 --> 00:08:47,420
02C5

117
00:08:50,519 --> 00:08:57,240
so these are two enlistments

118
00:08:54,060 --> 00:09:00,000
the enlistments are in these states so

119
00:08:57,240 --> 00:09:02,459
if we go back to here the test that the

120
00:09:00,000 --> 00:09:04,200
enlistment flags is not superior and

121
00:09:02,459 --> 00:09:07,519
the transaction is in the committed

122
00:09:04,200 --> 00:09:07,519
state so it's gonna go there

123
00:09:28,080 --> 00:09:33,899
so we can see that the move "bl r9b" was

124
00:09:31,980 --> 00:09:36,240
actually the bSendNotification equal

125
00:09:33,899 --> 00:09:39,000
true so now we're gonna set the

126
00:09:36,240 --> 00:09:41,700
breakpoints on this if condition where

127
00:09:39,000 --> 00:09:43,680
it actually tests BL

128
00:09:41,700 --> 00:09:47,060
we use F2

129
00:09:43,680 --> 00:09:47,060
and we continue execution

130
00:09:49,320 --> 00:09:53,820
so we see that we have our

131
00:09:52,140 --> 00:09:55,500
bSendNotification set to one

132
00:09:53,820 --> 00:09:58,019
so we know we're going to go into this

133
00:09:55,500 --> 00:10:00,560
call this TmpSetNotificationResourceManager

134
00:09:58,019 --> 00:10:00,560
call

135
00:10:03,000 --> 00:10:06,140
and bam

136
00:10:07,080 --> 00:10:13,860
it worked so we have from our userland

137
00:10:11,399 --> 00:10:16,440
binary we call recovery resource manager

138
00:10:13,860 --> 00:10:19,320
it's called our syscall which made it go

139
00:10:16,440 --> 00:10:21,540
into kernel land and then from kernel

140
00:10:19,320 --> 00:10:23,459
called the TmRecoverResourceManagerExt

141
00:10:21,540 --> 00:10:26,220
function which is our vulnerable

142
00:10:23,459 --> 00:10:28,200
function and it ended up calling

143
00:10:26,220 --> 00:10:30,120
TmpSetNotificationResourceManager

144
00:10:28,200 --> 00:10:31,680
to send the notification to userland

145
00:10:30,120 --> 00:10:33,980
we've reached our goal thank you for

146
00:10:31,680 --> 00:10:33,980
watching

