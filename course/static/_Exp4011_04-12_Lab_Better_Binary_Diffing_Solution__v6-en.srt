1
00:00:00,120 --> 00:00:04,740
so let's define better the source code

2
00:00:02,760 --> 00:00:06,960
of this function so here we're getting

3
00:00:04,740 --> 00:00:07,980
the enlistment head initially the type of

4
00:00:06,960 --> 00:00:10,620
this

5
00:00:07,980 --> 00:00:13,019
was list entry

6
00:00:10,620 --> 00:00:15,240
so if we do Ctrl L it was a list entry

7
00:00:13,019 --> 00:00:18,139
so now we can define it as a KENLISTMENT

8
00:00:15,240 --> 00:00:18,139
shifted pointer

9
00:00:22,859 --> 00:00:26,720
add a star it's either pointer

10
00:00:27,359 --> 00:00:33,360
so we can see this was also

11
00:00:30,900 --> 00:00:35,460
changed to the same type so we're going

12
00:00:33,360 --> 00:00:37,680
to rename that to the actual

13
00:00:35,460 --> 00:00:40,260
enlistment shifted so this is really the

14
00:00:37,680 --> 00:00:43,559
enlistment head because it's hard coded

15
00:00:40,260 --> 00:00:45,960
but this one is retrieved
from the enlistment

16
00:00:43,559 --> 00:00:48,620
head to get the first one so we rename

17
00:00:45,960 --> 00:00:48,620
it with l

18
00:00:50,039 --> 00:00:54,539
what I'm going to call it shifted and

19
00:00:52,379 --> 00:00:56,340
actually this is more the shifted one

20
00:00:54,539 --> 00:00:58,020
this is more like the next one because

21
00:00:56,340 --> 00:00:59,579
when it's going to loop over each

22
00:00:58,020 --> 00:01:02,660
element it's going to initialize the

23
00:00:59,579 --> 00:01:02,660
current one with the next one

24
00:01:09,360 --> 00:01:13,799
so it's looping over all the enlistments

25
00:01:11,580 --> 00:01:15,479
getting the current one with

26
00:01:13,799 --> 00:01:17,640
initializing it with the next one and

27
00:01:15,479 --> 00:01:19,260
then if the current one equals to the

28
00:01:17,640 --> 00:01:21,960
enlistment heads which means it

29
00:01:19,260 --> 00:01:24,240
reached the end of the list it exits so

30
00:01:21,960 --> 00:01:26,400
here it's going to retrieve the next

31
00:01:24,240 --> 00:01:28,560
pointer but it's still going to work

32
00:01:26,400 --> 00:01:31,380
with the current one for the remaining

33
00:01:28,560 --> 00:01:33,780
steps of the loop so we see it has the

34
00:01:31,380 --> 00:01:36,420
flag if it's finalized here we see all

35
00:01:33,780 --> 00:01:38,520
the flags are in use and here it's

36
00:01:36,420 --> 00:01:41,400
getting the next so an interesting thing

37
00:01:38,520 --> 00:01:45,119
is there is a first while loop here

38
00:01:41,400 --> 00:01:47,040
um the next was used here in the loop

39
00:01:45,119 --> 00:01:49,500
but then it's becoming the actual

40
00:01:47,040 --> 00:01:52,860
current one again because it's used in

41
00:01:49,500 --> 00:01:57,979
in the list and this looping over it so

42
00:01:52,860 --> 00:01:57,979
we're gonna do split out as new variable

43
00:01:59,460 --> 00:02:03,560
I'm going to add an underscore at the

44
00:02:01,439 --> 00:02:03,560
end

45
00:02:05,939 --> 00:02:09,660
so we have an unknown Boolean here so

46
00:02:08,459 --> 00:02:11,700
here

47
00:02:09,660 --> 00:02:13,800
um we're working with pEnlistment_shifted

48
00:02:11,700 --> 00:02:15,420
but we are pointing to the address of

49
00:02:13,800 --> 00:02:17,400
the cookie and we know in the normal

50
00:02:15,420 --> 00:02:20,760
structure the cookie the first element

51
00:02:17,400 --> 00:02:23,720
so here really it's retrieving the start

52
00:02:20,760 --> 00:02:23,720
of the enlistments

53
00:02:24,239 --> 00:02:29,180
and so we can Define that as a

54
00:02:26,940 --> 00:02:29,180
KENLISTMENT

55
00:02:30,480 --> 00:02:33,140
pointer

56
00:02:34,080 --> 00:02:38,580
and it does make sense because it's

57
00:02:35,520 --> 00:02:40,260
passed as ObfReferenceObject so it's

58
00:02:38,580 --> 00:02:44,660
increasing the ref count for this object

59
00:02:40,260 --> 00:02:44,660
here it's retrieving the enlistment flag

60
00:02:46,680 --> 00:02:50,120
it's returning the transaction

61
00:02:57,780 --> 00:03:03,300
so here Ghidra wasn't able to mark that

62
00:03:00,840 --> 00:03:04,920
as an enum however we know that it's

63
00:03:03,300 --> 00:03:07,980
actually

64
00:03:04,920 --> 00:03:10,019
the flags from KENLISTMENT so it's

65
00:03:07,980 --> 00:03:13,159
actually a KENLISTMENT flag so if we look

66
00:03:10,019 --> 00:03:13,159
for the KENLISTMENT flag

67
00:03:13,680 --> 00:03:20,780
four is can it's been finalized

68
00:03:18,360 --> 00:03:20,780
okay

69
00:03:21,180 --> 00:03:24,440
so I'm just going to add a comment

70
00:03:31,379 --> 00:03:35,840
okay

71
00:03:33,659 --> 00:03:35,840
yeah

72
00:03:40,379 --> 00:03:46,620
so because it's KENLISTMENT finalized

73
00:03:43,799 --> 00:03:49,440
if the current enlistment flags is

74
00:03:46,620 --> 00:03:52,500
finalized which is if it doesn't end

75
00:03:49,440 --> 00:03:54,120
with this enum is no no it's going to

76
00:03:52,500 --> 00:03:58,040
set a flag so we could actually name

77
00:03:54,120 --> 00:03:58,040
this flag be finalized

78
00:04:04,860 --> 00:04:09,319
and so this one is also be finalized

79
00:04:24,060 --> 00:04:29,340
so let's have a last run on checking

80
00:04:27,540 --> 00:04:31,919
what has been defined we see that b

81
00:04:29,340 --> 00:04:34,199
unknown 2 hasn't been defined yet so we

82
00:04:31,919 --> 00:04:37,020
can see it's set to false in this loop

83
00:04:34,199 --> 00:04:39,380
and in such case when it enters the if

84
00:04:37,020 --> 00:04:43,380
condition it can actually set it to true

85
00:04:39,380 --> 00:04:47,000
so if the enlistment flag is less than a

86
00:04:43,380 --> 00:04:47,000
certain value and then later

87
00:04:49,139 --> 00:04:52,919
here

88
00:04:50,520 --> 00:04:54,660
it's going to use that flag and if it

89
00:04:52,919 --> 00:04:57,419
enters that if condition it's going to

90
00:04:54,660 --> 00:04:59,040
call the TMP set notification resource

91
00:04:57,419 --> 00:05:01,500
manager

92
00:04:59,040 --> 00:05:04,979
and then it's not use so I think we can

93
00:05:01,500 --> 00:05:08,479
rename that Boolean to notify because

94
00:05:04,979 --> 00:05:08,479
it's going to set a notification

95
00:05:08,520 --> 00:05:11,900
be notified

96
00:05:12,720 --> 00:05:18,840
the do notify so now that we have

97
00:05:15,360 --> 00:05:21,960
defined the B do notify let's see where

98
00:05:18,840 --> 00:05:24,720
it's used so it's actually set into this

99
00:05:21,960 --> 00:05:27,780
if condition and it's when the end has

100
00:05:24,720 --> 00:05:30,000
been flagged is less than zero so here

101
00:05:27,780 --> 00:05:31,740
the output of Ghidra is a little bit

102
00:05:30,000 --> 00:05:33,600
confusing but if you think about it's

103
00:05:31,740 --> 00:05:36,120
less than zero

104
00:05:33,600 --> 00:05:39,600
what that mean basically it means that

105
00:05:36,120 --> 00:05:42,919
the higher bit is set so if you look at

106
00:05:39,600 --> 00:05:42,919
the enlistment flag

107
00:05:43,620 --> 00:05:49,740
the highest best is the bits eight so it

108
00:05:47,039 --> 00:05:52,759
means the  KENLISTMENT
is notifiable so here

109
00:05:49,740 --> 00:05:52,759
we can add a comment

110
00:05:55,860 --> 00:06:00,020
saying the can explain is not available

111
00:06:04,259 --> 00:06:08,699
so we have defined everything in the

112
00:06:06,720 --> 00:06:10,680
patch file as well so we can see the

113
00:06:08,699 --> 00:06:13,380
structures have been imported and all

114
00:06:10,680 --> 00:06:17,539
the accesses make sense we have defined

115
00:06:13,380 --> 00:06:17,539
the shifted pointer as well

116
00:06:20,160 --> 00:06:24,740
and the flag for notification

117
00:06:26,100 --> 00:06:33,660
so we could add one for the patch file

118
00:06:29,220 --> 00:06:36,240
so we have copied into two source files

119
00:06:33,660 --> 00:06:39,060
one for the patch and one for the

120
00:06:36,240 --> 00:06:40,919
vulnerable file and we have also changed

121
00:06:39,060 --> 00:06:44,220
the if condition similarly to what we

122
00:06:40,919 --> 00:06:47,220
did before so it matches the available

123
00:06:44,220 --> 00:06:49,440
one we have loaded the them in the

124
00:06:47,220 --> 00:06:52,100
actual win merge tool and we have

125
00:06:49,440 --> 00:06:55,979
actually generated a report into a

126
00:06:52,100 --> 00:06:57,660
diff.htm so loading the diff we see a

127
00:06:55,979 --> 00:06:59,819
couple of changes due to the different

128
00:06:57,660 --> 00:07:01,979
names for the stack variables which we

129
00:06:59,819 --> 00:07:04,020
don't care we see the layout is a bit

130
00:07:01,979 --> 00:07:06,240
different for the pEnlistment_shifted

131
00:07:04,020 --> 00:07:10,139
which we don't care so we see the

132
00:07:06,240 --> 00:07:11,940
bFinalized flag was removed in the patch

133
00:07:10,139 --> 00:07:15,840
file so it was initialized to false

134
00:07:11,940 --> 00:07:18,240
initially in the vulnerable file

135
00:07:15,840 --> 00:07:21,960
so then there is this loop looping over

136
00:07:18,240 --> 00:07:24,599
each of the enlistments of the list and

137
00:07:21,960 --> 00:07:27,120
there's a little difference in the

138
00:07:24,599 --> 00:07:29,699
states of the transaction being tested

139
00:07:27,120 --> 00:07:32,940
which we saw previously but in this case

140
00:07:29,699 --> 00:07:35,699
it's actually testing if the enlistment is

141
00:07:32,940 --> 00:07:37,560
notifiable and if it's the case it's

142
00:07:35,699 --> 00:07:39,900
going to do certain things and at the

143
00:07:37,560 --> 00:07:42,720
end it's going to actually set the B do

144
00:07:39,900 --> 00:07:46,080
notify flag potentially in both cases

145
00:07:42,720 --> 00:07:48,599
but then it's actually modifying some

146
00:07:46,080 --> 00:07:51,240
stack variables but we saw it's the same

147
00:07:48,599 --> 00:07:53,580
way okay so now if we are in the case

148
00:07:51,240 --> 00:07:55,560
where we do notify we're going to call

149
00:07:53,580 --> 00:07:58,139
the TMP set notification resource

150
00:07:55,560 --> 00:07:59,880
manager in both cases however in the

151
00:07:58,139 --> 00:08:02,880
patched version something has been

152
00:07:59,880 --> 00:08:06,060
removed so this test so basically what

153
00:08:02,880 --> 00:08:09,240
it's doing is that it's testing if the

154
00:08:06,060 --> 00:08:10,860
enlistment flag is set to finalized and

155
00:08:09,240 --> 00:08:12,900
if it's set to finalize it's going to

156
00:08:10,860 --> 00:08:15,419
set the boolean bFinalized to true

157
00:08:12,900 --> 00:08:17,880
which we know doesn't exist this flag

158
00:08:15,419 --> 00:08:21,360
doesn't in the patch version okay and

159
00:08:17,880 --> 00:08:22,680
then we have a test to potentially go to

160
00:08:21,360 --> 00:08:24,660
the end of the function in the

161
00:08:22,680 --> 00:08:27,060
vulnerable version it was only testing

162
00:08:24,660 --> 00:08:29,340
the resource manager if it was online it

163
00:08:27,060 --> 00:08:31,199
wasn't online it would go to the end of

164
00:08:29,340 --> 00:08:34,200
the function and in the patch version

165
00:08:31,199 --> 00:08:36,120
It's testing it makes sure the

166
00:08:34,200 --> 00:08:37,740
ResourceManager is still online but also the

167
00:08:36,120 --> 00:08:39,360
transaction manager pointer is not

168
00:08:37,740 --> 00:08:41,479
null and that the state of the

169
00:08:39,360 --> 00:08:43,560
transaction manager is also online

170
00:08:41,479 --> 00:08:46,260
otherwise it go to the end of the

171
00:08:43,560 --> 00:08:48,120
function so a couple more tests which is

172
00:08:46,260 --> 00:08:52,200
interesting I guess the most interesting

173
00:08:48,120 --> 00:08:56,100
change is that it enforces in the patch

174
00:08:52,200 --> 00:08:59,160
file that the pEnlistment_shifted

175
00:08:56,100 --> 00:09:01,380
pointer for the next iteration is set to

176
00:08:59,160 --> 00:09:04,140
the resource manager enlistment head

177
00:09:01,380 --> 00:09:06,000
Flink so basically what it does is it

178
00:09:04,140 --> 00:09:08,339
sets it to the very beginning of the

179
00:09:06,000 --> 00:09:10,740
list starting from the enlistment head

180
00:09:08,339 --> 00:09:13,920
however in the vulnerable version what

181
00:09:10,740 --> 00:09:15,779
it would do it would skip over this if

182
00:09:13,920 --> 00:09:18,839
and then it would basically reach this

183
00:09:15,779 --> 00:09:22,140
say okay is the bFinalized flag is set

184
00:09:18,839 --> 00:09:24,779
and if it's not set it's going to go to

185
00:09:22,140 --> 00:09:27,839
next which is basically going to take

186
00:09:24,779 --> 00:09:30,720
the p enlistment shifted from the

187
00:09:27,839 --> 00:09:32,820
 penlistment shifted Flink so it's going

188
00:09:30,720 --> 00:09:34,920
to basically use the p and it's been

189
00:09:32,820 --> 00:09:37,560
shifted to get access to the next one

190
00:09:34,920 --> 00:09:40,320
continue in the list however if it is

191
00:09:37,560 --> 00:09:42,660
finalized it's gonna actually get it

192
00:09:40,320 --> 00:09:45,120
from the beginning of the list so the

193
00:09:42,660 --> 00:09:47,399
difference is it uses the bFinalized

194
00:09:45,120 --> 00:09:50,220
flag that was retrieved from the

195
00:09:47,399 --> 00:09:53,640
enlistment flag to actually decide if

196
00:09:50,220 --> 00:09:56,700
it goes from the head of the list from

197
00:09:53,640 --> 00:09:59,160
here or if it goes from the current

198
00:09:56,700 --> 00:10:02,100
enlistment to get the next one however

199
00:09:59,160 --> 00:10:05,160
in the patch file it's gonna always get

200
00:10:02,100 --> 00:10:06,180
it from the head of the list so it's

201
00:10:05,160 --> 00:10:08,580
never going to use the current

202
00:10:06,180 --> 00:10:10,380
enlistments that's very interesting we

203
00:10:08,580 --> 00:10:13,500
will think about it if for whatever

204
00:10:10,380 --> 00:10:16,200
reason the TMP set notification resource

205
00:10:13,500 --> 00:10:19,680
manager function changes the enlistment

206
00:10:16,200 --> 00:10:23,519
and frees it potentially this KENLISTMENT

207
00:10:19,680 --> 00:10:27,060
pointer is a stale pointer and may point

208
00:10:23,519 --> 00:10:29,339
to uninitialized memory like freed

209
00:10:27,060 --> 00:10:32,279
memory so potentially there is some kind

210
00:10:29,339 --> 00:10:34,800
of race condition it could result into a

211
00:10:32,279 --> 00:10:36,600
use after free which is now not the

212
00:10:34,800 --> 00:10:39,060
case anymore because it's never going to

213
00:10:36,600 --> 00:10:40,920
reuse the KENLISTMENT pointer because

214
00:10:39,060 --> 00:10:43,740
it's going to use the head of the list

215
00:10:40,920 --> 00:10:46,560
instead of the actual pointer for the

216
00:10:43,740 --> 00:10:49,800
current one so we see that actually

217
00:10:46,560 --> 00:10:52,860
renaming everything helped us determine

218
00:10:49,800 --> 00:10:55,680
what the bug is and it's going to help

219
00:10:52,860 --> 00:10:57,779
us as well when we analyze more how to

220
00:10:55,680 --> 00:11:01,459
explore this vulnerability in one of the

221
00:10:57,779 --> 00:11:01,459
next video thank you for watching

