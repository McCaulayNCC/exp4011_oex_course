1
00:00:00,060 --> 00:00:04,560
the way we are winning the race is by

2
00:00:02,340 --> 00:00:08,040
cheating we are basically creating an

3
00:00:04,560 --> 00:00:10,679
infinite loop by patching the call to

4
00:00:08,040 --> 00:00:13,320
the KeWaitForSingleObject because

5
00:00:10,679 --> 00:00:15,780
it's after the finalization check but

6
00:00:13,320 --> 00:00:18,000
before the difference which allows us to

7
00:00:15,780 --> 00:00:21,600
change things and so this particular

8
00:00:18,000 --> 00:00:23,820
place is good to block temporarily so it

9
00:00:21,600 --> 00:00:26,400
doesn't affect anything else negatively

10
00:00:23,820 --> 00:00:28,680
I guess the main thing that we didn't go

11
00:00:26,400 --> 00:00:32,099
into detail is that in order to actually

12
00:00:28,680 --> 00:00:34,140
finalize an enlistment and free it the

13
00:00:32,099 --> 00:00:36,540
resource manager's mutex has to be

14
00:00:34,140 --> 00:00:39,480
unlocked and so basically part of

15
00:00:36,540 --> 00:00:43,140
freeing the enlistment is finalizing it

16
00:00:39,480 --> 00:00:45,899
and so it has to be after it checks to

17
00:00:43,140 --> 00:00:48,000
see if it is finalized but it also has

18
00:00:45,899 --> 00:00:50,820
to be before it actually locks the mutex

19
00:00:48,000 --> 00:00:53,460
so really any instructions between those

20
00:00:50,820 --> 00:00:56,899
two points is okay to simulate a

21
00:00:53,460 --> 00:00:56,899
blocking state that is realistic

